﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Data {
	/// <summary>
	/// An interface for objects that Grunt can load.
	/// </summary>
	public interface ILoadable {
		/// <summary>
		/// Whether or not this object has finished loading.
		/// </summary>
		bool IsLoaded { get; }

		/// <summary>
		/// The name and relative location of the file this object will be loaded from.
		/// </summary>
		string PathAndFilename { get; }

		/// <summary>
		/// Orders this object to begin the loading immediantly, in the current thread.
		/// Called by Grunt when it is this object's turn to load.
		/// </summary>
		void LoadNow();

		/// <summary>
		/// An event called when Grunt finishes loading this object.
		/// </summary>
		/// <remarks>
		/// <para>Because of the nature of threaded programs, it is possible that OnLoaded can be
		/// fired added *after* Grunt finishes loading the object, in which case the event will never
		/// fire. To prevent this, in your implementation of OnLoaded, you should immediantly fire the
		/// event if your object has already finished loading. The example below demonstrates a way
		/// to do this.</para>
		/// <para>Remember to use a lock, or else it's possible for Grunt and your object to fire the
		/// event at the same time.</para>
		/// <para>// This is the internal event used by the accessor.</para>
		/// <para>private EventHandler onLoaded;</para>
		/// <para>// Here is a private object to use for the lock.</para>
		/// <para>private object lockLobject;</para>
		/// <para>// This is the actual accessor that will be called by Grunt.</para>
		/// <para>public EventHandler OnLoaded {</para>
		///	<para>	get {</para>
		///	<para>		lock(lockObject) {</para>
		///	<para>			return onLoaded;</para>
		///	<para>		}</para>
		///	<para>	}</para>
		///	<para>	set {</para>
		///	<para>		lock(lockObject) {</para>
		///	<para>			if(IsLoaded) {</para>
		///	<para>				value.Invoke(this, EventArgs.Empty);</para>
		///	<para>			} else {</para>
		///	<para>				onLoaded = value;</para>
		///	<para>			}</para>
		///	<para>		}</para>
		///	<para>	}</para>
		/// <para>}</para>
		/// </remarks>
		EventHandler OnLoaded { get; }
	}
}
