﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace VisionRiders.OFE.Data {
	/// <summary>
	/// An attribute that causes the OFEdata serializer to skip a field or property.
	/// </summary>
	[AttributeUsage(AttributeTargets.All)]
	public class OFEdataIgnore : System.Attribute {
	}

	/// <summary>
	/// An attribute the causes the serializer to serialize the fields and properties of a class or struct.
	/// </summary>
	[AttributeUsage(AttributeTargets.All)]
	public class OFEdataSerialize : System.Attribute {
	}

	/// <summary>
	/// Serializes and deserializes objects into and from OFEdata files.
	/// </summary>
	/// <remaks>Note that the code is still very much in progress!
	/// Some kinds of data may not serialize/deserialize correctly.</remaks>
	public class OFEdataSerializer {
		#region Class Constructor
		/// <summary>
		/// Creates a new OFEdata serializer to serialize and deserialize OFEdata for the given object type.
		/// </summary>
		/// <param name="type">The object type this instance of the serializer will be working on.</param>
		/// <exception cref="ArgumentException">Thrown if you do not specify a type by trying to use null.</exception>
		public OFEdataSerializer(Type type) {
			if(type == null) {
				throw new ArgumentException("Type cannot be null. You must specify a type for the serializer to work with.");
			} else {
				this.type = type;
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// How many spaces constitute an indentation in the OFEdata document.
		/// Meaningless when deserializing, of course, but used when serializing.
		/// </summary>
		public int IndentSize = 3;

		/// <summary>
		/// The type of object this serializer will perform on.
		/// </summary>
		private Type type { get; set; }
		#endregion

		#region Serialization
		/// <summary>
		/// Serializes an object into an OFEdata file using the given stream.
		/// </summary>
		/// <param name="stream">The stream to write to.</param>
		/// <param name="o">The object to serialize.</param>
		/// <exception cref="IOException">Thrown if the stream does not have write access.</exception>
		public void Serialize(Stream stream, object o) {
			// Make sure we can actually write to the stream.
			if(!stream.CanWrite) {
				throw new IOException("The given stream does not have write access. Cannot serialize to this stream.");
			}

			StreamWriter writer = new StreamWriter(stream);

			// Write the header.
			writer.WriteLine("!OFEdata Version: 1.0");
			writer.WriteLine();

			// Begin on the root object.
			serializeObject(writer, o.GetType().Name, o, 0);

			// We're done. Flush any remaining data to the stream.
			writer.Flush();
		}

		/// <summary>
		/// Serialize the given object to the stream.
		/// </summary>
		/// <param name="writer">The stream to serialize to.</param>
		/// <param name="nodeName">The name of this node.</param>
		/// <param name="o">The object to serialize.</param>
		/// <param name="indent">How many times to indent, based on this object's depth in the root object.</param>
		private void serializeObject(StreamWriter writer, string nodeName, object o, int indent) {
			// Open a node.
			if (!string.IsNullOrEmpty(nodeName))
				writer.WriteLine(padding(indent) + nodeName + " {");
			else
				writer.WriteLine(padding(indent) + "{");

			// Increase indentation by 1.
			indent += 1;

			// Go through each public member in the object.
			foreach(MemberInfo member in o.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance)) {
				// Don't serialize things tagged to not be serialized.
				if(Attribute.IsDefined(member, typeof(NonSerializedAttribute)) || Attribute.IsDefined(member, typeof(OFEdataIgnore))) {
					continue;
				}

				// We're only interested in fields and properties.
				if(member.MemberType == MemberTypes.Property || member.MemberType == MemberTypes.Field) {
					// Get the name of the property.
					string name = member.Name;

					// Get the value of the property.
					string value = string.Empty;
					switch(member.MemberType) {
						case MemberTypes.Property:
							PropertyInfo property = member as PropertyInfo;
							if(typeof(ICollection).IsAssignableFrom(property.PropertyType) && property.GetValue(o, null) != null) {
								ICollection collection = (ICollection)property.GetValue(o, null);
								writer.WriteLine(padding(indent) + name + " {");
								foreach(object item in collection) {
									if(Attribute.IsDefined(item.GetType(), typeof(OFEdataSerialize))) {
										serializeObject(writer, string.Empty, item, indent + 1);
									} else {
										writer.WriteLine(padding(indent + 1) + item.ToString());
									}
								}
								writer.WriteLine(padding(indent) + "}");
							} else {
								ParameterInfo[] parameters = property.GetIndexParameters();
								if(parameters.Length == 0) {
									// Ignore nulls.
									if(property.GetValue(o, null) != null) {
										if(Attribute.IsDefined(property.GetValue(o, null).GetType(), typeof(OFEdataSerialize))) {
											serializeObject(writer, property.Name, property.GetValue(o, null), indent);
										} else {
											value = property.GetValue(o, null).ToString();
											if (!string.IsNullOrWhiteSpace(value))
												writer.WriteLine(padding(indent) + name + ": " + value);
											else
												writer.WriteLine(padding(indent) + name);
										}
									}
								}
							}							
							break;

						case MemberTypes.Field:
							FieldInfo field = member as FieldInfo;
							if(typeof(ICollection).IsAssignableFrom(field.FieldType) && field.GetValue(o) != null) {
								ICollection collection = (ICollection)field.GetValue(o);
								writer.WriteLine(padding(indent) + name + " {");
								foreach(object item in collection) {
									if(Attribute.IsDefined(item.GetType(), typeof(OFEdataSerialize))) {
										serializeObject(writer, string.Empty, item, indent + 1);
									} else {
										writer.WriteLine(padding(indent + 1) + item.ToString());
									}
								}
								writer.WriteLine(padding(indent) + "}");
							} else {
								// Ignore nulls.
								if(field.GetValue(o) != null) {
									if(Attribute.IsDefined(field.GetValue(o).GetType(), typeof(OFEdataSerialize))) {
										serializeObject(writer, field.Name, field.GetValue(o), indent);
									} else {
										value = field.GetValue(o).ToString();
										if (!string.IsNullOrWhiteSpace(value))
											writer.WriteLine(padding(indent) + name + ": " + value);
										else
											writer.WriteLine(padding(indent) + name);
									}
								}
							}

							break;
					}
				}
			}

			// Decrease indentation.
			indent -= 1;

			// Close the node.
			writer.WriteLine(padding(indent) + "}");
		}
		#endregion

		#region Deserialization
		/// <summary>
		/// Deserializes the OFEdata contained in the given stream.
		/// </summary>
		/// <param name="stream">The stream that contains OFEdata to deserialize.</param>
		/// <exception cref="IOException">Thrown if the given stream does not have reading access.</exception>
		/// <exception cref="MissingMethodException">Thrown if the object to deserialize does not have a parameterless class constructor.</exception>
		/// <returns>Null if no valid data nodes were found to deserialize.</returns>
		public object Deserialize(Stream stream) {
			// Make sure we can actually read from the stream.
			if(!stream.CanRead) {
				throw new IOException("The given stream does not have reading access. Cannot deserialize from this stream.");
			}

			// And Dale said, "Let there be a return object!"
			// And there was a return object. And lo, it was good.
			object o;
			try {
				o = Activator.CreateInstance(type);
			} catch {
				throw new MissingMethodException("The object you are deserializing must have a parameterless constructor.");
			}

			// Let's parse this sucker.
			DataTree tree = OFEdata.FromStream(stream, null);

			// Find the root node.			
			DataTree root = tree.GetNode(o.GetType().Name);

			// If we couldn't find anything, return nothing.
			if(root == null) {
				return null;
			}

			// Otherwise, start with that root.
			deserializeObject(root, ref o);

			// Done. Send back our results.
			return o;
		}

		/// <summary>
		/// Deserializes a single object.
		/// </summary>
		/// <param name="tree">The data tree to work from.</param>
		/// <param name="o">The object to deserialize.</param>
		private void deserializeObject(DataTree tree, ref object o) {
			// Now let's go through the object and see what to do about it.
			foreach (MemberInfo member in o.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance))
			{
				// Don't bother with things that weren't serialized.
				if (Attribute.IsDefined(member, typeof(NonSerializedAttribute)) || Attribute.IsDefined(member, typeof(OFEdataIgnore)))
				{
					continue;
				}

				// Let's see what kind of member this is.
				if (member.MemberType == MemberTypes.Property || member.MemberType == MemberTypes.Field)
				{
					// Get the name of the property.
					string name = member.Name;

					// Find the corresponding node in the data tree.
					DataTree node = tree.GetNode(name, tree.Name);

					// If we can't find it, the data must not be there.
					// Skip it and move on.
					if (node == null)
					{
						continue;
					}

					// Set the value of the property.
					string value = string.Empty;
					switch (member.MemberType)
					{
						case MemberTypes.Property:
							PropertyInfo property = member as PropertyInfo;
							if (!property.CanWrite) { continue; }

							Type propertyType = property.PropertyType;
							if (propertyType == typeof(String))
								property.SetValue(o, node.Value, null);
							else if (propertyType == typeof(Int16) || propertyType == typeof(Int32))
								property.SetValue(o, node.IntValue, null);
							else if (propertyType == typeof(Int64))
								property.SetValue(o, node.LongIntValue, null);
							else if (propertyType == typeof(Single))
								property.SetValue(o, node.FloatValue, null);
							else if (propertyType == typeof(Double))
								property.SetValue(o, node.DoubleValue, null);
							else if (propertyType == typeof(Decimal))
								property.SetValue(o, node.DecimalValue, null);
							else if (propertyType == typeof(Boolean))
								property.SetValue(o, node.BoolValue, null);
							else if (propertyType.IsEnum)
							{
								try
								{
									object enumObject = Enum.Parse(propertyType, node.Value);
									property.SetValue(o, enumObject, null);
								}
								catch (ArgumentException)
								{
									DebugLog.Failure("Could not find an enumerated value for " + node.Value + "!");
								}
							}
							else if (typeof(IList).IsAssignableFrom(property.PropertyType))
							{
								Type[] listType = property.PropertyType.GetGenericArguments();
								IList list = makeList(listType[0], node);
								property.SetValue(o, list, null);
							}
							else if (property.GetValue(o, null) != null && Attribute.IsDefined(property.GetValue(o, null).GetType(), typeof(OFEdataSerialize)))
							{
								object obj = FormatterServices.GetSafeUninitializedObject(propertyType);
								deserializeObject(node, ref obj);
								property.SetValue(o, obj, null);
							}

							break;
						case MemberTypes.Field:
							FieldInfo field = member as FieldInfo;

							Type fieldType = field.FieldType;

							if (fieldType == typeof(String))
								field.SetValue(o, node.Value);
							else if (fieldType == typeof(Int16) || fieldType == typeof(Int32))
								field.SetValue(o, node.IntValue);
							else if (fieldType == typeof(Int64))
								field.SetValue(o, node.LongIntValue);
							else if (fieldType == typeof(Single))
								field.SetValue(o, node.FloatValue);
							else if (fieldType == typeof(Double))
								field.SetValue(o, node.DoubleValue);
							else if (fieldType == typeof(Decimal))
								field.SetValue(o, node.DecimalValue);
							else if (fieldType == typeof(Boolean))
								field.SetValue(o, node.BoolValue);
							else if (fieldType.IsEnum)
							{
								try
								{
									object enumObject = Enum.Parse(fieldType, node.Value);
									field.SetValue(o, enumObject);
								}
								catch (ArgumentException)
								{
									DebugLog.Failure("Could not find an enumerated value for " + node.Value + "!");
								}
							}
							else if (typeof(IList).IsAssignableFrom(field.FieldType))
							{
								Type[] listType = field.FieldType.GetGenericArguments();
								IList list = makeList(listType[0], node);
								field.SetValue(o, list);
							}
							else if (Attribute.IsDefined(fieldType, typeof(OFEdataSerialize)))
							{
								object obj = FormatterServices.GetSafeUninitializedObject(fieldType);
								deserializeObject(node, ref obj);
								field.SetValue(o, obj);
							}
							break;
					}
				}
			}
		}
		#endregion

		#region Other Helpers
		/// <summary>
		/// Creates a string of padding.
		/// </summary>
		/// <param name="indent">The number of indents to make. Each will be IndentSize in length.</param>
		private string padding(int indent) {
			return new string(' ', indent * IndentSize);
		}

		/// <summary>
		/// Creates a list of the given type using data from the given node.
		/// </summary>
		/// <param name="type">The Type of list to make.</param>
		/// <param name="node">The DataTree node to get the list from.</param>
		/// <returns>A list of the given type, containing the data from the node.</returns>
		private IList makeList(Type type, DataTree node) {
			// Use the activator to create a generic list of the given type.
			IList list = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(type));

			// Fill it with data.
			foreach (DataTree child in node.Children)
			{
				if (type == typeof(String))
					list.Add(child.Name);
				else if (type == typeof(Int16) || type == typeof(Int32) || type == typeof(Int64))
					list.Add(child.IntName);
				else if (type == typeof(Single))
					list.Add(child.FloatName);
				else if (type == typeof(Double))
					list.Add(child.DoubleName);
				else if (type == typeof(Decimal))
					list.Add(child.DecimalName);
				else if (type == typeof(Boolean))
					list.Add(child.BoolName);
				else if (type.IsEnum)
				{					
					try
					{
						object enumObject = Enum.Parse(type, child.Name);
						list.Add(enumObject);
					}
					catch (ArgumentException)
					{
						DebugLog.Failure("Could not find an enumerated value for " + child.Name + "!");
					}
				}
				else
				{
					object newObject = (object)FormatterServices.GetSafeUninitializedObject(type);
					deserializeObject(child, ref newObject);
					list.Add(newObject);
				}
			}

			// Pass it on back.
			return list;
		}
		#endregion
	}
}
