﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using System.Xml.Linq;

namespace VisionRiders.OFE.Data {
  /// <summary>
  /// A data tree object that can hold data parsed from a file. You can easily retrieve data from the
  /// tree by supplying the name of the node you're looking for, and even search for nodes with specific
  /// parents.
  /// </summary>
	/// <remarks>
	/// When ordered to load from a file, Data Trees are queued to be loaded in the background by Grunt.
	/// If you request data from a data tree that's still waiting to load, or is in the process of loading and parsing a file,
	/// the current thread will be held up waiting for the file's loading and parsing process to finish.
	/// </remarks>
#if DEBUG && WINDOWS
  [System.Diagnostics.DebuggerDisplay("{name}, {nodeValue}, {children.Count}")]
#endif
  public class DataTree : ILoadable {
		#region Class Constructors
		/// <summary>
		/// Creates a null data tree node.
		/// </summary>
		public DataTree() {
			isLoaded = true;
			onLoadedLock = new object();
			PathAndFilename = string.Empty;
		}

		/// <summary>
		/// Creates a data tree node and assigns it the given name.
		/// </summary>
		/// <param name="name">The name to give to this data tree node.</param>
		public DataTree(string name) {
			isLoaded = true;
			onLoadedLock = new object();
			this.name = name;
			PathAndFilename = string.Empty;
		}
		#endregion

    /// <summary>
    /// The children of this data node.
    /// </summary>
    public List<DataTree> Children {
			get {
				stall();
				return children;
			}
			set {
				stall();
				children = value;
			}
		}
		private List<DataTree> children = new List<DataTree>();

    /// <summary>
    /// The attribute nodes of this data node.
    /// </summary>
    public List<DataTree> Attributes {
			get {
				stall();
				return attributes;
			}
			set {
				stall();
				attributes = value;
			}
		}
		private List<DataTree> attributes = new List<DataTree>();

    /// <summary>
    /// This data node's parent, if any.
    /// </summary>
    public DataTree Parent {
			get {
				stall();
				return parent;
			}
			set {
				stall();
				parent = value;
			}
		}
		private DataTree parent;

		#region Name
		/// <summary>
    /// This node's key name.
    /// </summary>
    public string Name {
			get {
				stall();
				return name;
			}
			set {
				stall();
				name = value;
			}
		}
		private string name = string.Empty;

		/// <summary>
		/// This node's name in all lower-case.
		/// </summary>
		/// <remarks>
		/// Uses the invariant culture to perform the conversion, so this is safe
		/// for comparing data regardless of the system language.
		/// </remarks>
		public string NameToLower {
			get {
				return Name.ToLowerInvariant();
			}
		}

		/// <summary>
		/// This node's name in all upper-case.
		/// </summary>
		/// <remarks>
		/// Uses the invariant culture to perform the conversion, so this is safe
		/// for comparing data regardless of the system language.
		/// </remarks>
		public string NameToUpper {
			get {
				return Name.ToUpperInvariant();
			}
		}
		#endregion

		#region Value
		/// <summary>
    /// The value assigned to this node.
    /// </summary>
    public string Value {
			get {
				stall();
				return nodeValue;
			}
			set {
				stall();
				nodeValue = value;
			}
		}
		private string nodeValue = string.Empty;

		/// <summary>
		/// This node's value in all lower-case.
		/// </summary>
		/// <remarks>
		/// Using the invariant culture to perform the conversion, so this is safe
		/// for comparing data regardless of the system language.
		/// </remarks>
		public string ValueToLower {
			get {
				return Value.ToLowerInvariant();
			}
		}

		/// <summary>
		/// This node's value in all upper-case.
		/// </summary>
		/// <remarks>
		/// Using the invariant culture to perform the conversion, so this is safe
		/// for comparing data regardless of the system language.
		/// </remarks>
		public string ValueToUpper {
			get {
				return Value.ToUpperInvariant();
			}
		}
		#endregion

		public static string DoesNotExist {
      get {
				return string.Empty;
        /*if(Debug.Enabled) {
          return @"!CAN'T FIND!";
        } else {
          return string.Empty;
        }*/
      }
    }

		#region Retrieval
		/// <summary>
    /// Looks through the data and returns the value that matches the given node. This varient is more precise in that
    /// it looks for a node with a given parent so that you don't get it mixed up with another similarily named node
    /// elsewhere in the data tree.
    /// </summary>
    /// <param name="node">The name of the node you're looking for.</param>
    /// <param name="parent">The names of the parents of the node you're looking for.
    /// List the name of the immediate parent first, then work your way up the list of ancestors.</param>
    /// <returns>The string value you were looking for, if it exists. Otherwise it returns an empty string.</returns>
    public string Get(string name, params string[] parents) {
      DataTree node = get(name, this, parents);

      if(node != null) {
        return node.Value;
      } else {
        return DoesNotExist;
      }
    }

    /// <summary>
    /// Looks through the data tree and returns the node whose name matches the given value.
    /// </summary>
    /// <param name="name">The name of the node you're looking for.</param>
    /// /// <param name="parent">The names of the parents of the node you're looking for.
    /// List the name of the immediate parent first, then work your way up the list of ancestors.</param>
    /// <returns>Null if the given node cannot be found.</returns>
    public DataTree GetNode(string name, params string[] parents) {
      return get(name, this, parents);
    }

    /// <summary>
    /// Checks if this data tree node's name matches the given string,
    /// regardless of capitalization.
    /// </summary>
    /// <param name="name">The name to check for.</param>
    /// <returns></returns>
    public bool NameIs(string name) {
      if(String.Compare(name, Name, StringComparison.InvariantCultureIgnoreCase) == 0)
        return true;
      else
        return false;
    }

    /// <summary>
    /// Adds another data tree's nodes, including attributes, to this data tree.
    /// </summary>
    /// <param name="child">The data tree you want to copy node data from.</param>
    public void Merge(DataTree child) {
      child.MergeInto(this);
    }

    /// <summary>
    /// Adds this data tree's nodes, including attributes, into another data tree.
    /// </summary>
    /// <param name="parent">The data tree you want to copy node data to.</param>
    public void MergeInto(DataTree parent) {
      if(parent != null) {
        foreach(DataTree child in this.Children) {
          parent.Children.Add(child);
        }
        foreach(DataTree attribute in this.Attributes) {
          parent.Attributes.Add(attribute);
        }
      }
    }

    /// <summary>
    /// A private function that does all Get's legwork. :)
    /// </summary>
    /// <param name="name"></param>
    /// <param name="parentHandle"></param>
    /// <returns>null if it cannot be found.</returns>
    private DataTree get(string name, DataTree node, params string[] parents) {
			// We can't find "nothing".
			if(name == null)
				return null;

      for(int i = 0; i < node.Children.Count; i++) {
				if(String.Compare(node.Children[i].Name, name, StringComparison.InvariantCultureIgnoreCase) == 0
					&& (parents == null || parents.Length == 0 ||parents != null && node.Children[i].IsChildOf(parents))) {
          return node.Children[i];
        }
        DataTree Return = get(name, node.Children[i], parents);
        if(Return != null) {
          return Return;
        }
      }

      // If the string can't be found, return NOTHING. :D
      return null;
    }

    /// <summary>
    /// Checks if this node has the given family tree.
    /// </summary>
    /// <param name="parents">The parent names of the value that you're looking for,
    /// listed in order of closest parent first.</param>
    public bool IsChildOf(params string[] parents) {
      DataTree node = this;

      for(int i = 0; i < parents.Length; i++) {
				// Ignore nulls.
				if(parents[i] == null)
					continue;

        node = node.Parent;
				if(node == null || String.Compare(node.Name, parents[i], StringComparison.InvariantCultureIgnoreCase) != 0) {
          return false;
        }
      }

      // If we made it this far, that means the node matches the family tree given.
      return true;
    }
    #endregion

    #region Loading
		/// <summary>
		/// The path and file name this data was (or will be) loaded from.
		/// </summary>
		public string PathAndFilename { get; private set; }

		/// <summary>
		/// A lock used when checking or firing the OnLoaded event.
		/// </summary>
		private object onLoadedLock = new Object();

		/// <summary>
		/// Whether or not the data tree has finished loading.
		/// </summary>
		public bool IsLoaded {
			get { return isLoaded; }
		}
		private bool isLoaded;

		/// <summary>
		/// Event fired by Grunt when this data tree is finished loading.
		/// </summary>
		public EventHandler OnLoaded {
			get {
				lock(onLoadedLock) {
					return onLoaded;
				}
			}
			set {
				lock(onLoadedLock) {
					if(IsLoaded) {
						value.Invoke(this, EventArgs.Empty);
					} else {
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;

    /// <summary>
    /// Queues a data tree to load from the given file.
    /// </summary>
    /// <param name="fileName">The name of the file to load, given with or without and extension.
    /// If no extension is given, several standard extensions will be searched for.</param>
    /// <returns>A data tree that will be created from the given file.</returns>
    public static DataTree LoadFrom(string fileName) {
      DataTree tree = new DataTree();
			tree.isLoaded = false;
      tree.Load(fileName);
      return tree;
    }

    /// <summary>
    /// Immediantly loads data from a file.
    /// </summary>
    /// <param name="file">The file name, given with or without an extension.
    /// If no extension is given, several standard extensions will be searched for.</param>
    public virtual void Load(string file) {
			PathAndFilename = file;
			LoadNow();
		}

		/// <summary>
		/// Queues this data tree to load from a file, using Grunt's work thread.
		/// Trying to access data from this object while it is loading or waiting to load
		/// will pause the calling thread until the data has finished loading.
		/// </summary>
		/// <param name="file">The file name, given with or without an extension.
		/// If no extension is given, several standard extensions will be searched for.</param>
		public virtual void LoadInBackground(string file) {
			isLoaded = false;
			PathAndFilename = file;
			Grunt.Enqueue(this);			
		}

		/// <summary>
		/// Loads this data tree from a file now in the current thread, using previously given parameters.
		/// Called by Grunt when it's time to load.
		/// </summary>
		public void LoadNow() {
			DebugLog.Write("Attempting to load data file " + PathAndFilename + "...");

			// Let's get the full path being used by the program.
			// If Grunt isn't up and running (because OFE was not fully booted), we'll have to find it ourselves.
			string fullPath = Grunt.FullPath;
			if(fullPath == null)
				fullPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

      // Attempt to load an OFEdata file.
			if(ofeAttemptLoad(Path.Combine(fullPath, PathAndFilename))) {
				isLoaded = true;
				DebugLog.Success("OFEdata file " + PathAndFilename + " was loaded successfully!");
        return;
      }

      // Attempt to load an XML file.
      if(xmlAttemptLoad(Path.Combine(fullPath, PathAndFilename))) {
				isLoaded = true;
				DebugLog.Success("XML data file " + PathAndFilename + " was loaded successfully!");				
        return;
      }

			// Failure.
			isLoaded = true;
			DebugLog.Failure("Could not load data file " + PathAndFilename + "!");
    }

    /// <summary>
    /// Attempts to search for and then load an OFEdata file.
    /// </summary>
    /// <param name="fileName">The file name, given with or without an extension.
    /// If no extension is given, several standard OFEdata extensions will be searched for.</param>
    /// <returns>True, if the attempt was successful. False otherwise. No exceptions will be thrown.</returns>
    private bool ofeAttemptLoad(string fileName) {
      // Check for a valid OFEdata file.
      if(!Path.HasExtension(fileName) && !File.Exists(fileName)) {
				foreach(string extension in OFEdata.Extensions) {
					if(File.Exists(fileName + extension)) {
						fileName = fileName + extension;
						break;
					}
				}
      } else if(!File.Exists(fileName)) {
        // No valid file found.
        return false;
      }

      // Attempt to parse the file.
      try {
        DataTree data = OFEdata.FromFile(fileName);
        this.name = data.Name;
        this.nodeValue = data.Value;
        this.children = data.Children;
        this.parent = data.Parent;
      } catch {
        // Failed for some reason.
        return false;
      }
      return true;
    }

    /// <summary>
    /// Loads data from an XML file.
    /// </summary>
    /// <param name="file">The file name, given with or without an extension.
    /// If no extension is given, several standard extensions will be searched for.</param>
    public void LoadXML(string file) {
      // Attempt to load an XML file.
      if(xmlAttemptLoad(file)) {
        return;
      }
    }

    /// <summary>
    /// Attempts to load the data from an XML file.
    /// </summary>
    /// <param name="file">The file name, without extension.</param>
    /// <returns>False, if there is not an xml file with the given name, or it could not be parsed.</returns>
    private bool xmlAttemptLoad(string file) {
      if(!Path.HasExtension(file)) {
        file = file + ".xml";
      }

      // Make sure the file exists.
      if(!System.IO.File.Exists(file)) {
        // The file doesn't exist, so we'll just return as a blank data node.
        return false;
      }

      // Open the file.
      StreamReader stream = new StreamReader(file);
      XDocument xml = XDocument.Load(stream);

      // Go through the file and find the root XML node. We'll set this to ourself,
      // then start adding its children as our children.
      foreach(XElement node in xml.Elements()) {
        this.Name = node.Name.LocalName;
        foreach(XElement child in node.Elements()) {
          this.Children.Add(xmlElement(child, this));
        }
      }

      // Close the file and return.
      stream.Close();
      return true;
    }

    /// <summary>
    /// Parses a single XML node into a DataTree node.
    /// </summary>
    /// <param name="xml">The XML node to parse.</param>
    /// <param name="parent">This node's parent node.</param>
    /// <returns>A Data node.</returns>
    private static DataTree xmlElement(XElement xml, DataTree parent) {
      DataTree node = new DataTree();

      // Get the name of this element.
      node.Name = xml.Name.LocalName;

      // Record this data node's parent.
      node.Parent = parent;

      // Record any attributes of this element.
      foreach(XAttribute xmlatt in xml.Attributes()) {
        DataTree nodeatt = new DataTree();
        nodeatt.Name = xmlatt.Name.LocalName;
        nodeatt.Value = xmlatt.Value;
        nodeatt.Parent = node;
        node.Attributes.Add(nodeatt);
      }

      // Record text data.
      node.Value = xml.Value;

      // Add children.
      foreach(XElement element in xml.Elements()) {
        node.Children.Add(xmlElement(element, node));
      }

      // Done. Return the node.
      return node;
    }
    #endregion

		#region Data Checking
		/// <summary>
		/// Whether or not this node's value is a number.
		/// </summary>
		public bool IsANumber {
			get {
				// If it parses to a float, it's a number.
				float f;
				if(float.TryParse(Value, NumberStyles.Number, CultureInfo.InvariantCulture, out f)) {
					return true;
				} else {
					return false;
				}
			}
		}
		#endregion

		#region Data Conversion
		/// <summary>
		/// The key of the first child of this node, converted to a boolean.
		/// </summary>
		/// <remarks>
		/// The following are valid values, regardless of case:
		/// - true/false
		/// - on/off
		/// - yes/no
		/// - active/inactive
		/// - 1/0
		/// 
		/// For any other value, false is returned.
		/// </remarks>
		public bool BoolValue {
			get {
				string value = Value.ToLowerInvariant();
				if(value == "true" || value == "on" || value == "yes" || value == "active" || IntValue == 1) {
					return true;
				} else {
					return false;
				}
			}
		}

		/// <summary>
		/// The name of this node, converted to a boolean.
		/// </summary>
		/// <remarks>
		/// The following are valid values, regardless of case:
		/// - true/false
		/// - on/off
		/// - yes/no
		/// - active/inactive
		/// - 1/0
		/// 
		/// For any other value, false is returned.
		/// </remarks>
		public bool BoolName {
			get {
				string value = Value.ToLowerInvariant();
				if(Name == "true" || Name == "on" || Name == "yes" || Name == "active" || IntName == 1) {
					return true;
				} else {
					return false;
				}
			}
		}

		/// <summary>
		/// The key of the first child of this node, converted to a 128-bit floating-point value.
		/// </summary>
		/// <remarks>
		/// If the value cannot be converted to a decimal, 0 is returned.
		/// </remarks>
		public decimal DecimalValue {
			get {
				decimal result;
				if(decimal.TryParse(Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The name of this node, converted to a 128-bit floating-point value.
		/// </summary>
		/// <remarks>
		/// If the name cannot be converted to a decimal, 0 is returned.
		/// </remarks>
		public decimal DecimalName {
			get {
				decimal result;
				if(decimal.TryParse(Name, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The key of the first child of this node, converted to a 64-bit floating-point value.
		/// </summary>
		/// <remarks>
		/// If the value cannot be converted to a double, 0 is returned.
		/// </remarks>
		public double DoubleValue {
			get {
				double result;
				if(double.TryParse(Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The name of this node, converted to a 64-bit floating-point value.
		/// </summary>
		/// <remarks>
		/// If the name cannot be converted to a double, 0 is returned.
		/// </remarks>
		public double DoubleName {
			get {
				double result;
				if(double.TryParse(Name, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The key of the first child of this node, converted to a 32-bit floating-point value.
		/// </summary>
		/// <remarks>
		/// If the value cannot be converted to a float, 0 is returned.
		/// </remarks>
		public float FloatValue {
			get {
				float result;
				if(float.TryParse(Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The name of this node, converted to a 32-bit floating-point value.
		/// </summary>
		/// <remarks>
		/// If the name cannot be converted to a float, 0 is returned.
		/// </remarks>
		public float FloatName {
			get {
				float result;
				if(float.TryParse(Name, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The key of the first child of this node, converted to a 32-bit integer.
		/// </summary>
		/// <remarks>
		/// If the value cannot be converted to an integer, 0 is returned.
		/// </remarks>
		public int IntValue {
			get {
				int result;
				if(int.TryParse(Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The name of this node, converted to a 32-bit integer.
		/// </summary>
		/// <remarks>
		/// If the name cannot be converted to an integer, 0 is returned.
		/// </remarks>
		public int IntName {
			get {
				int result;
				if(int.TryParse(Name, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The key of the first child of this node, converted to a 64-bit integer.
		/// </summary>
		/// <remarks>
		/// If the value cannot be converted to an integer, 0 is returned.
		/// </remarks>
		public long LongIntValue {
			get {
				long result;
				if(long.TryParse(Value, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// The name of this node, converted to a 64-bit integer.
		/// </summary>
		/// <remarks>
		/// If the name cannot be converted to an integer, 0 is returned.
		/// </remarks>
		public long LongIntName {
			get {
				long result;
				if(long.TryParse(Name, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					return result;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// Splits this data node's name into a list, with each item having been seperated by a comma.
		/// </summary>
		/// <returns>A string list object</returns>
		public List<string> NameToList() {
			List<string> list = Name.Split(',').ToList();
			for(int i = 0; i < list.Count; i++) {
				list[i] = list[i].Trim();
			}
			return list;
		}

    /// <summary>
    /// Splits this data node's value into a list, with each item having been seperated by a comma.
    /// </summary>
    /// <returns>A string list object</returns>
    public List<string> ValueToList() {
			List<string> list = Value.Split(',').ToList();
			for(int i = 0; i < list.Count; i++) {
				list[i] = list[i].Trim();
			}
			return list;
    }

		/// Takes a string of numbers in this node's name field
		/// and makes them into a list object of integers.
		/// </summary>
		public List<int> NameToIntList() {
			List<string> list = NameToList();
			List<int> intList = new List<int>();
			foreach(string item in list) {
				int result;
				if(int.TryParse(item, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					intList.Add(result);
				}
			}
			return intList;
		}

		/// <summary>
		/// Takes a string of numbers in this node's value field
		/// and makes them into a list object of integers.
		/// </summary>
		public List<int> ValueToIntList() {
			List<string> list = ValueToList();
			List<int> intList = new List<int>();
			foreach(string item in list) {
				int result;
				if(int.TryParse(item, NumberStyles.Number, CultureInfo.InvariantCulture, out result)) {
					intList.Add(result);
				}
			}
			return intList;
		}
    #endregion

    #region Threading
		/// <summary>
		/// A generic object used to lock the data tree for thread safety.
		/// </summary>
		private object padlock = new object();

		/// <summary>
		/// Checks if the data tree is still loading.
		/// If so, stall the current thread until it's done.
		/// </summary>
		private void stall() {
			// If we're already loaded, don't bother waiting.
			// And if we're in Grunt's work thread, we can just help ourself regardless. :)
			// (Otherwise you'd have to wait to load the file to load the file.)
			if(IsLoaded) { return; }
			
			// Otherwise, wait for Grunt to do his job.
			while(!IsLoaded && !Core.IsShuttingDown) {
				Thread.Yield();
				Thread.Sleep(10);
			}
		}
    #endregion
  }
}
