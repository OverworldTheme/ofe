﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Data {
  /// <summary>
  /// Data structure used to differentiate between supported localizations.
  /// </summary>
#if DEBUG && WINDOWS
  [System.Diagnostics.DebuggerDisplay("{Name}, {ISOCode}")]
#endif
  public struct Language {
		#region Supported Languages
		/// <summary>
		/// A list of all the default languages.
		/// </summary>
		/// <remarks>If you want to include a language that is not currently listed,
		/// you can do so by simply creating a new language object in your code.
		/// The language will be added to the list on creation.</remarks>
		public static List<Language> AllSupported = new List<Language>();

		/// <summary>
		/// The Arabic language group.
		/// </summary>
		public static readonly Language Arabic = new Language("Arabic", "العربية", "ara");

		/// <summary>
		/// The Cantonese language.
		/// </summary>
		/// <remarks>
		/// <para>A Chinese language,
		/// spoken as a native language in multiple areas of China,
		/// but most famously in Hong Kong and Macau.</para>
		/// <para>Not that the ISE 639-3 code "yue" is technically for the Yue language,
		/// of which Cantonese is considered a dialect.</para>
		/// </remarks>
		public static readonly Language Cantonese = new Language("Cantonese", "廣東話", "yue");

		/// <summary>
		/// The Chinese language group.
		/// </summary>
		/// <remarks>
		/// Not an actual language in and of itself,
		/// but rather a whole family of languages spoken throughout China and the surrounding areas.
		/// </remarks>
		public static readonly Language Chinese = new Language("Chinese", "汉语", "zho");

		/// <summary>
		/// The Dutch language.
		/// </summary>
		/// <remarks>
		/// Spoken in the Netherlands, Belgium, Suriname, and parts of the Caribbean.
		/// </remarks>
		public static readonly Language Dutch = new Language("Dutch", "Nederlands", "dut");

    /// <summary>
    /// The English language.
    /// </summary>
		/// <remarks>
		/// Currently the most widely used language in the world.
		/// Countries where English is the official or de facto language include
		/// the United Kingdom, the United States, Canada, Australia, New Zealand, and Ireland.
		/// </remarks>
    public static readonly Language English = new Language("English", "English", "eng");

    /// <summary>
    /// Esperanto, a constructed language created as an easy-to-learn second language.
		/// (Although just how easy it is to learn depends greatly on your native language.)
    /// </summary>
		/// <remakrs>
		/// Esperanto is not an official or de facto language in any recognized country of the world.
		/// </remakrs>
    public static readonly Language Esperanto = new Language("Esperanto", "Esperanto", "epo");

    /// <summary>
    /// The French language.
    /// </summary>
		/// <remarks>
		/// An official language of 29 countries.
		/// Spoken in France, Monaco, parts of Belgium and Switzerland, and parts of Canada.
		/// </remarks>
    public static readonly Language French = new Language("French", "Français", "fra");

    /// <summary>
    /// The German language.
    /// </summary>
    public static readonly Language German = new Language("German", "Deutsch", "deu");

		/// <summary>
		/// The (modern) Greek language.
		/// </summary>
		/// <remarks>
		/// Spoken throughout the southern Balkans.
		/// </remarks>
		public static readonly Language Greek = new Language("Greek", "ελληνικά", "ell");

		/// <summary>
		/// The (modern) Hebrew language.
		/// </summary>
		public static readonly Language Hebrew = new Language("Hebrew", "עברית", "heb");

    /// <summary>
    /// The Japanese language.
    /// </summary>
    public static readonly Language Japanese = new Language("Japanese", "日本語", "jpn");

		/// <summary>
		/// The Korean language.
		/// </summary>
		public static readonly Language Korean = new Language("Korean", "한국어", "kor");

		/// <summary>
		/// The Latin language.
		/// </summary>
		/// <remarks>
		/// An ancient language, now dead.
		/// Previously spoken in ancient Rome and later used primarily by scholars,
		/// now it is used mostly just for biological taxonomy, and movie and video game soundtracks.
		/// </remarks>
		public static readonly Language Latin = new Language("Latin", "lingua latina", "lat");

		/// <summary>
		/// The Mandarin Chinese language.
		/// </summary>
		/// <remarks>
		/// The most widely spoken language in modern China.
		/// It has more native speakers than any other language in the world!
		/// </remarks>
		public static readonly Language Mandarin = new Language("Mandarin", "官话", "cmn");

		/// <summary>
		/// The Mongolian language.
		/// </summary>
		public static readonly Language Mongolian = new Language("Mongolian", "монгол", "mon");

		/// <summary>
		/// The Norwegian language.
		/// </summary>
		public static readonly Language Norwegian = new Language("Norwegian", "Norsk", "nor");

		/// <summary>
		/// The Persian language.
		/// Also known as Farsi.
		/// </summary>
		/// <remarks>
		/// Persian is an Iranian language spoken in countries such as Afghanistan, Iran, Iraq, Kuwait, Tajikistan, and Uzbekistan.
		/// </remarks>
		public static readonly Language Persian = new Language("Persian", "فارسی", "fas");

		/// <summary>
		/// The Portuguese language.
		/// </summary>
		public static readonly Language Portuguese = new Language("Portuguese", "Português", "pt");
		
    /// <summary>
    /// The Spanish language.
    /// </summary>
    public static readonly Language Spanish = new Language("Spanish", "Español", "spa");

		/// <summary>
		/// The Swedish language.
		/// </summary>
		public static readonly Language Swedish = new Language("Swedish", "Svenska", "swe");
		#endregion

		#region Class Constructor
		/// <summary>
		/// Class constructor.
		/// </summary>
		/// <param name="name">The English name for this language.</param>
		/// <param name="nativeName">The native name for this language.</param>
		/// <param name="ISOCode">The two letter ISO 639-3 code for this language.</param>
		public Language(string name, string nativeName, string ISOCode) {
			this.Name = name;
			this.NativeName = nativeName;
			this.ISOCode = ISOCode;

			Language.AllSupported.Add(this);
		}
		#endregion

		/// <summary>
    /// The three letter ISO 639-3 code of the default system language.
    /// </summary>
    public static string SystemLanguage {
      get { return CultureInfo.CurrentCulture.ThreeLetterISOLanguageName; }
    }

    /// <summary>
    /// The currently set language.
    /// When OFE is first started up, this is set to whatever the system's currently set language is.
    /// </summary>
    public static Language Set = FindByISOCode(SystemLanguage);

    /// <summary>
    /// The default language, used as a fallback in case a localized text file cannot
    /// be found in the set language for some reason.
    /// By default, this is English, but you can change it if you want to.
    /// </summary>
    public static Language Default = Language.English;

		#region Identity
		/// <summary>
    /// The English name for this language.
    /// </summary>
    public string Name;

    /// <summary>
    /// The native name for this language. Included so that it's easy to find when
    /// doing a language selection option.
    /// </summary>
    public string NativeName;

    /// <summary>
    /// The three letter ISO 639-3 code for this language.
    /// </summary>
    public string ISOCode;
		#endregion		

		/// <summary>
    /// Searches for a language by its three-letter ISO 639-3 code.
    /// </summary>
    /// <param name="ISOCode">The ISO 639-3 code of the language you want.</param>
    /// <returns>If the language you are looking for cannot be found,
		/// or is not currently listed in the code,
		/// English will be returned instead.</returns>
    public static Language FindByISOCode(string ISOCode) {
      foreach(Language lang in AllSupported) {
        if(string.Compare(lang.ISOCode, ISOCode, StringComparison.InvariantCultureIgnoreCase) == 0) {
          return lang;
        }
      }

      // The given language could not be found. Use English instead as a backup.
      return English;
    }
  }
}
