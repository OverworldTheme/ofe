﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

#if XBOX
using Microsoft.Xna.Framework.Storage;
#endif

using VisionRiders.OFE.Graphics;

namespace VisionRiders.OFE.Data {
  /// <summary>
  /// A handy object to store saved game data. You can create a new class in your program that
  /// inherits this one and will be able to save all the data in itself to a file. The way the
  /// data is stored is customized to the platform OFE is running on, so you don't have to worry
  /// about dealing with that yourself.
  /// </summary>
  public class SaveData : ILoadable {
		#region Class Constructor
		public SaveData() {
			onLoadedLock = new object();
			Destination = Location.User;
		}
		#endregion

		#region Path and File Properties
		/// <summary>
		/// A general location where data can be saved to.
		/// </summary>
		public enum Location {
			/// <summary>
			/// The user's documents folder.
			/// </summary>
			User,

			/// <summary>
			/// The program's install folder, where it is being run from.
			/// </summary>
			Local,

			/// <summary>
			/// An external cloud data server.
			/// </summary>
			Cloud
		}

		/// <summary>
		/// Where the data is to be saved to.
		/// </summary>
		[OFEdataIgnore]
		public Location Destination { get; set; }

		/// <summary>
		/// The name to give this program's save data folder in the game's local directory structure.
		/// If null or empty, data will be saved to the root program folder.
		/// </summary>
		public static string LocalFolderName { get; set; }

		/// <summary>
    /// The name to give this program's save data folder in the user's system documents.
		/// This value should be set to the game's name, so that the user can easily locate it.
    /// </summary>
		public static string UserFolderName { get; set; }

    /// <summary>
    /// The file extension to give this save data.
    /// </summary>
		/// <remarks>
		/// <para>By default, the extension is ".save".</para>
		/// <para>If you want a different extension for your data,
		/// it is recommended that you set it in the inherited classes's constructor.</para>
		/// </remarks>
		[OFEdataIgnore]
		public string Extension = ".save";

		/// <summary>
		/// The file extension that is added to backup copies of save data.
		/// This is added to the normal file name extension (for example, ".save" will become ".save.BACKUP").
		/// </summary>
		[OFEdataIgnore]
		public const string BackupExtension = ".BACKUP";

		/// <summary>
		/// The file extenstion that is added to temporary copies of data created during the save process.
		/// This is added to the normal file name extension (for example, ".save" will become ".save.TEMP").
		/// </summary>
		[OFEdataIgnore]
		public const string TempExtension = ".TEMP";
		#endregion

		#region Parsing and Preperation
		/// <summary>
    /// Gets all the data for this save. It will be called before the data is saved.
    /// </summary>
    protected virtual void getData() {
    }

    /// <summary>
    /// Puts all the save data to use. Called after the data is loaded.
    /// </summary>
		/// <remarks>
		/// Object data CAN be null if there is no data, or no valid data in a loaded file.
		/// Remember to check if the object is an object of the expected class before attempting to work with it.
		/// </remarks>
    protected virtual void parseData(object data) {
    }
		#endregion

		#region Loading
		/// <summary>
		/// The relative location and file name where this save data was (or will be) loaded from, sans the extension.
		/// </summary>
		[OFEdataIgnore]
		public string PathAndFilename { get; set; }

		/// <summary>
		/// Whether or not the save data has finished loading.
		/// </summary>
		[OFEdataIgnore]
		public bool IsLoaded {
			get { return isLoaded; }
		}
		private bool isLoaded = false;

		/// <summary>
		/// Whether or not there was a problem with the file.
		/// </summary>
		[OFEdataIgnore]
		public bool FileWasBad {
			get { return fileWasBad; }
		}
		private bool fileWasBad = false;

		/// <summary>
    /// Queues this data to load from the file of the given name, located in the user's documents folder.
    /// </summary>
    /// <param name="filename">The name of the file. Do not include the extension.</param>
		public void LoadFromUser(string filename) {		
			// We are no longer considered loaded.
			isLoaded = false;
			fileWasBad = false;

      // Save the path and filename.
			Destination = Location.User;
			PathAndFilename = filename;// getFullPath(filename);

			// Tell Grunt we need to load
			Grunt.Enqueue(this);
		}

		/// <summary>
		/// Queues this data to load from the file of the given name, located in the user's documents folder.
		/// </summary>
		/// <param name="filename">The name of the file. Do not include the extension.</param>
		public void LoadFromLocal(string filename)
		{
			// We are no longer considered loaded.
			isLoaded = false;
			fileWasBad = false;

			// Save the path and filename.
			Destination = Location.Local;
			PathAndFilename = filename; //getFullPath(filename);

			// Tell Grunt we need to load
			Grunt.Enqueue(this);
		}

		/// <summary>
		/// Has the save data load now, in the current thread, using already requested parameters.
		/// Should generally only be called by Grunt.
		/// Attempting to manually call this method is a sure source of "gotchas".
		/// </summary>
		public void LoadNow() {
			// The actual name of the file we'll be loading today.
			// This will include the file extension.
			string fileToLoad = getFullPath(PathAndFilename) + Extension;

			// Tell the log what we're trying to do.
			DebugLog.Write("Loading save data from " + fileToLoad + "...");

			// Check if there's temp data.
			// If so, that means the save process was interupted and we need to load that instead of the normal save file,
			// which is likely incomplete or corrupt.
			if(File.Exists(fileToLoad + TempExtension)) {
				DebugLog.Note("The file " + fileToLoad + " will be loaded from the temporary backup file.");
				fileToLoad = fileToLoad + TempExtension;
			}

			// Make sure the file exists.
			if(!File.Exists(fileToLoad)) {
				DebugLog.Failure("The file " + fileToLoad + " could not be found!");

				// Check if there's a backup file to load from instead.
				if(File.Exists(fileToLoad + BackupExtension)) {
					DebugLog.Note("Attempting to load from backup file.");
					fileToLoad = fileToLoad + BackupExtension;
				} else {
					// Failure!
					DebugLog.Failure("Could not find any backup for " + fileToLoad + "!");
					fileWasBad = true;
					if(OnFailureToLoad != null) {
						OnFailureToLoad.Invoke(this, new ErrorEventArgs(new FileNotFoundException("The file " + fileToLoad + " could not be found.")));
					} else {
						new FileNotFoundException("The file " + fileToLoad + " could not be found.");
					}
					isLoaded = true;
					return;
				}
      }

      // Open a stream.
			DebugLog.Note("Reading save data from file...");
			FileStream stream = File.Open(fileToLoad, FileMode.Open, FileAccess.Read);

      // Read from the stream.
      OFEdataSerializer serializer = new OFEdataSerializer(this.GetType());
			object serializedObject = serializer.Deserialize(stream);
			if(serializedObject == null) {
				DebugLog.Note("It appears there was no valid data in file " + PathAndFilename);
			}
			parseData(serializedObject);

      // Close the stream.
      stream.Close();

			// Success!
			isLoaded = true;
			DebugLog.Success("Data loaded successfully!");
    }
		#endregion

		#region Saving
		/// <summary>
		/// Saves this data to the filename it was last loaded from.
		/// The location will be chosen based on the currently set destination.
		/// (Usually, whichever it was last saved to or loaded from, unless you manually changed it.)
		/// </summary>
		/// <exception cref="InvalidOperationException">
		/// Thrown when ordering save data to save to a file when no path or file name has been supplied.
		/// Use the SaveTo methods instead for save data that has not previously been loaded.
		/// </exception>
		public void Save() {
			if(string.IsNullOrEmpty(PathAndFilename)) {
				throw new InvalidOperationException("No path or file name has been supplied for this save data yet. Use the SaveTo methods for save data that was not previously loaded.");
			} else {
				switch (Destination)
				{
					case Location.User:
						SaveToUser(Path.GetFileNameWithoutExtension(PathAndFilename));
						break;
					case Location.Local:
						SaveToLocal(Path.GetFileNameWithoutExtension(PathAndFilename));
						break;
					default:
						throw new NotImplementedException("The set data location is not currently supported.");
				}
			}
		}

		/// <summary>
    /// Saves this data to a file of the given name, in the user's documents folder.
    /// </summary>
    /// <param name="fileName">The name of the file to save.
		/// If the file already exists, it will be overwritten.
    /// Do not include an extension.</param>
		public void SaveToUser(string filename)
		{
			Destination = Location.User;
			perfomSaveOperation(filename);
		}

		/// <summary>
		/// Saves this data to a file of the given name, in the user's documents folder.
		/// </summary>
		/// <param name="fileName">The name of the file to save.
		/// If the file already exists, it will be overwritten.
		/// Do not include an extension.</param>
		public void SaveToLocal(string filename)
		{
			Destination = Location.Local;
			perfomSaveOperation(filename);
		}

		/// <summary>
		/// Does the actual busy work of saving the data.
		/// </summary>
		/// <param name="filename">The name of the file to save.
		/// If it already exists, it will be overwritten.
		/// Do not include an extension.</param>
		private void perfomSaveOperation(string filename) {
			// Pack all the data in.
			this.getData();

			// Get the full path directory and file name.
			string fileToSave = getFullPath(filename) + Extension;

			// Tell the debug log what we're doing.
			DebugLog.Write("Saving data to " + fileToSave + "...");

			// First, back up the existing data, if any.
			// Unless the .TEMP file exists, which means that there was a problem last save.
			// You don't overwrite the .TEMP with the bad data! XD
			if(File.Exists(fileToSave) && !File.Exists(fileToSave + TempExtension)) {
				DebugLog.Note("Creating backups...");
				File.Copy(fileToSave, fileToSave + TempExtension, true);
				File.Copy(fileToSave, fileToSave + BackupExtension, true);				
			}

			// Open a steam.
			DebugLog.Note("Writing file...");
			using (FileStream stream = File.Open(fileToSave, FileMode.Create))
			{
				// Convert data to OFEdata and write to the stream.
				OFEdataSerializer serializer = new OFEdataSerializer(this.GetType());
				serializer.Serialize(stream, this);
			}

			// Delete the temporary data.
			if(File.Exists(fileToSave + TempExtension)) {
				File.Delete(fileToSave + TempExtension);
			}

			// Success!
			DebugLog.Success("Data saved successfully!");
		}
		#endregion

		#region Other File Operations
		/// <summary>
		/// Backup the given save data file, if it exists.
		/// </summary>
		/// <param name="data">The data to backup.</param>
		public static void Backup(SaveData data) {
			data.Backup();
		}

		/// <summary>
		/// Orders this save data to create a backup copy of itself, if possible.
		/// If the data does not currently exist at its internally-reported location
		/// (for example, if you manually changed the set filename or data location after last performing a save or load operation),
		/// then nothing will happen.
		/// </summary>
		public void Backup()
		{
			// Get the full path directory and file name.
			string filename = getFullPath(PathAndFilename) + Extension;

			// Back up the file.
			if (File.Exists(filename))
			{
				DebugLog.Note("Creating backup...");
				File.Copy(filename, filename + BackupExtension, true);
			}
			else
			{
				DebugLog.Failure("Requested backup does not exist. No operation was made.");
			}
		}
		#endregion

		#region Events
		/// <summary>
		/// Called if, for whatever reason, the save data cannot be loaded.
		/// If there are no listeners to this event, an exception will be thrown instead.
		/// </summary>
		[OFEdataIgnore]
		public EventHandler<ErrorEventArgs> OnFailureToLoad { get; set; }

		/// <summary>
		/// A lock used when checking or firing the OnLoaded event.
		/// </summary>		
		private object onLoadedLock;

		/// <summary>
		/// Event fired by Grunt when the save data is finished loading.
		/// </summary>
		[OFEdataIgnore]
		public EventHandler OnLoaded
		{
			get
			{
				lock (onLoadedLock)
				{
					return onLoaded;
				}
			}
			set
			{
				lock (onLoadedLock)
				{
					if (IsLoaded)
					{
						value.Invoke(this, EventArgs.Empty);
					}
					else
					{
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;
		#endregion

		/// <summary>
		/// Constructs the full path and file name for save data.
		/// </summary>
		/// <param name="fileName">The base file name (for example, "Save19" might become "C:\\My Documents\\My Games\\This Game\\Save19").</param>
		/// <returns></returns>
		/// <exception cref="InvalidOperationException">
		/// Thrown if data is an attempt is made to save data to the user's documents folder without first setting the UserFolderName string.
		/// Note that in this case, the string cannot be null or empty.
		/// </exception>
		private string getFullPath(string fileName) {
			// Make sure the program folder has been set.
			if(Destination == Location.User && string.IsNullOrEmpty(UserFolderName)) {
				throw new InvalidOperationException("String property UserFolderName must be set before performing SaveData operations to the user's documents folder. The string cannot be null or empty.");
			}
			
      // Set the path that we'll be saving to.
			string folder = string.Empty;
			switch (Destination)
			{
				case Location.User:					
					folder = Path.Combine(Core.UserFolder, UserFolderName);
					break;
				case Location.Local:
					folder = Path.Combine(Core.Directory, LocalFolderName);
					break;
				default:
					throw new NotImplementedException("The set data location is not currently supported.");
			}

      // If the path doesn't exist, create it.
      if(!Directory.Exists(folder)) {
        Directory.CreateDirectory(folder);
      }

      // Combine the folder and file name.
      fileName = Path.Combine(folder, fileName);

      return fileName;
    }
  }
}
