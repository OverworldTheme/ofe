﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace VisionRiders.OFE.Data {
  /// <summary>
  /// A class used to open and parse the data of an OFEdata file.
  /// </summary>
  /// <version>1.0</version>
  /// <guid>81ED1DB8-53A1-4103-8CBA-B82EFF59E8BF</guid>
  /// <guid-history>81ED1DB8-53A1-4103-8CBA-B82EFF59E8BF</guid-history>
  /// <remarks>
  /// <para>Programmed by Dale M.A. Johnson.</para>
  /// <para>Permission granted for free use, commercial or otherwise, with or without modification.
  /// This code is provided as-is; the author is not liable for malfunction, error, or stupidity.</para>
  /// <para>The code is designed for use with OFE's data tree object, but you should be able to change
  /// it around pretty easily to something else for your own use as needed. The comments should guide
  /// you well on your way.</para>
  /// </remarks>
  public class OFEdata {
    #region Enumerators
    /// <summary>
    /// An enumerator for the two types of fields in OFEdata:
    /// keys and values.
    /// </summary>
    private enum fieldType {
      /// <summary>
      /// The key (name) field.
      /// </summary>
      Key,

      /// <summary>
      /// The value field associated with a key.
      /// </summary>
      Value
    }
    #endregion

		#region Constants
		/// <summary>
		/// The standard extensions for an OFEdata file.
		/// </summary>
		public static readonly List<string> Extensions = new List<string> {
			".ofedata",
			".ofe",
			".data",
			".txt"
		};
		#endregion

    #region Properties
    /// <summary>
    /// A stream reader of the data file.
    /// </summary>
    private StreamReader reader;

    /// <summary>
    /// The line last taken from the file, and that is currently being parsed.
    /// </summary>
    private string line;

    /// <summary>
    /// The parser's current position on the line.
    /// </summary>
    private int pos;

    /// <summary>
    /// Whether or not the parser has hit the end of the file.
    /// </summary>
    private bool endOfFile;
    #endregion

    #region Loading
    /// <summary>
    /// Parses OFEdata from the given file.
    /// </summary>
    /// <exception cref="FileNotFoundException">Thrown when the given file does not exist.</exception>
    /// <returns>The parsed OFEdata file in an OFE data tree.</returns>
    public static DataTree FromFile(string fileName) {
			DebugLog.Write("Attempting to load OFEdata file \"" + fileName + "\".");

      // Make sure the file exists.
			if(!File.Exists(fileName)) {
				DebugLog.Failure("The requested OFEdata file \"" + fileName + "\" could not be found.");
#if !XBOX
        throw new FileNotFoundException("The requested file cannot be parsed because it could not be found.", fileName);
#else
        throw new Exception("The requested file, \"" + fileName + "\", cannot be parsed because it could not be found.");
#endif
      }

      // Open the file.
			FileStream stream = new FileStream(fileName, FileMode.Open);
			DataTree data = FromStream(stream, Path.GetFullPath(fileName));

			// Report success.
			DebugLog.Success("Successfully opened the OFEdata file \"" + fileName + "\".");

			// Return our findings.
			return data;
		}

		/// <summary>
		/// Parses OFEdata from the given stream.
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <param name="pathAndFilename">The path and filename or the data, or null.</param>
		/// <returns>The parsed OFEdata in a data tree.</returns>
		public static DataTree FromStream(Stream stream, string pathAndFilename) {
			OFEdata reader = new OFEdata();
			return reader.ParseStream(stream, pathAndFilename);
		}
		#endregion

		#region Parsing
		/// <summary>
		/// Parses the data in the given stream.
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <param name="pathAndFilename">The path and filename or the data, or null.</param>
		/// <returns>The parsed OFEdata in a data tree.</returns>
		public DataTree ParseStream(Stream stream, string pathAndFilename) {
			reader = new StreamReader(stream);

			// Reset the end-of-file state
			endOfFile = false;

			// Create the root node.
			// As per specifications, it is supposed to be given the name of the file, sans directory and extension.
			DataTree root = new DataTree();
			if(pathAndFilename != null) {
				root.Name = Path.GetFileNameWithoutExtension(pathAndFilename);
				directory = Path.GetDirectoryName(pathAndFilename);
			}

			// We'll be using a working node as we move along to keep track of where we are in the tree we're
			// creating so we know what to attach to what as a child.
			DataTree parent = root;
			DataTree current = null;

			// We need a way to record any !Define instructions we're given.
			definitions = new List<string[]>();

			// Let's go ahead and create some temp data storage objects.
			string key;
			string value;

			// Read the first line.
			advance();

			// Begin parsing line-by-line until we hit the end of the file.
			while(!endOfFile) {
				// Clear the values.
				key = string.Empty;
				value = string.Empty;

				// Get the key value.
				key = readValue(fieldType.Key);

				// If a name was given, or we hit a seperator go ahead and create the node.
				if(key != string.Empty || pos < line.Length && line[pos] == ':' || pos < line.Length && line[pos] == '{') {
					current = new DataTree(key);
					parent.Children.Add(current);
					current.Parent = parent;
				}

				// Check if we already hit the end of the line.
				if(pos >= line.Length) {
					// Skip to the next line.
					advance();
					continue;
				}

				// Check if we hit the seperator.
				if(line[pos] == ':') {
					// Advance one.
					pos++;

					// Read the value.
					value = readValue(fieldType.Value);

					// Add the value.
					current.Value = value;

					// Check if it's an instruction for us.
					if(current.Name.StartsWith("!"))
						readInstruction(current, root);

					// If we hit the end of the line, move on.
					if(pos >= line.Length) {
						// Skip to the next line.
						advance();
						continue;
					}
				}

				// Check if we hit a soft break that divides multiple entries on a single line.
				if(line[pos] == '|') {
					// Advance one and continue.
					pos++;
					continue;
				}

				// Check if we hit a new data set.
				if(line[pos] == '{') {
					// If we've hit the end of a line, advance.
					// Otherwise, just skip past the bracket.
					if(pos + 1 < line.Length) {
						pos++;
					} else {
						advance();
					}

					// Make this node the parent of those to come.
					parent = current;

					// Carry on.
					continue;
				}

				// Check if we finished a data set.
				if(line[pos] == '}') {
					// If we've hit the end of a line, advance.
					// Otherwise, just skip past the bracket.
					if(pos + 1 < line.Length) {
						pos++;
					} else {
						advance();
					}

					// Go back up the tree.
					if(parent.Parent != null) {
						parent = parent.Parent;
					}

					// Carry on.
					continue;
				}

				// Check if we hit a comment.
				if(line[pos] == '#') {
					// Skip to the next line.
					advance();
					continue;
				}
			}

			// Close the file now that we're done with it.
			reader.Close();

			// Strip the OFEdata Version information, as needed.
			for(int i = 0; i < root.Children.Count; i++) {
				if(root.Children[i].NameIs("(OFEdata Version") || root.Children[i].NameIs("OFEdata Version")) {
					root.Children.Remove(root.Children[i]);
					break;
				}
			}

			// Return the root.
			return root;
		}
    #endregion

		#region Reading
		/// <summary>
		/// Read a single line of OFEdata from a file stream.
		/// </summary>
		/// <param name="stream">The stream to read data from.</param>
		public void ReadNode(FileStream stream) {
		}

		/// <summary>
		/// A boolean flag denoting whether we've activated literal mode.
		/// </summary>
		private bool literal;

		/// <summary>
		/// The current directory.
		/// </summary>
		private string directory;
		#endregion

		#region In-line Value Replacement
		/// <summary>
		/// A list of replacements to make, as defined by parser instructions.
		/// ONLY ARRAYS WITH A LENGTH OF TWO MUST BE ADDED TO AVOID ERRORS!
		/// </summary>
		private List<string[]> definitions;

		/// <summary>
		/// Checks this node's name and value for anything that needs to be replaced,
		/// based on previous !define instructions given to the parser.
		/// </summary>
		/// <param name="node">The node to replace strings in.</param>
		private void redefine(DataTree node) {
			// No replacements? Don't bother.
			if(definitions.Count == 0)
				return;

			node.Name = redefine(node.Name);
			node.Value = redefine(node.Value);

			// Do this for all children, too.
			for(int i = 0; i < node.Children.Count; i++) {
				redefine(node.Children[i]);
			}
		}

		/// <summary>
		/// Checks this string for anything that needs to be replaced,
		/// based on previous !define instructions given to the parser.
		/// </summary>
		/// <param name="field"></param>
		private string redefine(string field) {
			// No replacements? Don't bother.
			if(definitions.Count == 0)
				return field;

			// Replace ALL the things!
			for(int i = 0; i < definitions.Count; i++) {
				field = field.Replace(definitions[i][0], definitions[i][1]);
			}

			return field;
		}
		#endregion

		#region Value Reading
		/// <summary>
    /// Reads and returns the next value from the stream.
    /// </summary>
    /// <returns></returns>
    private string readValue(fieldType fieldType) {
      // A string for the return value.
      string value = string.Empty;

			// Keep track of how many lines we cover.
			// This is important, because the name and value of a node
			// are allowed to be on separate lines, so long as the value
			// directly follows the name.
			int linesRead = 0;

			literal = false;

      // We go character by character and add it to the value
      // until we hit an important character.
      while(true) {
        // If we're in literal mode, and we hit the end of the line,
        // advance to the next line. We continue this until we hit the
        // next ">", signaling the end of the literal.
				//
				// Likewise, in non-literal mode if no value has been encountered
				// yet, we still advance to the next line because the name and
				// value of a node can be on different lines. Note that in non-
				// literal mode we only do this ONCE. There cannot be an empty
				// line between the name and value of a node.
				if(pos >= line.Length && literal
					|| pos >= line.Length && !literal && fieldType == fieldType.Value && linesRead < 1 && value.Trim() == string.Empty) {
          // Add a space to the end of the value and advance.
					if(literal) {
						value += ' ';
					}
          advance();

					// Note that we advanced a line.
					linesRead++;

          // If we hit the end of the file, then we're done here.
          // This shouldn't happen in the middle of reading a value; when it does
          // that generally means someone forgot a ">" somewhere.
          // Regardless, we must accept the data and not throw an exception.
          if(endOfFile && line == string.Empty) {
            break;
          } else {
						continue;
					}
				}
				
				// If we hit the end of a line under any other circumstances,
				// it means we're done here.
				else if(pos >= line.Length) {
					break;
				}

        // Check if we hit a system character.
        // These end the value.
        if(!literal) {
          if(line[pos] == '#'
            || line[pos] == ':' && fieldType == fieldType.Key
            || line[pos] == '{'
            || line[pos] == '}'
            || line[pos] == '|' && fieldType == fieldType.Value) {
            break;
          }
        }

        // Check if we hit a pointy bracket (a less-than/greater-than sign: "<" and ">").
        // These signal literal sections that may span multiple lines.
        if(line[pos] == '<' && !literal || line[pos] == '>' && literal) {
          // If we've hit the beginning of a literal, and there's nothing in the value yet,
          // we engage literal mode, and then skip this character (so that the brackets themselves
          // aren't actually added to the string.
          if(value.Trim() == string.Empty && line[pos] == '<') {
            // Advance one character.
            pos++;

            // Enable literal mode.
            literal = true;
          } else {
            // If we've already got a value, this bracket indicates a new value.
            // Return the value we already have, but don't advance past the bracket so that
            // the next value reading will know to engage literal mode.
            if(line[pos] == '<') {
              break;
            }

            // Check if we're in literal mode and we hit a ">".
            // If so, that's the end of this value. We're done.
            if(line[pos] == '>') {
              // Advance past the ">" before breaking, because we already read it.
              pos++;
              break;
            }
          }
        }

        // Check if we hit a backslash.
        // This means that the character immediatly after should be added to the string, even if it is a system character.
        // This allows the use of characters that are otherwise reserved.
        else if(line[pos] == '\\') {
          // Advance to the character.
          pos++;

          // Make sure we haven't gone past the line.
          if(pos < line.Length) {
            // Check for special "escape sequence" characters:
            // \n = New Line
            // \s = Space
            // \t = Tab
            if(line[pos] == 'n' || line[pos] == 'N') {
              value += Environment.NewLine;
            } else if(line[pos] == 's' || line[pos] == 'S') {
              value += ' ';
            } else if(line[pos] == 't' || line[pos] == 'T') {
              value += '\t';
            } else {
              value += line[pos];
            }
          }

          // Advance again, past the character.
          pos++;
        }

        // Don't bother adding whitespace to the beginning.
        // Make sure we have some content in the value before adding any whitespace.        
        else {          
          if(literal || !char.IsWhiteSpace(line[pos]) || value != string.Empty) {
            // We're good. Add the character.
            value += line[pos];
          }
          pos++;
        }
      }

      // Trim the whitespace from the end (and the beginning, although there shouldn't be any because we
      // already skipped it all).
      if(!literal) {
        value = value.Trim();
      }

			// Process any replacements.
			value = redefine(value);

      // Done! Return the value.
      return value;
    }
    #endregion

    #region Additional Parsing Methods
    /// <summary>
    /// Advances the parser to the next line.
    /// </summary>
    private void advance() {
      // Reset the position.
      pos = 0;

      // Get the next line from the stream.
      if(!reader.EndOfStream) {
        line = reader.ReadLine();
      } else {
        line = string.Empty;
        endOfFile = true;
      }

      // If the line is completely empty, skip it.
      while(line.Length <= 0 && !reader.EndOfStream) {
        line = reader.ReadLine();
      }

      // Trim the white space from the beginning and end of the line.
      line = line.Trim();
    }

		/// <summary>
		/// Figure out what to do with instructions given to us in the data.
		/// </summary>
		/// <param name="node">The instruction node.</param>
		/// <param name="root">The root node.</param>
		private void readInstruction(DataTree node, DataTree root) {
			switch(node.NameToLower) {
				case "!ofedata version":
					// Version number.
					// TODO: Something?
					break;
				case "!define":
					// Define a replacement value.
					List<string> replace = node.ValueToList();
					if(replace.Count >= 2) {
						definitions.Add(new string[] { replace[0], replace[1] });
					} else {
						DebugLog.Failure("OFEdata file " + root.NameToLower + " gave a !Define command, but nothing to define.");
					}
					break;
				case "!include":
					// Include another file at this point in the data.
					if(!string.IsNullOrWhiteSpace(node.Value)) {
						DataTree newTree = new DataTree();
						string path = string.Empty;
						if(directory != null)
							path = directory;
						newTree.Load(path + "//" + node.Value);
						if(node.Parent != null)
							node.Parent.Children.Add(newTree);
					}
					break;
			}
		}
    #endregion
  }
}
