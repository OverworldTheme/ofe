﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace VisionRiders.OFE.Data {
  /// <summary>
  /// A data container that is used to load localized text strings from a file.
  /// </summary>
  /// <remarks>
  /// The Localization data object is based on the normal OFE data object and contains most
  /// of the same basic functions and features. The main difference is that it will pull files
  /// from different folders based on what the current set language is, so that (if availible)
  /// the game text will automatically be displayed in the player's language without any extra
  /// work for the programmer.
  /// </remarks>
  public class Localization : DataTree {
    /// <summary>
    /// The relative location of the folder containing sub-folders for each language.
		/// By default, this is an empty string.
    /// </summary>
    public static string BaseFolder = string.Empty;

		private Localization() { }

    public Localization(string file) {
			findAndLoadFile(file);
    }

		/// <summary>
		/// Creates a new group of localized strings from a file.
		/// </summary>
		/// <param name="file">The file to load from.</param>
		/// <returns>A Localization object containing localized strings.</returns>
		public static Localization FromFile(string file) {
			return new Localization(file);
		}

		/// <summary>
		/// Creates a new group of localized strings from a file.
		/// </summary>
		/// <param name="file">The file to load from.</param>
		/// <returns>A Localization object containing localized strings.
		/// Note that trying to access its data before it has finished loading will pause the calling
		/// thread until loading is complete.</returns>
		public static Localization FromFileInBackground(string file) {
			Localization loc = new Localization();
			loc.findAndLoadFile(file);
			return loc;
		}

		/// <summary>
		/// Finds the requested filename with any extension.
		/// If it does not exist in the current language,
		/// the default language will be attempted.
		/// </summary>
		/// <param name="file"></param>
		private void findAndLoadFile(string file)
		{
			// Cobble together the possible set language path to the file,
			// and the possible default language path to the file.
			string fileLocal = Path.Combine(BaseFolder, Language.Set.ISOCode.ToLowerInvariant(), file);
			string fileDefault = Path.Combine(BaseFolder, Language.Default.ISOCode.ToLowerInvariant(), file);

			// Let's get the full path being used by the program.
			// If Grunt isn't up and running (because OFE was not fully booted), we'll have to find it ourselves.
			string fullPath = Grunt.FullPath;
			if (fullPath == null)
				fullPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

			// Attempt to load the given localized text file in the set language.
			if (!Path.HasExtension(fileLocal) && !File.Exists(Path.Combine(fullPath, fileLocal)))
			{
				foreach (string extension in OFEdata.Extensions)
				{
					if (File.Exists(Path.Combine(fullPath, fileLocal) + extension))
					{
						// Queue to load.
						LoadInBackground(fileLocal);
						return;
					}
				}
			}

			// If the file didn't exist, try loading it in the default language as a fallback.
			LoadInBackground(fileDefault);
		}
  }
}
