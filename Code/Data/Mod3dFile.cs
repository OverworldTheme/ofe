﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE3D;

namespace VisionRiders.OFE.Data
{
	/// <summary>
	/// A class used for loading model and animation data from a 3dMod file.
	/// </summary>
	/// <remarks>
	/// As you might have guessed,
	/// the class is called Mod3dFile because names cannot begin with a number.
	/// </remarks>
	public class Mod3dFile : IDisposable
	{
		#region Constructor
		/// <summary>
		/// Creates a 3dMod file reader.
		/// </summary>
		/// <param name="filename">
		/// The relative path and file name of the 3dMod file.
		/// If no file extension is included,
		/// one will be added if a file with that name exists.
		/// </param>
		/// <exception cref="FileNotFoundException">No file with a standard 3dMod file extension could be found with the given filename.</exception>
		/// <exception cref="IOException">The file exists, but there was a critical error trying to read it.</exception>
		public Mod3dFile(string filename)
		{
			filename = Path.Combine(Grunt.FullPath, filename);

			// We'll fill this in once we find the file we're looking for.
			pathAndFilename = null;

			// Try and locate the file.
			if (File.Exists(filename)) pathAndFilename = filename;
			else
			{
				foreach (string ext in Extensions)
				{
					if (File.Exists(filename + ext)) pathAndFilename = filename + ext;
				}
			}

			// Does it exist?
			if (pathAndFilename == null) throw new FileNotFoundException("Could not find any 3dMod file with the given filename. Does it have a non-standard extension?", filename);

			// Let's load it on up.
			try { stream = new FileStream(pathAndFilename, FileMode.Open); }
			catch (Exception e) { throw new IOException("There was a problem trying to open the given file.", e); }
			file = new BinaryReader(stream);
		}
		#endregion

		#region Disposal
		/// <summary>
		/// Disposes of this 3dMod file reader's resources,
		/// if any are still around.
		/// </summary>
		public void Dispose()
		{
			if (file != null) file.Dispose();
			if (stream != null) stream.Dispose();
		}

		~Mod3dFile()
		{
			Dispose();
		}
		#endregion

		#region Working Objects
		/// <summary>
		/// The file stream used to access the file.
		/// </summary>
		private FileStream stream;

		/// <summary>
		/// A binary reader to access data in the file.
		/// </summary>
		private BinaryReader file;

		/// <summary>
		/// The path and filename of the file being read.
		/// </summary>
		private string pathAndFilename;

		/// <summary>
		/// Major version number of the file format.
		/// </summary>
		private int majorVersion;

		/// <summary>
		/// Minor version number of the file format.
		/// </summary>
		private int minorVersion;

		/// <summary>
		/// The (reported) size of the file, in bytes,
		/// not counting the first 12 bytes of the header.
		/// </summary>
		private long fileSize;

		/// <summary>
		/// The model currently being loaded.
		/// </summary>
		private ModelSource model;

		/// <summary>
		/// The materials that have been loaded thus far.
		/// </summary>
		private List<Material> materials;
		#endregion

		#region File Extensions
		/// <summary>
		/// Valid file extensions for 3dMod files.
		/// </summary>
		public static readonly string[] Extensions = { ".3dmod", ".3danim" };
		#endregion

		#region Loading
		/// <summary>
		/// Immediantly loads model and animation data from the file,
		/// in the current thread.
		/// </summary>
		/// <returns>An empty ModelSource object if no valid data could be found.</returns>
		/// <exception cref="InvalidDataException">The file is not correctly formatted, is corrupt, or is not a 3dMod file.</exception>
		public ModelSource LoadModel() {
			model = new ModelSource();
			materials = new List<Material>();

			// Read the header data.
			readHeader();

			// Now, continue until the entire file is read.
			while (stream.Position < stream.Length)
			{
				// Read the next chunk header.
				string chunk = new string(file.ReadChars(4));
				switch (chunk)
				{
					case "MTRL":
						// Material data.
						materials.Add(readMaterial());
						break;
					case "MESH":
						// Mesh data.
						model.Meshes.Add(readMesh());
						break;

					default:
						// Unrecognized or unsupported chunk.
						// Skip it.
						long length = readUInt32();
						stream.Position += length;
						break;
				}
			}

			// Success!
			return model;
		}
		#endregion

		#region Header
		/// <summary>
		/// Read header data from the current file.
		/// </summary>
		private void readHeader()
		{
			// Is this a valid 3dMod file?
			string signature = new string(file.ReadChars(8));
			if (signature != "VRE3dMod") throw new InvalidDataException("The file being read does not have a valid 3dMod signature, or is corrupted.");

			// Check file size.
			fileSize = readUInt32();
			if (fileSize < 20) DebugLog.Note("The reported file size of a 3dMod file was invalid.");
			if (fileSize != stream.Length - 12) DebugLog.Note("The reported file size of a 3dMode file was incorrect. The file may be corrupt, or missing data.");

			// Get version numbers.
			majorVersion = file.ReadByte();
			minorVersion = file.ReadByte();
			if (majorVersion > 1)
			{
				DebugLog.Failure("A 3dMod file is using a newer version than is supported by this build of the OFE Engine.");
				throw new InvalidDataException("Incompatible 3dMod file version.");
			}

			// Skip reserved space.
			file.ReadBytes(18);
		}
		#endregion

		#region Material
		/// <summary>
		/// The type of texture map in a 3dMod file.
		/// </summary>
		private enum MapType
		{
			Diffuse = 1,
			Opacity = 2,

			Specular = 3,
			Gloss = 4,

			Bump = 5,
			Normal = 6,
			Displacement = 7,

			Environment = 8,

			Glow = 9,

			Mask = 10
		}

		/// <summary>
		/// Reads material data from the current position in the file.
		/// </summary>
		private Material readMaterial()
		{
			Material mat = new Material();

			// 0 - Chunk Identifier
			// (Should have already been read.)

			// 4 - Chunk Length
			uint chunkLength = readUInt32();

			// 8 - Stride
			uint stride = readUInt32();

			// 12 - Texture Count
			uint nTextures = readUInt32();

			// 16 - Name
			mat.Name = readString();

			// MATERIAL DATA

			// 0 - Boolean Flags
			int flags1 = stream.ReadByte();
			stream.ReadByte();

			if ((flags1 & 0x1) == 0x1) mat.IsLit = false;
			if ((flags1 & 0x2) == 0x2) mat.IsDoubleSided = true;
			if ((flags1 & 0x4) == 0x4) mat.IsWireframe = true;

			// 2 - Reserved
			stream.Position += 2;

			// 4 - Specularity
			mat.Specularity = readFloat();

			// 8 - Glossiness
			mat.Glossiness = readFloat();

			// Make sure we've passed the stride.
			if (stride < 12) stream.Position += stride - 12;

			// TEXTURE DATA

			string directory = Path.GetDirectoryName(pathAndFilename);

			for (int i = 0; i < nTextures; i++)
			{
				if (file.PeekChar() == -1)
				{
					DebugLog.Failure("A material's textures are missing in a 3dMod file.");
					break;
				}

				int mapType = stream.ReadByte();
				string mapFile = Path.Combine(directory, readString());

				switch ((MapType)mapType)
				{
					case MapType.Diffuse:
						mat.Diffuse = new Texture(mapFile);
						break;
					case MapType.Opacity:
						mat.Opacity = new Texture(mapFile);
						break;

					case MapType.Specular:
						mat.Specular = new Texture(mapFile);
						break;
					case MapType.Gloss:
						mat.Gloss = new Texture(mapFile);
						break;

					case MapType.Normal:
						mat.Normal = new Texture(mapFile);
						break;

					case MapType.Environment:
						mat.Environment = new Texture(mapFile);
						break;

					case MapType.Glow:
						mat.Glow = new Texture(mapFile);
						break;
				}
			}

			return mat;
		}
		#endregion

		#region Mesh
		/// <summary>
		/// Reads mesh data from the current position in the file.
		/// </summary>
		private Mesh readMesh() {
			Mesh mesh = new Mesh();

			// 0 - Chunk Identifier
			// (Should have already been read.)

			// 4 - Chunk Length
			long chunkLength = readUInt32();
			if (chunkLength < 60) {
				DebugLog.Failure("A mesh chunk in a 3dMod file reported that it was smaller than the standard allows.");
				throw new InvalidDataException("A mesh chunk reported an invalid chunk size.");
			}
			long start = stream.Position;			
			if (start + chunkLength > stream.Length)
			{
				DebugLog.Failure("A mesh chunk in a 3dMod file incorrectly reported its size, or is missing data.");
				throw new InvalidDataException("A mesh chunk reported an incorrect chunk size and may be missing data.");
			}

			// 8 - Vertex Count
			int nVerts = (int)readUInt32();
			mesh.Vertices = new List<Vertex>(nVerts);
			for (int i = 0; i < nVerts; i++)
				mesh.Vertices.Add(new Vertex());

			// 12 - Poly Count
			int nPolys = (int)readUInt32();

			// 16 - Material Index
			uint mat = readUInt32();
			if (mat == UInt32.MaxValue)
				// Has no material.
				mesh.Material = null;
			else if (materials.Count <= mat)
				DebugLog.Failure("A mesh chunk in a 3dMod file is missing material data.");
			else
				mesh.Material = materials[(int)mat];

			// 20 - Local Position
			mesh.Position = new Vector(readFloat(), readFloat(), readFloat());

			// 32 - Local Rotation
			mesh.Rotation = new Quaternion(readFloat(), readFloat(), readFloat(), readFloat());

			// 48 - Local Scale
			mesh.Scale = new Vector(readFloat(), readFloat(), readFloat());

			// 60 - Bounding Sphere
			Vector v = new Vector(readFloat(), readFloat(), readFloat());
			mesh.BoundingSphere = new Sphere(readFloat(), v);

			// 76 - Bounding Box
			Vector min = new Vector(readFloat(), readFloat(), readFloat());
			Vector max = new Vector(readFloat(), readFloat(), readFloat());

			// 100 - Vertex Data Array Count
			uint nArrays = readUInt32();

			// 1004 - Name
			mesh.Name = readString();

			// Read the poly array.
			mesh.Polygons = new List<Poly>(nPolys);
			for (int i = 0; i < nPolys; i++)
			{
				mesh.Polygons.Add(new Poly((int)readUInt32(), (int)readUInt32(), (int)readUInt32()));
			}

			// Read the various vert arrays.			
			for (int a = 0; a < nArrays; a++)
			{
				// Check the array identifier.
				byte type = file.ReadByte();
				byte data = file.ReadByte();
				byte stride = file.ReadByte();
				byte flags = file.ReadByte();

				// Read the array.
				float[] array = new float[stride * nVerts];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = readFloat();
				}

				// Put the array data in the mesh.
				Vertex vert;
				for (int i = 0; i < nVerts; i++)
				{
					switch (type)
					{
						case (byte)VertArrayType.Position:
							vert = mesh.Vertices[i];
							vert.Position = new Vector(array[i * 3], array[i * 3 + 1], array[i * 3 + 2]);
							mesh.Vertices[i] = vert;
							break;

						case (byte)VertArrayType.Normal:
							vert = mesh.Vertices[i];
							vert.Normal = new Vector(array[i * 3], array[i * 3 + 1], array[i * 3 + 2]);
							mesh.Vertices[i] = vert;
							break;

						case (byte)VertArrayType.UVs:
							vert = mesh.Vertices[i];
							vert.TextureCoords = new Coord(array[i * 2], array[i * 2 + 1]);
							mesh.Vertices[i] = vert;
							break;

						case (byte)VertArrayType.Color:
							vert = mesh.Vertices[i];
							vert.Color = new Color(array[i * stride], array[i * stride + 1], array[i * stride + 2]);
							if (stride > 3)
							{
								vert.Color.A = array[i * stride + 3];
							}
							mesh.Vertices[i] = vert;
							break;
					}
				}
			}

			// Make sure the mesh's buffer is updated.
			mesh.UpdateBuffer();

			// Done.
			return mesh;
		}

		private enum VertArrayType
		{
			Position = 1,
			Normal = 2,
			Color = 3,
			Tangent = 4,
			Bitangent = 5,
			UVs = 100,
			BoneWeights = 200,
			EndOfData = 255
		}		
		#endregion

		#region Helpers
		/// <summary>
		/// Reads a string of characters prefixed with a 16-bit unsigned length intger.
		/// </summary>
		/// <returns></returns>
		private string readString()
		{
			int length = readUInt16();
			return new string(file.ReadChars(length));
		}

		/// <summary>
		/// Reads a 16-bit integer from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private Int16 readInt16()
		{
			Byte[] b = file.ReadBytes(2);
			if (!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToInt16(b, 0);
		}

		/// <summary>
		/// Reads an unsigned 16-bit integer from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private UInt16 readUInt16()
		{
			Byte[] b = file.ReadBytes(2);
			if (!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToUInt16(b, 0);
		}

		/// <summary>
		/// Reads a 32-bit integer from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private Int32 readInt32()
		{
			Byte[] b = file.ReadBytes(4);
			if(!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToInt32(b, 0);
		}

		/// <summary>
		/// Reads an unsigned 32-bit integer from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private UInt32 readUInt32()
		{
			Byte[] b = file.ReadBytes(4);
			if (!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToUInt32(b, 0);
		}

		/// <summary>
		/// Reads a 64-bit integer from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private Int64 readInt64()
		{
			Byte[] b = file.ReadBytes(8);
			if (!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToInt64(b, 0);
		}

		/// <summary>
		/// Reads an unsigned 64-bit integer from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private UInt64 readUInt64()
		{
			Byte[] b = file.ReadBytes(8);
			if (!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToUInt64(b, 0);
		}

		/// <summary>
		/// Reads a single-precision 32-bit floating point number from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private Single readFloat()
		{
			Byte[] b = file.ReadBytes(4);
			if (!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToSingle(b, 0);
		}

		/// <summary>
		/// Reads a double-precision 64-bit floating point number from the file
		/// (advancing the current position of the stream),
		/// and makes sure its endianness matches the system.
		/// </summary>
		private Double readDouble()
		{
			Byte[] b = file.ReadBytes(8);
			if (!BitConverter.IsLittleEndian) b.Reverse();
			return BitConverter.ToDouble(b, 0);
		}
		#endregion
	}
}
