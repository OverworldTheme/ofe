﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

#if SDL
using SDL2;
#endif

using VisionRiders.OFE.Graphics;

namespace VisionRiders.OFE.Data
{
	/// <summary>
	/// Information about the system that the engine is running on.
	/// Some of this is available from default C# libraries,
	/// while other information must be gathered from various APIs.
	/// It's all gathered here in one place for ease of access.
	/// </summary>
	public static class SysInfo
	{
		#region Initialization
		/// <summary>
		/// Gathers all the information from the system and APIs,
		/// and then records it for later use.
		/// </summary>
		internal static void Init()
		{
			DebugLog.Write("Gathering system information...");
			DebugLog.Break();
						
			// Time to do some detective work.
			SystemName = Environment.MachineName;
			UserName = Environment.UserName;

			Locale = CultureInfo.CurrentCulture;
			LanguageName = CultureInfo.CurrentCulture.NativeName;
			LanguageEnglishName = CultureInfo.CurrentCulture.EnglishName;			

			if (Environment.Is64BitOperatingSystem)
				WordSize = 64;
			else
				WordSize = 32;

#if SDL
			NumProcessors = SDL.CPU.GetCPUCount();
			if (NumProcessors < 1)
				NumProcessors = Unknown;
			L1Cache = SDL.CPU.GetCacheLineSize();
			if (L1Cache < 1)
				NumProcessors = Unknown;
			RAM = SDL.CPU.GetSystemRAM();
			if (RAM < 1)
				NumProcessors = Unknown;

			NumDisplays = SDL.Window.GetNumVideoDisplays();
			if (NumDisplays < 1)
				NumProcessors = Unknown;
#endif

			// What did we learn today?			
			DebugLog.Write("The system's name is " + SystemName + ". The user's name is " + UserName + ".");
			DebugLog.Write("The system culture is " + Locale.ToString() + ".");
			DebugLog.Write("The system language is " + LanguageEnglishName + ".");
			DebugLog.Break();
			DebugLog.Write("This computer is running " + Environment.OSVersion.ToString() + ".");
			DebugLog.Write("The operating system is " + WordSize + "bit.");
			DebugLog.Write("The current .NET CLR version is " + Environment.Version + ".");
			DebugLog.Write("There are " + NumProcessors + " logical processors on this system.");
			DebugLog.Write("L1 cache line size is " + L1Cache + "KB. Total system RAM is " + RAM + "MB.");
			DebugLog.Break();
			DebugLog.Write("Full application directory is " + Core.Directory + ".");
			DebugLog.Write("User data folder set to " + Core.UserFolder + ".");
#if DEBUG
			DebugLog.Break();
			DebugLog.Write("This is a debug build. Official support may not be available.");
#endif
			DebugLog.Break(3);
		}
		#endregion

		#region Constants
		/// <summary>
		/// A constant representing an unknown value.
		/// </summary>
		public const int Unknown = Int32.MinValue;
		#endregion

		#region System Settings
		/// <summary>
		/// The user-assigned name of this system, if available.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static string SystemName { get; private set; }

		/// <summary>
		/// The operating system that the system is running.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static OS Platform { get; private set; }

		/// <summary>
		/// The culture setting of the system, if available.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static CultureInfo Locale { get; private set; }

		/// <summary>
		/// The localized name of the system lanugage.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static string LanguageName { get; private set; }

		/// <summary>
		/// The English name for the system language.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static string LanguageEnglishName { get; private set; }
		#endregion

		#region User
		/// <summary>
		/// The account name of the current user of the system, if available.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static string UserName { get; private set; }
		#endregion

		#region CPU
		/// <summary>
		/// The make and model of the main processor, if available.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static string CpuMake { get; private set; }

		/// <summary>
		/// The number of bits naturally used by the processor.
		/// For example, 32 bit or 64 bit.
		/// Not neccissarily the bit setting currently available by the engine.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static int WordSize { get; private set; }

		/// <summary>
		/// The number of logical processors available on the system.
		/// Will be SysInfo.Unknown if the number is unknown.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static int NumProcessors { get; private set; }

		/// <summary>
		/// The size of the processor's L1 cache line, in megabytes.
		/// Will be SysInfo.Unknown if the number is unknown.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static int L1Cache { get; private set; }

		/// <summary>
		/// Amount of Random Access Memory available on the system, in megabytes.
		/// Will be SysInfo.Unknown if the number is unknown.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static int RAM { get; private set; }
		#endregion

		#region Displays
		/// <summary>
		/// The total number of displays (monitors, etc.) used by the system.
		/// Will be SysInfo.Unknown if the number is unknown.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static int NumDisplays { get; private set; }

		/// <summary>
		/// The set resolution of each display.
		/// </summary>
		/// <remarks>
		/// This value is cached by the engine on initialization.
		/// </remarks>
		public static CoordInt[] DisplayResolution { get; private set; }
		#endregion

		#region Platform Detection
		/// <summary>
		/// Unix system name structure.
		/// Used to detect a Linux system on startup.
		/// </summary>
		/// <remarks>
		/// <para>Code based on that used by OpenTK.</para>
		/// <para>The layout of the utsname struct is defined by the Single Unix Specification.</para>
		/// </remarks>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
		private struct utsname
		{
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string sysname;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string nodename;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string release;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string version;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string machine;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
			public string extraJustInCase;
		}

		/// <summary>
		/// Used to get system information from the Unix kernal.
		/// </summary>
		/// <param name="uname_struct"></param>
		[DllImport("libc")]
		private static extern void uname(out utsname uname_struct);

		/// <summary>
		/// Determines what platform the engine is running on.
		/// Called by the engine core befire the actual SysInfo initialization.
		/// </summary>
		internal static void DetectPlatform()
		{
#if ANDROID
			CurrentPlatform = OS.Android;
#elif IOS
			CurrentPlatform = OS.iOS;
#else
			if (System.Environment.OSVersion.Platform == PlatformID.Win32NT
				|| System.Environment.OSVersion.Platform == PlatformID.Win32S
				|| System.Environment.OSVersion.Platform == PlatformID.Win32Windows
				|| System.Environment.OSVersion.Platform == PlatformID.WinCE)
			{
				// We're on a Windows system.
				Platform = OS.Windows;
			}
			else if (System.Environment.OSVersion.Platform == PlatformID.Unix)
			{
				// We're running on some sort of Unix.
				// We'll ask the kernel its name.
				utsname uts = new utsname();
				uname(out uts);

				// Who are you?
				switch (uts.sysname)
				{
					case "Linux":
						// It's some flavor of Linux.
						Platform = OS.Linux;
						break;
					case "Darwin":
						// It's a Mac OSX system,
						// reporting itself as Unix instead of Mac OS.
						Platform = OS.OSX;
						break;
					default:
						// It's Unix, but we don't know what kind.
						Platform = OS.Unix;
						break;
				}
			}
			else if (System.Environment.OSVersion.Platform == PlatformID.MacOSX)
			{
				// We're on a Mac OSX system.
				Platform = OS.OSX;
			}
			else
			{
				// We don't know what we're doing.
				Platform = OS.Unknown;
			}
#endif
		}
		#endregion
	}
}
