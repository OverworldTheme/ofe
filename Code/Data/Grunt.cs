﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

#if OPENGL
using System.Drawing;
#elif XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endif

using VisionRiders.OFE;
using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Graphics.OFE3D;

namespace VisionRiders.OFE.Data {
  /// <summary>
  /// "Grunt", OFE's multithreaded resource loader.
  /// </summary>
  /// <remarks>
  /// It slices! It dices! It works hard so that YOU DON'T HAVE TO!
  /// 
  /// Grunt is a helpful little workhorse. It runs in a seperate thread from
  /// OFE and your program. Whenever you load a sprite, or a texture, or a
  /// data file through OFE, Grunt goes to work opening and processing the file.
  /// In the meantime, you can go on with your program.
  /// 
  /// For example, if you load a sprite with OFE, you can display a dynamic,
  /// animated loading screen while Grunt works in the background. Or you could
  /// even go on with your program, positioning and moving the sprite around
  /// as normal. As soon as the sprite's actual graphics and data are finished,
  /// OFE can display it wherever you've moved it to.
  /// </remarks>
  public static class Grunt {
    #region Initialization
    /// <summary>
    /// Initializes Grunt, and spins it off in a new thread.
    /// </summary>
    internal static void Init() {
      // Tell the log what we're about to do.
      DebugLog.Write("Waking up Grunt...");

#if XNA
      // We need to make a new content manager for Grunt's personal use.
      xnaContent = new ContentManager(Core.XNA.Services);
#endif

      // Okay, Grunt. Off you go!
      Thread = new Thread(work);
			Thread.Name = "Grunt";
      Thread.Start();

      // Write to the log.
      DebugLog.Success("Good morning, Grunt!");
    }
    #endregion

    #region API Hooks
#if XNA
    /// <summary>
    /// The XNA content manager Grunt has at its disposal.
    /// </summary>
    internal static ContentManager xnaContent { get; set; }
#endif
    #endregion

    #region Thread
    /// <summary>
    /// Grunt's own personal thread.
    /// </summary>
    internal static Thread Thread { get; set; }
    #endregion

    #region Priority
    public enum Priority {
      /// <summary>
      /// High loading priority, for things that must be loaded first.
      /// </summary>
      /// <remarks>
      /// Most of the time, the only thing you'll be setting to high priority
      /// are the graphic elements for loading screens to make sure they don't
      /// get stuck waiting for the actual program content.
      /// </remarks>
      High,

      /// <summary>
      /// Normal loading priority.
      /// By default, this is what your content is assigned to.
      /// </summary>
      Normal,

      /// <summary>
      /// Low loading priority.
      /// Grunt saves them for last, and general calls to Loading will return
      /// false if only low-priority content is left in the queue.
      /// </summary>
      /// <remarks>
      /// This is OFE's "backburner" so to speak.
      /// Useful for unimportant things like loading the next level while the
      /// player finishes up the one they're on.
      /// Keep in mind that, even at low priority, your program may experience
      /// frame rate hiccups from the garbage collector or the operating system's
      /// file I/O.
      /// </remarks>
      Low
    }
    #endregion

    #region Loading Status
    /// <summary>
    /// Checks whether or not Grunt is currently busy loading any normal or high
    /// priority content.
    /// </summary>
    /// <returns>If Grunt is still loading low priority content, this will return
    /// false so long as there are no high or normal priority content left in
    /// the queue.</returns>
    public static bool IsLoading() {
      return IsLoading(Priority.Normal);
    }

    /// <summary>
    /// Checks whether or not Grunt is currently busy loading any content of the
    /// given priority or higher.
    /// </summary>
    /// <param name="priority">The lowest priority to check for.</param>
    public static bool IsLoading(Priority priority) {
      lock(queueLock) {
        switch (priority) {
          case Priority.High:
            if (UrgentQueue.Count > 0)
              return true;
            else
              return false;
          case Priority.Normal:
          default:
            if (NormalQueue.Count > 0
              || UrgentQueue.Count > 0)
              return true;
            else
              return false;
          case Priority.Low:
            if (BackburnerQueue.Count > 0
              || NormalQueue.Count > 0
              || UrgentQueue.Count > 0)
              return true;
            else
              return false;
        }
      }
    }
    #endregion

    #region Queues
    /// <summary>
    /// The urgent loading queue. These are the most important files that must be
    /// loaded before any others.
    /// </summary>
    /// <remarks>Guarded by queueLock</remarks>
		internal static Queue<ILoadable> UrgentQueue = new Queue<ILoadable>();

    /// <summary>
    /// The normal loading queue. By default, things to be loaded go here.
    /// </summary>
    /// <remarks>Guarded by queueLock</remarks>
		internal static Queue<ILoadable> NormalQueue = new Queue<ILoadable>();

    /// <summary>
    /// The lowest priority loading queue. 
    /// </summary>
    /// <remarks>Guarded by queueLock</remarks>
		internal static Queue<ILoadable> BackburnerQueue = new Queue<ILoadable>();
    
		/// <summary>
		/// The reader-writer lock, used for thread safety.
		/// </summary>
		private static object queueLock = new ReaderWriterLockSlim();

    /// <summary>
    /// Adds the given object to the loading queue, and assigns it as normal priority.
    /// Make sure OFE supports loading the given object before trying to add it.
    /// See the documentation for details.
    /// </summary>
		public static void Enqueue(ILoadable thisThing) {
      Enqueue(thisThing, Priority.Normal);
    }

    /// <summary>
    /// Adds the given object to the loading queue.
    /// Make sure OFE supports loading the given object before trying to add it.
    /// See the documentation for details.
    /// </summary>
    /// <param name="thisThing">The object to add to the queue.</param>
    /// <param name="priority">The priority to assaign to the object.
    /// Higher priority objects are loaded first.</param>
		public static void Enqueue(ILoadable thisThing, Priority priority) {
      lock(queueLock) {
        switch (priority) {
          case Priority.High:
            UrgentQueue.Enqueue(thisThing);
            break;
          case Priority.Normal:
          default:
            NormalQueue.Enqueue(thisThing);
            break;
          case Priority.Low:
            BackburnerQueue.Enqueue(thisThing);
            break;
        }
      }
    }
    #endregion

    #region Work Loop
    /// <summary>
    /// Grunt's core work loop.
    /// Each loop it will load one resource, if there is a resource in the queue.
    /// </summary>
    private static void work() {
      // Tell the log we're up and running.
      DebugLog.Success("\"This is Grunt, reporting for duty!\"");

      while (!Core.IsShuttingDown) {
        Queue<ILoadable> queueToPoll = null;
        ILoadable objectToLoad = null;

        lock(queueLock) {
          if (UrgentQueue.Count > 0) {
            queueToPoll = UrgentQueue;
          } else if (NormalQueue.Count > 0) {
            queueToPoll = NormalQueue;
          } else if (BackburnerQueue.Count > 0) {
            queueToPoll = BackburnerQueue;
          }
          if (queueToPoll != null) {
            objectToLoad = queueToPoll.Peek();
          }
        }

        if (objectToLoad == null) {
          // nothing to load so we can afford to sleep.
          Thread.Sleep(100);
        } else if(!Core.IsShuttingDown) {
          // We checked if we're shutting down again, just in case it changed.
          // We don't want to bother loading in the middle of a shut down.

          load(objectToLoad);
          lock(queueLock) {
            queueToPoll.Dequeue();
          }

          // Pause a moment before loading next object.
          // (Keeps the thread from hogging *ALL* the CPU cycle while loading.)
          Thread.Sleep(1);
        }
      }

      // If we're no longer in the work loop, it's because we're shutting down.
      // Tell the log as much.
      DebugLog.Success("Grunt is shutting down. Goodnight Grunt!");
    }
    #endregion

    #region Loading
		/// <summary>
		/// The (relative) directory where all resources should be loaded from.
		/// Can be left empty if there's no need to specify one.
		/// </summary>
		public static string BaseFolder { get; set; }

		/// <summary>
		/// The full, absolute path of the directory where all resources should be loaded from.
		/// </summary>
		/// <remarks>
		/// It's important to use this because the "current directory" may not be the same directory that the executable was launched from.
		/// For best results across all platforms, make sure to use the standard library's Path.Combine when building the full path to a fill.
		/// </remarks>
		public static string FullPath {
			get {
				if(BaseFolder != null) {
					return Path.Combine(Core.Directory, BaseFolder);
				} else {
					return Core.Directory;
				}
			}
		}

    /// <summary>
    /// Loads the given resource right away.
    /// </summary>
    /// <param name="thisThing">The resource to load.</param>
		private static void load(ILoadable thisThing) {
			DebugLog.Write("Grunt is about to load " + thisThing.PathAndFilename + "...");
			thisThing.LoadNow();
			if(thisThing.OnLoaded != null) {
        DebugLog.Write("Grunt is firing the OnLoaded event for " + thisThing.PathAndFilename + "...");
				thisThing.OnLoaded(typeof(Grunt), EventArgs.Empty);
        DebugLog.Note("The OnLoaded event was fired and has finished executing.");
			}
    }
    #endregion
  }
}
