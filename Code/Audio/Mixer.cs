﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Audio
{
	/// <summary>
	/// The heart of the audio system.
	/// Manages input from channels and sends them out to the audio API being used.
	/// </summary>
	public static class Mixer
	{
		#region Initialization
		/// <summary>
		/// Initializes the mixer and all its audio channels.
		/// </summary>
		internal static void Init()
		{
			// Report to the debug log.
			DebugLog.Write("Initializing audio...");

			// Set our audio resolution.
			AudioResolution = defaultAudioResolution;

			// Initialize our channels.
			DebugLog.Note("Initializing mixer channels...");
			channels = new List<Channel>(defaultNumChannels);
			for (int i = 0; i < NumChannels; i++)
				channels[i] = new Channel();

			// Success!
			DebugLog.Success("Audio was successfully initialized!");
		}
		#endregion

		#region Settings
		/// <summary>
		/// The default audio output, in Hz.
		/// </summary>
		private const int defaultAudioResolution = 48000;

		/// <summary>
		/// The current audio output, in Hz.
		/// That is, the number of audio samples in a single second.
		/// </summary>
		public static int AudioResolution { get; private set; }
		#endregion

		#region Channels
		/// <summary>
		/// The default total number of audio channels.
		/// </summary>
		private const int defaultNumChannels = 12;

		/// <summary>
		/// The total number of audio channels available to the mixer.
		/// </summary>
		public static int NumChannels
		{
			get { return channels.Count; }
		}

		/// <summary>
		/// All the audio channels for use by the system.
		/// </summary>
		private static List<Channel> channels;

		/// <summary>
		/// Find and return an unused audio channel.
		/// </summary>
		/// <returns>Null if there is no free channel.</returns>
		public static Channel GetFreeChannel()
		{
			// Do we have a free channel?
			foreach(Channel c in channels)
			{
				if (!c.IsInUse)
					return c;
			}

			// Looks like we don't.
			return null;
		}
		#endregion

		#region Output
		#endregion

		#region Work Loop
		private static void workLoop()
		{

		}
		#endregion
	}
}
