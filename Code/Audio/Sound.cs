﻿using System;
using System.Collections.Generic;
using System.Linq;

#if OPENGL
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
#elif XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// The heart of OFE's sound engine.
	/// This class oversees audio and playback, and interfaces with the API.
	/// </summary>
  public static class Sound {
		#region Initialization
		/// <summary>
    /// Initializes all sound components.
    /// </summary>
    internal static void Init() {
			DebugLog.Write("Initializing audio engine...");

#if OPENGL
			// Create Context.
			try {
				glContext = new AudioContext();
				glContext.MakeCurrent();
				LogErrors();
			} catch(AudioContextException e) {
				DebugLog.Failure("Failed to create soundscape!");
				LogErrors();
				throw new Exception("Could not create soundscape. Reason:" + Environment.NewLine + e.Message, e);
			}
#elif XNA
      try {
        xnaAudioEngine = new AudioEngine("audio\\audio.xgs");
        xnaWaveBank = new WaveBank(xnaAudioEngine, "audio\\sound.xwb");
        xnaSoundBank = new SoundBank(xnaAudioEngine, "audio\\sound.xsb");
      } catch {
        // DO SOMETHING!!! THIS MUST BE DEALT WITH ASAP!!!
      }
#endif

			// Initialize audio channels.
			Channel.Init();

			// Get soundscapes ready to go.
			Soundscape.Init();

			// Done!
			DebugLog.Success("The audio engine has been initialized.");
		}
		#endregion

		#region Settings
		/// <summary>
		/// Whether or not audio is enabled.
		/// </summary>
		/// <remarks>
		/// <para>Disabling sound prevents it from playing altogether.</para>
		/// <para>If you want to be able to turn audio on and off,
		/// but still be able to resume in the middle of a sound instead of having to restart it,
		/// you should mute the global volume level instead.</para>
		/// </remarks>
		public static bool IsEnabled = true;

		/// <summary>
		/// The global volume level.
		/// Changing this will affect the volume of all music, sound effects, and any other audio used by the engine.
		/// </summary>
		/// <remarks>1.0f is the default volume level.</remarks>
    public static float Volume = 1.0f;
		#endregion

		#region Buffers
		/// <summary>
		/// The number of buffers available to the engine.
		/// </summary>
		public static int NumberOfBuffers = 32;

#if OPENGL
		/// <summary>
		/// Requests an open buffer.
		/// </summary>
		/// <returns>An open buffer, if one is available.</returns>
		public static int glRequestBuffer() {
			Sound.LogErrors();
			return AL.GenBuffer();
		}
#endif

#if OPENGL
#endif
		#endregion

		#region API Interfaces
#if OPENGL
		/// <summary>
		/// The OpenAL context used for playback.
		/// </summary>
		private static AudioContext glContext;
#elif XNA
		public static AudioEngine xnaAudioEngine;
    public static SoundBank xnaSoundBank;
    public static WaveBank xnaWaveBank;
#endif
		#endregion

    #region Error Logging
    /// <summary>
    /// The maximum number of errors to check for in a single pass.
    /// Some platforms do not correctly observe the OpenAL standard and will generate new errors when error checking.
    /// Capping the number of times to check should eliminate the possiblity of an infinite loop. 
    /// </summary>
    private const int maxErrorsPerCheck = 10;

    /// <summary>
		/// Check if there have been any OpenAL errors, and if so, write them to the debug log.
		/// </summary>
		public static void LogErrors() {
#if OPENGL
			// Log any errors.
			ALError error = AL.GetError();
			int errors = 0;
			while(error != ALError.NoError && errors < maxErrorsPerCheck) {
				DebugLog.Failure("OpenAL Error: " + error.ToString() + "!");
				error = AL.GetError();

				// Limit the number of errors we check for,
				// so that we don't get stuck in an infinite loop in case OpenAL wets itself.
				errors++;
			}
#endif
    #endregion
    }
	}
}