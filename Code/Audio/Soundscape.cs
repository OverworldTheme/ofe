﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if OPENGL
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
#endif

using VisionRiders.OFE.Graphics;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// An imaginary environment where sounds can be positioned in three dimensions.
	/// Think of it as the audio equivalent to a Stage or Scene.
	/// </summary>
	public class Soundscape {
		#region Initialization
		internal static void Init() {
			// Create the soundscape list.
			All = new List<Soundscape>();

			// Let's create the default soundscape.
			Soundscape.Default = new Soundscape();
		}
		#endregion

		#region Class Constructor
		/// <summary>
		/// Creates a new soundscape.
		/// </summary>
		public Soundscape() {
			Sounds = new List<SFX>();
			Songs = new List<Song>();
			IsActive = true;

			All.Add(this);
		}
		#endregion

		#region Death
		/// <summary>
		/// Deletes this soundscape.
		/// </summary>
		/// <exception cref="InvalidOperationException">Called if the application attempts to delete the default soundscape.</exception>
		public void Dispose() {
			dispose(true);
		}
		
		/// <summary>
		/// Whether or not this soundscape has been disposed yet.
		/// </summary>
		private bool disposed;

		/// <summary>
		/// Disposes of this soundscape.
		/// </summary>
		/// <param name="disposeManaged">Whether or not to dispose managed resources.</param>
		private void dispose(bool disposeManaged) {
			if(disposed)
				return;
			else
				disposed = true;

			if(this == Default && !Core.IsShuttingDown) {
				throw new InvalidOperationException("The default sound scapecannot be deleted.");
			} else {
				if(disposeManaged) {
					All.Remove(this);
				}

				GC.SuppressFinalize(this);
			}
		}

		~Soundscape() {
			dispose(false);
		}
		#endregion

		#region Properties
		/// <summary>
		/// Whether or not this soundscape is active and its audio it allowed to play.
		/// </summary>
		public bool IsActive { get; set; }

		/// <summary>
		/// The listener is where the "ears" of the soundscape are located.
		/// All 3d sound will be generated in relation to the listener.
		/// </summary>
		/// <remarks>
		/// In most cases, the listener need only remain stationary at (0, 0, 0) while
		/// sounds are positioned around it.
		/// </remarks>
		public Vector Listener;
		#endregion

		#region Contents
		/// <summary>
		/// All the sounds in this soundscape.
		/// </summary>
		private List<SFX> Sounds { get; set; }

		/// <summary>
		/// All the songs in this soundscape.
		/// </summary>
		private List<Song> Songs { get; set; }

		/// <summary>
		/// Adds the given sound effect to this soundscape.
		/// </summary>
		/// <param name="sfx">The sound effect to add.</param>
		internal void AddSound(SFX sfx) {
			Sounds.Add(sfx);
		}

		/// <summary>
		/// Removes the given sound effect from this soundscape.
		/// </summary>
		/// <param name="sfx">The sound effect to remove.</param>
		internal void RemoveSound(SFX sfx) {
			Sounds.Remove(sfx);
		}
		#endregion

		#region Updating
		/// <summary>
		/// Orders all soundscapes to update for the frame.
		/// </summary>
		internal static void UpdateAll() {
			foreach(Soundscape soundscape in All) {
				soundscape.Update();
			}
		}

		/// <summary>
		/// Checks up on this soundscape and its contents each frame.
		/// </summary>
		internal void Update() {
			/*foreach(SFX sfx in Sounds) {
				sfx.Update();
			}*/
		}
		#endregion

		#region All
		/// <summary>
		/// All active soundscapes.
		/// </summary>
		internal static List<Soundscape> All { get; set; }
		#endregion

		#region Default soundscape
		/// <summary>
		/// The default sound scape.
		/// In most cases, this really is the only one you'll ever need.
		/// </summary>
		public static Soundscape Default { get; set; }
		#endregion
	}
}
