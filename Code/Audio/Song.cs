﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

#if OPENGL
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using NVorbis;
#elif XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// A single musical or audio track that can be played by the sound engine.
	/// </summary>
	public class Song : StreamedSFX {
		/// <summary>
		/// Loads a new song from the given file.
		/// </summary>
		/// <param name="filename">The relative path and file name of the song file,
		/// without the file extension.</param>
		public Song(string filename) : base(filename) {
			// By default, songs loop.
			IsLooped = true;
		}
	}
}
