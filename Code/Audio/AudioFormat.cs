﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// The format of raw audio data.
	/// </summary>
	public enum AudioFormat {
		/// <summary>
		/// 8-bit monaural sound.
		/// </summary>
		Mono8,

		/// <summary>
		/// 16-bit monaural sound.
		/// </summary>
		Mono16,
		
		/// <summary>
		/// 8-bit stereo sound.
		/// </summary>
		Stereo8,

		/// <summary>
		/// 16-bit stereo sound.
		/// </summary>
		Stereo16,
	}
}
