﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// An audio channel that can play audio such as music and sound effects.
	/// </summary>
	public class Channel {
		#region Initialization
		/// <summary>
		/// Initialize all channels.
		/// Called by the Sound engine when OFE boots.
		/// </summary>
		internal static void Init() {
			// Create the lock for later use.
			threadLock = new object();

			// Create the reserved audio channels.			
			/*Debug.Log("Creating "+ reserveCount + " reserve audio channels...");			
			Reserve = new List<Channel>(reserveCount);
			int count = 0;
			for(int i = 0; i < reserveCount; i++) {
				try {
					Channel channel = new Channel();
					Reserve.Add(channel);
					count++;
				} catch {
					break;
				}
			}
			if(count == reserveCount)
				Debug.LogSuccess("Created " + count + " reserve audio channels!");
			else if(count > 0)
				Debug.LogFailure("Only created " + count + " reserve audio channels!");
			else
				Debug.LogFailure("Unable to create any reserve audio channels!");*/
		}
		#endregion

		#region Class Constructor
		/// <summary>
		/// Creates a new channel.
		/// </summary>
		/// <param name="number">The number to assign to this channel.</param>
		/// <exception cref="InvalidOperationException">
		/// Thrown if there are no more audio channels that can be created on this hardware.
		/// </exception>
		internal Channel() {
			//Debug.Log("Creating a new audio channel...");

#if OPENGL
			// Attempt to make a source.
			alSource = AL.GenSource();
			ALError error = AL.GetError();
			if(error != ALError.NoError) {
				DebugLog.Failure("Hardware doesn't support any more audio channels!");
				throw new InvalidOperationException("Hardware doesn't support any more audio channels.");				
			}
#endif
			//Debug.LogSuccess("Audio channel created successfully!");
		}
		#endregion

		#region Class Disposal
		/// <summary>
		/// Whether or not the class is already disposed.
		/// </summary>
		private bool isDispoed;
		
		/// <summary>
		/// Class finalizer.
		/// </summary>
		~Channel() {
			if(!isDispoed) {
				// Delete the source.
				Sound.LogErrors();
				if(AL.IsSource(alSource))
					AL.DeleteSource(alSource);
				Sound.LogErrors();

				// We're disposed now.
				isDispoed = true;
			}
		}
		#endregion

		#region Channels
		/// <summary>
		/// The number of reserve channels to try making.
		/// Whether these get made depends on the hardware and how many channels
		/// other programs are already using.
		/// </summary>
		private const int reserveCount = 4;

		internal static List<Channel> Reserve { get; set; }

		/// <summary>
		/// Attach the given audio buffer to a free channel.
		/// </summary>
		/// <param name="buffer">The buffer to attach.</param>
		/// <returns>Null if there are no free channels. Too bad!</returns>
		public static Channel AttachToFree(AudioBuffer buffer) {
			lock(threadLock) {
				// See if we have a free channel in reserve.
				/*foreach(Channel channel in Reserve) {
					if(!channel.IsInUse) {
						channel.attach(buffer);
						return channel;
					}
				}*/

				// Looks like all the reserve channels are taken.
				// Let's try making a new one.
				try {
					Channel channel = new Channel();
					channel.attach(buffer);
					return channel;
				} catch {
					return null;
				}
			}
		}
		#endregion

		#region Thread Safety
		/// <summary>
		/// An object used for thread locking.
		/// </summary>
		private static object threadLock;
		#endregion

		#region Identity
		/// <summary>
		/// This channel's number.
		/// </summary>
		public int Number { get; private set; }
		#endregion

		#region Channel Operations
		/// <summary>
		/// Gets whether or not this channel is in use.
		/// </summary>
		public bool IsInUse {
			get {
				if(AttachedBuffer != null) return true; else return false;
			}
		}

		/// <summary>
		/// Gets the audio buffer currently using this channel.
		/// </summary>
		public AudioBuffer AttachedBuffer { get; private set; }

		/// <summary>
		/// Attach a buffer to this channel.
		/// </summary>
		/// <param name="buffer">The buffer to attach.</param>
		private void attach(AudioBuffer buffer) {
			if(buffer != null) {
				buffer.FreeChannel();
				AttachedBuffer = buffer;
				buffer.Channel = this;
			}
		}

		/// <summary>
		/// Frees this channel, detaching the current audio buffer that is using it, if any.
		/// </summary>
		internal void Free() {
			if(AttachedBuffer != null) {
				AttachedBuffer.Channel = null;
				AttachedBuffer = null;
			}
		}
		#endregion

		#region API Interface
#if OPENGL
		/// <summary>
		/// The OpenAL source used by this channel.
		/// </summary>
		internal int alSource;
#endif
		#endregion
	}
}
