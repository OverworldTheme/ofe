﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// A set of audio buffers, storing raw audio information to be sent out to the sound card.
	/// </summary>
	/// <remarks>
	/// <para>For most things, OFE's included sound and music classes will be all you need.
	/// However, if you're wanting to do something funky or creative for some reason, this class has been left exposed,
	/// giving you a way to send raw audio out without having to code around OFE.</para>
	/// <para>
	/// There are two ways to utilize an AudioBuffer. The first is to use method QueueClip to queue an audio clip.
	/// The AudioBuffer can then be used to play this queued clip.
	/// </para>
	/// <para>
	/// The second way is to use AudioBuffer to play streamed audio. Put your streaming code into a method that returns
	/// an AudioClip, and then assign that method to property OnFreeBuffer. Whenever a buffer needs to be filled, it
	/// will call the OnBufferFree method and then stick the returned AudioClip into the free buffer.
	/// </para>
	/// </remarks>
	public class AudioBuffer {
		#region Initialization and Death
		/// <summary>
		/// Initializes audio buffers for use.
		/// </summary>
		/// <param name="buffers">The number of buffers to use.</param>
		/// <param name="sampleRate">The sample rate to use for each channel.</param>
		/// <param name="audioFormat">The audio format to use.</param>
		public AudioBuffer(int buffers = 5, int sampleRate = 44100, AudioFormat audioFormat = AudioFormat.Stereo16) {
			// Check for errors before going in.
			Sound.LogErrors();

			// Initiate the buffers.
			SetBuffers(sampleRate, audioFormat);
			NumberOfBuffers = buffers;

#if OPENGL
			// Queue all buffers.
			//FillBuffers();
#endif

			// Set the volume.
			setVolume = volume = 1;

			// Create the control thread.
			/*thread = new Thread(control);
			thread.Start();*/
		}

		/// <summary>
		/// Whether or not these buffers have been disposed.
		/// </summary>
		protected bool disposed = false;

		/// <summary>
		/// Discard the buffers when no longer needed.
		/// </summary>
		public void Dispose() {
			if(!disposed) {
				Dispose(true);
			}
		}

		/// <summary>
		/// Discard the buffers when no longer needed.
		/// </summary>
		/// <param name="disposedManaged">Whether or not to dispose nonmanaged objects as well.</param>
		protected void Dispose(bool disposedManaged) {
			if(disposedManaged == true) {
				Stop();
			}

			// Delete the buffers as needed.
			FlushBuffers();

			disposed = true;
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Finalizer.
		/// </summary>
		~AudioBuffer() {
			if(!disposed) {
				Dispose(false);
			}
		}
		#endregion

		#region Thread
		/// <summary>
		/// The thread that will control this set of buffers.
		/// </summary>
		private Thread thread;

		/// <summary>
		/// A generic object, used to lock the thread.
		/// </summary>
		private object bufferLock = new object();

		/// <summary>
		/// The number of loops per second that the thread will run.
		/// </summary>
		private const int lps = 100;
		#endregion

		#region Audio Channel
		/// <summary>
		/// The audio channel being used by this buffer.
		/// </summary>
		public Channel Channel { get; internal set; }

		/// <summary>
		/// Frees the audio channel this buffer is using, if any.
		/// </summary>
		public void FreeChannel() {
			if(Channel != null) {
				Stop();
				dequeueBuffers();
				Channel.Free();
			}
		}

		/// <summary>
		/// Dequeues the buffers from the channel.
		/// </summary>
		private void dequeueBuffers() {
			lock(bufferLock) {
				if(Channel != null) {
#if OPENGL
					Sound.LogErrors();
					int buffersToDequeue = 0;
					AL.SourceStop(Channel.alSource);
					AL.GetSource(Channel.alSource, ALGetSourcei.BuffersProcessed, out buffersToDequeue);
					if (buffersToDequeue > 0)
						AL.SourceUnqueueBuffers(Channel.alSource, buffersToDequeue);
					AL.GetSource(Channel.alSource, ALGetSourcei.BuffersQueued, out buffersToDequeue);
					if (buffersToDequeue > 0)
						AL.SourceUnqueueBuffers(Channel.alSource, buffersToDequeue);					
					Sound.LogErrors();
#endif
				}
			}
		}
		#endregion

		#region Buffers
#if OPENGL
		/// <summary>
		/// The OpenAL buffers that this song is using.
		/// </summary>
		private int[] alBuffer;
#endif

		/// <summary>
		/// An event called whenever a buffer is free to be written to.
		/// Use this to send streamed audio data to the buffer.
		/// </summary>
		/// <remarks>
		/// You can return a null AudioClip when you want the buffers to stop playing
		/// on finishing what they've already got.
		/// Remember not to set IsLooped to true if you want to stream audio, or else
		/// this won't be invoked properly and your initial audio will loop forever
		/// until manually told to stop.
		/// </remarks>
		public FillBuffer OnFreeBuffer { get; set; }

		public delegate AudioClip FillBuffer();

		/// <summary>
		/// The number of audio buffers available for use.
		/// </summary>
		public int NumberOfBuffers {
			get {
				return numberOfBuffers;
			}
			set {
				numberOfBuffers = value;
				ResetBuffers();
			}
		}
		private int numberOfBuffers = 5;
		#endregion

		#region Audio Format
		/// <summary>
		/// The audio format of these buffers.
		/// </summary>
		public AudioFormat AudioFormat {
			get {
				return audioFormat;
			}
			set {
				SetBuffers(SampleRate, value);
			}
		}
		private AudioFormat audioFormat = AudioFormat.Stereo16;

		/// <summary>
		/// The sample rate of the buffers.
		/// That is, how many samples there are per second, per channel.
		/// </summary>
		public int SampleRate {
			get {
				return sampleRate;
			}
			set {
				SetBuffers(value, AudioFormat);
			}
		}
		private int sampleRate;

#if OPENGL
		/// <summary>
		/// The OpenAL audio format in this file.
		/// </summary>
		private ALFormat alFormat {
			get {
				switch(AudioFormat) {
					case AudioFormat.Mono8: return ALFormat.Mono8;
					case AudioFormat.Mono16: return ALFormat.Mono16;
					case AudioFormat.Stereo8: return ALFormat.Stereo8;
					case AudioFormat.Stereo16: return ALFormat.Stereo16;
					default: return ALFormat.Stereo16;
				}
			}
		}
#endif
		#endregion

		#region Volume
		/// <summary>
		/// The volume that this audio is supposed to be playing at
		/// </summary>
		private float setVolume;

		/// <summary>
		/// The volume to play at.
		/// </summary>
		/// <remarks>At 1 the audio is playing at full volume.
		/// At 0, it is muted.
		/// Volume can be set higher than 1 to make the audio louder than the source data.</remarks>
		public float Volume {
			get {
				return volume;
			}
			set {
				lock(bufferLock) {
					isFadingOut = false;
					if(value < 0) value = 0;
					volume = value;
					setVolume = value;
					if(Channel != null)
						AL.Source(Channel.alSource, ALSourcef.Gain, value);
				}
			}
		}

		/// <summary>
		/// The actual volume that the buffer is currently playing at.
		/// </summary>
		private float volume;

		/// <summary>
		/// The amount of change to apply to the volume each frame.
		/// </summary>
		private float fadeSpeed;

		/// <summary>
		/// Fade the volume to a given level over time.
		/// </summary>
		/// <param name="volume">The new volume to play at.</param>
		/// <param name="seconds">How long, in seconds, for the fade to last.</param>
		public void FadeTo(float volume, float seconds) {
			if(seconds <= 0) {
				Volume = volume;
			} else {
				fadeSpeed = (setVolume - volume) / (seconds * lps);
				setVolume = volume;
			}
		}

		/// <summary>
		/// Whether or not the buffer is fading out.
		/// </summary>
		private bool isFadingOut;

		/// <summary>
		/// Has the volume lowered over time, and when it reaches zero the buffer will stop playing.
		/// </summary>
		/// <param name="seconds"></param>
		public void FadeOut(float seconds) {
			FadeTo(0, seconds);
			isFadingOut = true;
		}
		#endregion

		#region Setting
		/// <summary>
		/// Initializes (or reinitializes) the audio buffers.
		/// </summary>
		/// <param name="sampleRate">The sample rate the buffers will use.</param>
		/// <param name="audioFormat">What audio format the buffers will use.</param>
		public void SetBuffers(int sampleRate, AudioFormat audioFormat) {
			lock(bufferLock) {
#if OPENGL
				Sound.LogErrors();

				// Stop the stream first.
				if(IsPlaying)
					Stop();				
#endif

				// Flush the old buffers.
				FlushBuffers();

				// Set the sample rate and audio format.
				this.sampleRate = sampleRate;
				this.audioFormat = audioFormat;

#if OPENGL
				// Create a new buffers.
				alBuffer = AL.GenBuffers(NumberOfBuffers);
				Sound.LogErrors();
#endif

				Sound.LogErrors();
			}
		}

		/// <summary>
		/// Resets the buffers using the current settings.
		/// </summary>
		public void ResetBuffers() {			
			SetBuffers(SampleRate, AudioFormat);
		}
		#endregion

		#region Buffer Work
		/// <summary>
		/// Fill the initial buffers with audio.
		/// </summary>
		private void fillBuffers() {
			lock(bufferLock) {
#if OPENGL
				if(this.OnFreeBuffer == null) {
					// If we're not set up for streaming, just snap any manually queued clip into place.
					if(queuedClip != AudioClip.Empty) {
						Sound.LogErrors();
						int buffer = alBuffer[0];
						short[] clipOut = convertToShort(queuedClip.Audio);
						AL.BufferData(buffer, alFormat, clipOut, clipOut.Length * sizeof(short), SampleRate);
						Sound.LogErrors();
						AL.SourceQueueBuffer(Channel.alSource, buffer);
						Sound.LogErrors();
					}
				} else {
					for(int i = 0; i < alBuffer.Length; i++) {
						AudioClip clip = OnFreeBuffer();
						if(clip != AudioClip.Empty) {
							short[] clipOut = convertToShort(clip.Audio);

							Sound.LogErrors();
							AL.BufferData(alBuffer[i], alFormat, clipOut, clipOut.Length * sizeof(short), SampleRate);
							Sound.LogErrors();
							AL.SourceQueueBuffer(Channel.alSource, alBuffer[i]);
							Sound.LogErrors();
						} else {
							IsLooped = false;
						}
					}
				}
#endif
			}
		}

		/// <summary>
		/// Flushes the buffers.
		/// Any audio playing will be stopped.
		/// The buffers will have to be set before using them again.
		/// </summary>
		public void FlushBuffers() {
			if (IsPlaying)
				Stop();

			lock (bufferLock)
			{
#if OPENGL
				// Unqueue all the old buffers, and delete them.
				dequeueBuffers();
				if (alBuffer != null && alBuffer.Length > 0)
				{
					Sound.LogErrors();
					if(Channel != null)
						AL.Source(Channel.alSource, ALSourcei.Buffer, 0);
					Sound.LogErrors();
					AL.DeleteBuffers(alBuffer);
					Sound.LogErrors();
				}
#endif
			}
		}

		/// <summary>
		/// The manually queued audio clip.
		/// </summary>
		private AudioClip queuedClip;

		/// <summary>
		/// Manually queues the given audio clip into the initial buffer.
		/// </summary>
		public void QueueClip(AudioClip clip) {
#if OPENGL
			queuedClip = clip;
			/*Sound.LogErrors();
			int buffer = alBuffer[0];
			short[] clipOut = convertToShort(clip.Audio);
			AL.BufferData(buffer, alFormat, clipOut, clipOut.Length * sizeof(short), SampleRate);
			Sound.LogErrors();
			AL.SourceQueueBuffer(alSource, buffer);
			Sound.LogErrors();*/
#endif
		}

		/// <summary>
		/// Queues the next audio clip in a streamed buffer.
		/// </summary>
		private void queueNextClip() {
#if OPENGL
			// Get the next audio clip.
			AudioClip clip;
			if(OnFreeBuffer != null) {
				clip = OnFreeBuffer();
			} else {
				clip = AudioClip.Empty;
			}

			// If we didn't get any audio data, then don't fill the buffer.
			if(clip == AudioClip.Empty)
				return;

			// Get the next free buffer.
			int buffer = AL.SourceUnqueueBuffer(Channel.alSource);

			short[] bufferOut = convertToShort(clip.Audio);

			// Hand it on off.
			Sound.LogErrors();
			AL.BufferData(buffer, alFormat, bufferOut, bufferOut.Length * sizeof(short), SampleRate);
			AL.SourceQueueBuffer(Channel.alSource, buffer);
			Sound.LogErrors();
#endif
		}

		/// <summary>
		/// Convert a float buffer to short for OpenAL.
		/// </summary>
		/// <remarks>
		/// (A very special thank you to Renaud Bedard for this trick.
		/// Without it, the sound gets lost in the static.)
		/// </remarks>
		/// <param name="buffer">The buffer samples to convert.</param>
		private short[] convertToShort(float[] buffer) {
			short[] bufferOut = new short[buffer.Length];

			for(int j = 0; j < buffer.Length; j++) {
				var temp = (int)(32767f * buffer[j]);
				if(temp > short.MaxValue) temp = short.MaxValue;
				else if(temp < short.MinValue) temp = short.MinValue;
				bufferOut[j] = (short)temp;
			}

			return bufferOut;
		}
		#endregion

		#region Control
		/// <summary>
		/// Gets whether or not the buffers are currently playing.
		/// </summary>
		public bool IsPlaying { get; private set; }

		/// <summary>
		/// Gets whether or not buffer playback has been paused.
		/// </summary>
		public bool IsPaused { get; private set; }

		/// <summary>
		/// Since the buffer is ordered to stop and start on a different thread than
		/// the one that controls it, we need to know if we were stopped and restarted
		/// in the middle of an update, in which case the control thread needs to
		/// request a new channel or else it will get stuck and play silence forever. :P
		/// </summary>
		private bool wasStopped;

		/// <summary>
		/// Whether or not to loop the audio.
		/// Don't enable this for streamed audio,
		/// or else it will loop the buffers instead of calling for new audio clips.
		/// </summary>
		/// <remarks>
		/// To end streaming audio, return a null AudioClip in your OnBufferFree method.
		/// </remarks>
		public bool IsLooped {
			get {
				return isLooped;
			}
			set {
				lock(bufferLock) {
					if(Channel != null)
						AL.Source(Channel.alSource, ALSourceb.Looping, value);
					isLooped = value;
				}
			}
		}
		private bool isLooped;

		/// <summary>
		/// Sets the buffer to play from its current location.
		/// </summary>
		/// <remarks>
		/// For non-streaming audio, consider using method RewindAndPlay to make sure the
		/// audio clip you queued always plays from the beginning.
		/// </remarks>
		public void Play() {
			// Sound is not allowed to play if the sound engine is not enabled.
			if(!Sound.IsEnabled) {
				return;
			}

			lock(bufferLock) {
				isFadingOut = false;

				// If we're already playing, just keep on keep'n on.
				if(IsPlaying && thread.IsAlive) {
					return;
				}

				IsPlaying = true;

				// If we're just paused, start things up again.
				if(IsPaused && Channel != null && thread.IsAlive) {
					IsPaused = false;
#if OPENGL
					Sound.LogErrors();
					AL.SourcePlay(Channel.alSource);
					Sound.LogErrors();
#endif
					return;
				}

				if(thread == null || !thread.IsAlive) {
					thread = new Thread(control);
					thread.Start();
				}
			}
		}

		/// <summary>
		/// Pauses the audio.
		/// The audio can be resumed later from its current position,
		/// but it will continue to hog the channel in the meantime.
		/// </summary>
		public void Pause() {
			IsPaused = true;
			IsPlaying = false;
#if OPENGL
			if(Channel != null) {
				Sound.LogErrors();
				AL.SourcePause(Channel.alSource);
				Sound.LogErrors();
			}
#endif
		}

		/// <summary>
		/// Stops the audio, if it's playing, and frees the audio channel it was using.
		/// </summary>
		public void Stop() {
			lock(bufferLock) {
#if OPENGL
				if(Channel != null) {
					Sound.LogErrors();
					AL.SourcePause(Channel.alSource);
					Sound.LogErrors();
				}
#endif

				// Let the control loop know it's free to exit.
				wasStopped = true;
				IsPlaying = false;
			}
		}

		/// <summary>
		/// Plays the buffer, making sure it starts from the beginning of the buffer
		/// if it's already playing.
		/// </summary>
		/// <remarks>
		/// This is really only useful for audio that only uses a single buffer.
		/// For streaming audio, you stop the buffer and restart the stream yourself.
		/// Calling this will only rewind to what's already in the buffers.
		/// </remarks>
		public void RewindAndPlay() {
			if(IsPlaying && Channel != null) {
				lock(bufferLock) {
#if OPENGL
					Sound.LogErrors();
					AL.SourceRewind(Channel.alSource);
					Sound.LogErrors();
					AL.SourcePlay(Channel.alSource);
					Sound.LogErrors();
#endif
				}
			} else {
				Play();
			}
		}
		#endregion

		#region Update Loop
		/// <summary>
		/// The control loop, that oversees the buffers in its own thread.
		/// </summary>
		private void control() {
			// We'll come back here if the buffer is restarted in the middle of stopping.
			do {
				// Get a free channel.
				lock(bufferLock) {
					Channel channel = Channel.AttachToFree(this);
					if(channel == null) {
						DebugLog.Failure("No more free audio channels for buffer playback.");
						return;
					}
				}

				// Queue the initial buffer(s).
				fillBuffers();

				lock(bufferLock) {
					// Set initial parameters.
#if OPENGL
					AL.Source(Channel.alSource, ALSourceb.Looping, IsLooped);
					AL.Source(Channel.alSource, ALSourcef.Gain, Volume);

					// Play the sound!
					Sound.LogErrors();
					AL.SourcePlay(Channel.alSource);
					Sound.LogErrors();
#endif

					// Clear our "stopped" state.
					wasStopped = false;
				}

				// Let's begin our work loop.
				// WE MUST REMEMBER NOT TO EVER INVOKE RETURN WHILE INSIDE THE LOOP!!!
				// Otherwise the channel will not be freed.
				while(!Core.IsShuttingDown && (IsPlaying || IsPaused) && !wasStopped) {
					lock(bufferLock) {
						if(this.OnFreeBuffer != null) {
#if OPENGL
							Sound.LogErrors();

							// Check if any buffers need to be updated.
							int buffersNeedingUpdate;
							AL.GetSource(Channel.alSource, ALGetSourcei.BuffersProcessed, out buffersNeedingUpdate);

							// If so, update them from the stream.
							while(buffersNeedingUpdate > 0) {
								queueNextClip();

								// One less buffer to worry about.
								buffersNeedingUpdate--;
							}

							Sound.LogErrors();
#endif
						}// else {
						// Check if we're at the end of the clip.
#if OPENGL
						if(AL.GetSourceState(Channel.alSource) == ALSourceState.Stopped)
							IsPlaying = false;
#endif
						//}

						// Tweak the volume, as needed.
						if(!Mather.Approx(volume, setVolume)) {
							volume -= fadeSpeed;
							if(Mather.Distance(volume, setVolume) <= fadeSpeed) {
								volume = setVolume;
							}
						}
#if OPENGL
						AL.Source(Channel.alSource, ALSourcef.Gain, volume);
#endif

						// Stop if we're fading out and the volume has hit zero.
						if(isFadingOut && Mather.Approx(volume, 0)) {
							Stop();
							DebugLog.Whisper("Audio buffer completed a fade out.");
						}
					}

					// Take a break.
					Thread.Sleep(TimeSpan.FromSeconds(1f / (float)lps));
				}

				// Okay, we're done. Free the channel.
				FreeChannel();

				// If we got started up again while stopping the buffer,
				// then go back to the beginning and get a new channel.
			} while(IsPlaying && !Core.IsShuttingDown);
		}
		#endregion
	}
}