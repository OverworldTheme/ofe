﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// A list of music or sound files, along with data used for playing them.
	/// This is the recommended way for getting audio into your program.
	/// </summary>
	public class Playlist {
		#region Class Constructor
		/// <summary>
		/// Creates a new, empty playlist.
		/// </summary>
		public Playlist() {
		}

		/// <summary>
		/// Creates a new playlist, using the given data file.
		/// </summary>
		/// <param name="filename">The name and relative location of the data file for this playlist, sans extension.</param>
		public Playlist(string filename) {
			Load(filename);
		}
		#endregion

		#region Playlist
		/// <summary>
		/// All the items in the playlist.
		/// </summary>
		private List<PlaylistItem> all { get; set; }

		/// <summary>
		/// Manually add a music track or sound to the playlist.
		/// </summary>
		/// <param name="item"></param>
		public void Add(PlaylistItem item) {

		}
		#endregion

		#region Loading
		/// <summary>
		/// Loads a playlist from the given data file.
		/// </summary>
		/// <param name="filename">The name and relative location of the data file, sans extension.</param>
		public void Load(string filename) {
			DataTree data = DataTree.LoadFrom(filename);
		}
		#endregion
	}
}
