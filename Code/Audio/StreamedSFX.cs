﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

#if OPENGL
using NVorbis;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Audio {
	public class StreamedSFX : ILoadable {
		#region Initialization
		/// <summary>
		/// Creates a new streamed audio clip that can be played by OFE.
		/// </summary>
		/// <param name="filename">The relative path and filename of the audio file to use,
		/// without the file extension.</param>
		public StreamedSFX (string filename) {
			buffer = new AudioBuffer(3);
			buffer.OnFreeBuffer = update;
			PathAndFilename = filename;
			isLoaded = false;
			IsLooped = false;
			onLoadedLock = new object();

			clipSender = new AudioClip(Mather.Floor((float)buffer.SampleRate * bufferLength));

			Grunt.Enqueue(this);
		}
		#endregion

		#region Death
		/// <summary>
		/// Whether or not this instance has already been disposed.
		/// </summary>
		protected bool disposed = false;

		/// <summary>
		/// Clears this audio from the engine.
		/// If the audio is playing, it will be stopped right away.
		/// </summary>
		public void Dispose() {
			Pause();
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Internal dispose method.
		/// </summary>
		/// <param name="dispose">Whether or not to dispose managed objects as well as non-managed.</param>
		protected void Dispose(bool dispose) {
			if(disposed) {
				// Already did this.
				return;
			}

			if(dispose) {
#if OPENGL
				if(vorbisReader != null) vorbisReader.Dispose();
				buffer.Dispose();
#endif
			}

			disposed = true;
		}

		/// <summary>
		/// Finalizer.
		/// </summary>
		~StreamedSFX() {
			Dispose(false);
		}
		#endregion

		#region File
		/// <summary>
		/// Whether or not the requested file failed to load, or did not exist.
		/// </summary>
		public bool FileWasBad {
			get {
				return fileWasBad;
			}
		}
		private bool fileWasBad;

		/// <summary>
		/// Whether or not the song has established its stream from the file.
		/// </summary>
		public bool IsLoaded {
			get {
				return isLoaded;
			}
		}
		private bool isLoaded;

		/// <summary>
		/// The audio format of this song.
		/// </summary>
		public AudioFormat AudioFormat {
			get {
				return audioFormat;
			}
		}
		private AudioFormat audioFormat;
		#endregion

		#region Buffers
		/// <summary>
		/// The audio buffers used by this song.
		/// </summary>
		private AudioBuffer buffer;

		/// <summary>
		/// How long, in seconds, each buffer should be.
		/// </summary>
		const float bufferLength = 1;

		/// <summary>
		/// Initializes or resets the audio buffers.
		/// </summary>
		private void resetBuffers() {
#if OPENGL
			// If the vorbis decoder doesn't exist, don't bother going any futher.
			// We'll set the buffers up once we have something to decode.
			if(vorbisReader == null) {
				return;
			}

			// Get info from the Vorbis decoder.			
			switch(vorbisReader.Channels) {
				case 1:
					audioFormat = AudioFormat.Mono16;
					break;
				case 2:
					audioFormat = AudioFormat.Stereo16;
					break;
				default:
					DebugLog.Failure("\"" + PathAndFilename + "\" contains unsupported audio data.");
					return;
			}

			// Do it to it.
			buffer.SetBuffers(vorbisReader.SampleRate, audioFormat);
			//buffer.FillBuffers();

			clipSender.Resize(Mather.Floor((float)buffer.SampleRate * bufferLength));
#endif
		}
		#endregion

		#region Player API
#if OPENGL
		/// <summary>
		/// The Vorbis reader to decode the .ogg file.
		/// </summary>
		private VorbisReader vorbisReader;
#elif XNA
#endif
		#endregion

		#region Loading
		/// <summary>
		/// The relative path and file name where this song was loaded from.
		/// </summary>
		public string PathAndFilename { get; set; }

		/// <summary>
		/// A lock used when checking or firing the OnLoaded event.
		/// </summary>
		private object onLoadedLock;

		/// <summary>
		/// Event fired by Grunt when this streamed sound effect is finished setting up.
		/// </summary>
		public EventHandler OnLoaded {
			get {
				lock(onLoadedLock) {
					return onLoaded;
				}
			}
			set {
				lock(onLoadedLock) {
					if(IsLoaded) {
						value.Invoke(this, EventArgs.Empty);
					} else {
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;

		/// <summary>
		/// A lock for the loading thread.
		/// </summary>
		private object threadLock = new object();

		/// <summary>
		/// Has the song open the file and establish its stream now, in the current thread.
		/// </summary>
		public void LoadNow() {
			if(disposed) {
				// If we were disposed before loading and now Grunt has called us, don't do anything.
				return;
			}

			// No errors yet!
			HadError = false;

			if(!string.IsNullOrEmpty(PathAndFilename)) {
				// Inform the log.
				DebugLog.Whisper("Attempting to load audio file " + PathAndFilename + "...");

				// If, for some reason, the song is actually playing, stop it.
				buffer.Stop();

				// Make sure the file exists.
				if(!File.Exists(Path.Combine(Grunt.FullPath, PathAndFilename) + ".ogg")) {
					DebugLog.Failure("The audio file " + PathAndFilename + ".ogg does not exist!");
					isLoaded = true;
					fileWasBad = true;
					return;
				}

				// Load away.
#if OPENGL
				lock(vorbisLock) {
					try {
						// Dispose of the old reader, if there is one.
						if(vorbisReader != null) vorbisReader.Dispose();

						// Create the vorbis decoder.
						vorbisReader = new VorbisReader(Path.Combine(Grunt.FullPath, PathAndFilename) + ".ogg");
						fileWasBad = false;
					} catch (Exception e) {
						// Failure.
						vorbisReader = null;
						DebugLog.Failure("Error on initial load of audio file " + PathAndFilename + ".ogg!");
						DebugLog.Failure("Error message: " + e.GetType() + " " + e.Message);
						HadError = true;
						fileWasBad = true;
						isLoaded = true;
						return;
					}
				}
#endif

				lock(threadLock) {
					// We're loaded!
					isLoaded = true;

					// Reset the buffers.
#if OPENGL
					try
					{
						vorbisReader.DecodedTime = queuedFrom;
					}
					catch
					{
						// Try, try again.
						try
						{
							vorbisReader.DecodedTime = queuedFrom;
						}
						catch
						{
							DebugLog.Failure("Audio file " + PathAndFilename + " could not assign queued time!");
							HadError = true;
						}
					}
#endif
					resetBuffers();

					// If we're queued, hit it!
					if(isQueued) {
						Play();
						isQueued = false;
					}
				}

				// Success!
				DebugLog.Whisper("Audio file " + PathAndFilename + ".ogg successfully loaded!");
			} else {
				// Tell the log about it.
				DebugLog.Whisper("A null or empty file name was requested for audio playback. No action taken.");
			}
		}
		#endregion

		#region Volume
		/// <summary>
		/// The volume to play this song at.
		/// </summary>
		public float Volume {
			get {
				return buffer.Volume;
			}
			set {
				buffer.Volume = value;
			}
		}

		/// <summary>
		/// Fade the volume to a given level over time.
		/// </summary>
		/// <param name="volume">The new volume to play at.</param>
		/// <param name="seconds">How long, in seconds, for the fade to last.</param>
		public void FadeTo(float volume, float seconds) {
			buffer.FadeTo(volume, seconds);
		}

		/// <summary>
		/// Has the audio fade out, if it is playing.
		/// </summary>
		public void FadeOut() {
			FadeOut(2);
		}

		/// <summary>
		/// Fade this song out, and then stop playing.
		/// </summary>
		/// <param name="seconds"></param>
		public void FadeOut(float seconds) {
			if(IsLoaded) {
				buffer.FadeOut(seconds);
			} else {
				Stop();
			}
		}
		#endregion

		#region Information
		/// <summary>
		/// Whether or not the audio is currently playing.
		/// </summary>
		public bool IsPlaying {
			get {
				return buffer.IsPlaying;
			}
		}

		/// <summary>
		/// Gets whether or not the audio is currently playing,
		/// or will begin playing as soon as it finishes loading.
		/// </summary>
		public bool IsPlayingOrQueued {
			get {
				if(IsPlaying || isQueued) return true; else return false;
			}
		}

		/// <summary>
		/// The current location in the song.
		/// </summary>
		public TimeSpan Location {
			get {
#if OPENGL
				if(vorbisReader != null) {
					return vorbisReader.DecodedTime;
				} else {
					return TimeSpan.Zero;
				}
#endif
			}
		}

		/// <summary>
		/// Gets whether or not the audio had any notable errors reading the file since loading.
		/// Note that this is concerning reading the audio,
		/// not whether or not the file existed or was too corrupt to even open.
		/// </summary>
		public bool HadError { get; private set; }
		#endregion

		#region Control
		/// <summary>
		/// Whether this audio loops and plays continuously.
		/// </summary>
		public bool IsLooped { get; set; }

		/// <summary>
		/// Where the audio will loop back to when it reaches the end.
		/// </summary>
		public TimeSpan LoopPoint { get; set; }

		/// <summary>
		/// Whether or not this audio is queued to begin playback as soon as it's finished loading.
		/// </summary>
		private bool isQueued = false;

		/// <summary>
		/// Where the audio is queued to play from.
		/// </summary>
		private TimeSpan queuedFrom;

		/// <summary>
		/// Plays the audio from its current location.
		/// </summary>
		public void Play() {
			lock(threadLock) {
				if(!isLoaded) {
					isQueued = true;
				} else {					
					buffer.Play();					
				}
			}
		}

		/// <summary>
		/// Plays the audio from the given location.
		/// </summary>
		/// <param name="position">The position to play from.</param>
		public void PlayFrom(TimeSpan position) {
			lock(threadLock) {
				DebugLog.Write("Audio queued to position " + position);

				if(!isLoaded) {
					isQueued = true;
					queuedFrom = position;
				} else {
#if OPENGL
					if(vorbisReader != null) {
						vorbisReader.DecodedTime = position;
					}
#endif
					buffer.ResetBuffers();
					Play();
				}
			}
		}

		/// <summary>
		/// Plays the audio from the beginning.
		/// </summary>
		public void PlayFromBeginning() {
			PlayFrom(TimeSpan.Zero);
		}

		/// <summary>
		/// Pauses the audio without affecting its current position.
		/// The audio can be resumed later using method Play.
		/// </summary>
		public void Pause() {
			lock(threadLock) {
				isQueued = false;
				if(isLoaded) {
					buffer.Pause();
				}
			}
		}

		/// <summary>
		/// Stops the audio and returns to the beginning of the file.
		/// </summary>
		public void Stop() {
			lock(threadLock) {
				if(!isLoaded) {
					isQueued = false;
					buffer.Stop();
				} else {
#if OPENGL
					if(vorbisReader != null) {
						buffer.Stop();
						try {
							vorbisReader.DecodedTime = TimeSpan.Zero;
						} catch {
							DebugLog.Failure("Audio file " + PathAndFilename + " could not reset time!");
							HadError = true;
						}
						buffer.ResetBuffers();						
					}
#endif
				}
			}
		}
		#endregion

		#region Update Cycle
		/// <summary>
		/// A lock object, to ensure only one thread ever calls any vorbis reader object.
		/// This is due to unknown errors regarding thread safety, presumably in the current NVorbis library.
		/// </summary>
		private static object vorbisLock = new object();

		/// <summary>
		/// We'll reuse the same audio clip object,
		/// so that we're not generating garbage every frame.
		/// </summary>
		private AudioClip clipSender;

		/// <summary>
		/// Updates the audio when called by the buffer to do so.
		/// </summary>
		private AudioClip update() {
			// Is the file bad? Then don't try playing it!
			if(fileWasBad) {
				return AudioClip.Empty;
			}

			// If there's no stream, return an empty clip.
#if OPENGL
			if(vorbisReader == null)
				return AudioClip.Empty;
#endif

			// Get an empty clip ready.
			AudioClip reader = clipSender;

#if OPENGL
			// Read from the stream.
			int samplesRead;
			lock(vorbisLock) {
				try {
					samplesRead = vorbisReader.ReadSamples(reader.Audio, 0, Mather.Floor((float)buffer.SampleRate * bufferLength));
				} catch {
					DebugLog.Failure("Audio stream reader for " + PathAndFilename + " threw an exception!");
					HadError = true;
					return AudioClip.Empty;
				}
			}

			// If we got zero samples, try rewinding.
			// (Looping skips on Mac otherwise.)
			if(samplesRead == 0 && IsLooped) {
				lock(vorbisLock) {
					vorbisReader.DecodedTime = LoopPoint;
					try {
						samplesRead = vorbisReader.ReadSamples(reader.Audio, 0, (int)reader.Length);
					} catch {
						DebugLog.Failure("Audio stream reader for " + PathAndFilename + " threw an exception!");
						HadError = true;
						return AudioClip.Empty;
					}
				}
			}
			
			// If we're at the end of the file, only the remaining samples would have been written,
			// so we may have less samples than the length of the buffer. In this case, just
			// copy to a shorter buffer so that there's not an odd silence every time the song loops.
			if(samplesRead < reader.Length) {
				AudioClip shorter = new AudioClip(samplesRead);
				shorter.CopyFrom(reader);
				reader = shorter;

				// While we're at it, go ahead and rewind to the loop point if we're supposed to.
				if(IsLooped) {
					vorbisReader.DecodedTime = LoopPoint;
				} else {
					return AudioClip.Empty;
				}
			}
#endif

			// Queue.
			return reader;
		}
		#endregion

		#region Events
		#endregion
	}
}
