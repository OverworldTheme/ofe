﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

#if OPENGL
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// A sound effect.
	/// </summary>
	public class SFX : ILoadable {
		#region Class Construction
		/// <summary>
		/// Creates a new sound effect.
		/// The sound effect will not be played until ordered to do so.
		/// </summary>
		/// <param name="soundscape"></param>
		/// <param name="file"></param>
		public SFX(Soundscape soundscape, string file) {
			isLoaded = false;
			onLoadedLock = new object();
			threadLock = new object();
			buffer = new AudioBuffer(1);
			clip = AudioClip.Empty;

			SetSoundscape(soundscape);
			FromFile(file);
		}
		#endregion

		#region Death
		/// <summary>
		/// Whether or not this sound effect has been disposed.
		/// </summary>
		private bool isDisposed;

		/// <summary>
		/// Finalizer.
		/// </summary>
		~SFX() {
			if(!isDisposed)
				dispose(false);
		}

		/// <summary>
		/// Removes this sound effect from memory.
		/// It will be stopped first, if it is playing.
		/// </summary>
		public void Dispose() {
			dispose(true);
		}

		/// <summary>
		/// Disposes the sound effect from memory.
		/// </summary>
		/// <param name="disposeManaged">Whether or not to clear managed resources as well as non-managed.</param>
		private void dispose(bool disposeManaged) {
			if(isDisposed)
				return;
			else {
				if(disposeManaged) {
					Stop();

					if(Soundscape != null) {
						Soundscape.RemoveSound(this);
						soundscape = null;
					}

					buffer.Dispose();
				}

				isDisposed = true;
				GC.SuppressFinalize(this);
			}
		}		
		#endregion

		#region Audio Data
		/// <summary>
		/// The buffers used to play this clip.
		/// </summary>
		private AudioBuffer buffer;

		/// <summary>
		/// The raw audio data from the file.
		/// </summary>
		private AudioClip clip;
		#endregion

		#region Soundscape
		/// <summary>
		/// The soundscape this sound effect will play in.
		/// </summary>
		public Soundscape Soundscape {
			get {
				return soundscape;
			}
		}
		private Soundscape soundscape;

		/// <summary>
		/// Changes the soundscape that this sound effect is a part of.
		/// </summary>
		/// <param name="soundscape">The soundscape to set this sound effect in.</param>
		public void SetSoundscape(Soundscape soundscape) {
			// Out with the old.
			if(this.soundscape != null) {
				this.soundscape.RemoveSound(this);
			}

			// In with the new.
			if(soundscape != null) {
				this.soundscape = soundscape;
				this.soundscape.AddSound(this);
			}
		}
		#endregion

		#region File Information
		/// <summary>
		/// The complete path and filename of the audio file this sound effect is using.
		/// </summary>
		public string PathAndFilename { get; set; }

		/// <summary>
		/// Gets whether or not this sound effect has finished loaded.
		/// </summary>
		public bool IsLoaded {
			get {
				return isLoaded;
			}
		}
		private bool isLoaded;

		/// <summary>
		/// Whether or not the requested file failed to load, or did not exist.
		/// </summary>
		public bool FileWasBad {
			get {
				return fileWasBad;
			}
		}
		private bool fileWasBad;

		/// <summary>
		/// The number of channels in the audio data.
		/// </summary>
		private int channels;

		/// <summary>
		/// How many bits per sample in the audio data.
		/// </summary>
		private int bitsPerSample;

		/// <summary>
		/// The audio data's sample rate.
		/// </summary>
		private int sampleRate;
		#endregion

		#region Threading
		/// <summary>
		/// An object used to lock the thread.
		/// </summary>
		public object threadLock;
		#endregion

		#region Loading
		/// <summary>
		/// A list of valid audio file extensions.
		/// </summary>
		/// <remarks>When using the XNA version of OFE, this list is moot because all audio is stored in an XACT file.
		/// 
		/// The extensions are listed in the order of preference. For example, .wav is listed
		/// before .ogg, and therefore if there is both a .wav and an .ogg with the same file name, the .wav will be loaded, not the .ogg.</remarks>
		public static readonly List<string> ValidExtensions = new List<string> { @".wav", @".riff", @".ogg" };

		/// <summary>
		/// Locking object used for the OnLoaded event.
		/// </summary>
		private object onLoadedLock;

		/// <summary>
		/// Event fired by Grunt when this sound effect is finished loading.
		/// </summary>
		public EventHandler OnLoaded {
			get {
				lock(onLoadedLock) {
					return onLoaded;
				}
			}
			set {
				lock(onLoadedLock) {
					if(IsLoaded) {
						value.Invoke(this, EventArgs.Empty);
					} else {
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;

		/// <summary>
		/// Loads the given file.
		/// </summary>
		/// <param name="file">The audio file to load.</param>
		public void FromFile(string file) {
			isLoaded = false;
			PathAndFilename = file;

			// Tell the log what we're doing.
			DebugLog.Whisper("A sound effect has requested file \"" + file + "\".");

			Grunt.Enqueue(this);
		}

		/// <summary>
		/// Loads the file immediantly, in the current thread.
		/// </summary>
		public void LoadNow() {
			// If we were disposed before loading, don't bother.
			if(isDisposed)
				return;

			DebugLog.Whisper("Grunt has begun loading audio file \"" + PathAndFilename + "\"...");

			// Stop the buffer if for some reason it's already playing.
			buffer.Stop();

			// Make sure the file exists.
			if(!File.Exists(Path.Combine(Grunt.FullPath, PathAndFilename) + ".wav")) {
				fileWasBad = true;
				DebugLog.Failure("Grunt could not find audio file \"" + PathAndFilename + "\".");
			} else {
				// Open the file for reading.
				Stream stream = File.Open(Path.Combine(Grunt.FullPath, PathAndFilename) + ".wav", FileMode.Open);

				// Make sure it worked.
				if(stream == null) {
					fileWasBad = true;
					DebugLog.Failure("Grunt could not open audio file \"" + PathAndFilename + "\".");
				} else {
					// Read the wave.
					readWave(stream);
				}

				stream.Close();
			}

			lock(threadLock) {
				// Done.
				isLoaded = true;

				// Play now, if queued.
				if(isQueued) {
					isQueued = false;

					if(IsLooped)
						PlayLooped();
					else
						Play();
				}
			}
		}

		/// <summary>
		/// Reads raw wave/riff files and loads them into this sound effect's data.
		/// </summary>
		private void readWave(Stream stream) {
			using(BinaryReader reader = new BinaryReader(stream)) {
				// Check the header signature.
				string signature = new string(reader.ReadChars(4));
				if(signature != "RIFF") {
					DebugLog.Failure("\"" + PathAndFilename + "\" is not a valid wave file.");
					return;
				}

				// Let's get the chunk size.
				int riffChunkSize = reader.ReadInt32();

				// Check the format and format signature.
				signature = new string(reader.ReadChars(4));
				if(signature != "WAVE") {
					DebugLog.Failure("\"" + PathAndFilename + "\" is not a valid wave file.");
					return;
				}
				signature = new string(reader.ReadChars(4));
				if(signature != "fmt ") {
					DebugLog.Failure("\"" + PathAndFilename + "\" is not a valid wave file.");
					return;
				}

				// Let's finish out the header info.
				int formatChunkSize = reader.ReadInt32();
				int audioFormat = reader.ReadInt16();
				channels = reader.ReadInt16();
				sampleRate = reader.ReadInt32();
				int byteRate = reader.ReadInt32();
				int blockAlign = reader.ReadInt16();
				bitsPerSample = reader.ReadInt16();

				// Check for the data signature.
				signature = new string(reader.ReadChars(4));
				if(signature != "data") {
					DebugLog.Failure("\"" + PathAndFilename + "\" is not a valid wave file.");
					return;
				}

				// Now, on to the actual audio data!
				// Let's see how big the chunk is that contains the audio data.
				int dataChunkSize = reader.ReadInt32();

				// Load the entire data chunk.
				byte[] data;
				try {
					data = reader.ReadBytes(dataChunkSize);
				} catch {
					DebugLog.Failure("Wave data was smaller than reported.");
					// Some information is missing, or else the file is corrupt because there was less data than reported.
					// Assume the rest of the file is audio and just run with it.
					data = reader.ReadBytes((int)reader.BaseStream.Length);
				}

				AudioFormat format;
				switch(channels) {
					case 1:
						format = bitsPerSample == 8 ? AudioFormat.Mono8 : AudioFormat.Mono16;
						break;
					case 2:
						format = bitsPerSample == 8 ? AudioFormat.Stereo8 : AudioFormat.Stereo16;
						break;
					default:
						DebugLog.Failure("\"" + PathAndFilename + "\" contains unsupported wave data.");
						return;
				}

				// Now store the information in the buffer.
				if(bitsPerSample == 8) {
					clip = new AudioClip(data.Length);
					for(int i = 0; i < clip.Length; i++) {
						// Convert each byte to OFE's float audio format (-1 to 1) by dividing by the max Byte value.
						clip.Audio[i] = (data[i] - 127) / 255f * 2;
					}
				} else if(bitsPerSample == 16) {
					clip = new AudioClip(data.Length / 2);
					for(int i = 0; i < clip.Length; i++) {
						// Convert each pair of bytes to a 16-bit integer,
						// then convert to OFE's float audio format (-1 to 1) by dividing by the max Int16 value.
						clip.Audio[i] = BitConverter.ToInt16(data, i * 2) / (float)Int16.MaxValue;
						if(i == clip.Length - 1) {
							float test = clip.Audio[i];
							byte byte1 = data[i * 2];
							byte byte2 = data[i * 2 + 1];
						}
					}
				}

				// Let's convert mono effects to stereo.
				// (This is a workaround because of some OpenAL issues.)
				if(format == AudioFormat.Mono8 || format == AudioFormat.Mono16) {
					AudioClip old = clip;
					clip = new AudioClip(old.Length * 2);

					for(int i = 0; i < old.Length; i++) {
						clip[i * 2] = old[i];
						clip[i * 2 + 1] = old[i];
					}

					if(format == AudioFormat.Mono8)
						format = AudioFormat.Stereo8;
					else if(format == AudioFormat.Mono16)
						format = AudioFormat.Stereo16;
				}

				buffer.SetBuffers(sampleRate, AudioFormat.Stereo16);
				buffer.QueueClip(clip);
			}

			// Success!
			DebugLog.Success("Grunt successfully loaded audio file \"" + PathAndFilename + "\".");
		}
		#endregion

		#region Volume
		/// <summary>
		/// The volume that the clip is playing at.
		/// </summary>
		public float Volume {
			get {
				return buffer.Volume;
			}
			set {
				buffer.Volume = value;
			}
		}

		/// <summary>
		/// Has this sound effect fade to the given volume over the given amount of time.
		/// </summary>
		/// <param name="volume"></param>
		/// <param name="seconds"></param>
		public void FadeTo(float volume, float seconds) {
			buffer.FadeTo(volume, seconds);
		}

		/// <summary>
		/// Has this sound effect fade out, and then stop.
		/// </summary>
		/// <param name="seconds">The number of seconds for the fade to last.</param>
		public void FadeOut(float seconds) {
			if(isLoaded) {
				buffer.FadeOut(seconds);
			} else {
				Stop();
			}
		}
		#endregion

		#region Control
		/// <summary>
		/// Whether or not this sound effect is currently being played.
		/// </summary>
		public bool IsPlaying {
			get {
				return buffer.IsPlaying;
			}
		}

		/// <summary>
		/// Gets whether or not this sound effect is currently playing,
		/// or will begin playing as soon as it finishes loading.
		/// </summary>
		public bool IsPlayingOrQueued {
			get {
				if(IsPlaying || isQueued) return true; else return false;
			}
		}

		/// <summary>
		/// Gets whether or not this sound effect is being looped.
		/// </summary>
		public bool IsLooped {
			get {
				return buffer.IsLooped;
			}
		}

		/// <summary>
		/// Whether or not this sound effect has been requested to play once it finishes loading.
		/// </summary>
		private bool isQueued;

		/// <summary>
		/// Plays this sound effect.
		/// </summary>
		public void Play() {
			lock(threadLock) {
				buffer.IsLooped = false;

				if(!IsLoaded) {
					isQueued = true;
				} else {
					buffer.RewindAndPlay();
				}
			}
		}

		/// <summary>
		/// Plays this sound effect indefinitely.
		/// </summary>
		public void PlayLooped() {
			lock(threadLock) {
				buffer.IsLooped = true;

				if(!IsLoaded) {
					isQueued = true;
					return;
				} else {
					buffer.RewindAndPlay();
				}
			}
		}

		/// <summary>
		/// Halts playback of this sound effect.
		/// </summary>
		public void Stop() {
			lock(threadLock) {
				if(!IsLoaded) {
					isQueued = false;
					return;
				} else {
					buffer.Stop();
				}
			}
		}
		#endregion

		#region Updating
		/// <summary>
		/// Called when the buffer needs to be added to.
		/// </summary>
		private AudioClip update() {
			return clip;
		}
		#endregion		
	}
}
