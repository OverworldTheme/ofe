﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

#if OPENGL
using OpenTK.Audio.OpenAL;
using NVorbis;
#elif XNA
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Audio;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.GamerServices;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Media;
    using Microsoft.Xna.Framework.Net;
    using Microsoft.Xna.Framework.Storage;
#endif

namespace VisionRiders.OFE.Audio {
  public static class Music {
		#region Initialization
		/// <summary>
		/// Initializes the music components.
		/// </summary>
		public static void Init() {
#if OPENGL
			
#endif
			Songs = new List<Song>(5);
		}
		#endregion

		#region Settings
		/// <summary>
		/// The global volume setting for music.
		/// </summary>
		public static float Volume { get; set; }
		#endregion

		#region Player
#if OPENGL
#endif
		#endregion

		#region Songs
		/// <summary>
		/// The songs currently in use by the engine.
		/// </summary>
		internal static List<Song> Songs { get; set; }
		#endregion

		/// <summary>
		/// Whether or not music is allowed to be player.
		/// </summary>
    public static bool IsEnabled = true;

#if XNA
		/// <summary>
		/// XACT cue data.
		/// </summary>
    private static Cue xnaCue;
#endif

		/// <summary>
    /// Pause all currently playing songs.
    /// </summary>
    public static void Pause() {
      if(Music.IsEnabled) {
#if OPENGL
				
#elif XNA
        if(xnaCue != null) {
          xnaCue.Pause();
        }
#endif
      }
    }

    /// <summary>
    /// Play a given music file.
    /// </summary>
    /// <param name="filename">The file name of the song to play.</param>
    /// <param name="bLoop">Whether or not the music should loop.</param>
    public static void Play(string filename) {
      // Make sure any music already playing is stopped.
      //Music.Stop();
#if OPENGL
			//AudioClip glClip = new AudioClip(tSong);
			//glClip.Play();

			Song song = new Song(filename);
			song.Play();
#elif XNA
      // Load and play the music file.
      try {
        xnaCue = Sound.xnaSoundBank.GetCue(tSong);
      } catch {
        xnaCue = null;
      }

      if(IsEnabled && xnaCue != null) {
        xnaCue.Play();
      }
#endif

      // Set looping properties.
      /*if(bLoop) {
          MediaPlayer.IsRepeating = true;
      } else {
          MediaPlayer.IsRepeating = false;
      }*/
    }

    /// <summary>
    /// If there is a song already loaded that has been paused, this routine will allow it
    /// to start playing again.
    /// </summary>
    public static void Resume() {
#if XNA
      if(IsEnabled && xnaCue != null) {
        try {
          xnaCue.Resume();
        } catch {
          xnaCue = null;
        }
      }
#endif
    }

    /// <summary>
    /// Stops any music that is playing and clears it from memory. Call this when you no longer
    /// need the song, otherwise call the pause function.
    /// </summary>
    public static void StopAll() {
#if OPENGL
			foreach(Song song in Songs) {
				song.Stop();
			}
#elif XNA
      if(xnaCue != null) {
        xnaCue.Stop(AudioStopOptions.AsAuthored);
      }
#endif
    }

		/// <summary>
		/// Checks the volume and all the songs every frame.
		/// </summary>
    public static void Update() {
			foreach(Song song in Songs) {
				//song.Update();
			}

#if OPENGL
#elif XNA
      // Check volume and change as necessary.
      if(!Mather.Approx(Volume, currentVolume) && xnaCue != null) {
        currentVolume = Volume;
        xnaCue.SetVariable("Volume", Volume);
      }
#endif
    }
  }
}

