﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// Information for finding a single music track or sound effect for use in the game.
	/// This data will be used to load and play the file from within a playlist.
	/// </summary>
	/// <remarks>
	/// <para>You can add playlist items manually, but the playlist is designed to be loaded from a data file on startup.</para>
	/// <para>Remember that each playlist item only contains the information for finding and playing the file.
	/// It doesn't contain the actual PCM or streaming data.
	/// That's handled by the Song and SFX classes, which will refer to each PlaylistItem as needed.</para>
	/// </remarks>
	public class PlaylistItem {
		/// <summary>
		/// The name and relative location, sans extension, of the audio file.
		/// </summary>
		public string Filename { get; set; }

		/// <summary>
		/// This item's playlist index.
		/// </summary>
		/// <remarks>
		/// This is useful for sorting tracks and audio in the playlist, but does affect playback in any way.
		/// </remarks>
		public int Index { get; set; }

		/// <summary>
		/// The relative volume to treat as "normal" for this audio file.
		/// A value of 1 will play it at the file's recorded volume.
		/// </summary>
		public float Volume { get; set; }
	}
}
