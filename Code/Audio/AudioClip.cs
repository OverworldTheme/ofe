﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Audio {
	/// <summary>
	/// A section of raw audio data that can be sent out to a sound buffer.
	/// </summary>
	/// <remarks>This is only the raw audio data.
	/// If you're just wanting to play a sound effect, you want the SFX class.</remarks>
	public class AudioClip {
		#region Class Constructor
		/// <summary>
		/// Creates a new, empty audio clip
		/// </summary>
		/// <param name="length">How long, in samples, this clip should be.</param>
		public AudioClip(long length) {
			Audio = new float[length];
		}
		#endregion

		#region Static Members
		/// <summary>
		/// An empty audio clip with no data.
		/// </summary>
		public static readonly AudioClip Empty = new AudioClip(0);
		#endregion

		#region Data
		/// <summary>
		/// The audio data contained by this clip.
		/// </summary>
		public float[] Audio;

		/// <summary>
		/// An accessor for the audio data contained by this clip.
		/// </summary>
		/// <param name="index">The index in the Audio array.</param>
		public float this[int index] {
			get { return Audio[index]; }
			set { Audio[index] = value; }
		}

		/// <summary>
		/// The length of this clip, in samples.
		/// </summary>
		public long Length {
			get {
				return Audio.Length;
			}
		}

		/// <summary>
		/// Sets the audio clip's length to a new value.
		/// If the new length is shorter than the current clip,
		/// the audio data at the end will be truncated.
		/// </summary>
		/// <param name="length">The new length, in samples.</param>
		public void Resize(long length)
		{
			float[] newContents = new float[length];
			for (int i = 0; i < length && i < Audio.Length; i++)
			{
				newContents[i] = Audio[i];
			}
			Audio = newContents;
		}

		/// <summary>
		/// The length of this clip at the given sample rate.
		/// </summary>
		/// <param name="sampleRate">The sample rate to interpret the raw audio data at.</param>
		public TimeSpan LengthAtRate(int sampleRate) {
			return TimeSpan.FromSeconds(Length / sampleRate);
		}
		#endregion

		#region Operations
		/// <summary>
		/// Copies the audio from another clip.
		/// Will not copy more samples than the length of this clip.
		/// </summary>
		/// <param name="clip">The clip to copy from.</param>
		public void CopyFrom(AudioClip clip) {
			for(int i = 0; i < Length; i++) {
				if(i > clip.Length) {
					break;
				}

				this.Audio[i] = clip.Audio[i];
			}
		}

		/// <summary>
		/// Copies the audio in this clip to another.
		/// Will not copy more samples than the length of that clip.
		/// </summary>
		/// <param name="clip">The clip to copy to.</param>
		public void CopyTo(AudioClip clip) {
			clip.CopyFrom(this);
		}
		#endregion
	}
}
