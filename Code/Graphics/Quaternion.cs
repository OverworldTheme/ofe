﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A four-dimensional vector used primarily in OFE to describe a rotation in 3D space.
  /// </summary>
  /// <remarks>In the OFE Engine, quaternions are expressed as (x, y, z, w).</remarks>
  public struct Quaternion
	{
		#region Data
		/// <summary>
    /// The x-value of this quaternion.
    /// </summary>
    public float X;

    /// <summary>
    /// The y-value of this quaternion.
    /// </summary>
    public float Y;

    /// <summary>
    /// The z-value of this quaternion.
    /// </summary>
    public float Z;

		/// <summary>
		/// The w-value of this quaternion.
		/// </summary>
		public float W;
		#endregion

		#region Constructors
		public Quaternion(float x, float y, float z, float w) {
      this.W = w;
      this.X = x;
      this.Y = y;
      this.Z = z;
    }

		/// <summary>
		/// Creates a quaternion from the given yaw-pitch-roll values.
		/// Rotation will be applied in the following order: yaw, then pitch, then roll.
		/// </summary>
		/// <param name="yaw"></param>
		/// <param name="pitch"></param>
		/// <param name="roll"></param>
		/// <returns></returns>
		public static Quaternion FromYawPitchRoll(float yaw, float pitch, float roll)
		{
			float cy = Mather.Cos(yaw * 0.5f);
			float cp = Mather.Cos(pitch * 0.5f);
			float cr = Mather.Cos(roll * 0.5f);
			float sy = Mather.Sin(yaw * 0.5f);
			float sp = Mather.Sin(pitch * 0.5f);
			float sr = Mather.Sin(roll * 0.5f);

			return new Quaternion(
				(cy * cp * sr) + (sy * sp * cr),
				(cy * sp * cr) - (sy * cp * sr),
				(sy * cp * cr) + (cy * sp * sr),				
				(cy * cp * cr) - (sy * sp * sr));
		}

		/// <summary>
		/// Creates a quaternion from the given axis angle.
		/// </summary>
		/// <param name="x">The unit vector.</param>
		/// <param name="rotation">Rotation, in revolutions.</param>
		/// <returns></returns>
		public static Quaternion FromAxisAngle(Vector v, float rotation)
		{
			v = v.Normalized();

			Quaternion q;

			// Calculate sine once.
			float s = Mather.Sin(rotation / 2);

			// Calculate x, y, z of the quaternion.
			q.X = v.X * s;
			q.Y = v.Y * s;
			q.Z = v.Z * s;

			// Calulate w, then normalize.
			q.W = Mather.Cos(rotation / 2);
			return q.Normalized();
		}

		/// <summary>
		/// Creates a quaternion from the given axis angle.
		/// </summary>
		/// <param name="x">X axis amount.</param>
		/// <param name="y">Y axis amount.</param>
		/// <param name="z">Z axis amount.</param>
		/// <param name="rotation">Rotation, in revolutions.</param>
		/// <returns></returns>
		public static Quaternion FromAxisAngle(float x, float y, float z, float rotation)
		{
			return FromAxisAngle(new Vector(x, y, z), rotation);
		}

		/// <summary>
		/// Creates a quaternion representing the rotation between two vectors.
		/// </summary>
		/// <param name="v1">The first vector.</param>
		/// <param name="v2">The second vector.</param>
		/// <returns></returns>
		public static Quaternion FromTwoVectors(Vector v1, Vector v2)
		{
			float cosTheta = Vector.Dot(Vector.Normalize(v1), Vector.Normalize(v2));
			float angle = Mather.Acos(cosTheta);
			Vector axis = Vector.Normalize(Vector.Cross(v1, v2));
			return Quaternion.FromAxisAngle(axis, angle);
		}
		#endregion

		#region Overloads
		public override string ToString()
		{
			return "{" + X + ", " + Y + ", " + Z + ", " + W + "}";
		}
		#endregion

		#region Operators
		public static Quaternion operator +(Quaternion q1, Quaternion q2)
		{
			return new Quaternion(
				q1.X + q2.X,
				q1.Y + q2.Y,
				q1.Z + q2.Z,
				q1.W + q2.W);		
		}

		public static Quaternion operator -(Quaternion q1, Quaternion q2)
		{
			return new Quaternion(
				q1.X - q2.X,
				q1.Y - q2.Y,
				q1.Z - q2.Z,
				q1.W - q2.W);
		}

		public static Quaternion operator *(Quaternion q1, Quaternion q2) {
			return new Quaternion(
				(q1.W * q2.X) + (q1.X * q2.W) + (q1.Y * q2.Z) - (q1.Z * q2.Y),
				(q1.W * q2.Y) - (q1.X * q2.Z) + (q1.Y * q2.W) + (q1.Z * q2.X),
				(q1.W * q2.Z) + (q1.X * q2.Y) - (q1.Y * q2.X) + (q1.Z * q2.W),				
				(q1.W * q2.W) - (q1.X * q2.X) - (q1.Y * q2.Y) - (q1.Z * q2.Z));
		}

		public static Quaternion operator /(Quaternion q, float scalar)
		{
			if (scalar == 0) throw new DivideByZeroException("Quaternions cannot be divided by zero.");
			else return new Quaternion(q.X / scalar, q.Y / scalar, q.Z / scalar, q.W / scalar);
		}
		#endregion

		#region Functions
		public Quaternion Conjugate()
		{
			return new Quaternion(-X, -Y, -Z, W);
		}

		/// <summary>
		/// Calculates the inverse of this quaternion.
		/// </summary>
		public Quaternion Inverse() {
			return Conjugate() / Magnitude();
		}

		/// <summary>
    /// Gets the length of this quaternion.
    /// </summary>
    public float Length {
			get { return (X * X) + (Y * Y) + (Z * Z) + (W * W); }
    }

		/// <summary>
		/// Calculates the magnitude (norm) of the quaternion.
		/// </summary>
		public float Magnitude()
		{
			return Mather.Sqrt(Length);
		}

    /// <summary>
    /// Normalizes this quaternion.
    /// </summary>
    public Quaternion Normalized() {
			Quaternion q = this;

      float magnitute = Magnitude();
      q.W /= magnitute;
      q.X /= magnitute;
      q.Y /= magnitute;
      q.Z /= magnitute;

			return q;
    }
		#endregion

		#region Identity
		/// <summary>
    /// A quaternion vector that represents the identity quaternion.
    /// </summary>
    public static readonly Quaternion Identity = new Quaternion(0, 0, 0, 1);
		#endregion
	}
}
