﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using VisionRiders.OFE.Data;
using VisionRiders.OFE.Graphics.OFE2D;

#if OPENGL
using OpenGL;
#endif

#if SDL
using SDL2;
#endif

namespace VisionRiders.OFE.Graphics
{
	/// <summary>
	/// The window or screen where the engine displays graphics to.
	/// </summary>
	public static class Display
	{
		#region Initialization
		/// <summary>
		/// Initializes graphics and display data.
		/// </summary>
		internal static void Init()
		{
			// Tell the log what we're about to do.
			DebugLog.Write("Initializing the Display...");

			// What is the system's screen resolution?
			DebugLog.Note("The screen resolution is " + Display.Resolution.X + "x" + Display.Resolution.Y + ".");

			// Make our window.
			DebugLog.Note("Creating a window...");
			if (windowTitle == null)
				windowTitle = Core.AppName;
#if SDL
			SDL_windowPointer = SDL.Window.Create(windowTitle, SDL.Window.PosCentered, SDL.Window.PosCentered, 640, 480,
				SDL.Window.Flag.AllowHighDPI | SDL.Window.Flag.OpenGL | SDL.Window.Flag.Hidden | SDL.Window.Flag.Resizable);

			// Did it work?
			if (SDL_windowPointer == null)
			{
				string sdlError = SDL.Error.Get();
				DebugLog.CriticalError("Could not create a window for the display!");
				DebugLog.CriticalError("Reported reason:");
				DebugLog.CriticalError(sdlError);
				throw new Exception("Could not create a window for the display. Reported reason: " + sdlError);
			}

			// Get an OpenGL context.
			IntPtr OGL_context = SDL.Window.GL.CreateContext(SDL_windowPointer);
			//SDL.Window.GL.MakeCurrent(SDL_windowPointer, OGL_context);
#endif

			// Make sure both width and height are set.
			if (Width == 0)
				SetSize(Height);

#if OPENGL
			// Find the function pointers for OpenGL.
			GL.InitLibrary();

			// Get the OpenGL version number.
			OGL_VersionString = GL.GetString(GlEnum.Name.Version);
			OGL_ShaderVersion = GL.GetString(GlEnum.Name.ShadingLanguageVersion);

			// Parse it to get the major and minor version numbers as actual integers.
			// (We have to do it this way because it's the only way to do it in earlier version of OpenGL.)
			string[] versionSplit = OGL_VersionString.Split('.');
			if (versionSplit.Length >= 2)
			{
				int version;
				Int32.TryParse(versionSplit[0], out version);
				OGL_VersionMajor = version;
				Int32.TryParse(versionSplit[1], out version);
				OGL_VersionMinor = version;
			}

			// Record the version number in the log.
			DebugLog.Note("OpenGL version is " + OGL_VersionString + ".");
			DebugLog.Note("GLSL version is " + OGL_ShaderVersion + ".");

			// Enable z-depth testing.
			GL.Enable(GlEnum.Capability.DepthTest);
			LogErrors();
#endif

			// Turn off VSync by default.
			IsVSyncEnabled = false;

			// Make sure the aspect ratio is correct.
			if (Width < 1)
			{
				SetViewportAspect(NativeAspectRatio);
			}

			// If we're in fullscreen mode, set the resolution to the current desktop resolution as
			// set by the user. Otherwise, make sure that the window won't be bigger than the desktop.
			if (IsFullScreen)
			{
				SetResolution((int)Resolution.X, (int)Resolution.Y);
			}
			else
			{
				fitOnScreen();
			}

			// Center the window on the screen.
			CenterWindow();

			// Log any otherwise uncaught errors during setup.
			LogErrors();

			// Record success.			
			DebugLog.Success("The Display was initialized!");
		}

		/// <summary>
		/// Shuts down the display.
		/// </summary>
		public static void Shutdown()
		{
			// Tell the debug log what we're doing.
			DebugLog.Write("Shutting down the display...");

			// Close out the window.
			DebugLog.Note("Closing the window...");
#if SDL
			if (SDL_windowPointer != null)
				SDL.Window.Destroy(SDL_windowPointer);
#endif
		}
		#endregion

		#region API
#if SDL
		/// <summary>
		/// A pointer to the SDL window.
		/// </summary>
		private static IntPtr SDL_windowPointer;
#endif

#if OPENGL
		/// <summary>
		/// The OpenGL version string provided by the GPU.
		/// </summary>
		internal static string OGL_VersionString { get; private set; }

		/// <summary>
		/// The OpenGL shader language version string provided by the GPU.
		/// </summary>
		internal static string OGL_ShaderVersion { get; private set; }

		/// <summary>
		/// The major version number of OpenGL in use.
		/// </summary>
		internal static int OGL_VersionMajor { get; private set; }

		/// <summary>
		/// The minor version number of OpenGL in use.
		/// </summary>
		internal static int OGL_VersionMinor { get; private set; }
#endif
		#endregion

		#region Window
		/// <summary>
		/// The title of this display window.
		/// </summary>
		public static string Title
		{
			get { return windowTitle; }
			set
			{
				windowTitle = value;
				if (!Core.IsShuttingDown)
				{
#if SDL
					SDL.Window.SetTitle(SDL_windowPointer, value);
#endif
				}
			}
		}

		static IntPtr sdlSurface;

		/// <summary>
		/// Sets the window icon.
		/// </summary>
		/// <param name="filename">The relative path and filename of the icon file.</param>
		public static void SetIcon(string filename)
		{
			// Whisper to the debug log.
			DebugLog.Whisper("Setting program window icon...");

			// Get our full directory.
			filename = Path.Combine(Core.Directory, filename);

			// Do we need to add an extension?
			if (!Path.HasExtension(filename))
			{
				foreach (string ext in Texture.ValidExtensions)
				{
					if (File.Exists(filename + ext))
					{
						filename += ext;
						break;
					}
				}
			}

			// Make sure the file actually exists.
			if (!File.Exists(filename))
			{
				DebugLog.Failure("Could not find the requested window icon file " + filename + "!");
				return;
			}

#if SDL
			// Load it on up.
			Bitmap bmp = new Bitmap(filename);

			try
			{
				// Windows icons must be resized.
				// Thanks, Bill Gates!
				if (SysInfo.Platform == OS.Windows)
				{
					Bitmap bmp2 = new Bitmap(bmp, 256, 256);
					bmp.Dispose();
					bmp = bmp2;
        }

				// Extract the bitmap data.
				BitmapData bmpData = bmp.LockBits(
					new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height),
					ImageLockMode.ReadOnly,
					bmp.PixelFormat);

				// Figure out the bit depth and format.
				int bitDepth = 16;
				uint rmask = 0x0f00;
				uint gmask = 0x00f0;
				uint bmask = 0x000f;
				uint amask = 0xf000;
				switch (bmpData.PixelFormat)
				{
					case PixelFormat.Format32bppArgb:
						bitDepth = 32;
						rmask = 0x00ff0000;
						gmask = 0x0000ff00;
						bmask = 0x000000ff;
						amask = 0xff000000;
						break;
				}

				// Transfer it to an SDL surface.
				//IntPtr sdlSurface;
				sdlSurface = SDL.Surface.CreateRgbSurfaceFrom(bmpData.Scan0, bmpData.Width, bmpData.Height, bitDepth, bmpData.Stride, rmask, gmask, bmask, amask);

				// Set it as the icon.
				SDL.Window.SetIcon(SDL_windowPointer, sdlSurface);

				// We no longer need the SDL surface.
				SDL.Surface.Free(sdlSurface);
      }
			catch
			{
				DebugLog.Failure("There was an error while trying to set the window icon!");
			}
			finally
			{
				if (bmp != null) bmp.Dispose();
			}
#endif
		}

		/// <summary>
		/// Private version of window title.
		/// Used in case the window is already closed, or is closing.
		/// Otherwise, we risk an access violation or other exception on some platforms.
		/// </summary>
		private static string windowTitle;

		/// <summary>
		/// Whether or not the display is in full screen mode.
		/// </summary>
		public static bool IsFullScreen = false;

		/// <summary>
		/// Centers the display window. While in fullscreen, obviously this has no effect.
		/// </summary>
		public static void CenterWindow()
		{
			if (!IsFullScreen)
			{
#if SDL
				SDL.Window.SetPosition(SDL_windowPointer,
					SDL.Window.PosCentered,
					SDL.Window.PosCentered);
#endif
			}
		}

		/// <summary>
		/// Make the display window visible if it is hidden, if possible.
		/// </summary>
		public static void Show()
		{
#if SDL
			SDL.Window.Show(SDL_windowPointer);
#endif
		}

		/// <summary>
		/// Hide the display window if it is visible, if possible.
		/// </summary>
		public static void Hide()
		{
#if SDL
			SDL.Window.Hide(SDL_windowPointer);
#endif
		}

		/// <summary>
		/// Sets the display's window size so that it fits on the screen without getting too close to,
		/// or going past, any of the screen's edges.
		/// </summary>
		private static void fitOnScreen()
		{
			int x = Width;
			int y = Height;
			if (x > Resolution.X * .95f || y > Resolution.Y * .95f)
			{
				x = (int)(Resolution.X * .85f);
				if (AspectRatioIsLocked)
					y = (int)((float)x / LockedAspectRatio);
				else
					y = (int)((float)x / NativeAspectRatio);
				SetSize(x, y);
			}
		}
		#endregion

		#region Viewport
		/// <summary>
		/// The scale of the display versus the viewport.
		/// At 1 both the display and viewport are the same resolution.
		/// </summary>
		public static Coord Scale
		{
			get
			{
				return new Coord((float)Width / (float)ActualWidth, (float)Height / (float)ActualHeight);
			}
		}

		/// <summary>
		/// Gets the width of this display's viewport.
		/// This may not be the same as the display's physical width in pixels.
		/// </summary>
		public static int Width { get; private set; }

		/// <summary>
		/// Gets the height of this display's viewport.
		/// This may not be the same as the display's physical height in pixels.
		/// </summary>
		public static int Height { get; private set; }

		/// <summary>
		/// Gets the physical width of the display on-screen, in pixels.
		/// </summary>
		public static int ActualWidth
		{
			get
			{
#if SDL
				int width;
				int height;
				SDL.Window.GetSize(SDL_windowPointer, out width, out height);
				return width;
#endif
			}
		}

		/// <summary>
		/// Gets the physical height of the display on-screen, in pixels.
		/// </summary>
		public static int ActualHeight
		{
			get
			{
#if SDL
				int width;
				int height;
				SDL.Window.GetSize(SDL_windowPointer, out width, out height);
				return height;
#endif
			}
		}

		/// <summary>
		/// The current aspect ratio of the display.
		/// </summary>
		public static float AspectRatio
		{
			get { return (float)ActualWidth / (float)ActualHeight; }
		}

		/// <summary>
		/// Gets whether or not the aspect ratio of the display window has been locked.
		/// Use LockAspectRatio to set this property.
		/// </summary>
		public static bool AspectRatioIsLocked { get; private set; }

		/// <summary>
		/// The aspect ratio that the display window has been locked to when not in fullscreen mode.
		/// </summary>
		internal static float LockedAspectRatio { get; private set; }

		/// <summary>
		/// Locks the aspect ratio of the display window.
		/// Even if resized by the user, the window will always be set this ratio.
		/// Note that this does not effect the ratio of the display when in fullscreen mode.
		/// </summary>
		/// <param name="ratio">The ratio of width to height.
		/// Old-style 4:3 standard defition is about 1.33f.
		/// 16:9 widescreen is about 1.77f.</param>
		public static void LockAspectRatio(float ratio)
		{
			AspectRatioIsLocked = true;
			LockedAspectRatio = ratio;

			// Resize the window to the locked aspect ratio.
			// Set resolution will do that for us.
			SetSize(ActualWidth, ActualHeight);
			fitOnScreen();
		}

		public static void UnlockAspectRatio()
		{
			AspectRatioIsLocked = false;
		}
		#endregion

		#region Relative Display Positions
		/// <summary>
		/// The y-coordinate of the top of the display.
		/// </summary>
		public static int Top
		{
			get { return 0; }
		}

		/// <summary>
		/// The y-coordinate of the bottom of the display.
		/// </summary>
		public static int Bottom
		{
			get { return ActualHeight; }
		}

		/// <summary>
		/// The x-coordinate of the left side of the display.
		/// </summary>
		public static int Left
		{
			get { return 0; }
		}

		/// <summary>
		/// The x-coordinate of the right side of the display.
		/// </summary>
		public static int Right
		{
			get { return ActualWidth; }
		}

		/// <summary>
		/// The coordinates giving the exact center of the screen.
		/// </summary>
		public static Coord Center { get { return new Coord(Width / 2, Height / 2); } }
		#endregion

		#region Backbuffer Settings
		/// <summary>
		/// The default color of the background when nothing is drawn over it.
		/// </summary>
		public static Color BackgroundColor = Color.Black;
		#endregion

		#region Title and Action Safe Properties
		/// <summary>
		/// The percentage on each side of the screen that text and other important elements should
		/// stay inside to ensure they aren't lost to overscan.
		/// </summary>
		/// <remarks>
		/// <para>By default this value is 10%.</para>
		/// <para>While not as bad as old tube-style television,
		/// modern HDTVs do have a small amount of overscan.
		/// You can safely lower this value now days,
		/// but be careful not to take it too far down or you risk text getting cut off.</para>
		/// <para>On personal computers and the like,
		/// monitors rarely have overscan so you can take the value way down without worry.</para>
		/// </remarks>
		public static int TitleSafePercentage
		{
			get { return titleSafePercentage; }
			set { titleSafePercentage = value; }
		}
		private static int titleSafePercentage = 10;

		/// <summary>
		/// The percentage on each side of the screen that important action should take place inside,
		/// so that it is not obscured by overscan.
		/// </summary>
		/// <remarks>
		/// By default this value is 5%.
		/// </remarks>
		public static int ActionSafePercentage
		{
			get { return actionSafePercentage; }
			set { actionSafePercentage = value; }
		}
		private static int actionSafePercentage = 5;
		#endregion

		#region System
		/// <summary>
		/// The native aspect ratio of the device this display is using.
		/// </summary>
		/// <remarks>
		/// If you change the fullscreen resolution,
		/// this value will be the current aspect ratio of the fullscreen resolution,
		/// which may not be the same as what the user has set.
		/// </remarks>
		public static float NativeAspectRatio
		{
			get { return (float)Resolution.X / (float)Resolution.Y; }
		}
		#endregion

		#region Basic Drawing
		/// <summary>
		/// Checks if a given imaginary rectangle would be visible on screen.
		/// </summary>
		/// <returns>True, if the given rectangle is within bounds of the display.</returns>
		public static bool IsOnScreen(float X1, float Y1, float X2, float Y2)
		{
			if (X1 < 0 && X2 < 0
					|| Y1 < 0 && Y2 < 0
					|| X1 > (Display.Width /*/ GFX.fScale*/) && X2 > (Display.Width /* / GFX.fScale*/)
					|| Y1 > (Display.Height /*/ GFX.fScale*/) && Y2 > (Display.Height /* / GFX.fScale*/))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		#endregion

		#region Screen and Screen Resolution
		/// <summary>
		/// The current display device's resolution setting.
		/// On most platforms, this is the desktop's resolution as set by the user.
		/// </summary>
		public static CoordInt Resolution
		{
			get
			{
#if SDL
				// Find out which display we're on.
				int display = SDL.Window.GetDisplayIndex(SDL_windowPointer);
				if (display < 0)
					display = 0;

				// Get the bounds.
				SDL.Rect rect;
				int error = SDL.Window.GetDisplayBounds(1, out rect);
				if (error < 0) return new CoordInt(0, 0);

				// Pass them back.
				return new CoordInt(rect.w, rect.h);
#endif
			}
		}

		/// <summary>
		/// Changes the fullscreen resolution, but not the viewport.
		/// </summary>
		/// <remarks>
		/// Only works while in fullscreen mode.
		/// </remarks>
		public static void SetResolution(int width, int height)
		{
			if (!IsFullScreen)
				return;

			// Set the window size, as needed.
#if SDL
			//SDL.Window.SetSize(SDL_windowPointer, width, height);
#endif

			// Set the resolution
#if SDL
			// Figure out the current display settings.
			SDL.Window.DisplayMode mode;
			int display = SDL.Window.GetDisplayIndex(SDL_windowPointer);
			if(display < 0)
			{
				DebugLog.Failure("Could not get the window's display index from SDL.");
				DebugLog.Failure("Reason given: " + SDL.Error.Get());
				return;
			}
			int error = SDL.Window.GetDisplayMode(display, 0, out mode);
			if(error < 0)
			{
				DebugLog.Failure("Could not get display mode information from SDL.");
				DebugLog.Failure("Reason given: " + SDL.Error.Get());
				return;
			}

			// Now substitute the resolution,
			// and apply the changes.
			mode.Width = width;
			mode.Height = height;
			SDL.Window.SetDisplayMode(SDL_windowPointer, mode);
#endif

			// Center stage origins, as needed.
			Stage.RecenterAll();
		}

		/// <summary>
		/// Changes the window size, but not the viewport or resolution.
		/// </summary>
		/// <param name="height">The height to set, in pixels.
		/// The width will be set to match the height,
		/// based on the screen's native aspect ratio.</param>
		public static void SetSize(int height)
		{
			SetSize(Mather.Floor(NativeAspectRatio * height), height);
		}

		/// <summary>
		/// Changes the window size, but not the viewport or resolution.
		/// </summary>
		/// <param name="width">The width to set, in pixels.</param>
		/// <param name="height">The height to set, in pixels</param>
		public static void SetSize(int width, int height)
		{
			// Clamp to our set aspect ratio, as needed.
			if (AspectRatioIsLocked && !IsFullScreen)
			{
				if (width > height)
				{
					height = Mather.Round(width / LockedAspectRatio);
				}
				else
				{
					width = Mather.Round(height * LockedAspectRatio);
				}
			}

			// Set the window size, regardless.
#if SDL
			SDL.Window.SetSize(SDL_windowPointer, width, height);
#endif

			// Center stage origins, as needed.
			Stage.RecenterAll();
		}

		/// <summary>
		/// Changes the size of the display's viewport, without changing the resolution of the screen or the size of the window.
		/// </summary>
		/// <param name="width">The new width to set.</param>
		/// <param name="height">The new height to set.</param>
		public static void SetViewport(int width, int height)
		{
			Width = width;
			Height = height;

			// Center stage origins, as needed.
			Stage.RecenterAll();
		}

		/// <summary>
		/// Changes the size of the display's viewport, without changing the resolution of the screen or the size of the window.
		/// </summary>
		/// <param name="height">The new height to set.
		/// Width will be set automatically, based on the aspect ratio of the window.</param>
		public static void SetViewport(int height)
		{
			Display.Height = height;

			// Can't safely set the width until the core has initialized any required APIs.
			// Otherwise, necessary information about the display may be unavailable.
			if (!Core.HasInitialized)
				return;
			else
				SetViewportAspect(AspectRatio);
		}

		/// <summary>
		/// Changes the size of the display's viewport,
		/// without changing the resolution of the screen or window,
		/// based on the given aspect ratio.
		/// The viewport's height will remain,
		/// and the width will be calculated based on that height.
		/// </summary>
		/// <param name="aspectRatio">The aspect ratio to set.
		/// For example, 1.33f for 4:3, or 1.77f for 16:9.</param>
		public static void SetViewportAspect(float aspectRatio)
		{
			if (!float.IsNaN(aspectRatio) && aspectRatio > 0)
			{
				int newWidth = (int)Math.Round(aspectRatio * Display.Height);
				SetViewport(newWidth, Height);
			}
		}

		/// <summary>
		/// A quick and easy way to toggle full screen mode.
		/// </summary>
		public static void ToggleFullScreen()
		{
			if (Display.IsFullScreen)
				Display.SetFullScreen(false);
			else
				Display.SetFullScreen(true);
		}

		/// <summary>
		/// A quick and easy way to toggle full screen mode.
		/// </summary>
		public static void SetFullScreen(bool enabled)
		{
			if (enabled)
			{
				Display.IsFullScreen = true;
#if SDL
				SDL.Window.SetFullscreen(SDL_windowPointer, SDL.Window.Flag.FullscreenDesktop);
#endif
			}
			else
			{
				Display.IsFullScreen = false;
#if SDL
				SDL.Window.SetFullscreen(SDL_windowPointer, 0);
#endif
				fitOnScreen();
			}
		}
		#endregion

		#region Device Capability Settings
		/// <summary>
		/// Whether or not to wait for the display's vertical blanking interval to draw to the screen.
		/// </summary>
		public static bool IsVSyncEnabled {
#if SDL
			get
			{
				if (SDL.Window.GL.GetSwapInterval() != 0)
					return true;
				else
					return false;
			}
			set
			{
				if (value == true)
					SDL.Window.GL.SetSwapInterval(1);
				else
					SDL.Window.GL.SetSwapInterval(0);
			}
#endif
		}

		/// <summary>
		/// Enables vertical synchronization of the display.
		/// </summary>
		public static void EnableVSync()
		{
			IsVSyncEnabled = true;
		}

		/// <summary>
		/// Disables vertical synchronization of the display.
		/// </summary>
		public static void DisableVSync()
		{
			IsVSyncEnabled = false;
		}

		/// <summary>
		/// Whether or not anisotropic filtering of textures has been enabled.
		/// </summary>
		public static bool AnisotropyEnabled { get; private set; }

		/// <summary>
		/// Maximum number of samples for anisotropic filtering. The higher the number, the better
		/// the quality, but high values may be slower on some systems.
		/// </summary>
		public static int AnisotropicSamples { get; private set; }

		/// <summary>
		/// Enables anistropy, setting it to the given level.
		/// </summary>
		/// <param name="samples">The number of samples desired. The higher the number, the better
		/// the quality, but the more taxing it will be on the system. If the given level is higher
		/// than the system supports, it will instead be set to the highest supported by the system.</param>
		public static void EnableAnisotropy(int samples)
		{
			AnisotropyEnabled = true;
			AnisotropicSamples = samples;
		}

		/// <summary>
		/// Enables anistropy, setting it to the highest level supported by the system.
		/// </summary>
		public static void EnableAnisotropy()
		{
			EnableAnisotropy(16);
		}

		/// <summary>
		/// Turns anistropy off.
		/// </summary>
		public static void DisableAnisotropy()
		{
			AnisotropyEnabled = false;
			AnisotropicSamples = 0;
		}

		/// <summary>
		/// Turns on anti-aliasing (if possible).
		/// </summary>
		public static void EnableAntiAliasing()
		{
#if OPENGL
			GL.Enable(GlEnum.Capability.Multisample);
			GL.Hint(GlEnum.HintTarget.LineSmooth, GlEnum.HintMode.Nicest);
			LogErrors();
#endif
		}

		/// <summary>
		/// Turns off anti-aliasing (if it was on).
		/// </summary>
		public static void DisableAntiAliasing()
		{
#if OPENGL
			GL.Disable(GlEnum.Capability.Multisample);
			GL.Hint(GlEnum.HintTarget.LineSmooth, GlEnum.HintMode.Fastest);
			LogErrors();
#endif
		}
		#endregion

		#region Events
		/// <summary>
		/// An event called when the display window is closed.
		/// If any listeners are added to this event,
		/// your application must be closed manually.
		/// OFE will no longer do it for you.
		/// </summary>
		public static EventHandler OnClose { get; set; }

		/// <summary>
		/// An event called when the display window is resized.
		/// </summary>
		public static EventHandler OnResize { get; set; }

		/// <summary>
		/// An event called when the display is rotated.
		/// This will generally only happen on mobile devices.
		/// </summary>
		public static EventHandler OnRotate { get; set; }
		#endregion

		#region Displaying to Screen
		/// <summary>
		/// Swaps the backbuffer to the screen.
		/// </summary>
		internal static void ToScreen()
		{
#if SDL
			SDL.Window.GL.SwapWindow(SDL_windowPointer);
#endif
		}
		#endregion

		#region Error Handling
		/// <summary>
		/// The maximum number of error message that will be written to the log file before giving up.
		/// Keeps the log file from ballooning if the engine is running up an absurd number of OpenGL errors a frame or something.
		/// </summary>
		private const int maxErrors = 1024;

		/// <summary>
		/// The number of errors recorded thus far.
		/// </summary>
		private static int errorCount;

		/// <summary>
		/// Check for any error reports from the graphics API and write them to the debug log,
		/// or otherwise handle them as needed.
		/// </summary>
		/// <returns>True if there were any errors.
		/// False if there were no errors, or errors could not be reported at this time.</returns>
		public static bool LogErrors()
		{
			bool anyErrors = false;

#if OPENGL
			// Get the latest OpenGL error.
			GlEnum.Error error = GL.GetError();

			// Are there any errors?
			while (error != GlEnum.Error.NoError)
			{
				anyErrors = true;

				// If we've exceeded the error count, don't bother writing it to the file.
				if (errorCount >= maxErrors)
				{
					DebugLog.Whisper("Another OpenGL error occurred!");
				}
				else
				{
					// Decide what to do about this garbage.
					switch (error)
					{
						case GlEnum.Error.InvalidFramebufferOperation:
							DebugLog.Failure("OpenGL reported that a recent frame buffer operation was invalid!");
							break;
						case GlEnum.Error.InvalidOperation:
							DebugLog.Failure("OpenGL reported that an invalid operation was attempted!");
							break;
						case GlEnum.Error.InvalidValue:
							DebugLog.Failure("OpenGL reported that an invalid value was passed!");
							break;
						case GlEnum.Error.OutOfMemory:
							DebugLog.Failure("OpenGL reported that it is out of memory!");
							break;
						case GlEnum.Error.StackOverflow:
							DebugLog.Failure("OpenGL reported that a stack overflow occured!");
							break;
						case GlEnum.Error.StackUnderflow:
							DebugLog.Failure("OpenGL reported that a stack underflow occured!");
							break;
						/*case GL.Error.TextureTooLargeExt:
							DebugLog.Failure("OpenGL reported that a recently mounted texture was too large for this system's graphics card!");
							break;*/
						default:
							// An error we haven't otherwise anticipated.
							DebugLog.Failure("OpenGL error: " + error.ToString() + "!");
							break;
					}
				}

				// Error count up by one.
				errorCount++;

				// Record to the log if the error count maxed out.
				if(errorCount == maxErrors)
				{
					DebugLog.Failure("The maximum number of display errors has been reached!");
					DebugLog.Note("No more display errors will be recorded to the log file in order to save space.");
				}

				// Check if there's any more errors.
				error = GL.GetError();
			}
#endif

			return anyErrors;
		}
		#endregion
	}
}
