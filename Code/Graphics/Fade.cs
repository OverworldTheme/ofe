﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics.OFE2D;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A class used to fade the screen in and out.
  /// </summary>
  public static class Fade {
    /// <summary>
    /// Whether or not the fading routines are actively running a fade.
    /// </summary>
    public static bool IsActive = false;

    /// <summary>
    /// The last color that was faded out to.
    /// </summary>
    public static Color LastColor = Color.Black;

    /// <summary>
    /// The color of the current fade.
    /// </summary>
    private static Color color = Color.Black;

    private static int fader = 0;
    private static int fadeFrom = 0;

    /// <summary>
    /// Keeps track of when a requested fade out has been completed.
    /// </summary>
    private static bool outIsDone = false;

    /// <summary>
    /// Keeps track of when a requested fade out has been completed.
    /// </summary>
    public static bool OutIsDone {
      get {
        if(outIsDone) {
          outIsDone = false;
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// Whether or not the screen is being faded out.
    /// </summary>
    public static bool ActiveOut {
      get {
        if(fader < 0) {
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// Fades the screen in.
    /// </summary>
    /// <param name="color">The color to fade in from.</param>
    /// <param name="seconds">The number of seconds the fade should last.</param>
    public static void In(Color color, float seconds) {
      // Initialize the fade.
      Fade.color = color;
      Fade.fader = (int)Math.Floor(seconds * Core.SetLPS);
      Fade.fadeFrom = (int)Math.Floor(seconds * Core.SetLPS);
      Fade.outIsDone = false;

      resetCurtain();
      createCurtain(color);
      curtain.Opacity = 1;
    }

    /// <summary>
    /// Fades in from the last color used.
    /// </summary>
    /// <param name="seconds">The number of seconds the fade should last.</param>
    public static void InFromLast(float seconds) {
      Fade.In(Fade.LastColor, seconds);
    }

    /// <summary>
    /// Fades the screen out.
    /// </summary>
    /// <param name="color">The color to fade the screen to.</param>
    /// <param name="seconds">The number of seconds the fade should last.</param>
    public static void Out(Color color, float seconds) {
      Fade.LastColor = color;
      Fade.color = color;
      Fade.fader = (int)Math.Floor(seconds * Core.SetLPS) * -1;
      Fade.fadeFrom = (int)Math.Floor(seconds * Core.SetLPS) * -1;
      Fade.outIsDone = false;

      resetCurtain();
      createCurtain(color);
    }

    /// <summary>
    /// Updates the fading variables as neccissary.
    /// </summary>
		/// <param name="seconds">The number of seconds, or fraction of a second, since the last update.</param>
    internal static void Update(float seconds) {
			// Make sure that the curtain fits to the screen.
			// (Add a little padding just to make sure there's no missing spots at the sides.)
			if (curtain != null)
			{
				curtain.Width = Stage.System.Width + 10;
				curtain.Height = Stage.System.Height + 10;
				curtain.Position = Stage.System.Center;
			}

			// Report back if we're done enough for other routines to begin making changes.
			if (Fade.fader > -1 && Fade.fader < 1) {
        Fade.IsActive = false;

        if(curtain != null) {
          curtain.Dispose();
        }
      } else {
        Fade.IsActive = true;
      }

      if(Mather.Approx(Fade.fader, -1)) {
        Fade.outIsDone = true;
				if(OnFadeOut != null) {
					OnFadeOut.Invoke(null, EventArgs.Empty);
					foreach(Delegate d in OnFadeOut.GetInvocationList()) {
						OnFadeOut -= (EventHandler)d;
					}
				}
			} else if(Mather.Approx(Fade.fader, 1)) {
				if(OnFadeIn != null) {
					OnFadeIn.Invoke(null, EventArgs.Empty);
					foreach(Delegate d in OnFadeIn.GetInvocationList()) {
						OnFadeIn -= (EventHandler)d;
					}
				}
			}

      // Update the counter.
      if(fader > 0) {
        Fade.fader--;
      } else if(fader < 0) {
        Fade.fader++;
      }

      if(Fade.fader > 0) {
        curtain.Opacity = (float)(Fade.fader - 1) / (float)Fade.fadeFrom;
      } else if(Mather.Approx(Fade.fader, -1)) {
        // If we're on the last frame of a fade-out, make sure the screen is
        // completely blotted out.
        curtain.Opacity = 1;
      } else if(Fade.fader < 0) {
        curtain.Opacity = ((float)((Fade.fader) * -1) / (float)Fade.fadeFrom) + 1f;
      }
    }

    #region Curtain
    /// <summary>
    /// A graphic primitive used to obscure the screen and create the actual fade effect.
    /// </summary>
    private static Shape curtain;

    /// <summary>
    /// Creates a new shape for the curtain.
    /// </summary>
    /// <param name="color">The color to use for the fade.</param>
    private static void createCurtain(Color color) {
			if(curtain == null)
				curtain = Shape.Box(Stage.System, 0, color, null, 0, 0.1f, Stage.System.Left, Stage.System.Top, Stage.System.Right, Stage.System.Bottom);
    }

    /// <summary>
    /// Deletes the "curtain" to make sure it doesn't get left behind and forgotten,
		/// blocking the screen, if a fade is ended early.
    /// </summary>
    private static void resetCurtain() {
			if(curtain != null)
				curtain.IsVisible = false;
    }
    #endregion

		#region Events
		/// <summary>
		/// An event fired when a fade in is completed.
		/// All events will removed after being fired.
		/// </summary>
		public static EventHandler OnFadeIn { get; set; }

		/// <summary>
		/// An event fired when a fade out is completed.
		/// All events will be removed after being fired.
		/// </summary>
		public static EventHandler OnFadeOut { get; set; }
		#endregion
	}
}
