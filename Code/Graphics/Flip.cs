﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// The various directions that an object, such as a sprite, can be mirrored.
  /// </summary>
  public enum Flip {
    /// <summary>
    /// Do not flip the object at all.
    /// </summary>
    None,

    /// <summary>
    /// Flip the object from right to left, or vice versa.
    /// </summary>
    Horizontal,

    /// <summary>
    /// Turn the object upsidedown.
    /// </summary>
    Vertical,

    /// <summary>
    /// Flip the object both horizontally, and vertically.
    /// </summary>
    Both
  }
}
