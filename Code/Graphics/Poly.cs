﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endif

using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Graphics.OFE3D;

namespace VisionRiders.OFE.Graphics {
#if DEBUG && WINDOWS
  [System.Diagnostics.DebuggerDisplay("{V1}, {V2}, {V3}")]
#endif
  /// <summary>
  /// A three-sided polygon. The polygon is defined by three integers that refer to
  /// a index position in an external vertex list.
  /// </summary>
  public struct Poly {
    /// <summary>
    /// The index of the first vertex.
    /// </summary>
    public int V1;

    /// <summary>
    /// The index of the second vertex.
    /// </summary>
    public int V2;

    /// <summary>
    /// The index of the third vertex.
    /// </summary>
    public int V3;

    /// <param name="v1">Index of the first vertex.</param>
    /// <param name="v2">Index of the second vertex.</param>
    /// <param name="v3">Index of the third vertex.</param>
    public Poly(int v1, int v2, int v3) {
      V1 = v1;
      V2 = v2;
      V3 = v3;
    }

    /// <summary>
    /// Draws this quad to the screen using the given vertex list. Must be called during a draw operation.
    /// </summary>
    /// <param name="vertexList">A list of vertices that this poly will reference when drawing.</param>
    /// <param name="color">The solid color to give this shape.</param>
    /// <param name="textureCoords">The coordinates for the texture of each vertex.</param>
		/// <exception cref="System.InvalidOperationException">Will be thrown if method is called outside the draw cycle.</exception>
    public void Out(Vertex[] vertexList) {
      // Make sure the verts fit on screen.
      // There's no reason to waste time drawing things that can't be seen.
      /*if(!DrawCycle.IsOnScreen(vertexList[V1].Position)
        && !DrawCycle.IsOnScreen(vertexList[V2].Position)
        && !DrawCycle.IsOnScreen(vertexList[V3].Position)) {
          return;
      }*/

      // Send them out.
      DrawCycle.Add(vertexList[V1], vertexList[V2], vertexList[V3]);
    }
  }
}
