﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if OPENGL
using OpenGL;
#endif

using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Graphics.OFE3D;

namespace VisionRiders.OFE.Graphics
{
	/// <summary>
	/// Class used for displaying polygons to the screen.
	/// The polys are sent out to the graphics card in batches to reduce the number of draw calls and improve performance.
	/// </summary>
	/// <remarks>
	/// Every time a texture is changed, DrawCycle has to send out a draw call with the current batch.
	/// </remarks>
	public static class DrawCycle
	{
		#region Constants
		/// <summary>
		/// The size of the vertex buffer used for 2d drawing.
		/// If it gets filled, it will be sent to the GPU before continuing.
		/// </summary>
		public const int BufferSize = 4096;

		/// <summary>
		/// The number of vertices in a single polygon.
		/// OFE operates exclusively using triangles, so every poly has three vertices.
		/// Although OFE handles quads as well, but it slices them into triangles first.
		/// </summary>
		public const int VertsPerPoly = 3;
		#endregion

		#region Initialization
		/// <summary>
		/// Initializes anything needed for the draw cycle that isn't handled elsewhere.
		/// </summary>
		internal static void Init()
		{
			// Tell the log what we're doing.
			DebugLog.Write("Initializing Draw Cycle functionality...");

			// Do it!
#if OPENGL
			// Create a frame buffer to draw to textures.
			try
			{
				if (Display.OGL_VersionMajor >= 3)
					GL.GenFramebuffers(1, out OGL_frameBufferID);
				else
					GL.Ext.GenFramebuffers(1, out OGL_frameBufferID);
			}
			catch
			{
				DebugLog.CriticalError("Framebuffers are not supported by this graphics device!");
				throw new PlatformNotSupportedException("This system's OpenGL version is not supported. OpenGL version is " + Display.OGL_VersionString);
			}
			Display.LogErrors();

			// Create our buffers to transfer data to OpenGL,
			// and bind them to the graphics card.
			GL.GenBuffers(1, out OGL_vertexBufferID);
			Display.LogErrors();
			GL.GenBuffers(1, out OGL_elementBufferID);
			Display.LogErrors();
			if (Display.OGL_VersionMajor >= 3)
				GL.GenVertexArrays(1, out OGL_vertexArray);
			//else if (Core.CurrentPlatform == OS.OSX)
				// TODO: Would Ext.GenVertexArrays work just as well?
				//GL.Apple.GenVertexArrays(1, out OGL_vertexArrayID);
			else
				GL.Ext.GenVertexArrays(1, out OGL_vertexArray);
			Display.LogErrors();

			// From its own viewpoint, OFE performs clockwise culling.
			// However, OpenGL's axes are different from OFE's,
			// so the hardware is actually culling counterclockwise.
			//GL.DepthFunc(DepthFunction.Gequal);
			GL.CullFace(GlEnum.Cull.Back);
			GL.FrontFace(GlEnum.FrontFace.CCW);
			Display.LogErrors();
#elif XNA

			// Set the current effect.
      xnaEffect = new BasicEffect(Display.xnaDevice);
#endif

			// Set the default frame skip value.			
			MaxFrameSkip = DefaultMaxFrameSkip;
			DebugLog.Note("Maximum frame skip has been set to " + MaxFrameSkip + " frames.");

			// Create the vertex buffer.
			vBuffer = new float[BufferSize * Vertex.Stride];
			pBuffer = new uint[BufferSize * VertsPerPoly];
			for(uint i = 0; i < pBuffer.Length; i++)
			{
				pBuffer[i] = i;
			}

			// Allocate our matrices.
			Camera = new Matrix();
			View = new Matrix();
			World = new Matrix();
			Projection = new Matrix();
			translation = new Matrix();
			rotation = new Matrix();
			scale = new Matrix();

			// Allocate our working variables.
			workVert = new Vertex();
			workMatrix = new Matrix();

			// Record success.
			DebugLog.Success("Draw Cycle functionality initialized!");
		}
		#endregion

		#region Shutdown
		internal static void Shutdown()
		{
#if OPENGL
			// Delete the frame buffer we were using.
			if (Display.OGL_VersionMajor >= 3)
				GL.DeleteFramebuffers(1, ref OGL_frameBufferID);
			else
				GL.Ext.DeleteFramebuffers(1, ref OGL_frameBufferID);

			Display.LogErrors();
#endif
		}
		#endregion

		#region API
#if OPENGL
		/// <summary>
		/// The frame buffer that acts as a handle to the target texture.
		/// </summary>
		private static uint OGL_frameBufferID;

		/// <summary>
		/// OpenGL handler for the vertex buffer.
		/// </summary>
		private static uint OGL_vertexBufferID;

		/// <summary>
		/// OpenGL handle for the polygon index buffer.
		/// </summary>
		private static uint OGL_elementBufferID;

		/// <summary>
		/// OpenGL handler for the array the we'll use to pass our vertices to OpenGL.
		/// </summary>
		private static uint OGL_vertexArray;
#endif
		#endregion

		#region Status
		/// <summary>
		/// Whether or not the engine is in the middle of the draw cycle.
		/// Trying to send polys outside the draw cycle will throw an exception.
		/// </summary>
		public static bool IsActive = false;
		#endregion

		#region Settings
		/// <summary>
		/// The default maximum number of frames that can be skipped if the draw cycle falls behind.
		/// </summary>
		public const int DefaultMaxFrameSkip = 10;

		/// <summary>
		/// The maximum number of frames that can be skipped if the draw cycle falls behind.
		/// Skipping more frames reduces slow-down,
		/// but loosing a bunch of frames to try and catch up can also be annoying if it jumps too far ahead.
		/// </summary>
		public static int MaxFrameSkip { get; set; }
		#endregion

		#region Working Variables
		/// <summary>
		/// We'll keep this in memory to copy vertex values instead of overwriting incoming verts,
		/// or creating new ones.
		/// </summary>
		private static Vertex workVert;

		/// <summary>
		/// We'll keep this in memory to construct and store matrix values without creating new objects every frame.
		/// </summary>
		private static Matrix workMatrix;
		#endregion

		#region Texture
		/// <summary>
		/// The texture the draw cycle is currently working with.
		/// Each new texture requires a new draw call to the graphics card.
		/// </summary>
		public static Texture Texture { get; private set; }

		/// <summary>
		/// The texture that the draw cycle is currently drawing to,
		/// or null if it is drawing to the screen.
		/// </summary>
		private static Texture target;

		/// <summary>
		/// Sets the given texture to the batch.
		/// If this is different than the one already set,
		/// the current batch will be set out with the old texture before continuing.
		/// </summary>
		/// <param name="texture">The texture to set, or null to disable textures and draw solid colors.</param>
		public static void SetTexture(Texture texture)
		{
			if (texture != Texture)
			{
				send();

				Texture = texture;

				if (Texture == null)
				{
					// Disable textures for things like shapes.
					GL.Disable(GlEnum.Capability.Texture2d);
					Display.LogErrors();
				}
				else
				{
					// Enable textures and set the texture.
					GL.Enable(GlEnum.Capability.Texture2d);
					GL.ActiveTexture(GlEnum.TextureUnit.Texture0);
					GL.BindTexture(GlEnum.TextureTarget.Texture2D, Texture.OGL_Texture);
					Display.LogErrors();

					// Enable or disable filtering.
					switch (Texture.Smoothing)
					{
						default:
						case Texture.FilterMode.Linear:
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.MagFilter, GlEnum.TextureFilter.Linear);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.MinFilter, GlEnum.TextureFilter.Linear);
							break;
						case Texture.FilterMode.Nearest:
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.MagFilter, GlEnum.TextureFilter.Nearest);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.MinFilter, GlEnum.TextureFilter.Nearest);
							break;
					}
					Display.LogErrors();

					// Set wrapping.
					switch (Texture.Repeat)
					{
						case Texture.RepeatMode.Clamp:
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapR, GlEnum.Wrap.ClampToEdge);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapS, GlEnum.Wrap.ClampToEdge);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapT, GlEnum.Wrap.ClampToEdge);
							break;
						case Texture.RepeatMode.Mirror:
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapR, GlEnum.Wrap.MirroredRepeat);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapS, GlEnum.Wrap.MirroredRepeat);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapT, GlEnum.Wrap.MirroredRepeat);
							break;
						case Texture.RepeatMode.Wrap:
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapR, GlEnum.Wrap.Repeat);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapS, GlEnum.Wrap.Repeat);
							GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.WrapT, GlEnum.Wrap.Repeat);
							break;
					}
					Display.LogErrors();
				}
			}
		}
		#endregion
		
		#region Shaders
		/// <summary>
		/// The currently active shader program being used by the draw cycle.
		/// </summary>
		public static ShaderProgram ActiveShader { get; private set; }

		/// <summary>
		/// Activates the given shader program.
		/// Will deactivate any other shader programs.
		/// </summary>
		/// <param name="shader">The shader program to activate,
		/// or null to deactivate any active shaders.</param>
		/// <exception cref="InvalidOperationException">Thrown if called when the draw cycle is not active.</exception>
		public static void SetShader(ShaderProgram shader)
		{
			// Can't change anything until the draw cycles has started!
			if (!IsActive)
			{
				DebugLog.CriticalError("Attempted to set a shader outside the draw cycle!");
				throw new InvalidOperationException
					("Cannot set shaders until the draw cycle has begun.");
			}

			// Substitute the default shader for "null".
			if (shader == null)
				shader = ShaderProgram.Default;

			// Don't waste draw calls if we're not actually changing the shader.
			if (ActiveShader != shader || ActiveShader.ParametersHaveChanged)
			{
				// We're done with the previous shader,
				// so send whatever it was we were working on to the graphics card.
				send();

				Display.LogErrors();

				if (shader == null || !shader.IsLoaded || shader.HasErrors)
				{
#if OPENGL
					GL.UseProgram(0);
#endif
					ActiveShader = null;
				}
				else
				{
#if OPENGL
					GL.UseProgram(shader.OGL_Program);
#endif
					ActiveShader = shader;
				}

				// Assign parameters.
				shader.AssignValues();

				// If there was a problem activating the shader program, disable it.
				if (ActiveShader != null && Display.LogErrors())
				{
					DebugLog.Failure("There was an error with one the shaders programs! OFE has disabed that shader program.");
					ActiveShader.HasErrors = true;
				}
			}
		}
		#endregion

		#region Buffers
		/// <summary>
		/// The vertex buffer.
		/// </summary>
		private static float[] vBuffer;

		/// <summary>
		/// The polygon buffer.
		/// </summary>
		private static uint[] pBuffer;
		
		/// <summary>
		/// Number of verts currently in use in the vertex buffer.
		/// </summary>
		private static int NumVerts;

		/// <summary>
		/// Number of indices currently in use in the polygon buffer.
		/// </summary>
		private static int NumIndices;

		/// <summary>
		/// The current position in the polygon buffer the draw cycle is adding to. When this value
		/// plus another poly worth of verts exceeds the total room in the array, then the
		/// draw cycle will send the current batch out to the graphics card and start over
		/// at the beginning of the array before continuing on.
		/// </summary>
		private static int vIndex;
		
		/// <summary>
		/// Gets or sets the last object that sent buffers to the graphics card.
		/// Will be null if it was the draw cycle, or no drawing has taken place yet.
		/// </summary>
		public static object LastBuffer { get; set; }

		/// <summary>
		/// Adds vertices to the batch. Every three of these define a poly.
		/// </summary>
		/// <param name="vertex">A single vertex. You'll need to send three for a poly.</param>
		/// <exception cref="InvalidOperationException">Thrown if called when the draw cycle is not active.</exception>
		private static void add(Vertex vertex)
		{
			// Can't add anything until the draw cycles has started. Otherwise, we throw a fit.
			if (!IsActive)
			{
				throw new InvalidOperationException
					("Cannot add vertices until the draw cycle has begun.");
			}

			// Copy the value over so that we don't overwrite.
			workVert.CopyFrom(vertex);

			// Increase counter.
			Core.VertCount++;
			NumVerts++;

			// If we're about to begin a new poly, make sure there's actually enough room in the array
			// to fit it. If not, send what we have out to the graphics card.
			/*if((vIndex % VertsPerPoly) == 0 && vIndex + VertsPerPoly >= vBuffer.Length) {
				send();
			}*/

			// Apply transformation.
			workMatrix.Multiply(Camera, World);
			workVert.Position *= workMatrix;// Camera * World;

			// Break the vert up into floats and stuff it into the buffer with all the others.
			int i = vIndex * Vertex.Stride;
			vBuffer[i + 0] = workVert.X;
			vBuffer[i + 1] = workVert.Y;
			vBuffer[i + 2] = workVert.Z;

			vBuffer[i + 3] = workVert.U;
			vBuffer[i + 4] = workVert.V;

			vBuffer[i + 5] = workVert.Color.R;
			vBuffer[i + 6] = workVert.Color.G;
			vBuffer[i + 7] = workVert.Color.B;
			vBuffer[i + 8] = workVert.Color.A;

			// Advance one position. Do not pass Go. Do not collect $200.
			vIndex++;
		}

		/// <summary>
		/// Adds vertices to the batch. Every three of these define a poly.
		/// </summary>
		/// <param name="vert1">The first vertex of the poly.</param>
		/// <param name="vert2">The second vertex of the poly.</param>
		/// <param name="vert3">The third vertex of the poly.</param>
		internal static void Add(Vertex vert1, Vertex vert2, Vertex vert3)
		{
			// If we're full up, flush the buffer first.
			if (NumVerts >= BufferSize - 1)
				send();

			NumIndices += VertsPerPoly;

			add(vert1);
			add(vert2);
			add(vert3);
		}
		#endregion

		#region Matrices
		/// <summary>
		/// The projection matrix,
		/// which translates coordinates in 3d to 2d,
		/// so that a 3d world can be displayed on a 2d screen.
		/// </summary>
		public static Matrix Projection { get; private set; }

		/// <summary>
		/// The view matrix, determined by the camera, and the current stage or scene.
		/// </summary>
		public static Matrix View { get; private set; }

		/// <summary>
		/// The world matrix, which positions polygons and models in world space.
		/// (Used primarily for 2d drawing within the draw cycle.)
		/// </summary>
		public static Matrix World { get; set; }

		/// <summary>
		/// Internal scale matrix.
		/// </summary>
		private static Matrix scale;

		/// <summary>
		/// Internal translation (position) matrix.
		/// </summary>
		private static Matrix translation;

		/// <summary>
		/// Internal rotation matrix.
		/// </summary>
		private static Matrix rotation;
		#endregion

		#region Drawing Initialization
		/// <summary>
		/// Prepares the graphics card for drawing 2d objects.
		/// </summary>
		/// <param name="stage">The stage that is about to be drawn.</param>
		internal static void Start2d(Stage stage)
		{
			// Begin.
			IsIn2dMode = true;
			Stage = stage;
			start();			
			Display.LogErrors();

			// Create our camera matrix.			
			if (stage.Target == null)
			{
				scale.Build2dScale(Stage.Camera.Zoom / Stage.Width * 2, Stage.Camera.Zoom / Stage.Height * -2);
				rotation.Build2dRotation(Stage.Camera.Rotation);
				translation.Build2dTranslation(
					-stage.Camera.X + Stage.Origin.X - (Stage.Width / 2 * Stage.Camera.Zoom),
					-stage.Camera.Y + Stage.Origin.Y - (Stage.Height / 2 * Stage.Camera.Zoom));

				Camera.Multiply(scale, rotation, translation);

				/*Camera = Matrix.Create2dScale(Stage.Camera.Zoom / Stage.Width * 2, Stage.Camera.Zoom / Stage.Height * -2)
					* Matrix.Create2dRotation(Stage.Camera.Rotation)
					* Matrix.Create2dTranslation(
						-stage.Camera.X + Stage.Origin.X - (Stage.Width / 2 * Stage.Camera.Zoom),
						-stage.Camera.Y + Stage.Origin.Y - (Stage.Height / 2 * Stage.Camera.Zoom));*/
			}
			else
			{
				scale.Build2dScale(Stage.Camera.Zoom / Stage.Width * 2, Stage.Camera.Zoom / Stage.Height * 2);
				rotation.Build2dRotation(Stage.Camera.Rotation);
				translation.Build2dTranslation(
					-stage.Camera.X + stage.Origin.X - Stage.Width / 2,
					-stage.Camera.Y + stage.Origin.Y - Stage.Height / 2);

				Camera.Multiply(scale, rotation, translation);
				/*Camera =
					Matrix.Create2dScale(Stage.Camera.Zoom / Stage.Width * 2, Stage.Camera.Zoom / Stage.Height * 2)
					* Matrix.Create2dRotation(Stage.Camera.Rotation)
					* Matrix.Create2dTranslation(
						-stage.Camera.X + stage.Origin.X - Stage.Width / 2,
						-stage.Camera.Y + stage.Origin.Y - Stage.Height / 2);*/
			}

			// Set the view and projection matrices.
			/*View = Matrix.Create2dTranslation(-stage.Camera.X, -stage.Camera.Y)
				* Matrix.Create2dScale(stage.Camera.Zoom)
				* Matrix.Create2dRotation(stage.Camera.Rotation)
				* Matrix.Create2dTranslation(stage.Origin.X, stage.Origin.Y);*/
			Projection.BuildIdentity();

#if OPENGL
			// If you're wondering why far clipping is set to 1.0001 and not just 1, it's because if it's set to
			// plain 1, OpenGL will ignore sprites with a Z-depth of 1, which is *supposed* to be the maximum value
			// in OFE. So in short, it's a compatibility thing to make sure our sprites aren't clipped incorrectly.
			// Also, we have to keep in mind that in OpenGL's texture coordinates 0, 0 is the bottom-left,
			// not the top-left for some reason.
			if (target == null)
			{
				GL.Viewport(0, 0, Display.ActualWidth, Display.ActualHeight);
			}
			else
			{
				GL.Viewport(0, 0, target.Width, target.Height);
				if (stage.BackgroundColor.HasValue)
				{
					GL.ClearColor(
						stage.BackgroundColor.Value.R,
						stage.BackgroundColor.Value.G,
						stage.BackgroundColor.Value.B,
						stage.BackgroundColor.Value.A);
					GL.Clear(GlEnum.ClearMask.ColorBuffer);
				}
			}

			// Disable culling, because why do we care in 2d.
			GL.Disable(GlEnum.Capability.CullFace);

			// Disable depth testing; we don't need it today because we've already sorted by depth.
			GL.Disable(GlEnum.Capability.DepthTest);

			// Enable alpha blending.
			GL.Enable(GlEnum.Capability.Blend);
			GL.BlendFunc(GlEnum.BlendFactor.SrcAlpha, GlEnum.BlendFactor.OneMinusSrcAlpha);

			// Now we're ready to start stamping out quads.
			Display.LogErrors();
#endif
		}

		/// <summary>
		/// Completes the 2d drawing phase.
		/// </summary>
		internal static void Finish2d()
		{
			// Send out whatever's left.
			send();

			// Done.
			finish();
		}

		/// <summary>
		/// Sends the parameters for a 3d scene to the graphics device.
		/// </summary>
		/// <param name="scene">The scene that will be drawn.</param>
		internal static void Start3d(Scene scene)
		{
			// Begin.
			Scene = scene;
			start();

			// Clear the depth buffer.
			ClearDepthBuffer();			
			
#if OPENGL			
			// Enable depth testing.
			GL.Enable(GlEnum.Capability.DepthTest);
			Display.LogErrors();
#elif XNA
      Display.xnaDevice.BlendState = BlendState.Opaque;
      Display.xnaDevice.DepthStencilState = DepthStencilState.Default;      
      Display.xnaDevice.SamplerStates[0] = SamplerState.LinearWrap;
#endif

			// Set our matrices.			
			Projection = Matrix.CreatePerspectiveFOV(scene.Camera.FieldOfView, Display.AspectRatio, Scene.Camera.Near, Scene.Camera.Far);
			View.Multiply(Matrix.CreateFromQuaternion(scene.Camera.Rotation * Quaternion.FromAxisAngle(Vector.Right, .25f)),
				Matrix.CreateTranslation(scene.Camera.AbsolutePosition.Inverse()));
			//View = Matrix.CreateLookAt(scene.Camera.Position, scene.Camera.Target.Position, Vector.Up);
		}

		/// <summary>
		/// Completes the 3d drawing phase.
		/// </summary>
		internal static void Finish3d()
		{
			// Done.			
			finish();
			Display.LogErrors();
		}

		/// <summary>
		/// Signals the beginning of drawing.
		/// </summary>
		private static void start()
		{
			// Make sure we haven't started before we finished the last cycle.
			if (IsActive)
			{
				DebugLog.CriticalError("A new draw cycle was attempted before the last one was completed!!!");
				throw new InvalidOperationException
					("The engine attempted to start a draw cycle before the last one was completed.");
			}

			// Drawing has begun!
			IsActive = true;

			// Reset position to start at the beginning of the vertex array.
			vIndex = 0;

			// Set our target.
			if (IsIn2dMode)
				target = Stage.Target;
			else
				target = Scene.Target;
#if OPENGL
			if (target != null && target.OGL_IsMounted)
			{
				if (Display.OGL_VersionMajor >= 3)
				{
					GL.BindFramebuffer(GlEnum.FramebufferTarget.Framebuffer, OGL_frameBufferID);
					GL.FramebufferTexture2d(GlEnum.FramebufferTarget.Framebuffer, GlEnum.FramebufferAttachment.Color0, GlEnum.TextureTarget.Texture2D, target.OGL_Texture, 0);
				}
				else
				{
					GL.Ext.BindFramebuffer(GlEnum.FramebufferTarget.Framebuffer, OGL_frameBufferID);
					GL.Ext.FramebufferTexture2d(GlEnum.FramebufferTarget.Framebuffer, GlEnum.FramebufferAttachment.Color0, GlEnum.TextureTarget.Texture2D, target.OGL_Texture, 0);
				}

				if (!OGL_isFramebufferIsOkay())
				{
					// For some reason, we can't draw to the frame buffer this frame.
					finish();
					return;
				}
			}
			else
			{
				// Unbind any current framebuffer, so that we draw to the screen.
				if (Display.OGL_VersionMajor >= 3)
					GL.BindFramebuffer(GlEnum.FramebufferTarget.Framebuffer, 0);
				else
					GL.Ext.BindFramebuffer(GlEnum.FramebufferTarget.Framebuffer, 0);
			}
#endif
			Display.LogErrors();
		}

		/// <summary>
		/// Signals the end of drawing.
		/// </summary>
		private static void finish()
		{			
			// Make sure we actually started.
			if (!IsActive)
			{
				DebugLog.CriticalError("The engine tried to end the draw cycle before any draw cycle was initiated!!!");
				throw new InvalidOperationException
					("The engine attempted to end a draw cycle before any draw cycle was initiated.");
			}

			// Drawing is complete.
			IsActive = false;			
			IsIn2dMode = false;
		}
		#endregion

		#region Drawing
		/// <summary>
		/// Whether or not the engine is in 2d drawing mode. This changes how OFE has to send
		/// the poly data to make sure the z and y axes don't get mixed up.
		/// </summary>
		public static bool IsIn2dMode = false;

		/// <summary>
		/// In 2d mode, the stage being drawn.
		/// </summary>
		private static Stage Stage;

		/// <summary>
		/// In 3d mode, the scene being drawn.
		/// </summary>
		private static Scene Scene;

		/// <summary>
		/// The camera matrix.
		/// </summary>
		public static Matrix Camera;

		/// <summary>
		/// Draws all active graphics to the screen.
		/// </summary>
		internal static void Draw()
		{			
			// Reset the performance counters.
			Core.DrawCalls = 0;
			Core.VertCount = 0;

			// Set the background color.
			clearTo(Display.BackgroundColor);

			// Draw everything! :D
			GraphicGroup.DrawAll();
		}

		/// <summary>
		/// Clears the z-depth buffer.
		/// </summary>
		internal static void ClearDepthBuffer()
		{
#if OPENGL
			GL.Clear(GlEnum.ClearMask.DepthBuffer);
#endif
		}

		/// <summary>
		/// Clears the buffer to the given color.
		/// </summary>
		internal static void clearTo(Color ClearColor)
		{
#if OPENGL
			GL.ClearColor(ClearColor.R, ClearColor.G, ClearColor.B, ClearColor.A);
			GL.Clear(GlEnum.ClearMask.ColorBuffer);
#endif
		}
		#endregion

		#region Sending to the Graphics Card
		/// <summary>
		/// Sends the current batch out to the graphics card. Godspeed little polygons!
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown if called when the draw cycle is not active.</exception>
		private static void send()
		{
			// Make sure we're not complete idiots.
			if (!IsActive)
			{
				DebugLog.CriticalError("The engine attempted to send the vertex buffer out to the graphics card before the draw cycle had begun!!!");
        throw new InvalidOperationException
					("The engine attempted to send the vertex buffer out to the graphics card before the draw cycle had begun.");
			}

			// If the index position is 0, that means no verts have been added,
			// so we don't need to do anything.
			if (vIndex == 0)
				return;

			// We are now the most recent buffer.
			LastBuffer = null;

			// Let's see how many polys we're sending...
			int polys = vIndex / VertsPerPoly;

			// Out to the graphics card they go!
			// Be brave, little polys!
#if OPENGL
			// Make sure the texture is loaded.
			// Otherwise, just skip displaying it.
			if (Texture == null || Texture != null && Texture.IsLoaded && Texture.OGL_IsMounted)
			{
				// Bind the vertex array.
				Display.LogErrors();
				if (Display.OGL_VersionMajor >= 3)
					GL.BindVertexArray(OGL_vertexArray);
				else
					GL.Ext.BindVertexArray(OGL_vertexArray);
				/*if(Core.CurrentPlatform != OS.OSX)
					GL.BindVertexArray(OGL_vertexArrayID);
				else
					GL.Apple.BindVertexArray(OGL_vertexArrayID);*/

				// Send the vertex buffer out.
				GL.BindBuffer(GlEnum.BufferTarget.Array, OGL_vertexBufferID);
				GL.BufferData(GlEnum.BufferTarget.Array, new IntPtr(sizeof(float) * NumVerts * Vertex.Stride), vBuffer, GlEnum.UsagePattern.DynamicDraw);

				// Send the poly index out.
				GL.BindBuffer(GlEnum.BufferTarget.ElementArray, OGL_elementBufferID);
				GL.BufferData(GlEnum.BufferTarget.ElementArray, new IntPtr(sizeof(int) * NumIndices), pBuffer, GlEnum.UsagePattern.DynamicDraw);

				// Apply the matrices.
				/*if (IsIn2dMode)
				{
					//GL.Translate(view.GetTranslation().X, view.GetTranslation().Y, view.GetTranslation().Z);
					//GL.Rotate(view.GetRotation().Z, Vector3d.UnitZ);
					//GL.Scale(view.GetScale().X, view.GetScale().Y, view.GetScale().Z);
				}
				else
				{					
					GL.Translate(View.GetTranslation().X, View.GetTranslation().Z, View.GetTranslation().Y);
					Vector r = View.GetRotation();
					GL.Rotate(r.X, Vector3d.UnitX);
					GL.Rotate(r.Y, Vector3d.UnitZ);
					GL.Rotate(r.Z, Vector3d.UnitY);
					GL.Scale(View.GetScale().X, View.GetScale().Y, View.GetScale().Z);
				}
				Display.LogErrors();*/

				// Send out shader parameters.
				if (ActiveShader != null)
				{					
					int vAtt = GL.GetAttribLocation(ActiveShader.OGL_Program, "position");
					if (vAtt != -1)
					{
						GL.VertexAttribPointer(vAtt, 3, GlEnum.Type.Float, false, 9 * sizeof(float), 0);
						GL.EnableVertexAttribArray(vAtt);
					}

					int tAtt = GL.GetAttribLocation(ActiveShader.OGL_Program, "texCoords");
					if (tAtt != -1)
					{
						GL.VertexAttribPointer(tAtt, 2, GlEnum.Type.Float, false, 9 * sizeof(float), 3 * sizeof(float));
						GL.EnableVertexAttribArray(tAtt);
					}

					int cAtt = GL.GetAttribLocation(ActiveShader.OGL_Program, "color");
					if (cAtt != -1)
					{
						GL.VertexAttribPointer(cAtt, 4, GlEnum.Type.Float, false, 9 * sizeof(float), 5 * sizeof(float));
						GL.EnableVertexAttribArray(cAtt);
					}
				}

				// Draw.
				GL.DrawElements(GlEnum.Primitive.Triangles, NumIndices, GlEnum.Type.UnsignedInt, 0);
				Display.LogErrors();

				// Increase the call count by one.
				Core.DrawCalls++;

				// Done!				
			}
#endif

			// Check for any lingering errors.
			Display.LogErrors();

			// Reset the buffers.
			NumVerts = 0;
			NumIndices = 0;

			// And we're back at the beginning.
			vIndex = 0;
		}
		#endregion

		#region Support Methods
		/// <summary>
		/// Checks if a given vector will appear on screen when run through the display matrices.
		/// </summary>
		/// <param name="vector">The vector position to check</param>
		/// <returns>True if it will be visible, false otherwise.</returns>
		public static bool IsOnScreen(Vector vector)
		{
			workMatrix.Multiply(World, View, Projection);
			vector *= workMatrix;// World * View * Projection;
			if (vector.X < Display.Left || vector.X > Display.Right
				|| vector.Y < Display.Top || vector.Y > Display.Bottom)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		#endregion

		#region Additional Error Checking
#if OPENGL
		/// <summary>
		/// Checks the status of the OpenGL frame buffer.
		/// </summary>
		/// <returns>False if there is some problem with the current frame buffer.</returns>
		private static bool OGL_isFramebufferIsOkay()
		{
			GlEnum.FramebufferStatus glStatus;

			if (Display.OGL_VersionMajor >= 3)
				glStatus = GL.CheckFramebufferStatus(GlEnum.FramebufferTarget.Framebuffer);
			else
				glStatus = GL.Ext.CheckFramebufferStatus(GlEnum.FramebufferTarget.Framebuffer);

			if (glStatus == GlEnum.FramebufferStatus.Complete)
			{
				// Good to go.
				return true;
			}
			else
			{
				// Uh-oh...
				DebugLog.Failure("OpenGL framebuffer error: " + glStatus.ToString() + "!");
				return false;
			}
		}
#endif
		#endregion
	}
}
