﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A 2d pair of coordinates representing a specific location on the screen, or the location of a
  /// 2d element in relation to another.
  /// </summary>
#if DEBUG && WINDOWS
  [System.Diagnostics.DebuggerDisplay("({X}, {Y})")]
#endif
  [OFEdataSerialize]
	public struct Coord {
    #region Class Constructors
    /// <param name="x">The location to assign for the X axis.</param>
    /// <param name="y">The location to assign for the Y axis.</param>
    public Coord(float x, float y) {
      X = x;
      Y = y;
    }

    /// <param name="xy">The location to assign for both the X and Y axes.</param>
    public Coord(float xy) : this(xy, xy) { }
    #endregion

    #region Static Coordinate Definitions
    /// <summary>
    /// Coordinate (0, 0).
    /// </summary>
    public static readonly Coord Zero = new Coord(0);

    /// <summary>
    /// Coordinate (1, 1).
    /// </summary>
    public static readonly Coord One = new Coord(1);
    #endregion

		#region Coordinates
		/// <summary>
    /// The coordinate of the X axis.
    /// </summary>
    public float X;

    /// <summary>
    /// The coordinate of the Y axis.
    /// </summary>
    public float Y;
		#endregion

    #region Operators
    public override int GetHashCode() {
      return base.GetHashCode();
    }

		public override string ToString() {
			return X + ", " + Y;
		}

    public override bool Equals(object obj) {
      return base.Equals(obj);
    }

    public static bool operator ==(Coord c1, Coord c2) {
      if(c1.X == c2.X && c1.Y == c2.Y) {
        return true;
      } else {
        return false;
      }
    }

    public static bool operator !=(Coord c1, Coord c2) {
      if(c1 == c2) {
        return false;
      } else {
        return true;
      }
    }

    public static Coord operator +(Coord c1, Coord c2) {
      return new Coord(c1.X + c2.X, c1.Y + c2.Y);
    }

    public static Coord operator -(Coord coord) {
      return new Coord(-coord.X, -coord.Y);
    }

    public static Coord operator -(Coord c1, Coord c2) {
      return new Coord(c1.X - c2.X, c1.Y - c2.Y);
    }

		public static Coord operator *(Coord c1, Coord c2) {
			return new Coord(c1.X * c2.X, c1.Y * c2.Y);
		}

		public static Coord operator /(Coord c1, Coord c2) {
			return new Coord(c1.X / c2.X, c1.Y / c2.Y);
		}

    public static implicit operator CoordInt(Coord coord) {
      return new CoordInt(Mather.Round(coord.X), Mather.Round(coord.Y));
    }
    #endregion

    #region Methods
    /// <summary>
    /// Calculates the smallest angle between two coordinates.
    /// </summary>
    /// <returns>A number from 0 to .25 revolutions. At 0, the points have the exact same Y coordinate.
    /// At .25, they have the exact same X coordinate.</returns>
    public static float AngleBetween(Coord coord1, Coord coord2) {
      float angle = Math.Abs(Mather.Atan2(coord2.Y - coord1.Y, coord2.Y - coord1.X));

      if(angle > .25f) {
        angle = .5f - angle;
      }

      return angle;
    }

    /// <summary>
    /// Calculates the smallest angle between this and another coordinate.
    /// </summary>
    /// <returns>A number from 0 to .25 revolutions. At 0, the points have the exact same Y coordinate.
    /// A .25, they have the exact same X coordinate.</returns>
    public float AngleTo(Coord coord) {
      return AngleBetween(this, coord);
    }

    /// <summary>
    /// Calculates the distance from this coordinate to the given coordinate.
    /// </summary>
    /// <param name="coord"></param>
    /// <returns>The distance.</returns>
    public float Distance(Coord coord) {
      // Get the differences between the two coordinates.
      float differenceX = coord.X - this.X;
      float differenceY = coord.Y - this.Y;

      // Now use some basic trigonometry to get the distance between them.
      return Mather.Sqrt((differenceX * differenceX) + (differenceY * differenceY));
    }

    /// <summary>
    /// Calculates the distance between two given coordinates
    /// </summary>
    /// <param name="coord1">The first coordinate.</param>
    /// <param name="coord2">The second coordinate.</param>
    /// <returns>The distance.</returns>
    public static float Distance(Coord coord1, Coord coord2) {
      return coord1.Distance(coord2);
    }
    #endregion
  }
}
