﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

#if OPENGL
using System.Drawing;
using System.Drawing.Imaging;
using OpenGL;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A two-dimensional image stored in memory that can be displayed as
  /// part of a sprite, or as a texture on a mesh.
  /// </summary>
  public class Texture : IDisposable, ILoadable {
    #region Static Management
    /// <summary>
    /// A list of all the active textures in memory.
    /// </summary>
    public static List<Texture> All {
      get {
        return all;
      }
    }
    private static List<Texture> all;

    /// <summary>
    /// Gets the total number of textures in use by the engine.
    /// </summary>
    public static int Total {
      get {
        return All.Count;
      }
    }

    /// <summary>
    /// Initializes the engine's texture management.
    /// </summary>
    internal static void Init() {
      // Tell the log what we're doing.
      DebugLog.Write("Initializing texture management...");

      // Create the list.
      all = new List<Texture>();

			// Set defaults.
			DefaultSmoothing = FilterMode.Linear;
			DefaultRepeat = RepeatMode.Clamp;

      // Record success.
      DebugLog.Success("Texture management has been initialized.");
    }

		/// <summary>
		/// Shuts down texture management and releases all resources.
		/// </summary>
		internal static void Shutdown() {
			while(all.Count > 0) {
				all[0].Dispose();
			}
		}
    #endregion

		#region Enumerations
		/// <summary>
		/// Modes degining the texture filters supported by OFE.
		/// These define how the texture is "smoothed" when resized.
		/// </summary>
		public enum FilterMode {
			/// <summary>
			/// Linear filtering, which will smooth the texture if it's resized.
			/// </summary>
			Linear,

			/// <summary>
			/// Nearest neighbor filtering, which will pixelate the texture if it's resized.
			/// </summary>
			Nearest
		}

		/// <summary>
		/// Modes defining a texture displays when refering to a coordinate outside
		/// its physical borders. More commonly known as a "texture address mode".
		/// </summary>
		public enum RepeatMode {
			/// <summary>
			/// The outermost pixels on the borders of this texture stretch into infinity.
			/// </summary>
			Clamp,

			/// <summary>
			/// When the texture is repeated, it is mirrored.
			/// </summary>
			Mirror,

			/// <summary>
			/// When the texture is repeated, it is tiled.
			/// </summary>
			Wrap
		}
		#endregion

		#region Class Constructors
		/// <summary>
    /// Creates a new texture for use by the engine.
    /// </summary>
    public Texture() : this(false, null) { }

    /// <summary>
    /// Creates a new texture from the given file.
    /// </summary>
    /// <param name="imageFile">The image file to use for this texture.</param>
		public Texture(string imageFile)
			: this(false, imageFile) { }

		/// <summary>
		/// Creates a new texture from the given system bitmap.
		/// </summary>
		/// <param name="bitmap">The bitmap to mount as a texture.</param>
		public Texture(Bitmap bitmap)
			: this(false, null) {
			// Set initial parameters
			Smoothing = DefaultSmoothing;
			Repeat = DefaultRepeat;

			// Ready the texture for loading.
#if OPENGL
			sysTexture = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			OGL_Mount(bitmap);
			isLoaded = true;
#endif

			// Add this to the texture repository.
			//All.Add(this);
		}

		/// <summary>
		/// Create a texture of the given size.
		/// </summary>
		/// <param name="width">The width, in pixels, to make this texture.</param>
		/// <param name="height">The height, in pixels, to make this texture.</param>
		public Texture(int width, int height)
			: this(width, height, Color.None) { }

		public Texture(int width, int height, Color color)
		{
			// Set initial parameters.
			Smoothing = DefaultSmoothing;
			Repeat = DefaultRepeat;

			// Create the texture.
#if OPENGL
			Bitmap bitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap))
			using (Brush brush = new SolidBrush(color.DotNetColor))
			{
				g.FillRectangle(brush, 0, 0, bitmap.Width, bitmap.Height);
			}

			sysTexture = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			OGL_Mount(bitmap);
			isLoaded = true;
#endif
		}

    /// <summary>
    /// Creates a new texture for use by the engine.
    /// </summary>
    /// <param name="autoManage">Whether or not OFE should auto-manage this texture.</param>
    /// <param name="imageFile">The image file to use for this texture.</param>
    internal Texture(bool autoManage, string imageFile) {
			// Set initial parameters
			Smoothing = DefaultSmoothing;
			Repeat = DefaultRepeat;

			// Ready the texture for loading.
			LoadFromFile(imageFile);
      managedByOFE = autoManage;

			// Add this to the texture repository.
      All.Add(this);
    }
    #endregion

    #region Disposal and Release
		/// <summary>
		/// Whether or not this class has already released its resources.
		/// </summary>
		protected bool isDisposed = false;

    /// <summary>
    /// Releases this texture's resources from memory.
    /// You should call this whenever you are done with a texture.
    /// </summary>
    public void Dispose() {
			Dispose(true);
		}

		/// <summary>
		/// Releases this texture's resources.
		/// </summary>
		/// <param name="disposeManaged">Whether to dispose manages resources as well.</param>
		protected void Dispose(bool disposeManaged) {
			if(isDisposed) {
				// Nothing to do. Already did this.
				return;
			}

      // Release resources.
#if OPENGL
			Core.AddUpdateCallback(() => {
				GL.DeleteTextures(1, ref OGL_Texture);
				Display.LogErrors();
			});
#endif

			if(disposeManaged) {
				// Clear all references.
				referenceList.Clear();
			}

			// Clear this from the list.
			if(All != null) {
				All.Remove(this);
			}

			isDisposed = true;
    }

		~Texture() {
			Dispose(false);
		}
		#endregion

		#region ToString
		/// <summary>
		/// Gets the filename, width, and height of the texture image,
		/// formated to be human-readable.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return FileName + " (" + Width + "×" + Height + ")";
		}
		#endregion

		#region Image File Loading and Properties
		/// <summary>
		/// A list of valid image file extensions. When using the XNA version of OFE, this list is
		/// moot because all images and textures are XNA data files and XNA will provide the
		/// extension on its own.
		/// </summary>
		/// <remarks>
		/// The extensions are listed in the order of preference. For example, .png is listed before .jpg.
		/// Therefore if there is both a .png and a .jpg present with the same file name, the .png will be loaded, not the .jpg.
		/// </remarks>
		public static readonly List<string> ValidExtensions = new List<string> { ".png", ".tiff", ".tif", ".jpeg", ".jpg", ".bmp", ".gif" };
		
#if OPENGL
		/// <summary>
		/// The bitmap data for the texture, retained so that we'll still have information about the texture's
		/// base image even though we're not retaining a reference to the bitmap itself.
		/// </summary>
		internal BitmapData sysTexture { get; set; }

		/// <summary>
		/// The texture ID for this bitmap, as generated by OpenGL.
		/// </summary>
		public uint OGL_Texture;

		/// <summary>
		/// Whether or not OFE has transfered a loaded bitmap to OpenGL for use yet.
		/// Although OFE can't display a texture until it's mounted, the other parts of the engine
		/// can already begin using it so long as it's been loaded.
		/// </summary>
		internal bool OGL_IsMounted { get; set; }

		/// <summary>
		/// Transfers the given bitmap into memory for OpenGL and sets the data and ID for this texture.
		/// </summary>
		internal void OGL_Mount(Bitmap image) {
			// GL operations need to be executed on the main thread.
			Core.AddUpdateCallback(() => {
				GL.GenTextures(1, out OGL_Texture);
				GL.BindTexture(GlEnum.TextureTarget.Texture2D, OGL_Texture);
				GL.TexImage2D(
					GlEnum.TextureTarget.Texture2D,
					0,
					GlEnum.PixelFormat.RGBA,
					sysTexture.Width,
					sysTexture.Height,
					0,
					GlEnum.PixelFormat.BGRA,
					GlEnum.Type.UnsignedByte,
					sysTexture.Scan0);
				image.UnlockBits(sysTexture);
				//GL.TexParameterI(GlEnum.TextureTarget.Texture2d, GlEnum.TextureParameterName.MinFilter, GlEnum.TextureFilter.Linear);				
				//GL.TexParameterI(GlEnum.TextureTarget.Texture2d, GlEnum.TextureParameterName.MagFilter, GlEnum.TextureFilter.Linear);
				Display.LogErrors();

				//req.texture.FinishLoad(req);
				OGL_IsMounted = true;
				DebugLog.Whisper("Grunt successfully mounted texture \"" + Directory + "//" + FileName + "\".");
			});
		}
#endif

    /// <summary>
    /// Keeps track of the most recent load request.
    /// </summary>
    /// <remarks>
    /// We need to ensure that the following is handled correctly:
    /// 
    /// 1) Request load A
    /// 2) Request load B
    /// 3) Grunt finishes A (need to ensure OFE doesn't think B is done loading).
    /// </remarks>
    private LoadTextureRequest activeLoad = null;

    /// <summary>
    /// The currently loaded TextureData, if it exists.
    /// </summary>
    internal TextureData Data {
			get {
				return data;
			}
    }
		private TextureData data = null;

    /// <summary>
    /// Called by Grunt to complete the load of a texture. Note that this call must happen on the main thread.
    /// </summary>
    /// <param name="req">The request that was completed.</param>
    internal void FinishLoad(LoadTextureRequest req) {
      if (req == activeLoad) {
        activeLoad = null;
        data = req.textureData;
      }
      // else we've started a new load since this req was issued, so ignore this request.
    }

    /// <summary>
    /// Gets whether or not this texture has finished loading.
    /// It will not display until it's finished loading.
    /// </summary>
    public bool IsLoaded {
      get {
				lock(textureLock) {
					return isLoaded;
				}
      }
    }
		private bool isLoaded;

		/// <summary>
		/// Event fired by Grunt when this texture is finished loading.
		/// </summary>
		public EventHandler OnLoaded {
			get {
				lock(textureLock) {
					return onLoaded;
				}
			}
			set {
				lock(textureLock) {
					if(IsLoaded) {
						value.Invoke(this, EventArgs.Empty);
					} else {
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;

    /// <summary>
    /// The name of the file this texture was loaded from (or attempted to load from).
    /// </summary>
		public string FileName { get; private set; }

    /// <summary>
    /// The folder where the image file this texture came from.
    /// </summary>
		public string Directory { get; private set; }

		/// <summary>
		/// The path and file of the texture this file was or will be loaded from.
		/// </summary>
		public string PathAndFilename {
			get {
				if (!string.IsNullOrWhiteSpace(FileName))
					return Directory + "//" + FileName;
				else
					return string.Empty;
			}
		}
    
    /// <summary>
    /// Gets whether or not the file given for this texture was unable to be opened.
    /// If true, the last file this texture attempted to load either didn't exist or was
    /// corrupt, and so a 2x2 pixel image was substituted.
    /// </summary>
    public bool FileWasBad {
      get {
				return fileWasBad;
      }
    }
		private bool fileWasBad;

    /// <summary>
    /// Orders a texture to be loaded from a file.
    /// </summary>
    /// <param name="File">The relative path and filename of the image file to load.</param>
    public void LoadFromFile(string imageFile) {
      data = null;
      activeLoad = null;
			isLoaded = false;
#if OPENGL
			OGL_IsMounted = false;
#endif

      FileName = string.Empty;
      Directory = string.Empty;

      // Note the file name.
      if(imageFile != null && imageFile != string.Empty) {
        FileName = Path.GetFileNameWithoutExtension(imageFile);

        // Attempt to parse the directory information.
        try {
          Directory = Path.GetDirectoryName(imageFile);
        } catch {}

        // Tell the log what we're doing.
        DebugLog.Whisper("A texture has requested file \"" + imageFile + "\".");

        // Queue ourselves up!
        TextureData newData = new TextureData();
        LoadTextureRequest req = new LoadTextureRequest(this, newData, FileName, Directory);
        activeLoad = req;
        Grunt.Enqueue(this);
      } else if (imageFile == null) {
        // Tell the log about it.
        DebugLog.Whisper("A texture has been set to load a null image file by the application.");
      } else if(imageFile == string.Empty) {
        // Tell the log about it.
        DebugLog.Whisper("A texture has been set to load an image file with no name by the application.");
      }
    }

		public void LoadNow() {
			// Let's get the file name, with the extension.
			string imageFile = OFE.Graphics.Texture.GetFileExtension(Path.Combine(Grunt.FullPath, this.PathAndFilename));

#if OPENGL
			Bitmap toMount;
#elif XNA
      Texture2D toLoad;
#endif

			// Make sure we were given a file to load. If not, create a blank texture to use
			// for this sprite so that the engine doesn't lock up when trying to display a
			// non-existant sprite.
			if(imageFile == null) {
				fileWasBad = true;				
#if OPENGL
				toMount = new Bitmap(2, 2);
				sysTexture = toMount.LockBits(new System.Drawing.Rectangle(0, 0, toMount.Width, toMount.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				OGL_Mount(toMount);
#elif XNA
        toLoad = new Texture2D(Display.xnaDevice, 2, 2);
#endif

				// The texture is considered loaded now.
				isLoaded = true;

				// Tell the log we failed.
				DebugLog.Warning("Couldn't find the texture \"" + Directory + "//" + FileName + "\".");
			} else {
				fileWasBad = false;

				// Tell the log what we're about to do.
				DebugLog.Whisper("Grunt has begun loading texture \"" + imageFile + "\"...");

#if OPENGL
				toMount = new Bitmap(imageFile);
				sysTexture = toMount.LockBits(new System.Drawing.Rectangle(0, 0, toMount.Width, toMount.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				DebugLog.Whisper("Grunt is waiting to mount texture \"" + imageFile + "\"...");				
				OGL_Mount(toMount);
				isLoaded = true;
#elif XNA
        toLoad = Grunt.xnaContent.Load<Texture2D>(imageFile);
				xnaTexture = toLoad;
				isLoaded = true;
				Debug.LogSuccess("Grunt successfully loaded texture \"" + imageFile + "\"!");
#endif
			}
		}

    /// <summary>
    /// Searches for a valid image file with the given file name. If it exists, it returns the file name
    /// appended with its extension.
    /// </summary>
    /// <returns>Null if a valid file is not found.</returns>
    public static string GetFileExtension(string fileName) {
#if !XNA
      foreach(string extension in ValidExtensions) {
        if(File.Exists(fileName + extension)) {
          return fileName + extension;
        }
      }
#else
      if(File.Exists(fileName + ".xnb"))
        return fileName;
#endif

      // No valid image file was found;
      return null;
    }

    /// <summary>
    /// Checks if a given texture file exists. The exact method of checking depends on what
    /// libary we're using.
    /// </summary>
    private bool fileExists(string fileName) {
#if OPENGL
      fileName = getFileExtension(fileName);
#elif XNA
      if(Display.xnaContent.RootDirectory != string.Empty) {
        fileName = Display.xnaContent.RootDirectory + "//" + fileName + ".xnb";
      } else {
        fileName = fileName + ".xnb";
      }
#endif

      if(File.Exists(fileName)) {
        return true;
      } else {
        return false;
      }
    }

    /// <summary>
    /// Searches for a valid image file with the given file name. If it exists, it returns the file name
    /// appended with its extension.
    /// </summary>
    /// <returns>Null if a valid file is not found.</returns>
    private string getFileExtension(string fileName) {
      foreach(string extension in ValidExtensions) {
        if(File.Exists(fileName + extension)) {
          return fileName + extension;
        }
      }

      // No valid image file was found;
      return null;
    }
    #endregion

		#region Image File Output
		/// <summary>
		/// Saves the current state of this texture to a bitmap file.
		/// Transparency will not be saved.
		/// </summary>
		/// <param name="filename">The path and filename of the texture, sans the file extension.</param>
		public void SaveToBitmapFile(string filename) {
			// Get bitmap.
			Bitmap bitmap = getBitmapData(false);

			// Make sure directory exists.
			if(!System.IO.Directory.Exists(Path.GetDirectoryName(Path.Combine(Core.Directory, filename))))
				System.IO.Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(Core.Directory, filename)));

			// Save!
			filename = Path.Combine(Core.Directory, filename + ".bmp");
			bitmap.Save(filename, ImageFormat.Bmp);
		}

		/// <summary>
		/// Saves the current state of this texture to a jpeg file.
		/// Transparency will not be saved.
		/// </summary>
		/// <param name="filename">The path and filename of the texture, sans the file extension.</param>
		public void SaveToJpegFile(string filename) {
			// Get bitmap.
			Bitmap bitmap = getBitmapData(false);

			// Make sure directory exists.
			if(!System.IO.Directory.Exists(Path.GetDirectoryName(Path.Combine(Core.Directory, filename))))
				System.IO.Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(Core.Directory, filename)));

			// Save!
			filename = Path.Combine(Core.Directory, filename + ".jpeg");
			bitmap.Save(filename, ImageFormat.Jpeg);
		}

		/// <summary>
		/// Saves the current state of this texture to a png file.
		/// </summary>
		/// <param name="filename">The path and filename of the texture, sans the file extension.</param>
		public void SaveToPngFile(string filename) {
			// Get bitmap.
			Bitmap bitmap = getBitmapData(true);

			// Make sure directory exists.
			if(!System.IO.Directory.Exists(Path.GetDirectoryName(Path.Combine(Core.Directory, filename))))
				System.IO.Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(Core.Directory, filename)));

			// Save!
			filename = Path.Combine(Core.Directory, filename + ".png");
			bitmap.Save(filename, ImageFormat.Png);
		}

		/// <summary>
		/// Saves the current state of this texture to a png file.
		/// </summary>
		/// <param name="filename">The path and filename of the texture, sans the file extension.</param>
		public void SaveToTiffFile(string filename) {
			// Get bitmap.
			Bitmap bitmap = getBitmapData(true);

			// Make sure directory exists.
			if(!System.IO.Directory.Exists(Path.GetDirectoryName(Path.Combine(Core.Directory, filename))))
				System.IO.Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(Core.Directory, filename)));

			// Save!
			filename = Path.Combine(Core.Directory, filename + ".tiff");
			bitmap.Save(filename, ImageFormat.Tiff);
		}

		/// <summary>
		/// Gets a new bitmap from this texture.
		/// The bitmap can then be saved to a file.
		/// </summary>
		/// <param name="includeAlpha">Whether the alpha (transparency) will be saved.
		/// Determines what format the bitmap data will take.</param>
		/// <returns></returns>
		private Bitmap getBitmapData(bool includeAlpha) {
			// Decide on a format.
			System.Drawing.Imaging.PixelFormat format;
			if(includeAlpha)
				format = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			else
				format = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
			
			// Create a new bitmap and lock the bitmap for manipulation.
			Bitmap bitmap = new Bitmap(Width, Height);			
			BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, Width, Height), ImageLockMode.WriteOnly, format);

			// Get the data from the texture.
			// (And don't forget that Microsoft bitmaps are BGR, not RGB.)
			GL.GetTexImage(GlEnum.TextureTarget.Texture2D, 0, GlEnum.PixelFormat.BGRA, GlEnum.Type.UnsignedByte, data.Scan0);

			// Unlock the bitmap.
			bitmap.UnlockBits(data);

			// Done!
			return bitmap;
		}
		#endregion

		#region Size Properties
		/// <summary>
    /// The width of the texture, in pixels.
    /// </summary>
    public int Width {
      get {
        if (IsLoaded) {
#if OPENGL
          return sysTexture.Width;
#elif XNA
          return xnaTexture.Width;
#endif
        } else {
          return -1;
        }
      }
    }

    /// <summary>
    /// The height of the texture, in pixels.
    /// </summary>
    public int Height {
      get {
        if (IsLoaded) {
#if OPENGL
          return sysTexture.Height;
#elif XNA
          return xnaTexture.Height;
#endif
        } else {
          return -1;
        }
      }
    }

    /// <summary>
    /// The center of the texture.
    /// </summary>
    public Coord Center {
      get {
#if OPENGL
        if (IsLoaded && sysTexture != null) {
          float X = sysTexture.Width / 2;
          float Y = sysTexture.Height / 2;
          return new Coord(X, Y);
        } else {
          return new Coord(-1, -1);
        }
#elif XNA
        if(IsLoaded && xnaTexture != null) {
          float X = xnaTexture.Width / 2;
          float Y = xnaTexture.Height / 2;
          return new Coord(X, Y);
        } else {
          return new Coord(-1, -1);
        }
#endif
      }
    }
    #endregion

    #region Texture Settings
		/// <summary>
		/// The default texture filter (smoothing) mode,
		/// that will be applied to all new textures.
		/// Existing textures will not be affected.
		/// </summary>
		public static FilterMode DefaultSmoothing { get; set; }

		/// <summary>
		/// The default texture filter (smoothing) mode,
		/// that will be applied to all new textures.
		/// Existing textures will not be affected.
		/// </summary>
		public static RepeatMode DefaultRepeat { get; set; }

    /// <summary>
    /// Gets or sets the texture filter (smoothing) for this texture.
    /// </remarks>
		public FilterMode Smoothing { get; set; }

    /// <summary>
    /// Gets or sets how the texture displays when repeated.
    /// </summary>
		public RepeatMode Repeat { get; set; }
    #endregion

		#region References and Management
		/// <summary>
    /// The number of sprites, models, and other objects using this texture.
    /// </summary>
    public int References {
      get {
        return referenceList.Count;
      }
    }

    /// <summary>
    /// A list of all the objects that reference this texture.
    /// </summary>
    private List<object> referenceList = new List<object>();

    /// <summary>
    /// Gets whether or not any objects in the engine are using this texture at the moment.
    /// </summary>
    public bool InUse {
      get {
        if(References > 0) {
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// Gets whether or not OFE automatically generated this texture for you.
    /// </summary>
    /// <remarks>
    /// OFE is helpful in creating objects such as sprites on the fly, loading the texture
    /// on its own and putting it in a Texture for you.
    /// 
    /// But it's important to remember that when OFE does this, it will manage the Texture object automatically on its own.
    /// If you want to use a single texture in many different places, this can be a problem because OFE will release the
    /// texture as soon as it sees that no more objects are using it, even if you plan to use that texture later.
    /// In this case, you should create the Texture object yourself and handle it manually.
    /// </remarks>
    public bool ManagedByOFE {
      get {
        return managedByOFE;
      }
    }
    private bool managedByOFE;

    /// <summary>
    /// Returns an already-loaded texture of the given file, if it exists.
    /// </summary>
    /// <param name="fileName">The full path and filename, sans extension, of the image file.</param>
    /// <returns>Null if the file has not already been loaded, or it is not auto-managed by OFE</returns>
    internal static Texture GetManagedInstanceOf(string fileName) {
			if(fileName != null && fileName != string.Empty) {
				foreach(Texture texture in All) {
					if(texture != null && Path.GetFullPath(fileName) == Path.GetFullPath(texture.Directory + "//" + texture.FileName)) {
						return texture;
					}
				}
			}

      // Couldn't find it, sorry.
      return null;
    }

    /// <summary>
    /// Adds an object reference, denoting that the given object is using this texture.
    /// </summary>
    /// <param name="o">The referenced object.</param>
    internal void Attach(object o) {
      referenceList.Add(o);
    }

    /// <summary>
    /// Removes an object reference, denoting that the given object is no longer using this texture.
    /// </summary>
    /// <param name="o">The referenced object.</param>
    internal void Detach(object o) {
      referenceList.Remove(o);

      if(ManagedByOFE && !InUse) {
        this.Dispose();
      }
    }

    /// <summary>
    /// Removes ALL references to the given object.
    /// This should be done when an object is dying, in case it had multiple uses of this texture
    /// (for example, in the case of a model).
    /// </summary>
    /// <param name="o">The referenced object.</param>
    internal void DetachAll(object o) {
      while(referenceList.Remove(o)) { }

      if(ManagedByOFE && !InUse) {
        this.Dispose();
      }
    }
    #endregion

    #region Blitting
		/// <summary>
		/// Keep these in memory and reuse the same object.
		/// That way we don't create extra garbage.
		/// </summary>
		private Vertex[] verts;

		//private Matrix worldMatrix;
		private Matrix scaleMatrix = new Matrix();
		private Matrix positionMatrix = new Matrix();
		private Matrix rotationMatrix = new Matrix();

		/// <summary>
		/// Displays a portion of this texture to the screen. Must be called during the draw cycle.
		/// </summary>
		/// <param name="fromUpperLeft">The upper-left corner of the source quad, with (0, 0) being the upper-left corner of the texture,
		/// and (1, 1) being the lower-right corner.</param>
		/// <param name="fromUpperRight">The upper-right corner of the source quad, with (0, 0) being the upper-left corner of the texture,
		/// and (1, 1) being the lower-right corner.</param>
		/// <param name="fromLowerLeft">The lower-left corner of the source quad, with (0, 0) being the upper-left corner of the texture,
		/// and (1, 1) being the lower-right corner.</param>
		/// <param name="fromLowerRight">The lower-right corner of the source quad, with (0, 0) being the upper-left corner of the texture,
		/// and (1, 1) being the lower-right corner.</param>
		/// <param name="toUpperLeft">The upper-left corner of the destination quad, where the source quad will be drawn.</param>
		/// <param name="toUpperRight">The upper-right corner of the destination quad, where the source quad will be drawn.</param>
		/// <param name="toLowerLeft">The lower-left corner of the destination quad, where the source quad will be drawn.</param>
		/// <param name="toLowerRight">The lower-right corner of the destination quad, where the source quad will be drawn.</param>
		/// <param name="z">The z-depth to display at..</param>
		/// <param name="origin">The coordinate to scale and rotate around, in relation to the upper-left corner of the destination.</param>
		/// <param name="flip">How to mirror this blit command in relation to the original texture</param>
		/// <param name="rotation">The rotation, in revolutions, to give this operation.</param>
		/// <param name="scale">The scale to give the x and y axes, to stretch and squash the graphic.</param>
		/// <param name="opacity">The opacity to give, from 0 (invisible) to 1 (fully opaque).</param>
		/// <param name="hue">The hue to give the texture when displayed.</param>
		/// <exception cref="System.InvalidOperationException">Will be thrown if method is called outside the draw cycle.</exception>
		public void Blit(Coord fromUpperLeft, Coord fromUpperRight, Coord fromLowerLeft, Coord fromLowerRight,
			Coord toUpperLeft, Coord toUpperRight, Coord toLowerLeft, Coord toLowerRight,
			float z, Coord origin, Flip flip, float rotation, Coord scale, float opacity, Color hue) {
			// Make sure our outgoing vertex array exists,
			// and if not create it.
			if(verts == null) {
				verts = new Vertex[] { new Vertex(), new Vertex(), new Vertex(), new Vertex() };
			}

			// Factor in opacity.
			hue.A *= opacity;

			// Construct the world matrix.
			DrawCycle.World.BuildIdentity();
			
			if(scale.X != 1 || scale.Y != 1) {
				//DrawCycle.World *= Matrix.CreateScale(scale.X, scale.Y, 1);
				scaleMatrix.Build2dScale(scale.X, scale.Y);
				DrawCycle.World.MultiplyBy(scaleMatrix);
			}
			if(rotation != 0) {
				//DrawCycle.World *= Matrix.Create2dRotation(rotation);
				rotationMatrix.Build2dRotation(rotation);
				DrawCycle.World.MultiplyBy(rotationMatrix);
			}
			//DrawCycle.World *= Matrix.CreateTranslation(toUpperLeft.X + origin.X, toUpperLeft.Y + origin.Y, z);
			positionMatrix.BuildTranslation(toUpperLeft.X + origin.X, toUpperLeft.Y + origin.Y, z);
			DrawCycle.World.MultiplyBy(positionMatrix);

			// Set the texture.
			DrawCycle.SetTexture(this);

			// Make a quad and vert index for this operation.
			Quad quad = new Quad(0, 1, 2, 3);
			//Vertex verts = new Vertex[4];

			// Center the coords around the origin point.
			Coord offset = toUpperLeft + origin;
			toUpperLeft -= offset;
			toUpperRight -= offset;
			toLowerLeft -= offset;
			toLowerRight -= offset;

			// Set the vertices based on the origin of the operation, and adjust by the matrix.			
			verts[0].Set(toUpperLeft.X, toUpperLeft.Y, 0, hue,
				fromUpperLeft.X / Width, fromUpperLeft.Y / Height);
			verts[1].Set(toUpperRight.X, toUpperRight.Y, 0, hue,
				fromUpperRight.X / Width, fromUpperRight.Y / Height);
			verts[2].Set(toLowerRight.X, toLowerRight.Y, 0, hue,
				fromLowerRight.X / Width, fromLowerRight.Y / Height);
			verts[3].Set(toLowerLeft.X, toLowerLeft.Y, 0, hue,
				fromLowerLeft.X / Width, fromLowerLeft.Y / Height);

			// Mirror as needed.
			if(flip == Flip.Horizontal || flip == Flip.Both) {
				Coord temp;
				temp = verts[0].TextureCoords;
				verts[0].TextureCoords = verts[1].TextureCoords;
				verts[1].TextureCoords = temp;
				temp = verts[2].TextureCoords;
				verts[2].TextureCoords = verts[3].TextureCoords;
				verts[3].TextureCoords = temp;
			}
			if(flip == Flip.Vertical || flip == Flip.Both) {
				Coord temp;
				temp = verts[0].TextureCoords;
				verts[0].TextureCoords = verts[3].TextureCoords;
				verts[3].TextureCoords = temp;
				temp = verts[1].TextureCoords;
				verts[1].TextureCoords = verts[2].TextureCoords;
				verts[2].TextureCoords = temp;
			}

			// Send the quad out.
			quad.Out(verts);

		}

    /// <summary>
    /// Displays a portion of this texture to the display. Must be called during the draw cycle.
    /// </summary>
    /// <remarks>Despite the name, OFE uses polygons and textures, not blitting, to display graphics.</remarks>
    /// <param name="displayFrom">The part of the texture to display on the screen.</param>
    /// <param name="displayTo">The area of the stage to copy the texture to.</param>
    /// <param name="z">The z-depth to display at.</param>
		/// <param name="origin">The coordinate to scale and rotate around, in relation to the upper-left corner of the destination.</param>
    /// <param name="flip">How to mirror this blit compared to the original texture.</param>
    /// <param name="rotation">The rotation, in revolutions, to give to this operation.</param>
    /// <param name="scale">The scale to give the x and y axes, to stretch and squash the graphic.</param>
    /// <param name="opacity">The opacity to set.</param>
    /// <param name="hue">The hue to give the texture when displayed.</param>
		/// <exception cref="System.InvalidOperationException">Will be thrown if method is called outside the draw cycle.</exception>
    public void Blit(Rectangle displayFrom, Rectangle displayTo, float z, Coord origin, Flip flip, float rotation, Coord scale, float opacity, Color hue) {
			Blit(new Coord(displayFrom.Left, displayFrom.Top),
				new Coord(displayFrom.Right, displayFrom.Top),
				new Coord(displayFrom.Left, displayFrom.Bottom),
				new Coord(displayFrom.Right, displayFrom.Bottom),
				new Coord(displayTo.Left, displayTo.Top),
				new Coord(displayTo.Right, displayTo.Top),
				new Coord(displayTo.Left, displayTo.Bottom),
				new Coord(displayTo.Right, displayTo.Bottom),
				z,
				new Coord(origin.X, origin.Y),
				flip,
				rotation,
				scale,
				opacity,
				hue);
    }

    /// <summary>
    /// Displays a portion of this texture to the display. Must be called during the draw cycle.
    /// </summary>
    /// <remarks>Despite the name, OFE uses polygons and textures, not blitting, to display graphics.</remarks>
    /// <param name="displayFrom">The part of the texture to display on the screen.</param>
    /// <param name="displayTo">The coordinate to copy the texture to.</param>
    /// <param name="z">The z-depth to display at.</param>
		/// <param name="origin">The coordinate to scale and rotate around, in relation to the upper-left corner of the destination.</param>
    /// <param name="flip">How to mirror this blit compared to the original texture.</param>
    /// <param name="rotation">The rotation, in revolutions, to give to this operation.</param>
    /// <param name="scale">The scale to give the x and y axes, to stretch and squash the graphic.</param>
    /// <param name="opacity">The opacity to set.</param>
    /// <param name="hue">The hue to give the texture when displayed.</param>
		/// <exception cref="System.InvalidOperationException">Will be thrown if method is called outside the draw cycle.</exception>
    public void Blit(Rectangle displayFrom, Coord displayTo, float z, Coord origin, Flip facing, float rotation, Coord scale, float opacity, Color hue) {
      Blit(displayFrom,
        new Rectangle(displayTo.X - origin.X,
          displayTo.Y - origin.Y,
          displayTo.X - origin.X + displayFrom.Width,
          displayTo.Y - origin.Y + displayFrom.Height),
        z,
        origin,
        facing,
        rotation,
        scale,
        opacity,
        hue);
    }
    #endregion

		#region Threading
		/// <summary>
		/// A generic object, used to lock the thread.
		/// </summary>
		private object textureLock = new object();
		#endregion
	}

  /// <summary>
  /// An immutable object that represents a request to load a texture.
  /// </summary>
  internal class LoadTextureRequest {
    internal LoadTextureRequest(Texture texture, TextureData textureData, string fileName, string directory) {
      this.texture = texture;
      this.textureData = textureData;
      this.fileName = fileName;
      this.directory = directory;
    }

    internal readonly Texture texture;
    internal readonly TextureData textureData;
    internal readonly string fileName;
    internal readonly string directory;
  }

	#region Internal Texture Data Class
	/// <summary>
	/// The image data for a texture.
	/// The application must interface with it through OFE to ensure platform independance.
	/// </summary>
  internal class TextureData {
		/// <summary>
		/// Creates a new texture data object.
		/// </summary>
    internal TextureData() {}

		/// <summary>
		/// Whether or not the file was corrupted or missing.
		/// </summary>
    internal bool BadFile = false;

#if XNA
    /// <summary>
    /// The actual texture that makes up the instanced sprite's appearance.
    /// </summary>
    internal Texture2D xnaTexture { get; set; }
#elif OPENGL
    /// <summary>
    /// The bitmap data for the texture, retained so that we'll still have information about the texture's
    /// base image even though we're not retaining a reference to the bitmap itself.
    /// </summary>
    internal BitmapData sysTexture { get; set; }

		/// <summary>
		/// The texture ID for this bitmap, as generated by OpenGL.
		/// </summary>
		//internal uint OGL_Texture;

    /// <summary>
    /// Loads the given bitmap into memory and sets this data and ID for this texture.
    /// </summary>
    /*internal void glLoad(Bitmap image, LoadTextureRequest req) {
      // GL operations need to be executed on the main thread.
      Core.AddUpdateCallback(() => {
        GL.GenTextures(1, out OGL_Texture);
				Display.LogErrors();
        GL.BindTexture(GlEnum.TextureTarget.Texture2D, OGL_Texture);
				Display.LogErrors();
        sysTexture = image.LockBits(new System.Drawing.Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
        GL.TexImage2D(
					GlEnum.TextureTarget.Texture2D,
					0,
					GlEnum.PixelFormat.RGBA,
					sysTexture.Width,
					sysTexture.Height,
					0,
					GlEnum.PixelFormat.BGRA,
					GlEnum.Type.UnsignedByte,
					sysTexture.Scan0);
				Display.LogErrors();
				image.UnlockBits(sysTexture);
        GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.MinFilter, GlEnum.TextureFilter.Linear);
				Display.LogErrors();
				GL.TexParameterI(GlEnum.TextureTarget.Texture2D, GlEnum.TextureParameterName.MagFilter, GlEnum.TextureFilter.Linear);
				Display.LogErrors();

        req.texture.FinishLoad(req);
        DebugLog.Whisper("Grunt successfully loaded texture \"" + req.directory + "//" + req.fileName + "\".");
      });
    }*/
#endif
  }
	#endregion
}
