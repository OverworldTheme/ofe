﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

#if OPENGL
using OpenGL;
#endif

namespace VisionRiders.OFE.Graphics {
	/// <summary>
	/// A shader program is a group of shaders that work together as a group.
	/// Shader programs can be attached to Graphic objects and Entity objects for rendering effects.
	/// </summary>
	public class ShaderProgram {
		#region Initialization
		/// <summary>
		/// Initializes the default shaders.
		/// </summary>
		internal static void Init()
		{
			Default = new ShaderProgram();
			Default.AttachShader(Shader.FromCode(Shader.Type.Vertex, defaultVertexShaderCode));
			Default.AttachShader(Shader.FromCode(Shader.Type.Pixel, defaultPixelShaderCode));
			Display.LogErrors();
		}
		#endregion

		/// <summary>
		/// Creates a new shader program.
		/// </summary>
		public ShaderProgram() {
			// Create our shader list.
			Shaders = new List<Shader>(4);

			// Create our paramter list.
			parameters = new List<Parameter>(16);

#if OPENGL
			// Let's grab ourselves a shader program handle.
			OGL_Program = GL.CreateProgram();
			Display.LogErrors();
#endif
		}

		#region Default Shaders
		/// <summary>
		/// Code for the default vertex shader.
		/// For compatibility, uses the lowest version possible.
		/// </summary>
		private const string defaultVertexShaderCode =
#if OPENGL
			"#version 120\n"

			+ "attribute vec3 position;"
			+ "attribute vec2 texCoords;"
			+ "attribute vec4 color;"

			+ "varying vec4 Color;"
			+ "varying vec2 TexCoords;"

			+ "void main() {"
			+ "  Color = color;"
			+ "  TexCoords = texCoords;"
			+ "  gl_Position = vec4(position.x, position.y, 0.0, 1.0);"
			+ "}";
#endif

		/// <summary>
		/// Code for the default pixel shader.
		/// For compatibility, uses the lowest version possible.
		/// </summary>
		private const string defaultPixelShaderCode =
#if OPENGL
			"#version 120\n"

			+ "uniform sampler2D Texture;"

			+ "varying vec4 Color;"
			+ "varying vec2 TexCoords;"

			+ "void main() {"
			+ "  gl_FragColor = Color * texture2D(Texture, TexCoords);"
			+ "}";
#endif		

		/// <summary>
		/// The default shader program.
		/// </summary>
		internal static ShaderProgram Default;
		#endregion

		#region Disposal
		/// <summary>
		/// Whether or not this shader program has been disposed.
		/// </summary>
		private bool isDisposed;

		/// <summary>
		/// Disposes of this shader program.
		/// </summary>
		public void Dispose() {
			dispose(true);
		}

		/// <summary>
		/// Disposes of this shader program.
		/// </summary>
		/// <param name="disposeManaged">Whether or not to dispose managed objects along with non-managed.</param>
		private void dispose(bool disposeManaged) {
			if(isDisposed)
				return;
			
			isDisposed = true;

#if OPENGL
			// We can delete our OpenGL program now.
			Core.AddUpdateCallback(() => {
				GL.DeleteProgram(OGL_Program);
				Display.LogErrors();
			});
#endif
						
			GC.SuppressFinalize(this);
		}

		~ShaderProgram() {
			dispose(false);
		}
		#endregion

		#region Shaders
		/// <summary>
		/// This shader program's shaders.
		/// </summary>
		internal List<Shader> Shaders { get; set; }

		/// <summary>
		/// The program name for this texture's shaders.
		/// </summary>
		internal uint OGL_Program { get; set; }

		/// <summary>
		/// Gets whether or not all of the shaders in this shader program have finished loading.
		/// </summary>
		public bool IsLoaded {
			get {
				foreach(Shader shader in Shaders) {
					if(!shader.IsLoaded)
						return false;
				}
				return true;
			}
		}

		/// <summary>
		/// Whether or not one of the shaders had a major error.
		/// When this happens, the entire shader program is disabled
		/// until another shader is added or removed.
		/// </summary>
		public bool HasErrors { get; internal set; }

		/// <summary>
		/// Attach a shader (or shaders) to this shader program..
		/// </summary>
		/// <param name="shader">The shaders to attach.</param>
		public void AttachShader(params Shader[] shader) {
#if OPENGL
			if(OGL_Program == 0)
				OGL_Program = GL.CreateProgram();
			Display.LogErrors();
#endif

			if(shader.Length > 0)
				HasErrors = false;

			// Enable any shaders.
			foreach(Shader s in shader) {
				if(!Shaders.Contains(s)) {
					Shaders.Add(s);
#if OPENGL
					s.OnLoaded += delegate(object sender, EventArgs e) {
						Core.AddUpdateCallback(() => {
							if(!s.FileWasBad) {
								Display.LogErrors();
								GL.AttachShader(OGL_Program, s.OGL_Shader);
								Display.LogErrors();
								GL.LinkProgram(OGL_Program);
								Display.LogErrors();
							}
							else
							{
								HasErrors = true;
							}
						});
					};
#endif
				}
			}
		}

		/// <summary>
		/// Detach a shader (or shaders) from this shader program.
		/// </summary>
		/// <param name="shader">The shaders to add.</param>
		public void DetachShader(params Shader[] shader) {
			if(shader.Length > 0)
				HasErrors = false;

			foreach(Shader s in shader) {
				Shaders.Remove(s);
			}
		}
		#endregion

		#region Sending Values
#if OPENGL
		/// <summary>
		/// Used to unbox a matrix value for AssignValue(),
		/// and then send it to OpenGL.
		/// We'll keep it around and overwrite it,
		/// so that we're not generating garbage every frame.
		/// </summary>
		private float[] matrixSender = new float[16];
#endif

		/// <summary>
		/// Called only by the draw cycle when it's time to pass our values to the graphics card.
		/// </summary>
		internal void AssignValues() {
			// Only bother messing with things if something's changes.
			if(ParametersHaveChanged) {
#if OPENGL
				foreach(Parameter p in parameters) {
					Display.LogErrors();

					// Get the location in the shader code.
					int glLoc = GL.GetUniformLocation(OGL_Program, p.Name);
					
					// Set the value.
					if (p.Value is int)
						GL.Uniform1i(glLoc, (int)p.Value);
					else if (p.Value is float)
						GL.Uniform1f(glLoc, (float)p.Value);
					else if (p.Value is Coord)
						GL.Uniform2f(glLoc, ((Coord)p.Value).X, ((Coord)p.Value).Y);
					else if (p.Value is Vector)
						GL.Uniform3f(glLoc, ((Vector)p.Value).X, ((Vector)p.Value).Z, ((Vector)p.Value).Y);
					else if (p.Value is Color)
						GL.Uniform4f(glLoc, ((Color)p.Value).R, ((Color)p.Value).G, ((Color)p.Value).B, ((Color)p.Value).A);
					else if (p.Value is Matrix)
					{
						Matrix m = ((Matrix)p.Value);
						matrixSender[0] = m.M11;
						matrixSender[1] = m.M12;
						matrixSender[2] = m.M13;
						matrixSender[3] = m.M14;
						matrixSender[4] = m.M21;
						matrixSender[5] = m.M22;
						matrixSender[6] = m.M23;
						matrixSender[7] = m.M24;
						matrixSender[8] = m.M31;
						matrixSender[9] = m.M32;
						matrixSender[10] = m.M33;
						matrixSender[11] = m.M34;
						matrixSender[12] = m.M41;
						matrixSender[13] = m.M42;
						matrixSender[14] = m.M43;
						matrixSender[15] = m.M44;
						GL.UniformMatrix4fv(glLoc, 1, false, ref matrixSender);
					}

					// Log any errors.
					Display.LogErrors();
				}
#endif

				ParametersHaveChanged = false;
			}
		}
		#endregion

		#region Passing Values
		/// <summary>
		/// Stores a parameter value to be passed to the graphics card.
		/// All the other value-passing methods call this,
		/// but it's not public so that value types we're set up to handle can
		/// </summary>
		/// <param name="parameter"></param>
		/// <param name="value"></param>
		private void passValue(string parameter, object value) {
			// Check if the parameter already exists.
			for(int i = 0; i < parameters.Count; i++) {
				if(parameters[i].Name == parameter) {
					// Only change the parameter if the value is different.
					if (parameters[i].Value != value)
					{
						parameters[i].Value = value;
						ParametersHaveChanged = true;
					}

					// We're done here.
					return;
				}
			}

			// Looks like it doesn't. Make a new one.
			ParametersHaveChanged = true;
			parameters.Add(new Parameter(parameter, value));
		}

		/// <summary>
		/// Passes an integer value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="value">The value to pass on.</param>
		public void PassInt(string parameter, int value) {
			passValue(parameter, value);
		}

		/// <summary>
		/// Passes a floating-point value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="value">The value to pass on.</param>
		public void PassFloat(string parameter, float value) {
			passValue(parameter, value);
		}

		/// <summary>
		/// Passes a coordinate value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="x">The x-value of the coordinate.</param>
		/// <param name="y">The y-value of the coordinate.</param>
		public void PassCoord(string parameter, float x, float y) {
			PassCoord(parameter, new Coord(x, y));
		}

		/// <summary>
		/// Passes a coordinate value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="value">The value to pass on.</param>
		public void PassCoord(string parameter, Coord value) {
			passValue(parameter, value);
		}

		/// <summary>
		/// Passes a color value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="r">The level of red.</param>
		/// <param name="g">The level of blue.</param>
		/// <param name="b">The level of green.</param>
		public void PassColor(string parameter, float r, float g, float b)
		{
			passValue(parameter, new Color(r, g, b));
		}

		/// <summary>
		/// Passes a color value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="r">The level of red.</param>
		/// <param name="g">The level of blue.</param>
		/// <param name="b">The level of green.</param>
		/// <param name="a">The alpha level.</param>
		public void PassColor(string parameter, float r, float g, float b, float a)
		{
			passValue(parameter, new Color(r, g, b, a));
		}

		/// <summary>
		/// Passes a color value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="color">The color to pass on.</param>
		public void PassColor(string parameter, Color color)
		{
			passValue(parameter, color);
		}

		/// <summary>
		/// Passes a vector value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="x">The x-value of the vector.</param>
		/// <param name="y">The y-value of the vector.</param>
		/// <param name="z">The z-value of the vector.</param>
		public void PassVector(string parameter, float x, float y, float z) {
			PassVector(parameter, new Vector(x, y, x));
		}

		/// <summary>
		/// Passes a vector value to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="vector">The vector to pass on.</param>
		public void PassVector(string parameter, Vector vector) {
			passValue(parameter, vector);
		}

		/// <summary>
		/// Passes a 4x4 matrix to the shader.
		/// </summary>
		/// <param name="parameter">The name of the parameter to pass.</param>
		/// <param name="matrix">The matrix to pass on.</param>
		public void PassMatrix(string parameter, Matrix matrix)
		{
			passValue(parameter, matrix);
		}
		#endregion

		#region Value Storage
		/// <summary>
		/// Gets whether the shader program's parameters have changed recently,
		/// and have not yet been sent to the graphics card.
		/// </summary>
		public bool ParametersHaveChanged { get; private set; }

		/// <summary>
		/// List of parameters set to this shader program.
		/// </summary>
		private List<Parameter> parameters;

		/// <summary>
		/// A single value that can be passed to the shader's actual code.
		/// </summary>
		private class Parameter {
			/// <summary>
			/// Create a new parameter.
			/// </summary>
			/// <param name="name">The name of the parameter.</param>
			/// <param name="value">The parameter's value.</param>
			public Parameter(string name, object value) {
				Name = name;
				Value = value;
			}

			/// <summary>
			/// The name of this parameter.
			/// </summary>
			public string Name;

			/// <summary>
			/// This parameter's value.
			/// </summary>
			public object Value;
		}
		#endregion

		#region API Hooks
#if OPENGL
#endif
		#endregion
	}
}
