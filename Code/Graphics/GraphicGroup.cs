﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A graphic group is a collection of 2d or 3d objects that can be manuipulated and drawn to the screen.
  /// There are two kinds of graphic groups: stages, which are 2d graphic groups, and scenes, which are
  /// 3d graphic groups.
  /// </summary>
  public class GraphicGroup {
		#region Class Constructor
		protected GraphicGroup() {
			BackgroundColor = Color.None;
		}
		#endregion

		/// <summary>
    /// A list of all the graphic groups that are in memory. As long as they are active,
    /// they will be automatically displayed.
    /// </summary>
    protected static List<GraphicGroup> all = new List<GraphicGroup>();

		/// <summary>
    /// Whether or not this graphic group should be drawn to the screen and have its members updated.
    /// </summary>
    public bool IsActive = true;

    /// <summary>
    /// This graphic groups's display priority. Scenes with lower numbers are displayed on top of those
    /// with higher ones.
    /// </summary>
    public float Priority {
      get {
        return priority;
      }
      set {
        this.priority = value;
        GraphicGroup.all.Sort(delegate(GraphicGroup group1, GraphicGroup group2) {
          if(group1 == group2) {
            return 0;
          } else if(group1 == null && group2 != null) {
            return 1;
          } else if(group1 != null && group2 == null) {
            return -1;
          } else if(group1 == null && group2 == null) {
            return 0;
          } else {
            return group1.Priority.CompareTo(group2.Priority) * -1;
          }
        });
      }
    }
    private float priority = 1;

    protected void Init (float priority) {
      GraphicGroup.all.Add(this);
      Priority = priority;
    }

		#region Disposal
		/// <summary>
		/// Whether or not this object has been disposed yet.
		/// </summary>
		private bool isDisposed;

		/// <summary>
    /// Removes this graphic group from the priority list.
    /// </summary>
    public virtual void Dispose() {
			dispose(true);
    }

		/// <summary>
		/// Disposes of this graphic group object.
		/// </summary>
		/// <param name="disposeManaged">Whether or not to dispose non-managed
		/// or only managed objects related to this graphic group.</param>
		protected virtual void dispose(bool disposeNonManaged) {
			if(isDisposed)
				return;
			else {
				if(disposeNonManaged) {
					// Remove ourselves from the list.
					all.Remove(this);
					GC.SuppressFinalize(this);
				}

				isDisposed = true;
			}
		}

		/// <summary>
		/// Finalizer.
		/// </summary>
		~GraphicGroup() {
			dispose(false);
		}
		#endregion

		#region Drawing
		/// <summary>
		/// Gets the render target.
		/// This graphic group will be rendered onto the target texture.
		/// </summary>
		/// <remarks>
		/// <para>By default, the render target is the backbuffer.
		/// You can change targets by using method SetTarget.</para>
		/// <para>For best results, the graphic groups where the render targets is going to be displayed
		/// should have a higher Priority than this graphic group.
		/// This will ensure that this group gets drawn first.
		/// Otherwise, there will be a delay of one frame because the previous frame is going to be the one drawn
		/// to the other graphic group since it's going to happen earlier in the draw cycle.</para>
		/// </remarks>
		public Texture Target { get; private set; }

		/// <summary>
		/// This graphic group's background color when rendered to a texture.		
		/// </summary>
		/// <remarks>
		/// <para>Set the background color to Color.None for transparency,
		/// in which case the texture will be cleared each frame to that transparent value.</para>
		/// <para>Alternatively, you can set the background color to null.
		/// When you do this, the texture will not be cleared before drawing.
		/// This allows you to "layer" multiple graphic groups in a texture.</para>
		/// </remarks>
		public Nullable<Color> BackgroundColor { get; set; }

		/// <summary>
		/// Sets the render target for this graphic group.
		/// </summary>
		/// <param name="texture">The texture that this graphic group will be drawn to.</param>
		public void SetTarget(Texture texture) {
			if(texture == null) {
				Target = null;
			} else {
				Target = texture;
			}
		}

		/// <summary>
    /// Draws all of this graphic groups's active members to the screen.
    /// </summary>
    protected virtual void Draw() {
    }

    /// <summary>
    /// Draws all active graphic groups.
    /// </summary>
    internal static void DrawAll() {
      foreach(GraphicGroup group in all) {
        group.Draw();
      }
    }
		#endregion

		#region Updating
		/// <summary>
		/// Updates all of this graphic groups's members.
		/// </summary>
		protected virtual void Update() {
		}

		/// <summary>
    /// Updates all graphic groups.
    /// </summary>
    internal static void UpdateAll() {
      foreach(GraphicGroup group in all) {
        group.Update();
      }
		}
		#endregion
	}
}
