﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// An enumeration of the different directions that an object, such as a sprite, may face.
  /// </summary>
  public enum Direction {
    /// <summary>
    /// The object is facing left.
    /// </summary>
    Left,

    /// <summary>
    /// The object is facing right.
    /// </summary>
    Right,

    /// <summary>
    /// The object is facing up.
    /// </summary>
    Up,

    /// <summary>
    /// The object is facing down.
    /// </summary>
    Down
  }
}
