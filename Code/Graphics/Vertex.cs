﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A single corner of a polygon.
  /// </summary>
  public class Vertex {
    #region Class Constructors
		public Vertex() : this(Vector.Zero, Color.White, Coord.Zero) { }

    /// <param name="position">The position to give this vertex.</param>
    public Vertex(Vector position) : this(position, Color.White, Coord.Zero) { }

    /// <param name="position">The position to give this vertex.</param>
    /// <param name="color">The color to give this vertex.</param>
    public Vertex(Vector position, Color color) : this(position, color, Coord.Zero) { }

    /// <param name="position">The position to give this vertex.</param>
    /// <param name="textureCoords">The texture coordinates to set to this vertex.</param>
    public Vertex(Vector position, Coord textureCoords) : this(position, Color.White, textureCoords) { }

    /// <param name="x">The location on the X axis to put this vertex.</param>
    /// <param name="y">The location on the Y axis to put this vertex.</param>
    /// <param name="z">The location on the Z axis to put this vertex.</param>
    public Vertex(float x, float y, float z) : this(x, y, z, Color.White, 0, 0) { }

    /// <param name="x">The location on the X axis to put this vertex.</param>
    /// <param name="y">The location on the Y axis to put this vertex.</param>
    /// <param name="z">The location on the Z axis to put this vertex.</param>
    /// <param name="color">The color to give this vertex.</param>
    public Vertex(float x, float y, float z, Color color) : this(x, y, z, color, 0, 0) { }

    /// <param name="x">The location on the X axis to put this vertex.</param>
    /// <param name="y">The location on the Y axis to put this vertex.</param>
    /// <param name="z">The location on the Z axis to put this vertex.</param>
    /// <param name="u">The U (x-axis) value to set for the texture coordinates.</param>
    /// <param name="v">The V (y-axis) value to set for the texture coordinates.</param>
    public Vertex(float x, float y, float z, float u, float v) : this(x, y, z, Color.White, u, v) { }

    /// <param name="x">The location on the X axis to put this vertex.</param>
    /// <param name="y">The location on the Y axis to put this vertex.</param>
    /// <param name="z">The location on the Z axis to put this vertex.</param>
    /// <param name="color">The color to give this vertex.</param>
    /// <param name="u">The U (x-axis) value to set for the texture coordinates.</param>
    /// <param name="v">The V (y-axis) value to set for the texture coordinates.</param>
    public Vertex(float x, float y, float z, Color color, float u, float v) : this(new Vector(x, y, z), color, new Coord(u, v)) { }

		/// <param name="position">The position to give this vertex.</param>
		/// <param name="color">The color to give this vertex.</param>
		/// <param name="textureCoords">The texture coordinates to set to this vertex.</param>
		public Vertex(Vector position, Color color, Coord textureCoords)
			: this(position, Vector.Zero, color, textureCoords) { }

    /// <param name="position">The position to give this vertex.</param>
		/// <param name="normal">Normal values to give this vertex.</param>
    /// <param name="color">The color to give this vertex.</param>
    /// <param name="textureCoords">The texture coordinates to set to this vertex.</param>
    public Vertex(Vector position, Vector normal, Color color, Coord textureCoords) {
      Position = position;
			Normal = normal;
      Color = color;
      TextureCoords = textureCoords;
    }
		#endregion

		#region Constants
		/// <summary>
		/// The number of floats that make up a single vertex.
		/// </summary>
		/// <remarks>
		/// There are nine total:
		/// three for position (x, y, z),
		/// two for texture coordinates (u, v),
		/// and four for color (r, g, b, a).
		/// </remarks>
		public const int Stride = 9;
		#endregion

		#region Operator Overloads
		public override int GetHashCode() {
      return base.GetHashCode();
    }

		public override string ToString()
		{
			return "{" + X + ", " + Y + ", "+Z+"} {" + U + ", " + V+ "} {" + Color.R + ", " + Color.G + ", " + Color.B + ", " + Color.A + "}";
		}

    public override bool Equals(object obj) {
      return base.Equals(obj);
    }

    public static bool operator ==(Vertex v1, Vertex v2) {
      if(v1.Position == v2.Position && v1.Color == v2.Color && v1.TextureCoords == v2.TextureCoords) {
        return true;
      } else {
        return false;
      }
    }

    public static bool operator !=(Vertex v1, Vertex v2) {
      if(v1 == v2) {
        return false;
      } else {
        return true;
      }
    }

    public static Vertex operator +(Vertex vertex, Vector vector) {
      return new Vertex(new Vector(vertex.X + vector.X, vertex.Y + vector.Y, vertex.Z + vector.Z),
        vertex.Color, vertex.TextureCoords);
    }

    public static Vertex operator -(Vertex vertex, Vector vector) {
      return new Vertex(new Vector(vertex.X - vector.X, vertex.Y - vector.Y, vertex.Z - vector.Z),
        vertex.Color, vertex.TextureCoords);
    }

    public static Vertex operator *(Vertex vertex, Matrix matrix) {
      return new Vertex(vertex.Position * matrix, vertex.Color, vertex.TextureCoords);
    }

    public static Vertex operator *(Vertex vertex, Vector vector) {
      return new Vertex(vertex.Position * vector, vertex.Color, vertex.TextureCoords);
    }
    #endregion
		
		#region Manipulation
		/// <summary>
		/// A quick and easy way to set all the values in the vertex.
		/// </summary>
		/// <param name="x">The location on the X axis to put this vertex.</param>
		/// <param name="y">The location on the Y axis to put this vertex.</param>
		/// <param name="z">The location on the Z axis to put this vertex.</param>
		/// <param name="u">The U (x-axis) value to set for the texture coordinates.</param>
		/// <param name="v">The V (y-axis) value to set for the texture coordinates.</param>
		public void Set(float x, float y, float z, float u, float v) {
			X = x;
			Y = y;
			Z = z;
			U = u;
			V = v;
		}

		/// <summary>
		/// A quick and easy way to set all the values in the vertex.
		/// </summary>
		/// <param name="x">The location on the X axis to put this vertex.</param>
		/// <param name="y">The location on the Y axis to put this vertex.</param>
		/// <param name="z">The location on the Z axis to put this vertex.</param>
		/// <param name="color">The color to give this vertex.</param>
		/// <param name="u">The U (x-axis) value to set for the texture coordinates.</param>
		/// <param name="v">The V (y-axis) value to set for the texture coordinates.</param>
		public void Set(float x, float y, float z, Color color, float u, float v) {
			X = x;
			Y = y;
			Z = z;
			Color = color;
			U = u;
			V = v;
		}

		/// <summary>
		/// Copies the values from another vertex.
		/// </summary>
		/// <param name="vertex">The vertex to copy from.</param>
		public void CopyFrom(Vertex vertex) {
			this.Set(vertex.X, vertex.Y, vertex.Z, vertex.Color, vertex.U, vertex.V);
		}

		/// <summary>
		/// Copies this vertex's values to another vertex.
		/// </summary>
		/// <param name="vertex"></param>
		public void CopyTo(Vertex vertex) {
			vertex.CopyFrom(this);
		}
		#endregion

		#region Position
		/// <summary>
    /// This vertex's location in 3d space.
    /// </summary>
		public Vector Position;

    /// <summary>
    /// This vertex's position on the X axis.
    /// </summary>
    public float X {
      get {
        return Position.X;
      }
      set {
        Position.X = value;
      }
    }

    /// <summary>
    /// This vertex's position on the Y axis.
    /// </summary>
    public float Y {
      get {
        return Position.Y;
      }
      set {
        Position.Y = value;
      }
    }

    /// <summary>
    /// This vertex's position on the Y axis.
    /// </summary>
    public float Z {
      get {
        return Position.Z;
      }
      set {
        Position.Z = value;
      }
    }
    #endregion

		#region Normals
		/// <summary>
		/// This vertex's normal.
		/// </summary>
		public Vector Normal;

		/// <summary>
		/// The x-axis normal.
		/// </summary>
		public float NX {
			get { return Normal.X; }
			set { Normal.X = value; }
		}

		/// <summary>
		/// The y-axis normal.
		/// </summary>
		public float NY
		{
			get { return Normal.Y; }
			set { Normal.Y = value; }
		}

		/// <summary>
		/// The z-axis normal.
		/// </summary>
		public float NZ {
			get { return Normal.Z; }
			set { Normal.Z = value; }
		}
		#endregion

		#region Color
		/// <summary>
    /// This vertex's color.
    /// </summary>
		public Color Color;
    #endregion

    #region Texture Coordinates
    /// <summary>
    /// The UV position of the texture on this vertex.
    /// </summary>
		public Coord TextureCoords;

    /// <summary>
    /// The U (x-axis) position of the texture on this vertex.
    /// </summary>
    public float U {
      get {
        return TextureCoords.X;
      }
      set {
        TextureCoords.X = value;
      }
    }

    /// <summary>
    /// The V (y-axis) position of the texture on this vertex.
    /// </summary>
    public float V {
      get {
        return TextureCoords.Y;
      }
      set {
        TextureCoords.Y = value;
      }
    }
    #endregion
  }
}
