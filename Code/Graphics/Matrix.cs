﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
	/// <summary>
	/// A 4x4 matrix of single-precision floats. The fields are numbered with row first and column second.
	/// </summary>
	public class Matrix {
		#region Matrix Contents
		/// <summary>
		/// The first row, first column value of the matrix.
		/// </summary>
		public float M11;

		/// <summary>
		/// The first row, second column value of the matrix.
		/// </summary>
		public float M12;

		/// <summary>
		/// The first row, third column value of the matrix.
		/// </summary>
		public float M13;

		/// <summary>
		/// The first row, fourth column value of the matrix.
		/// </summary>
		public float M14;

		/// <summary>
		/// The second row, first column value of the matrix.
		/// </summary>
		public float M21;

		/// <summary>
		/// The second row, second column value of the matrix.
		/// </summary>
		public float M22;

		/// <summary>
		/// The second row, third column value of the matrix.
		/// </summary>
		public float M23;

		/// <summary>
		/// The second row, fourth column value of the matrix.
		/// </summary>
		public float M24;

		/// <summary>
		/// The third row, first column value of the matrix.
		/// </summary>
		public float M31;

		/// <summary>
		/// The third row, second column value of the matrix.
		/// </summary>
		public float M32;

		/// <summary>
		/// The third row, third column value of the matrix.
		/// </summary>
		public float M33;

		/// <summary>
		/// The third row, fourth column value of the matrix.
		/// </summary>
		public float M34;

		/// <summary>
		/// The fourth row, first column value of the matrix.
		/// </summary>
		public float M41;

		/// <summary>
		/// The fourth row, second column value of the matrix.
		/// </summary>
		public float M42;

		/// <summary>
		/// The fourth row, third column value of the matrix.
		/// </summary>
		public float M43;

		/// <summary>
		/// The fourth row, fourth column value of the matrix.
		/// </summary>
		public float M44;
		#endregion

		#region Readonlys
		/// <summary>
		/// Your classic 4x4 identity matrix.
		/// </summary>
		public static readonly Matrix Identity = new Matrix(
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1);

    /// <summary>
    /// Used to transfrom a right-handed Y-axis-up Matrix to OFE's right-handed Z-axis-up Matrix.
    /// </summary>
    internal static readonly Matrix FlipYZ = new Matrix(
      1, 0, 0, 0,
      0, 0, 1, 0,
      0, 1, 0, 0,
      0, 0, 0, 1);
		#endregion

		#region Constructors
		/// <summary>
		/// Creates a new, empty matrix.
		/// </summary>
		public Matrix() { }

		public Matrix(float m11, float m12, float m13, float m14,
			float m21, float m22, float m23, float m24,
			float m31, float m32, float m33, float m34,
			float m41, float m42, float m43, float m44)
		{
			M11 = m11;
			M12 = m12;
			M13 = m13;
			M14 = m14;
			M21 = m21;
			M22 = m22;
			M23 = m23;
			M24 = m24;
			M31 = m31;
			M32 = m32;
			M33 = m33;
			M34 = m34;
			M41 = m41;
			M42 = m42;
			M43 = m43;
			M44 = m44;
		}
		#endregion

		#region Matrix Setting
		/// <summary>
		/// Quick way to set all the fields in the matrix.
		/// </summary>
		/// <param name="m11">The value for row 1, column 1.</param>
		/// <param name="m12">The value for row 1, column 2.</param>
		/// <param name="m13">The value for row 1, column 3.</param>
		/// <param name="m14">The value for row 1, column 4.</param>
		/// <param name="m21">The value for row 2, column 1.</param>
		/// <param name="m22">The value for row 2, column 2.</param>
		/// <param name="m23">The value for row 2, column 3.</param>
		/// <param name="m24">The value for row 2, column 4.</param>
		/// <param name="m31">The value for row 3, column 1.</param>
		/// <param name="m32">The value for row 3, column 2.</param>
		/// <param name="m33">The value for row 3, column 3.</param>
		/// <param name="m34">The value for row 3, column 4.</param>
		/// <param name="m41">The value for row 4, column 1.</param>
		/// <param name="m42">The value for row 4, column 2.</param>
		/// <param name="m43">The value for row 4, column 3.</param>
		/// <param name="m44">The value for row 4, column 4.</param>
		public void Set(float m11, float m12, float m13, float m14,
			float m21, float m22, float m23, float m24,
			float m31, float m32, float m33, float m34,
			float m41, float m42, float m43, float m44)
		{
			M11 = m11;
			M12 = m12;
			M13 = m13;
			M14 = m14;

			M21 = m21;
			M22 = m22;
			M23 = m23;
			M24 = m24;

			M31 = m31;
			M32 = m32;
			M33 = m33;
			M34 = m34;

			M41 = m41;
			M42 = m42;
			M43 = m43;
			M44 = m44;
		}

		/// <summary>
		/// Copy values from another matrix to this matrix.
		/// Any values already in the matrix will be lost.
		/// </summary>
		/// <param name="matrix">The matrix to copy to this one.</param>
		public void Copy(Matrix matrix)
		{
			M11 = matrix.M11;
			M12 = matrix.M12;
			M13 = matrix.M13;
			M14 = matrix.M14;

			M21 = matrix.M21;
			M22 = matrix.M22;
			M23 = matrix.M23;
			M24 = matrix.M24;

			M31 = matrix.M31;
			M32 = matrix.M32;
			M33 = matrix.M33;
			M34 = matrix.M34;

			M41 = matrix.M41;
			M42 = matrix.M42;
			M43 = matrix.M43;
			M44 = matrix.M44;
		}
		#endregion

		#region Operators
		/*public static Matrix operator +(Matrix a, Matrix b) {
      Matrix result = new Matrix(
        a.M11 + b.M11,
        a.M12 + b.M12,
        a.M13 + b.M13,
        a.M14 + b.M14,
        
        a.M21 + b.M21,
        a.M22 + b.M22,
        a.M23 + b.M23,
        a.M24 + b.M24,
        
        a.M31 + b.M31,
        a.M32 + b.M32,
        a.M33 + b.M33,
        a.M34 + b.M34,
        
        a.M41 + b.M41,
        a.M42 + b.M42,
        a.M43 + b.M43,
        a.M44 + b.M44);
      return result;
    }

    public static Matrix operator -(Matrix a, Matrix b) {
      Matrix result = new Matrix(
        a.M11 - b.M11,
        a.M12 - b.M12,
        a.M13 - b.M13,
        a.M14 - b.M14,

        a.M21 - b.M21,
        a.M22 - b.M22,
        a.M23 - b.M23,
        a.M24 - b.M24,

        a.M31 - b.M31,
        a.M32 - b.M32,
        a.M33 - b.M33,
        a.M34 - b.M34,

        a.M41 - b.M41,
        a.M42 - b.M42,
        a.M43 - b.M43,
        a.M44 - b.M44);
      return result;
    }

    public static Matrix operator *(Matrix mB, Matrix mA) {
      Matrix result = new Matrix();
      result.M11 = mA.M11 * mB.M11 + mA.M12 * mB.M21 + mA.M13 * mB.M31 + mA.M14 * mB.M41;
      result.M12 = mA.M11 * mB.M12 + mA.M12 * mB.M22 + mA.M13 * mB.M32 + mA.M14 * mB.M42;
      result.M13 = mA.M11 * mB.M13 + mA.M12 * mB.M23 + mA.M13 * mB.M33 + mA.M14 * mB.M43;
      result.M14 = mA.M11 * mB.M14 + mA.M12 * mB.M24 + mA.M13 * mB.M34 + mA.M14 * mB.M44;
      
      result.M21 = mA.M21 * mB.M11 + mA.M22 * mB.M21 + mA.M23 * mB.M31 + mA.M24 * mB.M41;
      result.M22 = mA.M21 * mB.M12 + mA.M22 * mB.M22 + mA.M23 * mB.M32 + mA.M24 * mB.M42;
      result.M23 = mA.M21 * mB.M13 + mA.M22 * mB.M23 + mA.M23 * mB.M33 + mA.M24 * mB.M43;
      result.M24 = mA.M21 * mB.M14 + mA.M22 * mB.M24 + mA.M23 * mB.M34 + mA.M24 * mB.M44;
      
      result.M31 = mA.M31 * mB.M11 + mA.M32 * mB.M21 + mA.M33 * mB.M31 + mA.M34 * mB.M41;
      result.M32 = mA.M31 * mB.M12 + mA.M32 * mB.M22 + mA.M33 * mB.M32 + mA.M34 * mB.M42;
      result.M33 = mA.M31 * mB.M13 + mA.M32 * mB.M23 + mA.M33 * mB.M33 + mA.M34 * mB.M43;
      result.M34 = mA.M31 * mB.M14 + mA.M32 * mB.M24 + mA.M33 * mB.M34 + mA.M34 * mB.M44;
      
      result.M41 = mA.M41 * mB.M11 + mA.M42 * mB.M21 + mA.M43 * mB.M31 + mA.M44 * mB.M41;
      result.M42 = mA.M41 * mB.M12 + mA.M42 * mB.M22 + mA.M43 * mB.M32 + mA.M44 * mB.M42;
      result.M43 = mA.M41 * mB.M13 + mA.M42 * mB.M23 + mA.M43 * mB.M33 + mA.M44 * mB.M43;
      result.M44 = mA.M41 * mB.M14 + mA.M42 * mB.M24 + mA.M43 * mB.M34 + mA.M44 * mB.M44;
      return result;
    }*/

		/// <summary>
		/// Multiplies one matrix by another,
		/// and then stores the results in this matrix.
		/// The current values of this matrix will be overwritten.
		/// </summary>
		/// <param name="matrix1">The first matrix.</param>
		/// <param name="matrix2">The second matrix.</param>
		public void Multiply(Matrix matrix1, Matrix matrix2)
		{
			this.M11 = matrix2.M11 * matrix1.M11 + matrix2.M12 * matrix1.M21 + matrix2.M13 * matrix1.M31 + matrix2.M14 * matrix1.M41;
			this.M12 = matrix2.M11 * matrix1.M12 + matrix2.M12 * matrix1.M22 + matrix2.M13 * matrix1.M32 + matrix2.M14 * matrix1.M42;
			this.M13 = matrix2.M11 * matrix1.M13 + matrix2.M12 * matrix1.M23 + matrix2.M13 * matrix1.M33 + matrix2.M14 * matrix1.M43;
			this.M14 = matrix2.M11 * matrix1.M14 + matrix2.M12 * matrix1.M24 + matrix2.M13 * matrix1.M34 + matrix2.M14 * matrix1.M44;

			this.M21 = matrix2.M21 * matrix1.M11 + matrix2.M22 * matrix1.M21 + matrix2.M23 * matrix1.M31 + matrix2.M24 * matrix1.M41;
			this.M22 = matrix2.M21 * matrix1.M12 + matrix2.M22 * matrix1.M22 + matrix2.M23 * matrix1.M32 + matrix2.M24 * matrix1.M42;
			this.M23 = matrix2.M21 * matrix1.M13 + matrix2.M22 * matrix1.M23 + matrix2.M23 * matrix1.M33 + matrix2.M24 * matrix1.M43;
			this.M24 = matrix2.M21 * matrix1.M14 + matrix2.M22 * matrix1.M24 + matrix2.M23 * matrix1.M34 + matrix2.M24 * matrix1.M44;

			this.M31 = matrix2.M31 * matrix1.M11 + matrix2.M32 * matrix1.M21 + matrix2.M33 * matrix1.M31 + matrix2.M34 * matrix1.M41;
			this.M32 = matrix2.M31 * matrix1.M12 + matrix2.M32 * matrix1.M22 + matrix2.M33 * matrix1.M32 + matrix2.M34 * matrix1.M42;
			this.M33 = matrix2.M31 * matrix1.M13 + matrix2.M32 * matrix1.M23 + matrix2.M33 * matrix1.M33 + matrix2.M34 * matrix1.M43;
			this.M34 = matrix2.M31 * matrix1.M14 + matrix2.M32 * matrix1.M24 + matrix2.M33 * matrix1.M34 + matrix2.M34 * matrix1.M44;

			this.M41 = matrix2.M41 * matrix1.M11 + matrix2.M42 * matrix1.M21 + matrix2.M43 * matrix1.M31 + matrix2.M44 * matrix1.M41;
			this.M42 = matrix2.M41 * matrix1.M12 + matrix2.M42 * matrix1.M22 + matrix2.M43 * matrix1.M32 + matrix2.M44 * matrix1.M42;
			this.M43 = matrix2.M41 * matrix1.M13 + matrix2.M42 * matrix1.M23 + matrix2.M43 * matrix1.M33 + matrix2.M44 * matrix1.M43;
			this.M44 = matrix2.M41 * matrix1.M14 + matrix2.M42 * matrix1.M24 + matrix2.M43 * matrix1.M34 + matrix2.M44 * matrix1.M44;
		}

		/// <summary>
		/// Multiplies the given matrices, in order,
		/// and then stores the results in this matrix.
		/// The current values of this matrix will be overwritten.
		/// </summary>
		/// <param name="matrix1">The first matrix.</param>
		/// <param name="matrix2">The second matrix.</param>
		/// <param name="matrix3">The third matrix.</param>
		public void Multiply(Matrix matrix1, Matrix matrix2, Matrix matrix3)
		{
			this.Multiply(matrix1, matrix2);
			this.Multiply(this, matrix3);
		}

		/// <summary>
		/// Multiplies the given matrices, in order,
		/// and then stores the results in this matrix.
		/// The current values of this matrix will be overwritten.
		/// </summary>
		/// <param name="matrix1">The first matrix.</param>
		/// <param name="matrix2">The second matrix.</param>
		/// <param name="matrix3">The third matrix.</param>
		/// <param name="matrix4">The fourth matrix.</param>
		public void Multiply(Matrix matrix1, Matrix matrix2, Matrix matrix3, Matrix matrix4)
		{
			this.Multiply(matrix1, matrix2);
			this.Multiply(this, matrix3);
			this.Multiply(this, matrix4);
		}

		/// <summary>
		/// Multiplies this matrix times a second matrix.
		/// The current values in this matrix will be overwritten.
		/// </summary>
		/// <param name="matrix">The matrix to multiply by.</param>
		public void MultiplyBy(Matrix matrix)
		{
			this.Multiply(this, matrix);
		}
    #endregion

		#region ToString
		public override string ToString()
		{
			return "{" + M11 + ", " + M12 + ", " + M13 + ", " + M14
				+ "} {" + M21 + ", " + M22 + ", " + M23 + ", " + M24
				+ "} {" + M31 + ", " + M32 + ", " + M33 + ", " + M34
				+ "} {" + M41 + ", " + M42 + ", " + M43 + ", " + M44 + "}";
		}
		#endregion

		#region Extraction
		/// <summary>
    /// Returns the (approximate) yaw-pitch-roll rotation information from this matrix.
    /// </summary>
    /// <remarks>This only works if the matrix was multiplied in the order: yaw, pitch,
    /// then roll. By default, this is how OFE combines them. If you're doing something
    /// different on your own using this matrix struct, you will need to do the math
    /// yourself.
    /// 
    /// Special thanks to Robert Blum's page for the math on this.</remarks>
    /// <returns>A vector with the rotations stored in their respective axes:
    /// pitch in x, roll in y, and yaw in z. The values are rounded to the third
    /// decimal point for clarity.</returns>
    public Vector GetRotation() {
      // Oh boy! Here we go!
      // First, let's go ahead and grab the scaling value, since we'll need it later,
      // and define some floats for the yaw, pitch, and roll values.
      Vector scale = this.GetScale();
      float yaw = 0;
      float pitch = 0;
      float roll = 0;

      // Pitch is the easy one. We just run an Asin, and remember to factor out the scale.
      pitch = (float)Math.Asin(-this.M32 / scale.Z);

      // Yaw and roll will take a bit more work...
      // First we check if the pitch is equal to, or near enough, to 1 or -1. This will determine
      // which Matrix cells to pull from. Then we'll need to run a couple Atan2s, and factor the
      // scale out, which is stored in the same part of the matrix.
      if(pitch < -0.999f || pitch > 0.999) {
        yaw = -(float)Math.Atan2(this.M21, this.M11);
        roll = (float)Math.Atan2(this.M31 * scale.Z, this.M32);
      } else {
        yaw = -(float)Math.Atan2(this.M31, this.M33);
        roll = (float)Math.Atan2(this.M12 * scale.Y, this.M22 * scale.X);
      }

      // Done! Convert to revolutions and return.
      return new Vector(Mather.Round(pitch, 6),
				Mather.Round(roll, 6),
				Mather.Round(yaw, 6));
    }

    /// <summary>
    /// Returns the scaling information from this matrix.
    /// </summary>
    /// <returns>An x, y, z vector. The values are scaled to the third decimal place.</returns>
    public Vector GetScale() {
      // Not too difficult. We need to take each row and compute their length.
      // This will give us the scale for each axis.
      Vector x = new Vector(M11, M12, M13);
      Vector y = new Vector(M21, M22, M23);
      Vector z = new Vector(M31, M32, M33);

      // TODO: Check for negative scaling and... do things... about it.

      return new Vector((float)Math.Round(x.Magnitude, 3),
        (float)Math.Round(y.Magnitude, 3),
        (float)Math.Round(z.Magnitude, 3));
    }

    /// <summary>
    /// Returns the translation information from this matrix (the position-changing values).
    /// </summary>
    /// <returns>An x, y, z vector.</returns>
    public Vector GetTranslation() {
      // This is the easy one. :D
      return new Vector(M41, M42, M43);
    }
		#endregion

		#region Identity Matrix
		/// <summary>
		/// Changes this to a classic identity matrix.
		/// </summary>
		public void BuildIdentity()
		{
			Set(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1);
		}
		#endregion

		#region Rotation Matrix
		/// <summary>
		/// Creates a rotation matrix from the given yaw, pitch, and roll values.
		/// </summary>
		/// <remarks>Rotation is combined in the following order: yaw, then pitch, then roll.</remarks>
		/// <param name="yaw">The "left-right" rotation to set, given in revolutions.
		/// Increasing the value creates clockwise rotation, when viewed from above.</param>
		/// <param name="pitch">The "up-down" rotation to set, given in revolutions.
		/// Increasing the value increases/raises the pitch.</param>
		/// <param name="roll">The roll rotation to set, given in revolutions.
		/// Increasing the value creates clockwise rotation, when viewed from behind.</param>
		/// <returns></returns>
		public static Matrix CreateFromYawPitchRoll(float yaw, float pitch, float roll) {
      // Translate values to radians. We'll call them a, b, and g for "alpha", "beta",
      // and "gamma", because that's how mathmaticians roll (pun unintended).
			// Values are inverted so that they rotate as defined in OFE's coordinate system.
      float a = Mather.RevolutionsToRadians(-yaw);
      float b = Mather.RevolutionsToRadians(-pitch);
      float g = Mather.RevolutionsToRadians(-roll);

      return new Matrix(
        sin(g) * sin(b) * sin(a) + cos(g) * cos(a), cos(b) * sin(a), cos(g) * sin(b) * sin(a) - sin(g) * cos(a), 0,
        cos(a) * sin(g) * sin(b) - sin(a) * cos(g), cos(b) * cos(a), cos(g) * cos(a) * sin(b) + sin(g) * sin(a), 0,
        sin(g) * cos(b), -sin(b), cos(g) * cos(b), 0,
        0, 0, 0, 1);
    }

		/// <summary>
		/// Changes this to a rotation matrix using the given yaw, pitch, and roll values.
		/// </summary>
		/// <remarks>Rotation is combined in the following order: yaw, then pitch, then roll.</remarks>
		/// <param name="yaw">The "left-right" rotation to set, given in revolutions.
		/// Increasing the value creates clockwise rotation, when viewed from above.</param>
		/// <param name="pitch">The "up-down" rotation to set, given in revolutions.
		/// Increasing the value increases/raises the pitch.</param>
		/// <param name="roll">The roll rotation to set, given in revolutions.
		/// Increasing the value creates clockwise rotation, when viewed from behind.</param>
		/// <returns></returns>
		public void BuildFromYawPitchRoll(float yaw, float pitch, float roll)
		{
			// Translate values to radians. We'll call them a, b, and g for "alpha", "beta",
			// and "gamma", because that's how mathmaticians roll (pun unintended).
			// Values are inverted so that they rotate as defined in OFE's coordinate system.
			float a = Mather.RevolutionsToRadians(-yaw);
			float b = Mather.RevolutionsToRadians(-pitch);
			float g = Mather.RevolutionsToRadians(-roll);

			Set(
				sin(g) * sin(b) * sin(a) + cos(g) * cos(a), cos(b) * sin(a), cos(g) * sin(b) * sin(a) - sin(g) * cos(a), 0,
				cos(a) * sin(g) * sin(b) - sin(a) * cos(g), cos(b) * cos(a), cos(g) * cos(a) * sin(b) + sin(g) * sin(a), 0,
				sin(g) * cos(b), -sin(b), cos(g) * cos(b), 0,
				0, 0, 0, 1);
		}

		/// <summary>
		/// Changes this to a rotation matrix built from the given quaternion.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <param name="w"></param>
		public void BuildFromQuaternion(float x, float y, float z, float w)
		{
			Set(
				1 - (2 * y * y) - (2 * z * z), (2 * x * y) - (2 * z * w), (2 * x * z) + (2 * y * w), 0,
				(2 * x * y) + (2 * z * w), 1 - (2 * x * x) - (2 * z * z), (2 * y * z) - (2 * x * w), 0,
				(2 * x * z) - (2 * y * w), (2 * y * z) + (2 * x * w), 1 - (2 * x * x) - (2 * y * y), 0,
				0, 0, 0, 1);
		}

		/// <summary>
		/// Changes this to a rotation matrix build from the given quaternion.
		/// </summary>
		/// <param name="quaternion">The quaternion to build the matrix from.</param>
		public void BuildFromQuaternion(Quaternion quaternion)
		{
			BuildFromQuaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
		}

		/// <summary>
		/// Creates a rotation matrix from the given quaternion.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <param name="w"></param>
		/// <returns></returns>
		public static Matrix CreateFromQuaternion(float x, float y, float z, float w)
		{
			return new Matrix(
				1 - (2 * y * y) - (2 * z * z), (2 * x * y) - (2 * z * w), (2 * x * z) + (2 * y * w), 0,
				(2 * x * y) + (2 * z * w), 1 - (2 * x * x) - (2 * z * z), (2 * y * z) - (2 * x * w), 0,
				(2 * x * z) - (2 * y * w), (2 * y * z) + (2 * x * w), 1 - (2 * x * x) - (2 * y * y), 0,
				0, 0, 0, 1);
		}

		/// <summary>
		/// Creates a rotation matrix from the given quaternion.
		/// </summary>
		/// <param name="quaternion">The quaternion to create a matrix from.</param>
		/// <returns></returns>
		public static Matrix CreateFromQuaternion(Quaternion quaternion)
		{
			return CreateFromQuaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);			
		}

    /// <summary>
    /// Creates a rotation matrix specifically for a 2d object,
		/// which only has one axis to rotate on.
    /// </summary>
    /// <param name="rotation">The amount of rotation to give the matrix, in revolutions.</param>
    /// <returns></returns>
    public static Matrix Create2dRotation(float rotation) {
      return Matrix.CreateFromYawPitchRoll(0, 0, rotation);
    }

		/// <summary>
		/// Changes this to a rotation matrix specifically for a 2d object,
		/// which only has one axis to rotate on.
		/// </summary>
		/// <param name="rotation">The amount of rotation to give the matrix, in revolutions.</param>
		/// <returns></returns>
		public void Build2dRotation(float rotation)
		{
			BuildFromYawPitchRoll(0, 0, rotation);
		}
		#endregion

		#region Perspective Matrix
		/// <summary>
		/// Creates a look-at matrix using the given position and target.
		/// </summary>
		/// <remarks>OFE's up-axis will be supplied automatically.</remarks>
		/// <param name="position">The position of the entity this matrix is being created from.</param>
		/// <param name="target">The camera's target; the point in world space that the entity is supposed to look at.</param>
		/// <returns></returns>
		public static Matrix CreateLookAt(Vector position, Vector target)
		{
			return CreateLookAt(position, target, Vector.Up);
		}

    /// <summary>
    /// Creates a look-at Matrix using the given position and target.
    /// </summary>
    /// <param name="cameraPosition">The position of the camera this matrix is being created from.</param>
    /// <param name="cameraTarget">The camera's target; the point in world space that the camera is supposed to look at.</param>
    /// <param name="up">The up direction.</param>
    /// <returns></returns>
    public static Matrix CreateLookAt(Vector cameraPosition, Vector cameraTarget, Vector up) {
			Vector axisZ = Vector.Normalize(cameraTarget - cameraPosition);
      Vector axisX = Vector.Normalize(Vector.Cross(up, axisZ));
      Vector axisY = Vector.Cross(axisZ, axisX);

      return new Matrix(
        axisX.X, axisY.X, axisZ.X, 0,
				axisX.Y, axisY.Y, axisZ.Y, 0,
        axisX.Z, axisY.Z, axisZ.Z, 0,				
        -Vector.Dot(axisX, cameraPosition), -Vector.Dot(axisY, cameraPosition), -Vector.Dot(axisZ, cameraPosition), 1);
    }

		/// <summary>
		/// Creates a projection matrix based on a field-of-view value.
		/// </summary>
		/// <param name="fieldOfView">The field-of-view value, in degrees.</param>
		/// <param name="aspectRatio">The aspect ratio.</param>
		/// <param name="near">The distance to the near clippling plane.</param>
		/// <param name="far">The distance to the far clipping plane.</param>
		/// <returns></returns>
		public static Matrix CreatePerspectiveFOV(float fieldOfView, float aspectRatio, float near, float far) {
      float height = 1f / Mather.Tan(Mather.DegreesToRevolutions(fieldOfView) / 2f);
      float width = height / aspectRatio;
			
			// This produces a left-handed perspective FOV.
			// OpenGL's cordinate system is left-handed.
			return new Matrix(
				width, 0, 0, 0,
				0, height, 0, 0,
				0, 0, far / (far - near), 1,
				0, 0, -near * far / (far - near), 0);
    }
		#endregion

		#region Scale Matrix
		/// <summary>
		/// Creates a scaling matrix.
		/// </summary>
		/// <param name="x">The value to scale on the x-axis.</param>
		/// <param name="y">The value to scale on the y-axis.</param>
		/// <param name="z">The value to scale on the z-axis.</param>
		/// <returns></returns>
		public static Matrix CreateScale(float x, float y, float z) {
      return new Matrix(
        x, 0, 0, 0,
        0, y, 0, 0,
        0, 0, z, 0,
        0, 0, 0, 1);
    }

		/// <summary>
		/// Changes this to a scaling matrix.
		/// </summary>
		/// <param name="x">The value to scale on the x-axis.</param>
		/// <param name="y">The value to scale on the y-axis.</param>
		/// <param name="z">The value to scale on the z-axis.</param>
		/// <returns></returns>
		public void BuildScale(float x, float y, float z)
		{
			Set(
				x, 0, 0, 0,
				0, y, 0, 0,
				0, 0, z, 0,
				0, 0, 0, 1);
		}

		/// <summary>
		/// Creates a scaling matrix.
		/// </summary>
		/// <param name="scale">A vector containing the scaling values for each axis.</param>
		public static Matrix CreateScale(Vector scale)
		{
			return CreateScale(scale.X, scale.Y, scale.Z);
		}

		/// <summary>
		/// Changes this to a scaling matrix.
		/// </summary>
		/// <param name="scale">A vector containing the scaling values for each axis.</param>
		public void BuildScale(Vector scale)
		{
			BuildScale(scale.X, scale.Y, scale.Z);
		}

		/// <summary>
		/// Creates a 2d scale matrix that only affects the x and y axes.
		/// The z-axis will always remain at 1.
		/// </summary>
		/// <param name="x">The value to scale on the x-axis.</param>
		/// <param name="y">The value to scale on the y-axis.</param>
		/// <returns></returns>
		public static Matrix Create2dScale(float x, float y) {
      return Matrix.CreateScale(x, y, 1);
    }

		/// <summary>
		/// Changes this to a 2d scale matrix that only affects the x and y axes.
		/// The z-axis will always remain at 1.
		/// </summary>
		/// <param name="x">The value to scale on the x-axis.</param>
		/// <param name="y">The value to scale on the y-axis.</param>
		/// <returns></returns>
		public void Build2dScale(float x, float y)
		{
			BuildScale(x, y, 1);
		}

		/// <summary>
		/// Creates a 2d scale matrix that only affects the x and y axes.
		/// The z-axis will always remain at 1.
		/// </summary>
		/// <param name="x">The value to scale on both the x and y axes.</param>
		/// <returns></returns>
		public static Matrix Create2dScale(float xy) {
      return Matrix.CreateScale(xy, xy, 1);
    }

		/// <summary>
		/// Changes this to a 2d scale matrix that only affects the x and y axes.
		/// The z-axis will always remain at 1.
		/// </summary>
		/// <param name="x">The value to scale on both the x and y axes.</param>
		/// <returns></returns>
		public void Build2dScale(float xy)
		{
			BuildScale(xy, xy, 1);
		}
		#endregion

		#region Translation Matrix
		/// <summary>
		/// Creates a translation matrix, used to transform position.
		/// </summary>
		/// <param name="position">The position to create a translation matrix from.</param>
		/// <returns></returns>
		public static Matrix CreateTranslation(Vector position)
		{
			return new Matrix(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				position.X, position.Y, position.Z, 1);
		}

		/// <summary>
		/// Changes this matrix into a transformation matrix,
		/// used to transform position.
		/// </summary>
		/// <param name="position">The position to build the translation matrix from.</param>
		public void BuildTranslation(Vector position)
		{
			Set(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				position.X, position.Y, position.Z, 1);
		}

    /// <summary>
    /// Creates a translation matrix, used to transform position.
    /// </summary>
    /// <param name="x">The x-coordinate position to set.</param>
    /// <param name="y">The y-coordinate position to set.</param>
    /// <param name="z">The z-coordinate position to set.</param>
    /// <returns></returns>
    public static Matrix CreateTranslation(float x, float y, float z) {
      return new Matrix(
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        x, y, z, 1);
    }

		/// <summary>
		/// Changes this matrix into a transformation matrix,
		/// used to transform position.
		/// </summary>
		/// <param name="x">The x-coordinate position to set.</param>
		/// <param name="y">The y-coordinate position to set.</param>
		/// <param name="z">The z-coordinate position to set.</param>
		/// <returns></returns>
		public void BuildTranslation(float x, float y, float z)
		{
			Set(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				x, y, z, 1);
		}

		/// <summary>
		/// Creates a translation matrix for two-axis translation. Z will remain at 0.
		/// You should use the normal Matrix.Translation method, even in 2d, if you need z-depth translation.
		/// Otherwise, this is more convenient.
		/// </summary>
		/// <param name="x">The x-coordinate position to set.</param>
		/// <param name="y">The y-coordinate position to set.</param>
		/// <returns></returns>
		public static Matrix Create2dTranslation(float x, float y) {
      return Matrix.CreateTranslation(x, y, 0);
    }

		/// <summary>
		/// Changes this to a translation matrix for two-axis translation. Z will remain at 0.
		/// You should use the normal Matrix.Translation method, even in 2d, if you need z-depth translation.
		/// Otherwise, this is more convenient.
		/// </summary>
		/// <param name="x">The x-coordinate position to set.</param>
		/// <param name="y">The y-coordinate position to set.</param>
		/// <returns></returns>
		public void Build2dTranslation(float x, float y)
		{
			BuildTranslation(x, y, 0);
		}
		#endregion

		#region Math
		/// <summary>
		/// Calculates the determinant of this matrix.
		/// </summary>
		/// <returns></returns>
		public float Determinant() {
      // Yes, this is a horrible, crummy way to write this out. Shut up.

      return M14 * M23 * M32 * M41 - M13 * M24 * M32 * M41 -
        M14 * M22 * M33 * M41 + M12 * M24 * M33 * M41 +
        M13 * M22 * M34 * M41 - M12 * M23 * M34 * M41 -
        M14 * M23 * M31 * M42 + M13 * M24 * M31 * M42 +
        M14 * M21 * M33 * M42 - M11 * M24 * M33 * M42 -
        M13 * M21 * M34 * M42 + M11 * M23 * M34 * M42 +
        M14 * M22 * M31 * M43 - M12 * M24 * M31 * M43 -
        M14 * M21 * M32 * M43 + M11 * M24 * M32 * M43 +
        M12 * M21 * M34 * M43 - M11 * M22 * M34 * M43 -
        M13 * M22 * M31 * M44 + M12 * M23 * M31 * M44 +
        M13 * M21 * M32 * M44 - M11 * M23 * M32 * M44 -
        M12 * M21 * M33 * M44 + M11 * M22 * M33 * M44;
    }

		/// <summary>
		/// Calculates and returns the inversion of this matrix.
		/// </summary>
		/// <returns></returns>
		public Matrix Inverted()
		{
			return Invert(this);
		}

    /// <summary>
    /// Returns a new matrix that is an inversion of the given one.
    /// </summary>
    /// <param name="matrix">The matrix to get an inversion of.</param>
    public static Matrix Invert(Matrix matrix) {
      // Yes, I am aware of how ugly this is.
      Matrix inversion = new Matrix();
      float m11 = matrix.M11;
      float m12 = matrix.M12;
      float m13 = matrix.M13;
      float m14 = matrix.M14;
      float m21 = matrix.M21;
      float m22 = matrix.M22;
      float m23 = matrix.M23;
      float m24 = matrix.M24;
      float m31 = matrix.M31;
      float m32 = matrix.M32;
      float m33 = matrix.M33;
      float m34 = matrix.M34;
      float m41 = matrix.M41;
      float m42 = matrix.M42;
      float m43 = matrix.M43;
      float m44 = matrix.M44;

      inversion.M11 = m23 * m34 * m42 - m24 * m33 * m42 + m24 * m32 * m43 - m22 * m34 * m43 - m23 * m32 * m44 + m22 * m33 * m44;
      inversion.M12 = m14 * m33 * m42 - m13 * m34 * m42 - m14 * m32 * m43 + m12 * m34 * m43 + m13 * m32 * m44 - m12 * m33 * m44;
      inversion.M13 = m13 * m24 * m42 - m14 * m23 * m42 + m14 * m22 * m43 - m12 * m24 * m43 - m13 * m22 * m44 + m12 * m23 * m44;
      inversion.M14 = m14 * m23 * m32 - m13 * m24 * m32 - m14 * m22 * m33 + m12 * m24 * m33 + m13 * m22 * m34 - m12 * m23 * m34;
      inversion.M21 = m24 * m33 * m41 - m23 * m34 * m41 - m24 * m31 * m43 + m21 * m34 * m43 + m23 * m31 * m44 - m21 * m33 * m44;
      inversion.M22 = m13 * m34 * m41 - m14 * m33 * m41 + m14 * m31 * m43 - m11 * m34 * m43 - m13 * m31 * m44 + m11 * m33 * m44;
      inversion.M23 = m14 * m23 * m41 - m13 * m24 * m41 - m14 * m21 * m43 + m11 * m24 * m43 + m13 * m21 * m44 - m11 * m23 * m44;
      inversion.M24 = m13 * m24 * m31 - m14 * m23 * m31 + m14 * m21 * m33 - m11 * m24 * m33 - m13 * m21 * m34 + m11 * m23 * m34;
      inversion.M31 = m22 * m34 * m41 - m24 * m32 * m41 + m24 * m31 * m42 - m21 * m34 * m42 - m22 * m31 * m44 + m21 * m32 * m44;
      inversion.M32 = m14 * m32 * m41 - m12 * m34 * m41 - m14 * m31 * m42 + m11 * m34 * m42 + m12 * m31 * m44 - m11 * m32 * m44;
      inversion.M33 = m12 * m24 * m41 - m14 * m22 * m41 + m14 * m21 * m42 - m11 * m24 * m42 - m12 * m21 * m44 + m11 * m22 * m44;
      inversion.M34 = m14 * m22 * m31 - m12 * m24 * m31 - m14 * m21 * m32 + m11 * m24 * m32 + m12 * m21 * m34 - m11 * m22 * m34;
      inversion.M41 = m23 * m32 * m41 - m22 * m33 * m41 - m23 * m31 * m42 + m21 * m33 * m42 + m22 * m31 * m43 - m21 * m32 * m43;
      inversion.M42 = m12 * m33 * m41 - m13 * m32 * m41 + m13 * m31 * m42 - m11 * m33 * m42 - m12 * m31 * m43 + m11 * m32 * m43;
      inversion.M43 = m13 * m22 * m41 - m12 * m23 * m41 - m13 * m21 * m42 + m11 * m23 * m42 + m12 * m21 * m43 - m11 * m22 * m43;
      inversion.M44 = m12 * m23 * m31 - m13 * m22 * m31 + m13 * m21 * m32 - m11 * m23 * m32 - m12 * m21 * m33 + m11 * m22 * m33;

      float det = matrix.Determinant();

      if(det == 0) {
        // TODO: Um... Something?
      }

      det = 1f / det;

      inversion.M11 *= det;
      inversion.M12 *= det;
      inversion.M13 *= det;
      inversion.M14 *= det;
      inversion.M21 *= det;
      inversion.M22 *= det;
      inversion.M23 *= det;
      inversion.M24 *= det;
      inversion.M31 *= det;
      inversion.M32 *= det;
      inversion.M33 *= det;
      inversion.M34 *= det;
      inversion.M41 *= det;
      inversion.M42 *= det;
      inversion.M43 *= det;
      inversion.M44 *= det;

      return inversion;
    }

    /// <summary>
    /// Handy shortcut for getting a float cosine value.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    private static float cos(float value) {
      return (float)Math.Cos(value);
    }

    /// <summary>
    /// Handy shortcut for getting a float sine value.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    private static float sin(float value) {
      return (float)Math.Sin(value);
    }
		#endregion

		#region Platform-Specific Conversion
#if OPENGL
		/// <summary>
		/// Gets a version of the matrix in a format compatible with OpenGL.
		/// </summary>
		/*internal Matrix4 glMatrix
		{
			get
			{
				// We're using OpenTK, which is also row-major.
				// It'll swap to column-major for us.
				return new Matrix4(
					-M11, M12, M13, M14,
					-M21, M22, M23, M24,
					-M31, M32, M33, M34,
					-M41, M42, M43, M44);
			}
		}*/
#endif
    #endregion
  }
}
