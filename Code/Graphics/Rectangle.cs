﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A defined, rectangular area on the screen or part of an image.
  /// </summary>
  public struct Rectangle {
    /// <summary>
    /// The upper-left corner of the rectangle.
    /// </summary>
    public Coord UpperLeft;

    /// <summary>
    /// The lower-right corner of the rectangle.
    /// </summary>
    public Coord LowerRight;

    /// <summary>
    /// The width of this rectangle.
    /// </summary>
    public float Width {
      get {
        return Mather.Distance(LowerRight.X, UpperLeft.X);
      }
    }

    /// <summary>
    /// The height of this rectangle.
    /// </summary>
    public float Height {
      get {
        return Mather.Distance(LowerRight.Y, UpperLeft.Y);
      }
    }

    /// <summary>
    /// The coordinate at the center of this rectangle.
    /// </summary>
    public Coord Center {
      get {
        return new Coord(Left + Width / 2, Top + Height / 2);
      }
    }

    /// <summary>
    /// The top side of the rectangle.
    /// </summary>
    public float Bottom {
      get {
        return LowerRight.Y;
      }
      set {
        LowerRight.Y = value;
      }
    }

    /// <summary>
    /// The left side of the rectangle.
    /// </summary>
    public float Left {
      get {
        return UpperLeft.X;
      }
      set {
        UpperLeft.X = value;
      }
    }

    /// <summary>
    /// The right side of the rectangle.
    /// </summary>
    public float Right {
      get {
        return LowerRight.X;
      }
      set {
        LowerRight.X = value;
      }
    }

    /// <summary>
    /// The top side of the rectangle.
    /// </summary>
    public float Top {
      get {
        return UpperLeft.Y;
      }
      set {
        UpperLeft.Y = value;
      }
    }

    public Rectangle(float left, float top, float right, float bottom) : this(new Coord(left, top), new Coord(right, bottom)) { }

    public Rectangle(float width, float height) : this(0, 0, width, height) { }

    public Rectangle(Coord upperLeft, Coord lowerRight) {
      this.UpperLeft = upperLeft;
      this.LowerRight = lowerRight;
    }

    /// <summary>
    /// Moves all this rectangle's points the given distances for the x and y axes.
    /// </summary>
    /// <param name="x">The distance to move the x axis.</param>
    /// <param name="y">The distance to move the y axis.</param>
    public void Move(float x, float y) {
      Move(new Coord(x, y));
    }

    /// <summary>
    /// Moves all this rectangle's points the given distances for the x and y axes.
    /// </summary>
    /// <param name="distance">The distance to move the x and y axes.</param>
    public void Move(Coord distance) {
      this.Top += distance.Y;
      this.Bottom += distance.Y;
      this.Left += distance.X;
      this.Right += distance.X;
    }
  }
}
