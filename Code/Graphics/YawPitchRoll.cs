﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics
{
	/// <summary>
	/// A set of rotation values in three-dimensional space.
	/// Susceptible to gimbal lock if you're not careful.
	/// </summary>
	public struct YawPitchRoll
	{
		#region Data
		/// <summary>
		/// Left-right rotation around the z axis.
		/// Higher values rotate clockwise, when viewed from above.
		/// </summary>
		public float Yaw;

		/// <summary>
		/// Up-down rotation around the x axis.
		/// Higher values rotate "nose-up", when viewed from ahead.
		/// </summary>
		public float Pitch;

		/// <summary>
		/// Side-to-side Rotation around the y axis.
		/// Higher values rotate clockwise, when viewed from behind.
		/// </summary>
		public float Roll;
		#endregion

		#region Conversions
		/// <summary>
		/// Converts this yaw-pitch-roll value to a quaternion.
		/// Rotation will be applied in the following order: yaw, then pitch, then roll.
		/// </summary>
		public Quaternion ToQuaternion()
		{
			return Quaternion.FromYawPitchRoll(Yaw, Pitch, Roll);
		}
		#endregion
	}
}
