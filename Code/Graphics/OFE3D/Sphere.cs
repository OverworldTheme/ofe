﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE3D {
	/// <summary>
	/// A simple sphere, useful in calculating collision.
	/// </summary>
	/// <remarks>
	/// The sphere is a collision object, not a graphical entity, and so is not displayed on-screen.
	/// </remarks>
	public struct Sphere {
		#region Creation
		/// <summary>
		/// Creates a new bounding sphere.
		/// </summary>
		/// <param name="radius">The radius to give to this sphere.</param>
		public Sphere(float radius) : this(radius, Vector.Zero) { }

		/// <summary>
		/// Creates a new bounding sphere.
		/// </summary>
		/// <param name="radius">The radius to give to this sphere.</param>
		/// <param name="position">Where the sphere should be located.</param>
		public Sphere(float radius, Vector position) {
			Position = position;
			Radius = radius;
		}
		#endregion

		#region Values
		/// <summary>
		/// This sphere's position in 3d space.
		/// </summary>
		public Vector Position;

		/// <summary>
		/// The radius of this sphere.
		/// </summary>
		/// <remarks>
		/// The radius of a sphere is the distance from its center to its perimeter.
		/// </remarks>
		public float Radius;

		/// <summary>
		/// The diameter of this sphere.
		/// </summary>
		/// <remarks>The diameter is twice the radius.</remarks>
		public float Diameter {
			get {
				return Radius * 2;
			}
			set {
				Radius = value / 2;
			}
		}
		#endregion

		#region Intersection
		/// <summary>
		/// Checks if this sphere intersects a given ray.
		/// </summary>
		/// <param name="ray">The ray to check.</param>
		/// <returns>The distance at which the ray intersects the sphere, or null if there is no intersection.</returns>
		/// <remarks>
		/// The CGSociety wiki was a useful resource in creating this algorithm.
		/// </remarks>
		public Nullable<float> Intersects(Ray ray) {
			// First we need to adjust the ray so that it's in our object space instead of world space.
			ray.Source = ray.Source - Position;

			// Now let's compute the coefficients.
			// Traditionally these are called a, b, and c.
			float a = Vector.Dot(ray.Direction, ray.Direction);
			float b = 2 * Vector.Dot(ray.Direction, ray.Source);
			float c = Vector.Dot(ray.Source, ray.Source) - (Radius * Radius);

			// Find the discriminant.
			float d = b * b - 4 * a * c;

			// If the discriminant is negative then there are no positive roots,
			// which in English means the ray doesn't even hit the sphere.
			if(d < 0) {
				return null;
			}

			// Otherwise, go on to compute the quadratic equation.
			float sqrt = Mather.Sqrt(d);
			float q;
			if(b < 0) {
				q = (-b - sqrt) / 2f;
			} else {
				q = (-b + sqrt) / 2f;
			}

			// Can you believe we still aren't done yet?
			// We still have to calculate the two possible intersections.
			// (Remember, the ray passes both into and then out of the sphere.)
			float t0 = q / a;
			float t1 = c / q;

			// Make sure that t0 is smaller than t1.
			// We want the closest intersection!
			if(t0 > t1) {
				Mather.Swap(ref t0, ref t1);
			}

			// Okay, now if t1 is less than zero, that means the sphere is behind the ray,
			// and thus it misses the sphere.
			if(t1 < 0) {
				return null;
			}

			// We want to return t0, where the ray passes into the sphere,
			// unless t0 is smaller than zero which means that the ray's source is actually inside the sphere,
			// and we'll have to make do with returning t1 where the ray passes out of the sphere.
			if(t0 < 0) {
				return t0;
			} else {
				return t1;
			}

		}

		/// <summary>
		/// Checks if this sphere intersects another, given sphere.
		/// </summary>
		/// <param name="sphere">The sphere to test against.</param>
		/// <returns>True if there is an intersection.</returns>
		public bool Intersects(Sphere sphere) {
			// This one is easy!
			// If the distance between the two spheres' positions is less than their combined radius,
			// then they are intersecting.
			if(Vector.Distance(this.Position, sphere.Position) < this.Radius + sphere.Radius) {
				return true;
			} else {
				return false;
			}
		}

		/// <summary>
		/// Checks if this sphere instersects the given plane. UNIMPLEMENTED.
		/// </summary>
		/// <param name="plane">The plane to test against.</param>
		/// <returns>True if there is an intersection.</returns>
		public bool Intersects(Plane plane) {
			// This is actually easier than it seems.
			// First we need to transfer the plane into object space.
			throw new NotImplementedException();
		}
		#endregion
	}
}
