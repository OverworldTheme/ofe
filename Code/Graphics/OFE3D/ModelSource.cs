﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

#if XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// All models pull their meshes and animation from a model source. Each model is basically an
  /// instance of a model source.
  /// </summary>
	public class ModelSource : ILoadable
	{
		#region Constructor
		public ModelSource()
		{
			allSources.Add(this);
		}

		public ModelSource(string fileName)
		{
			this.Load(fileName);
			allSources.Add(this);
		}
		#endregion

		/// <summary>
		/// A list of valid model file extensions. When using the XNA version of OFE, this list is
		/// moot because all images and textures are XNA data files.
		/// </summary>
		public static List<string> ValidExtensions = new List<string> { @".x" };

		/// <summary>
		/// A list of all the active model sources.
		/// </summary>
		internal static List<ModelSource> allSources = new List<ModelSource>();

		/// <summary>
		/// All the models that use this source.
		/// </summary>
		//public List<Model> models = new List<Model>();

		#region Meshes
		/// <summary>
		/// All the meshes that make up this model.
		/// </summary>
		public List<Mesh> Meshes = new List<Mesh>();
		#endregion

		#region Bones
		/// <summary>
		/// All the bones that are part of this model source, in their default state. Models that use
		/// this source have their own bones for modifying the source's meshes and vertices, based on these.
		/// </summary>
		public List<Entity> Bones = new List<Entity>();

		/// <summary>
		/// The root bone that all the other bones in this model can trace back to.
		/// </summary>
		public Entity RootBone;
		#endregion

		#region Loading
		/// <summary>
		/// Mutex lock used for threaded loading.
		/// </summary>
		private object loadLock = new object();

		/// <summary>
		/// The path and filename this model's data was loaded from, or is to be loaded from.
		/// </summary>
		public string PathAndFilename { get; private set; }

		/// <summary>
		/// Gets whether or not this model's data has finished loading.
		/// </summary>
		public bool IsLoaded { get; private set; }

		/// <summary>
		/// Gets whether or not there was a problem with the most recent model file supplied.
		/// The file may be missing, have corrupted data, or may not be a supported file type.
		/// </summary>
		public bool WasFileBad { get; private set; }

		/// <summary>
		/// Event fired by Grunt when this model's data is finished loading.
		/// </summary>
		public EventHandler OnLoaded
		{
			get
			{
				lock (loadLock)
				{
					return onLoaded;
				}
			}
			set
			{
				lock (loadLock)
				{
					if (IsLoaded)
					{
						value.Invoke(this, EventArgs.Empty);
					}
					else
					{
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;

		/// <summary>
		/// Queues loading of a new model from the given file.
		/// </summary>
		/// <param name="fileName">The relative path and file name, without file extension, of the
		/// model file you wish to load.</param>
		public void Load(string fileName)
		{
			lock (loadLock)
			{
				PathAndFilename = fileName;
				IsLoaded = false;
				WasFileBad = false;

				Grunt.Enqueue(this);
			}
		}

		/// <summary>
		/// Loads this model's data immediantly, in the current thread.
		/// </summary>
		public void LoadNow()
		{
			lock (loadLock)
			{
				ModelSource newData;

				try
				{
					using (Mod3dFile file = new Mod3dFile(PathAndFilename))
					{

						// Load the file.
						newData = file.LoadModel();

						// Copy everything over.
						this.Meshes = newData.Meshes;
						this.Bones = newData.Bones;
						this.RootBone = newData.RootBone;
					}
				}
				catch (Exception e) {
					if (e is FileNotFoundException)
						DebugLog.Failure("Could not find 3d model file " + PathAndFilename + ".");
					else if(e is IOException)
						DebugLog.Failure("There was a problem trying to open the 3d model file " + PathAndFilename + ".");
					else
						DebugLog.Failure("Could not load 3d model data from " + PathAndFilename + ".");
					WasFileBad = true;
				}
			}
		}
		#endregion

#if XNA
    public Microsoft.Xna.Framework.Graphics.Model xnaModel;
#endif

		/// <summary>
		/// Adds the given model to the list of models that use this as their source.
		/// </summary>
		/// <param name="model">The model to add to the list.</param>
		internal void Add(Model model)
		{
			if (model != null)
			{
				//models.Add(model);
			}
		}

		/// <summary>
		/// Removes a given model from the list of models that use this as their source.
		/// </summary>
		/// <param name="model">The model to remove from the list.</param>
		internal void Remove(Model model)
		{
			if (model != null)
			{
				//models.Remove(model);
			}
		}
	}
}
