﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE3D
{ 
	/// <summary>
	/// A collection of data and textures
	/// that determines how the surface of a 3d object looks.
	/// </summary>
	public class Material
	{
		#region Constructor
		public Material()
		{
			IsLit = true;
			IsDoubleSided = false;
			IsWireframe = false;
		}
		#endregion

		#region Identity
		/// <summary>
		/// The name of this material.
		/// </summary>
		public string Name { get; set; }
		#endregion

		#region Values
		/// <summary>
		/// How shiny the material is meant to be.
		/// More specifically, the intensity of the highlights on the material.
		/// Higher values create a shinier material.
		/// </summary>
		public float Specularity { get; set; }

		/// <summary>
		/// How wide or narrow the specular highlights are meant to be.
		/// Higher values create a narrower (sharder) highlight.
		/// </summary>
		public float Glossiness { get; set; }

		/// <summary>
		/// Whether lighting is enabled for this material.
		/// </summary>
		public bool IsLit { get; set; }

		/// <summary>
		/// Whether culling is enabled.
		/// If this value is set to true,
		/// polygons will always be visible,
		/// regardless of which way they're facing.
		/// </summary>
		public bool IsDoubleSided { get; set; }

		/// <summary>
		/// Whether this material is meant to be displayed as a wire frame.
		/// </summary>
		public bool IsWireframe { get; set; }
		#endregion

		#region Shaders
		/// <summary>
		/// The shader program used to draw this material.
		/// </summary>
		public ShaderProgram ShaderProgram;
		#endregion

		#region Textures
		/// <summary>
		/// The diffuse (color) map, if any.
		/// </summary>
		public Texture Diffuse;
		
		/// <summary>
		/// The opacity map, if any.
		/// (Or you can just use an alpha channel on the diffuse map.)
		/// </summary>
		public Texture Opacity;

		/// <summary>
		/// The specular map, if any.
		/// </summary>
		public Texture Specular;

		/// <summary>
		/// The gloss map, if any.
		/// </summary>
		public Texture Gloss;

		/// <summary>
		/// The normal map, if any.
		/// </summary>
		public Texture Normal;

		/// <summary>
		/// The environment (reflection) map, if any.
		/// </summary>
		public Texture Environment;

		/// <summary>
		/// The glow map, if any.
		/// </summary>
		public Texture Glow;
		#endregion
	}
}
