﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endif

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// A scene is a group of 3d entities that are grouped together and drawn together as a
  /// single, integrated world.
  /// </summary>
  public class Scene : GraphicGroup
	{
		#region Construction
		public Scene(float priority)
			: base()
		{
			base.Init(priority);

			this.Camera = new Camera3D(this);
			Lights = new List<Light>(Light.MaxLights);
		}
		#endregion

		#region Disposal
		/// <summary>
		/// Clears this scene's graphics, and removes it from priority list.
		/// You should call this when you're done with a scene and no longer plan to use it,
		/// or plan to replace it with a new one.
		/// </summary>
		public override void Dispose()
		{
			base.Dispose();
		}
		#endregion

		#region Members
		/// <summary>
    /// A list of all the entities in this graphic group.
    /// </summary>
    private List<Entity> children = new List<Entity>();

		/// <summary>
		/// Adds a new entity to this scene.
		/// </summary>
		/// <param name="NewChild">The entity to add.</param>
		internal void Add(Entity entity)
		{
			this.children.Add(entity);
			if (entity is Light) Lights.Add((Light)entity);
			entity.Scene = this;
		}

		/// <summary>
		/// Removes the given entity from this scene.
		/// </summary>
		/// <param name="entity">The entity to axe.</param>
		internal void Remove(Entity entity)
		{
			children.Remove(entity);
			if (entity is Light) Lights.Remove((Light)entity);
		}

		/// <summary>
		/// The number of entities, active or inactive, that are part of this scene.
		/// </summary>
		public int EntityCount
		{
			get
			{
				return children.Count;
			}
		}

		/// <summary>
		/// The number of active entities that are part of this scene.
		/// </summary>
		public int ActiveEntityCount
		{
			get
			{
				return children.Count(delegate(Entity e) { if (e.IsActive) return true; else return false; });
			}
		}

		/// <summary>
		/// The number of models, active or inactive, that are part of this scene.
		/// </summary>
		public int ModelCount
		{
			get
			{
				return children.Count(delegate(Entity e) { if (e is Model) return true; else return false; });
			}
		}

		/// <summary>
		/// The number of active models that are part of this scene.
		/// </summary>
		public int ActiveModelCount
		{
			get
			{
				return children.Count(delegate(Entity e) { if (e is Model && e.IsActive) return true; else return false; });
			}
		}		
		#endregion

		#region Lights
		/// <summary>
		/// The ambient light in the scene.
		/// </summary>
		public Color Ambient = Color.Black;

		/// <summary>
		/// Gets the number of lights in the scene.
		/// </summary>
		public int NumLights { get { return Lights.Count; } }

		/// <summary>
		/// All the lights in this scene.
		/// </summary>
		public List<Light> Lights { get; private set; }
		#endregion

		#region Camera
		/// <summary>
    /// The active camera that this scene will be viewed through.
    /// </summary>
    public Camera3D Camera;
		#endregion

		#region Mouse
		/// <summary>
    /// Returns a ray giving the mouse pointer's position and direction in 3d space relative to the camera.
    /// </summary>
    public Ray MouseRay() {
			Vector nearPoint = Camera.Unproject(new Coord(Mouse.Default.X, Mouse.Default.Y), 0f);
			Vector farPoint = Camera.Unproject(new Coord(Mouse.Default.X, Mouse.Default.Y), 1f);

      return new Ray(nearPoint, Vector.Normalize(farPoint-nearPoint));
    }
		#endregion

		#region Drawing
		protected override void Draw() {
      // A camera MUST exist and be active in order for this scene to render.
      if(this.Camera == null) {
        return;
      } else if(!this.Camera.IsActive || !this.IsActive) {
        return;
      }

      DrawCycle.Start3d(this);

      // Draw all the little children.
      foreach(Entity child in children) {
        child.Draw();
      }

      DrawCycle.Finish3d();
    }
		#endregion

		#region Updating
		/// <summary>
    /// Updates all the entities in this scene. Only parentless entities are updated by the
    /// scene. Children are automatically updated internally by their parents after updating
    /// themselves.
    /// </summary>
    protected override void Update() {
      // Experimental sorting routine to achieve better FPS in scenes with lots of models.
      // Didn't work.
      //children.Sort(sortModelsBySource);

      if(this.IsActive && this.children.Count > 0) {
        foreach(Entity child in children) {
          child.Update();
        }
      }
    }

    private int sortModelsBySource(Entity entity1, Entity entity2) {
      if(entity1 == null && entity2 == null) {
        return 0;
      } else if(entity1 == null) {
          return 1;
      } else if(entity2 == null) {
        return -1;
      } else {
        if(entity1 is Model && entity2 is Model) {
          Model model1 = (Model)entity1;
          Model model2 = (Model)entity2;
          if(model1.Source == model2.Source) {
            return 0;
          } else if(model1.Source == null) {
            return 1;
          } else if(model2.Source == null) {
            return -2;
          } else {
            return model1.Source.PathAndFilename.CompareTo(model2.Source.PathAndFilename);
          }
        } else if(entity1 is Model) {
          return 1;
        } else if(entity2 is Model) {
          return -1;
        } else {
          return 0;
        }
      }
		}
		#endregion
	}
}
