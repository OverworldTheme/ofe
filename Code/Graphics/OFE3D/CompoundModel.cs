﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endif

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// A collection of models treated as a single entity, designed so that models can easily be added
  /// and removed on the fly.
  /// </summary>
  public class CompoundModel : Entity {
    /// <summary>
    /// The collection of models that make up this compound model.
    /// </summary>
    private List<Model> models;

    /// <summary>
    /// The current number of models stored in the collection. Note that not all of the models in
    /// the collection are necessarily active.
    /// </summary>
    public int Capacity {
      get {
        return models.Count;
      }
    }

    /// <summary>
    /// The total number of active models in the collection.
    /// </summary>
    public int Count {
      get {
        return count;
      }
    }
    private int count;

    /// <summary>
    /// Creates a new compound model.
    /// </summary>
    /// <param name="scene">The scene to put this compound model in.</param>
    /// <param name="slots">How many initial "slots" to create in the list. Estimate how many models will usually
    /// be in this collection at any given time and base the value on that.</param>
    /// <param name="position">Where to put this compound model in the scene.</param>
    public CompoundModel(Scene scene, int slots, Vector position) {
      this.Scene = scene;
      models = new List<Model>(slots);
      for(int i = 0; i < slots; i++) {
        models.Add(new Model(scene));
      }

      this.Position = position;
    }

    /// <summary>
    /// Adds a single model to the collection.
    /// </summary>
    /// <param name="source">The model source to use for the new model.</param>
    /// <param name="position">Where to put the model, in relation to the compound entity.</param>
    public void Add(ModelSource source, Vector position) {
      Add(source, position, 0);
    }

    /// <summary>
    /// Adds a single model to the collection.
    /// </summary>
    /// <param name="source">The model source to use for the new model.</param>
    /// <param name="position">Where to put the model, in relation to the compound entity.</param>
    /// <param name="cullDistance">The cull distance to set for the new model; at what distance from the camera the
    /// new model will no longer be displayed. Set to zero if you want the model to always be visible when on-screen
    /// and within the z-depth of the camera.</param>
    public void Add(ModelSource source, Vector position, float cullDistance) {
      Add(source, position, Vector.Zero, Vector.One, 0);
    }

    /// <summary>
    /// Adds a single model to the collection.
    /// </summary>
    /// <param name="source">The model source to use for the new model.</param>
    /// <param name="position">Where to put the model, in relation to the compound entity.</param>
    /// <param name="rotation">The rotation to give this model.</param>
    /// <param name="scale">The scale to give this model.</param>
    public void Add(ModelSource source, Vector position, Vector rotation, Vector scale) {
      Add(source, position, rotation, scale, 0);
    }

    /// <summary>
    /// Adds a single model to the collection.
    /// </summary>
    /// <param name="source">The model source to use for the new model.</param>
    /// <param name="position">Where to put the model, in relation to the compound entity.</param>
    /// <param name="cullDistance">The cull distance to set for the new model; at what distance from the camera the
    /// new model will no longer be displayed. Set to zero if you want the model to always be visible when on-screen
    /// and within the z-depth of the camera.</param>
    /// <param name="rotation">The rotation to give this model.</param>
    /// <param name="scale">The scale to give this model.</param>
    public void Add(ModelSource source, Vector position, Vector rotation, Vector scale, float cullDistance) {
      if(source != null) {
        // Note the increase.
        count++;

        // Find an inactive model and use that, if there is one.
        Model model = getModel();

        // Turn the model on and set its values.
        model.IsActive = true;
        model.SetSource(source);
        model.Position = position;
				model.Rotation = Quaternion.FromYawPitchRoll(rotation.Z, rotation.X, rotation.Z);
        model.Scale = scale;
        model.CullDistance = cullDistance;

        // Set this compound model as the parent of the model.
        this.AddChild(model);
      }
    }

    /// <summary>
    /// Disables all the models in this collection, but keeps them around to recycle as "new" models.
    /// </summary>
    public void Clear() {
      // Turn off all the models--but do not release them!
      for(int i = 0; i < models.Count; i++) {
        models[i].IsActive = false;
        models[i].SetSource(null);
      }
      count = 0;
    }

    public override void Dispose() {
      // We need to deactivate everything in the collection.
      foreach(Model model in models) {
        model.Dispose();
      }

      // Now we can let ourself go.
      base.Dispose();
    }

    internal override void Update() {
      base.Update();

      // Make sure all the models in the collection reflect this entity's status.
      for(int i = 0; i < models.Count; i++) {
        if(models[i].IsActive) {
          models[i].Visible = this.Visible;
        }
      }
    }

    /// <summary>
    /// Returns a model that is not currently being used.
    /// </summary>
    /// <returns>If there is no inactive model, a new model is returned that has been added to
    /// the collection.</returns>
    private Model getModel() {
      foreach(Model model in models) {
        if(!model.IsActive) {
          return model;
        }
      }

      Model newModel = new Model(this.Scene);
      models.Add(newModel);
      return newModel;
    }
  }
}
