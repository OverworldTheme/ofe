﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// A plane in 3d space that stretches out into infinity.
  /// </summary>
  public struct Plane {
    /// <summary>
    /// This plane's distance along its normal from the world origin.
    /// </summary>
    public float Distance;

    /// <summary>
    /// The plane's normal vector that describes the direction it points.
    /// </summary>
    public Vector Normal;

    public Plane(Vector normal, float distance) {
      Normal = normal;
      Distance = distance;
    }

    /// <summary>
    /// Returns a plane that has been transformed by the given matrix.
    /// </summary>
    /// <param name="plane">The plane to transform.</param>
    /// <param name="matrix">The matrix to transform the plane by.</param>
    public static Plane Transform(Plane plane, Matrix matrix) {
      Plane newPlane = new Plane();

      newPlane.Normal.X = plane.Normal.X * matrix.M11 + plane.Normal.Y * matrix.M21 + plane.Normal.Z * matrix.M31 + plane.Distance * matrix.M41;
      newPlane.Normal.Y = plane.Normal.X * matrix.M12 + plane.Normal.Y * matrix.M22 + plane.Normal.Z * matrix.M32 + plane.Distance * matrix.M42;
      newPlane.Normal.Z = plane.Normal.X * matrix.M13 + plane.Normal.Y * matrix.M23 + plane.Normal.Z * matrix.M33 + plane.Distance * matrix.M43;
      newPlane.Distance = plane.Normal.X * matrix.M14 + plane.Normal.Y * matrix.M24 + plane.Normal.Z * matrix.M34 + plane.Distance * matrix.M44;

      return newPlane;
    }
  }
}
