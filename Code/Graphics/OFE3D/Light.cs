﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE3D
{
	/// <summary>
	/// A light source in 3d space,
	/// which provides illumination to 3d objects.
	/// </summary>
	public class Light : Entity
	{
		#region Constants
		/// <summary>
		/// The maximum number of lights supported in a single scene.
		/// </summary>
		public const int MaxLights = 4;
		#endregion

		#region Construction
		public Light(Scene scene)
		{
			Kind = Type.Point;
			Range = 100;
			Falloff = 10;
			DiffuseColor = Color.White;
			SpecularColor = Color.White;
			Intensity = 1;

			scene.Add(this);
		}
		#endregion

		#region Type
		/// <summary>
		/// An enumeration of light source types.
		/// </summary>
		public enum Type
		{
			/// <summary>
			/// A light that has no type will not be displayed.
			/// </summary>
			None = 0,

			/// <summary>
			/// A point light that radiates light in all directions from is position.
			/// </summary>
			Point = 1,

			/// <summary>
			/// A directional light that casts light in a set direction,
			/// from infinity to infinity.
			/// </summary>
			Directional = 2,

			/// <summary>
			/// A spot light that casts light in a set direction,
			/// in a given radius.
			/// </summary>
			Spot = 3,
		}

		/// <summary>
		/// What kind of light source this is.
		/// </summary>
		public Type Kind { get; set; }
		#endregion

		#region Properties
		/// <summary>
		/// The intensity of this light.
		/// Normal range is 0 through 1.
		/// </summary>
		public float Intensity { get; set; }

		/// <summary>
		/// The range of this light's full power.
		/// </summary>
		public float Range { get; set; }

		/// <summary>
		/// The additional falloff range of this light.
		/// </summary>
		public float Falloff { get; set; }
		#endregion

		#region Colors
		/// <summary>
		/// The diffuse color of this light.
		/// </summary>
		public Color DiffuseColor;

		/// <summary>
		/// The specular color of this light.
		/// This determines the color of highlights on 3d objects.
		/// </summary>
		public Color SpecularColor;
		#endregion

		#region Orientation
		/// <summary>
		/// Gets a unit vector representing the direction that this light is pointing.
		/// </summary>
		/// <remarks>Unlike most other entities,
		/// initially all lights point down.</remarks>
		public Vector Direction
		{
			get { return Vector.Rotate(Vector.Up, Rotation); }
		}
		#endregion
	}
}
