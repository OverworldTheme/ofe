﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// An entity is any 3D object that can be placed in a scene.
  /// </summary>
  public class Entity
	{
		#region Constructors
		public Entity() { }

		public Entity(Scene scene)
		{
			this.Scene = scene;
			scene.Add(this);
		}
		#endregion

		#region Disposal
		/// <summary>
		/// Disables this entity and removes it from its group. All its children (if any) will be released.
		/// </summary>
		public virtual void Dispose()
		{
			ReleaseChildren();

			if (this.Scene != null)
			{
				this.Scene.Remove(this);
			}
		}
		#endregion

		#region Identity
		/// <summary>
    /// This entity's name.
    /// </summary>
    public string Name;

    /// <summary>
    /// This enity's unique Guid.
    /// </summary>
    /// <remarks>When an entity is created, it is assigned a unique GUID. Certain entities, such as
    /// bones, may need this value from time to time, but in most cases it is safe to change the
    /// GUID on creation.</remarks>
    public Guid GUID = Guid.NewGuid();

    /// <summary>
    /// For entities that have been arranged in a list or array, this can be used to store thier
    /// index location within that array or list.
    /// </summary>
    public int Index;
		#endregion

		#region Status
		/// <summary>
    /// Whether or not this entity is being drawn to the screen and updated.
    /// </summary>
    public bool IsActive = true;

    /// <summary>
    /// Whether or not this entity is being drawn to the screen. Active entities
    /// that are invisible will continue to be updated. Note that inactive entities
    /// are never visible, regardless of this value.
    /// </summary>
    public bool Visible = true;
		#endregion

		#region Scene
		/// <summary>
    /// The scene that this entity is a part of.
    /// </summary>
    public Scene Scene;
		#endregion

		#region Parent-Child Relationships
		/// <summary>
    /// This entity's parent. Any changes made to the parent's position, orientation, or
    /// scale will affect this entity.
    /// </summary>
    public Entity Parent {
      get {
        return parent;
      }
    }
    private Entity parent;

    /// <summary>
    /// Gets whether this entity has a parent.
    /// </summary>
    public bool IsParentless {
      get {
        if(parent == null) {
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// All the immediate children of this entity.
    /// </summary>
    private List<Entity> childList = new List<Entity>();

    /// <summary>
    /// Gets whether this entity has any children.
    /// </summary>
    public bool Childless {
      get {
        if(ChildCount > 0) {
          return false;
        } else {
          return true;
        }
      }
    }

    /// <summary>
    /// Gets the number of immediate children this entity has.
    /// </summary>
    public int ChildCount {
      get {
        return childList.Count;
      }
    }

		/// <summary>
		/// Adds a child to this entity.
		/// </summary>
		/// <remarks>If the enity being added already has a parent, it will be released from that
		/// parent before being added as a child of this entity.</remarks>
		/// <param name="child">The entity to add as a child to this one.</param>
		public void AddChild(Entity child)
		{
			if (!child.IsParentless)
			{
				child.parent.ReleaseChild(child);
			}

			child.parent = this;
			childList.Add(child);
		}

		/// <summary>
		/// Breaks the link between this entity and one of its children. The child entity will no longer
		/// have this entity as its parent.
		/// </summary>
		/// <remarks>If the entity given is not this entity's child, the method will not sever the link
		/// between the entity and its actual parent, if any.</remarks>
		/// <param name="entity">The entity to release.</param>
		public void ReleaseChild(Entity entity)
		{
			if (entity.parent == this)
			{
				entity.parent = null;
			}

			childList.Remove(entity);
		}

		/// <summary>
		/// Breaks the link between this entity and all its children. The child entities will no longer
		/// have this entity as its parent.
		/// </summary>
		public void ReleaseChildren()
		{
			foreach (Entity child in childList)
			{
				child.parent = null;
			}
			childList.Clear();
		}
		#endregion

		#region Position
		/// <summary>
    /// This entity's position in 3d space, in relation to its parent.
    /// </summary>
    public Vector Position;

    /// <summary>
    /// Gets this entity's position in 3d world space.
    /// </summary>
    public Vector AbsolutePosition {
      get {
        if(IsParentless) {
          return Position;
        } else {
          return Parent.AbsolutePosition + Position;
        }
      }
    }

    /// <summary>
    /// This entity's position on the x axis in 3d space, in relation to its parent.
    /// </summary>
		public float X
		{
			get { return Position.X; }
			set { Position.X = value; }
		}

    /// <summary>
    /// This entity's position on the y axis in 3d space, in relation to its parent.
    /// </summary>
		public float Y
		{
			get { return Position.Y; }
			set { Position.Y = value; }
		}

    /// <summary>
    /// This entity's position on the z axis in 3d space, in relation to its parent.
    /// </summary>
		public float Z
		{
			get { return Position.Z; }
			set { Position.Z = value; }
		}
		#endregion

		#region Rotation
		/// <summary>
		/// This entity's orientation in 3D space, in relation to its parent.
		/// </summary>
		public Quaternion Rotation = new Quaternion(0, 0, 0, 1);

		/// <summary>
		/// This entity's rotation in 3d world space,
		/// taking into account its parent's rotation, if any.
		/// </summary>
		public Quaternion AbsoluteRotation
		{
			get
			{
				if (IsParentless) { return Rotation;}
				else { return Parent.Rotation * Rotation; }
			}
		}

		/// <summary>
		/// Rotates this entity on the world z axis.
		/// </summary>
		/// <param name="revolutions">The rotation value, in revolutions.</param>
		public void RotateYaw(float revolutions)
		{
			Rotation = Quaternion.FromAxisAngle(0, 0, 1, revolutions) * Rotation;
			Rotation = Rotation.Normalized();
		}

		/// <summary>
		/// Rotates this entity on the world x axis.
		/// </summary>
		/// <param name="revolutions">The rotation value, in revolutions.</param>
		public void RotatePitch(float revolutions)
		{
			Rotation = Quaternion.FromAxisAngle(1, 0, 0, revolutions) * Rotation;
			Rotation = Rotation.Normalized();
		}

		/// <summary>
		/// Rotates this entity on the world y axis.
		/// </summary>
		/// <param name="revolutions">The rotation value, in revolutions.</param>
		public void RotateRoll(float revolutions)
		{
			Rotation = Quaternion.FromAxisAngle(0, 1, 0, revolutions) * Rotation;
			Rotation = Rotation.Normalized();
		}

		/// <summary>
		/// Rotates this entity on its local z axis.
		/// </summary>
		/// <param name="revolutions">The rotation value, in revolutions.</param>
		public void RotateLocalYaw(float revolutions)
		{
			Rotation *= Quaternion.FromAxisAngle(0, 0, 1, revolutions);
			Rotation = Rotation.Normalized();
		}

		/// <summary>
		/// Rotates this entity its local x axis.
		/// </summary>
		/// <param name="revolutions">The rotation value, in revolutions.</param>
		public void RotateLocalPitch(float revolutions)
		{
			Rotation *= Quaternion.FromAxisAngle(1, 0, 0, revolutions);
			Rotation = Rotation.Normalized();
		}

		/// <summary>
		/// Rotates this entity its local y axis.
		/// </summary>
		/// <param name="revolutions">The rotation value, in revolutions.</param>
		public void RotateLocalRoll(float revolutions)
		{
			Rotation *= Quaternion.FromAxisAngle(0, 1, 0, revolutions);
			Rotation = Rotation.Normalized();
		}
		#endregion

		#region Scale
		/// <summary>
    /// This entity's scale on each axis. With a value of 1, the object is its normal size. At 2, it
    /// is double is normal size. At 0.5, it is half of its normal size. At 0 it is visually non-existant.
    /// </summary>
    public Vector Scale = new Vector(1, 1, 1);

		/// <summary>
		/// Gets this entity's scale in 3d world space,
		/// taking into account the scale of any parents it has.
		/// </summary>
		public Vector AbsoluteScale
		{
			get
			{
				if (IsParentless)
				{
					return Scale;
				}
				else
				{
					return Parent.AbsoluteScale + Scale;
				}
			}
		}
		#endregion

		#region Updating
		/// <summary>
    /// Updates this entity each logic cycle.
    /// </summary>
    internal virtual void Update() {
      // Update all this entity's children.
      foreach(Entity child in childList) {
        child.Update();
      }
    }
		#endregion

		#region Drawing
		/// <summary>
		/// At what distance from the camera to stop displaying this entity. When set to 0 or lower,
		/// the entity will always be drawn.
		/// </summary>
		public float CullDistance = 0;

		/// <summary>
		/// Draws this entity each frame.
		/// </summary>
		internal virtual void Draw()
		{
		}
		#endregion

    /// <summary>
    /// Rotates this entity so that it "faces" the given angle. This will affect only the entity's yaw.
    /// </summary>
    /// <param name="angle">The angle to face, in revolutions.</param>
    public void Face(float angle) {
      this.Rotation = Quaternion.FromYawPitchRoll(angle, 0, 0);
    }

    /// <summary>
    /// Rotates this entity so that it "faces" another entity. This will affect only the entity's yaw.
    /// </summary>
    /// <param name="entity">The entity to face.</param>
    public void Face(Entity entity) {
      Face(entity.Position);
    }

    /// <summary>
    /// Rotates this entity so that it "faces" a given point in space. This will affect only the entity's yaw.
    /// </summary>
    /// <param name="entity">The point for this entity to face.</param>
    public void Face(Vector point) {
      this.Rotation = Quaternion.FromYawPitchRoll(
				Mather.Atan2(point.Y - this.Y, point.X - this.X) - .25f,
				0,
				0);
    }

    /// <summary>
    /// Rotates this entity so that it faces the given angle in relation to the entity's scene's camera.
    /// </summary>
    /// <param name="angle">The angle to face in relation to the scene camera. At 0 it will face the same
    /// direction as the camera.</param>
    public void FaceByCamera(float angle) {
      if(Scene != null && Scene.Camera != null) {
        FaceByEntity(angle, Scene.Camera);
      }
    }

    /// <summary>
    /// Rotates this entity so that it faces the given angle in relation to the direction the given entity is facing.
    /// </summary>
    /// <param name="angle">The angle to face in relation to the given entity. At 0 it will face the same
    /// direction as the given entity.</param>
    public void FaceByEntity(float angle, Entity entity) {
      if(entity != null) {				
				throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Has this entity face its scene's camera.
    /// </summary>
    public void FaceCamera() {
      Face(Scene.Camera);
    }

    /// <summary>
    /// Directs this entity to "look at" a given point in space. This will affect both the yaw and pitch of
    /// the entity.
    /// </summary>
    /// <param name="point">The point in space to look at.</param>
    public void LookAt(Vector point) {
      //this.Yaw = Mather.RadiansToRevolutions(Mather.Atan2(point.Y - this.Y, point.X - this.X)) - .25f;
      // TODO: Make this actually look at stuff.
    }
    
    /// <summary>
    /// Moves this entity the given distance (on the x and y axes) in the direction that it is facing.
    /// </summary>
    /// <param name="distance"></param>
    public void MoveForward(float distance) {
      //this.X -= Mather.Sin(Mather.RevolutionsToRadians(Yaw)) * distance;
      //this.Y += Mather.Cos(Mather.RevolutionsToRadians(Yaw)) * distance;
    }

    /// <summary>
    /// Puts this entity at the given position in 3d space.
    /// </summary>
    /// <param name="position">The point in 3d space to move this entity to.</param>
    public void MoveTo(Vector position) {
      this.Position = position;
    }

    /// <summary>
    /// Puts this entity at the given position in 3d space.
    /// </summary>
    /// <param name="x">The x-axis coordinate to set this model at.</param>
    /// <param name="y">The y-axis coordinate to set this model at.</param>
    /// <param name="z">The z-axis coordinate to set this model at.</param>
    public void MoveTo(float x, float y, float z) {
      this.Position = new Vector(x, y, z);
    }

    /// <summary>
    /// Puts this entity at a given distance from a point, at a given angle. Useful for getting entities
    /// to circle around things.
    /// </summary>
    /// <param name="point">The point to position around.</param>
    /// <param name="angle">What part of the orbit around the point to position this entity at.</param>
    /// <param name="distance">How far from the point to put this entity.</param>
    /// <param name="height">How high, in relation to the point, to position this entity.</param>
    public void OrbitPoint(Vector point, float angle, float distance, float height) {
      angle = Mather.RevolutionsToRadians(angle);
      X = point.X + (Mather.Sin(angle) * distance);
      Y = point.Y + (Mather.Cos(angle) * distance);
      Z = point.Z + height;
    }

    /// <summary>
    /// Puts this entity at a given distance from another enity, at a given angle. Useful for getting entities
    /// to circle around each other.
    /// </summary>
    /// <param name="entity">The entity to position around.</param>
    /// <param name="angle">What part of the orbit around the point to position this entity at.</param>
    /// <param name="distance">How far from the point to put this entity.</param>
    /// <param name="height">How high, in relation to the point, to position this entity.</param>
    public void OrbitEntity(Entity entity, float angle, float distance, float height) {
      OrbitPoint(entity.Position, angle, distance, height);
    }
  }
}
