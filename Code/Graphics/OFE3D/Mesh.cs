﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

#if XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endif

using VisionRiders.OFE.Graphics;

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// A collection of polygon and vertex data that defines an object that can be rendered
  /// to the screen in 3d.
  /// </summary>
  /// <remarks>
  /// In OFE, "mesh" and "model" are terms that are used to describe two different things.
  /// A model is a 3d entity that exists in the world space of a scene. Meshes do not exist
  /// in world space. However, the model has at least one mesh that defines what it looks like,
  /// and it is these meshes that the model will position and animate to create the illusion
  /// of existance.
  /// </remarks>
#if DEBUG && WINDOWS
  [System.Diagnostics.DebuggerDisplay("{Name} verts: {Vertices.Length}, polys: {Polygons.Length}")]
#endif  
  public class Mesh
	{
		#region Constants
		/// <summary>
		/// The stride of the vertex buffer, in bytes.
		/// That is, the number of floats for each vertex in the model.
		/// </summary>
		private static int Stride = 12;
		#endregion

		#region Constructor
		public Mesh()
		{
			// Create our buffers.
			Vertices = new List<Vertex>();
			Polygons = new List<Poly>();

			// Create our working variables.
			matrix = new Matrix();
			scale = new Matrix();
			rotation = new Matrix();
			translation = new Matrix();
		}
		#endregion

		#region Release
		~Mesh()
		{
#if OPENGL
			// Dump our handles on the main thread.
			if (!Core.IsShuttingDown && glHasAquiredHandles)
			{
				Core.AddUpdateCallback(() =>
				{
					GL.DeleteVertexArrays(1, ref glVArrayID);
					GL.DeleteBuffers(1, ref glPBufferID);
					GL.DeleteBuffers(1, ref glVBufferID);
				});
			}
#endif
		}		
		#endregion

		#region Identity
		/// <summary>
    /// The name given to this mesh.
    /// </summary>
    public string Name;
		#endregion

		#region Mesh Data
		/// <summary>
    /// The vertices that the polygons of this mesh are defined by.
    /// </summary>
    public List<Vertex> Vertices;

    /// <summary>
    /// The three-sided polygons (triangles) that make up this mesh.
    /// </summary>
    public List<Poly> Polygons;
		#endregion

		#region Local Space
		/// <summary>
		/// This mesh's position in model space.
		/// </summary>
		public Vector Position { get; set; }

		/// <summary>
		/// This mesh's rotation in model space.
		/// </summary>
		public Quaternion Rotation { get; set; }

		/// <summary>
		/// This mesh's scale in model space.
		/// </summary>
		public Vector Scale { get; set; }
		#endregion

		#region Shaders and Material
		/// <summary>
		/// Gets the shader program that is used by this mesh's material.
		/// </summary>
		public ShaderProgram ShaderProgram
		{
			get
			{
				if (Material == null) return null;
				else return Material.ShaderProgram;
			}
		}

		/// <summary>
		/// The material used for this mesh.
		/// </summary>
		public Material Material;
		#endregion

		#region Buffers
		/// <summary>
		/// The vertex buffer.
		/// </summary>
		private float[] vBuffer;

		/// <summary>
		/// The poly buffer.
		/// </summary>
		private int[] pBuffer;

		/// <summary>
		/// Makes sure that the mesh's buffers are up-to-date.
		/// </summary>
		public void UpdateBuffer()
		{
#if OPENGL
			// Copy all the vert data to the buffer.
			vBuffer = new float[Vertices.Count * Stride];
			for (int i = 0; i < Vertices.Count; i++)
			{
				vBuffer[i * Stride] = Vertices[i].X;
				vBuffer[i * Stride + 1] = Vertices[i].Y;
				vBuffer[i * Stride + 2] = Vertices[i].Z;

				vBuffer[i * Stride + 3] = Vertices[i].NX;
				vBuffer[i * Stride + 4] = Vertices[i].NY;
				vBuffer[i * Stride + 5] = Vertices[i].NZ;

				vBuffer[i * Stride + 6] = Vertices[i].U;
				vBuffer[i * Stride + 7] = Vertices[i].V;

				vBuffer[i * Stride + 8] = Vertices[i].Color.R;
				vBuffer[i * Stride + 9] = Vertices[i].Color.G;
				vBuffer[i * Stride + 10] = Vertices[i].Color.B;
				vBuffer[i * Stride + 11] = 1;// mesh.Vertices[i].Color.A;
			}

			// Copy all the polys to our buffer.
			pBuffer = new int[Polygons.Count * 3];
			for (int i = 0; i < Polygons.Count; i++)
			{
				pBuffer[i * 3] = Polygons[i].V1;
				pBuffer[i * 3 + 1] = Polygons[i].V2;
				pBuffer[i * 3 + 2] = Polygons[i].V3;
			}
#endif
		}
		#endregion

		#region Bounding Objects
		/// <summary>
		/// A bounding sphere that contains all of this mesh.
		/// </summary>
		public Sphere BoundingSphere;

		//public Cube BoundingBox;
		#endregion

		#region Bones
		/// <summary>
    /// This mesh's parent bone, which describes how it is positioned in relation to any parent meshes.
    /// </summary>
    public Entity ParentBone;

#if XNA
    /// <summary>
    /// Copies data from an XNA model mesh and returns a new mesh with that same data
    /// formatted for OFE.
    /// </summary>
    internal static Mesh xnaCopyMeshData(ModelSource modelSource, ModelMesh source) {
      Mesh newMesh = new Mesh();
      int verts = 0;
      int polys = 0;

      // Let's extract the mesh's name.
      newMesh.Name = source.Name;

      // Let's set the parent bone.
      newMesh.ParentBone = modelSource.Bones[source.ParentBone.Index];

      // Let's find out how many polys and vertices the model has in total. In order to do this in
      // the current (as of this addition) version of XNA, we have to step through each individual
      // part that XNA has the model split up into.
      foreach(ModelMeshPart part in source.MeshParts) {
        verts += part.NumVertices;
        polys += part.IndexBuffer.IndexCount;
      }

      // With that data, now we can initiate the arrays.
      newMesh.initializeArrays(verts, polys);

      // Now let's start pulling the data from XNA and transferring it to the now-initialized arrays.
      int vertOffset = 0;
      int polyOffset = 0;
      foreach(ModelMeshPart part in source.MeshParts) {
        Vector3[] vertData = new Vector3[part.NumVertices];
        short[] polyData = new short[part.PrimitiveCount * 3];
        part.VertexBuffer.GetData<Vector3>(part.VertexOffset, vertData, 0, part.NumVertices, part.VertexBuffer.VertexDeclaration.VertexStride);
        part.IndexBuffer.GetData<short>(polyData);

        for(int i = 0; i < vertData.Length; i++) {
          newMesh.Vertices[i + vertOffset].X = vertData[i].X;
          newMesh.Vertices[i + vertOffset].Y = vertData[i].Z;
          newMesh.Vertices[i + vertOffset].Z = vertData[i].Y;
        }
        for(int j = 0; j < polyData.Length / 3; j++) {
          newMesh.Polygons[j + polyOffset].V1 = polyData[(j * 3)] + vertOffset;
          newMesh.Polygons[j + polyOffset].V2 = polyData[(j * 3) + 1] + vertOffset;
          newMesh.Polygons[j + polyOffset].V3 = polyData[(j * 3) + 2] + vertOffset;
        }
        vertOffset += vertData.Length;
        polyOffset += polyData.Length;
      }

      return newMesh;
    }
#endif
		#endregion

		#region Drawing
		/// <summary>
		/// A matrix object kept in memory to calculate matrix values without creating new objects each frame.
		/// </summary>
		private Matrix matrix;

		/// <summary>
		/// A matrix object kept in memory to calculate a scale matrix without creating new objects each frame.
		/// </summary>
		private Matrix scale;

		/// <summary>
		/// A matrix object kept in memory to calculate a rotation matrix without creating new objects each frame.
		/// </summary>
		private Matrix rotation;

		/// <summary>
		/// A matrix object kept in memory to calculate a translation matrix without creating new objects each frame.
		/// </summary>
		private Matrix translation;

		/// <summary>
		/// Draws this mesh immediantly.
		/// Must be called during the draw cycle.
		/// (As needed, remember to send the model matrix and bone positions and other shader data to the GPU first.)
		/// </summary>
		/// <param name="model">The matrix defining where to position the model in the world.</param>
		public void Draw(Matrix model)
		{
			// Make sure we have our handles.
			if (!glHasAquiredHandles)
				glAquireHandles();

			// Set and send along our matrices.
			//Matrix local = Matrix.CreateScale(Scale.X, Scale.Y, Scale.Z) * Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(Position.X, Position.Y, Position.Z);
			scale.BuildScale(Scale.X, Scale.Y, Scale.Z);
			rotation.BuildFromQuaternion(Rotation);
			translation.BuildTranslation(Position.X, Position.Y, Position.Z);
			matrix.Multiply(scale, rotation, translation);
			matrix.Multiply(DrawCycle.Projection, DrawCycle.View, model, matrix);
			ShaderProgram.PassMatrix("Matrix", matrix);			

			// Set the shader and texture.
			DrawCycle.SetShader(Material.ShaderProgram);
			DrawCycle.SetTexture(Material.Diffuse);

			// Check if we can just reuse our buffer to speed things along.		
			bool needToSend = false;
			if(DrawCycle.LastBuffer != this) {
				DrawCycle.LastBuffer = this;
				needToSend = true;
			}
			
#if OPENGL
			if (needToSend)
			{
				// Set material properties.
				if (Material.IsDoubleSided)
					GL.Disable(EnableCap.CullFace);
				else
					GL.Enable(EnableCap.CullFace);

				// Set drawing properties.
				GL.FrontFace(FrontFaceDirection.Ccw);
				GL.Enable(EnableCap.DepthTest);

				// Bind the array to the GPU.
				GL.BindVertexArray(glVArrayID);
				Display.LogErrors();

				// Bind the vertex buffer to the GPU.
				GL.BindBuffer(BufferTarget.ArrayBuffer, glVBufferID);
				GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(sizeof(float) * vBuffer.Length), vBuffer.ToArray(), BufferUsageHint.StaticDraw);
				Display.LogErrors();

				// Bind the polygon buffer to the GPU.
				GL.BindBuffer(BufferTarget.ElementArrayBuffer, glPBufferID);
				GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(int) * pBuffer.Length), pBuffer.ToArray(), BufferUsageHint.StaticDraw);
				Display.LogErrors();

				// Send out shader parameters.
				int vAtt = GL.GetAttribLocation(DrawCycle.ActiveShader.OGL_Program, "position");
				if (vAtt != -1)
				{
					GL.VertexAttribPointer(vAtt, 3, VertexAttribPointerType.Float, false, Stride * sizeof(float), 0);
					GL.EnableVertexAttribArray(vAtt);
					Display.LogErrors();
				}

				int nAtt = GL.GetAttribLocation(DrawCycle.ActiveShader.OGL_Program, "normals");
				if (nAtt != -1)
				{
					GL.VertexAttribPointer(nAtt, 3, VertexAttribPointerType.Float, false, Stride * sizeof(float), 3 * sizeof(float));
					GL.EnableVertexAttribArray(nAtt);
					Display.LogErrors();
				}

				int tAtt = GL.GetAttribLocation(DrawCycle.ActiveShader.OGL_Program, "texCoords");
				if (tAtt != -1)
				{
					GL.VertexAttribPointer(tAtt, 2, VertexAttribPointerType.Float, false, Stride * sizeof(float), 6 * sizeof(float));
					GL.EnableVertexAttribArray(tAtt);
					Display.LogErrors();
				}

				int cAtt = GL.GetAttribLocation(DrawCycle.ActiveShader.OGL_Program, "color");
				if (cAtt != -1)
				{
					GL.VertexAttribPointer(cAtt, 4, VertexAttribPointerType.Float, false, Stride * sizeof(float), 8 * sizeof(float));
					GL.EnableVertexAttribArray(cAtt);
					Display.LogErrors();
				}
			}

			// Draw.			
			GL.DrawElements(BeginMode.Triangles, pBuffer.Length, DrawElementsType.UnsignedInt, 0);		
			Display.LogErrors();
#elif XNA
      BoundingFrustum frustum;
      if(!Scene.Camera.Orthographic) {
        frustum = new BoundingFrustum(Microsoft.Xna.Framework.Matrix.CreateLookAt(new Vector3(Scene.Camera.AbsolutePosition.X, Scene.Camera.AbsolutePosition.Z, Scene.Camera.AbsolutePosition.Y),
          new Vector3(Scene.Camera.Target.AbsolutePosition.X, Scene.Camera.Target.AbsolutePosition.Z, Scene.Camera.Target.AbsolutePosition.Y), Vector3.Up)
          * Microsoft.Xna.Framework.Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(this.Scene.Camera.FieldOfView), Display.AspectRatio, Scene.Camera.Near, Scene.Camera.Far));
      } else {
        frustum = new BoundingFrustum(Microsoft.Xna.Framework.Matrix.CreateLookAt(new Vector3(Scene.Camera.Position.X, Scene.Camera.Position.Z, Scene.Camera.Position.Y),
          new Vector3(Scene.Camera.Target.AbsolutePosition.X, Scene.Camera.Target.AbsolutePosition.Z, Scene.Camera.Target.AbsolutePosition.Y), Vector3.Up)
          * Microsoft.Xna.Framework.Matrix.CreateOrthographic(Display.Width, Display.Height, Scene.Camera.Near, Scene.Camera.Far));
      }

      // Set the bones.
      Microsoft.Xna.Framework.Matrix[] transforms = new Microsoft.Xna.Framework.Matrix[Source.xnaModel.Bones.Count];
      Source.xnaModel.CopyAbsoluteBoneTransformsTo(transforms);

      // Enable Anisotropy.
      if(Display.AnisotropyEnabled) {
        SamplerState sampler = new SamplerState();
        sampler.Filter = TextureFilter.Anisotropic;
        sampler.MaxAnisotropy = Display.AnisotropicSamples;
        Display.xnaDevice.SamplerStates[0] = sampler;
      }

      // Draw each mesh in the model
      foreach(ModelMesh mesh in Source.xnaModel.Meshes) {
        // Make sure the model is visible on screen. Otherwise, there's no reason to bother drawing it.
        if(frustum.Intersects(new BoundingSphere(new Vector3(this.AbsolutePosition.X, this.AbsolutePosition.Z, this.AbsolutePosition.Y), mesh.BoundingSphere.Radius))) {
          // Set the model effects.
          foreach(BasicEffect effect in mesh.Effects) {
            // Set lighting.
            effect.EnableDefaultLighting();

            // Set transforms.
            effect.World = transforms[mesh.ParentBone.Index]
              * Microsoft.Xna.Framework.Matrix.CreateScale(this.Scale.X, this.Scale.Z, this.Scale.Y)
              * Microsoft.Xna.Framework.Matrix.CreateFromYawPitchRoll(MathHelper.ToRadians(this.Yaw) * -1, MathHelper.ToRadians(this.Pitch), MathHelper.ToRadians(this.Roll))
              * Microsoft.Xna.Framework.Matrix.CreateTranslation(this.AbsolutePosition.X, this.AbsolutePosition.Z, this.AbsolutePosition.Y);
            effect.View = Microsoft.Xna.Framework.Matrix.CreateLookAt(new Vector3(Scene.Camera.AbsolutePosition.X, Scene.Camera.AbsolutePosition.Z, Scene.Camera.AbsolutePosition.Y),
              new Vector3(Scene.Camera.Target.AbsolutePosition.X, Scene.Camera.Target.AbsolutePosition.Z, Scene.Camera.Target.AbsolutePosition.Y), Vector3.Up);
            if(!Scene.Camera.Orthographic) {
              effect.Projection = Microsoft.Xna.Framework.Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(this.Scene.Camera.FieldOfView), Display.AspectRatio, Scene.Camera.Near, Scene.Camera.Far);
            } else {
              effect.Projection = Microsoft.Xna.Framework.Matrix.CreateOrthographic(Display.Width, Display.Height, Scene.Camera.Near, Scene.Camera.Far);
            }
          }
          mesh.Draw();
        }
      }
#endif

			// Increase the call count by one.
			Core.DrawCalls++;
			Display.LogErrors();
		}
		#endregion

		#region OpenGL
#if OPENGL
		/// <summary>
		/// The OpenGL handle for the vertex buffer.
		/// </summary>
		private int glVBufferID;

		/// <summary>
		/// The OpenGL handle for the polygon buffer.
		/// </summary>
		private int glPBufferID;

		/// <summary>
		/// The OpenGL handler for the vertex array.
		/// </summary>
		private int glVArrayID;

		/// <summary>
		/// Whether or not the mesh has gotten handles from OpenGL.
		/// This must be done on the main thread, so is deferred during loading.
		/// </summary>
		private bool glHasAquiredHandles = false;

		/// <summary>
		/// Generates OpenGL handles for the buffers.
		/// Must be called on the main thread.
		/// </summary>
		private void glAquireHandles()
		{			
			GL.GenBuffers(1, out glVBufferID);
			GL.GenBuffers(1, out glPBufferID);
			GL.GenVertexArrays(1, out glVArrayID);

			glHasAquiredHandles = true;
		}
#endif
		#endregion
	}
}
