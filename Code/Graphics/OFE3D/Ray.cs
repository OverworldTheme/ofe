﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// A Euclidean half-line that starts at a given point and extends in one direction into infinity. These are
  /// useful in 3d programming for collision handling and other assorted functions.
  /// </summary>
  public struct Ray {
    /// <summary>
    /// The direction this ray extends in 3d space.
    /// </summary>
    public Vector Direction;

    /// <summary>
    /// The starting point for this ray.
    /// </summary>
    public Vector Source;

    public Ray(Vector source, Vector direction) {
      this.Source = source;
      this.Direction = direction;
    }

    public static Ray PointAt(Vector source, Vector pointAt) {
      Ray ray = new Ray();
      ray.Source = source;
      ray.Direction = pointAt - source;
      return ray;
    }

    /// <summary>
    /// Checks if this ray intersects a given model, and returns the point at which it intersects.
    /// </summary>
    /// <param name="model">The model to check against.</param>
    /// <returns>null if there is no intersection. Otherwise, a vector containg the point in world
    /// space nearest to the ray's origin point that the ray intersects with the model.</returns>
    /// <remarks>Special thanks to the SoftSurfer site and Mr. Meanie's Page for much of the
    /// math in this.</remarks>
    public Nullable<Vector> Intersects(Model model) {
      // Make sure the model and its mesh data exist.
      if(model == null || model.Source.WasFileBad || model.Source.Meshes.Count < 1) {
        return null;
      }

      // Create a variable to return data in.
      Vector returnVector = new Vector();
      bool intersects = false;

      // We'll be going through each polygon in each mesh of the model individually.
      foreach(Mesh mesh in model.Source.Meshes) {
        // TODO: This is a temp bone hack.
        Matrix rootBone = Matrix.CreateTranslation(mesh.ParentBone.AbsolutePosition.X,
          mesh.ParentBone.AbsolutePosition.Y,
          mesh.ParentBone.AbsolutePosition.Z);

        // Transform the mesh's vertices into world space.
        Vector[] verts = new Vector[mesh.Vertices.Count];
        mesh.Vertices.ToArray().CopyTo(verts, 0);
				Matrix world = new Matrix();
				world.Multiply(rootBone,
          Matrix.CreateFromQuaternion(model.Rotation),
          Matrix.CreateTranslation(model.X, model.Y, model.Z));
        for(int i = 0; i < verts.Length; i++) {
          verts[i] = Vector.Transform(verts[i], world);
        }

        foreach(Poly poly in mesh.Polygons) {
          // Get the poly's world-transformed vertices.
          Vector[] corner = new Vector[]{new Vector(verts[poly.V1].X, verts[poly.V1].Y, verts[poly.V1].Z),
            new Vector(verts[poly.V2].X, verts[poly.V2].Y, verts[poly.V2].Z),
            new Vector(verts[poly.V3].X, verts[poly.V3].Y, verts[poly.V3].Z)};

          // Check if the ray at least intersects this polygon's plane. Do that, we first need to
          // calculate the polygon's plane. First we calculate the poly's edges (u, v) and surface
          // normal (n).
          Vector u, v, n;
          u = corner[1] - corner[0];
          v = corner[2] - corner[0];
          n = Vector.Cross(u, v);
          if(n == Vector.Zero) {
            // This poly is degenerate (two or more of the vertices are in the exact same position,
            // making it a line or point instead of a polygon). We can't work with this, so we need
            // to just go ahead and skip it.
            continue;
          }

          // Let's run some numbers on the ray.
          Vector w0, w;
          float r, a, b;
          w0 = this.Source - corner[0];
          a = -Vector.Dot(n, w0);
          b = Vector.Dot(n, this.Direction);

          // Let's check to make sure the ray even intersects the plane. First, let's find out if
          // the ray is parallel to the poly's plane.
          if(Math.Abs(b) < 0.000001f) {
            // Check if the ray is actually running along in the plane.
            if(a == 0) {
              // The ray's source is inside this poly. We're not going to get any closer than this,
              // and we can just return the ray's source as the intersection point.
              return this.Source;
            } else {
              // The ray never crosses the plane.
              continue;
            }
          }

          // Get the intersection point of the ray and the polygon.
          r = a / b;          
          if(r < 0) {
            // The ray is traveling away from the poly, so they never actually intersect.
            continue;
          }
          Vector intersection = this.Source + Vector.Multiply(r, this.Direction);

          // Now that we've made it this far, it's time to check if the intersection point is
          // inside the poly or not.
          float uu, uv, vv, wu, wv, D;
          uu = Vector.Dot(u, u);
          uv = Vector.Dot(u, v);
          vv = Vector.Dot(v, v);
          w = intersection - corner[0];
          wu = Vector.Dot(w, u);
          wv = Vector.Dot(w, v);
          D = uv * uv - uu * vv;

          float s, t;
          s = (uv * wv - vv * wu) / D;
          if(s < 0 || s > 1) {
            // The intersection point is outside the polygon.
            continue;
          }
          t = (uv * wu - uu * wv) / D;
          if(t < 0 || (s + t) > 1) {
            // The intersection point is outside the polygon.
            continue;
          }

          // If we've made it this far, we have a legit intersection. We'll note that we've found
          // an intersection, and check if this intersection is closer than the last one we found
          // (if we had found another one). If this is the closest, we'll put it in the return
          // vector and go on to the next poly.
          if(intersects) {
            if(Vector.Distance(this.Source, intersection) < Vector.Distance(this.Source, returnVector)) {
              returnVector = intersection;
            }
          } else {
            intersects = true;
            returnVector = intersection;
          }
        }
      }

      if(intersects) {
        return returnVector;
      } else {
        return null;
      }
    }

    /// <summary>
    /// Checks if this ray intersects a given plane and returns the point at which it intersects.
    /// </summary>
    /// <param name="plane">The plane to check against.</param>
    /// <returns>null if there is no intersection. Otherwise, a vector containing the point in world
    /// space at which the ray intersects with the plane.</returns>
    public Nullable<Vector> Intersects(Plane plane) {
      // Get the distance.
      Nullable<float> distance = this.IntersectDistance(plane);

      // Check for intersection.
      if(distance == null) {
        // The rays do not intersect.
        return null;
      } else {
        Vector vector = new Vector();
        vector.X = this.Source.X + this.Direction.X * (float)distance;
        vector.Y = this.Source.Y + this.Direction.Y * (float)distance;
        vector.Z = this.Source.Z + this.Direction.Z * (float)distance;
        return vector;
      }
    }

    /// <summary>
    /// Checks if this ray intersects a given plane and returns the distance from the ray's origin
    /// to the point of intersection.
    /// </summary>
    /// <param name="plane"></param>
    /// <returns></returns>
    public Nullable<float> IntersectDistance(Plane plane) {
      // Check for intersection.
      if(Vector.Dot(plane.Normal, this.Direction) == 0) {
        // The rays do not intersect.
        return null;
      }

      return Vector.Dot(plane.Normal, Vector.Zero - this.Source) / Vector.Dot(plane.Normal, this.Direction);
    }
  }
}
