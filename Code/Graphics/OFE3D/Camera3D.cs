﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// A camera for use in 3d scenes.
  /// </summary>
  public class Camera3D : Entity {
		#region Constructor
		public Camera3D(Scene scene) {
      this.Scene = scene;
      scene.Add(this);

			// Create our target.
			Target = new Entity();
			IsTargeted = true;

			// Set our field of view.
			FieldOfView = 45;
			Near = 1;
			Far = 10000;
    }
		#endregion

		#region Target
		/// <summary>
		/// Whether this camera looks at its target.
		/// If false, this camera is "free" and can be manually rotated.
		/// </summary>
		public bool IsTargeted { get; set; }

		/// <summary>
		/// This camera's target.
		/// </summary>
		public Entity Target { get; set; }
		#endregion

		#region Update Cycle
		internal override void Update()
		{
			base.Update();

			// Look at our target.
			if (IsTargeted) Rotation = Quaternion.FromTwoVectors(Position, Target.Position).Inverse();
		}
		#endregion

		#region FieldOfView
		/// <summary>
		/// Whether or not this camera is in orthographic mode, where perspective is ignored. In orthographic
		/// mode, objects will be the same size regardless of their distance to the camera.
		/// </summary>
		public bool Orthographic { get; set; }

		/// <summary>
		/// The field of view value for this camera, in degrees. Must be greater than 0 and less than 180.
		/// </summary>
		public float FieldOfView { get; set; }

		/// <summary>
		/// The near clipping plane. Everything nearer than this distance to the camera will be clipped.
		/// </summary>
		public float Near { get; set; }

		/// <summary>
		/// The far clipping plane. Everything beyond this distance to the camera will be clipped.
		/// </summary>
		public float Far { get; set; }
		#endregion

		#region Matrices
		/// <summary>
    /// Returns this camera's current projection matrix.
    /// </summary>
    /// <remarks>Special thanks to Wudan for his helpful tutorial on this subject. Thanks to him, I only cried tears
    /// and screamed in rage for half as long as I might have otherwise.</remarks>
    public Matrix GetProjectionMatrix() {
      return Matrix.CreatePerspectiveFOV(this.FieldOfView, Display.AspectRatio, this.Near, this.Far);
    }

    /// <summary>
    /// Calculates a 2d screen coordinate's position in 3d space as seen by this camera, based on the given depth.
    /// </summary>
    /// <param name="coordinate">The 2d screen coordinate to work off of.</param>
    /// <param name="depth">How far from the camera to set this point. At 0 it will be set on the near clipping plane, and
    /// at 1 it will be set on the far clipping plane.</param>
    /// <returns></returns>
    public Vector Unproject(Coord coordinate, float depth) {
      // We need to calculate the actual distance based on the near and far planes.
      depth *= (Far - Near);
      
      // Next we convert the screen coordinates to floats based on their realitive positions.
      float modX = Mather.Tan(this.FieldOfView*.5f) * ((1f - coordinate.X / (Display.Width/2)) * Display.AspectRatio);
      float modY = Mather.Tan(this.FieldOfView*.5f) * (1f - coordinate.Y / (Display.Height/2));

      // Now we set the point in its own space based on the depth.
      Vector point = new Vector(modX * depth, modY * depth, depth*-1);
      
      // Grab the camera view matrix.
      Matrix view = Matrix.CreateLookAt(this.Position, this.Target.Position, Vector.Up);

      // Invert the matrix, and transform the point position based on the result. This will put the point in world space
      // relative to the camera.
      //view = Matrix.Invert(view);
      Vector newPoint = Vector.Transform(point, view.Inverted());

      // And now we're done! Hooray!
      return newPoint;
		}
		#endregion
	}
}
