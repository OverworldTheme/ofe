﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if OPENGL
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
#elif XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endif

namespace VisionRiders.OFE.Graphics.OFE3D {
  /// <summary>
  /// In OFE, a model is something like the 3D counterpart to a sprite. It is an object with a mesh
  /// that can be animated and moved within a scene.
  /// </summary>
  public class Model : Entity
	{
		#region Constructors
		public Model(Scene scene) : this(scene, false, null) { }

		public Model(Scene scene, bool active, string fileName) : this(scene, active, fileName, 0, 0, 0) { }

		public Model(Scene scene, bool active, string fileName, float x, float y, float z)
		{
			scene.Add(this);
			this.IsActive = active;

			if (fileName != null && fileName != string.Empty)
			{
				Source = new ModelSource(fileName);
				SetSource(Source);
			}

			this.Position.X = x;
			this.Position.Y = y;
			this.Position.Z = z;

			matrix = new Matrix();
			lightMatrix = new Matrix();
			scale = new Matrix();
			rotation = new Matrix();
			translation = new Matrix();
		}
		#endregion

		#region Source
		/// <summary>
    /// The source model that this model pulls its data from.
    /// </summary>
		public ModelSource Source { get; private set; }

		/// <summary>
		/// Sets this model's source to the given one.
		/// </summary>
		/// <param name="source">The source to set to this model.</param>
		public void SetSource(ModelSource source)
		{
			// Remove the old source (if there was one).
			if (Source != null)
			{
				Source.Remove(this);
			}

			// Make sure a valid source was offered, and then set our source to that.
			if (source != null)
			{
				Source = source;
				Source.Add(this);

				// Make our own copy of all the bones.
				Bones.Clear();
				foreach (Entity bone in source.Bones)
				{
					Entity newBone = new Entity();
					newBone.Name = bone.Name;
					newBone.Position = bone.Position;
					newBone.Rotation = bone.Rotation;
					newBone.Scale = bone.Scale;
					Bones.Add(newBone);
				}
				this.RootBone = source.RootBone;
			}
		}
		#endregion

		#region Bones
		/// <summary>
    /// All the bones that are part of this model.
    /// </summary>
    public List<Entity> Bones = new List<Entity>();

    /// <summary>
    /// The root bone that all the other bones in this model can trace back to.
    /// </summary>
    public Entity RootBone;

    /// <summary>
    /// Sets all this model's bones back to their default positions (as determined
    /// by this model's source).
    /// </summary>
    public void ResetBones() {
      // Make sure the model and its source still have the same number of bones.
      if(Bones.Count != Source.Bones.Count) {
        return;
      }

      // Go through each bone and reset its properties.
      for(int i = 0; i < Bones.Count; i++) {
        Bones[i].Name = Source.Bones[i].Name;
        Bones[i].Position = Source.Bones[i].Position;
        Bones[i].Rotation = Source.Bones[i].Rotation;
        Bones[i].Scale = Source.Bones[i].Scale;
      }
    }
		#endregion

		#region Drawing
		/// <summary>
		/// A matrix object kept in memory to calculate matrix values without creating new objects each frame.
		/// </summary>
		private Matrix matrix;

		/// <summary>
		/// A matrix object kept in memory to calculate light orientation matrix values without creating new objects each frame.
		/// </summary>
		private Matrix lightMatrix;

		/// <summary>
		/// A matrix object kept in memory to calculate a scale matrix without creating new objects each frame.
		/// </summary>
		private Matrix scale;

		/// <summary>
		/// A matrix object kept in memory to calculate a rotation matrix without creating new objects each frame.
		/// </summary>
		private Matrix rotation;

		/// <summary>
		/// A matrix object kept in memory to calculate a translation matrix without creating new objects each frame.
		/// </summary>
		private Matrix translation;

		internal override void Draw() {
      base.Draw();

      // Make sure this model is active, and there's an actual camera in the scene. Otherwise, don't draw it.
      if(!this.IsActive || !this.Visible || Source == null || Scene.Camera == null) {
        return;
      }

      // Make sure the object is within the cull distance.
      if(CullDistance > 0 && Vector.Distance(this.AbsolutePosition, Scene.Camera.AbsolutePosition) > this.CullDistance) {
        return;
      }

      // Make sure we have an actual model to display.
      if(Source.WasFileBad) {
        return;
      }

			// Perform drawing.
			foreach (Mesh mesh in Source.Meshes)
			{
				if (mesh.Material.ShaderProgram == null)
				{
					mesh.Material.ShaderProgram = new ShaderProgram();
					mesh.Material.ShaderProgram.AttachShader(Shader.FromFile(Shader.Type.Vertex, "Graphics//Shaders//3dVertex.shader"));
					mesh.Material.ShaderProgram.AttachShader(Shader.FromFile(Shader.Type.Pixel, "Graphics//Shaders//3dPixel.shader"));				
				}

				// Select our shader.
				ShaderProgram shader = mesh.Material.ShaderProgram;

				if(mesh.Material != null && shader.IsLoaded) {
					// Get our matrix ready.
					scale.BuildScale(AbsoluteScale.X, AbsoluteScale.Y, AbsoluteScale.Z);
          rotation.BuildFromQuaternion(Rotation);
					translation.BuildTranslation(AbsolutePosition.X, AbsolutePosition.Y, AbsolutePosition.Z);
					matrix.Multiply(scale, rotation, translation);
					/*Matrix model = Matrix.CreateScale(AbsoluteScale.X, AbsoluteScale.Y, AbsoluteScale.Z)
						* Matrix.CreateFromQuaternion(Rotation)
						* Matrix.CreateTranslation(AbsolutePosition.X, AbsolutePosition.Y, AbsolutePosition.Z);*/

					// Send camera data to the shader.
					shader.PassVector("CameraPosition", Scene.Camera.Position);

					// Send light data to the shader.
					shader.PassColor("Ambient", Scene.Ambient);

					shader.PassMatrix("World", matrix);

					for (int i = 0; i < Light.MaxLights; i++)
					{
						if (i >= Scene.NumLights)
						{
							shader.PassInt("lights[" + i + "].type", (int)Light.Type.None);
							continue;
						}
						else
						{
							//Vector v = Vector.Transform(Scene.Lights[i].Position, DrawCycle.Projection * DrawCycle.View);
							lightMatrix.Multiply(DrawCycle.Projection, DrawCycle.View);
							shader.PassInt("lights[" + i + "].type", (int)Scene.Lights[i].Kind);
							shader.PassVector("lights[" + i + "].position", Vector.Transform(Scene.Lights[i].Position, lightMatrix));
							//shader.PassVector("lights[" + i + "].position", Vector.Transform(Scene.Lights[i].Position, DrawCycle.Projection * DrawCycle.View * Matrix.Invert(model)));
							shader.PassVector("lights[" + i + "].direction", Scene.Lights[i].Direction);
							//shader.PassVector("lights[" + i + "].direction", Vector.Rotate(Scene.Lights[i].Direction, Rotation.Inverse()));
							//shader.PassVector("lights[" + i + "].direction", Vector.Rotate(Scene.Lights[i].Direction, Scene.Camera.Rotation).Normalized());
							//shader.PassVector("lights[" + i + "].direction", Vector.Transform(Scene.Lights[i].Direction, DrawCycle.Projection * DrawCycle.View));
							shader.PassFloat("lights[" + i + "].intensity", Scene.Lights[i].Intensity);
							shader.PassColor("lights[" + i + "].diffuseColor", Scene.Lights[i].DiffuseColor);
							shader.PassColor("lights[" + i + "].specularColor", Scene.Lights[i].SpecularColor);
						}
					}
					
					// Draw!
					mesh.Draw(matrix);
				}
			}
		}
		#endregion
	}
}
