﻿using System;
using System.Collections.Generic;
using System.Linq;

#if OPENGL
using OpenTK.Graphics;
#elif XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A solid RGBA value used to define color.
  /// </summary>
  /// <remarks>
  /// OFE uses a floating-point variable color system that allows it to remain independant of the system's color depth.
  /// Each color is defined by a red, green, and blue value given as a float in the range of 0 to 1.
  /// There is also an additional alpha value that defines the color's opacity.
  /// 
  /// OFE comes with an assortment of pre-defined colors for your use.
  /// You can get a lot of milage out of these, and their tooltip summaries will give you a quick look at their actual RGB values.
  /// </remarks>
#if DEBUG && WINDOWS
  [System.Diagnostics.DebuggerDisplay("R:{R} G:{G} B:{B} A:{A}")]
#endif
	[OFEdataSerialize]
	public struct Color {
    #region Color List
		#region Major Colors
		/// <summary>
    /// The color black. (0, 0, 0)
    /// </summary>
    public static readonly Color Black = new Color(0.0f, 0.0f, 0.0f);

    /// <summary>
    /// The color blue. (.2, .2, 1)
    /// </summary>
    public static readonly Color Blue = new Color(0.2f, 0.2f, 1.0f);

    /// <summary>
    /// The color brown. (.7, .5, 0)
    /// </summary>
    public static readonly Color Brown = new Color(0.7f, 0.5f, 0.0f);

    /// <summary>
    /// The color gray.
    /// This shade of gray is halfway between white and black. (.5, .5, .5)
    /// </summary>
    public static readonly Color Gray = new Color(0.5f, 0.5f, 0.5f);

    /// <summary>
    /// The color green. (0, .85, 0)
    /// </summary>
    public static readonly Color Green = new Color(0.0f, 0.85f, 0.0f);

    /// <summary>
    /// The color orange. (1, .5, 0)
    /// </summary>
    public static readonly Color Orange = new Color(1.0f, 0.5f, 0.0f);

		/// <summary>
		/// The color purple, not the movie. (.85, 0, 1)
		/// </summary>
		public static readonly Color Purple = new Color(0.85f, 0.0f, 1.0f);

    /// <summary>
    /// The color red. (1, 0, 0)
    /// </summary>
    public static readonly Color Red = new Color(1.0f, 0.0f, 0.0f);

    /// <summary>
    /// The color white. (1, 1, 1)
    /// </summary>
    public static readonly Color White = new Color(1.0f, 1.0f, 1.0f);

    /// <summary>
    /// The color yellow. (1, .95, 0)
    /// </summary>
    public static readonly Color Yellow = new Color(1.0f, 0.95f, 0.0f);
    #endregion

    #region Tertiary Colors
    /// <summary>
    /// The color charteuse, a mix of yellow and green. (.7, 1, .2)
    /// </summary>
    public static readonly Color Chartreuse = new Color(0.7f, 1.0f, 0.2f);

    /// <summary>
    /// The color cyan, a mix of green and blue. (.2, .7, 1)
    /// </summary>
    public static readonly Color Cyan = new Color(0.2f, 0.7f, 1.0f);

		/// <summary>
		/// Magenta, a striking mix of red and blue. (1, 0, 1)
		/// </summary>
		public static readonly Color Magenta = new Color(1.0f, 0.0f, 1.0f);

    /// <summary>
    /// The color pink, a bright varient of light red. (1, .5, .7)
    /// </summary>
    public static readonly Color Pink = new Color(1.0f, 0.5f, 0.7f);

		/// <summary>
		/// The color tan, a light shade of brown similar to that of some people's skin. (.9, .8, .5)
		/// </summary>
		public static readonly Color Tan = new Color(0.9f, 0.7f, 0.5f);

    /// <summary>
    /// The color teal, a dark, vivid mix of blue and green. (0, .6, .6)
    /// </summary>
    public static readonly Color Teal = new Color(0.0f, 0.6f, 0.6f);

		/// <summary>
		/// The color violet, a bluish-purple. (.7, 0, 1)
		/// </summary>
		public static readonly Color Violet = new Color(0.7f, 0.0f, 1.0f);
    #endregion

    #region Minor Colors
		/// <summary>
		/// A deep, vivid shade of red.
		/// </summary>
		public static readonly Color Maroon = new Color(.7f, 0, .2f);

    /// <summary>
    /// A dark, yellowish color. (.7, .7, 0)
    /// </summary>
    public static readonly Color Olive = new Color(0.7f, 0.7f, 0.0f);
    #endregion

		#region Special Colors
		/// <summary>
		/// A value specifying the lack of a color.
		/// </summary>
		/// <remarks>
		/// This "color" has 0 for all values.
		/// </remarks>
		public static readonly Color None = new Color(0, 0, 0, 0);
		#endregion
		#endregion

		#region RBGA Values
		/// <summary>
    /// Red value.
    /// </summary>
    public float R {
      get {
#if OPENGL
        return OGL_ColorFetcher.R;
#elif XNA
        return (float)Math.Round(xnaColor.R / 255f, 2);
#endif
      }
      set {
#if OPENGL
        OGL_ColorFetcher.R = value;
#elif XNA
        xnaColor.R = (byte)(value * 255);
#endif
      }
    }

    /// <summary>
    /// Green value.
    /// </summary>
    public float G {
      get {
#if OPENGL
        return OGL_ColorFetcher.G;
#elif XNA
        return (float)Math.Round(xnaColor.G / 255f, 2); ;
#endif
      }
      set {
#if OPENGL
        OGL_ColorFetcher.G = value;
#elif XNA
        xnaColor.G = (byte)(value * 255);
#endif
      }
    }

    /// <summary>
    /// Blue value.
    /// </summary>
    public float B {
      get {
#if OPENGL
        return OGL_ColorFetcher.B;
#elif XNA
        return (float)Math.Round(xnaColor.B / 255f, 2); ;
#endif
      }
      set {
#if OPENGL
        OGL_ColorFetcher.B = value;
#elif XNA
        xnaColor.B = (byte)(value * 255);
#endif
      }
    }

    /// <summary>
    /// Alpha (opacity) value.
    /// </summary>
    public float A {
      get {
#if OPENGL
        return OGL_ColorFetcher.A;
#elif XNA
        return (float)Math.Round(xnaColor.A / 255f, 2);
#endif
      }
      set {
#if OPENGL
        OGL_ColorFetcher.A = value;
#elif XNA
        xnaColor.A = (byte)(value * 255);
#endif
      }
    }
    #endregion

		#region Variants
		/// <summary>
		/// A darker shade of this color.
		/// </summary>
		/// <remarks>
		/// Returns a color that is halfway between this color and black.
		/// The alpha value will be the same as this color.
		/// </remarks>
		public Color Dark {
			get {
				Color dark = new Color();
				dark.R = Mather.Lerp(this.R, 0, 0.5f);
				dark.G = Mather.Lerp(this.G, 0, 0.5f);
				dark.B = Mather.Lerp(this.B, 0, 0.5f);
				dark.A = this.A;
				return dark;
			}
		}

		/// <summary>
		/// A lighter shade of this color.
		/// </summary>
		/// <remarks>
		/// Returns a color that is halfway between this color and white.
		/// The alpha value will be the same as this color.
		/// </remarks>
		public Color Light {
			get {
				Color light = new Color();
				light.R = Mather.Lerp(this.R, 1, 0.5f);
				light.G = Mather.Lerp(this.G, 1, 0.5f);
				light.B = Mather.Lerp(this.B, 1, 0.5f);
				light.A = this.A;
				return light;
			}
		}
		#endregion

		#region Alpha Multiplication
		/// <summary>
    /// Gets the multiplied alpha version of this color.
    /// </summary>
    public Color Multiplied {
      get {
        Color color = new Color(R, G, B);
#if OPENGL
        // TODO: Add the multiplication math.
        // For now, just return the original color.
        color.A = this.A;
#elif XNA
        float a = this.A;        
        color.xnaColor *= a;
#endif

        return color;
      }
    }
    #endregion

    #region API Color Interfaces
		/// <summary>
		/// Gets the .NET System.Drawing.Color version of this color.
		/// </summary>
		public System.Drawing.Color DotNetColor
		{
			get
			{
				return System.Drawing.Color.FromArgb(
					(int)(A * 255),
					(int)(R * 255),
					(int)(G * 255),
					(int)(B * 255));
			}
		}

#if OPENGL
    internal Color4 OGL_ColorFetcher;
#elif XNA
    internal Microsoft.Xna.Framework.Color xnaColor;
#endif
    #endregion

    #region Class Constructors
    /// <summary>
    /// Class Constructor
    /// </summary>
    public Color(float red, float green, float blue)
      : this(red, green, blue, 1) {}

    /// <summary>
    /// Class Constructor
    /// </summary>
    public Color(float red, float green, float blue, float alpha) {
#if OPENGL
      OGL_ColorFetcher = new Color4(red, green, blue, alpha);
#elif XNA
      xnaColor = new Microsoft.Xna.Framework.Color(red, green, blue, alpha);
#endif
    }
    #endregion

    #region Operators
    public static bool operator ==(Color color1, Color color2) {
      if(color1.R == color2.R
          && color1.G == color2.G
          && color1.B == color2.B
					&& color1.A == color2.A) {
        return true;
      } else {
        return false;
      }
    }

    public static bool operator !=(Color color1, Color color2) {
      if(color1.R == color2.R
          && color1.G == color2.G
          && color1.B == color2.B
					&& color1.A == color2.A) {
        return false;
      } else {
        return true;
      }
    }

    public override int GetHashCode() {
      return base.GetHashCode();
    }

    public override bool Equals(object obj) {
      return base.Equals(obj);
    }
    #endregion

		#region Getting Colors From Strings
		/// <summary>
		/// Gets a color by given name.
		/// </summary>
		/// <param name="name">The name of the color.</param>
		/// <returns>Color.None if a color cannot be found.</returns>
		/// <example>
		/// // Get blue.
		/// Color color1 = Color.GetColorByName("Blue");
		/// 
		/// // Get a lighter blue.
		/// Color color2 = Color.GetColorNyName("Light Blue");
		/// 
		/// // Get dark red; notice that letter case is ignored.
		/// Color color3 = Color.GetColorByName("dArK rED");
		/// </example>
		public static Color GetColorByName(string name) {
			// Check for modifiers.
			string[] words = name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			if(words.Length == 0) {
				// Sanity check.
				return Color.None;
			} else if(words.Length == 1) {
				return getColorByNameWithoutModifiers(words[0]);
			} else {
				Color color = getColorByNameWithoutModifiers(words[1]);
				switch(words[0].Trim().ToLowerInvariant()) {
					case "light":
						return color.Light;
					case "dark":
						return color.Dark;
					default:
						return color;
				}
			}
		}

		/// <summary>
		/// Gets a color from a three or six character hexidecimal number.
		/// </summary>
		/// <param name="hex">The hexadecimal number. Can start with the pound sign (#), or may omit it.</param>
		/// <returns>Color.None if the number is invalid or not recognized.</returns>
		public static Color GetColorByHexadecimal(string hex) {
			// Trim the excess and convert to lower case.
			hex = hex.Trim().ToLowerInvariant();

			// Sanity check.
			if(hex.Length < 3) {
				return Color.None;
			}

			// Create an indexer, and a new working string.
			int i = 0;
			string value = string.Empty;

			// Omit the pound sign as needed.
			if(hex[0] == '#') {
				i++;
			}

			// Read the color.
			while(value.Length < 6 && i < hex.Length) {
				if(hex[i] >= '0' && hex[i] <= '9' || hex[i] >= 'a' && hex[i] <= '9') {
					value += hex[i];
				} else {
					break;
				}
			}

			// Check the length of the value.
			if(value.Length == 6) {
				// Six-digit hexidecimal.
				try {				
					float red = Mather.ConvertHexStringToInt(value.Substring(0, 2)) / 256;
					float green = Mather.ConvertHexStringToInt(value.Substring(2, 2)) / 256;
					float blue = Mather.ConvertHexStringToInt(value.Substring(4, 2)) / 256;
					return new Color(red, green, blue);
				} catch {
					return Color.None;
				}
			} else if(value.Length == 3) {
				// Three-digit hexidecimal.
				try {
					float red = Mather.ConvertHexStringToInt(value.Substring(0, 1)) / 256;
					float green = Mather.ConvertHexStringToInt(value.Substring(1, 1)) / 256;
					float blue = Mather.ConvertHexStringToInt(value.Substring(2, 1)) / 256;
					return new Color(red, green, blue);
				} catch {
					return Color.None;
				}
			} else {
				// Not a valid value.
				return Color.None;
			}
		}

		/// <summary>
		/// Gets a color by given name, without any modifiers.
		/// </summary>
		/// <param name="name">The name of the color.</param>
		/// <returns>Color.None if a color cannot be found.</returns>
		private static Color getColorByNameWithoutModifiers(string name) {
			switch(name.Trim().ToLowerInvariant()) {
				case "black":
					return Color.Black;
				case "gray":
					return Color.Gray;
				case "white":
					return Color.White;

				case "blue":
					return Color.Blue;
				case "brown":
					return Color.Brown;
				case "green":
					return Color.Green;
				case "orange":
					return Color.Orange;
				case "purple":
					return Color.Purple;
				case "red":
					return Color.Red;
				case "yellow":
					return Color.Yellow;

				case "pink":
					return Color.Pink;

				default:
					return Color.None;
			}
		}
		#endregion
	}
}
