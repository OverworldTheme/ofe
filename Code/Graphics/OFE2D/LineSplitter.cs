﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
	/// <summary>
	/// A utility class used to split a string into individual lines.
	/// </summary>
	internal class LineSplitter {
		#region Working Variables
		/// <summary>
		/// The source text we're working with at present.
		/// </summary>
		private string source;

		/// <summary>
		/// A working list of lines, which will be returned by the splitter when finished.
		/// </summary>
		private List<TextChunk> lines;

		/// <summary>
		/// The current character position;
		/// where we're working at within the string.
		/// </summary>
		private int position;

		/// <summary>
		/// The content of the next line, constructed thus far.
		/// </summary>
		private string nextLine;

		/// <summary>
		/// The content of the next word, constructed thus far.
		/// </summary>
		private string nextWord;

		/// <summary>
		/// The current line number from the top.
		/// </summary>
		private int line;

		/// <summary>
		/// The height of each line.
		/// </summary>
		private float height = 0;

		/// <summary>
		/// The x-coordinate that the next line will be assigned.
		/// </summary>
		private float x = 0;

		/// <summary>
		/// The y-coordinate that the next line will be assigned.
		/// </summary>
		private float y = 0;

		/// <summary>
		/// The scale width to assign to lines.
		/// </summary>
		private float scaleWidth = 1;

		/// <summary>
		/// The nested tags currently in effect within the current position of the source string.
		/// </summary>
		private List<TextTag> tags = new List<TextTag>();

		/// <summary>
		/// The nested text colors created by tags that are in effect within the current position of the source string.
		/// </summary>
		private Stack<Color> colorStack = new Stack<Color>();

		/// <summary>
		/// The number of italics tags in effect at the current position of the source string.
		/// </summary>
		private int italicsDepth;

		/// <summary>
		/// Whether or not to ingnore tags for the moment.
		/// </summary>
		private bool ignoreTags;
		#endregion

		#region Splitting
		/// <summary>
		/// Splits this text into lines, formatted to fit in a box.
		/// </summary>
		public List<TextChunk> Split(string text, Font typeface, float width, float lineHeight) {
			// Create the list.
			lines = new List<TextChunk>();

			// Before we go any further, do we even have a font to work with?
			if (typeface == null) return lines;

			// Grab the string and replace any shorthand new lines ("\n") with those defined by Environment.NewLine.
			source = text.Replace("\n", Environment.NewLine);

			// We begin at the beginning.
			position = 0;

			// Reset our local variables in place.
			nextLine = string.Empty;
			nextWord = string.Empty;			
			line = 0;
			height = 0;
			x = 0;
			y = 0;
			scaleWidth = 1;
			ignoreTags = false;

			// Initiate our color stack and tag list.
			colorStack.Clear();
			colorStack.Push(Color.None);
			tags.Clear();
			italicsDepth = 0;

			// Okay, here's how this is going down: We want to start at the beginning of the string
			// and go one-by-one through the characters, adding words to a seperate string that will
			// be copied to the individual lines. Whenever the string becomes long enough that adding the
			// next word will push it beyond the borders of the text box (or we just come to the end
			// of the string), we finish off the line and continue the process as nauseam until we come
			// to the end of the text.
			while(position <= source.Length && source.Length > 0) {
				// See how long the string will be with the next word.
				// While we're at it, let's also see if the character we're about to take out is
				// signalling us to skip to a new line.
				if(x + typeface.Width(nextLine + nextWord) > width || position == source.Length || newlineAt(position)) {
					if(typeface.Width(nextWord) > width) {
						// The box isn't wide enough for a single word!!!
						// Stuff it in anyway. :)
						addChunk(nextWord);
						y += height;
						line++;
						x = 0;
						position += nextWord.Length;
						nextWord = string.Empty;
						continue;
					} else {
						// If we hit a new line marker, add what we had so far.
						if(newlineAt(position)) {
							nextLine += nextWord;
							nextWord = string.Empty;
						}

						// Add this line to the list.
						addChunk(nextLine);
						y += lineHeight;
						line++;
						x = 0;

						// Get ready for the next line.
						nextLine = string.Empty;

						// Trim white space from the beginning of the next word to make
						// sure everything will be lined up correctly.
						nextWord = nextWord.TrimStart();

						// If this character signled a new line, we need to skip to the next one.
						if(newlineAt(position)) {
							position += Environment.NewLine.Length;

							// We want to skip all the white space ahead while we're at it.
							skipSpace();
						}
					}

					// Check if we've hit the end of the content string.
					if(position == source.Length) {
						// Make sure we include the last space, if there is one.
						if(position - 1 > 0 && source[position - 1] == ' ' && lines.Count > 0) {
							lines[lines.Count - 1].Content = lines[lines.Count - 1].Content + ' ';
						}

						// Get us out of this loop.
						break;
					}

					// Make sure there's still room in the box for the next line.
					{
						// Might add this one day! XD
					}
				} else if(position < source.Length) {
					// Check if there's a tag here.
					if(source[position] == '[' && !ignoreTags) {
						TextTag tag = getValidTag(position);

						// Check if this is a valid tag.
						// If not, carry on.
						if(!tag.IsInvalidOrUnrecognized) {
							// Add what we had so far.
							nextLine += nextWord;
							addChunk(nextLine);
							x += typeface.Width(nextLine);
							nextWord = string.Empty;
							nextLine = string.Empty;							

							// Advance past the end of the tag.
							while(source[position] != ']' && position < source.Length) {
								position++;
							}
							position++;

							// Decide what to do about the tag.
							if(!tag.IsClosing && !tag.IsSelfClosing) {
								// This is an opening tag.
								// Add it to the list.
								tags.Add(tag);

								// Decide what to do about it.
								switch(tag.Key) {
									case TextTag.Keyword.Bold:
										scaleWidth += .5f;
										break;
									case TextTag.Keyword.Color:
										// Get the color.
										Color color = Color.GetColorByName(tag.Value);
										if(color == Color.None) {
											color = Color.GetColorByHexadecimal(tag.Value);
										}

										// If the color was invalid, just clone the last one.
										if(color == Color.None) {
											color = colorStack.Peek();
										}

										// Add it to the stack
										colorStack.Push(color);
										break;
									case TextTag.Keyword.Italics:
										italicsDepth++;
										break;
								}
							} else if(tag.IsClosing) {
								// Remove its match from the list.
								bool matchExisted = false;
								for(int i = tags.Count - 1; i >= 0; i--) {
									if(tags[i].Key == tag.Key) {
										tags.RemoveAt(i);
										matchExisted = true;
										break;
									}
								}

								if(matchExisted) {
									// Decide what to do about it.
									switch(tag.Key) {
										case TextTag.Keyword.Bold:
											scaleWidth -= .5f;
											break;
										case TextTag.Keyword.Color:
											// Pop the most recent color off the top.
											if(colorStack.Count > 1) {
												colorStack.Pop();
											}
											break;
										case TextTag.Keyword.Italics:
											italicsDepth--;
											break;
									}
								}
							} else {
								// Handle a self-closing tag.
								switch(tag.Key) {
									case TextTag.Keyword.Beat:
										// Just add an empty line, and then assign it the given beat value.
										TextChunk beatLine = addChunk(string.Empty);

										float wait = tag.FloatValue;
										if(wait != float.NaN && !string.IsNullOrWhiteSpace(tag.Value)) {
											beatLine.Beat = wait;
										} else {
											beatLine.Beat = .5f;
										}
										break;
									case TextTag.Keyword.LineBreak:
										// Begin a new line.
										addChunk(nextLine);										
										x = 0;
										if(!string.IsNullOrEmpty(tag.Value)) {
											for(int i = 0; i < tag.IntValue; i++) {
												y += lineHeight;
												line++;
											}
										} else {
											y += lineHeight;
											line++;
										}
										skipSpace();
										break;
								}
							}
							continue;
						}
					}

					if(!char.IsWhiteSpace(source[position])) {
						// Add the next letter.
						nextWord += source[position];
					}

					if(char.IsWhiteSpace(source[position]) || position + 1 == source.Length) {
						// Add the current word to the string.
						nextLine += nextWord;
						nextWord = source[position].ToString();
					}

					// Go to the next character.
					position++;
				}
			}

			// Done. Whew!
			return lines;
		}
		#endregion

		#region Alignment
		/// <summary>
		/// Takes a group of lines and adjusts them so that they're in the given alignment.
		/// </summary>
		/// <param name="lines">A list collection of the lines that need to be adjusted</param>
		/// <param name="typeface">The typeface these lines use.</param>
		/// <param name="alignment">The alignment to give these lines.</param>
		public void Align(List<TextChunk> lines, Font typeface, Font.Align alignment) {
			// If we don't have any lines, don't bother.
			if(lines == null || lines.Count <= 0) {
				return;
			}

			int line = 0;
			float lineWidth = 0;			
			float widthSoFar = 0;

			List<TextChunk> thisLine = new List<TextChunk>(lines.Count);
			for(position = 0; position <= lines.Count; position++) {
				// Check if we've gotten all the members of this line.
				if(position == lines.Count || lines[position].Line != line) {
					widthSoFar = 0;
					foreach(TextChunk member in thisLine) {
						// Reset alignment.
						member.X = widthSoFar;
						widthSoFar += typeface.Width(member.Content) * member.Scale.X;

						// Give new alignment.
						switch(alignment) {
							case Font.Align.Left:
							case Font.Align.Justify:
								// Do nothing.
								break;
							case Font.Align.Right:
								member.X -= lineWidth;
								break;
							case Font.Align.Center:
								member.X -= lineWidth / 2;
								break;
						}
					}

					// If we're at the end, we're done.
					// Otherwise, move down to the next group of lines.
					if(position == lines.Count) {						
						break;
					} else {
						thisLine.Clear();
						line = lines[position].Line;
						lineWidth = 0;
					}
				}

				// Add the next member of the current line, and then continue.
				lineWidth += typeface.Width(lines[position].Content) * lines[position].Scale.X;
				thisLine.Add(lines[position]);
			}
		}
		#endregion

		#region Tags
		/// <summary>
		/// Gets the tag from the given position, and makes sure that it has a matching closing tag.
		/// The given position must be situated on the opening brace.
		/// </summary>
		/// <param name="atPosition">The position to get the tag from.</param>
		/// <returns>
		/// <para>TextTag.Keyword.Invalid if the current position is not situtated on the opening brace,
		/// or the tag is mangled or missing a proper closing tag.</para>
		/// <para>Closing tags will have the flag TextTag.Keyword.Closing,
		/// but a valid, matching opening tag will not be searched for.</para>
		/// <para>Self-closing tags will have the flag TextTag.Keyword.SelfClosing.</para></returns>
		private TextTag getValidTag(int atPosition) {
			// Make sure we're in bounds before doing anything.
			if(atPosition < 0 || atPosition >= source.Length) return new TextTag(TextTag.Keyword.None);

			// Sanity check to make sure the caller followed instructions and we're actually at the start of a tag.
			if(source[atPosition] != '[') return new TextTag(TextTag.Keyword.Invalid);

			// Read the tag.
			TextTag tag = readTag(atPosition);

			// If it's invalid, it's invalid.
			// No need to check anything else.
			if(tag.Key == TextTag.Keyword.Invalid) {
				return tag;
			}

			// Okay, now that we've read the tag, make sure there's a closing tag.
			if(!tag.IsSelfClosing) {
				if(!hasClosingTag(tag, atPosition + 1)) {
					return new TextTag(TextTag.Keyword.Invalid);
				}
			}

			// Looks like this tag is valid thus far.
			return tag;
		}

		/// <summary>
		/// Reads the tag at the given position in the source string.
		/// </summary>
		/// <param name="atPosition"></param>
		/// <returns></returns>
		public TextTag readTag(int atPosition) {
			// Okie-dokie, let's get started.
			int i = atPosition;

			// Declare our return object.
			TextTag tag = new TextTag();

			// Get ready to read the tag.
			bool readingValue = false;
			bool readingString = false;

			// Screw good programming practices!
			// We're starting an infinite loop!
			// Yeah baby!
			while(true) {
				// Advance one.
				i++;

				// If we're out of bounds and haven't hit the end of the tag,
				// then the tag is invalid.
				if(i >= source.Length) {
					return new TextTag(TextTag.Keyword.Invalid);
				}

				// Check the current character.
				if(source[i] == ']') {
					// The tag has been closed.
					// Trim all whitespace.
					tag.KeywordText = tag.KeywordText.Trim();
					tag.Value = tag.Value.Trim();

					// Make sure any string was closed; if so this tag was invalid.
					// Otherwise, let's get out of this infernal loop!
					if(readingString) {
						return new TextTag(TextTag.Keyword.Invalid);
					} else {
						break;
					}
				} else if(tag.IsSelfClosing) {
					// If a tag has already declared itself self-closing, it can't contain any more information.
					// If this character isn't whitespace, the tag is invalid.
					// Otherwise, keep advancing to the closing brace.
					if(char.IsWhiteSpace(source[i])) {
						continue;
					} else {
						return new TextTag(TextTag.Keyword.Invalid);
					}
				} else if(source[i] == '[') {
					// You can't put a tag IN a tag.
					return new TextTag(TextTag.Keyword.Invalid);
				} else if(source[i] == '=') {
					// Okay, we finished reading the keyword, now we start on the value.
					readingValue = true;
				} else if(source[i] == '/' && !readingString) {
					// This tag is either a closing tag, or a self-closing tag.
					// Let's check which!
					if(tag.IsClosing || tag.IsSelfClosing) {
						// Oops! The tag has more than one closing marker and is thus invalid. Sorry!
						return new TextTag(TextTag.Keyword.Invalid);
					}
					if(tag.KeywordText.Trim() == string.Empty) {
						// If we're at the beginning of the tag, then this must be a closing tag.
						tag.IsClosing = true;
					} else {
						// This must be a self-closing tag.						
						tag.IsSelfClosing = true;
					}
				} else {
					// Check if we hit a string marker.
					if(source[i] == '"') {
						// If we're reading the value, either start or end reading the string.
						// If we're still on the keyword, however, then this is an invalid tag because
						// the keyword can't contain such a thing.
						if(readingValue) {
							// Go, go gadget boolean toggle!
							readingString = !readingString;
						} else {
							return new TextTag(TextTag.Keyword.Invalid);
						}
					}

					// Either way, add this character to the current string.
					if(readingValue) {
						tag.Value += source[i];
					} else {
						tag.KeywordText += source[i];						
					}
				}
			}

			// Let us inspect the tag's key and see what it is!
			switch(tag.KeywordText.ToLowerInvariant()) {
				case "b":
				case "bold":
				case "strong":
					tag.Key = TextTag.Keyword.Bold;
					break;
				case "beat":
				case "pause":
					tag.Key = TextTag.Keyword.Beat;
					tag.IsSelfClosing = true;
					break;
				case "br":
					tag.Key = TextTag.Keyword.LineBreak;
					tag.IsSelfClosing = true;
					break;
				case "color":
					tag.Key = TextTag.Keyword.Color;
					break;
				case "i":
				case "em":
				case "italic":
					tag.Key = TextTag.Keyword.Italics;
					break;
				case "link":
				case "url":
					tag.Key = TextTag.Keyword.Link;
					break;
				case "s":
					tag.Key = TextTag.Keyword.Strikethrough;
					break;
				case "sub":
					tag.Key = TextTag.Keyword.Subscript;
					break;
				case "sup":
					tag.Key = TextTag.Keyword.Superscript;
					break;
				case "tab":
					tag.Key = TextTag.Keyword.Tab;
					tag.IsSelfClosing = true;
					break;
				case "u":
					tag.Key = TextTag.Keyword.Underline;
					break;
				default:
					// Unrecognized tag.
					tag.Key = TextTag.Keyword.Unrecognized;
					break;
			}

			return tag;
		}

		/// <summary>
		/// Makes sure that there is a closing tag for the given tag.
		/// </summary>
		/// <param name="tag">The tag name.</param>
		/// <param name="searchFrom">The position in the source string to search from.</param>
		/// <returns>True if there is a valid closing tag for the given tag.
		/// Otherwise, false.</returns>
		private bool hasClosingTag(TextTag tag, int searchFrom) {
			// Check if it's already closed itself.
			if(tag.IsClosing || tag.IsSelfClosing) {
				return true;
			}

			int i = searchFrom;
			while(i < source.Length) {
				if(source[i] == '[') {
					TextTag newTag = readTag(i);
					if(newTag.KeywordText.ToLowerInvariant() == tag.KeywordText.ToLowerInvariant() && newTag.IsClosing) {
						return true;
					}
				}

				// Advance.
				i++;
			}

			// No matching closing tag was found.
			return false;
		}
		#endregion

		#region Utility Methods
		/// <summary>
		/// Adds the given content as a new chunk of text.
		/// All set properties will be assigned to the chunk (such as location, width, italics, and so on).
		/// </summary>
		/// <param name="content"></param>
		private TextChunk addChunk(string content) {
			// Add the line.
			lines.Add(new TextChunk(content, line, colorStack.Peek(), x, y));

			// Assign width.
			lines[lines.Count - 1].Scale.X = scaleWidth;

			// Addign italics.
			if(italicsDepth > 0) {
				lines[lines.Count - 1].Skew = 0.33f;
			}

			// Return the line object.
			return lines[lines.Count - 1];
		}

		/// <summary>
		/// Skip all spaces until we come to another character.
		/// </summary>
		private void skipSpace() {
			while(position < source.Length) {
				if(source[position] == ' ') {
					position++;
				} else {
					break;
				}
			}
		}

		/// <summary>
		/// Checks if there's a newline marker at the given location in the source string.
		/// </summary>
		/// <returns>Will avoid the exception and return false if you specify a location out of bounds.</returns>
		private bool newlineAt(int at) {
			// Make sure we're in bounds.
			if(at < 0 || at + Environment.NewLine.Length > source.Length - 1) {
				return false;
			}

			// Check for the new line.
			if(source.Substring(at, Environment.NewLine.Length) == Environment.NewLine) {
				return true;
			} else {
				return false;
			}
		}
		#endregion
	}
}
