﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A 2d sprite that can be displayed on the screen.
  /// </summary>
#if DEBUG
  [System.Diagnostics.DebuggerDisplay("{ Source.SpriteSheet.FileName } ({X}, {Y})")]
#endif
  public class Sprite : Graphic {
		#region Sprite Types
		/// <summary>
		/// The different kinds of sprites.
		/// </summary>
		public enum Type {
			/// <summary>
			/// The sprite is animated and has animation data to play.
			/// </summary>
			Dynamic,

			/// <summary>
			/// The sprite has no animation data.
			/// </summary>
			Static,

			/// <summary>
			/// The sprite is laid out on its texture in a grid
			/// and the application will handle its animation on its own
			/// by specifying which cell in the grid to display.
			/// </summary>
			Grid,
		}
		#endregion

		#region Creation
		/// <summary>
		/// Creates a blank sprite for later use and assigns it to the given stage.
		/// </summary>
		/// <param name="stage">The stage you want this sprite to be part of.</param>
		public Sprite(Stage stage) {
			this.Load(null);
			SetStage(stage);
		}

		/// <summary>
		/// Creates a new sprite using the given file.
		/// </summary>
		/// <param name="stage">The stage you want this sprite to be part of.</param>
		/// <param name="imageFile">The image file to load the sprite from.</param>
		/// <param name="z">What z-depth to give this sprite.</param>
		public Sprite(Stage stage, string imageFile, float z) {
			this.Load(imageFile);
			this.CenterOrigin();
			this.Z = z;

			SetStage(stage);
		}

		/// <summary>
		/// Creates a new sprite using the given sprite source.
		/// </summary>
		/// <param name="stage">The stage you want this sprite assigned to.</param>
		/// <param name="spriteSource">The sprite source for this sprite to draw its graphics from.</param>
		/// <param name="z">The z-depth to assign this sprite.</param>
		public Sprite(Stage stage, SpriteSource spriteSource, float z) {
			this.SetSource(spriteSource);
			this.CenterOrigin();
			this.Z = z;

			SetStage(stage);
		}

		/// <summary>
		/// Creates a new sprite using the given file.
		/// </summary>
		/// <param name="stage">The stage you want this sprite to be part of.</param>
		/// <param name="imageFile">The image file to load the sprite from.</param>
		/// <param name="z">What z-depth to give this sprite.</param>
		/// <param name="x">Where on the x-axis of the stage to display this sprite.</param>
		/// <param name="y">Where on the y-axis of the stage to display this sprite.</param>
		public Sprite(Stage stage, string imageFile, float z, float x, float y) {
			this.Load(imageFile);
			this.CenterOrigin();

			this.X = x;
			this.Y = y;
			this.Z = z;

			SetStage(stage);
		}

		/// <summary>
		/// Creates a new sprite using the given sprite source.
		/// </summary>
		/// <param name="stage">The stage you want this sprite assigned to.</param>
		/// <param name="spriteSource">The sprite source for this sprite to draw its graphics from.</param>
		/// <param name="z">The z-depth to assign this sprite.</param>
		/// <param name="x">Where on the x-axis of the stage to display this sprite.</param>
		/// <param name="y">Where on the y-axis of the stage to display this sprite.</param>
		public Sprite(Stage stage, SpriteSource spriteSource, float z, float x, float y) {
			this.SetSource(spriteSource);
			this.CenterOrigin();

			this.X = x;
			this.Y = y;
			this.Z = z;

			SetStage(stage);
		}
		#endregion		

		#region Sprite Source
		/// <summary>
		/// Gets the source image that this sprite draws from.
		/// </summary>
		public SpriteSource Source {
			get {
				return source;
			}
		}
		private SpriteSource source;

		/// <summary>
		/// Gets whether or not this sprite's source texture has finished loading.
		/// Will also return "true" if this sprite has no source.
		/// </summary>
		public bool IsLoaded {
			get {
				if(Source == null) {
					return true;
				} else {
					return Source.IsLoaded;
				}
			}
		}

		/// <summary>
		/// Sets this sprite's source to the given one.
		/// </summary>
		/// <param name="source">The new source sheet for this sprite.</param>
		public void SetSource(SpriteSource source) {
			// Don't bother if it's just the same old thing.
			if(source == this.Source) {
				return;
			}

			// Detach the old source.
			if(this.source != null) {
				this.source.Detach(this);
			}

			// Set the new source.
			this.source = source;
			if(this.source != null) {
				this.source.Attach(this);
			}
		}
		#endregion

		#region Grid Sprites
		/// <summary>
		/// For grid sprites, which part of the Sprite Sheet will be displayed.
		/// </summary>
		public CoordInt Cell;
		#endregion

		#region Origin
		/// <summary>
		/// Centers the origin point of this sprite.
		/// </summary>
		/// <remarks>
		/// Centering a sprite's origin point before its source has finished loading will alert
		/// it to do so once that source is available.
		/// </remarks>
		public override void CenterOrigin() {
			if(this.Source != null) {
				if(this.Source.IsLoaded) {
					switch(Source.Kind) {
						case Type.Static:
							this.SetOrigin(this.Source.SheetCenter.X, this.Source.SheetCenter.Y);
							break;
						case Type.Grid:
							this.SetOrigin(this.Source.CellSize.X / 2, this.Source.CellSize.Y / 2);
							break;
						case Type.Dynamic:
							// To-do.
							break;
					}
				} else {
					queuedCenterOrigin = true;
				}
			}
		}

		/// <summary>
		/// If CenterOrigin is called while a sprite's source graphics are loading,
		/// it will remember to call CenterOrigin properly on its own once it has
		/// finished loading.
		/// </summary>
		private bool queuedCenterOrigin = false;

		/// <summary>
		/// This sprite's origin point.
		/// </summary>
		public override Coord Origin {
			get {
				return base.Origin;
			}
			set {
				base.Origin = value;

				// We don't need to mess with the origin point anymore unless otherwise directed to do so.
				queuedCenterOrigin = false;
			}
		}
		#endregion

		#region Frame Size
		/// <summary>
    /// Gets the width of the graphics in this sprite's current frame. Does not include any additional
    /// elements such as collision boundries.
    /// </summary>
		/// <remarks>If the sprite has no source texture, the sprite's width is considered to be 0.</remarks>
		public override float Width {
			get {
				if(Source != null) {
					switch(Source.Kind) {
						case Type.Grid:
							return Source.CellSize.X;
						case Type.Static:
						default:
							return Source.Width;
					}
				} else {
					return 0;
				}
			}
			set {
				if(Source != null) {
					switch(Source.Kind) {
						case Type.Grid:
							ScaleWidth = value / Source.CellSize.X;
							break;
						case Type.Static:
							ScaleWidth = value / Source.Width;
							break;
					}
				}
			}
		}

    /// <summary>
    /// Gets the height of the graphics in this sprite's current frame. Does not include any additional
    /// elements such as collision boundries.
    /// </summary>
		/// <remarks>If the sprite has no source texture, the sprite's height is considered to be 0.</remarks>
    public override float Height {
      get {
				if(Source != null) {
					switch(Source.Kind) {
						case Type.Grid:
							return Source.CellSize.Y;
						case Type.Static:
						default:
							return Source.Height;
					}
				} else {
					return 0;
				}
      }
			set {
				if(Source != null) {
					switch(Source.Kind) {
						case Type.Grid:
							ScaleHeight = value / Source.CellSize.Y;
							break;
						case Type.Static:
							ScaleHeight = value / Source.Height;
							break;
					}
				}
			}
    }
		#endregion

		#region Display Properties
    /// <summary>
    /// The hue to apply to this sprite.
    /// </summary>
    public Color Hue = Color.White;
		#endregion

		#region Update Cycle
		public override void Update() {
			base.Update();

			if(queuedCenterOrigin && Source != null && Source.IsLoaded) {
				CenterOrigin();
			}
		}
		#endregion

		#region Drawing
		/// <summary>
    /// Draws this sprite to the screen.
    /// </summary>
    public override void Draw() {
			if(this.IsActive && this.IsVisible && this.source != null) {
				// Set our shader as active.
				DrawCycle.SetShader(Shader);

				// "Blit" the sprite. Go on, do it!
				switch(Source.Kind) {
					case Type.Static:
						this.Source.Draw(new Coord(AbsolutePosition.X, AbsolutePosition.Y),
							AbsoluteZ, this.Origin, this.Flip, this.Rotation, this.ScaleWidth, this.ScaleHeight, this.Opacity, this.Hue);
						break;
					case Type.Grid:
						this.Source.SpriteSheet.Blit(
							new Rectangle(Source.Offset.X + Cell.X * Source.CellSize.X,
								Source.Offset.Y + Cell.Y * Source.CellSize.Y,
								Source.Offset.X + (Cell.X + 1) * Source.CellSize.X,
								Source.Offset.Y + (Cell.Y + 1) * Source.CellSize.Y),
							AbsolutePosition,
							AbsoluteZ,
							Origin,
							Flip,
							Rotation,
							new Coord(ScaleWidth, ScaleHeight),
							Opacity,
							Hue);
						break;
				}
			}
    }
		#endregion

		#region Loading
		/// <summary>
    /// Loads a sprite from a file.
    /// </summary>
    /// <param name="imageFile">The relative path and filename of the image file to load.</param>
    public void Load(string imageFile) {
			// Make sure we're actually requesting a file.
			if(imageFile == null || imageFile == string.Empty) {
				SetSource(null);
				return;
			}

      // Check if there's already a managed instance of the given sprite source.
      SpriteSource managed = SpriteSource.GetManagedInstanceOf(imageFile);

			if(managed != null) {
				// Tell the log and set the source.
				DebugLog.Whisper("There is a managed instance of \"" + imageFile + "\".");
				SetSource(managed);
			} else {
				SetSource(new SpriteSource(true, imageFile));
			}
    }
		#endregion
				
    #region Death
    protected override void dispose(bool dispose) {
      base.dispose(dispose);

			if(dispose) {
				if(this.Source != null) {
					this.source.Detach(this);
				}
			}
    }
    #endregion
  }
}
