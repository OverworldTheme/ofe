﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A map of square tiles displayed as a group to create the appearance of a larger graphical object,
  /// such as a map.
  /// </summary>
  public class TileMap : Graphic {
    #region Class Constructor
        /// <summary>
    /// Creates a new tilemap using the given texture.
    /// </summary>
    /// <param name="stage">The stage where this tile map will be placed.</param>
    /// <param name="texture">The texture to use for this tilemap.</param>
    /// <param name="width">The number of tile columns to give this tilemap.</param>
    /// <param name="height">The number of tile rows to give this tilemap.</param>
    /// <param name="tileSize">The size to make each tile.</param>
    /// <param name="sourceSize">The source size of each tile in the source texture, in pixels, not including the rise.</param>
    /// <param name="rise">An extra number of pixels at the top of each tile that overlaps those directly to the "north" of them.
    /// Given as their size on the source texture, in pixels. Set to 0 if you're not using a rise.</param>
    public TileMap(Stage stage, Texture texture, Coord tileSize, Coord sourceSize, int width, int height, int rise)
      : this(stage, texture, tileSize, sourceSize, width, height, rise, 1) { }

    /// <summary>
    /// Creates a new tilemap using the given texture.
    /// </summary>
    /// <param name="stage">The stage where this tile map will be placed.</param>
    /// <param name="texture">The texture to use for this tilemap.</param>
    /// <param name="width">The number of tile columns to give this tilemap.</param>
    /// <param name="height">The number of tile rows to give this tilemap.</param>
    /// <param name="tileSize">The size to make each tile.</param>
    /// <param name="sourceSize">The source size of each tile in the source texture, in pixels, not including the rise.</param>
    /// <param name="rise">An extra number of pixels at the top of each tile that overlaps those directly to the "north" of them.
    /// Given as their size on the source texture, in pixels. Set to 0 if you're not using a rise.</param>
    /// <param name="z">The z-depth to assign to the tilemap.</param>
    public TileMap(Stage stage, Texture texture, Coord tileSize, Coord sourceSize, int width, int height, int rise, float z) {
      Texture = texture;
      Layout = new TileMapTile[width, height];
      TileSize = tileSize;
      SourceSize = sourceSize;
      Rise = rise;
      Origin = new Coord(TileSize.X * width / 2, TileSize.Y * height / 2);
      Position = stage.Center;
      Z = z;

			Hue = Color.White;

			for(int y = 0; y < height; y++) {
				for(int x = 0; x < width; x++) {
					Layout[x, y] = new TileMapTile();
				}
			}

			SetStage(stage);
    }
    #endregion

		#region Properties
		/// <summary>
		/// The hue to give to all the tiles of this tile map.
		/// </summary>
		public Color Hue { get; set; }
		#endregion

		#region Graphic Size
		/// <summary>
    /// The total height of this tilemap.
    /// </summary>
    public override float Height {
      get {
        return (Rows * TileSize.Y) * ScaleHeight;
      }
			set {
				ScaleHeight = (Rows * TileSize.Y) / value;
			}
    }

    /// <summary>
    /// The total width of this tilemap.
    /// </summary>
    public override float Width {
      get {
        return (Columns * TileSize.X) * ScaleWidth;
      }
			set {
				ScaleWidth = (Columns * TileSize.X) / value;
			}
    }

    /// <summary>
    /// The y-coordinate of the top of this tilemap.
    /// </summary>
    public float Top {
      get {
        return Y * ScaleHeight - Origin.Y;
      }
    }

    /// <summary>
    /// The y-coordinate of the bottom of this tilemap.
    /// </summary>
    public float Bottom {
      get {
        return Top + Height;
      }
    }

    /// <summary>
    /// The x-coordinate of the left side of this tilemap.
    /// </summary>
    public float Left {
      get {
				return X * ScaleWidth - Origin.X;
      }
    }

    /// <summary>
    /// The x-coordinate of the right side of this tilemap.
    /// </summary>
    public float Right {
      get {
        return Left + Width;
      }
    }
    #endregion

    #region Layout
    /// <summary>
    /// This tilemap's layout.
    /// Each value refers to a specific tile on the source texture.
    /// </summary>
    public TileMapTile[,] Layout;

		/// <summary>
		/// Allows you to access the tile map values quickly.
		/// </summary>
		/// <param name="x">X-coordinate of the tile value to get or set..</param>
		/// <param name="y">Y-coordinate of the tile value to get or set.</param>
		/// <returns>The source cell of the given tile.</returns>
		/// <exception cref="IndexOutOfRangeException">
		/// An IndexOutOfRangeException will be thrown if you try to access a range outside of the tile map array.
		/// </exception>
		public CoordInt this[int x, int y] {
			get {
				return Layout[x, y].Tile;
			}
			set {
				Layout[x, y].Tile = value;
			}
		}

    /// <summary>
    /// The number of columns of tiles in this map.
    /// </summary>
    public int Columns {
      get {
        return Layout.GetLength(0);
      }
    }

    /// <summary>
    /// The number of rows of tiles in this map.
    /// </summary>
    public int Rows {
      get {
        return Layout.GetLength(1);
      }
    }

    /// <summary>
    /// The display size of each tile.
    /// </summary>
    public Coord TileSize { get; set; }

		/// <summary>
		/// Move this tile map's layout by the given amount.
		/// Tiles on the edges will be wrapped to the other side.
		/// </summary>
		/// <remarks>
		/// This method is useful for creating things like scrolling levels.
		/// Make the tile map bigger than what's displayed on screen,
		/// and then add new tiles to the edges of the tile map where the player can't see.
		/// </remarks>
		/// <param name="x">How many columns to shift to the left or right.</param>
		/// <param name="y">How many rows to shift up or down.</param>
		public void Shift(int x, int y) {
			// Make sure there's something to shift so that we're not wasting CPU cycles.
			if(x == 0 && y == 0)
				return;

			// Create a new layout.
			// We'll copy the old layout to this.
			TileMapTile[,] newMap = new TileMapTile[Columns, Rows];

			// Begin copying over.
			for(int i = 0; i < Columns; i++) {
				for(int j = 0; j < Rows; j++) {
					newMap[Mather.Modulo(i + x, Columns), Mather.Modulo(j + y, Rows)] = Layout[i, j];
				}
			}

			// Replace the old with the new.
			Layout = newMap;
		}
		#endregion

		#region Source Texture
		/// <summary>
		/// The texture where all this tilemap's tiles are stored.
		/// </summary>
		public Texture Texture { get; set; }

    /// <summary>
    /// The size of each tile in the source texture, in pixels.
    /// </summary>
    public Coord SourceSize { get; set; }

    /// <summary>
    /// The amount of overlap tiles should have that allows their tops to cover those directly above them.
    /// </summary>
    public int Rise { get; set; }

    /// <summary>
    /// The spacing on every side of the tile.
    /// The space between each tile is thus equal to twice this value.
    /// </summary>
    public int Gutter { get; set; }
    #endregion

    #region Drawing
    /// <summary>
    /// The locations in the layout array of the first visible row and column.
    /// </summary>
    public CoordInt First {
      get {
				// TODO: This is a hack for making sure flipped tile maps display for now.
				if(Flip != Flip.None)
					return CoordInt.Zero;

        CoordInt begin = new CoordInt(
          Mather.Floor((Stage.Left - this.Left) / TileSize.X),
          Mather.Floor((Stage.Top - this.Top) / TileSize.Y)
        );

        if(begin.X < 0) {
          begin.X = 0;
        }
        if(begin.Y < 0) {
          begin.Y = 0;
        }

        return begin;
      }
    }

    /// <summary>
    /// The locations in the layout array of last visible row and column.
    /// </summary>
    public CoordInt Last {
      get {
        CoordInt begin = First;

				CoordInt end = new CoordInt(
          begin.X + Mather.Ceiling((Stage.Width / TileSize.X)) + 1,
          begin.Y + Mather.Ceiling((Stage.Height / TileSize.Y) + (Rise / TileSize.Y)) + 1
        );
        
        if(end.X >= Columns) {
          end.X = Columns - 1;
        }
        if(end.Y >= Rows) {
          end.Y = Rows - 1;
        }

        return end;
      }
    }

		private Matrix scaleMatrix = new Matrix();
		private Matrix positionMatrix = new Matrix();
		private Matrix rotationMatrix = new Matrix();
		Vertex[] verts = new Vertex[] { new Vertex(), new Vertex(), new Vertex(), new Vertex() };

		/// <summary>
		/// Displays the entire tilemap to the stage.
		/// </summary>
		public override void Draw() {
      // Make sure we were invited to the party.
      if(!IsActive || !IsVisible) {
        return;
      }

			// Set our shader as active.
			DrawCycle.SetShader(Shader);

      // Set the texture.
      DrawCycle.SetTexture(Texture);

			// Construct the world matrix.
			DrawCycle.World = Matrix.Identity; //Matrix.Create2dTranslation(-Stage.Origin.X, -Stage.Origin.Y);

			// Decide on the color to use.
			Color color = Hue;
			color.A *= Opacity;


      // We'll need this quad to stamp each tile.
      Quad quad = new Quad(0, 1, 3, 2);

      // We'll reuse this array to send out each tile via Quad.Out().
			//Vertex[] verts = new Vertex[] { new Vertex(), new Vertex(), new Vertex(), new Vertex() };

			// If we're flipped, draw backwards on that axis!
			Coord scale = new Coord(ScaleWidth, ScaleHeight);
			Coord origin = Origin;
			if(Flip == Flip.Horizontal || Flip == Flip.Both) {
				scale.X *= -1;
			}
			if(Flip == Flip.Vertical || Flip == Flip.Both) {
				scale.Y *= -1;
			}

			// Set the World Matrix.
			// TODO: I have no clue why I have to multiply these backwards...
			positionMatrix.BuildTranslation(X + origin.X, Y + origin.Y, Z);
			rotationMatrix.Build2dRotation(Rotation);
			scaleMatrix.Build2dScale(scale.X, scale.Y);
			DrawCycle.World.Multiply(positionMatrix, rotationMatrix, scaleMatrix);
			/*DrawCycle.World = Matrix.CreateTranslation(X + origin.X, Y + origin.Y, Z)
				* Matrix.Create2dRotation(Rotation)
				* Matrix.CreateScale(scale.X, scale.Y, 1);*/

      // Find out where to begin and end. We don't want to bother with tiles outside the screen.
      // Don't forget to take the rise into account for the height!
			CoordInt begin = First;
			CoordInt end = Last + CoordInt.One;

      // Time for the hard part!
			// Let's make some grids real quick.
			// The first will be the upper-left corner of each tile.
			// The second will be the lower-right corner of each tile.
			// We'll need a grid for the sources, and let's also make matching grids for the tile rise.
			Coord[,] grid1 = new Coord[Layout.GetLength(0), Layout.GetLength(1)];
			Coord[,] grid2 = new Coord[Layout.GetLength(0), Layout.GetLength(1)];
			Coord[,] src1 = new Coord[Layout.GetLength(0), Layout.GetLength(1)];
			Coord[,] src2 = new Coord[Layout.GetLength(0), Layout.GetLength(1)];

			// Let's get the grid going.
			for(int y = begin.Y; y < end.Y; y++) {
				for(int x = begin.X; x < end.X; x++) {
					grid1[x, y] = new Coord(x * TileSize.X, y * TileSize.Y);
					grid2[x, y] = new Coord((x + 1) * TileSize.X, (y + 1) * TileSize.Y);

					src1[x, y] = new Coord(
						(this[x, y].X * (SourceSize.X + (Gutter * 2)) + Gutter) / Texture.Width,
						(this[x, y].Y * (SourceSize.Y + Rise + (Gutter * 2)) + Gutter) / Texture.Height
						);
					src2[x, y] = new Coord(
						((this[x, y].X + 1) * (SourceSize.X + (Gutter * 2)) - Gutter - 0.1f) / Texture.Width,
						((this[x, y].Y + 1) * (SourceSize.Y + Rise + (Gutter * 2)) - Gutter - 0.1f) / Texture.Height
						);

					// Flip the source as needed.
					if(Layout[x,y].Flip == Flip.Horizontal || Layout[x,y].Flip == Flip.Both)
						Mather.Swap(ref src1[x, y].X, ref src2[x, y].X);
					if(Layout[x, y].Flip == Flip.Vertical || Layout[x, y].Flip == Flip.Both)
						Mather.Swap(ref src1[x, y].Y, ref src2[x, y].Y);
				}
			}

			// Now begin drawing!
      for(int y = begin.Y; y < end.Y; y++) {
        for(int x = begin.X; x < end.X; x++) {
          // Only draw valid tiles, and those within bounds of the screen.
          if(this[x, y].X >= 0 && this[x, y].Y >= 0) {
            // Draw the rise.
            if(Rise > 0) {
              /*float difference = Rise * (TileSize.X / SourceSize.X);
              verts[0] = new Vertex(x * TileSize.X, y * TileSize.Y - difference, 0,
								(this[x, y].X * (SourceSize.X + Gutter * 2) + Gutter) / Texture.Width, (this[x, y].Y * (SourceSize.Y + Rise + Gutter * 2) + Gutter) / Texture.Height);
              verts[1] = new Vertex((x + 1) * TileSize.X, y * TileSize.Y - difference, 0,
								((this[x, y].X + 1) * (SourceSize.X + Gutter * 2) - 1 - Gutter) / Texture.Width, (this[x, y].Y * (SourceSize.Y + Rise + Gutter * 2) + Gutter) / Texture.Height);
              verts[2] = new Vertex(x * TileSize.X, y * TileSize.Y, 0,
								(this[x, y].X * (SourceSize.X + Gutter * 2) + Gutter) / Texture.Width, (this[x, y].Y * (SourceSize.Y + Rise + Gutter * 2) + Rise + Gutter) / Texture.Height);
              verts[3] = new Vertex((x + 1) * TileSize.X, y * TileSize.Y, 0,
								((this[x, y].X + 1) * (SourceSize.X + Gutter * 2) - 1 - Gutter) / Texture.Width, (this[x, y].Y * (SourceSize.Y + Rise + Gutter * 2) + Rise + Gutter) / Texture.Height);
              quad.Out(verts);*/
            }

            // Draw the main tile.
						verts[0].Set(grid1[x, y].X, grid1[x, y].Y, 0, color,
							src1[x,y].X, src1[x,y].Y);
						verts[1].Set(grid2[x, y].X, grid1[x, y].Y, 0, color,
							src2[x, y].X, src1[x, y].Y);
						verts[2].Set(grid1[x, y].X, grid2[x, y].Y, 0, color,
							src1[x, y].X, src2[x, y].Y);
						verts[3].Set(grid2[x, y].X, grid2[x, y].Y, 0, color,
							src2[x, y].X, src2[x, y].Y);
						
            quad.Out(verts);
          }
        }
      }
    }
    #endregion

		#region Public Tile Function and Interaction
		/// <summary>
		/// Checks whether or not the given coordinates are a valid tile location.
		/// </summary>
		/// <param name="coords">The coordinates to check.</param>
		/// <returns>False if the given coordinates are beyond the bounds of the tile map.</returns>
		public bool IsInBounds(CoordInt coords) {
			if(coords.X < 0 || coords.X >= this.Columns
				|| coords.Y < 0 || coords.Y >= this.Rows) {
				return false;
			} else {
				return true;
			}
		}

		/// <summary>
		/// Returns the map coordinates of the tile the mouse is hovering over.
		/// The returned coordinate can be outside the bounds of the tile map.
		/// </summary>
		/// <remarks>Always returns (0, 0) if the mouse is inactive.</remarks>
		public CoordInt TileMouseIsOver {
			get {
				// TODO: Take into account scale and rotation.

				// Check if the mouse is active, and is actually hovering over the tile map.
				if(Mouse.Default.IsActive && Stage != null) {
					// Calculate the tile the mouse is over.
					int x = Mather.Floor((this.Stage.Mouse.X - (this.X - this.Origin.X)) / this.TileSize.X);
					int y = Mather.Floor((this.Stage.Mouse.Y - (this.Y - this.Origin.Y)) / this.TileSize.Y);
					return new CoordInt(x, y);
				} else {
					return new CoordInt(0);
				}
			}
		}

		/// <summary>
		/// Gets a rectangle object that encompases the area on-screen that a given tile is taking up.
		/// </summary>
		/// <remarks>The given tile can be outside the bounds of the tile map.
		/// 
		/// This method does not take rotation into account.
		/// If the tile map is rotated, the returned rectangle will not be accurate.</remarks>
		/// <param name="x">The x-coordinate in the tile map to calculate for.</param>
		/// <param name="y">The y-coordinate in the tile map to calculate for.</param>
		/// <returns></returns>
		public Rectangle TileSpace(int x, int y) {
			// TODO: Take into account scale.

			Rectangle tile = new Rectangle();
			tile.Top = this.Y - this.Origin.Y + (y * TileSize.Y);
			tile.Left = this.X - this.Origin.X + (x * TileSize.X);
			tile.Bottom = tile.Top + this.TileSize.Y;
			tile.Right = tile.Left + this.TileSize.X;

			return tile;
		}

		public Rectangle TileSpace(CoordInt coords) {
			return TileSpace(coords.X, coords.Y);
		}
		#endregion
	}
}
