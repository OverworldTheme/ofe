﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A graphic object that contains text for on-screen display.
  /// </summary>
#if DEBUG && WINDOWS
	[System.Diagnostics.DebuggerDisplay("{ Content } ({X}, {Y})")]
#endif
	public class Text : Graphic {
		#region Creation
		public Text(Stage stage, string content, Font typeface) : this(stage, content, typeface, Font.Align.Center, Color.White, 0) {	}

		public Text(Stage stage, string content, Font typeface, Font.Align alignment, Color fill, float z)
			: this(stage, content, typeface, alignment, fill, z, 0, 0) { }

		public Text(Stage stage, string content, Font typeface, Font.Align alignment, Font.VerticalAlign verticalAlignment, Color fill, float z)
			: this(stage, content, typeface, alignment, verticalAlignment, fill, z, 0, 0) { }

		public Text(Stage stage, string content, Font typeface, Font.Align alignment, Color fill, float z, float x, float y)
			: this(stage, content, typeface, alignment, Font.VerticalAlign.Center, NoScroll, fill, z, x, y, NoWidth) { }

		public Text(Stage stage, string content, Font typeface, Font.Align alignment, Font.VerticalAlign verticalAlignment, Color fill, float z, float x, float y)
			: this(stage, content, typeface, alignment, verticalAlignment, NoScroll, fill, z, x, y, NoWidth) { }

		public Text(Stage stage, string content, Font typeface, Font.Align alignment, int scrollSpeed, Color fill, float z, float x, float y, float width)
			: this(stage, content, typeface, alignment, Font.VerticalAlign.Cap, scrollSpeed, fill, z, x, y, width) { }

		public Text(Stage stage, string content, Font typeface, Font.Align alignment, Font.VerticalAlign verticalAlignment, int scrollSpeed, Color fill, float z, float x, float y, float width) {		
			if(width == NoWidth) {
				Kind = Type.Line;
			} else {
				Kind = Type.Box;
				this.Origin = new Coord(width / 2, 0);				
			}

			// Initialize a line splitter.
			lineSplitter = new LineSplitter();
			chunks = new List<TextChunk>();

			// Set everything.
      VerticalAlignment = verticalAlignment;
      Typeface = typeface;
      Fill = fill;
      Alignment = alignment;
      X = x;
      Y = y;
      Z = z;
      boxWidth = width;

			// Assign to the stage.
			SetStage(stage);

			// Set content as late as possible.
			// Otherwise it will reparse the strings everytime you change something.
			Set(content);

			// Set up scrolling.
			ScrollSpeed = scrollSpeed;
			checkScroll();
		}
		#endregion

		#region Disposal
		protected override void dispose(bool dispose) {
			if(dispose) {
				if(chunks != null) {
					chunks.Clear();
				}
			}
			
			base.dispose(dispose);
		}
		#endregion

		#region Type
		/// <summary>
		/// What kind of text object this is.
		/// </summary>
		public enum Type {
			/// <summary>
			/// Text contained in a box.
			/// </summary>
			Box,

			/// <summary>
			/// A single line of text, or one with manual line breaks.
			/// </summary>
			Line
		}

		/// <summary>
		/// What kind of text this is; a true text box or a single line of text with manual line breaks.
		/// </summary>
		public Type Kind = Type.Line;
		#endregion

		#region Content
		/// <summary>
		/// The actual text contained by this text object. To change the text, use the Set method
		/// of this object.
		/// </summary>
		public string Content {
			get {
				return content;
			}
		}
		string content = null;

		/// <summary>
		/// Changes the text in this box, and resets the scroll display as neccissary.
		/// </summary>
		/// <param name="text">The text to display.</param>
		public void Set(string text) {
			if(text != Content) {
				if(text == null) {
					text = string.Empty;
				}

				content = text;
				chunks = lineSplitter.Split(Content, Typeface, BoxWidth, LineHeight);
				lineSplitter.Align(chunks, Typeface, Alignment);
				getScrollLength();
				checkScroll();
			}
		}
		#endregion

		#region Alignment
		/// <summary>
		/// How to align the text horizontally.
		/// </summary>
		public Font.Align Alignment {
			get { return alignment; }
			set {
				alignment = value;				
				lineSplitter.Align(chunks, Typeface, Alignment);
			}
		}
		private Font.Align alignment = Font.Align.Center;

		/// <summary>
		/// How to align the text vertically.
		/// </summary>
		public Font.VerticalAlign VerticalAlignment {
			get { return verticalAlignment; }
			set {
				verticalAlignment = value;				
				lineSplitter.Align(chunks, Typeface, Alignment);
			}
		}
		private Font.VerticalAlign verticalAlignment = Font.VerticalAlign.Baseline;
		#endregion

		#region Effects
		/// <summary>
		/// The color the text will be.
		/// </summary>
		public Color Fill;

		/// <summary>
		/// The color of this text's outline.
		/// </summary>
		public Color Outline;

		/// <summary>
		/// Whether or not this text will have a shadow
		/// </summary>
		public Color Shadow;

		/// <summary>
		/// Whether or not to parse OFE tags in the text, allowing text formating.
		/// </summary>
		/// <remarks>
		/// It's often a good idea to disable tags for user-editable text.
		/// That way the text doesn't display weird if they put formating tags in
		/// without realizing what they're doing.
		/// </remarks>
		public bool EnableTags = true;
		#endregion

		#region Font
		/// <summary>
		/// The font this text will use.
		/// </summary>
		public Font Typeface {
			get { return typeface; }
			set {
				typeface = value;
				//splitLines();
			}
		}
		private Font typeface;
		#endregion

		#region Dimensions
		/// <summary>
		/// A value signifying that a text object has no box, and thus no maximum width.
		/// </summary>
		public const float NoWidth = float.MaxValue;

		/// <summary>
		/// For text boxes, this is how wide the box is. Text lines ignore this value.
		/// </summary>
		public float BoxWidth {
			get {
				return boxWidth;
			}
			set {
				if(!Mather.Approx(BoxWidth, value)) {
					boxWidth = value;
					if(boxWidth == NoWidth) {
						Kind = Type.Line;
					} else {
						Kind = Type.Box;
					}
					chunks = lineSplitter.Split(Content, Typeface, BoxWidth, LineHeight);
					lineSplitter.Align(chunks, Typeface, Alignment);
				}
			}
		}
		private float boxWidth;

		/// <summary>
		/// The vertical height of each line. Set to 0 to use the font's standard height.
		/// </summary>
		public float LineHeight {
			get {
				if(lineHeight == 0 && Typeface != null) {
					return Typeface.Height;
				} else {
					return lineHeight;
				}
			}
			set {
				lineHeight = value;
			}
		}
		private float lineHeight = 0;

		/// <summary>
		/// Returns the width (on the screen) of the text in this text object. For text boxes, if the
		/// value is higher than the width of the box, the width of the box will be returned.
		/// </summary>
		public override float Width {
			get {
				float width = Typeface.Width(this.content);
				if(width > this.BoxWidth && this.Kind == Type.Box) {
					return BoxWidth;
				} else {
					return width;
				}
			}
		}

		/// <summary>
		/// For text boxes, the number of lines it takes to display the entire content of the box.
		/// </summary>
		public int Lines {
			get {
				if(chunks.Count > 0) {
					// The last chunk will give us the line count.
					return chunks[chunks.Count - 1].Line + 1;
				} else {
					return 0;
				}
			}
		}

		/// <summary>
		/// Gets the height of the text inside this box.
		/// </summary>
		public override float Height {
			get {
				return Lines * LineHeight;
			}
		}
		#endregion

		#region Origin Point
		/// <summary>
		/// Centers the origin point of the text.
		/// </summary>
		public override void CenterOrigin() {
			if(Kind == Type.Box) {
				Origin = new Coord(BoxWidth / 2, Height / 2);
			} else {
				base.CenterOrigin();
			}
		}
		#endregion

		#region Text Scrolling
		/// <summary>
		/// How quickly text will "type itself out" onto the screen. The higher the number, the faster it
		/// will display. To display text without this effect, set it to Text.NoScroll.
		/// </summary>
		public int ScrollSpeed {
			get { return scrollSpeed; }
			set {
				scrollSpeed = value;
				getScrollLength();
			}
		}
		private int scrollSpeed = NoScroll;

		/// <summary>
		/// A value that, when assigned to ScrollSpeed, disables text scrolling.
		/// </summary>
		public const int NoScroll = int.MinValue;

		/// <summary>
		/// Whether or not this text object is still in the process of scrolling.
		/// </summary>
		public bool IsScrolling {
			get {
				if(this.scrolledTo >= scrollLength) {
					return false;
				} else {
					return true;
				}
			}
		}

		/// <summary>
		/// For scrolling text, how many characters (and beats) have been displayed thus far.
		/// </summary>
		private int scrolledTo;

		/// <summary>
		/// The total number of characters (and beats) that need to be displayed.
		/// </summary>
		private int scrollLength;

		/// <summary>
		/// Tallies the number of characters (and beats) that need to be displayed,
		/// and records them in scrollLength.
		/// </summary>
		private void getScrollLength() {
			scrollLength = 0;

			if(chunks != null) {
				foreach(TextChunk line in chunks) {
					scrollLength += line.Content.Length + Mather.Round(line.Beat * Core.SetLPS * ScrollSpeed);
				}
			}
		}

		/// <summary>
		/// Forces the text's scrolling skip to the end so that everything will be visible.
		/// </summary>
		public void FinishScroll() {
			this.scrolledTo = scrollLength;
		}

		/// <summary>
		/// Sets the speed for text scrolling, reseting the text's scroll in the process.
		/// </summary>
		/// <param name="NewSpeed"></param>
		public void SetSpeed(int NewSpeed) {
			this.ScrollSpeed = NewSpeed;
			this.checkScroll();
		}

		/// <summary>
		/// When setting text, this method is used to check in order to make sure that the scrolling begins
		/// properly and doesn't try to reach outside the text string. In addition, for non-scrolling text
		/// it makes sure that the text is "pre-scrolled" so that the whole string will be displayed.
		/// </summary>
		private void checkScroll() {
			if(this.ScrollSpeed == NoScroll) {
				this.scrolledTo = scrollLength;
			} else {
				this.scrolledTo = this.ScrollSpeed;
				if(this.scrolledTo > scrollLength) {
					this.scrolledTo = scrollLength;
				}
			}
		}
		#endregion

		#region Drawing
		public override void Draw() {
			if(this.IsActive) {
				// Set our shader as active.
				DrawCycle.SetShader(Shader);

				// Make sure a font has been specified.
				if(Typeface == null) {
					return;
				}

				// Adjust by alignment.
				float verticalAdjust = 0;
				switch(VerticalAlignment) {
					case Font.VerticalAlign.Cap:
						// Do nothing.
						break;
					case Font.VerticalAlign.Center:
						verticalAdjust = Height / 2;
						break;
					case Font.VerticalAlign.Descent:
					case Font.VerticalAlign.Baseline:
						verticalAdjust = Height;
						break;
				}

				// Pass on color settings to the font before we begin.
				Color fill = this.Fill;
				fill.A *= this.Opacity;
				if(Outline != Color.None) {
					Color outline = Outline;
					outline.A *= Opacity;
					Typeface.Outline = outline;
				} else {
					Typeface.Outline = Color.None;
				}
				if(Shadow != Color.None) {
					Color shadow = Shadow;
					shadow.A *= Opacity;
					Typeface.Shadow = shadow;
				} else {
					Typeface.Shadow = Color.None;
				}

				// Draw the text to the screen.
				int scroll = scrolledTo;
				switch(this.Kind) {
					case Type.Line:
						foreach(TextChunk chunk in chunks) {
							if(ScrollSpeed == NoScroll) {
								scroll = NoScroll;
							}
							if(chunk.Color == Color.None) {
								fill = this.Fill;
							} else {
								fill = chunk.Color;
							}
							fill.A *= this.Opacity;

							Typeface.Skew = chunk.Skew;
							Typeface.Scale = chunk.Scale * new Coord(ScaleWidth, ScaleHeight);
							Typeface.Out(new string[1] { chunk.Content },
								scroll,
								Font.Align.Left,
								Font.VerticalAlign.Cap,
								fill,
								this.X - Origin.X + chunk.X,
								this.Y - Origin.Y + chunk.Y - verticalAdjust,
								this.Z);

							// Advance the scroll by the length of this line,
							// and any beat value it contains.
							scroll -= chunk.Content.Length + Mather.Round(chunk.Beat * Core.SetLPS * ScrollSpeed);
						}
						break;
					case Type.Box:
						float x = this.X - Origin.X;
						switch(Alignment) {
							case Font.Align.Center:
								x += BoxWidth / 2;
								break;
							case Font.Align.Right:
								x += BoxWidth;
								break;
						}
						foreach(TextChunk chunk in chunks) {
							if(ScrollSpeed == NoScroll) {
								scroll = NoScroll;
							}
							if(chunk.Color == Color.None) {
								fill = this.Fill;
							} else {
								fill = chunk.Color;
							}
							fill.A *= this.Opacity;

							Typeface.Skew = chunk.Skew;
							Typeface.Scale = chunk.Scale * new Coord(ScaleWidth, ScaleHeight);
							Typeface.Out(new string[1] {chunk.Content},
								scroll,
								Font.Align.Left,
								Font.VerticalAlign.Cap,
								fill,
								x + chunk.X,
								this.Y - Origin.Y + chunk.Y - verticalAdjust,
								this.Z,
								this.BoxWidth,
								this.LineHeight,
								true);

							// Advance the scroll by the length of this line,
							// and any beat value it contains.
							scroll -= chunk.Content.Length + Mather.Round(chunk.Beat * Core.SetLPS * ScrollSpeed);
						}
						break;
				}
			}
		}
		#endregion

		#region Updating
		/// <summary>
		/// Updates this text object each frame.
		/// </summary>
		public override void Update() {
			if(this.ScrollSpeed != NoScroll) {
				this.scrolledTo += this.ScrollSpeed;
			} else {
				this.scrolledTo = scrollLength;
			}

			if(this.scrolledTo > scrollLength) {
				this.scrolledTo = scrollLength;
			}
		}
		#endregion

		#region Lines
		/// <summary>
		/// All the chunks of text in this text object.
		/// </summary>
		private List<TextChunk> chunks { get; set; }

		/// <summary>
		/// Gets the length of the given line of text.
		/// </summary>
		/// <param name="lineNumber">The zero-indexed line number to check.</param>
		/// <returns>The length of the line, in virtual pixels.
		/// If a given line does not exist, it is considered to be 0 pixels long.</returns>
		public float LengthOfLine(int lineNumber) {
			float length = 0;
			foreach(TextChunk chunk in chunks) {
				if(chunk.Line == lineNumber) {
					length += Typeface.Width(chunk.Content) * chunk.Scale.X;
				}
			}
			return length;
		}

		/// <summary>
		/// Gets the coordinate of the given character in the text,
		/// in relation to the text graphic's origin point.
		/// </summary>
		/// <param name="index">The index of the character in the Content string.</param>
		/// <returns>The left-side coordinate of the given character,
		/// in relation to the text graphic's origin point.
		/// The y coordinate will be influenced by the text's vertical alignment.</returns>
		/// <exception cref="IndexOutOfRangeException">Thrown if the given index is
		/// out of bounds of the Content string.
		/// Note, however, that the given index can exceed the length of the content string by 1.
		/// This will give you the position after the final character in the string.</exception>
		public Coord GetCharacterPosition(int index) {
			// Make sure the index is in-bounds.
			if(index < 0 || index > Content.Length) {
				throw new IndexOutOfRangeException();
			}

			// Make our variables.
			float x = 0;
			float y = 0;
			// TODO: Take scale height into account?
			TextChunk line = null;

			// If there aren't any lines at the moment, we don't need to search for anything.
			if(chunks.Count > 0) {
				// Find out what line the charcter is on.
				if(index != Content.Length) {
					foreach(TextChunk division in chunks) {
						if(index > division.Length) {
							index -= division.Length;
						} else {
							line = division;
							break;
						}
					}
				} else {
					// If it's at the end of the string, it's considered to be on the last line.
					line = chunks[chunks.Count - 1];
					index = line.Length;
				}

				// Make sure we found the line.
				if(line != null) {
					// Get the y coordinate.
					y = line.Y;

					// Get the x coordinate.
					if(index == 0) {
						x = 0;
					} else {
						x = Typeface.Width(line.Content.Substring(0, index)) * line.Scale.X;
					}
				}
			}

			// Make a return value.
			Coord coord = new Coord(x, y);

			// Shift the x-coordinate by alignment.
			switch(Alignment) {
				case Font.Align.Center:
					x -= Width / 2;
					break;
				case Font.Align.Right:
					x -= Width;
					break;
			}

			// Shift the y-coordinate by vertical alignment.
			switch(VerticalAlignment) {
				case Font.VerticalAlign.Cap:
					y -= Typeface.Cap;
					break;
				case Font.VerticalAlign.Center:
					y -= Typeface.Baseline - Typeface.Cap;
					break;
				case Font.VerticalAlign.Baseline:
					y -= Typeface.Baseline;
					break;
				case Font.VerticalAlign.Descent:
					y -= Typeface.Descent;
					break;
			}

			// Alter by the origin point.
			coord -= Origin;

			// Return the coridinates.
			return coord;
		}

		/// <summary>
		/// Returns the line number that the character of the given index is located on.
		/// </summary>
		/// <param name="index">The index of the character in the Content string.</param>
		/// <returns>The left-side coordinate of the given character,
		/// in relation to the text graphic's origin point.
		/// The y coordinate will be influenced by the text's vertical alignment.</returns>
		/// <exception cref="IndexOutOfRangeException">Thrown if the given index is
		/// out of bounds of the Content string.
		/// Note, however, that the given index can exceed the length of the content string by 1.
		/// This will give you the position after the final character in the string.</exception>
		public int GetLine(int index) {
			// Make sure the index is in-bounds.
			if(index < 0 || index > Content.Length) {
				throw new IndexOutOfRangeException();
			}			

			// Find out what line the charcter is on.
			if(index != Content.Length) {
				foreach(TextChunk division in chunks) {
					if(index > division.Length) {
						index -= division.Length;
					} else {
						return division.Line;
					}
				}
			} else {
				// If it's at the end of the string, it's considered to be on the last line.
				if(chunks.Count > 0) {
					return chunks[chunks.Count - 1].Line;
				} else {
					return 0;
				}
			}

			// Consider it at the end.
			return chunks[chunks.Count - 1].Line;
		}

		/// <summary>
		/// Returns the index of the first character in the given line.
		/// </summary>
		/// <param name="line">The line to check.</param>
		/// <returns>The character's index in the Content string.
		/// If the line does not exist, the first character in the Content string will be returned
		/// if the given line is before the first line, or the last character in the Content string
		/// will be returned if the given line is after the last line.</returns>
		public int FirstIndexOfLine(int line) {
			if(line < 0) {
				return 0;
			} else if(line >= chunks.Count) {
				if(Content.Length > 0) {
					return Content.Length - 1;
				} else {
					return 0;
				}
			}

			// Find out what line the charcter is on.
			int index = 0;
			for(int i = 0; i < line; i++) {
				index += chunks[i].Length;
			}
			return index;
		}

		/// <summary>
		/// Returns the index of the last character in the given line.
		/// </summary>
		/// <param name="line">The line to check.</param>
		/// <returns>The character's index in the Content string.
		/// If the line does not exist, the first character in the Content string will be returned
		/// if the given line is before the first line, or the last character in the Content string
		/// will be returned if the given line is after the last line.</returns>
		public int LastIndexOfLine(int line) {
			if(line < 0) {
				return 0;
			} else if(line >= chunks.Count) {
				if(Content.Length > 0) {
					return Content.Length - 1;
				} else {
					return 0;
				}
			}

			// Find out what line the charcter is on.
			int index = 0;
			for(int i = 0; i < line + 1; i++) {
				index += chunks[i].Length;
			}
			return index - 1;
		}
		#endregion

		#region Line Splitting
		/// <summary>
		/// The line splitter utility used by this text object to organize lines.
		/// </summary>
		private LineSplitter lineSplitter;
		#endregion
	}
}
