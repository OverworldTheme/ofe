﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Data;
using VisionRiders.OFE.Graphics;

#if OPENGL
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics.OpenGL;
#elif XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
#endif

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// All sprites pull their data from a "source sprite". Source sheets contain a single image
  /// that the sprite will draw from, along with data on how to animate it. Source sprites are
  /// automatically created by OFE as needed, but you can also make use of them, instancing
  /// multiple sprites from a single source.
  /// </summary>
  public class SpriteSource {
    #region Initialization
    /// <summary>
    /// Initializes OFE's sprite source management.
    /// </summary>
    internal static void Init() {
      All = new List<SpriteSource>();
    }
    #endregion

		#region Class Constructors
		/// <summary>
    /// Creates a new sprite source from the given file.
    /// </summary>
    /// <param name="fileName">The file name of the sprite data or image file to use for this source.</param>
    public SpriteSource(string fileName) : this(false, fileName) { }

		/// <summary>
		/// Create a new sprite source from the given texture.
		/// </summary>
		/// <param name="texture">The texture that will be this sprite source's sprite sheet.</param>
		public SpriteSource(Texture texture) {
			SpriteSheet = texture;

			this.managedByOFE = false;
			All.Add(this);
		}

    /// <summary>
    /// Creates a new sprite source.
    /// </summary>
    /// <param name="autoManage">Whether or not OFE should auto-manage this sprite source and its texture.</param>
    /// <param name="fileName">The file name of the sprite data or image file to use for this source.</param>
    internal SpriteSource(bool autoManage, string fileName) {
      this.Load(fileName);
      this.managedByOFE = autoManage;

      All.Add(this);
    }
    #endregion

		#region Sprite Types
		/// <summary>
    /// What kind of sprite this is; animated or static.
    /// </summary>
    public Sprite.Type Kind = Sprite.Type.Static;

		/// <summary>
		/// For grid sprites, the size of each "cell" in the grid.
		/// </summary>
		public CoordInt CellSize;
		#endregion

		#region Source Sheet
		/// <summary>
    /// The texture that holds all this sprite's graphics.
    /// </summary>
    public Texture SpriteSheet;

    /// <summary>
    /// The center coordinate (in pixels) of the sprite sheet.
    /// </summary>
    public Coord SheetCenter {
      get {
        if(SpriteSheet != null) {
          return SpriteSheet.Center;
        } else {
          return Coord.Zero;
        }
      }
    }

    /// <summary>
    /// The width of the entire sprite sheet, in pixels.
    /// </summary>
    public int Width {
      get {
        if(SpriteSheet != null) {
          return SpriteSheet.Width;
        } else {
          return 0;
        }
      }
    }

    /// <summary>
    /// The height of the entire sprite sheet, in pixels.
    /// </summary>
    public int Height {
      get {
        if(SpriteSheet != null) {
          return SpriteSheet.Height;
        } else {
          return 0;
        }
      }
    }

		/// <summary>
		/// The offset value of the sprite sheet, in pixels.
		/// It will act as the absolute top-left of the useable area of the sheet.
		/// The area above and the the left of the offset area is ignored.
		/// </summary>
		public CoordInt Offset;

    /// <summary>
    /// If true, the last file this sprite source attempted to load either didn't exist or was
    /// corrupt, and so an "invisible sprite" was substituted.
    /// </summary>
    public bool FileWasBad {
      get {
        if(SpriteSheet != null) {
          return SpriteSheet.FileWasBad;
        } else {
          return false;
        }
      }
    }
		#endregion

		/// <summary>
    /// Draws a given frame of this sprite to the screen.
    /// </summary>
    internal void Draw(string stage, string sequence, int frame, Coord displayTo, float z, Coord origin, Flip flip, float rotation, float scaleX, float scaleY, float opacity, Color hue) {
      // TODO: Implement the Integrated Sprite System for animated sprite support.
      SpriteSheet.Blit(
        new Rectangle(0 + Offset.X, 0 + Offset.Y, SpriteSheet.Width - Offset.X, SpriteSheet.Height - Offset.Y),
        displayTo,
        z,
        origin,
        flip,
        rotation,
        new Coord(scaleX, scaleY),
        opacity,
        hue);
    }

    /// <summary>
    /// Draws a given frame of this sprite to the screen.
    /// </summary>
    internal void Draw(Coord DisplayTo, float Z, Coord Origin, Flip flip, float Rotation, float ScaleX, float ScaleY, float Opacity, Color Hue) {
      Draw(null, null, 0, DisplayTo, Z, Origin, flip, Rotation, ScaleX, ScaleY, Opacity, Hue);
    }

    /// <summary>
    /// Creates a new sprite that utilizes this source sprite.
		/// DEPRECIATED.
    /// </summary>
    public Sprite Instance(Stage stage, bool activateNow, float z) {
      Sprite Return;

      if(stage != null) {
        Return = this.Instance(stage, activateNow, z, stage.Center.X, stage.Center.Y);
      } else {
        Return = this.Instance(stage, activateNow, z, Display.Center.X, Display.Center.Y);
      }

      return Return;
    }

    /// <summary>
    /// Creates a new sprite that utilizes this souce sprite.
		/// DEPRECIATED.
    /// </summary>
		public Sprite Instance(Stage stage, bool activateNow, float z, float x, float y) {
      Sprite Return = new Sprite(stage);
      Return.X = x;
      Return.Y = y;
      Return.Z = z;
      Return.SetSource(this);
      Return.CenterOrigin();

      return Return;
    }

    /// <summary>
    /// Loads a sprite from a file.
    /// </summary>
    /// <param name="File">The relative path and filename of the image file to load.</param>
    public void Load(string imageFile) {
      // Out with the old.
      if(SpriteSheet != null) {
        SpriteSheet.DetachAll(this);
      }      

      // In with the new.
      SpriteSheet = new Texture(true, imageFile);
      if(SpriteSheet != null) {
        SpriteSheet.Attach(this);
      }
    }

    /// <summary>
    /// Gets whether or not the sprite sheet has finished loading.
    /// </summary>
    public bool IsLoaded {
      get {
        if(SpriteSheet != null) {
          return SpriteSheet.IsLoaded;
        } else {
          return true;
        }
      }
    }

    #region References and Management
    /// <summary>
    /// A list of all the active sprite sources in use by the engine.
    /// </summary>
    public static List<SpriteSource> All = new List<SpriteSource>();

    /// <summary>
    /// The number of active sprite sources in use by the engine.
    /// </summary>
    public static int Total {
      get {
        return All.Count;
      }
    }

    /// <summary>
    /// The number of sprites using this source.
    /// </summary>
    public int References {
      get {
        return referenceList.Count;
      }
    }

    /// <summary>
    /// A list of all the sprites that use this source.
    /// </summary>
    private List<Sprite> referenceList = new List<Sprite>();

    /// <summary>
    /// Gets whether or not any sprites in the engine are using this source at the moment.
    /// </summary>
    public bool InUse {
      get {
        if(References > 0) {
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// Gets whether or not OFE automatically generated this sprite source for you.
    /// </summary>
    /// <remarks>
    /// OFE is helpful in creating objects such as sprites on the fly, loading the texture and sprite source
    /// on its own and putting it in a SpriteSource object for you.
    /// 
    /// But it's important to remember that when OFE does this, it will manage the object automatically on its own.
    /// If you want to use a single sprite source in many different places, this can be a problem because OFE will release the
    /// sprite source as soon as it sees that no more objects are using it, even if you plan to use that sprite source later.
    /// In this case, you should create the SpriteSource object yourself and handle it manually.
    /// </remarks>
    public bool ManagedByOFE {
      get {
        return managedByOFE;
      }
    }
    private bool managedByOFE;

    /// <summary>
    /// Returns an already-loaded sprite source of the given file, if it exists.
    /// </summary>
    /// <param name="fileName">The full path and filename, sans extension, of the sprite source.</param>
    /// <returns>Null if the file has not already been loaded, or it is not auto-managed by OFE</returns>
    internal static SpriteSource GetManagedInstanceOf(string fileName) {
			if(fileName != string.Empty && fileName != null) {
				foreach(SpriteSource source in All) {
					if(source.SpriteSheet != null && !string.IsNullOrWhiteSpace(source.SpriteSheet.FileName)
						&& Path.GetFullPath(fileName) == Path.GetFullPath(source.SpriteSheet.Directory + "//" + source.SpriteSheet.FileName)) {
						return source;
					}
				}
			}

      // Couldn't find it, sorry.
      return null;
    }

    /// <summary>
    /// Adds a sprite reference, denoting that the given object is using this source.
    /// </summary>
    /// <param name="sprite">The referenced sprite.</param>
    internal void Attach(Sprite sprite) {
      referenceList.Add(sprite);
      SpriteSheet.Attach(sprite);
    }

    /// <summary>
    /// Removes an sprite reference, denoting that the given object is no longer using this texture.
    /// </summary>
    /// <param name="sprite">The referenced sprite.</param>
    internal void Detach(Sprite sprite) {
      referenceList.Remove(sprite);
      SpriteSheet.DetachAll(sprite);

      if(ManagedByOFE && !InUse) {
        this.Die();
      }
    }
    #endregion

    #region Death
    public void Die() {
      SpriteSheet.DetachAll(this);

      All.Remove(this);
    }
    #endregion
  }
}
