﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics.OFE2D {
  public class Font : ILoadable {
		#region Class Constructor
		/// <summary>
		/// Creates a new font, using the data from the given file.
		/// </summary>
		/// <param name="filename">The relative path and filename of the font file, given without the file extension.</param>
		public Font(string filename) {
			// Initialize the character dicitonary.
			Characters = new Dictionary<char, FontCharacter>(256);

			// Load the font sprite sheet.
			this.Texture = new Texture(true, Path.ChangeExtension(filename, null));

			// Cue the data file for loading.
			IsDataFileLoaded = false;
			PathAndFilename = filename;
			Grunt.Enqueue(this);
		}
		#endregion

		#region Properties
		/// <summary>
    /// How far from the top of a character (not including padding) the baseline of the font is.
    /// </summary>
    public float Baseline;

    /// <summary>
    /// How far from the top of a character (not including padding) the capline of the font is.
    /// </summary>
    public float Cap;

    /// <summary>
    /// How far from the top of a character (not including padding) the descent of the font is.
    /// </summary>
    public float Descent;

    /// <summary>
    /// The height of this font, in pixels.
    /// </summary>
    private float height;

    /// <summary>
    /// The amount of padding, in pixels, around each side of the characters in this font.
    /// </summary>
    public int Padding;

		/// <summary>
		/// The amount of space, in pixels, to put between letters.
		/// </summary>
		public float Tracking;

    /// <summary>
    /// The Unicode index of the first character in this font.
    /// </summary>
    public int FirstIndex;

    /// <summary>
    /// All the characters in this font.
    /// </summary>
    private Dictionary<char, FontCharacter> Characters;

    /// <summary>
    /// The sprite sheet for this font.
    /// </summary>
		public Texture Texture { get; set; }
    #endregion

		#region Drawing Properties
		internal Color Outline { get; set; }
		internal Color Shadow { get; set; }

		/// <summary>
		/// The scaling to give the text.
		/// </summary>
		internal Coord Scale;

		/// <summary>
		/// The amount of skew to give chatacters, in order to mimic italics.
		/// </summary>
		internal float Skew { get; set; }
		#endregion

		#region Alignment
		/// <summary>
    /// Text alignment options.
    /// </summary>
    public enum Align {
      /// <summary>
      /// Aligns text so that the right side of the text is flush with the x-axis coordinate.
      /// </summary>
      Right,

      /// <summary>
      /// Aligns text so that the left side of the text is flush with the x-axis coordiante.
      /// </summary>
      Left,

      /// <summary>
      /// Aligns text so that it is centered horizontally on the x-axis coordinate.
      /// </summary>
      Center,

      /// <summary>
      /// For text boxes, automatically adjusts the spacing so that both sides of a paragraph are
      /// flush with the sides of the box. UNIMPLEMENTED
      /// </summary>
      Justify
    }

    /// <summary>
    /// Vertical text alignment options for text objects.
    /// </summary>
    public enum VerticalAlign {
      /// <summary>
      /// Sets the cap line (top of the letters) to be flush with the y-axis coordinate. This is the
      /// default for text boxes.
      /// </summary>
      Cap,

      /// <summary>
      /// Centers the text vertically on the y-axis coordinate.
      /// </summary>
      Center,

      /// <summary>
      /// Sets the baseline of the text to be flush with the y-axis coordinate. This is the default
      /// for single-line text objects.
      /// </summary>
      Baseline,

      /// <summary>
      /// Sets the descent of the text (the underhang of letters like "g" and "y") to be flush with
      /// the y-axis coordinate.
      /// </summary>
      Descent
    }
		#endregion

		#region Loading
		/// <summary>
		/// A lock object, used to keep OnLoaded from tripping over itself.
		/// </summary>
		private object loadLock = new object();

		/// <summary>
		/// The relative path and filename, sans extension, where the font data file can be font.
		/// </summary>
		public string PathAndFilename { get; private set; }

		/// <summary>
		/// Whether or not this font has finished loading.
		/// </summary>
		public bool IsLoaded {
			get {
				if(!IsDataFileLoaded)
					return false;
				else if(Texture == null || !Texture.IsLoaded)
					return false;
				else
					return true;
			}
		}

		/// <summary>
		/// Gets whether or not this font has finished loading its data file
		/// (but not necissarily its texture file).
		/// </summary>
		public bool IsDataFileLoaded;

		/// <summary>
		/// Event fired by Grunt when this font's data file is finished loading.
		/// </summary>
		public EventHandler OnLoaded {
			get {
				lock(loadLock) {
					return onLoaded;
				}
			}
			set {
				lock(loadLock) {
					if(IsLoaded) {
						value.Invoke(this, EventArgs.Empty);
					} else {
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;

		/// <summary>
		/// Immediatly loads this font's data,
		/// using the stored parameters.
		/// </summary>
		public void LoadNow() {
			// Load the font data.
			DataTree fontData = DataTree.LoadFrom(PathAndFilename + ".font");

			// Now, parse the data.
			foreach(DataTree root in fontData.Children) {
				if(root.NameIs("font")) {
					foreach(DataTree node in root.Children) {
						switch(node.NameToLower) {
							case "properties":
								foreach(DataTree attribute in node.Children) {
									switch(attribute.NameToLower) {
										case "baseline":
											this.Baseline = attribute.FloatValue;
											break;
										case "height":
											this.height = attribute.FloatValue;
											break;
										case "padding":
											this.Padding = attribute.IntValue;
											break;
										case "start":
											this.FirstIndex = attribute.IntValue;
											break;
										case "cap":
											this.Cap = attribute.IntValue;
											break;
										case "descent":
											this.Descent = attribute.IntValue;
											break;
										case "tracking":
											this.Tracking = attribute.FloatValue;
											break;
									}
								}
								break;
							case "character":
								FontCharacter letter = new FontCharacter();
								foreach(DataTree attribute in node.Children) {
									switch(attribute.NameToLower) {
										case "value":
										case "index":
											letter.Index = attribute.IntValue;
											break;
										case "x":
											letter.X = attribute.IntValue;
											break;
										case "y":
											letter.Y = attribute.IntValue;
											break;
										case "width":
											letter.Width = attribute.FloatValue;
											break;
										case "sourcewidth":
											letter.SourceWidth = attribute.IntValue;
											break;
									}
								}
								this.Characters.Add((char)letter.Index, letter);
								break;
						}
					}
				}
			}

			// We're loaded!
			lock(loadLock) {
				IsDataFileLoaded = true;
				if(!Texture.IsLoaded) {
					Texture.OnLoaded += this.onLoaded;
				}
			}
		}
		#endregion

		/// <summary>
    /// Draws text to the screen, formatted to fit in a box.
    /// </summary>
    /// <param name="text"></param>
    /// <param name="toDisplay">Used for a scrolling text effect. How many characters from the text
    /// string to actually display on-screen. Pass -1 to show the entire string.</param>
    /// <param name="alignment"></param>
    /// <param name="color"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="width"></param>
    /// <param name="height">The height for each line. Pass 0 to use the font's standard height.</param>
    /// <param name="z"></param>
    /// <param name="toScreen">Whether or not to actually print the given text. Pass false if you just want
    /// to check the number of lines.</param>
    /// <returns>The number of lines it took to fit the text</returns>
		internal void Out(string[] text, int toDisplay, Align alignment, VerticalAlign verticalAlignment, Color color,
			float x, float y, float z, float width, float height, bool printToScreen) {
			// Make sure we've finished loading before trying to do anything.
			if(!IsLoaded)
				return;

			if(toDisplay == Text.NoScroll) {
				toDisplay = int.MaxValue;
			}

			if(height == 0) {
				height = Height;
			}

			// Draw each line of text.
			foreach(string line in text) {
				/*if(line.Length > toDisplay) {
					string toPrint = line.Substring(0, toDisplay);
					this.lineOut(toPrint, toDisplay, alignment, verticalAlignment, color, x, y, z);
					return;
				}*/
				this.lineOut(line, toDisplay, alignment, verticalAlignment, color, x, y, z);
				y += height;
				toDisplay -= line.Length;
			}
		}

    /// <summary>
    /// Draws text to the screen.
    /// </summary>
    internal void Out(string[] text, int toDisplay, Align alignment, VerticalAlign verticalAlignment, Color color, float x, float y, float z) {
			Out(text, toDisplay, alignment, verticalAlignment, color, x, y, z, float.PositiveInfinity, 0, true);
    }

    /// <summary>
    /// Draws text to the screen.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="Alignment"></param>
    /// <param name="Color"></param>
    /// <param name="X"></param>
    /// <param name="Y"></param>
    /// <param name="Z"></param>
    private void lineOut(string text, int toDisplay, Align alignment, VerticalAlign verticalAlignment, Color color, float x, float y, float z) {
			// Make sure we've finished loading before trying to do anything.
			if(!IsLoaded)
				return;

			// Make sure there's something to display.
			if(string.IsNullOrEmpty(text) || toDisplay <= 0) {
				return;
			}

			// Cut the string down to what's supposed to be displayed.
			if(text.Length > toDisplay) {
				text = text.Substring(0, toDisplay);
			}

      // Check alignment.
      switch(alignment) {
        case Align.Center:
          x -= Width(text) / 2;
          break;
        case Align.Right:
          x -= Width(text);
          break;
      }

      // Make sure the text is correctly aligned vertically.
      switch(verticalAlignment) {
        case VerticalAlign.Cap:
          // Do nothing.
          break;
        case VerticalAlign.Center:
          y -= Height / 2;
          break;
        case VerticalAlign.Baseline:
          y -= Baseline;
          break;
      }

      // Round the numbers to ensure the text isn't blurred.
      x = (float)Math.Round(x);
      y = (float)Math.Round(y);

      // Now draw the actual text.
      if(Shadow != Color.None) {
        drawString(text, Shadow, x + 3 * Display.Scale.X, y + 3 * Display.Scale.Y, z + 0.000011f);
      }
      if(Outline != Color.None) {
        drawString(text, Outline, x + Display.Scale.X, y, z + 0.00001f);
        drawString(text, Outline, x - Display.Scale.X, y, z + 0.00001f);
        drawString(text, Outline, x, y + Display.Scale.Y, z + 0.00001f);
        drawString(text, Outline, x, y - Display.Scale.Y, z + 0.00001f);
      }
      drawString(text, color, x, y, z);
    }

    /// <summary>
    /// Measures the width of a string of text.
    /// </summary>
    /// <param name="text">The text string to measure.</param>
    /// <returns>0 if the data file has not finished loading.</returns>
    public float Width(string text) {
			// Make sure we've finished loading before trying to do anything.
			if(!IsDataFileLoaded)
				return 0;

      float width = 0;
			FontCharacter l = new FontCharacter();
      foreach(char letter in text) {
				// Try to grab the character.
				bool exists = Characters.TryGetValue(letter, out l);

        // Make sure the character exists in this font. If not, skip it.
        if(!exists) {
          width += 0;
        } else {
          width += (l.Width + Tracking);
        }
      }
      return width;
    }

    /// <summary>
    /// Gets the height of this font.
    /// </summary>
    public float Height {
			get {
				return height;
			}
    }

    /// <summary>
    /// Internal method used by OFE to do the leg work of drawing text to the target.
    /// </summary>
    /// <param name="text"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="color"></param>
    private void drawString(string text, Color color, float x, float y, float z) {
			// Make sure we've finished loading before trying to do anything.
			if(!IsLoaded)
				return;

			// Set up our variables.
			FontCharacter letter;

      // Go through each character in the string and blit it.
      foreach(char character in text) {
				// Try to grab the character.
				bool exists = Characters.TryGetValue(character, out letter);

        // Make sure the character exists. If not, skip it.        
        if(!exists) {
          x += 0;
        } else {          
					// Calculate width and height.
					float width = letter.SourceWidth;
					float height = Height + Padding * 2;

          // Copy the character from the texture.
          Texture.Blit(
						new Coord(letter.X, letter.Y),
						new Coord(letter.X + width, letter.Y),
						new Coord(letter.X, letter.Y + height),
						new Coord(letter.X + width, letter.Y + height),
            new Coord(x - Padding + (width * (Skew / 2)), y - Padding),
						new Coord(x - Padding + (width * (Skew / 2)) + width, y - Padding),
						new Coord(x - Padding - (width * (Skew / 2)), y - Padding + height),
						new Coord(x - Padding - (width * (Skew / 2)) + width, y - Padding + height),
            z,
            new Coord(0, 0),
            Flip.None,
            0,
            Scale,
            1,
            color);

          // Advance the x value in preperation for the next character.
          x += (letter.Width + Tracking) * Scale.X;
        }
      }
    }
  }
}
