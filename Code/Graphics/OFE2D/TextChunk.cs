﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
	/// <summary>
	/// A single component of a larger text object.
	/// </summary>
#if DEBUG
	[System.Diagnostics.DebuggerDisplay("{ Content } ({X}, {Y})")]
#endif
	internal class TextChunk {
		#region Creation
		public TextChunk(string content, int line, Color color, float x, float y) {
			Content = content;
			Color = color;
			Line = line;
			Location = new Coord(x, y);
			Scale = Coord.One;
		}
		#endregion

		#region Content
		/// <summary>
		/// This chunk's displayed content.
		/// </summary>
		public string Content { get; set; }

		/// <summary>
		/// The length (in characters) of this line's text.
		/// </summary>
		public int Length {
			get { return Content.Length; }
		}

		/// <summary>
		/// The number of seconds to pause before displaying this chunk with scrolling text.
		/// </summary>
		public float Beat { get; set; }
		#endregion

		#region Display
		/// <summary>
		/// The color of this text chunk.
		/// </summary>
		public Color Color { get; set; }

		/// <summary>
		/// What line (from the top) in the larger text object this chunk is considered a part of.
		/// </summary>
		public int Line { get; set; }
		#endregion

		#region Orientation
		/// <summary>
		/// This text chunk's location, in relation to the main text object it is a part of.
		/// </summary>
		public Coord Location;

		/// <summary>
		/// This text chunk's scale.
		/// </summary>
		public Coord Scale;

		/// <summary>
		/// The x coordinate of this text chunk, in relation to the main text object it is a part of.
		/// </summary>
		public float X {
			get { return Location.X; }
			set { Location.X = value; }
		}

		/// <summary>
		/// The y coordinate of this text chunk, in relation to the main text object it is a part of.
		/// </summary>
		public float Y {
			get { return Location.Y; }
			set { Location.Y = value; }
		}
		#endregion

		#region Properties
		/// <summary>
		/// The amount of skew this text chunk has, to emulate italics.
		/// </summary>
		public float Skew { get; set; }
		#endregion
	}
}
