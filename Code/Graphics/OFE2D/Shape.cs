﻿using System;
using System.Collections.Generic;
using System.Linq;

#if XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
#endif

using VisionRiders.OFE.Graphics.OFE3D;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A simple geometric shape in 2d space.
  /// </summary>
  public class Shape : Graphic {
#if XNA
    public static Texture2D xPixel;
#endif

    #region Shape Types
    /// <summary>
    /// The different kinds of shapes that OFE knows how to draw.
    /// </summary>
    public enum Kind {
      /// <summary>
      /// A rectangle.
      /// </summary>
      Box,

      /// <summary>
      /// A circle.
      /// </summary>
      Circle,

      /// <summary>
      /// A line segment.
      /// </summary>
      Line,

      /// <summary>
      /// A triangle.
      /// </summary>
      Triangle
    }
    #endregion

    #region Size and Position
    /// <summary>
    /// The width of this shape.
    /// </summary>
    public override float Width {
      get {
        return width;
      }
      set {
        width = value;
      }
    }
    public float width;

    /// <summary>
    /// The height of this shape.
    /// </summary>
    public override float Height {
      get {
        return height;
      }
      set {
        height = value;
      }
    }
    private float height;

    public override Coord Origin {
      get {
        return origin;
      }
      set {
        origin = value;
      }
    }
    private Coord origin;
    #endregion

    /// <summary>
    /// The color of this shape's fill, if any.
    /// </summary>
    public Nullable<Color> Fill;

    /// <summary>
    /// The color of this shape's outline, if any.
    /// </summary>
    public Nullable<Color> Outline;

    /// <summary>
    /// What kind of primitive object this is.
    /// </summary>
    public Kind Type;

    /// <summary>
    /// How thick this primitive's outline is.
    /// </summary>
    public float Thickness = 1.0f;

		/// <summary>
		/// Creates a new, undefined shape.
		/// </summary>
		/// <param name="stage">The stage this shape will be a part of.</param>
    protected Shape(Stage stage) {
			SetStage(stage);
    }

    /// <summary>
    /// Sets up everything needed to draw Primitives.
    /// </summary>
    internal static void Init() {
#if XNA
      xPixel = new Texture2D(Display.xnaDevice, 2, 2);
      Microsoft.Xna.Framework.Color[] Fill = new Microsoft.Xna.Framework.Color[] { 
        Microsoft.Xna.Framework.Color.White,
        Microsoft.Xna.Framework.Color.White,
        Microsoft.Xna.Framework.Color.White,
        Microsoft.Xna.Framework.Color.White
      };
      xPixel.SetData(Fill);
#endif
    }

    /// <summary>
    /// Creates a new box shape.
    /// </summary>
    /// <returns>The shape object.</returns>
    public static Shape Box(Stage stage, float thickness, Nullable<Color> fill, Nullable<Color> outline, float opacity,
			float z, float x1, float y1, float x2, float y2) {
      Shape box = new Shape(stage);

      box.Width = Mather.Distance(x1, x2);
      box.Height = Mather.Distance(y1, y2);
      box.origin = new Coord(box.Width /2, box.Height / 2);
      box.X = Mather.SmallestOf(x1, x2) + box.Origin.X;
      box.Y = Mather.SmallestOf(y1, y2) + box.Origin.Y;
      box.Z = z;
      box.Thickness = thickness;
      if(fill != null) {
        box.Fill = (Color)fill;
      }
      if(outline != null) {
        box.Outline = (Color)outline;
      }
      box.Opacity = opacity;

      return box;
    }
		
    public override void Draw() {
			// Make sure we're active.
			if(this.IsActive && this.IsVisible) {
				// Set our shader as active.
				DrawCycle.SetShader(Shader);

				// Set the colors and opacity.
				Color fill = new Color();
				if(this.Fill != null) {
					fill = (Color)Fill;
					fill.A = Opacity;
				}

				// Draw the shape.
				switch(this.Type) {
					case Kind.Box:
						// Set up the vertices.
						Vertex[] verts = new Vertex[4] {
            new Vertex(-Origin.X, -Origin.Y, 0, fill),
            new Vertex(Width - Origin.X, -Origin.Y, 0, fill),
            new Vertex(Width - Origin.X, Height - Origin.Y, 0, fill),
            new Vertex(-Origin.X, Height - Origin.Y, 0, fill)};

						DrawCycle.SetTexture(null);
						DrawCycle.World.Multiply(Matrix.CreateScale(ScaleWidth, ScaleHeight, 1),
							Matrix.Create2dRotation(Rotation),
							Matrix.CreateTranslation(X, Y, Z));
						new Quad(0, 1, 2, 3).Out(verts);
						/*if(this.Thickness > 0 && this.Outline != null) {
							box(First.X + X + Stage.Origin.X, First.Y + Y + Stage.Origin.Y,
								Second.X + X + Stage.Origin.X, Second.Y + Y + Stage.Origin.Y,
								Z - 0.0001f, Thickness, (Color)Outline);
						}*/
						break;
					case Kind.Circle:
						break;
					case Kind.Line:
						break;
					case Kind.Triangle:
						break;
				}
			}
    }

    /// <summary>
    /// Draws the outline of a box onto the screen.
    /// </summary>
    private static void box(float X1, float Y1, float X2, float Y2, float Z, float Thickness, Color Color) {
      if(!Display.IsOnScreen((int)(X1 - Thickness), (int)(Y1 - Thickness), (int)(X2 - Thickness), (int)(Y2 - Thickness))) {
        return; // There's no reason to bother drawing what we can't see.
      }

      Shape.line(X1, Y1, X1, Y2, Z, Thickness, Color);
      Shape.line(X1, Y2, X2, Y2, Z, Thickness, Color);
      Shape.line(X2, Y2, X2, Y1, Z, Thickness, Color);
      Shape.line(X2, Y1, X1, Y1, Z, Thickness, Color);
    }

    /// <summary>
    /// Draws a box filled with the given color onto the screen.
    /// </summary>
    private static void filledBox(float fX1, float fY1, float fX2, float fY2, float fZ, Color zColor, float fOpacity) {
      if(!Display.IsOnScreen((int)fX1, (int)fY1, (int)fX2, (int)fY2)) {
        return; // There's no reason to bother drawing what we can't see.
      }

#if XNA
      Microsoft.Xna.Framework.Color xColor = zColor.xnaColor;
      xColor.A = (byte)(255 * fOpacity);
      /*Display.xnaSpriteBatch.Draw(xPixel,
          new Microsoft.Xna.Framework.Rectangle((int)fX1, (int)fY1, (int)(fX2 - fX1), (int)(fY2 - fY1)),
          new Microsoft.Xna.Framework.Rectangle(0, 0, 1, 1),
          xColor,
          0.0f,
          new Vector2(0, 0),
          SpriteEffects.None,
          fZ);*/
#endif
    }

    /// <summary>
    /// Draws a simple line on the screen.
    /// </summary>
    private static void line(float FromX, float FromY, float ToX, float ToY, float Z, float Thickness, Color Color, float Opacity) {
#if XNA
      float Length = (float)Math.Sqrt(Math.Pow((FromX - ToX), 2) + Math.Pow((FromY - ToY), 2)) + 2;
      float DifferenceX = ToX - FromX;
      float DifferenceY = ToY - FromY;
      float Angle = (float)Math.Atan2(DifferenceY, DifferenceX);
      Microsoft.Xna.Framework.Color xnaColor = Color.xnaColor;
      xnaColor.A = (byte)(255 * Opacity);

      FromX = (float)Math.Round(FromX);
      FromY = (float)Math.Round(FromY);
      ToX = (float)Math.Round(ToX);
      ToY = (float)Math.Round(ToY);

      // Center the coordinates.
      FromX += Thickness / 2;
      ToX += Thickness / 2;
      FromY += Thickness / 2;
      ToY += Thickness / 2;

      /*Display.xnaSpriteBatch.Draw(xPixel,
          new Microsoft.Xna.Framework.Rectangle((int)FromX, (int)FromY, (int)Length, (int)Thickness),
          new Microsoft.Xna.Framework.Rectangle(0, 0, 1, 1),
          xnaColor,
          Angle,
          new Vector2(0, 0),
          SpriteEffects.None,
          Z);*/
#endif
    }

    /// <summary>
    /// Line Overload
    /// </summary>
    private static void line(float fFromX, float fFromY, float fToX, float fToY, float fZ, float fThickness, Color zColor) {
      Shape.line(fFromX, fFromY, fToX, fToY, fZ, fThickness, zColor, 1.0f);
    }
  }
}
