﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// The data for a single character within a font.
  /// </summary>
  internal struct FontCharacter {
    /// <summary>
    /// This character's unicode index.
    /// </summary>
    public int Index;

    /// <summary>
    /// The x-coordinate of this character on the sprite sheet.
    /// </summary>
    public int X;

    /// <summary>
    /// The y-coordinate of this character on the sprite sheet.
    /// </summary>
    public int Y;

    /// <summary>
    /// The width of this character, not including padding.
    /// </summary>
    public float Width;

    /// <summary>
    /// The width of this character, including padding.
    /// </summary>
    public int SourceWidth;

    public FontCharacter(int index, int x, int y, float width, int sourceWidth) {
      this.Index = index;
      this.X = x;
      this.Y = y;
      this.Width = width;
      this.SourceWidth = sourceWidth;
    }
  }
}
