﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A 2d object that is displayed on the screen.
  /// </summary>
  public class Graphic : IDisposable {
    /// <summary>
    /// Whether or not this graphic should be updates and displayed.
    /// </summary>
    public bool IsActive = true;

		/// <summary>
		/// Whether or not this graphic is visible.
		/// Graphics that are not visible are not displayed, but will
		/// otherwise be updated as normal.
		/// </summary>
		public bool IsVisible = true;

    /// <summary>
    /// Gets the graphic group that this sprite belongs to.
    /// </summary>
		public Stage Stage { get; protected set; }

		/// <summary>
		/// Changes the stage this graphic is part of.
		/// </summary>
		/// <param name="stage">The stage to add this graphic to.</param>
		public void SetStage(Stage stage) {
			if(Stage != null) {
				Stage.Remove(this);
			}

			if(stage != null) {
				Stage = stage;
				stage.Add(this);
			} else {
				Stage = null;
			}
		}

    /// <summary>
    /// This graphic's location on the screen, in relation to its parent if it has one.
    /// </summary>
    public Coord Position;

		/// <summary>
		/// Gets this graphic's location on the screen.
		/// </summary>
		public Coord AbsolutePosition {
			get {
				if(Parentless) {
					return Position;
				} else {
					return Parent.AbsolutePosition + Position;
				}
			}
		}

    /// <summary>
    /// This graphics's location on the horizontal x axis.
    /// </summary>
    public float X {
      get {
        return Position.X;
      }
      set {
        Position.X = value;
      }
    }

    /// <summary>
    /// This graphic's location on the vertical y axis.
    /// </summary>
    public float Y {
      get {
        return Position.Y;
      }
      set {
        Position.Y = value;
      }
    }

    /// <summary>
    /// A value from one to zero that indicates this graphics's z depth, id est how far behind or in
    /// front it will be displayed on the screen. 0 is the "top", and will be displayed above everything
    /// else, while 1 is the "bottom".
		/// Values outside this range will not be display at all.
    /// </summary>
    public float Z {
      get {
				return z;
      }
      set {
        if(!Mather.Approx(z, value)) {
          z = value;
          if(Stage != null) {
						Stage.NeedsSorting = true;
          }
        }
      }
    }
    private float z;

		/// <summary>
		/// The actual z-depth of this graphic, taking into acount this graphic's parentage.
		/// </summary>
		public float AbsoluteZ {
			get {
				if(Parentless) {
					return Z;
				} else {
					return Parent.Z + Z;
				}
			}
		}

    /// <summary>
    /// How opaque this graphic is. At 1 it's completely opaque, at 0 it's completely transparent.
    /// </summary>
    public float Opacity {
      get {
        return opacity;
      }
      set {
        opacity = Mather.Clamp(value, 0, 1);
      }
    }
    private float opacity = 1;

		#region Rotation
		/// <summary>
    /// The angle, in revolutions, that this graphic will display at. 0 is upright. Increasing the number rotates
    /// the graphic clockwise, while decreasing it rotates it counterclockwise. At .5 or -.5 the graphic
    /// is completely upsidedown.
    /// </summary>
    public float Rotation = 0;
		#endregion

		#region Reflection
		/// <summary>
		/// The way that this graphic should be mirrored.
		/// </summary>
		public Flip Flip = Flip.None;
		#endregion

		#region Scale
		/// <summary>
    /// The scale to display this graphic at. 1 is its original scale. Increasing the number will make the
    /// graphic bigger, while decreasing will make it smaller. At zero the graphic will be virtually invisible.
    /// </summary>
    public float Scale {
      set {
        ScaleHeight = value;
        ScaleWidth = value;
      }
    }

    /// <summary>
    /// The height this graphic is scaled to.
    /// </summary>
    public float ScaleHeight = 1;

    /// <summary>
    /// The width this graphic is scaled to.
    /// </summary>
    public float ScaleWidth = 1;
		#endregion

		#region Dimensions
		/// <summary>
		/// The width, in pixels, of this graphic.
		/// </summary>
		public virtual float Width { get; set; }

		/// <summary>
		/// The height, in pixels, of this graphic.
		/// </summary>
		public virtual float Height { get; set; }
		#endregion

		#region Origin
		/// <summary>
    /// The origin point of this graphic. The sprite will scale and rotate in relation to this point.
    /// </summary>
    public virtual Coord Origin { get; set; }

		/// <summary>
		/// Centers the origin point of this graphic on the given coordinate.
		/// </summary>
		/// <remarks>
		/// Attempting to set a graphic's origin point based on its sides or dimensions before it
		/// has finished loading will accomplish nothing, because these variables are not
		/// known until after the texture is actually available in memory.
		/// </remarks>
		/// <param name="x">Where on the x-axis to center the origin point.</param>
		/// <param name="y">Where on the y-axis to center the origin point.</param>
		public void SetOrigin(float x, float y) {
			// Set the origin point.
			this.Origin = new Coord(x, y);
		}

		/// <summary>
		/// Centers the origin point of this graphic.
		/// </summary>
		public virtual void CenterOrigin() {
			this.Origin = new Coord(Width / 2, Height / 2);
		}
		#endregion

		#region Parent/Child Relationships
		/// <summary>
		/// This graphics's parent.
		/// Any changes made to the parent's position, orientation, or
		/// scale will affect this graphic.
		/// </summary>
		public Graphic Parent {
			get {
				return parent;
			}
		}
		private Graphic parent;

		/// <summary>
		/// Whether or not this graphic has a parent.
		/// </summary>
		public bool Parentless {
			get {
				if(parent == null) {
					return true;
				} else {
					return false;
				}
			}
		}

		/// <summary>
		/// All the children of this graphic.
		/// </summary>
		private List<Graphic> childList = new List<Graphic>();

		/// <summary>
		/// Whether or not this graphic has any children.
		/// </summary>
		public bool Childless {
			get {
				if(ChildCount > 0) {
					return false;
				} else {
					return true;
				}
			}
		}

		/// <summary>
		/// The number of children this graphic has.
		/// </summary>
		public int ChildCount {
			get {
				return childList.Count;
			}
		}

		/// <summary>
		/// Adds a child to this graphic.
		/// </summary>
		/// <remarks>If the graphic being added already has a parent, it will be released from that
		/// parent before being added as a child of this graphic.</remarks>
		/// <param name="child">The graphic to add as a child to this one.</param>
		/// <exception cref="ArgumentException">Thrown if you try to add this graphic as its own
		/// child, if you try to add this graphic's parent as a child, or any other ancestor of this
		/// graphic as a child.</exception>
		public void AddChild(Graphic child) {
			// The timey-wimey ball is not in play;
			// you can't be your own parent.
			if(child == this) {
				throw new ArgumentException("You cannot create recursive parentage.");
			}

			// And your parents and grandparents cannot be your children!
			if(child.RootParent == this.RootParent && RootParent != null) {
				throw new ArgumentException("You cannot create recursive parentage.");
			}

			if(!child.Parentless) {
				child.parent.ReleaseChild(child);
			}

			child.parent = this;
			childList.Add(child);
		}

		/// <summary>
		/// Breaks the link between this graphic and one of its children. The child graphic will no longer
		/// have this graphic as its parent.
		/// </summary>
		/// <remarks>If the graphic given is not this graphic's child, the method will not sever the link
		/// between the graphic and its actual parent, if any.</remarks>
		/// <param name="child">The graphic to release.</param>
		public void ReleaseChild(Graphic child) {
			if(child.parent == this) {
				child.parent = null;
			}

			childList.Remove(child);
		}

		/// <summary>
		/// Breaks the link between this graphic and all its children. The child graphics will no longer
		/// have this graphic as their parent.
		/// </summary>
		public void ReleaseChildren() {
			foreach(Graphic child in childList) {
				child.parent = null;
			}
			childList.Clear();
		}

		/// <summary>
		/// The root parent of this graphic.
		/// </summary>
		public Graphic RootParent {
			get {
				// Do a quick search.
				Graphic rabbitHole = this;
				while(rabbitHole.parent != null) {
					rabbitHole = rabbitHole.parent;
				}

				if(rabbitHole != this) {
					return rabbitHole;
				} else {
					return null;
				}
			}
		}
		//protected Graphic rootParent;
		#endregion

		/// <summary>
    /// All specific graphic objects have a unique draw routine that overrides this one. This empty
    /// function is provided so that routines handling generic graphic objects can call the overrides.
    /// </summary>
    public virtual void Draw() {
    }

    /// <summary>
    /// Graphic objects can have unique update routines that will be called each frame.
    /// </summary>
    public virtual void Update() {
			// Update all this graphic's children.
			foreach(Graphic child in childList) {
				child.Update();
			}
    }

		#region Death
		/// <summary>
		/// Whether or not this graphic has already been disposed.
		/// </summary>
		protected bool disposed = false;

		/// <summary>
    /// Disposes this graphic and removes it from its group.
    /// </summary>
    public void Dispose() {
			if(!disposed) {
				dispose(true);
			}
		}

		/// <summary>
		/// Disposes this graphic.
		/// </summary>
		/// <param name="dispose"></param>
		protected virtual void dispose(bool dispose) {
			if(dispose) {
				ReleaseChildren();

				if(this.Stage != null) {
					this.Stage.Remove(this);
				}
			}

			GC.SuppressFinalize(this);
    }

		~Graphic() {
			if(!disposed) {
				dispose(false);
			}
		}
		#endregion

		/// <summary>
    /// Moves this graphic to a given coordinate.
    /// </summary>
    /// <param name="x">The x coordinate to relocate this graphic to.</param>
    /// <param name="y">The y coordinate to relocate this graphic to.</param>
    public void PutAt(float x, float y) {
      this.X = x;
      this.Y = y;
    }

    /// <summary>
    /// Moves this graphic to a given coordinate.
    /// </summary>
    /// <param name="coordinate">The location to relocate this graphic to.</param>
    public void PutAt(Coord coordinate) {
      Position = coordinate;
		}

		#region Shaders
		/// <summary>
		/// The shader program to use for this graphic object.
		/// Just attach a shader program, and it will be utilized whenever this object is drawn.
		/// </summary>
		public ShaderProgram Shader { get; set; }
		#endregion
	}
}
