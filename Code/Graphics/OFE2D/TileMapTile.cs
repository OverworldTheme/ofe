﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
	/// <summary>
	/// A single tile within a tile map.
	/// </summary>
	public class TileMapTile {
		/// <summary>
		/// The tile from the source texture that will be displayed here.
		/// </summary>
		public CoordInt Tile;

		/// <summary>
		/// Which way to flip this tile, if any.
		/// </summary>
		public Flip Flip;

		/// <summary>
		/// Each tile in a map can have its own relative z-depth.
		/// </summary>
		public float Z;

		/// <summary>
		/// Copy another tile's values to this one.
		/// The old values will be overwritten.
		/// </summary>
		/// <param name="tile">The tile to copy.</param>
		public void Copy(TileMapTile tile)
		{
			Tile = tile.Tile;
			Flip = tile.Flip;
			Z = tile.Z;
		}
	}
}
