﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A "skinned box", one that can be given a custom graphical appearance based on a texture.
  /// </summary>
  /// <remarks>
  /// Skinned boxes take their appearance from a texture, and are defined by three rectangles:
  /// one that defines the parts of the texture that will be stretched to fit the given width
  /// and height on screen, a second that defines the box's reported borders, and a third that
  /// defines where content can go in this box. In addition, there is an optional fourth
  /// rectangle that can define where a seperate background texture should go in relation to
  /// the rest of the box (this allows for tiled backgrounds).
  /// 
  /// These values can be stored in a data file to be loaded when the box is initialized.
  /// </remarks>
  public class SkinBox : Graphic, ILoadable {
    #region Constructors
    /// <summary>
    /// Class constructor that takes a skinned box's data from a file.
    /// </summary>
    /// <param name="fileName">The name (and relative location) of a file where the data
    /// for this box can be found. The data file should include the name and location of
    /// the texture file.</param>
		public SkinBox(Stage stage, string fileName, float width, float height, float z, float x, float y) {
			loaded = false;
			pathAndfileName = fileName;
			onLoadedLock = new object();

			// Set graphics.
			Z = z;
			X = x;
			Y = y;
			Width = width;
			Height = height;
			Origin = new Coord(Width / 2, Height / 2);
			BorderHue = Color.White;
			BackgroundHue = Color.White;

			// Set the stage.
			SetStage(stage);

			// Load.
			LoadNow();
			//Grunt.Enqueue(this);
		}
		#endregion

		#region Loading
		/// <summary>
		/// The file name that this skinned box has beed or will be loaded from.
		/// </summary>
		public string PathAndFilename {
			get {
				return pathAndfileName;
			}
		}
		private string pathAndfileName;

		/// <summary>
		/// Whether or not this skinned box's data has finished loading and being parsed.
		/// </summary>
		public bool IsLoaded {
			get {
				return loaded;
			}
		}
		private bool loaded;

		/// <summary>
		/// Loads the data for this skinned box from a file.
		/// </summary>
		/// <param name="fileName">The name (and relative location) of the file where the data
		/// for this box can be found. The data file should include the name and location of
		/// the texture file.</param>
		private void loadFrom(string fileName) {
			// Load the data.
			DataTree data = DataTree.LoadFrom(fileName);

			// We'll need this later when we start parsing the dimensions.
			int[] list;

			// Let's start parsing the data.
			foreach(DataTree node in data.Children) {
				switch(node.Name.ToLowerInvariant()) {
					case "border":
						Border = Texture.GetManagedInstanceOf(Path.GetDirectoryName(fileName) + "//" + node.Value);
						if(Border == null) {
							Border = new Texture(true, Path.GetDirectoryName(fileName) + "//" + node.Value);
						}
						break;
					case "background":
						Background = Texture.GetManagedInstanceOf(Path.GetDirectoryName(fileName) + "//" + node.Value);
						if(Background == null) {
							Background = new Texture(true, Path.GetDirectoryName(fileName) + "//" + node.Value);
						}
						break;
					case "dim":
					case "dimensions":
						foreach(DataTree child in node.Children) {
							switch(child.NameToLower) {
								case "sides":
									list = child.ValueToIntList().ToArray();
									if(list.Length >= 4) {
										DisplayArea = new Rectangle(list[0], list[1], list[2], list[3]);
									}
									break;
								case "stretch":
									list = child.ValueToIntList().ToArray();
									if(list.Length >= 4) {
										StretchArea = new Rectangle(list[0], list[1], list[2], list[3]);
									}
									break;
								case "background":
									list = child.ValueToIntList().ToArray();
									if(list.Length >= 4) {
										BackgroundArea = new Rectangle(list[0], list[1], list[2], list[3]);
									}
									break;
							}
						}
						break;
				}
			}

			// We're done!
			loaded = true;
		}
		
		/// <summary>
		/// Immediantly loads this skinned box's data in the current thread.
		/// </summary>
		public void LoadNow() {
			loadFrom(PathAndFilename);
		}

		/// <summary>
		/// A lock used when checking or firing the OnLoaded event.
		/// </summary>
		private object onLoadedLock;

		/// <summary>
		/// Event fired by Grunt when this skinned box is finished loading.
		/// </summary>
		public EventHandler OnLoaded {
			get {
				lock(onLoadedLock) {
					return onLoaded;
				}
			}
			set {
				lock(onLoadedLock) {
					if(IsLoaded) {
						value.Invoke(this, EventArgs.Empty);
					} else {
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;
    #endregion

    #region Textures
    /// <summary>
    /// The border texture for this skinned box's graphics.
    /// </summary>
    public Texture Border { get; set; }

    /// <summary>
    /// The background texture for this box, if any.
    /// </summary>
    public Texture Background { get; set; }
    #endregion

		#region Properties
		/// <summary>
		/// The hue to give this skinned box's border.
		/// </summary>
		public Color BorderHue;

		/// <summary>
		/// The hue to give this skinned box's background.
		/// </summary>
		public Color BackgroundHue;
		#endregion

		#region Rectangle Definitions
		/// <summary>
    /// The rectangular area on this box's texture where the background graphic needs to go.
    /// </summary>
    /// <remarks>
    /// If no value is provided, this rectangle will be set to the same size as the full
    /// texture. If you are using a background texture, this is generally unwanted, so
    /// remember to specify where the background needs to go. If there is no background,
    /// this variable has no meaning and can be ignored.
    /// 
    /// It is also important to note that this background area must completely encompass
    /// the StretchArea. If any of its corners lie within the StretchArea, it won't be
    /// able to calculate where to put the background, and so the background will not be
    /// displayed.
    /// </remarks>
    public Rectangle BackgroundArea { get; set; }

    /// <summary>
    /// The rectangular area on this box's texture where content, such as text, can exist
    /// within this box. Widgets use this property on their own, but if you are using your
    /// own skinned boxes elsewhere, you're free to use it as you have need to arrange
    /// whatever you plan to put in it.
    /// </summary>
    /// <remarks>
    /// If no value is provided, this rectangle will be set to the same value as DisplayArea.
    /// </remarks>
    public Rectangle ContentArea { get; set; }

    /// <summary>
    /// The rectangular area on the texture that defines the borders of this box. On screen,
    /// this rectangular area is sized based on the box's width and height. Everything outside
    /// this area will actually be larger than the set and reported width and height.
    /// </summary>
    /// <remarks>
    /// If no value is provided, this rectangle will be set to the same value as StretchArea.
    /// </remarks>
    public Rectangle DisplayArea { get; set; }

    /// <summary>
    /// The texture of a skinned box is stretched to fit a given area onscreen. This rectangle
    /// defines the area that will be stretched. Only the area beyond the corners will be remain
    /// the same as normal regardless of size.
    /// </summary>
    /// <remarks>
    /// The width and height of a skinned box cannot be sized so small that the corners defined
    /// by this StretchArea begin to overlap each other. This can happen if the width or height
    /// of the StretchArea is smaller than the DisplayArea. In the case that this occurs, the
    /// width or height of the box will be increased so that the corners of the stretch area
    /// set flush with each other on the offending axis (or axes, if both are too small).
    /// </remarks>
    public Rectangle StretchArea { get; set; }
    #endregion

    #region Display
    public override void Draw() {
			// Are we even active and visible?
			if(!IsActive || !IsVisible)
				return;

			// Make sure the box texture and data has been loaded.
			// Otherwise, we can't do anything this frame.
			if(!IsLoaded) {
				return;
			}

			// Make our shader the active shader.
			DrawCycle.SetShader(Shader);

      // First we do the background, if there is one.
      if(Background != null) {
        Background.Blit(new Rectangle(0, 0, Background.Width, Background.Height),
          new Rectangle(X - Origin.X + (BackgroundArea.Left - DisplayArea.Left),
            Y - Origin.Y + (BackgroundArea.Top - DisplayArea.Top),
						X - Origin.X + Width + (BackgroundArea.Right - DisplayArea.Right),
						Y - Origin.Y + Height + (BackgroundArea.Bottom - DisplayArea.Bottom)),
          Z,
          Origin,
          Flip.None,
          Rotation,
          new Coord(ScaleWidth, ScaleHeight),
          Opacity,
          BackgroundHue);
      }

      // Now the main box.
      if(Border != null) {
        // Draw the upper-left corner.
        Border.Blit(
          new Rectangle(0, 0, StretchArea.Left, StretchArea.Top),
          new Rectangle(
            X - Origin.X - StretchArea.Left + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y - StretchArea.Top + (StretchArea.Top - DisplayArea.Top),
						X - Origin.X + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y + (StretchArea.Top - DisplayArea.Top)),
          Z,
          Origin,
          Flip.None,
          Rotation,
          new Coord(ScaleWidth, ScaleHeight),
          Opacity,
					BorderHue);

				// Draw the top side.
				Border.Blit(
					new Rectangle(StretchArea.Left, 0, StretchArea.Right, StretchArea.Top),
					new Rectangle(
						X - Origin.X + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y - StretchArea.Top + (StretchArea.Top - DisplayArea.Top),
						X - Origin.X + Width + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + (StretchArea.Top - DisplayArea.Top)),
					Z,
					Origin,
					Flip.None,
					Rotation,
					new Coord(ScaleWidth, ScaleHeight),
					Opacity,
					BorderHue);

        // Draw the upper-right corner.
        Border.Blit(
          new Rectangle(StretchArea.Right, 0, Border.Width, StretchArea.Top),
          new Rectangle(
						X - Origin.X + Width + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y - StretchArea.Top + (StretchArea.Top - DisplayArea.Top),
						X - Origin.X + Width + (Border.Width - StretchArea.Right) + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + (StretchArea.Top - DisplayArea.Top)),
          Z,
          Origin,
          Flip.None,
          Rotation,
          new Coord(ScaleWidth, ScaleHeight),
          Opacity,
					BorderHue);

				// Draw the left side.
				Border.Blit(
					new Rectangle(0, StretchArea.Top, StretchArea.Left, StretchArea.Bottom),
					new Rectangle(
						X - Origin.X - StretchArea.Left + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y + (StretchArea.Top - DisplayArea.Top),
						X - Origin.X + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y + Height + (StretchArea.Bottom - DisplayArea.Bottom)),
					Z,
					Origin,
					Flip.None,
					Rotation,
					new Coord(ScaleWidth, ScaleHeight),
					Opacity,
					BorderHue);

				// Draw the right side.
				Border.Blit(
					new Rectangle(StretchArea.Right, StretchArea.Top, Border.Width, StretchArea.Bottom),
					new Rectangle(
						X - Origin.X + Width + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + (StretchArea.Top - DisplayArea.Top),
						X - Origin.X + Width + (Border.Width - StretchArea.Right) + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + Height + (StretchArea.Bottom - DisplayArea.Bottom)),
					Z,
					Origin,
					Flip.None,
					Rotation,
					new Coord(ScaleWidth, ScaleHeight),
					Opacity,
					BorderHue);

				// Draw the lower-left corner.
				Border.Blit(
					new Rectangle(0, StretchArea.Bottom, StretchArea.Left, Border.Height),
					new Rectangle(
						X - Origin.X - StretchArea.Left + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y + Height + (StretchArea.Bottom - DisplayArea.Bottom),
						X - Origin.X + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y + Height + (Border.Height - StretchArea.Bottom) + (StretchArea.Bottom - DisplayArea.Bottom)),
					Z,
					Origin,
					Flip.None,
					Rotation,
					new Coord(ScaleWidth, ScaleHeight),
					Opacity,
					BorderHue);

				// Draw the bottom side.
				Border.Blit(
					new Rectangle(StretchArea.Left, StretchArea.Bottom, StretchArea.Right, Border.Height),
					new Rectangle(
						X - Origin.X + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y + Height + (StretchArea.Bottom - DisplayArea.Bottom),
						X - Origin.X + Width + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + Height + (Border.Height - StretchArea.Bottom) + (StretchArea.Bottom - DisplayArea.Bottom)),
					Z,
					Origin,
					Flip.None,
					Rotation,
					new Coord(ScaleWidth, ScaleHeight),
					Opacity,
					BorderHue);

				// Draw the lower-right corner.
				Border.Blit(
					new Rectangle(StretchArea.Right, StretchArea.Bottom, Border.Width, Border.Height),
					new Rectangle(
						X - Origin.X + Width + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + Height + (StretchArea.Bottom - DisplayArea.Bottom),
						X - Origin.X + Width + (Border.Width - StretchArea.Right) + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + Height + (Border.Height - StretchArea.Bottom) + (StretchArea.Bottom - DisplayArea.Bottom)),
					Z,
					Origin,
					Flip.None,
					Rotation,
					new Coord(ScaleWidth, ScaleHeight),
					Opacity,
					BorderHue);

				// Draw the middle.
				Border.Blit(
					new Rectangle(StretchArea.Left, StretchArea.Top, StretchArea.Right, StretchArea.Bottom),
					new Rectangle(
						X - Origin.X + (StretchArea.Left - DisplayArea.Left),
						Y - Origin.Y + (StretchArea.Top - DisplayArea.Top),
						X - Origin.X + Width + (StretchArea.Right - DisplayArea.Right),
						Y - Origin.Y + Height + (StretchArea.Bottom - DisplayArea.Bottom)),
					Z,
					Origin,
					Flip.None,
					Rotation,
					new Coord(ScaleWidth, ScaleHeight),
					Opacity,
					BorderHue);
      }
    }
    #endregion
  }
}
