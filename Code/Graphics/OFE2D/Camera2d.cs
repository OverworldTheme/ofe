﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A 2d camera attached to each 2d stage that allows the effect of panning and zooming in and out
  /// of the stage, in addition to other neat effects.
  /// </summary>
  public class Camera2d {
    #region Class Constructor
    /// <summary>
    /// Creates a new camera centered on coordinate (0, 0).
    /// </summary>
    public Camera2d() {
      Position = new Coord(0, 0);
      Zoom = 1;
      Rotation = 0;
    }
    #endregion

    #region Orientation
    /// <summary>
    /// This camera's position in 2d space.
    /// </summary>
		public Coord Position {
			get {
				return position;
			}
			set {
				position = value;
				if(Stage.Default != null && this == Stage.Default.Camera) {
					// The default stage and GUI stage cannot share the same
					// camera because the GUI is not meant to roll or zoom.
					// However, the do share the same position.
					Stage.GUI.Camera.Position = Stage.Default.Camera.Position;
				}
			}
		}
		private Coord position;

    /// <summary>
    /// This camera's x-coordinate position in 2d space.
    /// </summary>
    public float X {
      get {
        return Position.X;
      }
      set {
        Position = new Coord(value, Y);
      }
    }

    /// <summary>
    /// This camera's x-coordinate position in 2d space.
    /// </summary>
    public float Y {
      get {
        return Position.Y;
      }
      set {
        Position = new Coord(X, value);
      }
    }

    /// <summary>
    /// The rotation of this camera, given in revolutions.
    /// Increasing the value rotates the camera clockwise.
    /// </summary>
    public float Rotation { get; set; }

    /// <summary>
    /// The zoom level of this camera.
    /// Increasing the zoom level causes the camera to zoom in.
    /// </summary>
    /// <remarks>
    /// 1 is the normal zoom level.
    /// 2 is zoomed in to double size.
    /// .5f is zoomed out to half size.
    /// .25f is zoomed out to quarter size.
    /// At 0, the stage will not even be visible.
    /// </remarks>
    public float Zoom { get; set; }
    #endregion

    #region Yummy Matrix Maths
    /// <summary>
    /// Returns the view matrix for this camera's current orientation.
    /// </summary>
    /*public Matrix Matrix {
      get {
        return Matrix.CreateScale(this.Zoom, this.Zoom, 1)
          * Matrix.Create2dRotation(this.Rotation)
          * Matrix.CreateTranslation(-X, -Y, 0);
      }
    }

    /// <summary>
    /// Returns the view matrix for this camera's current orientation, without taking rotation into account.
    /// </summary>
    public Matrix MatrixWithoutRotation {
      get {
        return Matrix.CreateScale(this.Zoom, this.Zoom, 1)
          * Matrix.CreateTranslation(-X, -Y, 0);
      }
    }*/
    #endregion
  }
}
