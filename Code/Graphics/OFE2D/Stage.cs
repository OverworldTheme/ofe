﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.Graphics.OFE2D {
  /// <summary>
  /// A stage where 2d graphics are positioned for display to the screen.
  /// All individual Graphic objects are assigned to a stage.
  /// </summary>
  /// <remarks>
  /// <para>Three stages are created by the engine on startup.</para>
	/// <para>The first is Stage.Default, and is provided for your convenience. It's useful if you're
  /// not doing anything fancy, and just want a single stage to display all your sprites
	/// and other 2d graphics. Stage.Default has a priority of 0.</para>
	/// <para>The second is Stage.GUI, which is used by OFE's GUI system. This stage has a priority
	/// of -1.</para>
	/// <para>The last is Stage.System which, as its name suggests, is used exclusively by the
  /// system. Among other things, this is where the mouse cursor is displayed. The system stage
  /// has a priority of -99. If you set a stage on top of the system stage (by giving it
	/// an even lower priority value), it will be displayed above the mouse cusor.</para>
	/// <para>While the GUI and System stages are, for the most part, not directly accessible, setting
  /// the origin point of the default stage does change the origin points of the GUI and System
	/// stages as well.</para>
  /// </remarks>
  public class Stage : GraphicGroup {
    #region Initialization
    /// <summary>
    /// Initializes the system stages.
    /// </summary>
    internal static void Init() {
      defaultStage = new Stage(0);
      GUI = new Stage(-1);
      System = new Stage(-99);
    }
    #endregion

    #region Class Constructors
    /// <summary>
    /// Creates a new stage for 2d graphics.
    /// </summary>
    /// <param name="priority">How deep this stage should be displayed in the order of all
    /// stages and scenes. Stages and scenes with lower numbers are displayed over those
    /// with higher ones.</param>
    public Stage(float priority) : base() {
      base.Init(priority);
      Camera = new Camera2d();
			CenterOrigin();
    }
    #endregion

    #region Disposal
    /// <summary>
    /// Clears this stage's graphics, and removes it from priority list.
    /// You should call this when you're done with a stage and no longer plan to use it,
    /// or plan to replace it with a new one.
    /// </summary>
		/// <exception cref="InvalidOperationException">
		/// Called if the application attempts to delete one of the default stages.
		/// </exception>
    public override void Dispose() {
			if(this == Default || this == System || this == GUI) {
				throw new InvalidOperationException("The default stages cannot be deleted.");
			} else {
				this.Clear();
				base.Dispose();
			}
    }
    #endregion

    #region Default Stages
    /// <summary>
    /// A generic stage that you can use to display sprites and other graphics. If you're not
    /// doing anything fancy, this will probably work just fine instead of setting up another
    /// stage.
    /// </summary>
    public static Stage Default {
      get {
        return defaultStage;
      }
    }
    private static Stage defaultStage;

    /// <summary>
    /// A stage for use by the GUI.
    /// </summary>
    internal static Stage GUI;

    /// <summary>
    /// A stage for use by the OFE engine only.
    /// </summary>
    internal static Stage System;
    #endregion

    #region Children
    /// <summary>
    /// A list of all the graphics that are part of this stage. Stage at the beginning of the list
    /// are displayed first and, thus, will appear to be behind the sprite groups drawn
    /// right over them.
    /// </summary>
    private List<Graphic> children = new List<Graphic>();

    /// <summary>
    /// Removes and deactivates all graphics in this group.
    /// </summary>
    public void Clear() {
			for(int i = children.Count - 1; i >= 0; i-- ) {
				children[i].IsActive = false;
				children[i].SetStage(null);
			}
    }

    /// <summary>
    /// Adds a new graphic to this group.
		/// Normally, graphics will do this automatically themselves.
    /// </summary>
    /// <param name="newChild">The sprite object to add.</param>
    public void Add(Graphic newChild) {
      this.children.Add(newChild);
      //newChild.StageProperty = this;
			NeedsSorting = true;
    }

    /// <summary>
    /// Removes a given graphic from this group's list.
		/// Generally, graphics will do this automatically themselves..
    /// </summary>
    /// <param name="toRemove">The sprite to dispose of.</param>
    public void Remove(Graphic toRemove) {
			if(toRemove != null) {
				toRemove.IsActive = false;
				children.Remove(toRemove);
      }
    }
    #endregion

    #region Size and Positioning
    /// <summary>
    /// The visible height of this stage on the screen.
    /// </summary>
    public float Height {
      get {
				if(Target == null) 
					return Display.Height * (1 / Camera.Zoom);
				else
					return Target.Height * (1 / Camera.Zoom);
      }
    }

    /// <summary>
    /// The visible width of this stage on the screen.
    /// </summary>
    public float Width {
      get {
				if(Target == null)
					return Display.Width * (1 / Camera.Zoom);
				else
					return Target.Width * (1 / Camera.Zoom);
      }
    }

    /// <summary>
    /// This scene's origin point; all elements within the scene are positioned based on this point.
    /// By default this is set as the center of the screen.
    /// </summary>
		public Coord Origin {
			get { return origin; }
		}
    private Coord origin = new Coord(Display.Center.X, Display.Center.Y);

		/// <summary>
		/// Gets whether or not the origin point of this stage is centered on the screen.
		/// </summary>
		/// <remarks>
		/// To center the stage, call method CenterOrigin.
		/// This will center the origin point, and keep it centered
		/// even if the resolution or aspect ratio is altered.
		/// Changing the origin point will change this property to "false", and
		/// manually setting it to the center of the screen will
		/// not keep it centered if the resolution or aspect ratio is changed.
		/// </remarks>
		public bool IsCentered {
			get { return isCentered; }
		}
		private bool isCentered;

    /// <summary>
    /// Sets the origin point to be the center of the screen.
    /// </summary>
    public void CenterOrigin() {
			// Set the origin.
			if(Target == null) {
				SetOrigin(new Coord(Display.Center.X, Display.Center.Y));
			} else {
				SetOrigin(new Coord(Target.Center.X, Target.Center.Y));
			}

			// We're centered.
			isCentered = true;
    }

    /// <summary>
    /// Sets the origin point of this stage to a given coordinate.
    /// </summary>
    /// <param name="x">The x-axis coordinate to set.</param>
    /// <param name="y">The y-axis coordinate to set.</param>
    public void SetOrigin(int x, int y) {
      SetOrigin(new Coord(x, y));
    }

    /// <summary>
    /// Sets the origin point of this stage to a given coordinate.
    /// </summary>
    /// <param name="origin">The coordinate to set the origin point to.</param>
    public void SetOrigin(Coord origin) {
      this.origin = origin;

      // The system and GUI stages share the default stage's origin.
      if(this == Stage.Default) {
        Stage.System.SetOrigin(new Coord(this.origin.X, this.origin.Y));
        Stage.GUI.SetOrigin(new Coord(this.origin.X, this.origin.Y));
      }

			// We're no longer considered centered.
			// CenterOrigin will change this in a moment if this is not the case.
			isCentered = false;
    }

		/// <summary>
		/// Recenters all the stages that are supposed to be centered.
		/// </summary>
		internal static void RecenterAll() {
			foreach(GraphicGroup group in GraphicGroup.all) {
				if (group is Stage)
				{
					Stage stage = (Stage)group;
					if (stage.IsCentered) stage.CenterOrigin();
				}
			}
		}

    /// <summary>
    /// The y-coordinate of the top of the stage.
    /// </summary>
    public float Top {
      get {
        return (-Origin.Y * (1 / Camera.Zoom)) + Camera.Y;
      }
    }

    /// <summary>
    /// The y-coordinate of the bottom of the stage.
    /// </summary>
    public float Bottom {
      get {
        return Top + Height;
      }
    }

    /// <summary>
    /// The x-coordinate of the left side of the stage.
    /// </summary>
    public float Left {
      get {
        return (-Origin.X * (1 / Camera.Zoom)) + Camera.X;
      }
    }

    /// <summary>
    /// The x-coordinate of the right side of the stage.
    /// </summary>
    public float Right {
      get {
        return Left + Width;
      }
    }

    /// <summary>
    /// The coordinates giving the exact center of the stage.
    /// </summary>
    public Coord Center {
      get {
        return new Coord(
          Left + Width / 2,
          Top + Height / 2);
      }
    }
    #endregion

    #region Safe Area
    /// <summary>
    /// Gets a rectangular area that important action should take place
    /// within so that the user won't miss it because of overscan.
    /// </summary>
    /// <remarks>
    /// Generally, it isn't nearly as needed as the title safe area, but has been provided just
    /// in case. Keep in mind that you needn't neglet the area obscured by overscan, just make sure
    /// that everything important stays where the user can see it.
    /// </remarks>
    public Rectangle ActionSafe {
      get {
				float width = Width * (Display.ActionSafePercentage / 100f);
				float height = Height * (Display.ActionSafePercentage / 100f);
				return new Rectangle(Left + width, Top + height, Right - width, Bottom - height);
      }
    }

    /// <summary>
    /// Gets a rectangular that text and important graphics should stay within
    /// to be seen on the display without getting cut off.
    /// </summary>
    /// <remarks>
		/// It's considered best practice to use this area even on the PC where overscan isn't really
		/// a worry. On the PC, you can change Display.TitleSafePercentage to increase the title-safe
		/// coverage.
    /// </remarks>
    public Rectangle TitleSafe {
      get {
				float width = Width * (Display.TitleSafePercentage / 100f);
				float height = Height * (Display.TitleSafePercentage / 100f);
				return new Rectangle(Left + width, Top + height, Right - width, Bottom - height);
      }
    }
    #endregion

    #region Mouse Interaction
    /// <summary>
    /// The coordinate on this stage that the mouse is currently hovering over.
    /// </summary>
    public Coord Mouse {
      get {
        return new Coord(
					(Input.Mouse.Default.X * (1 / Camera.Zoom)) + Left,
					(Input.Mouse.Default.Y * (1 / Camera.Zoom)) + Top);
      }
    }
    #endregion

    #region Camera
    /// <summary>
    /// The camera for this stage.
    /// </summary>
		public Camera2d Camera { get; set; }
    #endregion

    #region Drawing and Updating
    /// <summary>
    /// If this stage is active, draws all visible graphics in the group.
    /// </summary>
    protected override void Draw() {
			if(this.IsActive && this.Camera != null && this.children.Count > 0) {
				DrawCycle.Start2d(this);

				foreach(Graphic child in children) {
					if(child.IsActive && child.IsVisible) {
						child.Draw();
					}
				}

				DrawCycle.Finish2d();
			}
    }

    /// <summary>
    /// Updates all the graphics in this group, if the group is active.
    /// </summary>
    protected override void Update() {
      if(this.IsActive && this.children.Count > 0) {
				// Update the graphics that are parentless.
				// Parents will direct their children to update themselves.
        foreach(Graphic child in children) {
					if(child.Parentless) {
						child.Update();
					}
        }

				// Check if we need to sort anything.
				if(NeedsSorting) {
					Sort();
					NeedsSorting = false;
				}
      }
    }
    #endregion

    #region Sorting Algoritms
		/// <summary>
		/// Whether or not any of the z-depths have been changed, and thus need to be sorted.
		/// </summary>
		internal bool NeedsSorting;

    /// <summary>
    /// Sorts all the graphics in this group based on z-depth.
    /// </summary>
    internal void Sort() {
      this.children.Sort(sortByZDepth);
    }

    private int sortByZDepth(Graphic g1, Graphic g2) {
      if(g1 == null) {
        if(g2 == null) {
          return 0;
        } else {
          return 1;
        }
      } else if(g2 == null) {
        if(g1 == null) {
          return 0;
        } else {
          return -1;
        }
      } else {
				return -g1.AbsoluteZ.CompareTo(g2.AbsoluteZ);
      }
    }
    #endregion
  }
}
