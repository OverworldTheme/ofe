﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
	/// <summary>
	/// A formatting tag, used to fine tune the appearance and arrangement of text objects.
	/// </summary>	
	internal class TextTag {
		#region Keywords
		/// <summary>
		/// An enumeration of accepted tag keywords.
		/// </summary>
		public enum Keyword {
			/// <summary>
			/// An empty tag.
			/// </summary>
			None = 0,

			/// <summary>
			/// An invalid tag.
			/// </summary>
			Invalid,

			/// <summary>
			/// An unrecognized or unsupported tag.
			/// </summary>
			Unrecognized,

			/// <summary>
			/// A pause, where the text will stop scrolling for a short moment for emphasis or effect.
			/// </summary>
			Beat,

			/// <summary>
			/// Bold.
			/// </summary>
			Bold,

			/// <summary>
			/// Color.
			/// </summary>
			Color,

			/// <summary>
			/// Italics.
			/// </summary>
			Italics,

			/// <summary>
			/// Inline hyperlink.
			/// </summary>
			Link,

			/// <summary>
			/// Line break, signifying the start of a new line.
			/// </summary>
			LineBreak,

			/// <summary>
			/// A "literal section", where tags are ignored and will be displayed on-screen
			/// as part of the text.
			/// </summary>
			Literal,

			/// <summary>
			/// Strikethrough.
			/// </summary>
			Strikethrough,

			/// <summary>
			/// Subscript.
			/// </summary>
			Subscript,

			/// <summary>
			/// Superscript.
			/// </summary>
			Superscript,

			/// <summary>
			/// Signals the text to jump to the next tab stop.
			/// </summary>
			Tab,

			/// <summary>
			/// Underline.
			/// </summary>
			Underline,
		}
		#endregion

		#region Data
		/// <summary>
		/// The key of this tag; what kind of tag it is.
		/// </summary>
		public Keyword Key;

		/// <summary>
		/// The name of this keyword, as it appeared in the original source.
		/// Capitalization is preserved.
		/// </summary>
		public string KeywordText;

		/// <summary>
		/// The value of this tag, if any.
		/// </summary>
		public string Value;

		/// <summary>
		/// The integer value of this tag.
		/// Returns 0 if the value cannot be converted to a number.
		/// </summary>
		public int IntValue {
			get {
				try {
					return Int32.Parse(Value);
				} catch {
					return 0;
				}
			}
		}

		/// <summary>
		/// The floating point value of this tag.
		/// Returns Single.NaN if the value cannot be converted to a number.
		/// </summary>
		public float FloatValue {
			get {
				if(string.IsNullOrWhiteSpace(Value)) {
					return 0;
				} else {
					try {
						return Single.Parse(Value);
					} catch {
						return Single.NaN;
					}
				}
			}
		}

		/// <summary>
		/// Whether or not this is a closing tag.
		/// </summary>
		public bool IsClosing;

		/// <summary>
		/// Whether or not this is a self-closing tag.
		/// </summary>
		public bool IsSelfClosing;
		#endregion

		#region Class Constructors
		/// <summary>
		/// Creates a new, empty text tag.
		/// </summary>
		public TextTag() : this(Keyword.None) { }

		/// <summary>
		/// Creates a new text tag of the given kind.
		/// </summary>
		/// <param name="key">What kind of text tag this will be.</param>
		public TextTag(Keyword key) {
			KeywordText = string.Empty;
			Value = string.Empty;

			Key = key;
		}
		#endregion

		#region Checks
		/// <summary>
		/// Whether or not this tag's keyword is unrecognized, or the tag was somehow invalid.
		/// </summary>
		public bool IsInvalidOrUnrecognized {
			get {
				if(Key == Keyword.Invalid || Key == Keyword.Unrecognized)
					return true;
				else
					return false;
			}
		}
		#endregion

		#region Colors
		/// <summary>
		/// The color of a color tag.
		/// </summary>
		/// <remarks>Color.None if the value is unrecognized, or this is not a color tag.</remarks>
		public Color GetColor() {
			Color color = Color.None;

			// Check if this is a custom color.
			if(Value.Length > 0 && Value[0] == '#') {
			} else {
				// Check for the color name.
				switch(Value.ToLowerInvariant()) {
					case "blue":
						return Color.Blue;
					case "red":
						return Color.Red;
					default:
						return Color.None;
				}
			}

			return color;
		}
		#endregion
	}
}
