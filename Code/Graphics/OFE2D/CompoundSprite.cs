﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics.OFE2D {
	/// <summary>
	/// A group of sprites grouped together and treated as a single entity, designed so that
	/// many layers of graphics can be easily added and removed on the fly.
	/// </summary>
	public class CompoundSprite : Graphic {
		#region Class Constructor
		/// <summary>
		/// Creates a new compound sprite.
		/// </summary>
		/// <param name="stage">The stage to put this compound sprite in.</param>
		/// <param name="slots">How many initial "slots" to create in the list. Estimate how many sprites will usually
		/// be in this collection at any given time and base the value on that.</param>
		/// <param name="z">The z-depth to set this </param>
		/// <param name="position">Where to put this compound sprite in the scene.</param>
		public CompoundSprite(Stage stage, int slots, int z, int x, int y) {
			SetStage(stage);
			this.Z = z;
			sprites = new List<Sprite>(slots);
			for(int i = 0; i < slots; i++) {
				sprites.Add(new Sprite(null));
			}

			this.Position = new Coord(x, y);
		}
		#endregion

		#region Collection
		/// <summary>
    /// The collection of models that make up this compound model.
    /// </summary>
    private List<Sprite> sprites;

    /// <summary>
    /// The current number of sprites stored in the collection.
		/// Note that not all of the models in the collection are necessarily active.
    /// </summary>
    public int Capacity {
      get {
        return sprites.Count;
      }
    }

    /// <summary>
    /// The total number of active models in the collection.
    /// </summary>
    public int Count {
      get {
        return count;
      }
    }
    private int count;

		/// <summary>
		/// Returns a model that is not currently being used.
		/// </summary>
		/// <returns>If there is no inactive model, a new model is returned that has been added to
		/// the collection.</returns>
		private Sprite getSprite() {
			for(int i = 0; i < sprites.Count; i++) {
				if(!sprites[i].IsActive) {
					return sprites[i];
				}
			}

			Sprite newSprite = new Sprite(null);
			sprites.Add(newSprite);
			return newSprite;
		}
		#endregion

		#region Management
    /// <summary>
    /// Adds a single sprite to the collection.
    /// </summary>
    /// <param name="source">The sprite source to use for the new model.</param>
    /// <param name="position">Where to put the sprite, in relation to the compound sprite.</param>
		/// <param name="z">The z-depth to give the sprite, in relation to the compound sprite.</param>
    public void Add(SpriteSource source, float z, Coord position) {
      Add(source, z, position, 0, Coord.One);
    }

    /// <summary>
    /// Adds a single sprite to the collection.
    /// </summary>
    /// <param name="source">The sprite source to use for the new sprite.</param>
    /// <param name="position">Where to put the sprite, in relation to the compound sprite.</param>
		/// <param name="z">The z-depth to give the sprite, in relation to the compound sprite.</param>
    /// <param name="rotation">The rotation to give this sprite.</param>
    /// <param name="scale">The scale to give this sprite.</param>
    public void Add(SpriteSource source, float z, Coord position, float rotation, Coord scale) {
      if(source != null) {
        // Note the increase.
        count++;

        // Find an inactive model and use that, if there is one.
        Sprite sprite = getSprite();

        // Turn the model on and set its values.
				sprite.IsActive = true;
				sprite.SetSource(source);
				sprite.Position = position;
				sprite.Z = z;
				sprite.Rotation = rotation;	
				sprite.ScaleWidth = scale.X;
				sprite.ScaleHeight = scale.Y;

				// Make this the most recent sprite.
				last = sprite;

        // Set this compound model as the parent of the sprite.
				this.AddChild(sprite);
      }
    }

		/// <summary>
		/// The sprite most recently added to this compound sprite.
		/// Allows you to edit the sprite's properties (such as animation) after adding it,
		/// before you go on to add the next sprite.
		/// </summary>
		public Sprite Last {
			get {
				return last;
			}
		}
		private Sprite last;

    /// <summary>
    /// Disables all the sprites in this collection, but keeps them around to recycle as "new" models.
    /// </summary>
    public void Clear() {
      // Turn off all the sprites--but do not release them!
      for(int i = 0; i < sprites.Count; i++) {
        sprites[i].IsActive = false;
        sprites[i].SetSource(null);
      }
      count = 0;
			last = null;
    }
		#endregion

		#region Death
		protected override void dispose(bool dispose) {
			if(dispose) {
				// We need to deactivate everything in the collection.
				foreach(Sprite sprite in sprites) {
					sprite.Dispose();
				}
			}

      // Now we can let ourself go.
      base.dispose(dispose);
    }
		#endregion

		#region Update Cycle
		public override void Update() {
      base.Update();

      // Make sure all the models in the collection reflect this entity's status.
      for(int i = 0; i < sprites.Count; i++) {
        if(sprites[i].IsActive) {
          sprites[i].IsVisible = this.IsVisible;
        }
      }
    }

		public override void Draw() {
			base.Draw();

			// We draw the sprites ourselves.
			for(int i = 0; i < sprites.Count; i++) {
				if(sprites[i].IsActive) {
					sprites[i].Draw();
				}
			}
		}
		#endregion		
	}
}
