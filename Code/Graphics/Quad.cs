﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Graphics.OFE3D;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A four-sided polygon. The polygon is defined by four integers that refer to
  /// a index position in an external vertex list.
  /// </summary>
  public struct Quad {
    /// <summary>
    /// The index of the first vertex.
    /// </summary>
    public int V1;

    /// <summary>
    /// The index of the second vertex.
    /// </summary>
    public int V2;

    /// <summary>
    /// The index of the third vertex.
    /// </summary>
    public int V3;

    /// <summary>
    /// The index of the fourth vertex.
    /// </summary>
    public int V4;

    /// <param name="v1">Index of the first vertex.</param>
    /// <param name="v2">Index of the second vertex.</param>
    /// <param name="v3">Index of the third vertex.</param>
    /// <param name="v4">Index of the fourth vertex.</param>
    public Quad(int v1, int v2, int v3, int v4) {
      V1 = v1;
      V2 = v2;
      V3 = v3;
      V4 = v4;
    }

    /// <summary>
    /// Draws this quad to the screen using the given vertex list. Must be called during a draw operation.
    /// </summary>
    /// <param name="vertexList">A list of vertices that this quad will reference when drawing.</param>
    /// <param name="matrix">The display matrix.</param>
		/// <exception cref="System.InvalidOperationException">Will be thrown if method is called outside the draw cycle.</exception>
    public void Out(Vertex[] vertexList) {
      // A quad is just two polygons. How simple! :D
      new Poly(V1, V2, V4).Out(vertexList);
      new Poly(V2, V3, V4).Out(vertexList);
    }
  }
}
