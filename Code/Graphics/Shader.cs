﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

#if OPENGL
using OpenGL;
#endif

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics {
	/// <summary>
	/// A shader that can be applied to a texture.
	/// </summary>
	public class Shader : ILoadable {
		#region Class Constructor
		/// <summary>
		/// Creates a new shader from the code in the given file.
		/// </summary>
		/// <param name="kind">The kind of shader to create.</param>
		/// <param name="filename">The relative path and filename of the shader code file.</param>
		/// <returns>The new shader.</returns>
		public static Shader FromFile(Type kind, string filename) {
			return new Shader(kind, filename);
		}

		/// <summary>
		/// Creates a new shader from the raw code given.
		/// </summary>
		/// <param name="kind">The kind of shader to create.</param>
		/// <param name="filename">The relative path and filename of the shader code file.</param>
		/// <returns>The new shader.</returns>
		public static Shader FromCode(Type kind, string code)
		{
			Shader shader = new Shader(kind);
			shader.Code = code;
			Core.AddUpdateCallback(shader.Mount);
			return shader;
		}
		
		private Shader(Type kind)
		{
			Kind = kind;
			onLoadedLock = new object();

#if OPENGL
			// We need to create an OpenGL shader of the given kind.
			switch (kind)
			{
				case Type.Pixel:
					DebugLog.Whisper("Created a new pixel shader.");
					OGL_Shader = GL.CreateShader(GlEnum.ShaderType.Fragment);
					break;
				case Type.Vertex:
					DebugLog.Whisper("Created a new vertex shader.");
					OGL_Shader = GL.CreateShader(GlEnum.ShaderType.Vertex);
					break;
				case Type.Geometry:
					DebugLog.Whisper("Created a new geometry shader.");
					OGL_Shader = GL.CreateShader(GlEnum.ShaderType.Geometry);
					break;
			}
#endif

			Display.LogErrors();
		}

		private Shader(Type kind, string filename)
			:this(kind)
		{
			PathAndFilename = filename;
			FileWasBad = false;
			IsLoaded = false;

			Grunt.Enqueue(this);
		}
		#endregion

		#region Disposal
				/// <summary>
		/// Whether or not this shader program has been disposed.
		/// </summary>
		private bool isDisposed;

		/// <summary>
		/// Disposes of this shader program.
		/// </summary>
		public void Dispose() {
			dispose(true);
		}

		/// <summary>
		/// Disposes of this shader program.
		/// </summary>
		/// <param name="disposeManaged">Whether or not to dispose managed objects along with non-managed.</param>
		private void dispose(bool disposeManaged) {
			if(isDisposed)
				return;
			
			isDisposed = true;

#if OPENGL
			// We can delete our OpenGL program now.
			Core.AddUpdateCallback(() => {
				GL.DeleteShader(OGL_Shader);
				Display.LogErrors();
			});
#endif
						
			GC.SuppressFinalize(this);
		}

		~Shader() {
			dispose(false);
		}
		#endregion

		#region Shader Types
		/// <summary>
		/// What kind of shader this is.
		/// </summary>
		public Type Kind { get; private set; }

		/// <summary>
		/// An enumeration of shader types.
		/// </summary>
		public enum Type {
			/// <summary>
			/// A geometry shader can be used to create graphics primatives.
			/// Not all platforms support them.
			/// </summary>
			Geometry,

			/// <summary>
			/// A pixel shader (also known as a fragment shader) performs operations on individual pixels.
			/// </summary>
			Pixel,

			/// <summary>
			/// A vertex shader performs operations on groups of vertices.
			/// </summary>
			Vertex,
		}
		#endregion

		#region Code
		/// <summary>
		/// The raw shader code, if available.
		/// </summary>
		/// <remarks>
		/// In release builds, the shader code is deleted once it's been mounted to the GPU to save a little RAM.
		/// </remarks>
		public string Code { get; private set; }
		#endregion

		#region Platform Specific
#if OPENGL
		/// <summary>
		/// The ID for the shader, given to us by OpenGL.
		/// </summary>
		internal uint OGL_Shader;
#endif
		#endregion		

		#region Loading
		/// <summary>
		/// The path and filename that the shader was loaded from, wihtout the file extension.
		/// </summary>
		public string PathAndFilename { get; private set; }

		/// <summary>
		/// Whether or not the shader code has finished loading.
		/// </summary>
		public bool IsLoaded { get; private set; }

		/// <summary>
		/// Whether or not there was some issue loading or compiling the file.
		/// </summary>
		public bool FileWasBad { get; private set; }

		/// <summary>
		/// The file extension for shaders.
		/// By default this is ".shader".
		/// </summary>
		public static string Extension = ".shader";

		/// <summary>
		/// A lock used when checking or firing the OnLoaded event.
		/// </summary>
		private object onLoadedLock;

		/// <summary>
		/// Event called when the shader has finished loading.
		/// </summary>
		public EventHandler OnLoaded {
			get {
				lock(onLoadedLock) {
					return onLoaded;
				}
			}
			set {
				lock(onLoadedLock) {
					if(IsLoaded) {
						value.Invoke(this, EventArgs.Empty);
					} else {
						onLoaded = value;
					}
				}
			}
		}
		private EventHandler onLoaded;

		/// <summary>
		/// Immediantly loads the code for this shadow.
		/// Will normally be called by Grunt when it's time to load.
		/// </summary>
		public void LoadNow() {
			// Tell the log what we're doing.
			DebugLog.Whisper("Preparing to load and prepare shader " + PathAndFilename + "...");

			// See if the file even exists.
			string file = Path.Combine(Grunt.FullPath, PathAndFilename);
			if (!Path.HasExtension(file)) file += Extension;
			if(!File.Exists(file)) {
				DebugLog.Failure("Could not find shader file " + PathAndFilename + "!");
				FileWasBad = true;
				IsLoaded = true;
				return;
			}

			// We'll load the code for the shader and then pass it to the graphics API.
			Code = string.Empty;
			try {
				StreamReader stream = File.OpenText(file);
				Code = stream.ReadToEnd();
				stream.Close();
			} catch {
				DebugLog.Failure("There was an issue reading shader file " + PathAndFilename + "!");
				FileWasBad = true;
				IsLoaded = true;
				return;
			}

#if OPENGL
			// We need to compile the shader source on the main thread.
			Core.AddUpdateCallback(Mount);
#endif			
		}
		#endregion

		#region Mounting
		/// <summary>
		/// Sends the shader data to the video card.
		/// Must be called from the main thread.
		/// </summary>
		internal void Mount()
		{
#if OPENGL
			// Compile
			GL.ShaderSource(OGL_Shader, 1, new string[] { Code }, new int[] { Code.Length });
			GL.CompileShader(OGL_Shader);

			// Check for errors.
			int isCompiled = 0;
			GL.GetShaderIV(OGL_Shader, GlEnum.ShaderParameter.CompileStatus, out isCompiled);
			if (isCompiled == 0)
			{
				string glInfoLog;
				int glInfoLogLength;
				GL.GetShaderInfoLog(OGL_Shader, 1024, out glInfoLogLength, out glInfoLog);
        DebugLog.Failure("Failed to compile a shader!");
				if(glInfoLogLength > 0)
					DebugLog.Failure("The compiler reports: "	+ glInfoLog);
				FileWasBad = true;
			}
			else
			{
				DebugLog.Whisper("A shader compiled successfully.");
				FileWasBad = false;
			}

			// Done!			
			IsLoaded = true;
			if (OnLoaded != null)
				OnLoaded.Invoke(this, EventArgs.Empty);

			// Delete the code to save a little RAM.
#if !DEBUG
			Code = null;
#endif

#endif
		}
		#endregion
	}
}
