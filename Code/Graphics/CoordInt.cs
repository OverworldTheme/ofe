﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A 2d coordinate, given as (x, y), in which both x and y are integers (whole numbers).
  /// </summary>
	[OFEdataSerialize]
	public struct CoordInt {
    #region Class Constructors
    /// <param name="x">The location to assign for the X axis.</param>
    /// <param name="y">The location to assign for the Y axis.</param>
    public CoordInt(int x, int y) {
      X = x;
      Y = y;
    }

    /// <param name="xy">The location to assign for both the X and Y axes.</param>
    public CoordInt(int xy) : this(xy, xy) { }
    #endregion

    #region Static Coordinate Definitions
    /// <summary>
    /// Coordinate (0, 0).
    /// </summary>
    public static readonly CoordInt Zero = new CoordInt(0);

    /// <summary>
    /// Coordinate (1, 1).
    /// </summary>
    public static readonly CoordInt One = new CoordInt(1);
    #endregion

		#region Coordinates
		/// <summary>
    /// The coordinate of the X axis.
    /// </summary>
    public int X;

    /// <summary>
    /// The coordinate of the Y axis.
    /// </summary>
    public int Y;
		#endregion

		#region Operators
		public override int GetHashCode() {
      return base.GetHashCode();
    }

		public override string ToString() {
			return X + ", " + Y;
		}

    public override bool Equals(object obj) {
      return base.Equals(obj);
    }

    public static bool operator ==(CoordInt c1, CoordInt c2) {
      if(c1.X == c2.X && c1.Y == c2.Y) {
        return true;
      } else {
        return false;
      }
    }

    public static bool operator !=(CoordInt c1, CoordInt c2) {
      if(c1 == c2) {
        return false;
      } else {
        return true;
      }
    }

    public static CoordInt operator +(CoordInt c1, CoordInt c2) {
			return new CoordInt(c1.X + c2.X, c1.Y + c2.Y);
    }

		public static CoordInt operator -(CoordInt coord) {
			return new CoordInt(-coord.X, -coord.Y);
    }

		public static CoordInt operator -(CoordInt c1, CoordInt c2) {
			return new CoordInt(c1.X - c2.X, c1.Y - c2.Y);
    }

		public static implicit operator Coord(CoordInt coord) {
			return new Coord(coord.X, coord.Y);
    }
    #endregion

    #region Methods
    /// <summary>
    /// Calculates the smallest angle between two coordinates.
    /// </summary>
    /// <returns>A number from 0 to .25 revolutions. At 0, the points have the exact same Y coordinate.
    /// A .25, they have the exact same X coordinate.</returns>
    public static float AngleBetween(CoordInt coord1, CoordInt coord2) {
      return Coord.AngleBetween((Coord)coord1, (Coord)coord2);
    }

    /// <summary>
    /// Calculates the smallest angle between this and another coordinate.
    /// </summary>
    /// <returns>A number from 0 to .25 revolutions. At 0, the points have the exact same Y coordinate.
    /// At .25, they have the exact same X coordinate.</returns>
    public float AngleTo(CoordInt coord) {
      return AngleBetween(this, coord);
    }

    /// <summary>
    /// Calculates the distance from this coordinate to the given coordinate.
    /// </summary>
    /// <param name="coord"></param>
    /// <returns>The distance.</returns>
    public float Distance(CoordInt coord) {
      // Get the differences between the two coordinates.
      float differenceX = coord.X - this.X;
      float differenceY = coord.Y - this.Y;

      // Now use some basic trigonometry to get the distance between them.
      return Mather.Sqrt((differenceX * differenceX) + (differenceY * differenceY));
    }

    /// <summary>
    /// Calculates the distance between two given coordinates
    /// </summary>
    /// <param name="coord1">The first coordinate.</param>
    /// <param name="coord2">The second coordinate.</param>
    /// <returns>The distance.</returns>
    public static float Distance(CoordInt coord1, CoordInt coord2) {
      return coord1.Distance(coord2);
    }
    #endregion
  }
}
