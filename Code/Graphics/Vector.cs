﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Graphics {
  /// <summary>
  /// A three dimensional vector defining a location or direction in 3d space.
  /// </summary>
  /// <remarks>OFE uses a coordinate system where Z is the up/down axis.</remarks>
  public struct Vector {
		#region Data
		/// <summary>
		/// The x-axis coordinate.
		/// </summary>
		public float X;

		/// <summary>
		/// The y-axis coordinate.
		/// </summary>
		public float Y;

		/// <summary>
		/// The z-axis coordinate. In OFE, this is the "up-down" axis that defines "height".
		/// </summary>
		public float Z;
		#endregion

    #region Vector Definitions
    /// <summary>
    /// A vector pointing back (0, 1, 0);
    /// </summary>
    public static readonly Vector Back = new Vector(0, -1, 0);

    /// <summary>
    /// A vector pointing down (0, 0, -1).
    /// </summary>
    public static readonly Vector Down = new Vector(0, 0, -1);

    /// <summary>
    /// A vector pointing forward.
    /// </summary>
    public static readonly Vector Forward = new Vector(0, 1, 0);

    /// <summary>
    /// A vector pointing left (-1, 0, 0).
    /// </summary>
    public static readonly Vector Left = new Vector(-1, 0, 0);

    /// <summary>
    /// A vector pointing right (1, 0, 0).
    /// </summary>
    public static readonly Vector Right = new Vector(1, 0, 0);

    /// <summary>
    /// A vector pointing in  the up direction. In OFE, this is the Z-axis (0, 0, 1).
    /// </summary>
    public static readonly Vector Up = new Vector(0, 0, 1);

    /// <summary>
    /// A vector consisting of (0, 0, 0).
    /// </summary>
    public static readonly Vector Zero = new Vector(0, 0, 0);

    /// <summary>
    /// A vector consisting of (1, 1, 1).
    /// </summary>
    public static readonly Vector One = new Vector(1, 1, 1);
    #endregion

    #region Other Properties
    /// <summary>
    /// The magnitute (length) of this vector.
    /// </summary>
    public float Magnitude {
      get {
        return (float)Math.Sqrt(X * X + Y * Y + Z * Z);
      }
    }
    #endregion

    #region Class Constructors
    /// <param name="value">The value to set to the x, y, and z-axis coordinate.</param>
    public Vector(float value) : this(value, value, value) { }

    /// <param name="x">The value to set for the x-axis coordinate.</param>
    /// <param name="y">The value to set for the y-axis coordinate.</param>
    public Vector(float x, float y) : this(x, y, 0) { }

    /// <param name="x">The value to set for the x-axis coordinate.</param>
    /// <param name="y">The value to set for the y-axis coordinate.</param>
    /// <param name="z">The value to set for the z-axis coordinate. Remember that this is the "up-down" axis in OFE.</param>
    public Vector(float x, float y, float z) {
      this.X = x;
      this.Y = y;
      this.Z = z;
    }

		/*/// <summary>
		/// Gets a directional axis angle from the given quaternion.
		/// </summary>
		/// <param name="quaternion"></param>
		/// <returns></returns>
		public Vector AxisAngleFromQuaternion(Quaternion quaternion)
		{
			Vector v;

			float sqrt = Mather.Sqrt(1 - (quaternion.W * quaternion.W));
			float angle = 2 * Mather.Acos(quaternion.W);
			v.X = quaternion.X / sqrt;
			v.Y = quaternion.Y / sqrt;
			v.Z = quaternion.Z / sqrt;

			return v;
		}*/
    #endregion

		#region Overrides
		public override string ToString()
		{
			return "{" + X + ", " + Y + ", " + Z + "}";
		}
		#endregion

    #region Operators
    public override int GetHashCode() {
      return base.GetHashCode();
    }

    public override bool Equals(object obj) {
      return base.Equals(obj);
    }

    public static bool operator ==(Vector v1, Vector v2) {
      if(v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z) {
        return true;
      } else {
        return false;
      }
    }

    public static bool operator !=(Vector v1, Vector v2) {
      if(v1 == v2) {
        return false;
      } else {
        return true;
      }
    }

    public static Vector operator +(Vector v1, Vector v2) {
      return new Vector(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
    }

    public static Vector operator -(Vector v1, Vector v2) {
      return new Vector(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
    }

    public static Vector operator *(Vector vector, Matrix matrix) {
      return Vector.Transform(vector, matrix);
    }
    
    public static Vector operator *(Vector v1, Vector v2) {
      return Vector.Multiply(v1, v2);
    }

    public static Vector operator *(Vector vector, float scalar) {
      return Vector.Multiply(vector, scalar);
    }

    public static Vector operator *(float scalar, Vector vector) {
      return Vector.Multiply(scalar, vector);
    }
    #endregion

    #region Vector Math
    /// <summary>
    /// Returns a vector containing the cross product of two vectors.
    /// </summary>
    /// <param name="v1">The first vector.</param>
    /// <param name="v2">The second vector.</param>
    public static Vector Cross(Vector v1, Vector v2) {
      // Algebra makes my brain hurt.
      return new Vector(
        v1.Y * v2.Z - v1.Z * v2.Y,
        v1.Z * v2.X - v1.X * v2.Z,
        v1.X * v2.Y - v1.Y * v2.X);
    }

    /// <summary>
    /// Calculates the distance between two vectors.
    /// </summary>
    /// <param name="v1">The first vector.</param>
    /// <param name="v2">The second vector.</param>
    public static float Distance(Vector v1, Vector v2) {
      float dX = v2.X - v1.X;
      float dY = v2.Y - v1.Y;
      float dZ = v2.Z - v1.Z;
      return (float)Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
    }

    /// <summary>
    /// Returns the dot product of two vectors.
    /// </summary>
    /// <param name="v1">The first vector.</param>
    /// <param name="v2">The second vector.</param>
    /// <returns>Some maths.</returns>
    public static float Dot(Vector v1, Vector v2) {
      return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
    }

		/// <summary>
		/// Reverses a vector.
		/// This is the same as multiplying it by -1.
		/// </summary>
		/// <returns></returns>
		public Vector Inverse()
		{
			return this * -1;
		}

    /// <summary>
    /// Multiplies a vector by the specified scalar.
    /// </summary>
    /// <param name="v">The vector.</param>
    /// <param name="scalar">The scalar.</param>
    /// <returns></returns>
    public static Vector Multiply(Vector v, float scalar) {
      return new Vector(v.X * scalar, v.Y * scalar, v.Z * scalar);
    }

    /// <summary>
    /// Multiplies a scalar by the specified vector.
    /// </summary>
    /// <param name="v">The vector.</param>
    /// <param name="scalar">The scalar.</param>
    /// <returns></returns>
    public static Vector Multiply(float scalar, Vector v) {
      return Vector.Multiply(v, scalar);
    }

    /// <summary>
    /// Multiplies the values of two vectors.
    /// </summary>
    /// <param name="v1">The first vector.</param>
    /// <param name="v2">The second vector.</param>
    /// <returns>A new vector whose components contain the multiplies products
    /// of the given vectors.</returns>
    /// <remarks>It is important to note that this is not vector multiplication
    /// (the "cross product" or "dot product"). Instead, this simply multiplies
    /// the individual values of the two vectors.</remarks>
    public static Vector Multiply(Vector v1, Vector v2) {
      return new Vector(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
    }

		/// <summary>
		/// Gets the normalized form of this vector.
		/// </summary>
		public Vector Normalized()
		{
			Vector v;

			float length = this.Magnitude;
			v.X = X / length;
			v.Y = Y / length;
			v.Z = Z / length;

			return v;
		}

    /// <summary>
    /// Returns the normalized version of a vector.
    /// </summary>
    /// <param name="vector">The vector to create a normalized vector from.</param>
    /// <returns>A new normalized vector.</returns>
    public static Vector Normalize(Vector vector) {
			return vector.Normalized();
    }
		#endregion

		#region Transformations
		/// <summary>
		/// Rotates a unit vector using a quaternion.
		/// </summary>
		/// <param name="vector">The unit vector to rotate.</param>
		/// <param name="quaternion">The quaternion rotation value.</param>
		/// <returns></returns>
		public static Vector Rotate(Vector vector, Quaternion quaternion)
		{
			// Treat the vector as a quaternion with a scalar of 0.
			Quaternion vq = new Quaternion(vector.X, vector.Y, vector.Z, 0);

			// Multiply by the quaternion, and its inverse.
			vq = quaternion * vq * quaternion.Inverse();

			// Now return the vector, ignoring the scalar.
			return new Vector(vq.X, vq.Y, vq.Z);
		}

		/// <summary>
    /// Transforms a vector based on the given matrix.
    /// </summary>
    /// <param name="vector">The vector value to calculate the transformation on.</param>
    /// <param name="matrix">The transformation matrix.</param>
    /// <returns></returns>
    public static Vector Transform(Vector vector, Matrix matrix) {
      Vector newVector = new Vector();

      newVector.X = vector.X * matrix.M11 + vector.Y * matrix.M21 + vector.Z * matrix.M31 + matrix.M41;
      newVector.Y = vector.X * matrix.M12 + vector.Y * matrix.M22 + vector.Z * matrix.M32 + matrix.M42;
      newVector.Z = vector.X * matrix.M13 + vector.Y * matrix.M23 + vector.Z * matrix.M33 + matrix.M43;

      return newVector;
    }
    #endregion
  }
}
