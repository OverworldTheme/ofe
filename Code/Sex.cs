﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE {
	/// <summary>
	/// An enumeration of human sexes.	
	/// </summary>
	/// <remarks>
	/// Numerical values are based on ISO/IEC 5218.
	/// The fact that 1 is assigned to male and 2 is assigned to female has no significance.
	/// </remarks>
	public enum Sex {
		/// <summary>
		/// The sex of the individual is not known.
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// The individual is a male.
		/// </summary>
		Male = 1,

		/// <summary>
		/// The individual is a female.
		/// </summary>
		Female = 2,

		/// <summary>
		/// The value is irrelevant on innapplicable.
		/// </summary>
		NotApplicable = 9,
	}
}
