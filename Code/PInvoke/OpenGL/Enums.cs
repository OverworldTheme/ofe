﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenGL
{
	/// <summary>
	/// <para>OpenGL has a lot of constants used as enumerations.
	/// No, seriously, a LOT.</para>
	/// <para>For the sake of cleanliness and sanity,
	/// they are given their own file here.
	/// They have also been catagorized,
	/// to further make our lives easier.</para>
	/// </summary>
	/// <remarks>
	/// Most of the enumeration categories are based on those of OpenTK.
	/// However, there are a number of differences,
	/// so if you're used to using OpenTK, just take note.
	/// </remarks>
	internal static class GlEnum
	{
		#region Name
		public enum Name
		{
			/// <summary>
			/// For GetStringI only, returns the extension string support by the implementation at index.
			/// </summary>
			Extensions = 7939,

			/// <summary>
			/// The name of the renderer.
			/// This name is typically specific to a particular configuration of a hardware platform.
			/// It does not change from release to release.
			/// </summary>
			Renderer = 7168,

			/// <summary>
			/// A version or release number for the shading language.
			/// </summary>
			ShadingLanguageVersion = 35724,

			/// <summary>
			/// The company responsible for this GL implementation.
			/// The name does not change from release to release.
			/// </summary>
			Vendor = 7936,

			/// <summary>
			/// A version or release number.
			/// </summary>
			Version = 7938,
		}
		#endregion

		#region Buffer
		/// <summary>
		/// Targets for buffer functions.
		/// </summary>
		public enum BufferTarget
		{
			Array = 0x8892,
			ElementArray = 0x8893,
			/// <summary>
			/// Requires OpenGL 2.1.
			/// </summary>
			PixelPack = 0x88EB,
			/// <summary>
			/// Required OpenGL 2.1.
			/// </summary>
			PixelUnpack = 0x88EC
		}		

		/// <summary>
		/// A hint to the GL implementation as to how a buffer object's data store will be accessed.
		/// </summary>
		public enum UsagePattern
		{
			/// <summary>
			/// The data store contents will be modified once and used at most a few times.
			/// The data contents are modified by the application,
			/// and used as the source for GL drawing and image specification commands.
			/// </summary>
			StreamDraw = 0x88E0,

			/// <summary>
			/// The data store contents will be modified once and used at most a few times.
			/// The data contents are modified by reading data from the GL,
			/// and used to return that data qhen queried by the application.
			/// </summary>
			StreamRead,

			/// <summary>
			/// The data store contents will be modified once and used at most a few times.
			/// The data contents are modified by reading data from the GL,
			/// and used as the source for GL drawing and image specification commands.
			/// </summary>
			StreamCopy,

			/// <summary>
			/// The data store contents will be modified once and used many times.
			/// The data contents are modified by the application,
			/// and used as the source for GL drawing and image specification commands.
			/// </summary>
			StaticDraw = 0x88E4,

			/// <summary>
			/// The data store contents will be modified once and used many times.
			/// The data contents are modified by reading data from the GL,
			/// and used to return that data qhen queried by the application.
			/// </summary>
			StaticRead,

			/// <summary>
			/// The data store contents will be modified once and used many times.
			/// The data contents are modified by reading data from the GL,
			/// and used as the source for GL drawing and image specification commands.
			/// </summary>
			StaticCopy,

			/// <summary>
			/// The data store contents will be modified repeatedly and used many times.
			/// The data contents are modified by the application,
			/// and used as the source for GL drawing and image specification commands.
			/// </summary>
			DynamicDraw = 0x88E8,

			/// <summary>
			/// The data store contents will be modified repeatedly and used many times.
			/// The data contents are modified by reading data from the GL,
			/// and used to return that data qhen queried by the application.
			/// </summary>
			DynamicRead,

			/// <summary>
			/// The data store contents will be modified repeatedly and used many times.
			/// The data contents are modified by reading data from the GL,
			/// and used as the source for GL drawing and image specification commands.
			/// </summary>
			DynamicCopy,
		}
		#endregion

		#region Capability
		/// <summary>
		/// A capability of the library.
		/// Used primarily to enable/disable OpenGL capabilities.
		/// </summary>
		public enum Capability
		{
			/// <summary>
			/// Alpha testing.
			/// </summary>
			AlphaTest = 3008,

			/// <summary>
			/// Generates normal vectors with either GL_MAP2_VERTEX_3 or GL_MAP2_VERTEX_4 is used to generate vertices.
			/// </summary>
			AutoNormal = 3456,

			/// <summary>
			/// Blends the computed fragment color values with the values in the color buffers.
			/// </summary>
			Blend = 3042,

			/// <summary>
			/// Clips geometry against user-defined clipping plane 0.
			/// </summary>
			ClipPlane0 = 12288,

			/// <summary>
			/// Clips geometry against user-defined clipping plane 1.
			/// </summary>
			ClipPlane1,

			/// <summary>
			/// Clips geometry against user-defined clipping plane 2.
			/// </summary>
			ClipPlane2,

			/// <summary>
			/// Clips geometry against user-defined clipping plane 3.
			/// </summary>
			ClipPlane3,

			/// <summary>
			/// Clips geometry against user-defined clipping plane 4.
			/// </summary>
			ClipPlane4,

			/// <summary>
			/// Clips geometry against user-defined clipping plane 5.
			/// </summary>
			ClipPlane5,

			/// <summary>
			/// Applies the currently selected logical operation to the computed fragment color and color buffer values.
			/// </summary>
			ColorLogicOp = 3058,

			/// <summary>
			/// If enabled, has one or more material parameters track the current color.
			/// </summary>
			ColorMaterial = 2903,

			/// <summary>
			/// If enabled and no fragment shader is active, add the secondary color value to the computer fragment color.
			/// </summary>
			ColorSum = 33880,

			/// <summary>
			/// If enabled, cull polygons based on their winding in window coordinates.
			/// </summary>
			CullFace =0xB44,

			/// <summary>
			/// If enabled, perform a color table lookup on the incoming RGBA color values.
			/// </summary>
			//ColorTable,

			/// <summary>
			/// If enabled, perform a 1D convolution operation on incoming RGBA color values.
			/// </summary>
			//Convolution1D,

			/// <summary>
			/// If enabled, perform a 2D convolution operation on incoming RGBA color values.
			/// </summary>
			//Convolution2D,

			/// <summary>
			/// If enabled, does depth comparisons and updates the depth buffer.
			/// Note that even if the depth buffer exists and the depth mask is non-zero,
			/// the depth buffer is not updated if the dapth test is disabled.
			/// </summary>
			DepthTest = 2929,

			/// <summary>
			/// If enabled, dither color components or indices before they are written to the color buffer.
			/// </summary>
			Dither = 3024,

			/// <summary>
			/// If enabled and no fragment shader is active,
			/// blend a fog color into the post texturing color.
			/// </summary>
			Fog = 2912,

			/// <summary>
			/// If enabled, histogram incoming RGBA color values.
			/// </summary>
			//Histogram,

			/// <summary>
			/// If enabled, apply the currently selected logical operation to the incoming index and color buffer indices.
			/// </summary>
			IndexLogicOp = 3057,

			/// <summary>
			/// If enabled, include light 0 in the evaluation of the lighting equation.
			/// </summary>
			Light0 = 16384,

			/// <summary>
			/// If enabled, include light 1 in the evaluation of the lighting equation.
			/// </summary>
			Light1,

			/// <summary>
			/// If enabled, include light 2 in the evaluation of the lighting equation.
			/// </summary>
			Light2,

			/// <summary>
			/// If enabled, include light 3 in the evaluation of the lighting equation.
			/// </summary>
			Light3,

			/// <summary>
			/// If enabled, include light 4 in the evaluation of the lighting equation.
			/// </summary>
			Light4,

			/// <summary>
			/// If enabled, include light 5 in the evaluation of the lighting equation.
			/// </summary>
			Light5,

			/// <summary>
			/// If enabled, include light 6 in the evaluation of the lighting equation.
			/// </summary>
			Light6,

			/// <summary>
			/// If enabled, include light 7 in the evaluation of the lighting equation.
			/// </summary>
			Light7,

			/// <summary>
			/// If enabled and no vertex shader is active,
			/// use the current lighting parameters to compute the vertex color or index.
			/// Otherwise, simply associate the current color or index with each vertex.
			/// </summary>
			Lighting = 2896,

			/// <summary>
			/// If enabled, draw lines with correct filtering.
			/// Otherwise, draw aliased lines.
			/// </summary>
			LineSmooth = 2848,

			/// <summary>
			/// If enabled, use the current line stipple pattern when drawing lines.
			/// </summary>
			LineStipple = 2852,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate RGBA values.
			/// </summary>
			Map1Color4 = 3472,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate color indices.
			/// </summary>
			Map1Index,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate normals.
			/// </summary>
			Map1Normal,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate s texture coordinates.
			/// </summary>
			Map1TextureCoord1,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate s and t texture coordinates.
			/// </summary>
			Map1TextureCoord2,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate s, t, and r texture coordinates.
			/// </summary>
			Map1TextureCoord3,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate s, t, r, and q texture coordinates.
			/// </summary>
			Map1TextureCoord4,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate x, y, and z vertex coordinates.
			/// </summary>
			Map1Vertex3,

			/// <summary>
			/// If enabled, calls to EvalCoord1, EvalMesh1, and EvalPoint1 generate x, y, z, and w vertex coordinates.
			/// </summary>
			Map1Vertex4,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate RGBA values.
			/// </summary>
			Map2Color4 = 3504,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate color indices.
			/// </summary>
			Map2Index,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate normals.
			/// </summary>
			Map2Normal,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate s texture coordinates.
			/// </summary>
			Map2TextureCoord1,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate s and t texture coordinates.
			/// </summary>
			Map2TextureCoord2,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate s, t, and r texture coordinates.
			/// </summary>
			Map2TextureCoord3,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate s, t, r, and q texture coordinates.
			/// </summary>
			Map2TextureCoord4,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate x, y, and z vertex coordinates.
			/// </summary>
			Map2Vertex3,

			/// <summary>
			/// If enabled, calls to EvalCoord2, EvalMesh2, and EvalPoint2 generate x, y, z, and w vertex coordinates.
			/// </summary>
			Map2Vertex4,

			/// <summary>
			/// If enabled, compute the minimum and maximum values of incoming RGBA color values.
			/// </summary>
			//MinMax,

			/// <summary>
			/// If enabled, use multiple fragment samples in computing the final color of a pixel.
			/// </summary>
			Multisample = 32925,

			/// <summary>
			/// If enabled an no vertex shader is active,
			/// normal vectors are normalized to unit length after transformation and before lighting.
			/// This method is generally less efficent than RescaleNormal.
			/// </summary>
			Normalize = 2977,

			/// <summary>
			/// If enabled, draw points with proper filtering.
			/// Otherwise, draw aliased points.
			/// </summary>
			PointSmooth = 2832,

			/// <summary>
			/// If enabled, calculate texture coordinates for points based on texture environment and point parameter settings.
			/// Otherwise texture coordinates are constant across points.
			/// </summary>
			//PointSprite,

			/// <summary>
			/// If enabled, and if the polygon is rendered in Fill mode,
			/// an offset is added to depth values of a polygon's fragments before the depth comparison is performed.
			/// </summary>
			PolygonOffsetFill = 32823,

			/// <summary>
			/// If enabled, and if the polygon is rendered in Fill mode,
			/// an offset is added to depth values of a polygon's fragments before the depth comparison is performed.
			/// </summary>
			PolygonOffsetLine = 10754,

			/// <summary>
			/// If enabled, an offset is added to depth values of a polygon's fragments before the depth comparison is performed,
			/// if the polygon is rendered in Point mode.
			/// </summary>
			PolygonOffsetPoint = 10753,

			/// <summary>
			/// If enabled, draw polygons with proper filtering.
			/// Otherwise, draw aliased polygons.
			/// For correct antialiased polygons,
			/// and alpha buffer is needed and the polygons must be sorted front to back.
			/// </summary>
			PolygonSmooth = 2881,

			/// <summary>
			/// If enabled, use the current polygon stipple pattern when rendering polygons.
			/// </summary>
			PolygonStipple = 2882,

			/// <summary>
			/// If enabled, perform a color table lookup on RGBA color values after color matrix transformation.
			/// </summary>
			//PostColorMatrixColorTable,

			/// <summary>
			/// If enabled, perform a color table lookup on RGBA values after convolution.
			/// </summary>
			//PostConvolutionColorTable,

			/// <summary>
			/// If enabled and no vertex shader is active,
			/// normal vectors are scaled after transformation and before lighting by a factor computed from the modelview matrix.
			/// If the modelview matrix scales space uniformly,
			/// this has the effect of restoring the transformed normal to unit length.
			/// This method is generally more efficient than Normalize.
			/// </summary>
			RescaleNormal = 32826,

			/// <summary>
			/// If enabled, compute a temporary coverage value where each bit is determined by the alpha value at the corresponding sample location.
			/// The temporary coverage value is then ANDed with the fragment coverage value.
			/// </summary>
			SampleAlphaToCoverage = 32926,

			/// <summary>
			/// If enabled, each sample alpha value is replaced by the maximum representable alpha value.
			/// </summary>
			SampleAlphaToOne = 32927,

			/// <summary>
			/// If enabled, the fragment's coverage is ANDed ith the temporary coverage value.
			/// If SampleCoverageInvert is set to true, invert the coverage value.
			/// </summary>
			SampleCoverage = 32928,

			/// <summary>
			/// If enabled, perform a two-dimensional convolution filter on incoming RGBA values.
			/// </summary>
			//Separable2d,

			/// <summary>
			/// If enabled, discard fragments that are outside the scissor rectangle.
			/// </summary>
			ScissorTest = 3089,

			/// <summary>
			/// If enabled, do stencil testing and update the stencil buffer.
			/// </summary>
			StencilTest = 2960,

			/// <summary>
			/// If enabled and no fragment shader is active, one-dimensional texturing is performed
			/// (unless two- or three-dimensional or cube-mapped texturing is also enabled).
			/// </summary>
			Texture1d = 3552,

			/// <summary>
			/// If enabled and no fragment shader is active, two-dimensional texturing is performed
			/// (unless three-dimensional or cube-mapped texturing is also enabled).
			/// </summary>
			Texture2d = 3553,

			/// <summary>
			/// If enabled and no fragment shader is active, one-dimensional texturing is performed
			/// (unless cube-mapped texturing is also enabled).
			/// </summary>
			Texture3d = 32879,

			/// <summary>
			/// If enabled and no fragment shader is active, cube-mapped texturing is performed.
			/// </summary>
			TextureCubeMap = 34067,

			/// <summary>
			/// If enabled and no vertex shader is active,
			/// the q texture coordinate is computed using the texture generation function devined with GL.TexGen.
			/// Otherwise, the current q texture coordinate is used.
			/// </summary>
			TextureGenQ = 3171,

			/// <summary>
			/// If enabled and no vertex shader is active,
			/// the q texture coordinate is computed using the texture generation function devined with GL.TexGen.
			/// Otherwise, the current r texture coordinate is used.
			/// </summary>
			TextureGenR = 3170,

			/// <summary>
			/// If enabled and no vertex shader is active,
			/// the q texture coordinate is computed using the texture generation function devined with GL.TexGen.
			/// Otherwise, the current s texture coordinate is used.
			/// </summary>
			TextureGenS = 3168,

			/// <summary>
			/// If enabled and no vertex shader is active,
			/// the q texture coordinate is computed using the texture generation function devined with GL.TexGen.
			/// Otherwise, the current t texture coordinate is used.
			/// </summary>
			TextureGenT = 3169,

			/// <summary>
			/// If enabled and a vertex shader is active,
			/// then the derived point size is taken from the (potentionally clipped) shader builtin gl_PointSize
			/// and clamped to the implementation-dependent point size range.
			/// </summary>
			//VertexProgramPointSize,

			/// <summary>
			/// If enabled and a vertex shader is active,
			/// it speicifies that the GL will choose between front and back colors based on the polygon's face direction
			/// of which the vertex being shaded is a part.
			/// It has no effect on points or lines.
			/// </summary>
			//VertexProgramTwoSide
		}
		#endregion

		#region Clear
		/// <summary>
		/// Buffers that can be cleared.
		/// </summary>
		[Flags]
		public enum ClearMask
		{
			/// <summary>
			/// Indicates the buffers currently enabled for color writing.
			/// </summary>
			ColorBuffer = 0x4000,

			/// <summary>
			/// Indicates the depth buffer.
			/// </summary>
			DepthBuffer = 0x100,

			/// <summary>
			/// Indicates the stencil buffer.
			/// </summary>
			StencilBuffer = 0x400,
		}
		#endregion

		#region Color
		/// <summary>
		/// Determines how source and destination blending factors are computed.
		/// </summary>
		public enum BlendFactor
		{
			Zero = 0,
			One = 1,
			SrcColor = 0x300,
			OneMinusSrcColor = 0x301,
			DstColor = 0x306,
			OneMinusDstColor = 0x307,
			SrcAlpha = 0x302,
			OneMinusSrcAlpha = 0x303,
			DstAlpha = 0x304,
			OneMinusDstAlpha = 0x305,
			//ConstantColor,
			//OneMinusConstantColor,
			//ConstantAlpha,
			//OneMinusConstantAlpha
		}
		#endregion

		#region Cull
		/// <summary>
		/// Culling mode values.
		/// </summary>
		public enum Cull {
			Front = 1028,

			Back = 1029,

			/// <summary>
			/// No facets are drawn,
			/// but other primitives such as points and lines are drawn.
			/// </summary>
			FrontAndBack = 1032
		}

		/// <summary>
		/// Specifies the orientation of front-facing polygons.
		/// </summary>
		public enum FrontFace
		{
			/// <summary>
			/// Clockwise.
			/// </summary>
			CW = 0x900,
			
			/// <summary>
			/// Counter-clockwise.
			/// </summary>
			CCW =0x901
		}
		#endregion

		#region Error
		/// <summary>
		/// An enumeration of OpenGL error types.
		/// </summary>
		public enum Error
		{
			/// <summary>
			/// No error has been recorded.
			/// The value of this symbolic constant is guaranteed to be 0.
			/// </summary>
			NoError = 0,

			/// <summary>
			/// An unacceptable value is specified for an enumerated argument.
			/// The offend command is ignored and has no other side effect than to set the error flag.
			/// </summary>
			InvalidEnum = 0x0500,

			/// <summary>
			/// A numeric argument is out of range.
			/// The offend command is ignored and has no other side effect than to set the error flag.
			/// </summary>
			InvalidValue = 0x0501,

			/// <summary>
			/// The specifies operation is not allowed in the current state.
			/// The offend command is ignored and has no other side effect than to set the error flag.
			/// </summary>
			InvalidOperation = 0x0502,

			/// <summary>
			/// An attempt has been made to perform an operation that would cause an internal stack to overflow.
			/// </summary>
			StackOverflow = 0x0503,

			/// <summary>
			/// An attempt has been made to perform an operation that would cause an internal stack to underflow.
			/// </summary>
			StackUnderflow = 0x0504,

			/// <summary>
			/// There is not enough memory left to execute the command.
			/// The state of the GL is undefined, except for the state of the error falgs, after this error is recorded.
			/// </summary>
			OutOfMemory = 0x0505,

			/// <summary>
			/// The framebuffer object is not complete.
			/// The offend command is ignored and has no other side effect than to set the error flag.
			/// </summary>
			InvalidFramebufferOperation = 0x0506,

			/// <summary>
			/// OpenGL 4.5
			/// </summary>
			ContextLost = 0x0507,

			/// <summary>
			/// Part of the ARB_imaging extension.
			/// Deprecated in 3.0 and removed in 3.1 core and above.
			/// </summary>
			TableTooLarge = 0x8031,
		}
		#endregion

		#region Framebuffer
		/// <summary>
		/// The target of a framebuffer operation.
		/// </summary>
		public enum FramebufferTarget
		{
			Framebuffer = 0x8D40,
			DrawFramebuffer = 0x8CA9,
			ReadFramebuffer = 0x8CA8,
		}

		/// <summary>
		/// The attachment point of a framebuffer.
		/// </summary>
		public enum FramebufferAttachment
		{
			Color0 = 0x8CE0,
			Color1,
			Color2,
			Color3,
			Color4,
			Color5,
			Color6,
			Color7,
			Color8,
			Color9,
			Color10,
			Color11,
			Color12,
			Color13,
			Color14,
			Color15,

			Depth = 0x8D00,

			Stencil = 0x8D20,

			/// <summary>
			/// Equivalent to attaching to the Depth and Stencil attachment points simultaneously.
			/// </summary>
			DepthStencil = 0x821A
		}

		/// <summary>
		/// The completeness status of a framebuffer object.
		/// </summary>
		public enum FramebufferStatus
		{
			/// <summary>
			/// Indicates an error occured.
			/// </summary>
			Zero = 0x0,

			/// <summary>
			/// The framebuffer is complete.
			/// </summary>
			Complete = 0x8CD5,

			/// <summary>
			/// The framebuffer is the default read or draw framebuffer,
			/// but the default framebuffer does not exist.
			/// </summary>
			Undefined = 0x8219,

			/// <summary>
			/// If any of the framebuffer attachment points are framebuffer incomplete.
			/// </summary>
			IncompleteAttachment = 0x8CD6,

			/// <summary>
			/// The framebuffer does not have at least one image attached to it.
			/// </summary>
			IncompleteMissingAttachment = 0x08CD7,

			/// <summary>
			/// The value of FramebufferAttachmentObjectType is None for any color attachment point(s).
			/// </summary>
			IncompleteDrawBuffer = 0x8CDB,

			/// <summary>
			/// ReadBuffer is not None and the value of FramebufferAttachmentObjectType is None
			/// for the color attachment point named by ReadBuffer.
			/// </summary>
			IncompleteReadBuffer = 0x8CDC,

			/// <summary>
			/// The combination of internal formats of the attached images violates an implementation-dependent set of restrictions.
			/// </summary>
			Unsupported = 0x8CDD,

			/// <summary>
			/// If the value of RenderbufferSamples is not the same for all attached renderbuffer,
			/// the value of TextureSamples is not the same for all attached textures;
			/// or, if the attached images are a mix of renderbuffer and textures,
			/// the value of RenderbufferSamples does not match the value of TextureSamples;
			/// or, if the value of TextureFixedSampleLocations is not the same for all attached textures;
			/// or, if the attached images are a mix of renderbuffers and textures,
			/// the value of TextureFixedSampleLocations is not true for all attached textures.
			/// </summary>
			IncompleteMultisample = 0x8D56,

			/// <summary>
			/// If any framebuffer attachment is layered, and any populated attachment is not layered,
			/// or if all populated color attachments are not from textures of the same target.
			/// </summary>
			IncompleteLayerTargets = 0x8DA8
		}
		#endregion

		#region Hint
		/// <summary>
		/// Enumeration of target values for GL.Hint.
		/// </summary>
		public enum HintTarget
		{
			/// <summary>
			/// Indicates the accuracy of fog calculation.
			/// If per-pixel fog calculation is not efficiently supported by the GL implementation,
			/// hinting HintMode.DontCare or HintMode.Fastest can result in per-vertex calculation of fog effects.
			/// </summary>
			Fog = 3156,

			/// <summary>
			/// Indicates the quality of filtering when generating mipmap images.
			/// </summary>
			GenerateMipMap = 33170,

			/// <summary>
			/// Indicates the sampling quality of antialiased lines.
			/// If a larger filter function is applied,
			/// hinting HintMode.Nicest can result in more pixel fragments being generated during rasterization.
			/// </summary>
			LineSmooth = 3154,

			/// <summary>
			/// Indicates the quality of color, texture coordinate, and fog coordinate interpolation.
			/// If perspective-corrected parameter interpolation is not efficiently supported by the GL implementation,
			/// hinting HintMode.DontCare or HintMode.Fastest can result in simple linear interpolation of colors and/or texture coordinates.
			/// </summary>
			PerspectiveCorrection = 3152,

			/// <summary>
			/// Indicates the sampling quality of antialiased points.
			/// If a larger filter function is applied,
			/// hinting HintMode.Nicest can result in more pixel fragments being generated during rasterization.
			/// </summary>
			PointSmooth = 3153,

			/// <summary>
			/// Indicates the sampling quality of antialiased polygons.
			/// Hinting GL.Nicest can result in more pixel fragments being generated during rasterization,
			/// if a larger filter function is applied.
			/// </summary>
			PolygonSmooth = 3155,

			/// <summary>
			/// Indicates the quality and performance of the compressing texture images.
			/// Hinting HintMode.Fastest indicates that texture images should be compressed as quickly as possible,
			/// while HintMode.Nicest indicates that texture images should be compressed with as little image quality loss as possible.
			/// HintMode.Nicest should be selected if the texture is to be retrieved by GL.GetCompressedTexImage for reuse.
			/// </summary>
			TextureCompression = 34031,

			/// <summary>
			/// Indicates the accuracy of the derivative calculation for the GL shading language fragment processing built-in functions:
			/// dFdx, dFdy, and fwidth.
			/// </summary>
			//FragmentShaderDerivative
		}

		/// <summary>
		/// Enumeration of mode values for GL.Hint.
		/// </summary>
		public enum HintMode
		{
			/// <summary>
			/// No preference.
			/// </summary>
			DontCare = 4352,

			/// <summary>
			/// The most efficient option should be chosen.
			/// </summary>
			Fastest = 4353,

			/// <summary>
			/// The most correct, or highest quality, option should be chosen.
			/// </summary>
			Nicest = 4354
		}
		#endregion

		#region Pixel
		/// <summary>
		/// The format of pixels within an image.
		/// (This is not a comperehensive enumuration of formats supported by OpenGL.)
		/// </summary>
		public enum PixelFormat
		{
			/// <summary>
			/// There is one unspecified color component.
			/// </summary>
			One = 1,

			/// <summary>
			/// There are two unspecified color components.
			/// </summary>
			Two = 2,

			/// <summary>
			/// There are three unspecified color components.
			/// </summary>
			Three = 3,

			/// <summary>
			/// There are four unspecified color components.
			/// </summary>
			Four = 4,

			/// <summary>
			/// Each element is an RGB triple.
			/// OpenGL converts it to floating point and assembles it into an RGBA element by attaching 1 for alpha.
			/// </summary>
			RGB = 0x1907,

			/// <summary>
			/// Each element is an BRG triple.
			/// OpenGL converts it to floating point and assembles it into an RGBA element by attaching 1 for alpha.
			/// </summary>
			BGR = 0x80E0,

			/// <summary>
			/// Each element contains all four components.
			/// </summary>
			RGBA = 0x1908,

			/// <summary>
			/// Each element contains all four components.
			/// </summary>
			BGRA = 0x80E1,
		}
		#endregion

		#region Primatives
		/// <summary>
		/// The kinds of geometric primatives.
		/// </summary>
		public enum Primitive
		{
			Points = 0x0,
			Lines,
			LineLoop,
			LineStrip,
			Triangles,
			TriangleStrip,
			TriangleFan,
			Quads,
			QuadStrip,
			Polygon
		}
		#endregion

		#region Shader
		/// <summary>
		/// Indicates the type of shader.
		/// </summary>
		public enum ShaderType
		{
			/// <summary>
			/// A shader that is intended to run on the programmable vertex processor.
			/// </summary>
			Vertex = 0x8B31,

			/// <summary>
			/// A shader that is intended to run on the programmable fragment shader.
			/// </summary>
			Fragment = 0x8B30,

			/// <summary>
			/// <para>A shader that is intended to run on the programmable compute processor.</para>
			/// <para>Requires OpenGL 4.3 or higher.</para>
			/// </summary>
			Compute = 0x91B9,

			/// <summary>
			/// <para>A shader that is intended to run on the programmable tessellation processor in the control stage.</para>
			/// <para>Requires OpenGL 4.0 or higher.</para>
			/// </summary>
			TessControl = 0x8E88,

			/// <summary>
			/// <para>A shader that is intended to run on the programmable geometry processor.</para>
			/// <para>Requires OpenGL 3.2 or higher.</para>
			/// </summary>
			Geometry = 0x8DD9,

			/// <summary>
			/// <para>A shader that is intended to run on the programmable tessellation processor in the evaluation stage.</para>
			/// <para>Requires OpenGL 4.0 or higher.</para>
			/// </summary>
			TessEvaluation = 0x8E87
		}

		/// <summary>
		/// The parameter for a shader object.
		/// </summary>
		public enum ShaderParameter
		{
			/// <summary>
			/// Returns the type of shader.
			/// </summary>
			ShaderType = 0x8B4F,

			/// <summary>
			/// Returns true if the shader is currently flagged for deletion,
			/// and false otherwise.
			/// </summary>
			DeleteStatus = 0x8B80,

			/// <summary>
			/// Returns true if the last compile operation on the shader was successful,
			/// and false otherwise.
			/// </summary>
			CompileStatus = 0x8B81,

			/// <summary>
			/// The number of characters in the information log for the shader
			/// including the null termination character.
			/// If the shader has no information log, a value of 0 is returned.
			/// </summary>
			InfoLogLength = 0x8B84,

			/// <summary>
			/// The length of the concatenation of the source strings that make up the shader source.
			/// If no source code exists, 0 is returned.
			/// </summary>
			ShaderSourceLength = 0x8B88
		}
		#endregion

		#region Texture
		/// <summary>
		/// A texture unit.
		/// The number of texture units is implementation dependent,
		/// but must be at least two.
		/// </summary>
		public enum TextureUnit
		{
			Texture0 = 33984,
			Texture1,
			Texture2,
			Texture3,
			Texture4,
			Texture5,
			Texture6,
			Texture7,
			Texture8,
			Texture9,
			Texture10,
			Texture11,
			Texture12,
			Texture13,
			Texture14,
			Texture15,
			Texture16,
			Texture17,
			Texture18,
			Texture19,
			Texture20,
			Texture21,
			Texture22,
			Texture23,
			Texture24,
			Texture25,
			Texture26,
			Texture27,
			Texture28,
			Texture29,
			Texture30,
			Texture31
		}

		public enum TextureTarget
		{
			Texture1d = 3552,
			//Texture1dArray,
			Texture2D = 3553,
			//Texture2dArray,
			//Texture2dMultisample,
			//Texture2dMultisampleArray,
			Texture3d = 32879,
			CubeMap = 34067,
			//CubeMapArray,
			//Rectangle
		}

		public enum TextureParameterName {
			//DepthStencilTextureMode,

			/// <summary>
			/// Specifies the index of the lowest defined mipmap level.
			/// This is an integer value.
			/// The initial value is 0.
			/// </summary>
			BaseLevel = 33084,

			/// <summary>
			/// Sets a border color.
			/// params contains four values that comprise the RGBA color of the texture border.
			/// Integer color components are interpreted linearly such that the most positive integer maps to 1.0,
			/// and the most negative integer maps to -1.0.
			/// The values are clamped to the range [0,1] when they are specified.
			/// Initially, the border color is (0, 0, 0, 0).
			/// </summary>
			BorderColor = 4100,

			/// <summary>
			/// Specifies the comparison operator used when CompareMode is set to CompareRToTexture
			/// </summary>
			CompareFunc = 34893,
			
			/// <summary>
			/// Specifies the texture comparison mode for currently bound depth textures.
			/// Permissible values are CompareRToTexture and None.
			/// </summary>
			CompareMode = 34892,

			/// <summary>
			/// params specifies a fixed bias value that is to be added to the level-of-detail parameter for the texture before texture sampling.
			/// The initial value is 0.0.
			/// </summary>
			LodBias = 34049,

			/// <summary>
			/// The texture minifying function is used whenever the pixel being textured maps to an area greater than one texture element.
			/// There are six defined minifying functions.
			/// Two of them use the nearest one or nearest four texture elements to compute the texture value.
			/// The other four use mipmaps.
			/// </summary>
			MinFilter = 10241,
			
			//MaxFilter,

			/// <summary>
			/// The texture magnification function is used when the pixel being textured maps to an area less than or equal to one texture element.
			/// It sets the texture magnification function to either Nearest or Linear.
			/// Nearest is generally faster than Linear,
			/// but it can produce textured images with sharper edges because the transition between texture elements is not as smooth.
			/// The initial value of MagFilter is Linear.
			/// </summary>
			MagFilter = 10240,

			/// <summary>
			/// Sets the minimum level-of-detail parameter.
			/// This floating-point value limits the selection of highest resolution mipmap (lowest mipmap level).
			/// The initial value is -1000.
			/// </summary>
			MinLod = 33082,

			/// <summary>
			/// Sets the maximum level-of-detail parameter.
			/// This floating-point value limits the selection of the lowest resolution mipmap (highest mipmap level).
			/// The initial value is 1000.
			/// </summary>
			MaxLod = 33083,

			/// <summary>
			/// Sets the index of the highest defined mipmap level.
			/// This is an integer value.
			/// The initial value is 1000.
			/// </summary>
			MaxLevel = 33085,
			//SwizzleR,
			//SwizzleG,
			//SwizzleB,
			//SwizzleA,

			/// <summary>
			/// Sets the wrap parameter for texture coordinate s to either Clamp, ClampToBorder, ClampToEdge, MirroredRepeat, or Repeat.
			/// </summary>
			WrapS = 10242,

			/// <summary>
			/// Sets the wrap parameter for texture coordinate t to either Clamp, ClampToBorder, ClampToEdge, MirroredRepeat, or Repeat.
			/// </summary>
			WrapT = 10243,

			/// <summary>
			/// Sets the wrap parameter for texture coordinate r to either Clamp, ClampToBorder, ClampToEdge, MirroredRepeat, or Repeat.
			/// </summary>
			WrapR = 32882,
		}

		/// <summary>
		/// Texture filtering value constants.
		/// </summary>
		public static class TextureFilter
		{
			/// <summary>
			/// Returns the value of the texture element that is nearest (in Manhattan distance) to the center of the pixel being textured.
			/// </summary>
			public const int Nearest = 9728;

			/// <summary>
			/// Returns the weighted average of the four texture elements that are closest to the center of the pixel being textured.
			/// These can include border texture elements, depending on the values of WrapS and WrapT, and on the exact mapping.
			/// </summary>
			public const int Linear = 9729;

			/// <summary>
			/// Chooses the mipmap that most closely matches the size of the pixel being textured and uses the Nearest criterion
			/// (the texture element nearest to the center of the pixel) to produce a texture value.
			/// </summary>
			public const int NearestMipmapNearest = 9984;

			/// <summary>
			/// Chooses the mipmap that most closely matches the size of the pixel being textured and uses the Linear criterion
			/// (a weighted average of the four texture elements that are closest to the center of the pixel) to produce a texture value.
			/// </summary>
			public const int LinearMipmapNearest = 9985;

			/// <summary>
			/// Chooses the two mipmaps that most closely match the size of the pixel being textured and uses the Nearest criterion
			/// (the texture element nearest to the center of the pixel) to produce a texture value from each mipmap.
			/// The final texture value is a weighted average of those two values.
			/// </summary>
			public const int NearestMipmapLinear = 9986;

			/// <summary>
			/// Chooses the two mipmaps that most closely match the size of the pixel being textured and uses the Linear criterion
			/// (a weighted average of the four texture elements that are closest to the center of the pixel) to produce a texture value from each mipmap.
			/// The final texture value is a weighted average of those two values.
			/// </summary>
			public const int LinearMipmapLinear = 9987;
		}

		/// <summary>
		/// Texture wrapping value constants.
		/// </summary>
		public static class Wrap
		{
			public const int Clamp = 10496;

			public const int ClampToBorder = 33069;

			public const int ClampToEdge = 33071;

			public const int MirroredRepeat = 33648;

			public const int Repeat = 10497;
		}
		#endregion

		#region Types
		/// <summary>
		/// Data type enumeration.
		/// </summary>
		public enum Type
		{
			Byte = 0x1400,
			UnsignedByte,
			Short,
			UnsignedShort,
			Int,
			UnsignedInt = 0x1405,
			Float,
			Bytes2,
			Bytes3,
			Bytes4,
			Double,
			/// <summary>
			/// Requires OpenGL 3.0 or higher.
			/// </summary>
			HalfFloat
		}
		#endregion
	}
}
#endregion
