﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace OpenGL
{
	internal static partial class GL
	{
		/// <summary>
		/// OpenGL extensions.
		/// </summary>
		public static class Ext
		{
			#region B
			/// <summary>
			/// Bind a framebuffer to a framebuffer target.
			/// </summary>
			/// <param name="target">Specifies the framebuffer target of the binding operation.</param>
			/// <param name="framebuffer">Specifies the name of the framebuffer object to bind.</param>
			public static void BindFramebuffer(GlEnum.FramebufferTarget target, uint framebuffer)
			{
				ptrBindFramebufferEXT(target, framebuffer);
			}

			/// <summary>
			/// <para>Bind a vertex array object.</para>
			/// </summary>
			/// <param name="array">Specifies the name of the vertex array to bind.</param>
			public static void BindVertexArray(uint array)
			{
				ptrBindVertexArrayEXT(array);
			}
			#endregion

			#region C
			/// <summary>
			/// <para>Check the completeness status of a framebuffer.</para>
			/// </summary>
			/// <param name="target">Specify the target to which the framebuffer is bound.</param>
			/// <returns>The completeness status of a framebuffer object when treated as a read or draw framebuffer,
			/// depending on the value of target.</returns>
			public static GlEnum.FramebufferStatus CheckFramebufferStatus(GlEnum.FramebufferTarget target)
			{
				return ptrCheckFramebufferStatusEXT(target);
			}
			#endregion

			#region D
			/// <summary>
			/// Delete named buffer objects.
			/// </summary>
			/// <param name="n">Specifies the number of framebuffer objects to be deleted.</param>
			/// <param name="framebuffers">A pointer to an array containing n framebuffer objects to be deleted.</param>
			public static void DeleteFramebuffers(int n, ref uint framebuffers)
			{
				ptrDeleteFramebuffersEXT(n, ref framebuffers);
			}

			/// <summary>
			/// Delete named buffer objects.
			/// </summary>
			/// <param name="n">Specifies the number of framebuffer objects to be deleted.</param>
			/// <param name="framebuffers">A pointer to an array containing n framebuffer objects to be deleted.</param>
			public static void DeleteFramebuffers(int n, uint[] framebuffers)
			{
				ptrDeleteFramebuffersEXT_Array(n, framebuffers);
			}

			/// <summary>
			/// Delete vertex array objects.
			/// </summary>
			/// <param name="n">Specifies the number of vertex array objects to be deleted.</param>
			/// <param name="arrays">Specifies the address of an array containing the n names of the objects to be deleted.</param>
			public static void DeleteVertexArrays(int n, ref uint arrays)
			{
				ptrDeleteVertexArraysEXT(n, ref arrays);
			}

			/// <summary>
			/// Delete vertex array objects.
			/// </summary>
			/// <param name="n">Specifies the number of vertex array objects to be deleted.</param>
			/// <param name="arrays">Specifies the address of an array containing the n names of the objects to be deleted.</param>
			public static void DeleteVertexArrays(int n, uint[] arrays)
			{
				ptrDeleteVertexArraysEXT_Array(n, arrays);
			}
			#endregion

			#region F
			/// <summary>
			/// Attach a level of a texture object as a logical buffer of a framebuffer object.
			/// </summary>
			/// <param name="target">Specifies the target to which the framebuffer is bound for all commands except NamedFramebufferTexture.</param>
			/// <param name="attachment">Specifies the attachment point of the framebuffer.</param>
			/// <param name="textarget">Specifies what type of texture is expected in the texture parameter.</param>
			/// <param name="texture">Specifies the name of an existing texture object to attach.</param>
			/// <param name="level">Specifies the mipmap level of the texture object to attach.</param>
			public static void FramebufferTexture1d(
				GlEnum.FramebufferTarget target,
				GlEnum.FramebufferAttachment attachment,
				GlEnum.TextureTarget textarget,
				uint texture,
				int level)
			{
				ptrFramebufferTexture1dEXT(target, attachment, textarget, texture, level);
			}
			
			/// <summary>
			/// Attach a level of a texture object as a logical buffer of a framebuffer object.
			/// </summary>
			/// <param name="target">Specifies the target to which the framebuffer is bound for all commands except NamedFramebufferTexture.</param>
			/// <param name="attachment">Specifies the attachment point of the framebuffer.</param>
			/// <param name="textarget">Specifies what type of texture is expected in the texture parameter.</param>
			/// <param name="texture">Specifies the name of an existing texture object to attach.</param>
			/// <param name="level">Specifies the mipmap level of the texture object to attach.</param>
			public static void FramebufferTexture2d(
				GlEnum.FramebufferTarget target,
				GlEnum.FramebufferAttachment attachment,
				GlEnum.TextureTarget textarget,
				uint texture,
				int level)
			{
				ptrFramebufferTexture2dEXT(target, attachment, textarget, texture, level);
			}

			/// <summary>
			/// Attach a level of a texture object as a logical buffer of a framebuffer object.
			/// </summary>
			/// <param name="target">Specifies the target to which the framebuffer is bound for all commands except NamedFramebufferTexture.</param>
			/// <param name="attachment">Specifies the attachment point of the framebuffer.</param>
			/// <param name="textarget">Specifies what type of texture is expected in the texture parameter.</param>
			/// <param name="texture">Specifies the name of an existing texture object to attach.</param>
			/// <param name="level">Specifies the mipmap level of the texture object to attach.</param>
			public static void FramebufferTexture3d(
				GlEnum.FramebufferTarget target,
				GlEnum.FramebufferAttachment attachment,
				GlEnum.TextureTarget textarget,
				uint texture,
				int level)
			{
				ptrFramebufferTexture3dEXT(target, attachment, textarget, texture, level);
			}
			#endregion

			#region G
			/// <summary>
			/// Generate framebuffer objects.
			/// </summary>
			/// <param name="n">Specifies the number of framebuffer object names to be generated.</param>
			/// <param name="framebuffers">Specifies an array in which the generated framebuffer object names are stored.</param>
			public static void GenFramebuffers(int n, out uint framebuffers)
			{
				ptrGenFramebuffersEXT(n, out framebuffers);
			}

			/// <summary>
			/// Generate framebuffer objects.
			/// </summary>
			/// <param name="n">Specifies the number of framebuffer object names to be generated.</param>
			/// <param name="framebuffers">Specifies an array in which the generated framebuffer object names are stored.</param>
			public static void GenFramebuffers(int n, out uint[] framebuffers)
			{
				ptrGenFramebuffersEXT_Array(n, out framebuffers);
			}

			/// <summary>
			/// <para>Generate vertex array object names.</para>
			/// <para>Requires OpenGL 3.0 or greater.</para>
			/// </summary>
			/// <param name="n">Specifies the number of vertex array object names to generate.</param>
			/// <param name="arrays">Specifies an array in which the generated vertex array object names are stored.</param>
			public static void GenVertexArrays(int n, out uint arrays)
			{
				ptrGenVertexArraysEXT(n, out arrays);
			}

			/// <summary>
			/// <para>Generate vertex array object names.</para>
			/// <para>Requires OpenGL 3.0 or greater.</para>
			/// </summary>
			/// <param name="n">Specifies the number of vertex array object names to generate.</param>
			/// <param name="arrays">Specifies an array in which the generated vertex array object names are stored.</param>
			public static void GenVertexArrays(int n, out uint[] arrays)
			{
				ptrGenVertexArraysEXT_Array(n, out arrays);
			}
			#endregion

			#region I
			/// <summary>
			/// Determine if a name corresponds to a framebuffer object.
			/// </summary>
			/// <param name="framebuffer">Specifies a value that may be the name of a framebuffer object.</param>
			/// <returns>True if framebuffer is currently the name of a framebuffer object.
			/// If framebuffer is zero, or is a non-zero value that is not currently the name of a framebuffer object,
			/// or if an error occurs, IsFramebuffer returns false.</returns>
			public static bool IsFramebuffer(IntPtr framebuffer)
			{
				return ptrIsFramebuffer(framebuffer);
			}
			#endregion
		}
	}
}
#endif