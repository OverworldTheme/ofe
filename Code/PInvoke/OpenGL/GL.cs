﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using VisionRiders.OFE;
using VisionRiders.OFE.Data;

namespace OpenGL
{
	/// <summary>
	/// A P/Invoke to the OpenGL graphics API.
	/// </summary>
	internal static partial class GL
	{
		#region Custom Functions
		/// <summary>
		/// Must be called before any actual GL calls,
		/// in order to get function pointers.
		/// </summary>
		public static void InitLibrary()
		{
			// Tell the debug log what we're going to do.
			DebugLog.Note("Getting OpenGL Function Addresses...");

			// On Windows, we need to hook up with the kernel real quick.
			// This is the only way for us to find OpenGL 1.1 extensions,
			// because that's how Microsoft rolls.
			if(SysInfo.Platform == OS.Windows)
			{
				WIN_OpenGlDllAddress = WIN_LoadLibraryA(WIN_OpenGlDll);
			}

			// Similarly, on OSX, we need to open access to the OpenGL library object.
			else if(SysInfo.Platform == OS.OSX)
			{				
				// (The 1 is for flag value RTLD_LAZY.)
				OSX_soImage = dlOpen("/System/Library/Frameworks/OpenGL.framework/Versions/Current/OpenGL", 1);
			}

			// Here we go, boys and girls.
			// Time to gather up all the OpenGL functions we need.
			// Because of course I would insist on doing this the hard way.

			getProcAddress(out ptrActiveTexture,  "glActiveTexture");
			getProcAddress(out ptrAttachShader, "glAttachShader");

			getProcAddress(out ptrBindBuffer, "glBindBuffer");
			getProcAddress(out ptrBindFramebuffer, "glBindFramebuffer");
			getProcAddress(out ptrBindFramebufferEXT, "glBindFramebufferEXT");
			getProcAddress(out ptrBindTexture, "glBindTexture");
			getProcAddress(out ptrBindVertexArray, "glBindVertexArray");
			getProcAddress(out ptrBindVertexArrayEXT, "glBindVertexArrayEXT");			
			getProcAddress(out ptrBlendFunc, "glBlendFunc");
			getProcAddress(out ptrBufferData_Float, "glBufferData");
			getProcAddress(out ptrBufferData_UInt, "glBufferData");

			getProcAddress(out ptrCheckFramebufferStatus, "glCheckFramebufferStatus");
			getProcAddress(out ptrCheckFramebufferStatusEXT, "glCheckFramebufferStatusEXT");
			getProcAddress(out ptrClear, "glClear");
			getProcAddress(out ptrClearColor, "glClearColor");
			getProcAddress(out ptrCompileShader, "glCompileShader");
			getProcAddress(out ptrCreateProgram, "glCreateProgram");
			getProcAddress(out ptrCreateShader, "glCreateShader");
			getProcAddress(out ptrCullFace, "glCullFace");			

			getProcAddress(out ptrDeleteBuffers, "glDeleteBuffers");
			getProcAddress(out ptrDeleteBuffers_Array, "glDeleteBuffers");
			getProcAddress(out ptrDeleteFramebuffers, "glDeleteFramebuffers");
			getProcAddress(out ptrDeleteFramebuffers_Array, "glDeleteFramebuffers");
			getProcAddress(out ptrDeleteFramebuffersEXT, "glDeleteFramebuffersEXT");
			getProcAddress(out ptrDeleteFramebuffersEXT_Array, "glDeleteFramebuffersEXT");
			getProcAddress(out ptrDeleteProgram, "glDeleteProgram");
			getProcAddress(out ptrDeleteShader, "glDeleteShader");
			getProcAddress(out ptrDeleteTextures, "glDeleteTextures");
			getProcAddress(out ptrDeleteTextures_Array, "glDeleteTextures");
			getProcAddress(out ptrDeleteVertexArrays, "glDeleteVertexArrays");
			getProcAddress(out ptrDeleteVertexArrays_Array, "glDeleteVertexArrays");
			getProcAddress(out ptrDeleteVertexArraysEXT, "glDeleteVertexArraysEXT");
			getProcAddress(out ptrDeleteVertexArraysEXT_Array, "glDeleteVertexArraysEXT");
			getProcAddress(out ptrDisableVertexAttribArray, "glDisableVertexAttribArray");
			getProcAddress(out ptrDrawElements, "glDrawElements");

			getProcAddress(out ptrEnableVertexAttribArray, "glEnableVertexAttribArray");
			
			getProcAddress(out ptrFramebufferTexture1d, "glFramebufferTexture1D");
			getProcAddress(out ptrFramebufferTexture1dEXT, "glFramebufferTexture1DEXT");
			getProcAddress(out ptrFramebufferTexture2d, "glFramebufferTexture2D");
			getProcAddress(out ptrFramebufferTexture2dEXT, "glFramebufferTexture2DEXT");
			getProcAddress(out ptrFramebufferTexture3d, "glFramebufferTexture3D");
			getProcAddress(out ptrFramebufferTexture3dEXT, "glFramebufferTexture3DEXT");
			getProcAddress(out ptrFrontFace, "glFrontFace");

			getProcAddress(out ptrGetAttribLocation, "glGetAttribLocation");
			getProcAddress(out ptrGetShaderiv, "glGetShaderiv");
			getProcAddress(out ptrGetShaderiv_Array, "glGetShaderiv");
			getProcAddress(out ptrGetShaderInfoLog, "glGetShaderInfoLog");
			getProcAddress(out ptrGetShaderSource, "glGetShaderSource");
			getProcAddress(out ptrGetTexImage, "glGetTexImage");
			getProcAddress(out ptrGetUniformLocation, "glGetUniformLocation");
			getProcAddress(out ptrGenBuffers, "glGenBuffers");
			getProcAddress(out ptrGenBuffers_Array, "glGenBuffers");
			getProcAddress(out ptrGenFramebuffers, "glGenFramebuffers");
			getProcAddress(out ptrGenFramebuffers_Array, "glGenFramebuffers");
			getProcAddress(out ptrGenFramebuffersEXT, "glGenFramebuffersEXT");
			getProcAddress(out ptrGenFramebuffersEXT_Array, "glGenFramebuffersEXT");
			getProcAddress(out ptrGenVertexArrays, "glGenVertexArrays");
			getProcAddress(out ptrGenVertexArrays_Array, "glGenVertexArrays");
			getProcAddress(out ptrGenVertexArraysEXT, "glGenVertexArraysEXT");
			getProcAddress(out ptrGenVertexArraysEXT_Array, "glGenVertexArraysEXT");

			getProcAddress(out ptrIsFramebuffer, "glIsFramebuffer");
			getProcAddress(out ptrIsFramebufferEXT, "glIsFramebufferEXT");

			getProcAddress(out ptrLinkProgram, "glLinkProgram");

			//getProcAddress(out ptrShaderSource, "glShaderSource");
			getProcAddress(out ptrShaderSource_Array, "glShaderSource");

			getProcAddress(out ptrTexImage2D, "glTexImage2D");
			
			getProcAddress(out ptrUniform1f, "glUniform1f");
			getProcAddress(out ptrUniform2f, "glUniform2f");
			getProcAddress(out ptrUniform3f, "glUniform3f");
			getProcAddress(out ptrUniform4f, "glUniform4f");
			getProcAddress(out ptrUniform1i, "glUniform1i");
			getProcAddress(out ptrUniform2i, "glUniform2i");
			getProcAddress(out ptrUniform3i, "glUniform3i");
			getProcAddress(out ptrUniform4i, "glUniform4i");
			getProcAddress(out ptrUniformMatrix2fv, "glUniformMatrix2fv");
			getProcAddress(out ptrUniformMatrix3fv, "glUniformMatrix3fv");
			getProcAddress(out ptrUniformMatrix4fv, "glUniformMatrix4fv");
			getProcAddress(out ptrUseProgram, "glUseProgram");

			getProcAddress(out ptrVertexAttribPointer, "glVertexAttribPointer");
		}

#if GLEW
		/// <summary>
		/// Initializes the OpenGL extension library.
		/// </summary>
		/// <returns>An error code on failure.</returns>
		//[DllImport(extLib, EntryPoint = "GlewInit", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
		//private extern static int glewInit();
#endif

		/// <summary>
		/// Queries the function pointer for the given OpenGL function.
		/// </summary>
		/// <param name="name">The exact, case-sensative name of the OpenGL function you want.</param>
		/// <returns>A pointer to the OpenGL function, or 0 if it cannot be found or there was some other error.</returns>
		private static void getProcAddress<T>(out T ptr, string name)
			where T : class
		{
			IntPtr address;

			// PInvoke the system for the process address.
			switch (SysInfo.Platform)
			{
				case OS.Windows:					
					address = WIN_wglGetProcAddress(name);

					// If we didn't find it there,
					// try the OpenGL 1.1 functions.
					if (address == IntPtr.Zero)
						address = WIN_GetProcAddress(WIN_OpenGlDllAddress, name);
					break;

				case OS.OSX:
					// Do a simple lookup.
					address = dlSym(OSX_soImage, name);
					break;

				default:
					VisionRiders.OFE.DebugLog.CriticalError("This platform is not supported by the engine's OpenGL implementation!");
					throw new NotSupportedException("The current platform is not supported by the engine's OpenGL implementation!");
			}

			// Did we find the function we were looking for?
			if (address == IntPtr.Zero)
			{
				DebugLog.Note("OpenGL implementation does not have the function " + name + ".");
				ptr = null;
			}
			else
			{
				// Store it as a delegate.
				ptr = (T)Convert.ChangeType(Marshal.GetDelegateForFunctionPointer(address, typeof(T)), typeof(T));
			}
		}

		#region Windows
		/// <summary>
		/// A handle to the OpenGL dll in Windows.
		/// Needed for calls to GetProcAddress on that platform.
		/// </summary>
		private static IntPtr WIN_OpenGlDllAddress;

		/// <summary>
		/// OpenGL dll in Windows.
		/// </summary>
		private const string WIN_OpenGlDll = "OpenGL32.dll";

		/// <summary>
		/// Kernel dll in Windows.
		/// </summary>
		private const string WIN_KernelDll = "Kernel32.dll";

		/// <summary>
		/// Loads the specified module into the address space of the calling process.
		/// The specified module may cause other modules to be loaded.
		/// (This is the ANSI version of the function, thus the A.
		/// It's needed to get the pointer for GetProcAddress in Windows.)
		/// </summary>
		/// <param name="filename">The name of the module.
		/// This can be either a library module (a .dll file) or an executable module (an .exe file). </param>
		/// <returns>If the function succeeds, the return value is a handle to the module.
		/// If the function fails, the return value is null.</returns>
		[DllImport(WIN_KernelDll, EntryPoint = "LoadLibraryA", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		private extern static IntPtr WIN_LoadLibraryA(string filename);

		/// <summary>
		/// Returns the address of an OpenGL extension function for use with the current OpenGL rendering context.
		/// </summary>
		/// <param name="name">The name of the extension function.
		/// The name of the extension function must be identical to a corresponding function implemented by OpenGL.</param>
		/// <returns>
		/// <para>When the function succeeds, the return value is the address of the extension function.</para>
		/// <para>When no current rendering context exists or the function fails, the return value is null.</para>
		/// </returns>
		[DllImport(WIN_OpenGlDll, EntryPoint = "wglGetProcAddress", ExactSpelling = true, CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi)]
		private extern static IntPtr WIN_wglGetProcAddress(string name);

		/// <summary>
		/// Retrieves the address of an exported function or variable from the specified dynamic-link library (DLL).
		/// (Needed to retrieve OpenGL 1.1 functions in Windows,
		/// because wglGetProcAddress will not find them.)
		/// </summary>
		/// <param name="module">A handle to the DLL module that contains the function or variable.</param>
		/// <param name="name">The function or variable name,
		/// or the function's ordinal value..</param>
		/// <returns>
		/// <para>When the function succeeds, the return value is the address of the extension function.</para>
		/// <para>When no current rendering context exists or the function fails, the return value is null.</para>
		/// </returns>
		[DllImport(WIN_KernelDll, EntryPoint = "GetProcAddress", ExactSpelling = true, CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi)]
		private extern static IntPtr WIN_GetProcAddress(IntPtr module, string name);
		#endregion

		#region OSX
		/// <summary>
		/// Handle for the OpenGL .so object on OSX.
		/// </summary>
		private static IntPtr OSX_soImage;

		private const string OSX_dynamicLoader = "ld.so";

		[DllImport(OSX_dynamicLoader, EntryPoint = "dlOpen", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
		private extern static IntPtr dlOpen(string filename, int flag);

		[DllImport(OSX_dynamicLoader, EntryPoint = "dlOpen", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
		private extern static IntPtr dlSym(IntPtr handle, string name);

		/// <summary>
		/// Close a dlopen object.
		/// </summary>
		/// <param name="handle"></param>
		/// <returns>If the referenced object was successfully closed, dlclose() shall return 0.
		/// If the object could not be closed, or if handle does not refer to an open object,
		/// dlclose() shall return a non-zero value.
		/// More detailed diagnostic information shall be available through dlerror().</returns>
		[DllImport(OSX_dynamicLoader, EntryPoint = "dlClose", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
		private extern static int dlClose(IntPtr handle);
		#endregion
		#endregion

		#region OpenGL Functions
		#region A
		/// <summary>
		/// Select active texture unit.
		/// </summary>
		/// <param name="texture">
		/// Specifies which texture unit to make active.
		/// The number of texture units is implementation dependent,
		/// but must be at least two.
		/// The initial value is Texture0.
		/// </param>
		public static void ActiveTexture(GlEnum.TextureUnit texture)
		{
			ptrActiveTexture(texture);
		}
		private delegate void glActiveTexture(GlEnum.TextureUnit texture);
		private static glActiveTexture ptrActiveTexture;

		public static void AttachShader(uint program, uint shader)
		{
			ptrAttachShader(program, shader);
		}
		private delegate void glAttachShader(uint program, uint shader);
		private static glAttachShader ptrAttachShader;
		#endregion

		#region B
		/// <summary>
		/// Binds a named buffer object.
		/// Requires OpenGL 1.5.
		/// </summary>
		/// <param name="target">Specifies the target to which the buffer object is bound.</param>
		/// <param name="buffer">Specifies the name of a buffer object.</param>
		public static void BindBuffer(GlEnum.BufferTarget target, uint buffer)
		{
			ptrBindBuffer(target, buffer);
		}
		private delegate void glBindBuffer(GlEnum.BufferTarget target, uint buffer);
		private static glBindBuffer ptrBindBuffer;

		/// <summary>
		/// Bind a framebuffer to a framebuffer target.
		/// </summary>
		/// <param name="target">Specifies the framebuffer target of the binding operation.</param>
		/// <param name="framebuffer">Specifies the name of the framebuffer object to bind.</param>
		public static void BindFramebuffer(GlEnum.FramebufferTarget target, uint framebuffer)
		{
			ptrBindFramebuffer(target, framebuffer);
		}
		private delegate void glBindFramebuffer(GlEnum.FramebufferTarget target, uint framebuffer);
    private static glBindFramebuffer ptrBindFramebuffer;
		private static glBindFramebuffer ptrBindFramebufferEXT;

		/// <summary>
		/// Bind a named texture to a texturing target.
		/// </summary>
		/// <param name="target">Specifies the target to which the texture is bound.</param>
		public static void BindTexture(GlEnum.TextureTarget target, uint texture)
		{
			ptrBindTexture(target, texture);
		}
		private delegate void glBindTexture(GlEnum.TextureTarget target, uint texture);
		private static glBindTexture ptrBindTexture;

		/// <summary>
		/// <para>Bind a vertex array object.</para>
		/// <para>Requires OpenGL 3.0 or higher.</para>
		/// </summary>
		/// <param name="array">Specifies the name of the vertex array to bind.</param>
		public static void BindVertexArray(uint array)
		{
			ptrBindVertexArray(array);
		}
		private delegate void glBindVertexArray(uint array);
		private static glBindVertexArray ptrBindVertexArray;
		private static glBindVertexArray ptrBindVertexArrayEXT;

		/// <summary>
		/// Specify pixel arithmetic.
		/// </summary>
		/// <param name="sfactor">Specifies how the red, green, blue, and alpha source blending factors are computed.
		/// The initial value is One.</param>
		/// <param name="dfactor">Specifies how the red, green, blue, and alpha destination blending factors are computer.
		/// The initial value is zero.</param>
		public static void BlendFunc(GlEnum.BlendFactor sfactor, GlEnum.BlendFactor dfactor)
		{
			ptrBlendFunc(sfactor, dfactor);
		}
		private delegate void glBlendFunc(GlEnum.BlendFactor sfactor, GlEnum.BlendFactor dfactor);
    private static glBlendFunc ptrBlendFunc;

		/// <summary>
		/// <para>Creates and initializes a buffer object's data store.</para>
		/// <para>Required OpenGL 1.5 or greater.</para>
		/// </summary>
		/// <param name="target">Specifies the target buffer object.</param>
		/// <param name="size">The size in bytes of the buffer object's new data store.</param>
		/// <param name="data">Specifies a pointer to data that will be copied into the data store for initialization,
		/// or null if no data is to be copied.</param>
		/// <param name="usage">Specifies the expected usaged pattern of the data store.</param>
		public static void BufferData(GlEnum.BufferTarget target, IntPtr size, float[] data, GlEnum.UsagePattern usage)
		{
			//byte[] result = new byte[data.Length * Marshal.SizeOf(typeof(T))];
			//Buffer.BlockCopy(data, 0, result, 0, result.Length);
			//ptrBufferData(target, size, Marshal.UnsafeAddrOfPinnedArrayElement(data, 0), usage);
			ptrBufferData_Float(target, size, data, usage);
		}
		private delegate void glBufferData_Float(
			GlEnum.BufferTarget target,
			IntPtr size,
			float[] data,
			GlEnum.UsagePattern usage);
    private static glBufferData_Float ptrBufferData_Float;

		/// <summary>
		/// <para>Creates and initializes a buffer object's data store.</para>
		/// <para>Required OpenGL 1.5 or greater.</para>
		/// </summary>
		/// <param name="target">Specifies the target buffer object.</param>
		/// <param name="size">The size in bytes of the buffer object's new data store.</param>
		/// <param name="data">Specifies a pointer to data that will be copied into the data store for initialization,
		/// or null if no data is to be copied.</param>
		/// <param name="usage">Specifies the expected usaged pattern of the data store.</param>
		public static void BufferData(GlEnum.BufferTarget target, IntPtr size, uint[] data, GlEnum.UsagePattern usage)
		{
			//byte[] result = new byte[data.Length * Marshal.SizeOf(typeof(T))];
			//Buffer.BlockCopy(data, 0, result, 0, result.Length);
			//ptrBufferData(target, size, Marshal.UnsafeAddrOfPinnedArrayElement(data, 0), usage);
			ptrBufferData_UInt(target, size, data, usage);
		}
		private delegate void glBufferData_UInt(
			GlEnum.BufferTarget target,
			IntPtr size,
			uint[] data,
			GlEnum.UsagePattern usage);
		private static glBufferData_UInt ptrBufferData_UInt;
		#endregion

		#region C
		/// <summary>
		/// <para>Check the completeness status of a framebuffer.</para>
		/// <para>Requires OpenGL 3.0 or higher.</para>
		/// </summary>
		/// <param name="target">Specify the target to which the framebuffer is bound.</param>
		/// <returns>The completeness status of a framebuffer object when treated as a read or draw framebuffer,
		/// depending on the value of target.</returns>
		public static GlEnum.FramebufferStatus CheckFramebufferStatus(GlEnum.FramebufferTarget target)
		{
			return ptrCheckFramebufferStatus(target);
		}
		private delegate GlEnum.FramebufferStatus glCheckFramebufferStatus(GlEnum.FramebufferTarget target);
		private static glCheckFramebufferStatus ptrCheckFramebufferStatus;
		private static glCheckFramebufferStatus ptrCheckFramebufferStatusEXT;

		/// <summary>
		/// Clear buffers to preset values.
		/// </summary>
		/// <param name="mask">Bitwise OR of masks that indicate the buffers to be cleared.</param>
		public static void Clear(GlEnum.ClearMask mask)
		{
			ptrClear(mask);
		}
		private delegate void glClear(GlEnum.ClearMask mask);
		private static glClear ptrClear;

		/// <summary>
		/// Specify clear values for the color buffers.
		/// </summary>
		/// <param name="red">Specify the red value used when the color buffers are cleared. The initial value is 0.</param>
		/// <param name="green">Specify the green value used when the color buffers are cleared. The initial value is 0.</param>
		/// <param name="blue">Specify the blue value used when the color buffers are cleared. The initial value is 0.</param>
		/// <param name="alpha">Specify the alpha value used when the color buffers are cleared. The initial value is 0.</param>
		public static void ClearColor(float red, float green, float blue, float alpha)
		{
			ptrClearColor(red, green, blue, alpha);
		}
		private delegate void glClearColor(float red, float green, float blue, float alpha);
		private static glClearColor ptrClearColor;

		/// <summary>
		/// <para>Compiles a shader object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the shader object to be compiled</param>
		public static void CompileShader(uint shader)
		{
			ptrCompileShader(shader);
		}
		private delegate void glCompileShader(uint shader);
		private static glCompileShader ptrCompileShader;

		/// <summary>
		/// <para>Creates a program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <returns>Creates an empty program object and returns a non-zero value by which it can be referenced.
		/// Returns 0 if an error occurs creating the program object.</returns>
		public static uint CreateProgram()
		{
			return ptrCreateProgram();
		}
		private delegate uint glCreateProgram();
		private static glCreateProgram ptrCreateProgram;

		/// <summary>
		/// <para>Creates a shader object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shaderType">Specifies the type of shader to be created.</param>
		/// <returns>Creates an empty shader object and returns a non-zero value by which it can be referenced.
		/// </returns>
		public static uint CreateShader(GlEnum.ShaderType shaderType)
		{
			return ptrCreateShader(shaderType);
		}
		private delegate uint glCreateShader(GlEnum.ShaderType shaderType);
		private static glCreateShader ptrCreateShader;

		/// <summary>
		/// Specify whether front- or back-facing facets can be culled.
		/// </summary>
		/// <param name="mode">Specifies whether front- or back-facing facets are candidates for culling.
		/// The initial value is Back.</param>
		public static void CullFace(GlEnum.Cull mode)
		{
			ptrCullFace(mode);
		}
		private delegate void glCullFace(GlEnum.Cull mode);
		private static glCullFace ptrCullFace;
		#endregion

		#region D
		/// <summary>
		/// Delete named buffer objects.
		/// </summary>
		/// <param name="n">Specifies the number of buffer objects to be deleted.</param>
		/// <param name="buffers">Specifies an array of buffer objects to be deleted.</param>
		public static void DeleteBuffers(int n, ref uint buffers)
		{
			ptrDeleteBuffers(n, ref buffers);
		}
		private delegate void glDeleteBuffers(int n, ref uint buffers);
		private static glDeleteBuffers ptrDeleteBuffers;

		/// <summary>
		/// Delete named buffer objects.
		/// </summary>
		/// <param name="n">Specifies the number of buffer objects to be deleted.</param>
		/// <param name="buffers">Specifies an array of buffer objects to be deleted.</param>
		public static void DeleteBuffers(int n, uint[] buffers)
		{
			ptrDeleteBuffers_Array(n, buffers);
		}
		private delegate void glDeleteBuffers_Array(int n, uint[] buffers);
		private static glDeleteBuffers_Array ptrDeleteBuffers_Array;

		/// <summary>
		/// Delete named buffer objects.
		/// </summary>
		/// <param name="n">Specifies the number of framebuffer objects to be deleted.</param>
		/// <param name="framebuffers">A pointer to an array containing n framebuffer objects to be deleted.</param>
		public static void DeleteFramebuffers(int n, ref uint framebuffers)
		{
			ptrDeleteFramebuffers(n, ref framebuffers);
		}
		private delegate void glDeleteFramebuffers(int n, ref uint buffers);
		private static glDeleteFramebuffers ptrDeleteFramebuffers;
		private static glDeleteFramebuffers ptrDeleteFramebuffersEXT;

		/// <summary>
		/// Delete named buffer objects.
		/// </summary>
		/// <param name="n">Specifies the number of framebuffer objects to be deleted.</param>
		/// <param name="framebuffers">A pointer to an array containing n framebuffer objects to be deleted.</param>
		public static void DeleteFramebuffers(int n, uint[] framebuffers)
		{
			ptrDeleteFramebuffers_Array(n, framebuffers);
		}
		private delegate void glDeleteFramebuffers_Array(int n, uint[] framebuffers);
		private static glDeleteFramebuffers_Array ptrDeleteFramebuffers_Array;
		private static glDeleteFramebuffers_Array ptrDeleteFramebuffersEXT_Array;

		/// <summary>
		/// <para>Deletes a program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="program">Specifies the program object to be deleted.</param>
		public static void DeleteProgram(uint program)
		{
			ptrDeleteProgram(program);
		}
		private delegate void glDeleteProgram(uint program);
		private static glDeleteProgram ptrDeleteProgram;

		/// <summary>
		/// <para>Deletes a shader object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the shader object to be deleted.</param>
		public static void DeleteShader(uint shader)
		{
			ptrDeleteShader(shader);
		}
		private delegate void glDeleteShader(uint shader);
		private static glDeleteShader ptrDeleteShader;

		/// <summary>
		/// <para>Delete named textures.</para>
		/// <para>Requires OpenGL 1.1 or greater.</para>
		/// </summary>
		/// <param name="n">Specifies the number of textures to be deleted.</param>
		/// <param name="textures">Specifies an array of textures to be deleted.</param>
		public static void DeleteTextures(int n, ref uint textures)
		{
			ptrDeleteTextures(n, ref textures);
		}
		private delegate void glDeleteTextures(int n, ref uint textures);
		private static glDeleteTextures ptrDeleteTextures;

		/// <summary>
		/// <para>Delete named textures.</para>
		/// <para>Requires OpenGL 1.1 or greater.</para>
		/// </summary>
		/// <param name="n">Specifies the number of textures to be deleted.</param>
		/// <param name="textures">Specifies an array of textures to be deleted.</param>
		public static void DeleteTextures(int n, ref uint[] textures)
		{
			ptrDeleteTextures_Array(n, ref textures);
		}
		private delegate void glDeleteTextures_Array(int n, ref uint[] textures);
		private static glDeleteTextures_Array ptrDeleteTextures_Array;

		/// <summary>
		/// Delete vertex array objects.
		/// </summary>
		/// <param name="n">Specifies the number of vertex array objects to be deleted.</param>
		/// <param name="arrays">Specifies the address of an array containing the n names of the objects to be deleted.</param>
		public static void DeleteVertexArrays(int n, ref uint arrays)
		{
			ptrDeleteVertexArrays(n, ref arrays);
		}
		private delegate void glDeleteVertexArrays(int n, ref uint arrays);
		private static glDeleteVertexArrays ptrDeleteVertexArrays;
		private static glDeleteVertexArrays ptrDeleteVertexArraysEXT;

		/// <summary>
		/// Delete vertex array objects.
		/// </summary>
		/// <param name="n">Specifies the number of vertex array objects to be deleted.</param>
		/// <param name="arrays">Specifies the address of an array containing the n names of the objects to be deleted.</param>
		public static void DeleteVertexArrays(int n, uint[] arrays)
		{
			ptrDeleteVertexArrays_Array(n, arrays);
		}
		private delegate void glDeleteVertexArrays_Array(int n, uint[] arrays);
		private static glDeleteVertexArrays_Array ptrDeleteVertexArrays_Array;
		private static glDeleteVertexArrays_Array ptrDeleteVertexArraysEXT_Array;

		/// <summary>
		/// Disables a server-side GL capabilities.
		/// </summary>
		/// <param name="cap">Specifies a symbolic constant indicating a GL capability.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glDisable", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void Disable(GlEnum.Capability cap);

		/// <summary>
		/// <para>Disable a generic vertex attribute array.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="index">Specifies the index of the generic vertex attribute to be disabled.</param>
		public static void DisableVertexAttribArray(int index)
		{
			ptrDisableVertexAttribArray(index);
		}
		private delegate void glDisableVertexAttribArray(int index);
		private static glDisableVertexAttribArray ptrDisableVertexAttribArray;

		/// <summary>
		/// <para>Render primitives from array data.</para>
		/// <para>Requires OpenGL 1.1 or higher.</para>
		/// </summary>
		/// <param name="mode">Specifies what kind of primitives to render.</param>
		/// <param name="count">Specifies the number of elements to be rendered.</param>
		/// <param name="type">Specifies the type of the values in indicies.
		/// Must be UnsignedByte, UnsignedShort, or UnsignedInt.</param>
		/// <param name="indices">Specifies a pointer to the location where the indices are stored.</param>
		public static void DrawElements(GlEnum.Primitive mode, int count, GlEnum.Type type, int indices)
		{
			ptrDrawElements(mode, count, type, indices);
		}
		private delegate void glDrawElements(GlEnum.Primitive mode, int count, GlEnum.Type type, int indices);
		private static glDrawElements ptrDrawElements;
		#endregion

		#region E
		/// <summary>
		/// Enables a server-side GL capabilities.
		/// </summary>
		/// <param name="cap">Specifies a symbolic constant indicating a GL capability.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glEnable", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void Enable(GlEnum.Capability cap);

		/// <summary>
		/// <para>Enable a generic vertex attribute array.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="index">Specifies the index of the generic vertex attribute to be enabled.</param>
		public static void EnableVertexAttribArray(int index)
		{
			ptrEnableVertexAttribArray(index);
		}
		private delegate void glEnableVertexAttribArray(int index);
		private static glEnableVertexAttribArray ptrEnableVertexAttribArray;
		#endregion

		#region F
		/// <summary>
		/// Attach a level of a texture object as a logical buffer of a framebuffer object.
		/// </summary>
		/// <param name="target">Specifies the target to which the framebuffer is bound for all commands except NamedFramebufferTexture.</param>
		/// <param name="attachment">Specifies the attachment point of the framebuffer.</param>
		/// <param name="textarget">Specifies what type of texture is expected in the texture parameter.</param>
		/// <param name="texture">Specifies the name of an existing texture object to attach.</param>
		/// <param name="level">Specifies the mipmap level of the texture object to attach.</param>
		public static void FramebufferTexture1d(
			GlEnum.FramebufferTarget target,
			GlEnum.FramebufferAttachment attachment,
			GlEnum.TextureTarget textarget,
			uint texture,
			int level)
		{
			ptrFramebufferTexture1d(target, attachment, textarget, texture, level);
		}
		private delegate void glFramebufferTexture1d(
			GlEnum.FramebufferTarget target,
			GlEnum.FramebufferAttachment attachment,
			GlEnum.TextureTarget textarget,
			uint texture,
			int level);
    private static glFramebufferTexture1d ptrFramebufferTexture1d;
		private static glFramebufferTexture1d ptrFramebufferTexture1dEXT;

		/// <summary>
		/// Attach a level of a texture object as a logical buffer of a framebuffer object.
		/// </summary>
		/// <param name="target">Specifies the target to which the framebuffer is bound for all commands except NamedFramebufferTexture.</param>
		/// <param name="attachment">Specifies the attachment point of the framebuffer.</param>
		/// <param name="textarget">Specifies what type of texture is expected in the texture parameter.</param>
		/// <param name="texture">Specifies the name of an existing texture object to attach.</param>
		/// <param name="level">Specifies the mipmap level of the texture object to attach.</param>
		public static void FramebufferTexture2d(
			GlEnum.FramebufferTarget target,
			GlEnum.FramebufferAttachment attachment,
			GlEnum.TextureTarget textarget,
			uint texture,
			int level)
		{
			ptrFramebufferTexture2d(target, attachment, textarget, texture, level);
		}
		private delegate void glFramebufferTexture2d(
			GlEnum.FramebufferTarget target,
			GlEnum.FramebufferAttachment attachment,
			GlEnum.TextureTarget textarget,
			uint texture,
			int level);
		private static glFramebufferTexture2d ptrFramebufferTexture2d;
		private static glFramebufferTexture2d ptrFramebufferTexture2dEXT;

		/// <summary>
		/// Attach a level of a texture object as a logical buffer of a framebuffer object.
		/// </summary>
		/// <param name="target">Specifies the target to which the framebuffer is bound for all commands except NamedFramebufferTexture.</param>
		/// <param name="attachment">Specifies the attachment point of the framebuffer.</param>
		/// <param name="textarget">Specifies what type of texture is expected in the texture parameter.</param>
		/// <param name="texture">Specifies the name of an existing texture object to attach.</param>
		/// <param name="level">Specifies the mipmap level of the texture object to attach.</param>
		public static void FramebufferTexture3d(
			GlEnum.FramebufferTarget target,
			GlEnum.FramebufferAttachment attachment,
			GlEnum.TextureTarget textarget,
			uint texture,
			int level)
		{
			ptrFramebufferTexture3d(target, attachment, textarget, texture, level);
		}
		private delegate void glFramebufferTexture3d(
			GlEnum.FramebufferTarget target,
			GlEnum.FramebufferAttachment attachment,
			GlEnum.TextureTarget textarget,
			uint texture,
			int level);
		private static glFramebufferTexture3d ptrFramebufferTexture3d;
		private static glFramebufferTexture3d ptrFramebufferTexture3dEXT;

		/// <summary>
		/// Define front- and back-facing polygons.
		/// </summary>
		/// <param name="mode">Specifies the orientation of front-facing polygons.
		/// The initial value is counter-clockwise.</param>
		public static void FrontFace(GlEnum.FrontFace mode)
		{
			ptrFrontFace(mode);
		}
		private delegate void glFrontFace(GlEnum.FrontFace mode);
		private static glFrontFace ptrFrontFace;
		#endregion

		#region G
		/// <summary>
		/// <para>Returns the location of an attribute variable.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="program">Specifies the program object to be queried.</param>
		/// <param name="name">Points to a null terminated string containing the name of the attribute variable whose location is to be queried.</param>
		/// <returns>The index of the generic vertex attribute that is bound to that attribute variable.</returns>
		public static int GetAttribLocation(uint program, string name)
		{
			return ptrGetAttribLocation(program, name);
		}
		private delegate int glGetAttribLocation(
			uint program,
			[MarshalAs(UnmanagedType.LPStr)]
			string name);
    private static glGetAttribLocation ptrGetAttribLocation;

		/// <summary>
		/// <para>Returns a parameter from a shader object. (The "iv" stands for integer vector.)</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the shader object to be queried.</param>
		/// <param name="pname">Specifies the object parameter.</param>
		/// <param name="parameters">Returns the requested object paramter.</param>
		public static void GetShaderIV(uint shader, GlEnum.ShaderParameter pname, out int parameters)
		{
			ptrGetShaderiv(shader, pname, out parameters);
		}
		private delegate void glGetShaderiv(uint shader, GlEnum.ShaderParameter pname, out int parameters);
		private static glGetShaderiv ptrGetShaderiv;

		/// <summary>
		/// <para>Returns a parameter from a shader object. (The "iv" stands for integer vector.)</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the shader object to be queried.</param>
		/// <param name="pname">Specifies the object parameter.</param>
		/// <param name="parameters">Returns the requested object paramter.</param>
		public static void GetShaderIV(uint shader, GlEnum.ShaderParameter pname, out int[] parameters)
		{
			ptrGetShaderiv_Array(shader, pname, out parameters);
		}
		private delegate void glGetShaderiv_Array(uint shader, GlEnum.ShaderParameter pname, out int[] parameters);
		private static glGetShaderiv_Array ptrGetShaderiv_Array;

		/// <summary>
		/// <para>Returns the information log for a shader object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the shader object whose information log is to be queried.</param>
		/// <param name="maxLength">Specifies the size of the character buffer for storing the returned information log.</param>
		/// <param name="length">Returns the length of the string returned in infoLog (excluding the null terminator).</param>
		/// <param name="infolog">Specifies an array of characters that is used to return the information log.</param>
		public static void GetShaderInfoLog(uint shader, int maxLength, out int length, out string infoLog)
		{
			StringBuilder build = new StringBuilder(maxLength);
			ptrGetShaderInfoLog(shader, maxLength, out length, build);
			infoLog = build.ToString();
		}
		private delegate void glGetShaderInfoLog(
			uint shader,
			int maxLength,
			out int length,
			StringBuilder infoLog);
		private static glGetShaderInfoLog ptrGetShaderInfoLog;

		/// <summary>
		/// <para>Returns the source code string from a shader object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the shader object to be queried.</param>
		/// <param name="bufSize">Specifies the size of the character buffer for storing the returned source code string.</param>
		/// <param name="length">Returns the length of the string returned in source (excluding the null terminator).</param>
		/// <param name="source">Specifies an array of characters that is used to return the source code string.</param>
		public static void GetShaderSource(uint shader, int bufSize, out int length, out string source)
		{
			StringBuilder build = new StringBuilder(bufSize);
			ptrGetShaderSource(shader, bufSize, out length, build);
			source = build.ToString();
		}
		private delegate void glGetShaderSource(
			uint shader,
			int bufSize,
			out int length,
			StringBuilder source);
		private static glGetShaderSource ptrGetShaderSource;

		/// <summary>
		/// Return a texture image.
		/// </summary>
		/// <param name="target">Specifies which texture is to be obtained.</param>
		/// <param name="level">Specifies the level-of-detail number of the desired image.
		/// Level 0 is the base image level.
		/// Level n is the nth mipmap reduction image.</param>
		/// <param name="format">Specifies a pixel format for the returned data.
		/// The supported formats are:
		/// Red, Green, Blue, Alpha, RGB, BGR, RGBA, BGRA, Luminance, and LuminanceAlpha.</param>
		/// <param name="type">Specifies a pixel type for the returned data.</param>
		/// <param name="img">Returns the texture image.
		/// Should be a pointer to an array of the type specified by type.</param>
		public static void GetTexImage(GlEnum.TextureTarget target, int level, GlEnum.PixelFormat format, GlEnum.Type type, IntPtr img)
		{
			ptrGetTexImage(target, level, format, type, img);
		}
		private delegate void glGetTexImage(GlEnum.TextureTarget target, int level, GlEnum.PixelFormat format, GlEnum.Type type, IntPtr img);
		private static glGetTexImage ptrGetTexImage;

		/// <summary>
		/// <para>Returns the location of a uniform variable.</para>
		/// </summary>
		/// <param name="program">Specifies the program object to be queried.</param>
		/// <param name="name">Points to a null terminated string
		/// containing the name of the uniform variable whose location is to be quieried.</param>
		/// <returns>An intefer that repesents the location of a specific uniform variable within a program object.
		/// Returns -1 if name does not correspond to an active uniform variable in program
		/// or if name starts with the reserved prefix gl_</returns>
		public static int GetUniformLocation(uint program, string name)
		{
			return ptrGetUniformLocation(program, name);
		}
		private delegate int glGetUniformLocation(
			uint program,
			[MarshalAs(UnmanagedType.LPStr)]
			string name);
		private static glGetUniformLocation ptrGetUniformLocation;

		/// <summary>
		/// Generates buffer object names.
		/// </summary>
		/// <param name="n">Specified the number of buffer object names to be generated.</param>
		/// <param name="buffers">Specifies an array in which the generated buffer object names are stored.</param>
		public static void GenBuffers(int n, out uint buffers)
		{
			ptrGenBuffers(n, out buffers);
		}
		private delegate void glGenBuffers(int n, out uint buffers);
		private static glGenBuffers ptrGenBuffers;

		/// <summary>
		/// Generates buffer object names.
		/// </summary>
		/// <param name="n">Specified the number of buffer object names to be generated.</param>
		/// <param name="buffers">Specifies an array in which the generated buffer object names are stored.</param>
		public static void GenBuffers(int n, out uint[] buffers)
		{
			ptrGenBuffers_Array(n, out buffers);
		}
		private delegate void glGenBuffers_Array(int n, out uint[] buffers);
		private static glGenBuffers_Array ptrGenBuffers_Array;

		/// <summary>
		/// Generate framebuffer objects.
		/// </summary>
		/// <param name="n">Specifies the number of framebuffer object names to be generated.</param>
		/// <param name="framebuffers">Specifies an array in which the generated framebuffer object names are stored.</param>
		public static void GenFramebuffers(int n, out uint framebuffers)
		{
			ptrGenFramebuffers(n, out framebuffers);
		}
		private delegate void glGenFramebuffers(int n, out uint framebuffers);
		private static glGenFramebuffers ptrGenFramebuffers;
		private static glGenFramebuffers ptrGenFramebuffersEXT;

		/// <summary>
		/// Generate framebuffer objects.
		/// </summary>
		/// <param name="n">Specifies the number of framebuffer object names to be generated.</param>
		/// <param name="framebuffers">Specifies an array in which the generated framebuffer object names are stored.</param>
		public static void GenFramebuffers(int n, out uint[] framebuffers)
		{
			ptrGenFramebuffers_Array(n, out framebuffers);
		}
		private delegate void glGenFramebuffers_Array(int n, out uint[] framebuffers);
		private static glGenFramebuffers_Array ptrGenFramebuffers_Array;
		private static glGenFramebuffers_Array ptrGenFramebuffersEXT_Array;
		//[DllImport(glLib, EntryPoint = "glGenFramebuffers", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		//public extern static void GenFrameBuffers(int n, IntPtr[] framebuffers);

		/// <summary>
		/// Generate texture names.
		/// </summary>
		/// <param name="n">Specifies the number of texture names to be generated.</param>
		/// <param name="textures">Specifies an array in which the generated texture names are stored.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glGenTextures", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void GenTextures(int n, out uint textures);

		/// <summary>
		/// Generate texture names.
		/// </summary>
		/// <param name="n">Specifies the number of texture names to be generated.</param>
		/// <param name="textures">Specifies an array in which the generated texture names are stored.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glGenTextures", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void GenTextures(int n, out uint[] textures);

		/// <summary>
		/// <para>Generate vertex array object names.</para>
		/// <para>Requires OpenGL 3.0 or greater.</para>
		/// </summary>
		/// <param name="n">Specifies the number of vertex array object names to generate.</param>
		/// <param name="arrays">Specifies an array in which the generated vertex array object names are stored.</param>
		public static void GenVertexArrays(int n, out uint arrays)
		{
			ptrGenVertexArrays(n, out arrays);
		}
		private delegate void glGenVertexArrays(int n, out uint arrays);
		private static glGenVertexArrays ptrGenVertexArrays;
		private static glGenVertexArrays ptrGenVertexArraysEXT;

		/// <summary>
		/// <para>Generate vertex array object names.</para>
		/// <para>Requires OpenGL 3.0 or greater.</para>
		/// </summary>
		/// <param name="n">Specifies the number of vertex array object names to generate.</param>
		/// <param name="arrays">Specifies an array in which the generated vertex array object names are stored.</param>
		public static void GenVertexArrays(int n, out uint[] arrays)
		{
			ptrGenVertexArrays_Array(n, out arrays);
		}
		private delegate void glGenVertexArrays_Array(int n, out uint[] arrays);
		private static glGenVertexArrays_Array ptrGenVertexArrays_Array;
		private static glGenVertexArrays_Array ptrGenVertexArraysEXT_Array;

		/// <summary>
		/// Returns error information.
		/// </summary>
		/// <returns></returns>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glGetError", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static GlEnum.Error GetError();

		/// <summary>
		/// Returns a string describing the current GL connection.
		/// </summary>
		/// <param name="name">Specifies a symbolic constant,
		/// one of Vender, Renderer, Version, or ShadingLanguageVersion.</param>
		/// <param name="index">Specifies the index of the string to return.</param>
		/// <returns>If an error is generated, the function returns an empty string.</returns>
		public static string GetString(GlEnum.Name name) { return Marshal.PtrToStringAnsi(getString(name)); }
		[DllImport(WIN_OpenGlDll, EntryPoint = "glGetString", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		private extern static IntPtr getString(GlEnum.Name name);

		/// <summary>
		/// Returns a string describing the current GL connection.
		/// </summary>
		/// <param name="name">Specifies a symbolic constant,
		/// one of Vender, Renderer, Version, ShadingLanguageVersion, or Extensions.</param>
		/// <param name="index">Specifies the index of the string to return.</param>
		/// <returns>If an error is generated, the function returns and empty string.</returns>
		public static string GetStringI(GlEnum.Name name, UInt32 index) { return Marshal.PtrToStringAnsi(getStringI(name, index)); }
		[DllImport(WIN_OpenGlDll, EntryPoint = "glGetStringi", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		private extern static IntPtr getStringI(GlEnum.Name name, UInt32 index);
		#endregion

		#region H
		/// <summary>
		/// Specify implementation-specific hints.
		/// </summary>
		/// <param name="target">Specifies a symbolic constant indicating the behaviour to be controlled.</param>
		/// <param name="mode">Specifies a symbolic constant indicating the desired behaviour.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glHint", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void Hint(GlEnum.HintTarget target, GlEnum.HintMode mode);
		#endregion

		#region I
		/// <summary>
		/// Determine if a name corresponds to a framebuffer object.
		/// </summary>
		/// <param name="framebuffer">Specifies a value that may be the name of a framebuffer object.</param>
		/// <returns>True if framebuffer is currently the name of a framebuffer object.
		/// If framebuffer is zero, or is a non-zero value that is not currently the name of a framebuffer object,
		/// or if an error occurs, IsFramebuffer returns false.</returns>
		public static bool IsFramebuffer(IntPtr framebuffer)
		{
			return ptrIsFramebuffer(framebuffer);
		}
		private delegate bool glIsFramebuffer(IntPtr framebuffer);
		private static glIsFramebuffer ptrIsFramebuffer;
		private static glIsFramebuffer ptrIsFramebufferEXT;
		#endregion

		#region L
		/// <summary>
		/// <para>Links a program object.</para>
		/// <para>Required OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="program">Specifies the handle of the program object to be linked.</param>
		public static void LinkProgram(uint program)
		{
			ptrLinkProgram(program);
		}
		private delegate void glLinkProgram(uint program);
		private static glLinkProgram ptrLinkProgram;
		#endregion

		#region S
		/// <summary>
		/// <para>Replaces the source code in a shader object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the handle of the shader object whose source code is to be replaced.</param>
		/// <param name="count">Specifies the number of elements in the string and length arrays.</param>
		/// <param name="glString">Specifies an array of pointers to strings containing the source code to be loaded into the shader.</param>
		/// <param name="length">Specifies an array of string lengths.</param>
		/*public static void ShaderSource(uint shader, int count, string glString, int length)
		{
			ptrShaderSource(shader, count, glString, length);
		}
		private delegate void glShaderSource(
			uint shader,
			int count,
			[MarshalAs(UnmanagedType.LPStr)]
			string[] glString,
			int length);
		private static glShaderSource ptrShaderSource;*/

		/// <summary>
		/// <para>Replaces the source code in a shader object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="shader">Specifies the handle of the shader object whose source code is to be replaced.</param>
		/// <param name="count">Specifies the number of elements in the string and length arrays.</param>
		/// <param name="glString">Specifies an array of pointers to strings containing the source code to be loaded into the shader.
		/// (Just called "string" by OpenGL, but they're using C where that name is not reserved.)</param>
		/// <param name="length">Specifies an array of string lengths.</param>
		public static void ShaderSource(uint shader, int count, string[] glString, int[] length)
		{
			/*Array[] strings = new Array[count];
			int totalLength = 0;
			for (int i = 0; i < count; i++) {
				strings[i] = Encoding.UTF8.GetBytes(glString[i]);
				length[i] = strings[i].Length;
				totalLength += length[i];
			}

			byte[] bytes = new byte[totalLength];
			int a = 0;
			for(int i = 0;i< count; i++)
			{
				for(int j= 0; j< length[i]; j++)
				{
					bytes[a] = (byte)strings[i].GetValue(j);
				}
				a++;
			}*/

			IntPtr[] sPtr = new IntPtr[count];
			for (int i = 0; i < count; i++)
				sPtr[i] = Marshal.StringToHGlobalAnsi(glString[i]);

			ptrShaderSource_Array(shader, count, sPtr, length);
		}
		private delegate void glShaderSource_Array(
			uint shader,
			int count,
			[MarshalAs(UnmanagedType.LPArray)]
			IntPtr[] glString,
			int[] length);
		private static glShaderSource_Array ptrShaderSource_Array;
		#endregion

		#region T
		/// <summary>
		/// <para>Specify a two-dimensional texture image.</para>
		/// <para>Requires OpenGL 1.1 or greater.</para>
		/// </summary>
		/// <param name="target">Specifies the target texture.</param>
		/// <param name="level">Specifies the level-of-detail number.
		/// Level 0 is the base image level.
		/// Leven n is the nth mipmap reduction image.</param>
		/// <param name="internalFormat">Specifies the number of color components in the texture.</param>
		/// <param name="width">Specifies the width of the texture image including the border if any.</param>
		/// <param name="height">Specifes the height of the texture image including the border it any.</param>
		/// <param name="border">Specifies the width of the border. Must be 0 or 1.</param>
		/// <param name="format">Specifies the format of the pixel data.
		/// The following symbolic values are accepted:
		/// ColorIndex, Red, Green, Blue, Alpha, RGB, BGR, RGBA, BGRA, Luminance, and LuminanceAlpha.</param>
		/// <param name="type">Specifies the data type of the pixel data.</param>
		/// <param name="data">Specifies a pointer to the image data in memory.</param>
		public static void TexImage2D(
			GlEnum.TextureTarget target,
			int level,
			GlEnum.PixelFormat internalFormat,
			int width,
			int height,
			int border,
			GlEnum.PixelFormat format,
			GlEnum.Type type,
			IntPtr data)
		{
			ptrTexImage2D(target, level, internalFormat, width, height, border, format, type, data);
		}
		private delegate void glTexImage2D(
			GlEnum.TextureTarget target,
			int level,
			GlEnum.PixelFormat internalFormat,
			int width,
			int height,
			int border,
			GlEnum.PixelFormat format,
			GlEnum.Type type,
			IntPtr data);
		private static glTexImage2D ptrTexImage2D;

		/// <summary>
		/// Sets texture parameters.
		/// </summary>
		/// <param name="target">Specifies the target to which the texture is bound for TexParameter functions.</param>
		/// <param name="pname">Specifies the symbolic name of a single-valued texture parameter.</param>
		/// <param name="param">Specifies the value of pname.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glTexParameterf", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void TexParameterF(GlEnum.TextureTarget target, GlEnum.TextureParameterName pname, float param);

		/// <summary>
		/// Sets texture parameters.
		/// </summary>
		/// <param name="target">Specifies the target to which the texture is bound for TexParameter functions.</param>
		/// <param name="pname">Specifies the symbolic name of a single-valued texture parameter.</param>
		/// <param name="param">Specifies the value of pname.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glTexParameteri", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void TexParameterI(GlEnum.TextureTarget target, GlEnum.TextureParameterName pname, int param);
		#endregion

		#region U
		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0"></param>
		public static void Uniform1f(int location, float v0)
		{
			ptrUniform1f(location, v0);
		}
		public delegate void glUniform1f(int location, float v0);
		public static glUniform1f ptrUniform1f;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v1">Specifies the new value to be used for the specified uniform variable.</param>
		public static void Uniform2f(int location, float v0, float v1)
		{
			ptrUniform2f(location, v0, v1);
		}
		public delegate void glUniform2f(int location, float v0, float v1);
		public static glUniform2f ptrUniform2f;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v1">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v2">Specifies the new value to be used for the specified uniform variable.</param>
		public static void Uniform3f(int location, float v0, float v1, float v2)
		{
			ptrUniform3f(location, v0, v1, v2);
		}
		public delegate void glUniform3f(int location, float v0, float v1, float v2);
		public static glUniform3f ptrUniform3f;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v1">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v2">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v3">Specifies the new value to be used for the specified uniform variable.</param>
		public static void Uniform4f(int location, float v0, float v1, float v2, float v3)
		{
			ptrUniform4f(location, v0, v1, v2, v3);
		}
		public delegate void glUniform4f(int location, float v0, float v1, float v2, float v3);
		public static glUniform4f ptrUniform4f;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0"></param>
		public static void Uniform1i(int location, int v0)
		{
			ptrUniform1i(location, v0);
		}
		public delegate void glUniform1i(int location, int v0);
		public static glUniform1i ptrUniform1i;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v1">Specifies the new value to be used for the specified uniform variable.</param>
		public static void Uniform2i(int location, int v0, int v1)
		{
			ptrUniform2i(location, v0, v1);
		}
		public delegate void glUniform2i(int location, int v0, int v1);
		public static glUniform2i ptrUniform2i;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v1">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v2">Specifies the new value to be used for the specified uniform variable.</param>
		public static void Uniform3i(int location, int v0, int v1, int v2)
		{
			ptrUniform3i(location, v0, v1, v2);
		}
		public delegate void glUniform3i(int location, int v0, int v1, int v2);
		public static glUniform3i ptrUniform3i;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform variable to be modified.</param>
		/// <param name="v0">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v1">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v2">Specifies the new value to be used for the specified uniform variable.</param>
		/// <param name="v3">Specifies the new value to be used for the specified uniform variable.</param>
		public static void Uniform4i(int location, int v0, int v1, int v2, int v3)
		{
			ptrUniform4i(location, v0, v1, v2, v3);
		}
		public delegate void glUniform4i(int location, int v0, int v1, int v2, int v3);
		public static glUniform4i ptrUniform4i;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.1 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform value to be modified.</param>
		/// <param name="count">Specifies the number of matrices that are to be modified.
		/// This should be 1 if the target uniform variable is not an array of matrices,
		/// and 1 or more if it is an array of matricies.</param>
		/// <param name="transpose">Specifies whether to transpose the matrix as the values are loaded into the uniform variable.</param>
		/// <param name="value">Specifies a pointer to an array of count values that will be used to update the specified uniform value.</param>
		public static void UniformMatrix2fv(int location, int count, bool transpose, ref float[] value)
		{
			ptrUniformMatrix2fv(location, count, transpose, ref value);
		}
		private delegate void glUniformMatrix2fv(int location, int count, bool transpose, ref float[] value);
		private static glUniformMatrix2fv ptrUniformMatrix2fv;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.1 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform value to be modified.</param>
		/// <param name="count">Specifies the number of matrices that are to be modified.
		/// This should be 1 if the target uniform variable is not an array of matrices,
		/// and 1 or more if it is an array of matricies.</param>
		/// <param name="transpose">Specifies whether to transpose the matrix as the values are loaded into the uniform variable.</param>
		/// <param name="value">Specifies a pointer to an array of count values that will be used to update the specified uniform value.</param>
		public static void UniformMatrix3fv(int location, int count, bool transpose, ref float[] value)
		{
			ptrUniformMatrix3fv(location, count, transpose, ref value);
		}
		private delegate void glUniformMatrix3fv(int location, int count, bool transpose, ref float[] value);
		private static glUniformMatrix3fv ptrUniformMatrix3fv;

		/// <summary>
		/// <para>Specify the value of a uniform variable for the current program object.</para>
		/// <para>Requires OpenGL 2.1 or greater.</para>
		/// </summary>
		/// <param name="location">Specifies the location of the uniform value to be modified.</param>
		/// <param name="count">Specifies the number of matrices that are to be modified.
		/// This should be 1 if the target uniform variable is not an array of matrices,
		/// and 1 or more if it is an array of matricies.</param>
		/// <param name="transpose">Specifies whether to transpose the matrix as the values are loaded into the uniform variable.
		/// If true, each matrix is assumed to be supplied in row major order.</param>
		/// <param name="value">Specifies a pointer to an array of count values that will be used to update the specified uniform value.</param>
		public static void UniformMatrix4fv(int location, int count, bool transpose, ref float[] value)
		{
			ptrUniformMatrix4fv(location, count, transpose, ref value);
		}
		private delegate void glUniformMatrix4fv(int location, int count, bool transpose, ref float[] value);
		private static glUniformMatrix4fv ptrUniformMatrix4fv;

		/// <summary>
		/// Installs a program object as part of current rendering state.
		/// </summary>
		/// <param name="program">Specifies the handle of the program object whose executables are to be used as part of current rendering state.</param>
		public static void UseProgram(uint program)
		{
			ptrUseProgram(program);
		}
		private delegate void glUseProgram(uint program);
		private static glUseProgram ptrUseProgram;
		#endregion

		#region V
		/// <summary>
		/// <para>Define an array of generic vertex attribute data.</para>
		/// <para>Requires OpenGL 2.0 or greater.</para>
		/// </summary>
		/// <param name="index">Specifies the index of the generic vertex attribute to be modified.</param>
		/// <param name="size">Specifies the number of components per generic vertex attribute.
		/// Must be 1, 2, 3, or 4. The initial value is 4.</param>
		/// <param name="type">Specifies the data type of each component in the array. The initial value is Float.</param>
		/// <param name="normalized">Specifies whether fixed-point data values should be normalized
		/// or converted directed as fixed-point values when they are accessed.</param>
		/// <param name="stride">Specifies the byte offset between consecutive generic vertex attributes.
		/// If stride is 0, the generic vertex attributes are understood to be tightly packed in the array.
		/// The initial value is 0.</param>
		/// <param name="pointer">Specifies a pointer to the first component of the generic vertex attribute in the array.
		/// The initial value is 0.</param>
		public static void VertexAttribPointer(int index, int size, GlEnum.Type type, bool normalized, int stride, int pointer)
		{
			ptrVertexAttribPointer(index, size, type, normalized, stride, pointer);
		}
		private delegate void glVertexAttribPointer(int index, int size, GlEnum.Type type, bool normalized, int stride, int pointer);
		private static glVertexAttribPointer ptrVertexAttribPointer;

		/// <summary>
		/// Set the viewport.
		/// </summary>
		/// <param name="x">Specify the lower left corner of the viewport retangle, in pixels. The initial value is 0.</param>
		/// <param name="y">Specify the lower left corner of the viewport retangle, in pixels. The initial value is 0.</param>
		/// <param name="width">Specify the width of the viewport.
		/// When a GL context is first attached to a window,
		/// width and height are set the the dimensions of that window.</param>
		/// <param name="height">Specify the height of the viewport.
		/// When a GL context is first attached to a window,
		/// width and height are set the the dimensions of that window.</param>
		[DllImport(WIN_OpenGlDll, EntryPoint = "glViewport", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		public extern static void Viewport(int x, int y, int width, int height);
		#endregion
		#endregion
	}
}
#endif