﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Functions for handling pixel formats and palettes.
		/// </summary>
		public static class Pixel
		{
			private const string dllName = "SDL2.dll";

			#region Enumerators
			/// <summary>
			/// Pixel type.
			/// </summary>
			public enum Type
			{
				Unknwown,
				Index1,
				Index4,
				Index8,
				Packed8,
				Packed16,
				Packed32,
				ArrayU8,
				ArrayU16,
				ArrayU32,
				ArrayF16,
				ArrayF32
			}

			/// <summary>
			/// Bitmap pixel order, high bit -> low bit.
			/// </summary>
			public enum BitmapOrder
			{
				None,
				Order4321,
				Order1234
			}

			/// <summary>
			/// Packed component order, high bit -> low bit.
			/// </summary>
			public enum PackedOrder
			{
				None,
				XRGB,
				RGBX,
				ARGB,
				RGBA,
				XBGR,
				BGRX,
				ABGR,
				BGRA
			}

			/// <summary>
			/// Array component order, low bit - high bit.
			/// </summary>
			public enum ArrayOrder
			{
				None,
				RGB,
				RGBA,
				ARGB,
				BGR,
				BGRA,
				ABGR
			}

			/// <summary>
			/// Packed component layout.
			/// </summary>
			public enum PackedLayout
			{
				None,
				Layout332,
				Layout4444,
				Layout1555,
				Layout5551,
				Layout565,
				Layout8888,
				Layout2101010,
				Layout1010102,
			}

			/// <summary>
			/// Pixel format value getters.
			/// Not an actual enum because SDL defines them with macros.
			/// </summary>
			public static class FormatEnum
			{
				public static UInt32 Unknown { get { return 0; } }
				public static UInt32 Index1LSB { get { return defineFormat(Type.Index1, BitmapOrder.Order4321, PackedLayout.None, 1, 0); } }
				public static UInt32 Index1MSB { get { return defineFormat(Type.Index1, BitmapOrder.Order1234, PackedLayout.None, 1, 0); } }
				public static UInt32 Index4LSB { get { return defineFormat(Type.Index4, BitmapOrder.Order4321, PackedLayout.None, 4, 0); } }
				public static UInt32 Index4MSB { get { return defineFormat(Type.Index4, BitmapOrder.Order1234, PackedLayout.None, 4, 0); } }
				public static UInt32 Index8 { get { return defineFormat(Type.Index8, BitmapOrder.None, PackedLayout.None, 8, 1); } }
				public static UInt32 RGB332 { get { return defineFormat(Type.Packed8, PackedOrder.XRGB, PackedLayout.Layout332, 8, 1); } }
				public static UInt32 RGB444 { get { return defineFormat(Type.Packed16, PackedOrder.XRGB, PackedLayout.Layout4444, 12, 2); } }
				public static UInt32 RGB555 { get { return defineFormat(Type.Packed16, PackedOrder.XRGB, PackedLayout.Layout1555, 15, 2); } }
				public static UInt32 BGR555 { get { return defineFormat(Type.Packed16, PackedOrder.XBGR, PackedLayout.Layout1555, 15, 2); } }
				public static UInt32 ARGB4444 { get { return defineFormat(Type.Packed16, PackedOrder.ARGB, PackedLayout.Layout4444, 16, 2); } }
				public static UInt32 RGBA4444 { get { return defineFormat(Type.Packed16, PackedOrder.RGBA, PackedLayout.Layout4444, 16, 2); } }
				public static UInt32 ABGR4444 { get { return defineFormat(Type.Packed16, PackedOrder.ABGR, PackedLayout.Layout4444, 16, 2); } }
				public static UInt32 BGRA4444 { get { return defineFormat(Type.Packed16, PackedOrder.BGRA, PackedLayout.Layout4444, 16, 2); } }
				public static UInt32 ARGB1555 { get { return defineFormat(Type.Packed16, PackedOrder.ARGB, PackedLayout.Layout1555, 16, 2); } }
				public static UInt32 RGBA5551 { get { return defineFormat(Type.Packed16, PackedOrder.RGBA, PackedLayout.Layout5551, 16, 2); } }
				public static UInt32 ABGR1555 { get { return defineFormat(Type.Packed16, PackedOrder.ABGR, PackedLayout.Layout1555, 16, 2); } }
				public static UInt32 BGRA5551 { get { return defineFormat(Type.Packed16, PackedOrder.BGRA, PackedLayout.Layout5551, 16, 2); } }
				public static UInt32 RGB565 { get { return defineFormat(Type.Packed16, PackedOrder.XRGB, PackedLayout.Layout565, 16, 2); } }
				public static UInt32 BGR565 { get { return defineFormat(Type.Packed16, PackedOrder.XBGR, PackedLayout.Layout565, 16, 2); } }
				public static UInt32 RGB24 { get { return defineFormat(Type.ArrayU8, ArrayOrder.RGB, PackedLayout.None, 24, 3); } }
				public static UInt32 BGR24 { get { return defineFormat(Type.ArrayU8, ArrayOrder.BGR, PackedLayout.None, 24, 3); } }
				public static UInt32 RGB888 { get { return defineFormat(Type.Packed32, PackedOrder.XRGB, PackedLayout.Layout8888, 24, 4); } }
				public static UInt32 RGBX8888 { get { return defineFormat(Type.Packed32, PackedOrder.RGBX, PackedLayout.Layout8888,24, 4); } }
				public static UInt32 BGR888 { get { return defineFormat(Type.Packed32, PackedOrder.XBGR, PackedLayout.Layout8888, 24, 4); } }
				public static UInt32 BGRX8888 { get { return defineFormat(Type.Packed32, PackedOrder.BGRX, PackedLayout.Layout8888, 24, 4); } }
				public static UInt32 ARGB8888 { get { return defineFormat(Type.Packed32, PackedOrder.ARGB, PackedLayout.Layout8888, 32, 4); } }
				public static UInt32 RGBA8888 { get { return defineFormat(Type.Packed32, PackedOrder.RGBA, PackedLayout.Layout8888, 32, 4); } }
				public static UInt32 ABGR8888 { get { return defineFormat(Type.Packed32, PackedOrder.ABGR, PackedLayout.Layout8888, 32, 4); } }
				public static UInt32 BGRA8888 { get { return defineFormat(Type.Packed32, PackedOrder.BGRA, PackedLayout.Layout8888, 32, 4); } }
				public static UInt32 ARGB2101010 { get { return defineFormat(Type.Packed32, PackedOrder.ARGB, PackedLayout.Layout2101010, 32, 4); } }
				
				/// <summary>
				/// Define a pixel format.
				/// SDL does this with macro'd enums.
				/// We have to do this manually.
				/// Check the SDL docs to make sure your combination is valid.
				/// </summary>
				/// <param name="type"></param>
				/// <param name="order"></param>
				/// <param name="layout"></param>
				/// <param name="bits"></param>
				/// <param name="bytes"></param>
				/// <returns></returns>
				private static UInt32 defineFormat(Type type, BitmapOrder order, PackedLayout layout, uint bits, uint bytes)
				{
					return ((1 << 28) | ((uint)type << 24) | ((uint)order << 20) | ((uint)layout | 16) | (bits << 8) | (bytes << 0));
				}
				
				private static UInt32 defineFormat(Type type, PackedOrder order, PackedLayout layout, uint bits, uint bytes)
				{
					return ((1 << 28) | ((uint)type << 24) | ((uint)order << 20) | ((uint)layout | 16) | (bits << 8) | (bytes << 0));
				}

				private static UInt32 defineFormat(Type type, ArrayOrder order, PackedLayout layout, uint bits, uint bytes)
				{
					return ((1 << 28) | ((uint)type << 24) | ((uint)order << 20) | ((uint)layout | 16) | (bits << 8) | (bytes << 0));
				}

			}
			#endregion

			#region Structures
			/// <summary>
			/// Contains pixel format information.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Format
			{
				/// <summary>
				/// One of the Pixel.FormatEnum values.
				/// (SDL just calls this field Format,
				/// but members can't have their class's name.)
				/// </summary>
				public UInt32 FormatType;

				/// <summary>
				/// An SDL palette structure associated with this pixel format,
				/// or null if the format doesn't have a palette.
				/// </summary>
				public IntPtr Palette;

				/// <summary>
				/// The number of significant bits in a pixel value, eg: 8, 15, 16, 24, 32.
				/// </summary>
				public byte BitsPerPixel;

				/// <summary>
				/// The number of bytes required to hold a pixel value, eg: 1, 2, 3, 4.
				/// </summary>
				public byte BytesPerPixel;

				/// <summary>
				/// A mask representing the location of the red component of the pixel.
				/// </summary>
				public uint Rmask;

				/// <summary>
				/// A mask representing the location of the green component of the pixel.
				/// </summary>
				public uint Gmask;

				/// <summary>
				/// A mask representing the location of the blue component of the pixel.
				/// </summary>
				public uint Bmask;

				/// <summary>
				/// A mask representing the location of the alpha component of the pixel
				/// or 0 if the pixel format doesn't have any alpha information.
				/// </summary>
				public uint Amask;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Rloss;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Gloss;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Bloss;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Aloss;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Rshift;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Gshift;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Bshift;

				/// <summary>
				/// Internal use.
				/// </summary>
				private byte Ashift;

				/// <summary>
				/// Internal use.
				/// </summary>
				private int refcount;

				/// <summary>
				/// Internal use.
				/// </summary>
				IntPtr next;
			}
			#endregion
		}
	}
}
#endif