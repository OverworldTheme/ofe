﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Functions for handling display and window actions.
		/// </summary>
		public static class Window
		{
			private const string dllName = "SDL2.dll";

			#region Constants
			public const int PosCentered = 0x2FFF0000;
			public const int PosUndefined = 0x1FFF0000;
			#endregion

			#region Enumerators
			/// <summary>
			/// An enumeration of window states.
			/// </summary>
			[Flags]
			public enum Flag
			{
				/// <summary>
				/// Fullscreen window.
				/// </summary>
				Fullscreen = 0x00000001,

				/// <summary>
				/// Window usable with OpenGL context.
				/// </summary>
				OpenGL = 0x00000002,

				/// <summary>
				/// Window is visible.
				/// </summary>
				Shown = 0x00000004,

				/// <summary>
				/// Window is not visible.
				/// </summary>
				Hidden = 0x00000008,

				/// <summary>
				/// No window decoration.
				/// </summary>
				Borderless = 0x00000010,

				/// <summary>
				/// Window can be resized.
				/// </summary>
				Resizable = 0x00000020,

				/// <summary>
				/// Window is minimized.
				/// </summary>
				Minimized = 0x00000040,

				/// <summary>
				/// Window is maximized.
				/// </summary>
				Maximized = 0x00000080,

				/// <summary>
				/// Window has grabbed input focus.
				/// </summary>
				InputGrabbed = 0x00000100,

				/// <summary>
				/// Window has input focus.
				/// </summary>
				InputFocus = 0x00000200,

				/// <summary>
				/// Window has mouse focus.
				/// </summary>
				MouseFocus = 0x00000400,

				/// <summary>
				/// Fullscreen window at the current desktop resolution.
				/// </summary>
				FullscreenDesktop = (Fullscreen | 0x00001000),

				/// <summary>
				/// Window was not created by SDL.
				/// </summary>
				Foreign = 0x00000800,

				/// <summary>
				/// Window should be created in high-DPI mode if supported.
				/// </summary>
				AllowHighDPI = 0x00002000,

				/// <summary>
				/// Window has mouse captured.
				/// (Unrelated to InputGrabbed.)
				/// </summary>
				MouseCapture = 0x00004000,
			}

			/// <summary>
			/// An enumeration of window events.
			/// </summary>
			public enum EventID : byte
			{
				/// <summary>
				/// (Never used.)
				/// </summary>
				None = 0,

				/// <summary>
				/// Window has been shown.
				/// </summary>
				Shown,

				/// <summary>
				/// Window has been hidden.
				/// </summary>
				Hidden,

				/// <summary>
				/// Window has been exposed and should be redrawn.
				/// </summary>
				Exposed,

				/// <summary>
				/// Window has been moved to data1, data2.
				/// </summary>
				Moved,

				/// <summary>
				/// Window has been resized to data1×data2;
				/// this event is always preceded by Event.WindowType.SizeChanged.
				/// </summary>
				Resized,

				/// <summary>
				/// Window size has changed,
				/// either as a result of an API call or through the system or user changing the window size;
				/// this event is followed by Event.WindowType.Resized if the size was changed by an external event,
				/// i.e. the user or the window manager.
				/// </summary>
				SizeChanged,

				/// <summary>
				/// Window has been minimized.
				/// </summary>
				Minimized,

				/// <summary>
				/// Window has been maximized.
				/// </summary>
				Maximized,

				/// <summary>
				/// Window has been restored to normal size and position.
				/// </summary>
				Restored,

				/// <summary>
				/// Window has gained mouse focus.
				/// </summary>
				Enter,

				/// <summary>
				/// Window has lost mouse focus.
				/// </summary>
				Leave,

				/// <summary>
				/// Window has gained keyboard focus.
				/// </summary>
				FocusGained,

				/// <summary>
				/// Window has lost keyboard focus.
				/// </summary>
				FocusLost,

				/// <summary>
				/// The window manager requests that the window be closed.
				/// </summary>
				Close
			}
			#endregion

			#region Structures
			/// <summary>
			/// A structure that describes a display mode.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct DisplayMode
			{
				/// <summary>
				/// One of the Pixel.FormatEnum values.
				/// </summary>
				public UInt32 Format;

				/// <summary>
				/// Width.
				/// </summary>
				public int Width;

				/// <summary>
				/// Height.
				/// </summary>
				public int Height;

				/// <summary>
				/// Refresh rate (in Hz), or 0 for unspecified.
				/// </summary>
				public int RefreshRate;

				/// <summary>
				/// Driver-specific data,
				/// initialize to 0.
				/// </summary>
				public IntPtr DriverDate;
			}

			[StructLayout(LayoutKind.Sequential)]
			public struct Event
			{
				/// <summary>
				/// Will be Event.Type.Window.
				/// </summary>
				public UInt32 Type;

				/// <summary>
				/// Timestamp of the event.
				/// </summary>
				public UInt32 Timestamp;

				/// <summary>
				/// The associated window.
				/// </summary>
				public UInt32 WindowID;

				/// <summary>
				/// Window.EventID
				/// </summary>
				public EventID EventID;

				/// <summary>
				/// Event dependant data.
				/// </summary>
				public Int32 Data1;

				/// <summary>
				/// Event dependant data.
				/// </summary>
				public Int32 Data2;
			}
			#endregion

			#region Functions
			#region Creation and Destruction
			/// <summary>
			/// Creates a window with the specified position, dimensions, and flags.
			/// </summary>
			/// <param name="title">The title of the window.</param>
			/// <param name="x">The x position of the window, Window.PosCentered, or Window.PosUndefined.</param>
			/// <param name="x">The y position of the window, Window.PosCentered, or Window.PosUndefined.</param>
			/// <param name="w">The width of the window.</param>
			/// <param name="h">The height of the window.</param>
			/// <param name="flags">One or more Window.Flag enums OR'd together.</param>
			/// <returns>An IntPtr to the window that was created or NULL on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_CreateWindow", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true, CharSet = CharSet.Ansi)]
			public extern static IntPtr Create(string title, int x, int y, int w, int h, Flag flags);

			/// <summary>
			/// Create a window and default renderer.
			/// </summary>
			/// <param name="width">The width of the window.</param>
			/// <param name="height">The height of the window.</param>
			/// <param name="windowFlags">The flags used to create the window.</param>
			/// <param name="window">A pointer filled with the window, or null on error.</param>
			/// <param name="renderer">A pointer filled with the renderer, or null on error.</param>
			/// <returns>0 on success, or -1 on error;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_CreateWindowAndRenderer", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static int CreateWindowAndRenderer(int width, int height, Flag windowFlags, out IntPtr window, out IntPtr renderer);

			/// <summary>
			/// Use this function to create an SDL window from an existing native window.
			/// </summary>
			/// <param name="data">A pointer to driver-dependent window creation data,
			/// typically your native window cast to a void*</param>
			/// <returns>An IntPtr to the window that was created or null on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_CreateWindowFrom", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static IntPtr CreateFrom(IntPtr data);

			/// <summary>
			/// Destroys a window.
			/// </summary>
			/// <param name="window">The window to destroy.</param>
			[DllImport(dllName, EntryPoint = "SDL_DestroyWindow", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static void Destroy(IntPtr window);
			#endregion

			#region Screen Saver
			/// <summary>
			/// Prevents the screen from being blanked by a screen saver.
			/// If you disable the screensaver, it is automatically re-enabled when SDL quits.
			/// </summary>
			[DllImport(dllName, EntryPoint = "SDL_DisableScreenSaver", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static void DisableScreenSaver();

			/// <summary>
			/// Allows the screen to be blanked by a screen saver.
			/// </summary>
			[DllImport(dllName, EntryPoint = "SDL_EnableScreenSaver", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static void EnableScreenSaver();

			/// <summary>
			/// Returns whether the screensaver is currently enabled.
			/// The screensaver is disabled by default since SDL 2.0.2.
			/// </summary>
			/// <returns>True if the screensaver is enabled, false if it is disabled.</returns>
			[DllImport(dllName, EntryPoint = "SDL_IsScreenSaverEnabled", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static bool IsScreenSaverEnabler();
			#endregion

			#region OpenGL
			/// <summary>
			/// OpenGL functions related to window management.
			/// </summary>
			public static class GL
			{
				/// <summary>
				/// Creates an OpenGL context for use with an OpenGL window, and makes it current.
				/// </summary>
				/// <param name="window">The window to associate with the context.</param>
				/// <returns>The OpenGL context associated with window or null on error;
				/// call Error.Get() for more information.</returns>
				[DllImport(dllName, EntryPoint = "SDL_GL_CreateContext", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
				public extern static IntPtr CreateContext(IntPtr window);

				/// <summary>
				/// Deletes an OpenGL context.
				/// </summary>
				/// <param name="context">The OpenGL context to be deleted.</param>
				[DllImport(dllName, EntryPoint = "SDL_GL_DeleteContext", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
				public extern static void DeleteContext(IntPtr context);

				/// <summary>
				/// Gets the swap interval for the current OpenGL context.
				/// </summary>
				/// <returns>0 if there is no vertical retrace synchronization,
				/// 1 if the buffer swap is synchronized with the vertical retrace,
				/// -1 if getting the swap interval is not support;
				/// call Error.Get() for more information.</returns>
				[DllImport(dllName, EntryPoint = "SDL_GL_GetSwapInterval", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
				public extern static int GetSwapInterval();

				/// <summary>
				/// Gets an OpenGL function by name.
				/// </summary>
				/// <param name="proc">The name of an OpenGL function.</param>
				/// <returns>A pointer to the named OpenGL function.
				/// The returned pointer should be cast to the appropriate function signature.</returns>
				[DllImport(dllName, EntryPoint = "SDL_EnableScreenSaver", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
				public extern static IntPtr GetProcAddress(string proc);

				/// <summary>
				/// Sets up an OpenGL context for rendering into an OpenGL window.
				/// </summary>
				/// <param name="window">The window to associate with the context.</param>
				/// <param name="context">The OpenGL context to associate with the window.</param>
				/// <returns>0 on success or a negative error code on failure;
				/// call Error.Get() for more information.</returns>
				[DllImport(dllName, EntryPoint = "SDL_GL_DeleteContext", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
				public extern static int MakeCurrent(IntPtr window, IntPtr context);

				/// <summary>
				/// Use this function to set the swap interval for the current OpenGL context.
				/// </summary>
				/// <param name="interval">0 for immediate updates,
				/// 1 for updates synchronized with the vertical retrace,
				/// -1 for late swap tearing</param>
				/// <returns>Returns 0 on success or -1 if setting the swap interval is not supported; call SDL_GetError() for more information.</returns>
				[DllImport(dllName, EntryPoint = "SDL_GL_SetSwapInterval", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
				public extern static int SetSwapInterval(int interval);

				/// <summary>
				/// Updates a window with OpenGL rendering.
				/// This is used with double-buffered OpenGL contexts,
				/// which are the default.
				/// </summary>
				/// <param name="window">The window to change.</param>
				[DllImport(dllName, EntryPoint = "SDL_GL_SwapWindow", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
				public extern static void SwapWindow(IntPtr window);
			}
			#endregion

			#region Getters
			/// <summary>
			/// Gets the desktop area represented by a display,
			/// with the primary display located at 0,0.
			/// </summary>
			/// <param name="displayIndex">The index of the display to query.</param>
			/// <param name="rect">The SDL.Rect structure filled in with the display bounds.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call SDL.Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetDisplayBounds", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetDisplayBounds(int displayIndex, out Rect rect);

			/// <summary>
			/// Gets information about a specific display mode.
			/// (Not to be confused with GetWindowDisplayMode.)
			/// </summary>
			/// <param name="displayIndex">The index of the display to query.</param>
			/// <param name="modeIndex">The index of the display mode to query.</param>
			/// <param name="mode">A DisplayMode structure filled in with the mode at modeIndex.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call SDL.Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetDisplayMode", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetDisplayMode(int displayIndex, int modeIndex, out DisplayMode mode);

			/// <summary>
			/// Fills in information about the display mode to use when a window is visible at fullscreen.
			/// </summary>
			/// <param name="window">The window to query.</param>
			/// <param name="mode">An SDL display mode structure filled in with the fullscreen display mode.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call SDL.Error.Get() for more informaiton.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetWindowDisplayMode", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetWindowDisplayMode(IntPtr window, out DisplayMode mode);

			/// <summary>
			/// Returns the number of available display modes.
			/// </summary>
			/// <param name="displayIndex">The index of the display to query.</param>
			/// <returns>A number >= 1 on success or a negative error code on failure;
			/// call SDL.Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetNumDisplayModes", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetNumDisplayModes(int displayIndex);

			/// <summary>
			/// Returns the number of available video displays.
			/// </summary>
			/// <returns>A number >= 1 or a negative error code on failure;
			/// call SDL.Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetNumVideoDisplays", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetNumVideoDisplays();

			/// <summary>
			/// Gets the index of the display associated with a window.
			/// </summary>
			/// <param name="window">The window to query.</param>
			/// <returns>The index of the display containing the center of the window on success or a negative error code on failure;
			/// call SDL.Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetWindowDisplayIndex", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetDisplayIndex(IntPtr window);

			/// <summary>
			/// Gets the window flags.
			/// </summary>
			/// <param name="window">The window to query.</param>
			/// <returns>A mask of the SDL window flags associated with window.
			/// The flags will be OR'd together.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetWindowFlags", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static Flag GetFlags(IntPtr window);

			/// <summary>
			/// Gets the position of a window.
			/// </summary>
			/// <param name="window">The window to query.</param>
			/// <param name="x">A pointer filled in with the x coordinate of the window.</param>
			/// <param name="y">A pointer filled in with the y coordinate of the window.</param>
			[DllImport(dllName, EntryPoint = "SDL_GetWindowPosition", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static void GetPosition(IntPtr window, out int x, out int y);

			/// <summary>
			/// Gets the size of a window's client area.
			/// </summary>
			/// <param name="window">The window to query the width and height from.</param>
			/// <param name="w">A pointer filled in with the width of the window.</param>
			/// <param name="h">A pointer filled in with the height of the window.</param>
			[DllImport(dllName, EntryPoint = "SDL_GetWindowSize", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static void GetSize(IntPtr window, out int w, out int h);

			/// <summary>
			/// Gets the title of a window.
			/// </summary>
			/// <param name="window">The window to query.</param>
			/// <returns>The title of the window or an empty string if there is no title.</returns>
			public static string GetTitle(IntPtr window) { return Marshal.PtrToStringAnsi(getTitle(window)); }
			[DllImport(dllName, EntryPoint = "SDL_GetWindowPosition", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			private extern static IntPtr getTitle(IntPtr window);
			#endregion

			#region Setters
			/// <summary>
			/// Sets the display mode to use when a window is visible at fullscreen.
			/// </summary>
			/// <param name="window">The window to affect.</param>
			/// <param name="mode">The SDL display mode structure represnting the mode to use,
			/// or null to use the window's dimensions and the desktop's format and refresh rate.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call SDL.Error.Get() for more informaiton.</returns>
			[DllImport(dllName, EntryPoint = "SDL_SetWindowDisplayMode", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int SetDisplayMode(IntPtr window, DisplayMode mode);

			/// <summary>
			/// Sets a window's fullscreen state.
			/// </summary>
			/// <param name="window">The window to change.</param>
			/// <param name="flags">Fullscreen for "real" fullscreen with a videomode change,
			/// FullscreenDesktop for "fake" fullscreen that takes the size of the desktop,
			/// or 0 for windowed mode.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call SDL.Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_SetWindowFullscreen", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int SetFullscreen(IntPtr window, Flag flags);

			/// <summary>
			/// Set the icon for a window.
			/// </summary>
			/// <param name="window">The window to change.</param>
			/// <param name="icon">A Surface structure containing the icon for the window.</param>
			[DllImport(dllName, EntryPoint = "SDL_SetWindowIcon", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static void SetIcon(IntPtr window, IntPtr icon);

			/// <summary>
			/// Sets the position of a window.
			/// </summary>
			/// <param name="window">The window to reposition.</param>
			/// <param name="x">The x coordinate of the window, Window.PosCentered, or Window.PosUndefined.</param>
			/// <param name="x">The y coordinate of the window, Window.PosCentered, or Window.PosUndefined.</param>
			[DllImport(dllName, EntryPoint = "SDL_SetWindowPosition", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static void SetPosition(IntPtr window, int x, int y);

			/// <summary>
			/// Sets the size of a window's client area.
			/// </summary>
			/// <param name="window">The window to change.</param>
			/// <param name="w">The width of the window in pixels.</param>
			/// <param name="h">The height of the window in pixels.</param>
			[DllImport(dllName, EntryPoint = "SDL_SetWindowSize", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static void SetSize(IntPtr window, int w, int h);

			/// <summary>
			/// Sets the title of a window.
			/// </summary>
			/// <param name="window">The window to change.</param>
			/// <param name="title">The desired window title.</param>
			[DllImport(dllName, EntryPoint = "SDL_SetWindowTitle", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
			public extern static void SetTitle(IntPtr window, string title);
			#endregion

			#region Visiblity		
			/// <summary>
			/// Shows a window.
			/// </summary>
			/// <param name="window">The window to show.</param>
			[DllImport(dllName, EntryPoint = "SDL_ShowWindow", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static void Show(IntPtr window);

			/// <summary>
			/// Hides a window.
			/// </summary>
			/// <param name="window">The window to hide.</param>
			[DllImport(dllName, EntryPoint = "SDL_HideWindow", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static void Hide(IntPtr window);
			#endregion

			#region SDL1-Style Surface Updating
			/// <summary>
			/// Copies the window surface to the screen.
			/// </summary>
			/// <param name="window">The window to update.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_UpdateWindowSurface", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static int UpdateSurface(IntPtr window);

			/// <summary>
			/// Copies areas of the window surface to the screen.
			/// </summary>
			/// <param name="window">The window to update.</param>
			/// <param name="rects">An array of Rect structures representing areas of the surface to copy.</param>
			/// <param name="numrects">The number of rectangles.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_UpdateWindowSurfaceRects", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static int UpdateSurfaceRects(IntPtr window, Rect[] rects, int numrects);
			#endregion
			#endregion
		}
	}
}
#endif