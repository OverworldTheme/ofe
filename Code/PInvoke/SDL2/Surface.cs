﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal partial class SDL
	{
		[StructLayout(LayoutKind.Sequential)]
		public struct Surface
		{
			private const string dllName = "SDL2.dll";

			#region Structure
			/// <summary>
			/// The format of the pixels stored in the surface.
			/// (Read-only.)
			/// </summary>
			public SDL.Pixel.Format Format;

			/// <summary>
			/// The width in pixels.
			/// (Read-only.)
			/// </summary>
			public int W;

			/// <summary>
			/// The height in pixels.
			/// (Read-only.)
			/// </summary>
			public int H;

			/// <summary>
			/// The length of a row of pixels in bytes.
			/// (Read-only.)
			/// </summary>
			public int Pitch;

			/// <summary>
			/// The pointer to the actual pixel data.
			/// (Read-write.)
			/// </summary>
			public IntPtr Pixels;

			/// <summary>
			/// An arbitrary pointer you can set.
			/// (Read-write.)
			/// </summary>
			public IntPtr UserData;

			/// <summary>
			/// Used ofr surfaces that require locking.
			/// (Internal use.)
			/// </summary>
			private int locked;

			/// <summary>
			/// Used for surfaces that require locking.
			/// (Internal use.)
			/// </summary>
			private IntPtr lockData;

			/// <summary>
			/// An SDL_Rect structure used to clip blits to the surface which can be set by SDL_SetClipRect().
			/// (Read-only.)
			/// </summary>
			public SDL.Rect ClipRect;

			/// <summary>
			/// Info for fast blit mapping to other surfaces.
			/// (Internal use.)
			/// </summary>
			private IntPtr map;

			/// <summary>
			/// Reference count that can be incremented by the application.
			/// </summary>
			public int RefCount;
			#endregion

			#region Functions
			/// <summary>
			/// Use this function to allocate a new RGB surface with existing pixel data.
			/// </summary>
			/// <param name="pixels">a pointer to existing pixel data</param>
			/// <param name="width">the width of the surface</param>
			/// <param name="height">the height of the surface</param>
			/// <param name="depth">the depth of the surface in dits</param>
			/// <param name="pitch">the length of a row of pixels in bytes</param>
			/// <param name="Rmask">the red mask for the pixels</param>
			/// <param name="Gmask">the green mask for the pixels</param>
			/// <param name="Bmask">the blue mask for the pixels</param>
			/// <param name="Amask">the alpha mask for the pixels</param>
			/// <returns>Returns the new SDL_Surface structure that is created or NULL if it fails;
			/// call SDL_GetError() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_CreateRGBSurfaceFrom", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
      public extern static IntPtr CreateRgbSurfaceFrom(IntPtr pixels, int width, int height, int depth, int pitch, uint Rmask, uint Gmask, uint Bmask, uint Amask);

			/// <summary>
			/// Use this function to free an RGB surface.
			/// If the surface was created using SDL_CreateRGBSurfaceFrom() then the pixel data is not freed.
			/// </summary>
			/// <param name="surface">the SDL_Surface structure to free</param>
			[DllImport(dllName, EntryPoint = "SDL_CreateRGBSurfaceFrom", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static void Free(IntPtr surface);
			#endregion
		}
	}
}
#endif