﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		#region Enumerators
		public enum Keymod : UInt16
		{
			None = 0,
			LShift = 0x0001,
			RShift = 0x0002,
			LCtrl = 0x0040,
			RCtrl = 0x0080,
			LAlt = 0x0100,
			RAlt = 0x0200,
			LGui = 0x0400,
			RGui = 0x0800,
			Num = 0x1000,
			Caps = 0x2000,
			Mode = 04000,
			Reserved = 0x8000,

			Ctrl = LCtrl | RCtrl,
			Shift = LShift | RShift,
			Alt = LAlt | RAlt,
			Gui = LGui | RGui
		}

		/// <summary>
		/// Virtual key representation.
		/// Numbers enumeration is prefixed by "Num", unlike normal SDL.
		/// </summary>
		public enum Keycode
		{
			Unknown = 0,

			Return = '\r',
			Escape = 33,
			Backspace = '\b',
			Tab = '\t',

			Space = ' ',
			Exclaim = '!',
			QuoteDbl = '"',
			Hash = '#',
			Percent = '%',
			Ampersand = '&',
			Quote = '\'',
			LeftParen = '(',
			RightParen = ')',
			Asterisk = '*',
			Plus = '+',
			Comma = ',',
			Minus = '-',
			Period = '.',
			Slash = '/',

			Num0 = '0',
			Num1 = '1',
			Num2 = '2',
			Num3 = '3',
			Num4 = '4',
			Num5 = '5',
			Num6 = '6',
			Num7 = '7',
			Num8 = '8',
			Num9 = '9',

			Colon = ':',
			Semicolon = ';',
			Less = '<',
			Equals = '=',
			Greater = '>',
			Question = '?',
			At = '@',

			// Uppercase letters are skipped in SDL.

			LeftBracket = '[',
			Backslash = '\\',
			RightBracket = ']',
			Caret = '^',
			Underscore = '_',
			Backquote = '`',

			a = 'a',
			b = 'b',
			c = 'c',
			d = 'd',
			e = 'e',
			f = 'f',
			g = 'g',
			h = 'h',
			i = 'i',
			j = 'j',
			k = 'k',
			l = 'l',
			m = 'm',
			n = 'n',
			o = 'o',
			p = 'p',
			q = 'q',
			r = 'r',
			s = 's',
			t = 't',
			u = 'u',
			v = 'v',
			w = 'w',
			x = 'x',
			y = 'y',
			z = 'z'
		}

		/// <summary>
		/// An enumeration of the SDL keyboard scancode representation.
		/// The values in this enumeration are based on the USB usage page standard.
		/// /// Numbers enumeration is prefixed by "Num", unlike normal SDL.
		/// </summary>
		public enum Scancode
		{
			Unknown = 0,

			A = 4,
			B = 5,
			C = 6,
			D = 7,
			E = 8,
			F = 9,
			G = 10,
			H = 11,
			I = 12,
			J = 13,
			K = 14,
			L = 15,
			M = 16,
			N = 17,
			O = 18,
			P = 19,
			Q = 20,
			R = 21,
			S = 22,
			T = 23,
			U = 24,
			V = 25,
			W = 26,
			X = 27,
			Y = 28,
			Z = 29,

			Num1 = 30,
			Num2 = 31,
			Num3 = 32,
			Num4 = 33,
			Num5 = 34,
			Num6 = 35,
			Num7 = 36,
			Num8 = 37,
			Num9 = 38,
			Num0 = 39,

			Return = 40,
			Escape = 41,
			Backspace = 42,
			Tab = 43,
			Space = 44,

			Minus = 45,
			Equals = 46,
			LeftBracket = 47,
			RightBracket = 48,
			Backslash = 49,
			NonUsHash = 50,
			Semicolon = 51,
			Apostrophe = 52,
			Grave = 53,
			Comma = 54,
			Period = 55,
			Slash = 56,

			CapsLock = 57,

			F1 = 58,
			F2 = 59,
			F3 = 60,
			F4 = 61,
			F5 = 62,
			F6 = 63,
			F7 = 64,
			F8 = 65,
			F9 = 66,
			F10 = 67,
			F11 = 68,
			F12 = 69,

			PrintScreen = 70,
			ScrollLock = 71,
			Pause = 72,
			Insert = 73,
			Home = 74,
			PageUp = 75,
			PageDown = 76,
			Right = 79,
			Left = 80,
			Up = 81,
			Down = 82,

			KpNumLockClear = 83,
			KpDivide = 84,
			KpMultiply = 85,
			KpMinus = 86,
			KpPlus = 87,
			KpEnter = 88,
			Kp1 = 89,
			Kp2 = 90,
			Kp3 = 91,
			Kp4 = 92,
			Kp5 = 93,
			Kp6 = 94,
			Kp7 = 95,
			Kp8 = 96,
			Kp9 = 97,
			Kp0 = 98,
			KpPeriod = 99,

			NonUsBackslash = 100,
			Application = 101,
			Power = 102,
			KpEquals = 103,
			F13 = 104,
			F14 = 105,
			F15 = 106,
			F16 = 107,
			F17 = 108,
			F18 = 109,
			F19 = 110,
			F20 = 111,
			F21 = 112,
			F22 = 113,
			F23 = 114,
			F24 = 115,
			Execute = 116,
			Help = 117,
			Menu = 118,
			Select = 119,
			Stop = 120,
			Again = 121,
			Undo = 122,
			Cut = 123,
			Copy = 124,
			Paste = 125,
			Find = 126,
			Mute = 127,
			VolumeUp = 128,
			VolumeDown = 129,

			// Next three values are purposefully not enabled in SDL.

			KpComma = 133,
			KpEqualsAs400 = 134,
			International1 = 135,
			International2 = 136,
			International3 = 137,
			International4 = 138,
			International5 = 139,
			International6 = 140,
			International7 = 141,
			International8 = 142,
			International9 = 143,
			Lang1 = 144,
			Lang2 = 145,
			Lang3 = 146,
			Lang4 = 147,
			Lang5 = 148,
			Lang6 = 149,
			Lang7 = 150,
			Lang8 = 151,
			Lang9 = 152,

			AltErase = 153,
			SysReq = 154,
			Cancel = 155,
			Clear = 156,
			Prior = 157,
			Return2 = 158,
			Separator = 159,
			Out = 160,
			Oper = 161,
			ClearAgain = 162,
			CrSel = 163,
			ExSel = 164,

			Kp00 = 176,
			Kp000 = 177,
			ThousandsSeparator = 178,
			DecimalsSeparator = 179,
			CurrencyUnit = 180,
			CurrencySubUnit = 181,
			KpLeftParen = 182,
			KpRightParen = 183,
			KpLeftBrace = 184,
			KpRightBrace = 185,
			KpTab = 186,
			KpBackspace = 187,
			KpA = 188,
			KpB = 189,
			KpC = 190,
			KpD = 191,
			KpE = 192,
			KpF = 193,
			KpXor = 194,
			KpPower = 195,
			KpPercent = 196,
			KpLess = 197,
			KpGreater = 198,
			KpAmpersand = 199,
			KpDblAmpersand = 200,
			KpVerticalBar = 201,
			KpDblVerticalBar = 202,
			KpColon = 203,
			KpHash = 204,
			KpSpace = 205,
			KpAt = 206,
			KpExclam = 207,
			KpMemStore = 208,
			KpMemRecall = 209,
			KpMemClear = 210,
			KpMemAdd = 211,
			KpMemSubtract = 212,
			KpMemMultiply = 213,
			KpMemDivide = 214,
			KpPlusMinus = 215,
			KpClear = 216,
			KpClearEntry = 217,
			KpBinary = 218,
			KpOctal = 219,
			KpDecimal = 220,
			KpHexadecimal = 221,

			// 222 and 223 are reserved.

			LCtrl = 224,
			LShift = 225,
			/// <summary>
			/// Alt, option.
			/// </summary>
			LAlt = 226,
			/// <summary>
			/// Windows, command (Apple), meta.
			/// </summary>
			LGui = 227,
			RCtrl = 228,
			RShift = 229,
			/// <summary>
			/// Alt, option.
			/// </summary>
			RAlt = 230,
			/// <summary>
			/// Windows, command (Apple), meta.
			/// </summary>
			RGui = 231,

			Mode = 257,

			AudioNext = 258,
			AudioPrev = 259,
			AudioStop = 260,
			AudioPlay = 261,
			AudioMute = 262,
			MediaSelect = 263,
			Www = 264,
			Mail = 265,
			Calculator = 266,
			Computer = 267,
			AcSearch = 268,
			AcHome = 269,
			AcBack = 270,
			AcForward = 271,
			AcStop = 272,
			AcRefresh = 273,
			AcBookmarks = 274,

			BrightnessDown = 275,
			BrightnessUp = 276,
			/// <summary>
			/// Display mirroring/dual display switch, video mode switch.
			/// </summary>
			DisplaySwitch = 277,
			KbDillumToggle = 278,
			KbDillumDown = 279,
			KbDillumUp = 280,
			Eject = 281,
			Sleep = 282,

			App1 = 283,
			App2 = 284,

			/// <summary>
			/// Not a key, just marks the number of scancodes for array bounds.
			/// </summary>
			NumScanCodes = 512
		}
		#endregion

		/// <summary>
		/// Functions for keyboard inputs.
		/// </summary>
		public static class Keyboard
		{
			private const string dllName = "SDL2.dll";

			#region Pressed
			/// <summary>
			/// A value representing a key being released.
			/// </summary>
			public const int Released = 0;

			/// <summary>
			/// A value representing a key being pressed.
			/// </summary>
			public const int Pressed = 1;
			#endregion

			#region Structures
			[StructLayout(LayoutKind.Sequential)]
			public struct Keysym
			{
				/// <summary>
				/// SDL physical key code.
				/// </summary>
				public Scancode Scancode;

				/// <summary>
				/// SDL virtual key code.
				/// </summary>
				public Keycode sym;

				/// <summary>
				/// Current key modifiers.
				/// May be OR'd.
				/// </summary>
				Keymod mod;

				/// <summary>
				/// Unused.
				/// </summary>
				UInt32 unused;
			}

			/// <summary>
			/// Contains keyboard button event information.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Event
			{
				/// <summary>
				/// The event type;
				/// KeyDown or KeyUp.
				/// </summary>
				public SDL.Event.Type Type;

				/// <summary>
				/// Timestamp of the event.
				/// </summary>
				public UInt32 Timestamp;

				/// <summary>
				/// The window with keyboard focus, if any.
				/// </summary>
				public UInt32 WindowID;

				/// <summary>
				/// The state of the key,
				/// Keyboard.Pressed or Keyboard.Released.
				/// </summary>
				public byte State;

				/// <summary>
				/// Non-zero if this is a key repeat.
				/// </summary>
				public byte Repeat;

				/// <summary>
				/// The Keysym representing the key that was pressed or released.
				/// </summary>
				public Keysym Keysym;
			}
			#endregion

			#region Functions
			/// <summary>
			/// Gets a key code from a human-readable name.
			/// </summary>
			/// <param name="name">The human-readable key name.</param>
			/// <returns>Key code, Keycode.Unknown if the name wasn't recognized;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetKeyFromName", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
			public extern static Keycode GetKeyFromName(string name);

			/// <summary>
			/// Gets the key code corresponding to the given scancode according to the current keyboard layout.
			/// </summary>
			/// <param name="scancode">The desired scancode to query.</param>
			/// <returns>The keycode taht corresponds to the given scancode.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetKeyFromScancode", CallingConvention = CallingConvention.Cdecl)]
			public extern static Keycode GetKeyFromScancode(Scancode scancode);

			/// <summary>
			/// Gets a human-readable name for a key.
			/// </summary>
			/// <param name="key">The desired keycode to query.</param>
			/// <returns>If the key doesn't have a name,
			/// this function returns an empty string.</returns>
			public static string GetKeyName(Keycode key) { return Marshal.PtrToStringAnsi(getKeyName(key)); }
			[DllImport(dllName, EntryPoint = "SDL_GetKeyName", CallingConvention = CallingConvention.Cdecl)]
			private extern static IntPtr getKeyName(Keycode key);

			/// <summary>
			/// Gets the window which currently has keyboard focus.
			/// </summary>
			/// <returns>The window with keybaord focus.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetKeyboardFocus", CallingConvention = CallingConvention.Cdecl)]
			public extern static IntPtr GetKeyboardFocus();

			/// <summary>
			/// Use this function to get a snapshot of the current state of the keyboard.
			/// </summary>
			/// <param name="numkeys">If non-null, recieves the length of the returned array.</param>
			/// <returns>An array of key states.
			/// A value of 1 means that the key is pressed and a value of 0 means that it is not.
			/// Indexes into this array are obtained by using scancode values.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetKeyboardState", CallingConvention = CallingConvention.Cdecl)]
			public extern static byte[] GetKeyboardState(out int numkeys);

			/// <summary>
			/// Gets the current key modifier state for the keyboard.
			/// </summary>
			/// <returns>An OR'd combination of the modifier keys for the keyboard.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetModState", CallingConvention = CallingConvention.Cdecl)]
			public extern static Keymod GetModState();

			/// <summary>
			/// Gets the scancode corresponding to the given key code according to the current keyboard layout.
			/// </summary>
			/// <param name="key">The desired keycode to query.</param>
			/// <returns>The scancode that corresponds to the given keycode.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetScancodeFromKey", CallingConvention = CallingConvention.Cdecl)]
			public extern static Scancode GetScancodeFromKey(Keycode key);

			/// <summary>
			/// Gets a scancode from a human-readable name.
			/// </summary>
			/// <param name="name">The human-readable scancode name.</param>
			/// <returns>The scancode, or Scancode.Unknown if the name wasn't recognized;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetScancodeFromName", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
			public extern static Scancode GetScancodeFromKey(string name);

			/// <summary>
			/// Gets a human-readable name for a scancode.
			/// </summary>
			/// <param name="key">The desired scancode to query.</param>
			/// <returns>The name for the scancode.
			/// If the scancode doesn't have a name this function returns an empty string.</returns>
			public static string GetScancodeName(Scancode key) { return Marshal.PtrToStringAnsi(getScancodeName(key)); }
			[DllImport(dllName, EntryPoint = "SDL_GetScancodeName", CallingConvention = CallingConvention.Cdecl)]
			private extern static IntPtr getScancodeName(Scancode key);

			/// <summary>
			/// Checks whether the platform has some screen keyboard support.
			/// </summary>
			/// <returns>True if the platform has some screen keyboard suppert or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasScreenKeyboardSupport", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasScreenKeyboardSupport();

			/// <summary>
			/// Checks whether the screen keyboard is shown for given window.
			/// </summary>
			/// <param name="window">The window for which the screen keyboard should be queried.</param>
			/// <returns>True if screen keyboard is shown or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_IsScreenKeyboardShown", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool IsScreenKeyboardShown(IntPtr window);

			/// <summary>
			/// Checks whether or not Unicode text input events are enabled.
			/// </summary>
			/// <returns>True if text input events are enabled else false.</returns>
			[DllImport(dllName, EntryPoint = "SDL_IsTextInputActive", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool IsTextInputActive();

			/// <summary>
			/// Sets the current key modifier state for the keyboard.
			/// </summary>
			/// <param name="modstate">The desired keymod for the keyboard.
			/// This value may be a bitwise, OR'd combination of keymod values.</param>
			[DllImport(dllName, EntryPoint = "SDL_SetModState", CallingConvention = CallingConvention.Cdecl)]
			public extern static void SetModState(Keymod modstate);

			/// <summary>
			/// Sets the rectange used to type Unicode text inputs.
			/// </summary>
			/// <param name="rect">The SDL rect structure representing the rectange to recieve text (ignored if null).</param>
			[DllImport(dllName, EntryPoint = "SDL_SetTextInputRect", CallingConvention = CallingConvention.Cdecl)]
			public extern static void SetTextInputRect(IntPtr rect);

			/// <summary>
			/// Start accepting Unicode text input events.
			/// On some platforms using this function activates the screen keyboard.
			/// </summary>
			[DllImport(dllName, EntryPoint = "SDL_StartTextInput", CallingConvention = CallingConvention.Cdecl)]
			public extern static void StartTextInput();

			/// <summary>
			/// Stop receiving any text input events.
			/// </summary>
			[DllImport(dllName, EntryPoint = "SDL_StopTextInput", CallingConvention = CallingConvention.Cdecl)]
			public extern static void StopTextInput();
			#endregion
		}
	}
}
#endif