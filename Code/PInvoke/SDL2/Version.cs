﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Used to collect or display information about the version of SDL that is currently being used by the program or that it was compiled against.
		/// </summary>
		public class Version
		{
			private const string dllName = "SDL2.dll";

			#region Structures
			/// <summary>
			/// Contains information about the version of SDL in use.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Number
			{
				/// <summary>
				/// Major version.
				/// </summary>
				public byte Major;

				/// <summary>
				/// Minor version.
				/// </summary>
				public byte Minor;

				/// <summary>
				/// Update version.
				/// (Patch level.)
				/// </summary>
				public byte Patch;
			}
			#endregion

			#region Functions
			/// <summary>
			/// Gets the code revision of SDL that is linked against your program.
			/// </summary>
			/// <returns>An arbitrary string, uniquely identifying the exact revision of the SDL library in use.</returns>
			public static string GetRevision() { return Marshal.PtrToStringAnsi(getRevision()); }
			[DllImport(dllName, EntryPoint = "SDL_GetRevision", CallingConvention = CallingConvention.Cdecl)]
			extern private static IntPtr getRevision();

			/// <summary>
			/// Gets the revision number of SDL that is linked against your program.
			/// </summary>
			/// <returns>A number uniquely identifying the exact revision of the SDL library in use.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetRevisionNumber", CallingConvention = CallingConvention.Cdecl)]
			extern public static int GetRevisionNumber();

			/// <summary>
			/// Gets the version of SDL that is linked against your program.
			/// </summary>
			/// <param name="ver">The Version.Number structure that contains the version information.</param>
			[DllImport(dllName, EntryPoint = "SDL_GetVersion", CallingConvention = CallingConvention.Cdecl)]
			extern public static void GetVersion(out Number ver);
			#endregion
		}
	}
}
#endif