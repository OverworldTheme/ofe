﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	// The functions in this category are used to set up SDL for use and generally have global effects in your program.

	/// <summary>
	/// A P/Invoke of the SDL2 library.
	/// </summary>
	internal static partial class SDL
	{
		private const string dllName = "SDL2.dll";

		#region Flags
		/// <summary>
		/// These are the flags which may be passed to SDL.Init().
		/// You should specify the subsystems which you will be using in your application.
		/// </summary>
		[Flags]
		public enum Subsystem
		{
			/// <summary>
			/// Timer subsystem.
			/// </summary>
			Timer = 0x00000001,

			/// <summary>
			/// Audio subsystem.
			/// </summary>
			Audio = 0x00000010,

			/// <summary>
			/// Video subsystem.
			/// Implies Init.Subsystem.Events.
			/// </summary>
			Video = 0x00000020,

			/// <summary>
			/// Joystick subsystem.
			/// Implies Init.Subsystem.Events.
			/// </summary>
			Joystick = 0x00000200,

			/// <summary>
			/// Haptic (force feedback) subsystem.
			/// </summary>
			Haptic = 0x00001000,

			/// <summary>
			/// Controller subsystem.
			/// Implies Init.Subsystem.Joystick.
			/// </summary>
			GameController = 0x00002000,

			/// <summary>
			/// Events subsystems.
			/// </summary>
			Events = 0x00004000,
			
			/// <summary>
			/// Compatibility; this flag is ignored.
			/// </summary>
			NoParachute=0x00100000,

			/// <summary>
			/// All subsystems.
			/// </summary>
			Everything = Timer | Audio | Video | Events | Joystick | Haptic | GameController
		}
		#endregion

		#region Functions
		/// <summary>
		/// Initializes the SDL library.
		/// This must be called before using any other SDL function.
		/// </summary>
		/// <param name="flags">Subsystem initialization flags.</param>
		/// <returns>0 on success or a negative error code on failure.
		/// Call Error.Get for more information.</returns>
		[DllImport(dllName, EntryPoint = "SDL_Init", CallingConvention = CallingConvention.Cdecl)]
		extern public static int Init(
			[MarshalAs(UnmanagedType.U4)]
			Subsystem flags);

		/// <summary>
		/// Initializes specific SDL subsystems.
		/// After SDL has been initialized with SDL.Init you may initialize uninitialized subsystems with this function.
		/// </summary>
		/// <param name="flags">Any of the flags used by SDL.Init.</param>
		/// <returns>0 on success or a negative error code on failure.
		/// Call Error.Get for more information.</returns>
		[DllImport(dllName, EntryPoint = "SDL_InitSubSystem", CallingConvention = CallingConvention.Cdecl)]
		extern public static int InitSubSystem(
			[MarshalAs(UnmanagedType.U4)]
			Subsystem flags);

		/// <summary>
		/// Clean up all initialized subsystems.
		/// You should call it upon all exit conditions.
		/// </summary>
		[DllImport(dllName, EntryPoint = "SDL_Quit", CallingConvention = CallingConvention.Cdecl)]
		extern public static void Quit();

		/// <summary>
		/// Shuts down specific SDL subsystems.
		/// </summary>
		/// <param name="flags">Any of the flags used by SDL.Init.</param>
		[DllImport(dllName, EntryPoint = "SDL_QuitSubSystem", CallingConvention = CallingConvention.Cdecl)]
		extern public static void QuitSubSystem(
			[MarshalAs(UnmanagedType.U4)]
			Subsystem flags);

		/// <summary>
		/// Returns a mask of the specified subsystems which have previously been initialized.
		/// </summary>
		/// <param name="flags">Any of the flags used by SDL.Init.</param>
		/// <returns>If flags is 0 it returns a mask of all initialized subsystems,
		/// otherwise it returns the initialization status of the specified subsystems.
		/// The return value does not include SDL.Subsystems.NoParachute.</returns>
		[DllImport(dllName, EntryPoint = "SDL_WasInit", CallingConvention = CallingConvention.Cdecl)]
		extern public static UInt32 WasInit(
			[MarshalAs(UnmanagedType.U4)]
			Subsystem flags);

		/// <summary>
		/// Checks if the specified subsystems have previously been initialized.
		/// </summary>
		/// <param name="flags">Any of the flags used by SDL.Init.</param>
		/// <returns>Will return false if any of the subsystems passed through flags have not been initialized.</returns>
		//public static bool WasInit(Subsystem flags)
		#endregion
	}
}
#endif