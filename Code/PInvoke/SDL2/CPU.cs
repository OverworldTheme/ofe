﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Functions for gathering information about the processor's features.
		/// </summary>
		public class CPU
		{
			private const string dllName = "SDL2.dll";

			#region Functions
			/// <summary>
			/// Determines the L1 cache line size of the CPU.
			/// </summary>
			/// <returns>The L1 cache line size of the CPU, in kilobytes.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetCPUCacheLineSize", CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetCacheLineSize();

			/// <summary>
			/// Returns the number of CPU cores available.
			/// </summary>
			/// <returns>The total number of logical CPU cores.
			/// On CPUs that include technologies such as hyperthreading,
			/// the number of logical cores may be more than the number of physical cores.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetCPUCount", CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetCPUCount();

			/// <summary>
			/// Gets the amount of RAM configured in the system.
			/// </summary>
			/// <returns>The amount of RAM configured in the system in MB.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetSystemRAM", CallingConvention = CallingConvention.Cdecl)]
			public extern static int GetSystemRAM();

			/// <summary>
			/// Determines whether the CPU has 3DNow! features.
			/// </summary>
			/// <returns>True if the CPU has 3DNow! features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_Has3DNow", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool Has3DNow();

			/// <summary>
			/// Determines whether the CPU has AVX features.
			/// </summary>
			/// <returns>True if the CPU has AVX features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasAVX", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasAVX();

			/// <summary>
			/// Determines whether the CPU has AVX2 features.
			/// </summary>
			/// <returns>True if the CPU has AVX2 features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasAVX2", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasAVX2();

			/// <summary>
			/// Determines whether the CPU has AltiVec features.
			/// </summary>
			/// <returns>True if the CPU has AltiVec features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasAltiVec", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasAltiVec();

			/// <summary>
			/// Determines whether the CPU has MMX features.
			/// </summary>
			/// <returns>True if the CPU has MMX features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasMMX", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasMMX();

			/// <summary>
			/// Determines whether the CPU has the RDTSC instruction.
			/// </summary>
			/// <returns>True if the CPU has the RDTSC function or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasRDTSC", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasRDTSC();

			/// <summary>
			/// Determines whether the CPU has SSE features.
			/// </summary>
			/// <returns>True if the CPU has SSE features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasSSE", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasSSE();

			/// <summary>
			/// Determines whether the CPU has SSE2 features.
			/// </summary>
			/// <returns>True if the CPU has SSE2 features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasSSE2", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasSSE2();

			/// <summary>
			/// Determines whether the CPU has SSE3 features.
			/// </summary>
			/// <returns>True if the CPU has SSE3 features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasSSE3", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasSSE3();

			/// <summary>
			/// Determines whether the CPU has SSE4.1 features.
			/// </summary>
			/// <returns>True if the CPU has SSE4.1 features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasSSE41", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasSSE41();

			/// <summary>
			/// Determines whether the CPU has SSE4.2 features.
			/// </summary>
			/// <returns>True if the CPU has SSE4.2 features or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasSSE42", CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasSSE42();
			#endregion
		}
	}
}
#endif