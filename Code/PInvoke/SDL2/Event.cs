﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Event handling allows your application to recieve input from the user.
		/// Internally, SDL stores all the events waiting to be handled in an event queue.
		/// Using functions like Event.Poll(), Event.Peep() and Event().Wait you can observe and handle waiting input events.
		/// </summary>
		public static class Event
		{
			private const string dllName = "SDL2.dll";

			#region Enumerators
			/// <summary>
			/// An enumeration of the types of events that can be delivered.
			/// </summary>
			public enum Type
			{
				/// <summary>
				/// Unused. (Do not remove.)
				/// </summary>
				First = 0,

				#region Application Events
				/// <summary>
				/// User-requested quit.
				/// </summary>
				Quit = 0x100,

				/// <summary>
				/// The application is being terminated by the OS.
				/// </summary>
				AppTerminating,

				/// <summary>
				/// The application is low on memory,
				/// free memory if possible.
				/// </summary>
				LowMemory,

				/// <summary>
				/// The application is about to enter the background.
				/// </summary>
				WillEnterBackground,

				/// <summary>
				/// The application did enter the background and may not get CPU for some time.
				/// </summary>
				DidEnterBackground,

				/// <summary>
				/// The application is about to enter the foreground.
				/// </summary>
				WillEnterForeground,

				/// <summary>
				/// The application is now interactive.
				/// </summary>
				DidEnterForeground,
				#endregion

				#region Window Events
				/// <summary>
				/// Window state change.
				/// </summary>
				WindowEvent = 0x200,

				/// <summary>
				/// System specific event.
				/// </summary>
				SysWMEvent,
				#endregion

				#region Keyboard Events
				/// <summary>
				/// Key pressed.
				/// </summary>
				KeyDown = 0x300,

				/// <summary>
				/// Key released.
				/// </summary>
				KeyUp,

				/// <summary>
				/// Keyboard text editing (composition).
				/// </summary>
				TextEditing,

				/// <summary>
				/// Keyboard text input.
				/// </summary>
				TextInput,
				#endregion

				#region MouseEvents
				/// <summary>
				/// Mouse moved.
				/// </summary>
				MouseMotion = 0x400,

				/// <summary>
				/// Mouse button pressed.
				/// </summary>
				MouseButtonDown,

				/// <summary>
				/// Mouse button released.
				/// </summary>
				MouseButtonUp,

				/// <summary>
				/// Mouse wheel motion.
				/// </summary>
				MouseWheel,
				#endregion

				#region Joystick
				/// <summary>
				/// Joystick axis motion.
				/// </summary>
				JoyAxisMotion = 0x600,

				/// <summary>
				/// Joystick trackball motion.
				/// </summary>
				JoyBallMotion,

				/// <summary>
				/// Joystick hat position change.
				/// </summary>
				JoyHatMotion,

				/// <summary>
				/// Joystick button pressed.
				/// </summary>
				JoyButtonDown,

				/// <summary>
				/// Joystick button released.
				/// </summary>
				JoyButtonUp,

				/// <summary>
				/// A new joystick has been inserted into the system.
				/// </summary>
				JoyDeviceAdded,

				/// <summary>
				/// An opened joystick has been removed.
				/// </summary>
				JoyDeviceRemoved,
				#endregion

				#region Game Controller Events
				/// <summary>
				/// Game controller axis motion.
				/// </summary>
				ControllerAxisMotion = 0x650,

				/// <summary>
				/// Game controller button pressed.
				/// </summary>
				ControllerButtonDown,

				/// <summary>
				/// Game controller button released.
				/// </summary>
				ControllerButtonUp,

				/// <summary>
				/// A new game controller has been inserted into the system.
				/// </summary>
				ControllerDeviceAdded,

				/// <summary>
				/// An opened game controller has been removed.
				/// </summary>
				ControllerDeviceRemoved,

				/// <summary>
				/// The controller mapping was updated.
				/// </summary>
				ControllerDeviceMapped,
				#endregion

				#region Touch Events
				/// <summary>
				/// User has touched input device.
				/// </summary>
				FingerDown = 0x700,

				/// <summary>
				/// User stopped touching input device.
				/// </summary>
				FingerUp,

				/// <summary>
				/// User is dragging finger on input device.
				/// </summary>
				FingerMotion,
				#endregion

				#region Gesture Events
				// These events are not currently documented by SDL.

				DollarGesture = 0x800,
				DollarRecord,
				MultiGesture,
				#endregion

				#region Clipboard Events
				/// <summary>
				/// The clipboard changed.
				/// </summary>
				ClipboardUpdate = 0x800,
				#endregion

				#region DragAndDropEvents
				/// <summary>
				/// The system requests a file open.
				/// </summary>
				DropFile = 0x1000,
				#endregion

				#region Audio Hotplug Events
				/// <summary>
				/// A new audio device is available.
				/// </summary>
				AudioDeviceAdded = 0x1100,

				/// <summary>
				/// An audio device has been removed.
				/// </summary>
				AudioDeviceRemoved,
				#endregion

				#region Render Events
				/// <summary>
				/// The render targets have been reset and their contents need to be updated.
				/// </summary>
				RenderTargetsReset = 0x2000,

				/// <summary>
				/// The device has been reset and all textures need to be recreated.
				/// </summary>
				RenderDeviceReset,
				#endregion

				#region User Events
				/// <summary>
				/// Values Type.UserEvent through Event.LastEvent are for your use,
				/// and should be allocated with Event.Register().
				/// </summary>
				UserEvent = 0x8000,
				#endregion

				/// <summary>
				/// The last event is only for bounding internal arrays.
				/// </summary>
				Last = 0xFFFF
			}

			/// <summary>
			/// An action to take on the SDL event queue.
			/// </summary>
			public enum Action
			{
				/// <summary>
				/// Adds events to the back of the event queue.
				/// </summary>
				AddEvent,

				/// <summary>
				/// Returns events without removing them from the queue.
				/// </summary>
				PeekEvent,

				/// <summary>
				/// Returns events and removes them from the queue.
				/// </summary>
				GetEvent
			}

			/// <summary>
			/// Represents the processing state of an event type.
			/// </summary>
			public enum StateProcess
			{
				/// <summary>
				/// Returns the current processing state of the specified event.
				/// </summary>
				Query = -1,

				/// <summary>
				/// The event will automatically be dropped from the event queue and will not be filtered.
				/// </summary>
				Ignore = 0,

				/// <summary>
				/// The event will be processed normally.
				/// </summary>
				Enable = 1,
			}
			#endregion

			#region Structures
			/// <summary>
			/// Generic event structure.
			/// Not present in actual SDL because SDL events are unions.
			/// In C#, we need a way to get an event and then interpret different event structures.
			/// </summary>
			[StructLayout(LayoutKind.Explicit, Size = 256)]
			public struct Generic
			{
				/// <summary>
				/// The type of event this is.
				/// </summary>
				[FieldOffset(0)]
				public Event.Type Type;

				// Everything after this is just making room for different structures.
				// Use them to pull the actual event structure out.
								
				[FieldOffset(0)]
				public Keyboard.Event Key;
				[FieldOffset(0)]
				public Window.Event Window;
				[FieldOffset(0)]
				public QuitEvent Quit;
			}

			/// <summary>
			/// A structure that contains the "quit requested" event.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct QuitEvent
			{
				/// <summary>
				/// EventType.Quit.
				/// </summary>
				public Event.Type Type;

				/// <summary>
				/// Timestamp of the event.
				/// </summary>
				public uint timestamp;
			}
			#endregion

			#region Functions
			/// <summary>
			/// Use this function to set the state of processing events by type.
			/// </summary>
			/// <param name="type">The type of event.</param>
			/// <param name="state">How to process the event.</param>
			/// <returns>Returns 0 (disable) or 1 (enable),
			/// representing the processing state of the event before this function makes any changes to it.</returns>
			[DllImport(dllName, EntryPoint = "SDL_EventState", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static byte State(Event.Type type, StateProcess state);

			/// <summary>
			/// Checks for the existence of certain event types in the event queue.
			/// </summary>
			/// <param name="type">The type of event to be queried.</param>
			/// <returns>True if events matching type are present,
			/// or false if events matching type are not present.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasEvent", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasEvent(Type type);

			/// <summary>
			/// Checks for the existence of a range of event types in the event queue.
			/// </summary>
			/// <param name="minType">The minimum type of event to be queried.</param>
			/// <param name="maxType">The maximum type of event to be queried.</param>
			/// <returns>True if events with types in the range between minType and maxType are present,
			/// or false if not.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HasEvents", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static bool HasEvents(Type minType, Type maxType);

			/// <summary>
			/// Polls currently pending events.
			/// If sdlEvent if not null, the next event is removed from the queue and stored in the SDL event structure pointed to by sdlEvent.
			/// If sdlEvent is null, is simply returns 1 if there is an event in the queue, but will not remove it.
			/// </summary>
			/// <param name="sdlEvent">The SDL event structure to be filled with the next event from the queue, or null.</param>
			/// <returns>1 if there is apending event or 0 if none are available.</returns>
			[DllImport(dllName, EntryPoint = "SDL_PollEvent", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int Poll(out Event.Generic sdlEvent);

			/// <summary>
			/// Checks the event queue for messages and optionally returns them.
			/// You may have to call Event.Pump() before calling this function.
			/// Otherwise, the events may not be ready to be filtered.
			/// </summary>
			/// <param name="events">Destination buffer for the retrieved events.</param>
			/// <param name="numEvents">If action is AddEvent, the number of events to add back to the event queue;
			/// if action is PeekEvent or GetEvent, the maximum number of events to retrieve.</param>
			/// <param name="action">Action to take.</param>
			/// <param name="minType">Minimum value of the event type to be considered; Type.FirstEvent is a safe choice.</param>
			/// <param name="maxType">Maximum value of the event type to be considered; Type.LastEvent is a sage choice.</param>
			/// <returns>The number of events actually stored or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_PeepEvents", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			public extern static int Peep(out Event.Generic[] events, int numEvents, Action action, Type minType, Type maxType);

			/// <summary>
			/// <para>Use this function to set up a filter to process all events before they change internal state
			/// and are posted to the internal event queue.</para>
			/// <para>Be very careful of what you do in the event filter funtion,
			/// as it may run in a different thread!</para>
			/// </summary>
			/// <param name="filter">The function to call when an event happens.</param>
			/// <param name="userdata">A pointer that is passed to filter.</param>
			public static void SetFilter(Func<IntPtr, SDL.Event.Generic, int> filter, IntPtr userdata)
			{
				setFilter(Marshal.GetFunctionPointerForDelegate(filter), userdata);
			}
			[DllImport(dllName, EntryPoint = "SDL_SetEventFilter", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
			private extern static void setFilter(IntPtr filter, IntPtr userdata);
			#endregion
		}
	}
}
#endif