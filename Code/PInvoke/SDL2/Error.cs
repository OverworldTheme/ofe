﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// <para>Functions in this category provide simple error message routines for SDL.
		/// Error.Get() can be called for almost all SDL functions to determine what problems are occurring.
		/// Check the wiki page of each specific SDL function to see whether Error.Get() is meaningful for them or not.</para>
		/// <para>The SDL error messages are in English.</para>
		/// </summary>
		public class Error
		{
			private const string dllName = "SDL2.dll";

			#region Functions
			/// <summary>
			/// Clears any previous error message.
			/// </summary>
			[DllImport(dllName, EntryPoint = "SDL_ClearError", CallingConvention = CallingConvention.Cdecl)]
			extern public static void Clear();

			/// <summary>
			/// Retrieves a message about the last error that occurred.
			/// </summary>
			/// <return>
			/// <para>Returns a message with information about the specific error that occurred,
			/// or an empty string if there hasn't been an error since the last call to Clear().
			/// Without calling Clear(), the message is only applicable when an SDL function has signaled an error.
			/// You must check the return values of SDL function calls to determine when to appropriately call Get().</para>
			/// <para>This string is statically allocated and must not be freed by the application.</para>
			/// </return>
			public static string Get() { return Marshal.PtrToStringAnsi(get()); }
			[DllImport(dllName, EntryPoint = "SDL_GetError", CallingConvention = CallingConvention.Cdecl)]
			extern private static IntPtr get();

			/// <summary>
			/// Sets the SDL error string.
			/// </summary>
			/// <param name="fmt">A C printf() style message format string.</param>
			/// <param name="parameters">Additional parameters matching % tokens in the fmt string, if any.</param>
			/// <returns>Always -1.</returns>
			[DllImport(dllName, EntryPoint = "SDL_SetError", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
			extern public static int Set(string fmt, params string[] parameters);
			#endregion
		}
	}
}
#endif