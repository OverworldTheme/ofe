﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// SDL2 haptic feedback bindings.
		/// </summary>
		public static class Haptic
		{
			private const string dllName = "SDL2.dll";

			#region Constants
			/// <summary>
			/// Used to play a device an infinite number of times.
			/// </summary>
			public const UInt32 Infinity = UInt32.MaxValue;
			#endregion

			#region Enumerations
			public enum EffectType
			{
				/// <summary>
				/// Constant haptic effect.
				/// Use the Haptic.Constant struct.
				/// </summary>
				Constant = 1 << 0,

				/// <summary>
				/// Periodic haptic effect that simulates sine waves.
				/// Use the Haptic.Periodic struct.
				/// </summary>
				Sine = 1 << 1,

				/// <summary>
				/// Haptic effect for direct control over high/low frequency motors.
				/// Use the Haptic.LeftRight struct.
				/// </summary>
				LeftRight = 1 << 2,

				/// <summary>
				/// Periodic haptic effect that simulates triangular waves.
				/// Use the Haptic.Periodic struct.
				/// </summary>
				Triangle = 1 << 3,

				/// <summary>
				/// Periodic haptic effect that simulates saw tooth up waves.
				/// Use the Haptic.Periodic struct.
				/// </summary>
				SawToothUp = 1 << 4,

				/// <summary>
				/// Periodic haptic effect that simulates saw tooth down waves.
				/// Use the Haptic.Periodic struct.
				/// </summary>
				SawToothDown = 1 << 5,

				/// <summary>
				/// Ramp haptic effect.
				/// Use the Haptic.Ramp struct.
				/// </summary>
				Ramp = 1 << 6,

				/// <summary>
				/// Condition haptic effect that simulates a spring.
				/// Effect is based on the aces position.
				/// Use the Haptic.Condition struct.
				/// </summary>
				Spring = 1 << 7,

				/// <summary>
				/// Condition haptic effect that simulates dampening.
				/// Effect is based on the axes velocity.
				/// Use the Haptic.Condition struct.
				/// </summary>
				Damper = 1 << 8,

				/// <summary>
				/// Condition haptic effect that simulates friction.
				/// Effect is based on the axes movement.
				/// Use the Haptic.Condition struct.
				/// </summary>
				Inertia = 1 << 9,

				/// <summary>
				/// Condition haptic effect that simulates friction.
				/// Effect is based on the axes movement.
				/// Use the Haptic.Condition struct.
				/// </summary>
				Friction = 1 << 10,

				/// <summary>
				/// User defined custom haptic effect.
				/// Use the Haptic.Custom struct.
				/// </summary>
				Custom = 1 << 11,
			}

			/// <summary>
			/// Features that a haptic device may have.
			/// </summary>
			[Flags]
			public enum Feature
			{
				/// <summary>
				/// Device supports setting the global gain.
				/// </summary>
				SetGain = 1 << 12,

				/// <summary>
				/// Device supports setting autocenter.
				/// </summary>
				Autocenter = 1 << 13,

				/// <summary>
				/// Device can be queried for effect status.
				/// </summary>
				Status = 1 << 14,

				/// <summary>
				/// Device can be paused.
				/// </summary>
				Pause = 1 << 15,
			}

			/// <summary>
			/// Coordinate systems used for direction encodings.
			/// </summary>
			public enum CoordSystem
			{
				/// <summary>
				/// Uses polar coordinates for the direction.
				/// </summary>
				Polar = 0,

				/// <summary>
				/// Uses cartestian coordinates for the direction.
				/// </summary>
				Cartesian = 1,

				/// <summary>
				/// Uses spherical coordinates for the direction.
				/// </summary>
				Spherical = 1
			}
			#endregion

			#region Structures
			/// <summary>
			/// A template used for a haptic effect.
			/// </summary>
			public interface IEffectTemplate { }

			/// <summary>
			/// A structure that contains a template for a condition effect.
			/// </summary>
			[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
			public struct Condition : IEffectTemplate
			{
				/// <summary>
				/// One of the effects handled by this structure:
				/// EffectType.Spring, EffectType.Damper, EffectType.Inertia, or EffectType.Friction.
				/// </summary>
				[MarshalAs(UnmanagedType.U2)]
				public EffectType Type;

				/// <summary>
				/// Direction of the effect.
				/// (Currently unimplemented on SDL's end.)
				/// </summary>
				[MarshalAs(UnmanagedType.U4)]
				public Direction Direction;

				/// <summary>
				/// Duration of the effect.
				/// </summary>
				public UInt32 Length;

				/// <summary>
				/// Delay before starting the effect.
				/// </summary>
				public UInt16 Delay;

				/// <summary>
				/// Button that triggers the effect.
				/// </summary>
				public UInt16 Button;

				/// <summary>
				/// How soon it can be triggered again after button.
				/// </summary>
				public UInt16 Interval;

				/// <summary>
				/// Level when joystick is to the positive side.
				/// Max 0xFFFF.
				/// </summary>
				public UInt16 RightSat;

				/// <summary>
				/// Level when joystick is to the negative side.
				/// Max 0xFFFF.
				/// </summary>
				public UInt16 LeftSat;

				/// <summary>
				/// How fast to increase the force towards the positive side.
				/// </summary>
				public Int16 RightCoeff;

				/// <summary>
				/// How fast to increase the force towards the negative side.
				/// </summary>
				public Int16 LeftCoeff;

				/// <summary>
				/// Size of the dead zone; max 0xFFFF:
				/// whole axis-range when 0-centered.
				/// </summary>
				public UInt16 Deadband;

				/// <summary>
				/// Position of the dead zone.
				/// </summary>
				public Int16 Center;
			}

			/// <summary>
			/// A structure that contains a template for a constant effect.
			/// A constant effect applies a constant force to the joystick in the specified direction.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Constant : IEffectTemplate
			{
				/// <summary>
				/// Must be EffectType.Constant.
				/// </summary>
				[MarshalAs(UnmanagedType.U2)]
				public EffectType Type;

				/// <summary>
				/// Direction of the effect.
				/// </summary>
				[MarshalAs(UnmanagedType.U4)]
				public Direction Direction;

				/// <summary>
				/// Duration of the effect.
				/// </summary>
				public UInt32 Length;

				/// <summary>
				/// Delay before starting the effect.
				/// </summary>
				public UInt16 Delay;

				/// <summary>
				/// Button that triggers the effect.
				/// </summary>
				public UInt16 Button;

				/// <summary>
				/// How soon it can be triggered again after button.
				/// </summary>
				public UInt16 Interval;

				/// <summary>
				/// Strength of the effect.
				/// </summary>
				public Int16 Level;

				/// <summary>
				/// Duration of the envelope's attack.
				/// </summary>
				public UInt16 AttackLength;

				/// <summary>
				/// Level at the start of the envelope's attack.
				/// </summary>
				public UInt16 AttackLevel;

				/// <summary>
				/// Duration of the envelope's fade.
				/// </summary>
				public UInt16 FadeLength;

				/// <summary>
				/// Level at the end of the fade.
				/// </summary>
				public UInt16 FadeLevel;
			}

			/// <summary>
			/// A structure that contains a template for a custom haptic effect.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Custom : IEffectTemplate
			{
				/// <summary>
				/// Must be EffectType.Custom.
				/// </summary>
				[MarshalAs(UnmanagedType.U2)]
				public EffectType Type;

				/// <summary>
				/// Direction of the effect, relative to the user.
				/// </summary>
				[MarshalAs(UnmanagedType.U4)]
				public Direction Direction;

				/// <summary>
				/// Duration of the effect.
				/// </summary>
				public UInt32 Length;

				/// <summary>
				/// Delay before starting the effect.
				/// </summary>
				public UInt16 Delay;

				/// <summary>
				/// Button that triggers the effect.
				/// </summary>
				public UInt16 Button;

				/// <summary>
				/// How soon it can be triggered again after Button.
				/// </summary>
				public UInt16 Interval;

				/// <summary>
				/// Axes to use, minimum of 1.
				/// </summary>
				public byte Channels;

				/// <summary>
				/// Sample periods.
				/// </summary>
				public UInt16 Period;

				/// <summary>
				/// Amount (number) of samples.
				/// </summary>
				public UInt16 Samples;

				/// <summary>
				/// Should contain Channels*Samples items.
				/// </summary>
				[MarshalAs(UnmanagedType.LPArray)]
				public UInt16[] Data;

				/// <summary>
				/// Strength of the effect.
				/// </summary>
				public Int16 Level;

				/// <summary>
				/// Duration of the envelope's attack.
				/// </summary>
				public UInt16 AttackLength;

				/// <summary>
				/// Level at the start of the envelope's attack.
				/// </summary>
				public UInt16 AttackLevel;

				/// <summary>
				/// Duration of the envelope's fade.
				/// </summary>
				public UInt16 FadeLength;

				/// <summary>
				/// Level at the end of the fade.
				/// </summary>
				public UInt16 FadeLevel;
			}

			/// <summary>
			/// A structure that contains a template for a haptic direction effect.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Direction
			{
				/// <summary>
				/// The type of encoding.
				/// </summary>
				[MarshalAs(UnmanagedType.U1)]
				public CoordSystem Type;

				/// <summary>
				/// The encoded direction.
				/// This is the direction where the force comes from,
				/// instead of the direction in which the force is exerted.
				/// </summary>
				public Int32 Dir;
			}

			/// <summary>
			/// A template for a Left/Right effect.
			/// This effect is used to explicitly control the large and small motors,
			/// commonly found in the modern game controllers.
			/// One motor is high frequency,
			/// the other is low frequency.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct LeftRight : IEffectTemplate
			{
				public UInt16 Type;

				/// <summary>
				/// Duration of the effect.
				/// </summary>
				public UInt32 Length;

				/// <summary>
				/// Control of the large controller motor.
				/// </summary>
				public UInt16 LargeMagnitude;

				/// <summary>
				/// Control of the small controller motor.
				/// </summary>
				public UInt16 SmallMagnitude;
			}

			/// <summary>
			/// A structure that contains a template for a periodic effect.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Periodic : IEffectTemplate
			{
				/// <summary>
				/// The shape of the waves.
				/// Valid values are:
				/// EffectType.Sine, EffectType.Square, EffectType.Triangle, EffectType.SawToothUp, and EffectType.SawToothDown.
				/// </summary>
				[MarshalAs(UnmanagedType.U2)]
				public EffectType Type;

				/// <summary>
				/// Direction of the effect.
				/// </summary>
				[MarshalAs(UnmanagedType.U4)]
				public Direction Direction;

				/// <summary>
				/// Duration of the effect.
				/// </summary>
				public UInt32 Length;

				/// <summary>
				/// Delay before starting the effect.
				/// </summary>
				public UInt16 Delay;

				/// <summary>
				/// Button that triggers the effect.
				/// </summary>
				public UInt16 Button;

				/// <summary>
				/// How soon it can be triggered again after button.
				/// </summary>
				public UInt16 Interval;

				/// <summary>
				/// Period of the wave.
				/// </summary>
				public UInt16 Period;

				/// <summary>
				/// Peak value;
				/// if negative, equivalent to 180 degrees extra phase shift.
				/// </summary>
				public Int16 Magnitude;

				/// <summary>
				/// Mean value of the wave.
				/// </summary>
				public Int16 Offset;

				/// <summary>
				/// Positive phase shift given by hundredths of a degree.
				/// </summary>
				public UInt16 Phase;

				/// <summary>
				/// Duration of the envelope's attack.
				/// </summary>
				public UInt16 AttackLength;

				/// <summary>
				/// Level at the start of the envelope's attack.
				/// </summary>
				public UInt16 AttackLevel;

				/// <summary>
				/// Duration of the envelope's fade.
				/// </summary>
				public UInt16 FadeLength;

				/// <summary>
				/// Level at the end of the fade.
				/// </summary>
				public UInt16 FadeLevel;
			}

			/// <summary>
			/// A structure that contains a template for a ramp effect.
			/// The ramp effect starts at Start strength and ends at End strength.
			/// It augments in linear fashion.
			/// </summary>
			[StructLayout(LayoutKind.Sequential)]
			public struct Ramp : IEffectTemplate
			{
				/// <summary>
				/// Must be EffectType.Ramp.
				/// </summary>
				[MarshalAs(UnmanagedType.U2)]
				public EffectType Type;

				/// <summary>
				/// Direction of the effect.
				/// </summary>
				[MarshalAs(UnmanagedType.U4)]
				public Direction Direction;

				/// <summary>
				/// Duration of the effect.
				/// </summary>
				public UInt32 Length;

				/// <summary>
				/// Delay before starting the effect.
				/// </summary>
				public UInt16 Delay;

				/// <summary>
				/// Button that triggers the effect.
				/// </summary>
				public UInt16 Button;

				/// <summary>
				/// How soon it can be triggered again after button.
				/// </summary>
				public UInt16 Interval;

				/// <summary>
				/// Beginning strength level.
				/// </summary>
				public Int16 Start;

				/// <summary>
				/// Ending strength level.
				/// </summary>
				public Int16 End;

				/// <summary>
				/// Duration of the envelope's attack.
				/// </summary>
				public UInt16 AttackLength;

				/// <summary>
				/// Level at the start of the envelope's attack.
				/// </summary>
				public UInt16 AttackLevel;

				/// <summary>
				/// Duration of the envelope's fade.
				/// </summary>
				public UInt16 FadeLength;

				/// <summary>
				/// Level at the end of the fade.
				/// </summary>
				public UInt16 FadeLevel;
			}
			#endregion

			#region Functions
			/// <summary>
			/// Closes a haptic device previously opened with Open().
			/// </summary>
			/// <param name="haptic">The haptic device to close.</param>
			[DllImport(dllName, EntryPoint = "SDL_HapticClose", CallingConvention = CallingConvention.Cdecl)]
			extern public static void Close(IntPtr haptic);

			/// <summary>
			/// Destroys a haptic effect on the device.
			/// Will stop the effect if it's running.
			/// </summary>
			/// <param name="haptic">The haptic device to destroy the effect on.</param>
			/// <param name="effect">The ID indentifier of the haptic effect to destroy.</param>
			[DllImport(dllName, EntryPoint = "SDL_HapticDestroyEffect", CallingConvention = CallingConvention.Cdecl)]
			extern public static void DestroyEffect(IntPtr haptic, int effect);

			/// <summary>
			/// Checks to see if an effect is supported by a haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to query.</param>
			/// <param name="effect">The desired effect to check to see if it is supported.</param>
			/// <returns>1 if effect is supported, false if it isn't, or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticEffectSupported", CallingConvention = CallingConvention.Cdecl)]
			extern public static int EffectSupported(IntPtr haptic, IEffectTemplate effect);

			/// <summary>
			/// Gets the status of the current effect on the specified haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to quety for the effect status on.</param>
			/// <param name="effect">The ID identifier of the haptic effect to query its status.</param>
			/// <returns>0 if it isn't playing, 1 if it is playing, or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticGetEffectStatus", CallingConvention = CallingConvention.Cdecl)]
			extern public static int GetEffectStatus(IntPtr haptic, int effect);

			/// <summary>
			/// Gets the index of a haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to get the index of.</param>
			/// <returns>Returns the index of the specified haptic device or a negative error code on gailure;
			/// call Erro.Gar() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticIndex", CallingConvention = CallingConvention.Cdecl)]
			extern public static int Index(IntPtr haptic);

			/// <summary>
			/// Gets the implementation dependent name of a haptic device.
			/// </summary>
			/// <param name="haptic">Index of the device to query the name of.</param>
			/// <returns>The name of the device or null on failure;
			/// call Error.Get() for more information.</returns>
			public static string Name(int deviceIndex) { return Marshal.PtrToStringAnsi(name(deviceIndex)); }
			[DllImport(dllName, EntryPoint = "SDL_HapticName", CallingConvention = CallingConvention.Cdecl)]
			extern private static IntPtr name(int deviceIndex);

			/// <summary>
			/// Creates a new haptic effect on a specified device.
			/// </summary>
			/// <param name="haptic">The haptic device to create the effect on.</param>
			/// <param name="effect">A haptic effect structure containing the properties of the effect to create.</param>
			/// <returns>The ID of the effect on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticNewEffect", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NewEffect(IntPtr haptic, IEffectTemplate effect);

			/// <summary>
			/// Gets the number of haptic axes the device has.
			/// </summary>
			/// <param name="haptic">The haptic device to query.</param>
			/// <returns>The number of axes on success or a negative error code on failure;
			/// call Error.Get for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticNumAxes", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumAxes(IntPtr haptic);

			/// <summary>
			/// Gets the number of effects a haptic device can store.
			/// </summary>
			/// <param name="haptic">The haptic device to query.</param>
			/// <returns>The number of effects the haptic device can store or a negative error code on failure;
			/// call Error.Get for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticNumEffects", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumEffects(IntPtr haptic);

			/// <summary>
			/// Gets the number of effects a haptic device can play at the same time.
			/// </summary>
			/// <param name="haptic">The haptic device to query.</param>
			/// <returns>The number of effects the haptic device can play at the same time or a negative error code on failure;
			/// call Error.Get for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticNumEffectsPlaying", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumEffectsPlaying(IntPtr haptic);

			/// <summary>
			/// Opens a haptic device for usage.
			/// </summary>
			/// <param name="deviceIndex">Index of the device to open.</param>
			/// <returns>The device identifier or null on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticOpen", CallingConvention = CallingConvention.Cdecl)]
			extern public static IntPtr Open(int deviceIndex);

			/// <summary>
			/// Opens a haptic device for use from a joystick device.
			/// </summary>
			/// <param name="joystick">Joystick to create a haptic device from.</param>
			/// <returns>A valid haptic device identifier on success or null on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticOpenFromJoystick", CallingConvention = CallingConvention.Cdecl)]
			extern public static IntPtr OpenFromJoystick(IntPtr joystick);

			/// <summary>
			/// Tries to open a haptic device from the current mouse.
			/// </summary>
			/// <returns>A valid haptic device identifier or null on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticOpenFromMouse", CallingConvention = CallingConvention.Cdecl)]
			extern public static IntPtr OpenFromMouse();

			/// <summary>
			/// Checks if the haptic device at the designated index has been opened.
			/// </summary>
			/// <param name="deviceIndex">The index of the device to query to check to see if it has been opened.</param>
			/// <returns>Returns 1 if it has been opened,
			/// 0 if it hasn't or on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticOpened", CallingConvention = CallingConvention.Cdecl)]
			extern public static int Opened(int deviceIndex);

			/// <summary>
			/// Pauses a haptic device.
			/// Device must support Feature.Pause.
			/// Call Unpause() to resume playback.
			/// Do not modify the effects nor add new ones while the device is paused.
			/// </summary>
			/// <param name="haptic">The haptic device to pause.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// called Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticPause", CallingConvention = CallingConvention.Cdecl)]
			extern public static int Pause(IntPtr haptic);

			/// <summary>
			/// Gets the haptic device's supported features in a bitwise manner.
			/// </summary>
			/// <param name="haptic">The haptic device to query.</param>
			/// <returns>A list of supported haptic features in bitwise manner (OR'd),
			/// or a negative error code on failure;
			/// call Error.Get() for more informaiton.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticQuery", CallingConvention = CallingConvention.Cdecl)]
			[return: MarshalAs(UnmanagedType.U4)]
			extern public static Feature Query(IntPtr haptic);

			/// <summary>
			/// Initializes a haptic device for simple rumble playback.
			/// </summary>
			/// <param name="haptic">The device to initialize.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticRumbleInit", CallingConvention = CallingConvention.Cdecl)]
			extern public static int RumbleInit(IntPtr haptic);

			/// <summary>
			/// Runs a simple rumble effect on a haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to play the rumble effect on.</param>
			/// <param name="strength">Strength of the rumble to play as a 0-1 float value.</param>
			/// <param name="length">Length of the rumble to play in milliseconds.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticRumblePlay", CallingConvention = CallingConvention.Cdecl)]
			extern public static int RumblePlay(IntPtr haptic, float strength, UInt32 length);

			/// <summary>
			/// Stop the simple rumble on a haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to stop the rumble effect on.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticRumbleStop", CallingConvention = CallingConvention.Cdecl)]
			extern public static int RumbleStop(IntPtr haptic);

			/// <summary>
			/// Checks whether rumble is supported on a haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to check for rumble support.</param>
			/// <returns>1 if effect is support, false if it isn't, or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticRumbleSupported", CallingConvention = CallingConvention.Cdecl)]
			extern public static int RumbleSupported(IntPtr haptic);

			/// <summary>
			/// Runs the haptic effect on its associated haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to run the effect on.</param>
			/// <param name="effect">The ID identifier of the haptic effect to run.</param>
			/// <param name="iterations">The number of iterations to runn the effect;
			/// use Haptic.Infinity for infinity,
			/// or the number of times to repeat the effect.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// see Error.Ger() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticRunEffect", CallingConvention = CallingConvention.Cdecl)]
			extern public static int RunEffect(IntPtr haptic, int effect, UInt32 iterations);

			/// <summary>
			/// Sets the global autocenter of the device.
			/// </summary>
			/// <param name="haptic">The haptic device to set autocentering on.</param>
			/// <param name="autocenter">Value to set autocenter to (0-100),
			/// 0 disables autocentering.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticSetAutocenter", CallingConvention = CallingConvention.Cdecl)]
			extern public static int Autocenter(IntPtr haptic, int autocenter);

			/// <summary>
			/// Sets the gloval fain of the specified haptic device.
			/// Device must support Feature.Autocenter.
			/// </summary>
			/// <param name="haptic">The haptic device to set the gain on.</param>
			/// <param name="gain">Value to set the gain to, should be between 0 and 100.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get for more informaiton.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticSetGain", CallingConvention = CallingConvention.Cdecl)]
			extern public static int SetGain(IntPtr haptic, int gain);

			/// <summary>
			/// Stops all the currently playing effects on a haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to stop.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticStopAll", CallingConvention = CallingConvention.Cdecl)]
			extern public static int StopAll(IntPtr haptic);

			/// <summary>
			/// Stops the haptic effect on its associated haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to stop the effect on.</param>
			/// <param name="effect">The ID identifier of the haptic effect ot stop.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticStopEffect", CallingConvention = CallingConvention.Cdecl)]
			extern public static int StopEffect(IntPtr haptic, int effect);

			/// <summary>
			/// Unpauses a haptic device.
			/// </summary>
			/// <param name="haptic">The haptic device to unpause.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticUnpause", CallingConvention = CallingConvention.Cdecl)]
			extern public static int Unpause(IntPtr haptic);

			/// <summary>
			/// Updates the properties of an effect.
			/// </summary>
			/// <param name="haptic">The haptic device that has the effect.</param>
			/// <param name="effect">The index of the effect to update.</param>
			/// <param name="data">A haptic effect structure contiaining the new effect properties to use.</param>
			/// <returns>0 on success or a -1 on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_HapticUpdateEffect", CallingConvention = CallingConvention.Cdecl)]
			extern public static int UpdateEffect(IntPtr haptic, int effect, IEffectTemplate data);

			/// <summary>
			/// Counts the number of haptic devices attached to the system.
			/// </summary>
			/// <returns>The number of haptic devices detected on the system or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_NumHaptics", CallingConvention = CallingConvention.Cdecl)]
			extern public static int Num();
			#endregion
		}
	}
}
#endif
