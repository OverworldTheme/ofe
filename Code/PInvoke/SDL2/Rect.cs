﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Defines a rectangle, with the origin at the upper left.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct Rect
		{
			/// <summary>
			/// The x location of the rectangle's upper left corner.
			/// </summary>
			public int x;

			/// <summary>
			/// The y location of the rectangle's upper left corner.
			/// </summary>
			public int y;

			/// <summary>
			/// The width of the rectangle.
			/// </summary>
			public int w;

			/// <summary>
			/// The height of the rectangle.
			/// </summary>
			public int h;
		}
	}
}
#endif