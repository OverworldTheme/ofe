﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Functions for displaying message boxes.
		/// </summary>
		class MessageBox
		{
			private const string dllName = "SDL2.dll";

			#region Enumerations
			/// <summary>
			/// Message box flags (e.g. if supported message box will display warning icon).
			/// </summary>
			public enum MessageBoxFlag
			{
				/// <summary>
				/// Error dialog.
				/// </summary>
				Error = 0x10,

				/// <summary>
				/// Warning dialog.
				/// </summary>
				Warning = 0x20,

				/// <summary>
				/// Information dialog.
				/// </summary>
				Information = 0x40
			}
			#endregion

			#region Functions
			/// <summary>
			/// Creates a modal message box.
			/// </summary>
			/// <param name="MessageBoxData">The MessageBoxData structure with title, text, and other options.</param>
			/// <param name="buttonid">The pointer to which user ID of hit button should be copied.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_ShowMessageBox", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static int Show(IntPtr MessageBoxData, out int buttonid);

			/// <summary>
			/// Displays a simple modal message box.
			/// This function may be called at any time,
			/// even before SDL.Init().
			/// </summary>
			/// <param name="flags">A MessageBoxFlag.
			/// Ban be any of the following:
			/// Error, Warning, or Information.</param>
			/// <param name="title">Title text.</param>
			/// <param name="message">Message text.</param>
			/// <param name="window">The parent window, or null for no parent.</param>
			/// <returns></returns>
			[DllImport(dllName, EntryPoint = "SDL_ShowSimpleMessageBox", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true, CharSet = CharSet.Ansi)]
			public extern static int ShowSimple(MessageBoxFlag flags, string title, string message, IntPtr window);
			#endregion
		}
	}
}
#endif