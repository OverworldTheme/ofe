﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Function for handling SDL power management routines.
		/// </summary>
		public static class Power
		{
			private const string dllName = "SDL2.dll";

			#region Enumerations
			/// <summary>
			/// An enumeration of the system's power supply.
			/// </summary>
			public enum State
			{
				/// <summary>
				/// Cannot determine power status.
				/// </summary>
				Unknown = 0,

				/// <summary>
				/// Not plugged in, running on the battery.
				/// </summary>
				OnBattery = 1,

				/// <summary>
				/// Plugged in, no battery available.
				/// </summary>
				NoBattery = 2,

				/// <summary>
				/// Plugged in, charging battery.
				/// </summary>
				Charging = 3,

				/// <summary>
				/// Plugged in, battery charged.
				/// </summary>
				Charged = 4,
			}
			#endregion

			#region
			/// <summary>
			/// Gets the current power supply details.
			/// </summary>
			/// <param name="secs">Seconds of battery life left.
			/// You can pass a null here if you don't care.
			/// Will return -1 if we can't determine a value,
			/// or we're not running on a battery.</param>
			/// <param name="pct">Percentage of battery life left, between 0 and 100.
			/// You can pass a null here if you don't care.
			/// Will return -1 if we can't determine a value,
			/// or we're not running on a battery.</param>
			/// <returns>The state of the battery, if any.</returns>
			[DllImport(dllName, EntryPoint = "SDL_GetPowerInfo", CallingConvention = CallingConvention.Cdecl, ExactSpelling = true)]
			public extern static State GetInfo(out int secs, out int pct);
			#endregion
		}
	}
}
#endif