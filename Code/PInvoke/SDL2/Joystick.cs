﻿#if SDL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SDL2
{
	internal static partial class SDL
	{
		/// <summary>
		/// Possible hat positions.
		/// </summary>
		public enum Hat
		{
			Centered = 0x00,
			Up = 0x01,
			Right = 0x02,
			Down = 0x04,
			Left = 0x08,
			RightUp = (Right | Up),
			RightDown = (Right | Down),
			LeftUp = (Left | Up),
			LeftDown = (Left | Down)
		}

		/// <summary>
		/// Functions for handling inputs from joysticks.
		/// </summary>
		public static class Joystick
		{
			private const string dllName = "SDL2.dll";

			#region enums
			public enum State
			{
				Query = -1,
				Ignore = 0,
				Enable = 1
			}
			#endregion

			#region Functions
			/// <summary>
			/// Close a joystick previously opened with Open.
			/// </summary>
			/// <param name="joystick">A joystick identifier containing joystick information.</param>
			[DllImport(dllName, EntryPoint = "SDL_JoystickClose", CallingConvention = CallingConvention.Cdecl)]
			extern public static void Close(IntPtr joystick);

			/// <summary>
			/// Enable/disable joystick event polling.
			/// </summary>
			/// <param name="state">Can be one of SDL_QUERY, SDL_IGNORE, or SDL_ENABLE.</param>
			/// <returns>1 if enabled, 0 if disabled, or a negative error code on failure.
			/// Call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickEventState", CallingConvention = CallingConvention.Cdecl)]
			extern public static int EventState(State state);

			/// <summary>
			/// Gets the status of a specified joystick.
			/// </summary>
			/// <param name="joystick">The joystick to query.</param>
			/// <returns>True if the joystick has been opened,
			/// false if it has not;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickGetAttached", CallingConvention = CallingConvention.Cdecl)]
			extern public static bool GetAttached(IntPtr joystick);

			/// <summary>
			/// Gets the current state of an axis control on a joystick.
			/// </summary>
			/// <param name="joystick">A joystick identifier containing joystick information.</param>
			/// <param name="axis">The axis to query;
			/// the axis indices start at index 0.</param>
			/// <returns>
			/// <para>A 16-bit signed integer representing the current position of the axis or 0 on failure;
			/// call Error.Get() for more informaiton</para>
			/// <para>The state is a value ranging from -32768 to 32767.</para></returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickGetAxis", CallingConvention = CallingConvention.Cdecl)]
			extern public static Int16 GetAxis(IntPtr joystick, int axis);

			/// <summary>
			/// Gets the ball axis change since the last poll.
			/// </summary>
			/// <param name="joystick">The joystic to query.</param>
			/// <param name="ball">The ball index to query; ball indices start at index 0.</param>
			/// <param name="dx">The difference in the x axis position since the last poll.</param>
			/// <param name="dy">The difference in the y axis position since the last poll.</param>
			/// <returns>0 on success or a negative error code on failure;
			/// call Error.Get() for more informaiton.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickGetBall", CallingConvention = CallingConvention.Cdecl)]
			extern public static int GetBall(IntPtr joystick, int ball, out int dx, out int dy);

			/// <summary>
			/// Gets the current state of a button on a joystick.
			/// </summary>
			/// <param name="joystick">A joystick identifier containing joystick information.</param>
			/// <param name="button">The button index to get the state from; indices start at index 0.</param>
			/// <returns>1 if the specified button is pressed, 0 otherwise.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickGetButton", CallingConvention = CallingConvention.Cdecl)]
			extern public static byte GetButton(IntPtr joystick, int button);

			/// <summary>
			/// Gets the implementation-dependent GUID for the joystick at a given index.
			/// </summary>
			/// <param name="deviceIndex">The index of the joystick to query. (The N'th joystick on the system.)</param>
			/// <returns>The GUID of the selected joystick.
			/// If called on an invalid index, this function returns a zero GUID;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickGetDeviceGUID", CallingConvention = CallingConvention.Cdecl)]
			extern public static Guid GetDeviceGUID(int deviceIndex);

			/// <summary>
			/// Gets the implementation-dependent GUID for the joystick.
			/// </summary>
			/// <param name="joystick">An open joystick.</param>
			/// <returns>The GUID of the given joystick.
			/// If called on an invalid index, this function returns a zero GUID;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickGetGUID", CallingConvention = CallingConvention.Cdecl)]
			extern public static Guid GetGUID(IntPtr joystick);

			/// <summary>
			/// Gets the current state of a POV hat on a joystick.
			/// </summary>
			/// <param name="joystick">A joystick identifier containing joystick informaiton.</param>
			/// <param name="hat">The hat index to get the state from; hat indices start at 0.</param>
			/// <returns>A Hat enumeration value.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickGetHat", CallingConvention = CallingConvention.Cdecl)]
			extern public static Hat GetHat(IntPtr joystick, int hat);

			/// <summary>
			/// Gets the instance ID of an opened joystick.
			/// </summary>
			/// <param name="joystick">A joystick structure containing joystick information.</param>
			/// <returns>The instance ID of the specified joystick on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickInstanceID", CallingConvention = CallingConvention.Cdecl)]
			extern public static int InstanceID(IntPtr joystick);

			/// <summary>
			/// Gets the implementation dependent name of a joystick.
			/// </summary>
			/// <param name="joystick">The joystick obtained from Joystick.Open().</param>
			/// <returns>The name of the selected joystick.
			/// If no name can be found, this function returns null;
			/// call Error.Get() for more information.</returns>
			public static string Name(IntPtr joystick) { return Marshal.PtrToStringAnsi(name(joystick)); }
			[DllImport(dllName, EntryPoint = "SDL_JoystickName", CallingConvention = CallingConvention.Cdecl)]
			extern private static IntPtr name(IntPtr joystick);

			/// <summary>
			/// Gets the implementation dependent name of a joystick.
			/// </summary>
			/// <param name="deviceIndex">The index of the joystick to query. (The N'th joystick on the system.)</param>
			/// <returns>The name of the selected joystick.
			/// If no name can be found, this function returns null;
			/// call Error.Get() for more information.</returns>
			public static string NameForIndex(int deviceIndex) { return Marshal.PtrToStringAnsi(nameForIndex(deviceIndex)); }
			[DllImport(dllName, EntryPoint = "SDL_JoystickNameForIndex", CallingConvention = CallingConvention.Cdecl)]
			extern private static IntPtr nameForIndex(int deviceIndex);

			/// <summary>
			/// Gets the number of general axis controls on a joystick.
			/// </summary>
			/// <param name="joystick">A joystick structure containing joystick information.</param>
			/// <returns>The number of axis controls/number of axes on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickNumAxes", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumAxes(IntPtr joystick);

			/// <summary>
			/// Gets the number of trackballs on a joystick.
			/// </summary>
			/// <param name="joystick">A joystick structure containing joystick information.</param>
			/// <returns>The number of trackballs on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickNumBalls", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumBalls(IntPtr joystick);

			/// <summary>
			/// Gets the number of buttons on a joystick.
			/// </summary>
			/// <param name="joystick">A joystick structure containing joystick information.</param>
			/// <returns>The number ofbuttons on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickNumButtons", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumButtons(IntPtr joystick);

			/// <summary>
			/// Gets the number of POV hats on a joystick.
			/// </summary>
			/// <param name="joystick">A joystick structure containing joystick information.</param>
			/// <returns>The number of POV hats on success or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickNumHats", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumHats(IntPtr joystick);

			/// <summary>
			/// Use this function to open a joystick for use.
			/// </summary>
			/// <param name="deviceIndex">The index of the joystick to query.</param>
			/// <returns>A joystick identifier or null if an error occured; call GetError for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickOpen", CallingConvention = CallingConvention.Cdecl)]
			extern public static IntPtr Open(int deviceIndex);

			/// <summary>
			/// Update the current state of the open joysticks.
			/// This is called automatically by the event loop if any joystick events are enabled.
			/// </summary>
			[DllImport(dllName, EntryPoint = "SDL_JoystickUpdate", CallingConvention = CallingConvention.Cdecl)]
			extern public static void Update();

			/// <summary>
			/// Count the number of joysticks attached to the system.
			/// </summary>
			/// <returns>The number of attached joysticks on success or a negative error code on failure; call SDL_GetError for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_NumJoysticks", CallingConvention = CallingConvention.Cdecl)]
			extern public static int NumJoysticks();

			/// <summary>
			/// Checks to see if a joystick has haptic features.
			/// </summary>
			/// <param name="joystick">Joystick to test for japtic capabilities.</param>
			/// <returns>1 if the joystick is haptic, 0 if it isn't, or a negative error code on failure;
			/// call Error.Get() for more information.</returns>
			[DllImport(dllName, EntryPoint = "SDL_JoystickIsHaptic", CallingConvention = CallingConvention.Cdecl)]
			extern public static int IsHaptic(IntPtr joystick);
			#endregion
		}
	}
}
#endif