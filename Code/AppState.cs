﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;

namespace VisionRiders.OFE {
  /// <summary>
  /// A reusable component for application state management.
	/// Each one can be a screen or other major component of a project,
	/// such as a title page, menu screen, battle, and so on.
  /// </summary>
  public class AppState {
    #region Class Constructor
    public AppState() {      
    }
    #endregion

    /// <summary>
    /// The current state that the application is in. This is the state that will be run each update.
    /// </summary>
		public static AppState Current { get; private set; }

		/// <summary>
		/// The most recent state before the current one.
		/// </summary>
		/// <remarks>
		/// Will be null until the app state is set and then changed at least once.
		/// </remarks>
		public static AppState Previous { get; private set; }

    /// <summary>
    /// Makes this the active app state.
    /// </summary>
    public void Set() {
			Previous = AppState.Current;
			AppState.Current = this;
      if(Previous != null) {
        Previous.Close();
      }			
			Setup();      
    }

    /// <summary>
		/// Gets this app state ready for display.
		/// Called when this is set as the active app state.
    /// </summary>
    protected virtual void Setup() {
    }

    /// <summary>
		/// Instructs the app state to carry out whatever it needs to do for this frame.
    /// </summary>
		/// <param name="seconds">The number of seconds (or fractions of seconds) that have passed since the last frame.</param>
    public virtual void Update(float seconds) {
    }

    /// <summary>
		/// Allows this app state to finish out anything before handing control over to another app state.
    /// </summary>
    protected virtual void Close() {
    }

    /// <summary>
		/// Closes out this app state when it is no longer needed.
    /// </summary>
    public virtual void Dispose() {
    }
  }
}
