#if XNA
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using VisionRiders.OFE.Graphics;

namespace VisionRiders.OFE {
  /// <summary>
  /// This class is OFE's bridge to XNA.
  /// </summary>
  internal class XNACore : Microsoft.Xna.Framework.Game {
    internal long TicksPerFrame = TimeSpan.TicksPerSecond / 60;
    private long sinceLastFrame = 0;

    /// <summary>
    /// Initializes the XNA core.
    /// </summary>
    /// <param name="windowTitle">The title to give the game window.</param>
    /// <param name="updatesPerSecond">The number of times per second to run the program's logic.</param>
    /// <param name="framesPerSecond">The number of frames to display per second.</param>
    public XNACore(string windowTitle, int updatesPerSecond, int framesPerSecond) {
      // Tell the log what we're doing.
      Debug.Log("Initializing the XNA module...");

      Display.xnaManager = new GraphicsDeviceManager(this);
      Content.RootDirectory = @"";
      Display.xnaContent = this.Content;
      this.Window.Title = windowTitle;
      this.TicksPerFrame = TimeSpan.TicksPerSecond / framesPerSecond;

      if(updatesPerSecond == framesPerSecond) {
        this.TargetElapsedTime = TimeSpan.FromTicks(TimeSpan.TicksPerSecond / updatesPerSecond);
        this.IsFixedTimeStep = true;
      } else {
        this.TargetElapsedTime = TimeSpan.FromTicks(TimeSpan.TicksPerSecond / updatesPerSecond);
        this.IsFixedTimeStep = false;
      }

      // Record success.
      Debug.LogSuccess("The XNA module was initialized!");
    }

    /// <summary>
    /// Allows the game to perform any initialization it needs to before starting to run.
    /// This is where it can query for any required services and load any non-graphic
    /// related content.  Calling base.Initialize will enumerate through any components
    /// and initialize them as well.
    /// </summary>
    protected override void Initialize() {
      Core.Setup();

      base.Initialize();
    }

    /// <summary>
    /// LoadContent will be called once per game and is the place to load
    /// all of your content.
    /// </summary>
    protected override void LoadContent() {
      // TODO: use this.Content to load your game content here
    }

    /// <summary>
    /// UnloadContent will be called once per game and is the place to unload
    /// all content.
    /// </summary>
    protected override void UnloadContent() {
      // TODO: Unload any non ContentManager content here
    }

    /// <summary>
    /// Allows the game to run logic such as updating the world,
    /// checking for collisions, gathering input, and playing audio.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    protected override void Update(GameTime gameTime) {
      Core.Update();

      base.Update(gameTime);
    }

    /// <summary>
    /// This is called when the game should draw itself.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    protected override void Draw(GameTime gameTime) {
      sinceLastFrame += gameTime.ElapsedGameTime.Ticks;
      if(this.IsFixedTimeStep || sinceLastFrame > TicksPerFrame) {
        sinceLastFrame = 0;
        Display.Draw();
      }

      base.Draw(gameTime);
    }

    /// <summary>
    /// Called when the program is closed by the GUI.
    /// </summary>
    protected override void OnExiting(object sender, EventArgs args) {
      base.OnExiting(sender, args);

      // Tell OFE to shut off if it hasn't already.
      if(!Core.ShuttingDown) {
        Core.Shutdown();
      }
    }
  }
}
#endif