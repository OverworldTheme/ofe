﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Input;

namespace VisionRiders.OFE {
  /// <summary>
  /// A special OpenTK-specific class that acts as the bridge between OFE and OpenTK.
  /// </summary>
  internal class GLCore : GameWindow
	{
		#region Class Construction and Initialization
		/// <summary>
		/// Creates the initial GLCore window.
		/// </summary>
		/// <param name="width">The initial width of the display window.</param>
		/// <param name="height">The initial height of the display window.</param>
		public GLCore(int width, int height)
		{
			Width = width;
			Height = height;
		}

		/// <summary>
		/// Creates the initial GLCore window,
		/// and specifices version of OpenGL to be used, when possible.
		/// Required for OS X, because otherwise it defaults to OpenGL 2.1.
		/// </summary>
		/// <param name="width">The initial width of the display window.</param>
		/// <param name="height">The initial height of the display window.</param>
		/// <param name="glMajor">The major version number of OpenGL to request from the OS.</param>
		/// <param name="glMinor">The minor version number of OpenGL to request from the OS.</param>
		public GLCore(int width, int height, int glMajor, int glMinor)
			: base(640, 480,
				OpenTK.Graphics.GraphicsMode.Default,
				string.Empty,
				GameWindowFlags.Default,
				DisplayDevice.Default,
				glMajor, glMinor,
				OpenTK.Graphics.GraphicsContextFlags.Default)
		{
			Width = width;
			Height = height;
		}

		protected override void OnLoad(EventArgs e) {      			
			base.OnLoad(e);
			//Core.appSetup();	
    }
		#endregion

		#region Mouse
		protected override void OnMouseEnter(EventArgs e) {
      base.OnMouseEnter(e);

      
			Cursor = MouseCursor.Empty;
			OFE.Input.Mouse.Default.ShouldBeVisible = true;
    }

    protected override void OnMouseLeave(EventArgs e) {
      base.OnMouseLeave(e);

      
			Cursor = MouseCursor.Default;
			OFE.Input.Mouse.Default.ShouldBeVisible = false;
    }

		protected override void OnMouseMove(OpenTK.Input.MouseMoveEventArgs e)
		{
			base.OnMouseMove(e);

			OFE.Input.Mouse.Default.AbsolutePosition = new CoordInt(e.Position.X, e.Position.Y);
		}
		#endregion

		#region Rendering
		protected override void OnRenderFrame(FrameEventArgs e) {
      base.OnRenderFrame(e);
			if(!Core.IsShuttingDown) {
				DrawCycle.Draw();
			}
    }

    protected override void OnUpdateFrame(FrameEventArgs e) {
      base.OnUpdateFrame(e);
			if(!Core.IsShuttingDown) {
				Core.Update((float)e.Time);
			}
    }
		#endregion

		#region Window
		private bool changingWindowStates = false;

		protected override void OnWindowStateChanged(EventArgs e) {
			base.OnWindowStateChanged(e);

			// When switching to fullscreen, the window state will actually change multiple times.
			// We need to prevent recursion, or else we'll get stuck in an infinite loop.
			if(!changingWindowStates) {
				changingWindowStates = true;

				// Following was removed because it causes conflicts in OS X.
				if(this.WindowState == WindowState.Maximized) {
					//this.Size = new System.Drawing.Size(DisplayDevice.Default.Width, DisplayDevice.Default.Height);
					//OnResize(e);
					//Display.SetFullScreen(true);
				} else if(this.WindowState == WindowState.Normal) {
					//Display.SetFullScreen(false);
				}
				changingWindowStates = false;
			}
		}

		protected override void OnResize(EventArgs e) {
      base.OnResize(e);
			
      // Make sure that OFE's Display has been initialized before messing with anyting.
      if(Display.Width >= 0) {
				// If we're windowed, run some checks to make sure the window stays properly formatted.
				if(!Display.IsFullScreen) {
					// Make sure the window doesn't become too small.
					if(this.Height < 100)
						this.Height = 100;
					if(this.Width < 100)
						this.Width = 105;

					// Keep the aspect ratio intact.
					if(Display.AspectRatioIsLocked && WindowState != OpenTK.WindowState.Maximized) {
						if(this.Height / Display.LockedAspectRatio > this.Width) {
							this.Height = Mather.Round(this.Width / Display.LockedAspectRatio);
						} else {
							this.Width = Mather.Round(this.Height * Display.LockedAspectRatio);
						}
					}
				}
				else if (WindowState == WindowState.Maximized)
				{
					
				}

				// Call the resize event.
				EventHandler onResize = Display.OnResize;
				if(onResize != null) { onResize.Invoke(this, e); }

				// Log our current aspect ratio, and redraw the frame.
        float aspect = (float)this.ClientRectangle.Width / (float)this.ClientRectangle.Height;
        Display.SetViewportAspect(aspect);
				this.OnRenderFrame(null);
      }
    }
		#endregion

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e) {
			base.OnClosing(e);

			// If there are any OnClose events, call them.
			// Otherwise, shut down the application automatically.
			EventHandler close = Display.OnClose;
			if(close == null) {
				// Tell everything we're shutting down.
				if(!Core.IsShuttingDown) {
					Core.Shutdown();
				}
			} else {
				Display.OnClose.Invoke(this, e);
			}
		}		
	}
}
#endif