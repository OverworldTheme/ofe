﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE {
  /// <summary>
  /// The "MATH helpER" class with many useful math-related methods and constants. Because
  /// values are returned as floats instead of doubles (as in the standard C# math library),
  /// you don't need to use casts on everything.
  /// </summary>
  public class Mather {
    #region Members Imported From the Standard Math Library, Cast As Floats
    /// <summary>
    /// The smallest value greater than zero that a float can have.
    /// </summary>
    public const float Epsilon = float.Epsilon;

    /// <summary>
		/// The mathematical constant π.
    /// Delicious 3.1415926...
		/// A ratio of a circle's circumference to its diameter.
    /// </summary>
    public const float Pi = (float)Math.PI;

		/// <summary>
		/// Returns the absolute value of the given number. (The absolute value of a number is
		/// its value without a sign; for example the absolute value of both 5 and -5 is 5.)
		/// </summary>
		/// <param name="value"></param>
		/// <remarks>
		/// C#'s Math library already has an Abs method that returns an integer,
		/// but it has been included here for convenience.
		/// </remarks>
		public static int Abs(int value) {
			return Math.Abs(value);
		}

    /// <summary>
    /// Returns the absolute value of the given number. (The absolute value of a number is
    /// its value without a sign; for example the absolute value of both 5 and -5 is 5.)
    /// </summary>
    /// <param name="value"></param>
		/// <remarks>
		/// C#'s Math library already has an Abs method that returns a float,
		/// but it has been included here for convenience.
		/// </remarks>
		public static float Abs(float value) {
      return Math.Abs(value);
    }

    /// <summary>
    /// Returns the lowest integer value greater than or equal to the given number.
    /// </summary>
    /// <param name="value">A number.</param>
    public static int Ceiling(float value) {
      return (int)Math.Ceiling(value);
    }

    /// <summary>
    /// Returns epsilon raised to the given power.
    /// </summary>
    /// <param name="toThePowerOf">The power to raise epsilon to.</param>
    public static float Exp(float toThePowerOf) {
      return (float)Math.Exp(toThePowerOf);
    }

    /// <summary>
    /// "Floors" the given value. This returns the greatest integer less than or equal to
    /// the given value.
    /// </summary>
    /// <param name="value">The number to floor.</param>
    public static int Floor(float value) {
      return (int)Math.Floor(value);
    }

    /// <summary>
    /// Returns the greater of the two given values.
    /// </summary>
    /// <param name="value1">The first number.</param>
    /// <param name="value2">The second number.</param>
    /// <remarks>Like abs, there is already a float version of this in the standard library,
    /// but it has also been included here for convenience.</remarks>
    /// <seealso cref="Mather.Min"/>
    public static float Max(float value1, float value2) {
      return Math.Max(value1, value2);
    }

    /// <summary>
    /// Returns the lesser of the two given values.
    /// </summary>
    /// <param name="value1">The first number.</param>
    /// <param name="value2">The second number.</param>
    /// <remarks>Like abs, there is already a float version of this in the standard library,
    /// but it has also been included here for convenience.</remarks>
    /// <seealso cref="Mather.Max"/>
    public static float Min(float value1, float value2) {
      return Math.Min(value1, value2);
    }

    /// <summary>
    /// Raises a number to the given power.
    /// </summary>
    /// <param name="value">The number to raise.</param>
    /// <param name="toThePowerOf">The power to raise to.</param>
    public static float Pow(float value, float toThePowerOf) {
      return (float)Math.Pow(value, toThePowerOf);
    }

    /// <summary>
    /// Rounds a given value to the nearest integer.
    /// </summary>
    /// <param name="value">The number to round.</param>
    public static int Round(float value) {
      return (int)Math.Round(value);
    }

    /// <summary>
    /// Rounds a given value to the given number of decimal places.
    /// </summary>
    /// <param name="value">The number to round.</param>
    /// <param name="toDecimalPlace">How many decimal places the return value will be
    /// rounded to.</param>
    public static float Round(float value, int toDecimalPlace) {
      return (float)Math.Round(value, toDecimalPlace);
    }

    /// <summary>
    /// Extracts the sign of a given value. (This is the same as the standard C# Math
    /// class's Sign method.)
    /// </summary>
    /// <param name="value">The number to inspect.</param>
    /// <returns>1 if the value is greater than zero, -1 if it is less than zero,
    /// and 0 if the number is zero.</returns>
    public static int Signum(float value) {
      return (int)Math.Sign(value);
    }

    /// <summary>
    /// Returns the square root of a given value.
    /// </summary>
    /// <param name="value">A number.</param>
    /// <returns></returns>
    public static float Sqrt(float value) {
      return (float)Math.Sqrt(value);
    }

    #region Angle Math
    /// <summary>
    /// Returns the arccosine of the given angle.
    /// </summary>
    /// <param name="value">An angle given in revolutions.</param>
    /// <returns>An angle in revolutions.</returns>
    public static float Acos(float value) {
      return RadiansToRevolutions((float)Math.Acos(RevolutionsToRadians(value)));
    }

    /// <summary>
    /// Returns the arcsine of the given angle.
    /// </summary>
    /// <param name="value">An angle given in revolutions.</param>
    /// <returns>An angle in revolutions.</returns>
    public static float Asin(float value) {
      return RadiansToRevolutions((float)Math.Asin(RevolutionsToRadians(value)));
    }

    /// <summary>
    /// Returns the arctangent of the given angle.
    /// </summary>
    /// <param name="value">An angle given in revolutions.</param>
    /// <returns>An angle in revolutions.</returns>
    public static float Atan(float value) {
      return RadiansToRevolutions((float)Math.Atan(RevolutionsToRadians(value)));
    }

    /// <summary>
    /// Returns the angle whose tangent is angle2 divided by angle1.
    /// </summary>
    /// <param name="value1">The first angle, given in revolutions.</param>
    /// <param name="value2">The second angle, given in revolutions.</param>
    /// <returns>An angle in revolutions.</returns>
    public static float Atan2(float value1, float value2) {
      return RadiansToRevolutions((float)Math.Atan2(RevolutionsToRadians(value1), RevolutionsToRadians(value2)));
    }
    
    /// <summary>
    /// Returns the cosine of the given angle.
    /// </summary>
    /// <param name="angle">An angle given in revolutions.</param>
    /// <returns>An angle in revolutions.</returns>
    public static float Cos(float angle) {
      return (float)Math.Cos(RevolutionsToRadians(angle));
    }

    /// <summary>
    /// Returns the sine of the given angle.
    /// </summary>
    /// <param name="angle">An angle given in revolutions.</param>
    /// <returns>An angle in revolutions.</returns>
    public static float Sin(float angle) {
      return (float)Math.Sin(RevolutionsToRadians(angle));
    }

    /// <summary>
    /// Returns the tangent of the given angle.
    /// </summary>
    /// <param name="angle">An angle given in revolutions.</param>
    /// <returns>An angle in revolutions.</returns>
    public static float Tan(float angle) {
      return (float)Math.Tan(RevolutionsToRadians(angle));
    }
    #endregion
    #endregion

    #region Members Unique To Mather
    /// <summary>
    /// The property of a number being positive or negative.
    /// </summary>
    public enum Sign {
      /// <summary>
      /// A number that is greater than zero.
      /// </summary>
      Positive,

      /// <summary>
      /// A number that is smaller than zero.
      /// </summary>
      Negative,

      /// <summary>
      /// Zero is signless; neither positive or negative (or both positive and negative
      /// depending on the context).
      /// </summary>
      Zero
    }

		/// <summary>
		/// The mathematical constant τ.
		/// The ratio of a circle's circumference to its radius.
		/// Equal to 6.2831853...
		/// </summary>
		public const float Tau = 6.283185307179586f;

    /// <summary>
		/// The mathematical constant φ.
    /// The "golden ratio", equal to 1.6180339887...
    /// </summary>
    public const float Phi = 1.6180339887f;

    /// <summary>
    /// Checks two floats for approximate equality. Remember kids, don't depend on == with floats!
    /// </summary>
    /// <param name="f1">First float.</param>
    /// <param name="f2">Second float.</param>
    /// <returns></returns>
    public static bool Approx(float f1, float f2) {
      if(Distance(f1, f2) < 0.0001f) {
        return true;
      } else {
        return false;
      }
    }

    /// <summary>
    /// Clamps the given value to the given minimum or maximum.
		/// If the value is less than the minimum, it will be made equal to the minimum.
		/// If the value is more than the maximum, it will be made equal to the maximum.
    /// </summary>
    /// <param name="value">The value to check.</param>
    /// <param name="min">The minimum value.</param>
    /// <param name="max">The maximum value.</param>
    /// <returns>If the given maximum is lower than the given minimum,
		/// the return value will be equal to the given value.</returns>
    public static float Clamp(float value, float min, float max) {
      if(max < min) {
        return value;
      }

      if(value < min) {
        return min;
      } else if (value > max) {
        return max;
      } else {
        return value;
      }
    }

		/// <summary>
		/// Clamps the given value to the given minimum or maximum.
		/// If the value is less than the minimum, it will be made equal to the minimum.
		/// If the value is more than the maximum, it will be made equal to the maximum.
		/// </summary>
		/// <param name="value">The value to check.</param>
		/// <param name="min">The minimum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>If the given maximum is lower than the given minimum,
		/// the return value will be equal to the given value.</returns>
		public static int Clamp(int value, int min, int max) {
			if(max < min) {
				return value;
			}

			if(value < min) {
				return min;
			} else if(value > max) {
				return max;
			} else {
				return value;
			}
		}

		/// <summary>
		/// Convert a hexadecimal value in a string to a 64-bit integer.
		/// </summary>
		/// <param name="hex">The hexadecimal value, with or without the pound sign (#) or C#-style hexadecimal prefix (0x).</param>
		/// <returns>A 64-bit integer.</returns>
		/// <exception cref="FormatException">Thrown if the given string contains a non-hexadecimal value.</exception>
		public static long ConvertHexStringToInt(string hex) {
			// Trim the excess and convert to lower case.
			hex = hex.Trim().ToLowerInvariant();

			// Sanity check.
			if(hex.Length < 0) {
				return 0;
			}

			// Omit the pound sign or 0x prefix as needed.
			if(hex[0] == '#') {
				hex = hex.Remove(0, 1);
			} else if(hex.Length >= 2 && hex[0] == '0' && hex[1] == '1') {
				hex = hex.Remove(0, 2);
			}

			// Read the value.
			int value = 0;
			try {
				value = Int32.Parse(hex, NumberStyles.AllowHexSpecifier);
			} catch {
				throw new FormatException("The given string did not contain a valid hexidecimal number.");
			}

			// Done!
			return value;
		}

		/// <summary>
		/// Convert a hexadecimal value in a string to a 64-bit integer.
		/// </summary>
		/// <param name="hex">The hexadecimal value, with or without the pound sign (#) or C#-style hexadecimal prefix (0x).</param>
		/// <returns>A 64-bit integer.</returns>
		/// <exception cref="FormatException">Thrown if the given string contains a non-hexadecimal value.</exception>
		public static long ConvertHexStringToLong(string hex) {
			// Trim the excess and convert to lower case.
			hex = hex.Trim().ToLowerInvariant();

			// Sanity check.
			if(hex.Length < 0) {
				return 0;
			}

			// Omit the pound sign or 0x prefix as needed.
			if(hex[0] == '#') {
				hex = hex.Remove(0,1);
			} else if(hex.Length >= 2 && hex[0] == '0' && hex[1] == '1') {
				hex = hex.Remove(0,2);
			}

			// Read the value.
			long value = 0;
			try {
				value = Int64.Parse(hex, NumberStyles.AllowHexSpecifier);
			} catch {
				throw new FormatException("The given string did not contain a valid hexidecimal number.");
			}

			// Done!
			return value;
		}

		/// <summary>
		/// Calculates the absolute value of difference between two numbers.
		/// </summary>
		/// <param name="value1">The first number.</param>
		/// <param name="value2">The second number.</param>
		/// <returns></returns>
		public static int Distance(int value1, int value2) {
			return Abs(value2 - value1);
		}

		/// <summary>
		/// Calculates the absolute value of difference between two numbers.
		/// </summary>
		/// <param name="value1">The first number.</param>
		/// <param name="value2">The second number.</param>
		/// <returns></returns>
		public static float Distance(float value1, float value2) {
      return Abs(value2 - value1);
    }

    /// <summary>
    /// Returns the largest of a set of numbers.
    /// </summary>
    /// <param name="numbers">The numbers to compare.</param>
    /// <returns>0 if no numbers were provided.</returns>
    public static int LargestOf(params int[] numbers) {
      if(numbers.Length < 1)
        return 0;

      int largest = int.MinValue;

      foreach(int number in numbers) {
        if(number > largest) {
          largest = number;
        }
      }

      return largest;
    }

    /// <summary>
    /// Returns the largest of a set of numbers.
    /// </summary>
    /// <param name="numbers">The numbers to compare.</param>
    /// <returns>0 if no numbers were provided.</returns>
    public static float LargestOf(params float[] numbers) {
      if(numbers.Length < 1)
        return 0;

      float largest = float.MinValue;

      foreach(float number in numbers) {
        if(number > largest) {
          largest = number;
        }
      }

      return largest;
    }

    /// <summary>
    /// Linear interpolation method. Returns a number between the given values based on the
    /// given magnitute.
    /// </summary>
    /// <param name="from">The starting value.</param>
    /// <param name="to">The end value.</param>
    /// <param name="time">How far to interpolate, from 0 to 1.</param>
    /// <returns>Returns from value when time is equal to 0, to value when time is equal
    /// to 1, and the average of from and to when time is equal to 0.5.</returns>
    /// <remarks>The time value is clamped between 0 and 1.</remarks>
    public static float Lerp(float from, float to, float time) {
      time = Clamp(time, 0, 1);
      float distance = Mather.Abs(to - from);
      float length = distance * time;
      return Mather.SmallestOf(from, to) + length;
    }

		/// <summary>
		/// Gets the modulo of two numbers.
		/// This is similar to the remainder, except it doesn't "flip" on negative numbers.
		/// </summary>
		/// <remarks>
		/// Included because C#'s % operator returns a remainder and not a more standard modulo.
		/// </remarks>
		/// <param name="dividend">The dividend.</param>
		/// <param name="b">The divisor.</param>
		/// <returns>The returned number will always be positive.</returns>
		public static int Modulo(int dividend, int divisor) {
			return ((dividend % divisor) + divisor) % divisor;
		}

		/// <summary>
		/// Gets the modulo of two numbers.
		/// This is similar to the remainder, except it doesn't "flip" on negative numbers.
		/// </summary>
		/// <remarks>
		/// Included because C#'s % operator returns a remainder and not a more standard modulo.
		/// </remarks>
		/// <param name="dividend">The dividend.</param>
		/// <param name="b">The divisor.</param>
		/// <returns>The returned number will always be positive.</returns>
		public static float Modulo(float dividend, float divisor) {
			return ((dividend % divisor) + divisor) % divisor;
		}

    /// <summary>
    /// Checks the sign of a number.
    /// </summary>
    /// <param name="value">The number to inspect.</param>
    /// <returns>An enumerator giving the number's sign.</returns>
    public static Sign Signage(float value) {
      switch(Signum(value)) {
        case 1:
          return Sign.Positive;
        case 0:
        default:
          return Sign.Zero;
        case -1:
          return Sign.Negative;
      }
    }

    /// <summary>
    /// Returns the smallest of a set of numbers.
    /// </summary>
    /// <param name="numbers">The numbers to compare.</param>
    /// <returns>0 if no numbers were provided.</returns>
    public static int SmallestOf(params int[] numbers) {
      if(numbers.Length < 1)
        return 0;

      int smallest = int.MaxValue;

      foreach(int number in numbers) {
        if(number < smallest) {
          smallest = number;
        }
      }

      return smallest;
    }

    /// <summary>
    /// Returns the smallest of a set of numbers.
    /// </summary>
    /// <param name="numbers">The numbers to compare.</param>
    /// <returns>0 if no numbers were provided.</returns>
    public static float SmallestOf(params float[] numbers) {
      if(numbers.Length < 1)
        return 0;

      float smallest = float.MaxValue;

      foreach(float number in numbers) {
        if(number < smallest) {
          smallest = number;
        }
      }

      return smallest;
    }

    /// <summary>
    /// Swaps two values.
    /// </summary>
    /// <param name="value1">The first value.</param>
    /// <param name="value2">The second value.</param>
    public static void Swap(ref int value1, ref int value2) {
      // No programming tricks here, folks. Using a temp variable is the most
      // reliable way to get the job done. The compiler should be able to tell
      // what we're doing and perform any needed optimization that can be done
      // (if any).
      int temp = value1;
      value1 = value2;
      value2 = temp;
    }

    /// <summary>
    /// Swaps two values.
    /// </summary>
    /// <param name="value1">The first value.</param>
    /// <param name="value2">The second value.</param>
    public static void Swap(ref float value1, ref float value2) {
      float temp = value1;
      value1 = value2;
      value2 = temp;
    }

		#region Angle Conversions
    /// <summary>
    /// Converts an angle given in degrees to radians.
    /// </summary>
    /// <param name="degrees">The angle in degrees to convert.</param>
    public static float DegreesToRadians(float degrees) {
      return (float)(degrees * (Math.PI / 180));
    }

		/// <summary>
		/// Converts an angle given in degrees to revolutions.
		/// </summary>
		/// <param name="degrees">The angle in degrees to convert.</param>
		public static float DegreesToRevolutions(float degrees) {
			return degrees / 360;
		}

		/// <summary>
		/// Converts an angle given in radians to degrees.
		/// </summary>
		/// <param name="radians">The angle in radians to convert.</param>
		public static float RadiansToDegrees(float radians) {
			return (float)(radians * (180 / Math.PI));
		}

		/// <summary>
		/// Converts an angle given in radians to revolutions.
		/// </summary>
		/// <param name="radians">The angle in radians to convert.</param>
		public static float RadiansToRevolutions(float radians) {
			return (float)(radians / (Math.PI * 2));
		}

		/// <summary>
		/// Converts an angle given in revolutions to degrees.
		/// </summary>
		/// <param name="rotations">The angle in revolutions</param>
		/// <returns></returns>
		public static float RevolutionsToDegrees(float revolutions) {
			return revolutions * 360;
		}

		/// <summary>
		/// Converts an angle given in revolutions to radians.
		/// </summary>
		/// <param name="rotations">The angle in revolutions to convert.</param>
		/// <returns></returns>
		public static float RevolutionsToRadians(float revolutions) {
			return (float)(revolutions * (Math.PI * 2));
		}
		#endregion

		/// <summary>
    /// "Loops" the given value, making sure it is never greater than or equal to
    /// the maximum value, and never smaller than the minimum value.
    /// </summary>
    /// <param name="value">The value to wrap.</param>
    /// <param name="min">The inclusive minimum value.</param>
    /// <param name="wrapAt">The exclusive maximum value; the value at which the range ends
    /// and is looped down to the minimum value.</param>
    /// <returns>The wrapped value.</returns>
    public static float Wrap(float value, float min, float wrapAt) {
      // If the two are close together, just return one of them.
      if(Approx(min, wrapAt)) {
        return min;
      }

      // Make sure wrapAt is the bigger of the two.
      if(wrapAt < min) {
        Swap(ref min, ref wrapAt);
      }

      // Get the distance.
      float distance = Mather.Distance(min, wrapAt);

			// Preform the calculation.
			value = min + Modulo(value - min, distance);

      // Done.
      return value;
    }

    /// <summary>
    /// "Loops" the given value, and makes sure it is never greater than or equal to 
    /// the maximum value, and never smaller than the minimum value.
    /// </summary>
    /// <param name="value">The value to wrap.</param>
    /// <param name="min">The inclusive minimum value.</param>
		/// <param name="wrapAt">The exclusive maximum value; the value at which the range ends
		/// and is looped down to the minimum value.</param>
    /// <returns>The wrapped value.</returns>
    public static int Wrap(int value, int min, int wrapAt) {
			// If the two are the same, just return one of them.
			if(min == wrapAt) {
				return min;
			}

			// Make sure wrapAt is the bigger of the two.
			if(wrapAt < min) {
				Swap(ref min, ref wrapAt);
			}

			// Get the distance.
			int distance = Mather.Distance(min, wrapAt);

			// Preform the calculation.
			value = min + Modulo(value - min, distance);

			// Done.
			return value;
    }

    /// <summary>
    /// "Loops" the given value, so that it is always an angle between 0 and 360 degrees.
    /// </summary>
    /// <param name="value">The value to wrap.</param>
    /// <returns>The wrapped value.</returns>
    public static float WrapDegrees(float value) {
      return Wrap(value, 0f, 360f);
    }

		/// <summary>
		/// "Loops" the given value, so that it is always an angle between 0 and Pi * 2 radians.
		/// </summary>
		/// <param name="value">The value to wrap.</param>
		/// <returns>The wrapped value.</returns>
		public static float WrapRadians(float value) {
			return Wrap(value, 0f, (float)(Math.PI * 2));
		}

		/// <summary>
		/// "Loops" the given value, so that it is always an angle between 0 and 1 revolutions.
		/// </summary>
		/// <param name="value">The value to wrap.</param>
		/// <returns>The wrapped value.</returns>
		public static float WrapRevolutions(float value) {
			return Wrap(value, 0f, 1f);
		}
    #endregion
  }
}
