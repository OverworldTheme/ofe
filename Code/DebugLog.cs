﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Data;

namespace VisionRiders.OFE {
  /// <summary>
  /// A running log that the engine keeps to write human-readable debug messages.
	/// The most important of these are written to a file,
	/// so that they can be investigated later.
	/// End-users can also send these logs to the developer to help with troubleshooting.
  /// </summary>
  public static class DebugLog {
		#region Initialization and Release
		/// <summary>
		/// Initializes the debug log,
		/// and creates the file that the log will write to.
		/// </summary>
		public static void Init()
		{
			// In debug mode, the console should be enabled by default.
#if DEBUG
			IsConsoleEnabled = true;
#endif

			// Decide where we're going.
			pathAndFilename = Path.Combine(Core.UserFolder, SaveData.UserFolderName, logFolder, filename);

			// Backup any old logs.
			for(int i=maxBackups - 1; i>= 0;i--)
			{
				// Does it exist?
				if(File.Exists(backupPathAndFilename(i)))
				{
					// Back it up, overwritting the existing file as is neccissary.
					try
					{
						File.Copy(backupPathAndFilename(i), backupPathAndFilename(i + 1), true);
					}
					catch (Exception e)
					{
						throw new IOException("Could not get access to the directory for the debug log.", e);
					}
				}
			}

			// Now, create a new debug log.
			try
			{
				// Make sure the directory exists.
				// If not, create it.
				string path = Path.GetDirectoryName(pathAndFilename);
				if(!Directory.Exists(path))
					Directory.CreateDirectory(path);

				// Create the file, and attach a stream to it.
				file = new StreamWriter(new FileStream(pathAndFilename + extension, FileMode.Create), Encoding.UTF8);
				file.AutoFlush = true;
			}
			catch (Exception e)
			{
				throw new IOException("Could not create a debug log file.", e);
			}
		}

		/// <summary>
		/// Releases the debug log's resources,
		/// and closes out the debug log file.
		/// </summary>
		public static void Release()
		{
			DebugLog.Break();
			DebugLog.Write("End of log.");
			file.Close();
		}
		#endregion

		#region Settings
		/// <summary>
		/// Whether the log is to be written to the console or terminal.
		/// If disabled, the log will only write to a file.
		/// </summary>
		public static bool IsConsoleEnabled { get; set; }
		#endregion

		#region Debug Log File
		/// <summary>
		/// The number of log backups to keep.
		/// </summary>
		private const int maxBackups = 3;
		
		/// <summary>
		/// The file that the debug log will be writing to.
		/// </summary>
		private static StreamWriter file;

		/// <summary>
		/// The name of the debug log file, sans any extension.
		/// </summary>
		private const string filename = "DebugLog";

		/// <summary>
		/// The file extension used by the debug log.
		/// </summary>
		private const string extension = ".txt";

		/// <summary>
		/// The name of the folder to put the debug log in.
		/// </summary>
		private const string logFolder = "Logs";

		/// <summary>
		/// The absolute path and filename of the debug log file,
		/// not including the file extension.
		/// </summary>
		private static string pathAndFilename;

		/// <summary>
		/// Constructs the absolute path and filename of a backup debug log file.
		/// </summary>
		/// <param name="number">Which backup number to construct the filename for.</param>
		/// <returns><para>If number is 0, the actual debug log file will be returned.</para>
		/// <para>If number is 1, it will be the initial debug log backup,
		/// which is simply the normal filename with ".backup" added to the end.</para>
		/// <para>If number is higher than 1,
		/// the method will return the normal filename with ".backup" appended by the number.
		/// (So, for example, if the number is 3, it will be ".backup3".</para></returns>
		/// <exception cref="ArgumentException">Thrown if number is a negative value.</exception>
		private static string backupPathAndFilename(int number)
		{
			if (number > 1)
				return pathAndFilename + ".backup" + number + extension;
			else if (number == 1)
				return pathAndFilename + ".backup" + extension;
			else if (number == 0)
				return pathAndFilename + extension;
			else
				throw new ArgumentException("An invalid backup number was provided for the debug log.", "number");
    }
		#endregion

		#region Writing to the Log
		/// <summary>
		/// A lock to use when accessing the log,
		/// to make sure it's not tripping over itself.
		/// </summary>
		/// <remarks>
		/// TODO: In the future, this should really be done with asynchrony.
		/// However, at the moment the engine is trying to stay at .NET 4.0 which limits the ability to do so.
		/// </remarks>
		private static object mutex = new object();

		/// <summary>
		/// Internal method for doing the actual writing.
		/// </summary>
		/// <param name="message">The message to write.</param>
		/// <param name="indent">Whether to indent the message.</param>
		/// <param name="toFile">Whether to write to the log file in addition to the console/terminal.</param>
		private static void write(string message, bool indent, bool toFile)
		{
			if (IsConsoleEnabled)
			{
				if (indent) Console.Write("   ");
				Console.WriteLine(message);
			}

			if (toFile)
			{
				lock (mutex)
				{
					if (indent) file.Write("   ");
					file.WriteLine(message);
				}
			}
		}

		/// <summary>
		/// Writes a generic message to the debug log.
		/// </summary>
		/// <param name="message">The message to write in the log.</param>
		public static void Write(string message) {
			Console.ForegroundColor = ConsoleColor.White;
			write(message, false, true);
    }

		/// <summary>
		/// Writes a debugging message to the console, but not the log file.
		/// </summary>
		/// <param name="message">The message to write in the log.</param>
		public static void Whisper(string message)
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			write(message, false, false);
		}

    /// <summary>
    /// Writes a blank line to the debug log.
    /// </summary>
    public static void Break() {
      Write(string.Empty);
    }

		/// <summary>
		/// Writes multiple blank lines to the debug log.
		/// </summary>
		/// <param name="breaks">The number of breaks to write to the log.</param>
		public static void Break(int breaks)
		{
			for (int i = 0; i < breaks; i++)
				Break();
		}

    /// <summary>
    /// Writes a sort of side-note to the debug log.
    /// This is used to pass less important information to the log,
		/// such as progress milestones telling what the application is doing.
    /// </summary>
    public static void Note(string message) {
			Console.ForegroundColor = ConsoleColor.White;
			write(message, true, true);
		}

    /// <summary>
    /// Records a success to the debug log.
    /// </summary>
    /// <param name="message">The message to write in the log.</param>
    public static void Success(string message) {
			Console.ForegroundColor = ConsoleColor.Green;
			write(message, true, true);
		}

    /// <summary>
    /// Records a failure to the debug log.
    /// </summary>
    /// <param name="message">The message to write in the log.</param>
    public static void Failure(string message) {
			Console.ForegroundColor = ConsoleColor.Red;
			write(message, true, true);
		}

		/// <summary>
		/// Records a warning to the debug log.
		/// Warnings are notices that,
		/// while nothing is "wrong" per se,
		/// there is an issue that something should be kept in mind.
		/// </summary>
		/// <param name="message">The message to write in the log.</param>
		public static void Warning(string message)
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			write(message, true, true);
		}

		/// <summary>
		/// Records a (usually fatal) major error that has occured.
		/// </summary>
		/// <param name="message">The message to write in the log.</param>
		public static void CriticalError(string message)
		{
			Console.ForegroundColor = ConsoleColor.DarkRed;
			write(message, false, true);
		}
		#endregion
	}
}
