﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;

namespace VisionRiders.OFE.GUI {
  /// <summary>
  /// A single selectable menu item within a menu.
  /// </summary>
  public class MenuItem : Widget {
    #region Class Constructors
    /// <param name="typeface">The typeface for this menu item's text.</param>
    /// <param name="content">The visible text to set for this menu item.</param>
		public MenuItem(string content, Font typeface)
      : this(content, null, content, typeface, 0, 0) { }

    /// <param name="handle">This menu item's handle name.</param>
    /// <param name="typeface">The typeface for this menu item's text.</param>
    /// <param name="content">The visible text to set for this menu item.</param>
    public MenuItem(string handle, string content, Font typeface)
      : this(handle, null, content, typeface, 0, 0) { }

    /// <param name="parent">The parent of this menu item.</param>
    /// <param name="typeface">The typeface for this menu item's text.</param>
    /// <param name="content">The visible text to set for this menu item.</param>
    /// <param name="x">The X axis location to set for this menu item.</param>
    /// <param name="y">The Y axis location to set for this meny item.</param>
		public MenuItem(Widget parent, string content, Font typeface, float x, float y)
      : this(content, parent, content, typeface, x, y) { }

    /// <param name="parent">The parent of this menu item.</param>
    /// <param name="handle">This menu item's handle name.</param>
    /// <param name="typeface">The typeface for this menu item's text.</param>
    /// <param name="content">The visible text to set for this menu item.</param>
    /// <param name="x">The X axis location to set for this menu item.</param>
    /// <param name="y">The Y axis location to set for this meny item.</param>
		public MenuItem(string handle, Widget parent, string content, Font typeface, float x, float y) {
      // Set up the label.
      label = new Label(content, typeface, Color.White, 0, 0);
      label.Alignment = Font.Align.Left;
      label.VerticalAlignment = Font.VerticalAlign.Center;
      label.OnClick += this.onClick;
      label.Disabled = true;
			AddChild(label);

      // Set our parent.
      SetParent(parent);

      // Set everything else.
      Handle = handle;
			position = new Coord(x, y);
    }
    #endregion

    #region Icon
    // TODO
    #endregion

    #region Label and Text
    /// <summary>
    /// Label object used for this widget's text.
    /// </summary>
    private Label label;

    /// <summary>
    /// The alignment of this menu item's text.
    /// </summary>
    public Font.Align Alignment {
      get {
        return label.Alignment;
      }
      set {
        label.Alignment = value;
				onGraphicsChange(this, EventArgs.Empty);
      }
    }

    /// <summary>
    /// The vertical alignment of this menu item's text.
    /// </summary>
    public Font.VerticalAlign VerticalAlignment {
      get {
        return label.VerticalAlignment;
      }
      set {
        label.VerticalAlignment = value;
				onGraphicsChange(this, EventArgs.Empty);
      }
    }

		/// <summary>
		/// The fill color of this menu item's text.
		/// </summary>
		public Color Fill {
			get {
				return label.Fill;
			}
			set {
				label.Fill = value;
			}
		}

		/// <summary>
		/// The outline color of this menu item's text.
		/// </summary>
		public Color Outline {
			get {
				return label.Outline;
			}
			set {
				label.Outline = value;
			}
		}

		/// <summary>
		/// The shadow color of this menu item's text.
		/// </summary>
		public Color Shadow {
			get {
				return label.Shadow;
			}
			set {
				label.Shadow = value;
			}
		}

    /// <summary>
    /// The text content of this menu item.
    /// </summary>
    public string Content {
      get {
        if(label != null) {
          return label.Content;
        } else {
          return string.Empty;
        }
      }
      set {
        if(label != null) {
          label.Content = value;
        } else {
        }
      }
    }
    #endregion

    #region Position and Size
    /// <summary>
    /// The width of this menu item.
    /// </summary>
    public override float Width {
      get {
        return label.Width;
      }
    }

    /// <summary>
    /// The height of this menu item.
    /// </summary>
    public override float Height {
      get {
        return label.Height;
      }
    }
    #endregion

    #region Updates and Management
    public override void Update() {
      base.Update();
    }

    public override void SetGraphics() {
      base.SetGraphics();

      Area = label.Area;
    }
    #endregion
  }
}
