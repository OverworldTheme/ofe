﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;

namespace VisionRiders.OFE.GUI {
  /// <summary>
  /// A vertically-arranged menu that has many uses. Clicking outside the menu will close it.
  /// </summary>
  public class DropDownMenu : Widget{
    #region Class Constructor
    /// <param name="menuItems">The menu items to give this menu, listed in order from
    /// first (top) to last (bottom).</param>
    public DropDownMenu(string graphicFile, params MenuItem[] menuItems)
			: this(graphicFile, Desktop.Stage.Mouse.X, Desktop.Stage.Mouse.Y, menuItems) { }

    /// <param name="x">The X axis coordinate to set this menu.</param>
    /// <param name="y">The Y axis coordinate to set this meny.</param>
    /// <param name="menuItems">The menu items to give this menu, listed in order from
    /// first (top) to last (bottom).</param>
    public DropDownMenu(string graphicFile, float x, float y, params MenuItem[] menuItems)
      : this(null, graphicFile, x, y, menuItems) { }

    /// <param name="parent">The parent for this menu.</param>
    /// <param name="x">The X axis coordinate to set this menu.</param>
    /// <param name="y">The Y axis coordinate to set this meny.</param>
    /// <param name="menuItems">The menu items to give this menu, listed in order from
    /// first (top) to last (bottom).</param>
    public DropDownMenu(Widget parent, string graphicFile, float x, float y, params MenuItem[] menuItems) {
			// Tell the log what we're doing.
			DebugLog.Whisper("Creating a new drop-down menu in the GUI...");

      // Set positioning.
      SetParent(parent);
			position = new Coord(x, y);
			Alignment = Font.Align.Left;

      // Set the skin box.
			box = new SkinBox(Stage.GUI, graphicFile, 0, 0, z, X, Y);
      box.Origin = new Coord(0, 0);

      // Set up all the menu items.
      AddMenuItem(menuItems);

      // Give the first menu item focus or, if there isn't one, give it to the menu itself.
      if(this.menuItems.Count > 0) {
				Desktop.Focus = menuItems[0];
      } else {
				Desktop.Focus = this;
      }

			// Success!
			DebugLog.Whisper("Created a new drop-down menu in the GUI.");
    }
    #endregion

    #region Menu Items
    /// <summary>
    /// Gets a copy of the list of menu items.
    /// </summary>
		/// <remarks>
		/// This list is useful for enumerating and making minor changes to the menu items in a drop-down
		/// menu, but cannot be used for adding or removing them as the list is a copy.
		/// Use AddMenuItem and RemoveMenuItem to add and remove items from a drop-down menu.
		/// </remarks>
		public List<MenuItem> MenuItems {
			get {
				return new List<MenuItem>(menuItems);
			}
		}
		
		/// <summary>
		/// The actual list of all menu items in the menu.
		/// </summary>
    private List<MenuItem> menuItems = new List<MenuItem>();

    /// <summary>
    /// Adds selections to the menu.
    /// </summary>
    /// <param name="items">The menu items to add.</param>
    public void AddMenuItem(params MenuItem[] items) {
			if(items == null)
				return;

      foreach(MenuItem item in items) {
        AddChild(item);
        menuItems.Add(item);
        item.OnClick += this.onClick;
				item.Alignment = Alignment;
        item.VerticalAlignment = Font.VerticalAlign.Cap;
      }

			setArea();
			setBox();
    }

    /// <summary>
    /// Removes selections from the menu.
    /// </summary>
    /// <param name="items">The menu items to remove.</param>
    public void RemoveMenuItem(params MenuItem[] items) {
      foreach(MenuItem item in items) {
        RemoveChild(item);
        menuItems.Remove(item);
      }

			setArea();
			setBox();
    }

    /// <summary>
    /// The width of the menu item in this menu that is the widest.
    /// </summary>
    public float WidestMenuItem {
      get {
        float widest = 0;
        foreach(MenuItem item in menuItems) {
          if(item.Width > widest) {
            widest = item.Width;
          }
        }
        return widest;
      }
    }

    /// <summary>
    /// The height of all the meny items in this menu.
    /// </summary>
    public float heightOfAllMenuItems {
      get {
        float height = 0;
        foreach(MenuItem item in menuItems) {
          height += item.Height;
        }
        return height;
      }
    }

    /// <summary>
    /// Cascades the items in the menu into a proper list.
    /// </summary>
    private void arrangeItems() {
			switch(Alignment) {
				case Font.Align.Left:
				case Font.Align.Justify:
					Desktop.Arrange(Direction.Down, -Origin.X + Padding, -Origin.Y + Padding, 0, this.menuItems.ToArray());
					break;
				case Font.Align.Center:
					Desktop.Arrange(Direction.Down, -Origin.X + Width / 2 + Padding, -Origin.Y + Padding, 0, this.menuItems.ToArray());
					break;
				case Font.Align.Right:
					Desktop.Arrange(Direction.Down, -Origin.X + Width + Padding, -Origin.Y + Padding, 0, this.menuItems.ToArray());
					break;
			}
			setAbsolutePosition();
    }
    #endregion

    #region Graphics
		/// <summary>
		/// The hue given to this drop-down box's border graphic.
		/// </summary>
		public Color BorderHue {
			get {
				if(box != null) {
					return box.BorderHue;
				} else {
					return Color.Black;
				}
			}
			set {
				if(box != null) {
					box.BorderHue = value;
				}
			}
		}

		/// <summary>
		/// The hue given to this drop-down box's background graphic.
		/// </summary>
		public Color BackgroundHue {
			get {
				if(box != null) {
					return box.BackgroundHue;
				} else {
					return Color.Black;
				}
			}
			set {
				if(box != null) {
					box.BackgroundHue = value;
				}
			}
		}

		/// <summary>
		/// The font alignment of the menu items in this menu.
		/// </summary>
		public Font.Align Alignment {
			get {
				return alignment;
			}
			set {
				alignment = value;
				foreach(MenuItem item in menuItems) {
					item.Alignment = alignment;
				}
			}
		}
		private Font.Align alignment;

    /// <summary>
    /// The skinned box that makes up this menu's graphics.
    /// </summary>
    private SkinBox box;

    /// <summary>
    /// Sets the size of the box graphic.
    /// </summary>
    private void setBox() {
      Width = box.Width = WidestMenuItem + Padding * 2;
      Height = box.Height = heightOfAllMenuItems + Padding * 2;
    }

		/// <summary>
		/// Sets the menu's active area.
		/// </summary>
		private void setArea() {
			Area = new Rectangle(box.Width, box.Height);
			Area.Move(-Origin);
		}
    #endregion

    #region Size and Position
    /// <summary>
    /// The amount of padding on each side between the border of this
    /// menu and its contents.
    /// </summary>
    public float Padding = 5;
    #endregion

    #region Updates and Management
    public override void Update() {
      base.Update();

      // If the menu ever loses focus, close it.
      if(!HasFocus) {
				suicide();
      }
    }

		public override void SetGraphics() {
			base.SetGraphics();
			
			// Make sure we don't fall off the edges of the screen.
			if ((Y - Origin.Y) + heightOfAllMenuItems + Padding * 2 > Desktop.Stage.TitleSafe.Bottom)
			{
				Y = Desktop.Stage.TitleSafe.Bottom - (heightOfAllMenuItems - Origin.Y) - Padding * 2;
			}
			if ((Y - Origin.Y) < Desktop.Stage.TitleSafe.Top)
			{
				Y = Desktop.Stage.TitleSafe.Top + Origin.Y;
			}
			if ((X - Origin.X) + Width + Padding * 2 > Desktop.Stage.TitleSafe.Right)
			{
				X = Desktop.Stage.TitleSafe.Right - (Width - Origin.X) - Padding * 2;
			}
			if ((X - Origin.X) < Desktop.Stage.TitleSafe.Left)
			{
				X = Desktop.Stage.TitleSafe.Left + Origin.X;
			}

			// Update graphics.
			if(box != null) {
				box.Origin = this.Origin;
				box.Position = AbsolutePosition;
				box.Z = this.z;
				setBox();
				setArea();
			}

			arrangeItems();
		}

    public override void Dispose() {
      base.Dispose();

      // Kill all menu items.
      foreach(MenuItem item in menuItems) {
        item.Dispose();
      }

      // TODO: Kill the icons.

      // Kill the box. Do it now!
      box.Dispose();
    }
    #endregion

		#region Event Overrides
		protected override void onClick(object sender, EventArgs e) {
			base.onClick(sender, e);

			//suicide(sender, e);
		}
		#endregion
	}
}
