﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;

namespace VisionRiders.OFE.GUI {
  /// <summary>
  /// Descriptive text used within the GUI.
  /// </summary>
  public class Label : Widget {
    #region Class Constructors
    public Label(string content, Font typeface, Color color, float x, float y) {
      this.active = true;

      this.position = new Coord(x, y);

      Handle = content;
			text = new Text(Desktop.Stage, content, typeface, Font.Align.Left, color, 0, x, y);
      setArea();
    }
    #endregion

    #region Text Object
    /// <summary>
    /// Text graphic object used for this label widget's display.
    /// </summary>
    private Text text;

    /// <summary>
    /// This label's text.
    /// </summary>
    public string Content {
      get {
        return text.Content;
      }
      set {
        Set(value);
        onGraphicsChange(this, EventArgs.Empty);
      }
    }

    /// <summary>
    /// Gets what kind of text this is; a text box or a single line of text with manual line breaks.
    /// </summary>
    public Text.Type Kind {
      get {
        return text.Kind;
      }
    }

    /// <summary>
    /// How to align the text horizontally.
    /// </summary>
    public Font.Align Alignment {
      get {
        return text.Alignment;
      }
      set {
        text.Alignment = value;
				setArea();
      }
    }

    /// <summary>
    /// How to align the text vertically.
    /// </summary>
    public Font.VerticalAlign VerticalAlignment {
      get {
        return text.VerticalAlignment;
      }
      set {
        text.VerticalAlignment = value;
				setArea();
      }
    }

    /// <summary>
    /// The color the text will be.
    /// </summary>
    public Color Fill {
      get {
        return text.Fill;
      }
      set {
        text.Fill = value;
      }
    }

    /// <summary>
    /// The color the text's outline.
    /// </summary>
    public Color Outline {
      get {
        return text.Outline;
      }
      set {
        text.Outline = value;
      }
    }

    /// <summary>
    /// The color the text's shadow.
    /// </summary>
    public Color Shadow {
      get {
        return text.Shadow;
      }
      set {
        text.Shadow = value;
      }
    }

    /// <summary>
    /// The font this text will use.
    /// </summary>
    public Font Typeface {
      get {
        return text.Typeface;
      }
      set {
        text.Typeface = value;
				setArea();
      }
    }
    #endregion

    #region Position and Size
    /// <summary>
    /// The width of this label's text.
    /// </summary>
    public override float Width {
      get {
        return text.Width;
      }
    }

    /// <summary>
    /// The height of this label's text.
    /// </summary>
    public override float Height {
      get {
        return text.Height;
      }
    }
    #endregion

    /// <summary>
    /// Deletes this label.
    /// </summary>
    public override void Dispose() {
      base.Dispose();

      text.Dispose();
    }

    /// <summary>
    /// Changes this label's text.
    /// </summary>
    /// <param name="newText">The text for this label to display.</param>
    public void Set(string newText) {
      text.Set(newText);
    }

    /// <summary>
    /// Updates this label each frame.
    /// </summary>
    public override void Update() {
      base.Update();
    }

    /// <summary>
    /// Sets the hit area of this label.
    /// </summary>
    private void setArea() {
      if(text != null && text.Typeface != null) {
        Area = new Rectangle(text.Width, text.Typeface.Baseline - text.Typeface.Cap);
        Area.Move(0, text.Typeface.Cap);
        switch(VerticalAlignment) {
          case Font.VerticalAlign.Center:
						Area.Move(0, -Typeface.Height / 2);
            break;
          case Font.VerticalAlign.Baseline:
            Area.Move(0, -Typeface.Baseline);
            break;
          case Font.VerticalAlign.Cap:
            // Do Nothing
            break;
          case Font.VerticalAlign.Descent:
            Area.Move(0, -Typeface.Descent);
            break;
        }
        switch(Alignment) {
          case Font.Align.Center:
            Area.Move(-text.Width / 2, 0);
            break;
          case Font.Align.Right:
            Area.Move(-text.Width, 0);
            break;
        }
      } else if(text != null) {
        text.IsActive = false;
      }
    }

    /// <summary>
    /// Sets the state and placement of the text graphic used by this label.
    /// </summary>
    public override void SetGraphics() {
      base.SetGraphics();

      // Make sure there's an actual text object, and a valid typeface. Otherwise, there's
      // no reason to activate the text object at all.
      if(text != null && text.Typeface != null) {
        // Set the text graphic object.
        text.IsActive = this.IsActive;
        text.Opacity = this.Opacity;
        text.Position = this.AbsolutePosition;
        text.Z = this.z;
				text.Fill = this.Fill;
				text.Outline = this.Outline;
				text.Shadow = this.Shadow;
      }

      setArea();
    }
  }
}
