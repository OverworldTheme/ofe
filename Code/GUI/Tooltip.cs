﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.GUI {
	/// <summary>
	/// A box used to display information about the widget it is hovering over.
	/// </summary>
	/// <remarks>
	/// <para>The tooltip is displayed on the system stage above the mouse cursor.
	/// It will automatically adjust its position to ensure it stays within the safe frame.</para>
	/// <para>Setting the font and the box graphics will initialize the tooltip.
	/// Both must be set, or else the tooltip will not appear.</para>
	/// </remarks>
	public static class Tooltip {
		#region Content
		
		#endregion

		#region Graphics
		/// <summary>
		/// The text object for the tooltip.
		/// </summary>
		private static Text content { get; set; }

		/// <summary>
		/// The box for the tooltip.
		/// </summary>
		private static SkinBox box { get; set; }

		/// <summary>
		/// The font used for the tooltip.
		/// </summary>
		/// <remarks>
		/// Returns null if no font has been set.
		/// </remarks>
		public static Font Typeface {
			get {
				if(content != null)
					return content.Typeface;
				else
					return null;
			}
			set {
				if(content != null)
					content.Typeface = value;
				else
					content = new Text(Stage.System, string.Empty, value, Font.Align.Left, Color.Black, 0);
			}
		}

		/// <summary>
		/// The color fill used for the tooltip's text.
		/// </summary>
		/// <remarks>
		/// Returns null if the tooltip has not yet been initialized.
		/// </remarks>
		public static Color Fill {
			get {
				if(content != null)
					return content.Fill;
				else
					return Color.None;
			}
			set {
				if(content != null)
					content.Fill = value;
				else
					content = new Text(Stage.System, string.Empty, null, Font.Align.Left, value, 0);
			}
		}

		/// <summary>
		/// Sets the skinned box used for the tooltip.
		/// </summary>
		/// <param name="filename">The path and filename of the skinned box data file.</param>
		public static void SetBoxGraphics(string filename) {
			box = new SkinBox(Stage.System, filename, 0, 0, 0, 0, 0.00001f);
			box.IsVisible = false;
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates the tooltip every frame.
		/// </summary>
		internal static void Update() {
			if(Desktop.Aim != null && !string.IsNullOrEmpty(Desktop.Aim.Tooltip)) {
				if(content != null && box != null) {
					content.Set(Desktop.Aim.Tooltip);
					content.BoxWidth = Stage.System.TitleSafe.Width / 3;
					content.VerticalAlignment = Font.VerticalAlign.Cap;
					content.Origin = Coord.Zero;

					box.Origin = Coord.Zero;
					box.Width = content.Width;
					box.Height = content.Height;

					content.Position = new Coord(Stage.System.Mouse.X, Stage.System.Mouse.Y + Mouse.Default.Cursor.Height - Mouse.Default.Cursor.Origin.Y);

					// Keep it on-screen.
					if(content.X + content.Width > Stage.System.TitleSafe.Right)
						content.X = Stage.System.TitleSafe.Right - content.Width;
					if(content.X < Stage.System.TitleSafe.Left)
						content.X = Stage.System.TitleSafe.Left;
					if(content.Y + content.Height > Stage.System.TitleSafe.Bottom)
						content.Y = Stage.System.TitleSafe.Bottom - content.Height;
					if(content.Y < Stage.System.TitleSafe.Top)
						content.Y = Stage.System.TitleSafe.Top;

					box.Position = content.Position;

					box.IsVisible = true;
					content.IsVisible = true;
				}				
			} else {
				if(box != null)
					box.IsVisible = false;
				if(content != null)
					content.IsVisible = false;
			}
		}
		#endregion
	}
}
