﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.GUI {
  /// <summary>
  /// Widgets are elements that makes up the graphical user interface (GUI). Every GUI element
  /// is derived from this class.
  /// </summary>
#if DEBUG && WINDOWS
  [System.Diagnostics.DebuggerDisplay("{ Handle } ({AbsolutePosition.X}, {AbsolutePosition.Y})")]
#endif
  public class Widget {
		#region Class Constructor
		/// <summary>
		/// Creates a new widget and adds it to the GUI.
		/// </summary>
		protected Widget() {
			// Add ourselves to the GUI.
			Desktop.Widgets.Insert(0, this);

			// Call our creation event.
			new Desktop.Invocation(onCreation, this, EventArgs.Empty);
		}
		#endregion

		#region Disposal
		/// <summary>
		/// Whether or not this widget has been disposed.
		/// </summary>
		private bool isDisposed = false;

		/// <summary>
		/// Removes this widget and all its children. It will no longer be active or displayed.
		/// </summary>
		public virtual void Dispose() {
			// Don't do anything if we've already been disposed.
			if(isDisposed) return;

			// Call the death event.
			onDisposal(this, EventArgs.Empty);

			// Deactivate ourselves.
			IsActive = false;

			// Remove ourself from the list.
			Desktop.Widgets.Remove(this);

			// Detach ourself from our parent.
			ClearParent();

			// Dispose of all our children.
			while(children.Count > 0) {
				children[0].Dispose();
			}

			isDisposed = true;
		}

		/// <summary>
		/// Calls the suicide invocation.
		/// Needed because a widget can't remove itself from the widget list in the middle of a forall.
		/// </summary>
		protected void suicide() {
			new Desktop.Invocation(suicide, this, EventArgs.Empty);
		}

		/// <summary>
		/// An internal event that can be called by a widget to dispose of themself for whatever reason.
		/// Needed because they can't remove themselves from the widget list in the middle of a forall.
		/// </summary>
		protected void suicide(object sender, EventArgs e) {
			this.Dispose();
		}
		#endregion

		#region Widget Identity
		/// <summary>
    /// A name that can be given to this widget for internal use in your program.
    /// </summary>
    public string Handle;

		/// <summary>
		/// An override of ToString that gives some basic information about this widget.
		/// Useful for debugging (and how), but not much else.
		/// </summary>
		/// <returns>A string with the widget's class, handle (if any), and current position.</returns>
		public override string ToString() {
			if(this.Handle != null) {
				return GetType().Name + " \"" + this.Handle + "\" (" + X + ", " + Y + ")";
			} else {
				return GetType().Name + " (" + X + ", " + Y + ")";
			}
		}
		#endregion

		#region Widget Properties
		/// <summary>
    /// Whether or not this widget is actively being updated and is visible. Changes to
    /// this value will be passed along to the widget's children.
    /// </summary>
    public bool IsActive {
      get {
        return active;
      }
      set {
        if(value != active) {
          active = value;
          onActivationChange(this, EventArgs.Empty);

          foreach(Widget child in children) {
            child.IsActive = value;
          }
        }
      }
    }
    protected bool active = true;

    /// <summary>
    /// Whether or not this widget is visible when active, but cannot be changed, selected, clicked, etc.
    /// </summary>
    public bool Disabled = false;

    /// <summary>
    /// Checks if this widget or one of its children has focus.
    /// </summary>
    public bool HasFocus {
      get {
        // Get the widget with the main focus.
        Widget hasFocus = Desktop.Focus;

        // Make sure *a* widget has focus *somewhere*.
        if(hasFocus == null)
          return false;

        // Check the widget with focus to see if this is it, and if not
        // keep checking parents.
        while(hasFocus != null) {          
          if(hasFocus == this) {
            return true;
          }
          hasFocus = hasFocus.parent;
        }

        // This widget does not have focus. Sorry.
        return false;
      }
    }

		/// <summary>
		/// Whether or not this widget's graphics and other resources have finished loading.
		/// </summary>
		/// <remarks>
		/// Unless overridden by the inheriting class, this boolean will always return "true".
		/// </remarks>
		public virtual bool IsLoaded {
			get {
				return true;
			}
		}
    #endregion

    #region Positioning
    /// <summary>
    /// How far down the list this widget is. Widgets with a lower depth value are displayed in
    /// front of those with higher values. 1 is the lowest value.
    /// </summary>
		public int Depth { get; internal set; }

    public Coord Origin {
      get {
        return origin;
      }
      set {
				if(!Mather.Approx(value.X, origin.X) || !Mather.Approx(value.Y, origin.Y)) {
					origin = value;
					onMove(this, EventArgs.Empty);
				}
      }
    }
    protected Coord origin;

    /// <summary>
    /// The position of this widget in relation to its parent.
    /// </summary>
    public Coord Position {
      get {
        return position;
      }
      set {
        if(!Mather.Approx(value.X, position.X) || !Mather.Approx(value.Y, position.Y)) {
          position = value;
					onMove(this, EventArgs.Empty);
        }
      }
    }
    protected Coord position;

    /// <summary>
    /// The position of this widget on the screen, taking into account its parents' positions.
    /// </summary>
    public Coord AbsolutePosition {
      get {
        return absolutePosition;
      }
    }
    private Coord absolutePosition;

    /// <summary>
    /// Sets this widget's absolute position value.
    /// </summary>
    protected void setAbsolutePosition() {
      // Set our absolute position.
      if(Parent != null) {
        absolutePosition = parent.AbsolutePosition + Position;
      } else {
				absolutePosition = Position;
      }

      // Move all this widget's children, too.
      foreach(Widget child in children) {
				child.setAbsolutePosition();
      }
    }

    /// <summary>
    /// This widget's location on the X axis of the screen in relation to its parent.
    /// </summary>
    public float X {
      get {
        return Position.X;
      }
      set {
        if(!Mather.Approx(value, X)) {
          Position = new Coord(value, Y);
          onMove(this, EventArgs.Empty);
        }
      }
    }

    /// <summary>
    /// This widget's location on the Y axis of the screen in relation to its parent.
    /// </summary>
    public float Y {
      get {
        return Position.Y;
      }
      set {
        if(!Mather.Approx(value, Y)) {
          Position = new Coord(X, value);
					onMove(this, EventArgs.Empty);
        }
      }
    }

    /// <summary>
    /// The width of this widget.
    /// </summary>
    public virtual float Width { get; set; }

    /// <summary>
    /// The height of this widget.
    /// </summary>
    public virtual float Height { get; set; }

    /// <summary>
    /// Move this widget to the top of the GUI, then does the same for all its children.
    /// </summary>
    public void MoveToTop() {
      // Take it out, put it in.
      Desktop.Widgets.Remove(this);
      Desktop.Widgets.Insert(0, this);

      // Tell our children to do the same.
      foreach(Widget child in children) {
        child.MoveToTop();
      }

      // Update everyone's graphics.
      Desktop.SetAllGraphics();
    }

    /// <summary>
    /// The recursive part of the MoveToTop algorithm. The base method will call SetGraphics
    /// once the thing is done which is why it can't work on itself.
    /// </summary>
    private void moveToTopIndex() {
      // Take it out, put it in.
      Desktop.Widgets.Remove(this);
      Desktop.Widgets.Insert(0, this);

      // Tell our children to do the same.
      foreach(Widget child in children) {
        child.MoveToTop();
      }
    }

    /// <summary>
    /// This widget's z-depth, as determined by its position in the list.
    /// </summary>
    protected internal float z {
      get {
        return Desktop.ZStartAt + (Desktop.ZSpacing * Desktop.Widgets.IndexOf(this));
      }
    }
    #endregion

    #region Graphics and Graphic Properties
    /// <summary>
    /// This widget's opacity. 1 is completely opaque, and 0 is completely transparent.
    /// </summary>
    public float Opacity {
      get {
        return opacity;
      }
      set {
        opacity = Mather.Clamp(value, 0, 1);
        onGraphicsChange(this, EventArgs.Empty);
      }
    }
    private float opacity = 1;

    /// <summary>
    /// This widget's active hit area, in relation to its origin point.
    /// </summary>
    public Rectangle Area;

		/// <summary>
		/// Gets the top side of the widget's hit area.
		/// </summary>
		public float Top {
			get { return Area.Top; }
		}

		/// <summary>
		/// Gets the bottom side of the widget's hit area.
		/// </summary>
		public float Bottom {
			get { return Area.Bottom; }
		}

		/// <summary>
		/// Gets the left side of the widget's hit area.
		/// </summary>
		public float Left {
			get { return Area.Left; }
		}

		/// <summary>
		/// Gets the right side of the widget's hit area.
		/// </summary>
		public float Right {
			get { return Area.Right; }
		}

    /// <summary>
    /// Method for setting this widget's graphics whenever the widget's graphic properties
    /// have changed or it has been moved.
    /// </summary>
    public virtual void SetGraphics() {
      /*foreach(Widget child in children) {
        //child.setGraphics();
      }*/
    }
    #endregion

    #region Parent/Child Relationships
    /// <summary>
    /// This widget's parent.
    /// </summary>
    public Widget Parent {
      get {
        return parent;
      }
    }
    protected Widget parent;

    /// <summary>
    /// Whether or not this widget has a parent.
    /// </summary>
    public bool Parentless {
      get {
        if(Parent == null) {
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// The root parent of this widget.
    /// </summary>
    public Widget RootParent {
      get {
        // Do a quick search.
        Widget rabbitHole = this;
        while(rabbitHole.parent != null) {
          rabbitHole = rabbitHole.parent;
        }

        if(rabbitHole != this) {
          return rabbitHole;
        } else {
          return null;
        }
      }
    }
    protected Widget rootParent;

    /// <summary>
    /// A list of all this widget's immediate children.
    /// </summary>
    public List<Widget> Children {
      get {
        return children;
      }
    }
    private List<Widget> children = new List<Widget>();

    /// <summary>
    /// Sets the parent of this widget.
    /// </summary>
    /// <param name="parent">This widget's new parent.</param>
    public void SetParent(Widget parent) {
      if(parent != null) {
        ClearParent();        
        parent.AddChild(this);
      }
    }

    /// <summary>
    /// Gives this widget a new child.
    /// </summary>
    /// <param name="child">This widget's new child.</param>
    public void AddChild(Widget child) {
      if(child != null) {
        child.ClearParent();
        child.parent = this;
				setAbsolutePosition();
        this.children.Add(child);
      }
    }

    /// <summary>
    /// Removes this widget from its parent (if any).
    /// </summary>
    public void ClearParent() {
      if(parent != null) {
        parent.children.Remove(this);
        this.parent = null;
				setAbsolutePosition();
      }
    }

    /// <summary>
    /// Removes one of this widget's children.
    /// </summary>
    /// <param name="child">The child widget to remove.</param>
    /// <remarks>The given widget will not be altered unless it is actually this widget's child.</remarks>
    public void RemoveChild(Widget child) {
      if(child != null && child.parent == this) {
        child.parent = null;
				child.setAbsolutePosition();
        children.Remove(child);
      }
    }
    #endregion

    #region Mouse Interaction
    /// <summary>
    /// Whether or not the left mouse button was pressed while the cursor was over this
    /// widget. Used to determine if this widget counts as clicked. In order to be "clicked",
    /// the left mouse button must be pressed and released while over this widget.
    /// </summary>
    private bool clickedAndWaiting;

    /// <summary>
    /// Whether or not the mouse was over this widget last frame. Used for determining when to call
    /// the OnMouseOver event.
    /// </summary>
    private bool mouseWasOverLastFrame = false;

    /// <summary>
    /// Gets whether or not the mouse is hovering over this widget, regardless of whether or not
		/// it is obscured by other widgets.
    /// </summary>
    public virtual bool MouseOver {
      get {
        if(Desktop.Stage != null) {
          if(Desktop.Stage.Mouse.X >= Area.Left + AbsolutePosition.X && Desktop.Stage.Mouse.X <= Area.Right + AbsolutePosition.X
            && Desktop.Stage.Mouse.Y >= Area.Top + AbsolutePosition.Y && Desktop.Stage.Mouse.Y <= Area.Bottom + AbsolutePosition.Y) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    }

		/// <summary>
		/// The text that the tooltip will display when the cursor hovers over this widget.
		/// </summary>
		public string Tooltip { get; set; }
    #endregion

    #region Individual Widget Updating and Management
    /// <summary>
    /// Updates this widget's graphics and other features.
		/// Normally, this will be called by the engine every frame.
    /// </summary>
    public virtual void Update() {
      // Make sure this widget is active.
      if(!this.IsActive) {
        return;
      }

      // Check if this is the top widget.
      if(this == Desktop.Aim) {
				// Check for mouseover events.
				if(!mouseWasOverLastFrame) {
					new Desktop.Invocation(onMouseOver, this, EventArgs.Empty);
				}
				mouseWasOverLastFrame = true;

        // Check if this button has been clicked on.
        if(Mouse.Default.LeftClick()) {
					// Widgets cannot change focus when the GUI is locked.
					if(!Desktop.IsLocked) {
						Desktop.Focus = this;
					}

					// As long as one of this widget's parents or children has
					// focus, it can be clicked.
					if(this.HasFocus || this.RootParent != null && this.RootParent.HasFocus) {
						clickedAndWaiting = true;
					}
        } else if(!Mouse.Default.Held(Mouse.Button.Left)) {
          if(clickedAndWaiting) {
            new Desktop.Invocation(onClick, this, EventArgs.Empty);

            clickedAndWaiting = false;
          }
        }
      } else {
				// Check for mouse out events.
				if(mouseWasOverLastFrame) {
					new Desktop.Invocation(onMouseOut, this, EventArgs.Empty);
				}
				mouseWasOverLastFrame = false;

				// We're no longer clicked and waiting, that's for sure.
        clickedAndWaiting = false;
      }
    }

    /// <summary>
    /// For widgets that don't use sprites for their graphics, draws this widget's graphics to the screen.
    /// </summary>
    protected virtual void draw() {
    }
    #endregion    

    #region Events
    /// <summary>
    /// An event that is called whenever this widget is activated or deactivated.
    /// </summary>
    public event EventHandler OnActivationChange;

    /// <summary>
    /// Internal event called when this widget is activated or deactivated.
    /// </summary>
    protected virtual void onActivationChange(object sender, EventArgs e) {
			setAbsolutePosition();
			//setGraphics();

      if(OnActivationChange != null) {
        OnActivationChange(sender, e);
      }
    }

    /// <summary>
    /// An event that is called whenever this widget has been left clicked on. In order for the click to count, the
    /// mouse has to remain over the widget from the time the left mouse button is pressed to the time it is released.
    /// </summary>
    public event EventHandler OnClick;

    /// <summary>
    /// Calls the OnClick event manually.
    /// </summary>
    public void Click() {
      onClick(this, EventArgs.Empty);
    }

    /// <summary>
    /// Calls the OnClick event manually, and allows you to specify the sender and any event arguments.
    /// </summary>
    public void Click(object sender, EventArgs e) {
      onClick(sender, e);
    }

    /// <summary>
    /// Internal event called when this widget is clicked.
    /// </summary>
    protected virtual void onClick(object sender, EventArgs e) {
			MoveToTop();

      if(OnClick != null) {
        OnClick(sender, e);
      }     
    }

    /// <summary>
    /// An event that is called after this widget has been created.
    /// </summary>
    public event EventHandler OnCreation;

    /// <summary>
    /// An intenrnal event called after this widget has been created.
    /// </summary>
    protected virtual void onCreation(object sender, EventArgs e) {
      setAbsolutePosition();

      if(OnCreation != null) {
        OnCreation(sender, e);
      } else {
        // If no OnCreation has been supplied, just pop this up to the top.
        MoveToTop();
      }
    }

    /// <summary>
    /// An event that is called when this widget is about to be disposed.
    /// </summary>
    public event EventHandler OnDisposal;

    /// <summary>
    /// Internal event called when this widget is about to be disposed.
    /// </summary>
    protected virtual void onDisposal(object sender, EventArgs e) {
      if(OnDisposal != null) {
        OnDisposal(sender, e);
      }
    }

    /// <summary>
    /// An event that is called when this widget, or one of its parents, is given focus.
    /// </summary>
    public event EventHandler OnGetFocus;

    /// <summary>
    /// Internal event called when this widget is given focus.
    /// </summary>
    protected virtual void onGetFocus(object sender, EventArgs e) {
      if(OnGetFocus != null) {
        OnGetFocus(sender, e);
      }

      // Pop this sucker up to the top of the list.
      Desktop.Widgets.Remove(this);
			Desktop.Widgets.Insert(0, this);

      // Pass the focus on to all this widget's children.
      foreach(Widget child in children) {
        child.onGetFocus(sender, e);
      }
    }

    /// <summary>
    /// An event that is called whenever this widget's graphics are altered in some manner other
    /// than simply changing their position. This happens when a widget's opacity is changed, or
    /// a new sprite is set, etc.
    /// </summary>
    public event EventHandler OnGraphicsChange;

    /// <summary>
    /// Internal event called for OnGraphicsChange.
    /// </summary>
    protected virtual void onGraphicsChange(object sender, EventArgs e) {
			setAbsolutePosition();

      if(OnGraphicsChange != null) {
        OnGraphicsChange(sender, e);
      }
    }

		/// <summary>
		/// An event called when this widget's resources have finished loading.
		/// </summary>
		//public event EventHandler OnLoaded;
		
		/// <summary>
		/// Internal event called for OnLoaded
		/// </summary>
		//protected virtual void onLoaded(object sender, EventArgs e) {}

    /// <summary>
    /// Called when the mouse pointer is moved off of this widget.
    /// </summary>
    public event EventHandler OnMouseOut;

    /// <summary>
    /// Internal event called when the mouse pointer moves off this widget.
    /// </summary>
    protected virtual void onMouseOut(object sender, EventArgs e) {
      if(OnMouseOut != null) {
        OnMouseOut(sender, e);
      }
    }

    /// <summary>
    /// An event that is called when the mouse is moved over this widget.
    /// </summary>
    public event EventHandler OnMouseOver;

    /// <summary>
    /// Internal event called when the mouse pointer is moved over this widget.
    /// </summary>
    protected virtual void onMouseOver(object sender, EventArgs e) {
      if(OnMouseOver != null) {
        OnMouseOver(sender, e);
      }
    }

    /// <summary>
    /// An event that is called whenever the widget's location is changed.
    /// </summary>
    public event EventHandler OnMove;

    /// <summary>
    /// Internal event called when this widget is moved.
    /// </summary>
    protected virtual void onMove(object sender, EventArgs e) {
			setAbsolutePosition();

			// Invoke all our children.
			foreach(Widget child in Children) {
				child.onMove(this, EventArgs.Empty);				
			}

      // Call the external event, if there is one.
      if(OnMove != null) {
        OnMove(sender, e);
      }
    }

		/// <summary>
		/// An event that is called when this widget is resized.
		/// </summary>
		public event EventHandler OnResize;

		/// <summary>
		/// Internal event called when this widget is resized.
		/// </summary>
		protected virtual void onResize(object sender, EventArgs e) {
			setAbsolutePosition();

			// Call the external event, if there is one.
			if(OnResize != null) {
				OnResize(sender, e);
			}
		}
    #endregion    
  }
}
