﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.GUI
{
	/// <summary>
	/// The virtual "desktop" where the GUI resides.
	/// </summary>
	public static class Desktop
	{
		#region Constants
		/// <summary>
		/// The amount of z-depth between each GUI element.
		/// </summary>
		public const float ZSpacing = 0.001f;

		/// <summary>
		/// The z-depth of the highest GUI widget.
		/// </summary>
		public const float ZStartAt = 0.001f;
		#endregion

		#region Initialization
		/// <summary>
		/// Initializes the OFE GUI system.
		/// </summary>
		internal static void Init()
		{
			// Tell the log what we're doing.
			DebugLog.Write("Initializing the GUI...");

			// Set the stage.
			stage = Stage.GUI;

			// Report the success.
			DebugLog.Success("The GUI was intialized.");
		}
		#endregion

		#region Stage Management
		/// <summary>
		/// The stage that GUI widgets are displayed on.
		/// </summary>
		public static Stage Stage
		{
			get
			{
				return stage;
			}
		}
		private static Stage stage;
		#endregion

		#region GUI Updating and Management
		/// <summary>
		/// A list of all existing widgets on the desktop.
		/// </summary>
		internal static List<Widget> Widgets = new List<Widget>();

		/// <summary>
		/// Updates all the widgets.
		/// </summary>
		internal static void UpdateAll()
		{
			getAim();

			// Update everyone and set their depth.
			int depth = 1;
			foreach (Widget widget in Widgets)
			{
				if (widget != null)
				{
					widget.Depth = depth;
					widget.Update();
				}
				depth++;
			}

			// Check if the mouse was clicked outside of any GUI object. This causes all GUI objects
			// to lose focus.
			if (Aim == null && Mouse.Default.LeftClick() && !IsLocked)
			{
				Focus = null;
			}

			FireEvents();

			// Update the tooltip.
			GUI.Tooltip.Update();
		}

		/// <summary>
		/// Updates all the widget graphics for the frame.
		/// </summary>
		internal static void UpdateGraphics()
		{
			getAim();

			foreach (Widget widget in Desktop.Widgets)
			{
				widget.SetGraphics();
			}
		}

		/// <summary>
		/// Deactives and clears all widgets.
		/// </summary>
		public static void ClearAll()
		{
			while (Desktop.Widgets.Count > 0)
			{
				Desktop.Widgets[0].Dispose();
			}
		}

		/// <summary>
		/// Updates the graphics of ALL widgets.
		/// </summary>
		public static void SetAllGraphics()
		{
			foreach (Widget widget in Desktop.Widgets)
			{
				// Find all the root widgets. They'll update
				// all the childrens on their own.
				if (widget.Parentless)
				{
					widget.SetGraphics();
				}
			}
		}
		#endregion

		#region GUI Focus
		/// <summary>
		/// The widget that currently has focus. (All of this widget's parents are
		/// considered to have focus as well. Use HasFocus to check if a widget has
		/// focus!)
		/// </summary>
		public static Widget Focus
		{
			get
			{
				if (focus != null && !focus.IsActive || focus != null && focus.Disabled)
				{
					focus = null;
				}
				return focus;
			}
			set
			{
				focus = value;
			}
		}
		private static Widget focus;

		/// <summary>
		/// The highest widget that the mouse is hovering over that is active, and enabled.
		/// This is the widget that the user is "aiming" at with their cursor.
		/// </summary>
		public static Widget Aim { get; private set; }

		/// <summary>
		/// Sets the private widget aim variable.
		/// </summary>
		private static void getAim()
		{
			foreach (Widget widget in Desktop.Widgets)
			{
				if (widget.IsActive && !widget.Disabled)
				{
					if (widget.MouseOver)
					{
						Aim = widget;
						return;
					}
				}
			}

			// Couldn't find an active widget under the mouse.
			Aim = null;
		}

		/// <summary>
		/// Whether or not the GUI has been locked.
		/// When locked, the widget that currently has focus can not lose it.
		/// </summary>
		public static bool IsLocked { get; set; }

		/// <summary>
		/// Locks the GUI.
		/// The widget that currently has focus will not be able to lose it.
		/// </summary>
		public static void Lock()
		{
			IsLocked = true;
		}

		/// <summary>
		/// Unlocks the GUI, allowing widgets to lose and gain focus as normal.
		/// </summary>
		public static void Unlock()
		{
			IsLocked = false;
		}
		#endregion

		#region Event Queue
		/// <summary>
		/// A planned invocation of an event that can be added to the event queue
		/// to be called after an update.
		/// </summary>
		public struct Invocation
		{
			/// <summary>
			/// The widget or other object invoking this event.
			/// </summary>
			public object Sender;

			/// <summary>
			/// The event being invoked.
			/// </summary>
			public EventHandler Event;

			/// <summary>
			/// The arguments to pass to the event. If none, use EventArgs.Empty instead of null.
			/// </summary>
			public EventArgs EventArgs;

			/// <summary>
			/// Adds a new invocation to the queue.
			/// </summary>
			/// <param name="task">The event being invoked.</param>
			/// <param name="sender">The widget or other object invoking this event.</param>
			/// <param name="e">The arguments to pass to the event. If none, use EventArgs.Empty instead of null.</param>
			public Invocation(EventHandler task, object sender, EventArgs e)
			{
				this.Event = task;
				this.Sender = sender;
				this.EventArgs = e;
				eventQueue.Push(this);
			}
		}

		/// <summary>
		/// A list of events that have been queued during an update cycle and are
		/// waiting to be executed. We call the events after the update cycle so that
		/// removing widgets from the list during the foreach won't crash the program.
		/// </summary>
		private static Stack<Invocation> eventQueue = new Stack<Invocation>(128);

		/// <summary>
		/// Fires all events in the queue.
		/// </summary>
		internal static void FireEvents()
		{
			// As long as there are queued events in the stack, continue to pop and execute them.
			while (eventQueue.Count > 0)
			{
				Invocation invocation = eventQueue.Pop();
				if (invocation.Event != null)
				{
					invocation.Event.Invoke(invocation.Sender, invocation.EventArgs);
				}
			}
		}
		#endregion

		#region Arrangement
		/// <summary>
		/// Arranges a group of widgets on the screen based on their individual size and origin points.
		/// </summary>
		/// <param name="direction">Starting from the given point, the direction to start laying the widgets in.</param>
		/// <param name="x">The x coordinate on-screen to start from.</param>
		/// <param name="y">The y coordinate on-screen to start from.</param>
		/// <param name="spacing">The amount of padding to set between each widget.</param>
		/// <param name="toArrange">All the widgets to arrange. The widgets will be placed with the first listed 
		/// placed the furthest to the left/top and then working right/down.</param>
		public static void Arrange(Direction direction, float x, float y, float spacing, params Widget[] toArrange)
		{
			// Make sure that the widget array is ordered so that the one furthest to the left/top
			// will end up being the first given in the original array. To do this, we'll reverse
			// the array's order depending on the direction we'll be arranging the widgets.
			if (direction == Direction.Left || direction == Direction.Up)
			{
				toArrange = toArrange.Reverse().ToArray();
			}

			// Now arrange the widgets.
			foreach (Widget widget in toArrange)
			{
				if (widget == null || !widget.IsActive)
				{
					continue;
				}
				else
				{
					// TEMP? Wait until the widget has finished loading.
					while (!widget.IsLoaded) { Thread.Yield(); }

					// Now figure out where to put it.
					switch (direction)
					{
						case Direction.Down:
							if (widget != null)
							{
								y += widget.Origin.Y;
								widget.X = x;
								widget.Y = y;
								y += (widget.Height - widget.Origin.Y) + spacing;
							}
							break;
						case Direction.Left:
							if (widget != null)
							{
								x -= widget.Width - widget.Origin.X;
								widget.X = x;
								widget.Y = y;
								x -= widget.Width - (widget.Width - widget.Origin.X) + spacing;
							}
							break;
						case Direction.Right:
							if (widget != null)
							{
								x += widget.Origin.X;
								widget.X = x;
								widget.Y = y;
								x += (widget.Width - widget.Origin.X) + spacing;
							}
							break;
						case Direction.Up:
							if (widget != null)
							{
								y -= widget.Height - widget.Origin.Y;
								widget.X = x;
								widget.Y = y;
								y -= widget.Height - (widget.Height - widget.Origin.Y) + spacing;
							}
							break;
					}
				}
			}
		}
		#endregion
	}
}
