﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;

namespace VisionRiders.OFE.GUI {
  /// <summary>
  /// A window widget is just what it says on the tin: a GUI window.
	/// These often form the basis for a user interface.
  /// </summary>
  public class Window : Widget {
		#region Creation
		public Window(string fileName, float width, float height, float x, float y)  {
			// Tell the log what we're doing.
			DebugLog.Whisper("Creating a new window in the GUI...");

			// Position ourselves.
			position = new Coord(x, y);

			// Get our box up.
			box = new SkinBox(Stage.GUI, fileName, width, height, 0, x, y);
			origin = new Coord(width / 2, height / 2);
			setArea();

			// Done.
			DebugLog.Whisper("Window created successfully.");
		}
		#endregion

		#region Update Cycle
		/// <summary>
    /// Updates this window's graphics, checks on user input, and directs its children to do the same.
    /// </summary>
    public override void Update() {
      base.Update();
		}
		#endregion

		#region Graphics
		/// <summary>
		/// The main skin box that will constitute this window's graphics.
		/// </summary>
		private SkinBox box { get; set; }

		/// <summary>
		/// The hue given to this drop-down box's border graphic.
		/// </summary>
		public Color BorderHue {
			get {
				if(box != null) {
					return box.BorderHue;
				} else {
					return Color.Black;
				}
			}
			set {
				if(box != null) {
					box.BorderHue = value;
				}
			}
		}

		/// <summary>
		/// The hue given to this drop-down box's background graphic.
		/// </summary>
		public Color BackgroundHue {
			get {
				if(box != null) {
					return box.BackgroundHue;
				} else {
					return Color.Black;
				}
			}
			set {
				if(box != null) {
					box.BackgroundHue = value;
				}
			}
		}

		/// <summary>
		/// Sets this window's graphics.
		/// </summary>
		public override void SetGraphics() {
			base.SetGraphics();
			if(box != null) {
				setArea();

				box.IsActive = IsActive;
				box.Position = AbsolutePosition;
				box.Origin = Origin;
				box.Z = z;
			}
		}

		/// <summary>
		/// Sets this window's clickable area.
		/// </summary>
		private void setArea() {
			Area = new Rectangle(box.Width, box.Height);
			Area.Move(-Origin);
		}

		/// <summary>
		/// The width of this window.
		/// </summary>
		public override float Width {
			get {
				return Area.Width;
			}
			set {
				if(!Mather.Approx(value, Area.Width)) {
					box.Width = value;
					setArea();
					onResize(this, EventArgs.Empty);
				}
			}
		}

		/// <summary>
		/// The height of this window.
		/// </summary>
		public override float Height {
			get {
				return Area.Height;
			}
			set {
				if(!Mather.Approx(value, Area.Height)) {
					box.Height = value;
					setArea();
					onResize(this, EventArgs.Empty);
				}
			}
		}

		#endregion

		#region Events
		protected override void onDisposal(object sender, EventArgs e) {
			base.onDisposal(sender, e);

			box.Dispose();
		}
		#endregion
	}
}
