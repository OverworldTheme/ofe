﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;

namespace VisionRiders.OFE.GUI {
	public class Image : Widget {
		#region Class Constructor
		public Image(Widget parent, SpriteSource image, float x, float y) {
			// Set our parent.
			SetParent(parent);

			// Set our position.
			position = new Coord(x, y);

			// Make sure the image is loaded before going on.			
			while(image != null && !image.IsLoaded) { Thread.Yield(); }
			sprite = new Sprite(Desktop.Stage, image, 0);
			setArea();

			// Success!
			DebugLog.Success("Created a new image in the GUI.");
		}
		#endregion

		#region Sprite
		/// <summary>
		/// This is the sprite that is being displayed as the image.
		/// </summary>
		private Sprite sprite { get; set; }

		/// <summary>
		/// Changes the sprite source for this image.
		/// </summary>
		/// <param name="source">The new sprite source.</param>
		public void SetSource(SpriteSource source) {
			sprite.SetSource(source);
		}

		/// <summary>
		/// The hue to give this image.
		/// </summary>
		public Color Hue {
			get {
				return sprite.Hue;
			}
			set {
				sprite.Hue = value;
			}
		}

		/// <summary>
		/// Set the hit area for this image.
		/// </summary>
		protected void setArea() {
			if(sprite != null) {
				Area = new Rectangle(sprite.Width, sprite.Height);
				Area.Move(-Origin);
			}
		}

		/// <summary>
		/// Sets the graphics for the image, as needed.
		/// </summary>
		public override void SetGraphics() {
			base.SetGraphics();

			if(sprite != null) {
				sprite.Position = AbsolutePosition;
				sprite.Origin = Origin;
				sprite.Z = z;

				sprite.Opacity = Opacity;
			}
		}

		public override float Width {
			get {
				return Area.Width;
			}
		}

		public override float Height {
			get {
				return Area.Height;
			}
		}
		#endregion

		#region Update Cycle
		public override void Update() {
			base.Update();
		}
		#endregion

		#region Death
		protected override void onDisposal(object sender, EventArgs e) {
			base.onDisposal(sender, e);

			sprite.Dispose();
		}
		#endregion
	}
}
