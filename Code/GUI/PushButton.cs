﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.GUI {
  /// <summary>
  /// An on-screen button that can be clicked and interacted with.
  /// </summary>
  public class PushButton : Widget {
		#region Graphics
		/// <summary>
		/// Whether or not this button's graphics have finished loading.
		/// </summary>
		public override bool IsLoaded {
			get {
				if(Graphic != null && !Graphic.IsLoaded) {
					return false;
				} if(GraphicDown != null && !GraphicDown.IsLoaded) {
					return false;
				} if(GraphicHover != null && !GraphicHover.IsLoaded) {
					return false;
				} else {
					return true;
				}
			}
		}

		/// <summary>
    /// The graphic for this button.
    /// </summary>
    public Sprite Graphic;

    /// <summary>
    /// The graphic for this button when the mouse is hovering over it.
    /// </summary>
    public Sprite GraphicHover;

    /// <summary>
    /// The graphic for this button when the mouse is clicking on it.
    /// </summary>
    public Sprite GraphicDown;

		/// <summary>
		/// The text displayed on this button.
		/// </summary>
		public String Label {
			get {
				return labelObject.Content;
			}
			set {
				labelObject.Set(value);
			}
		}

		/// <summary>
		/// The on-screen label text graphic.
		/// </summary>
		private Label labelObject;

		/// <summary>
		/// The color of this button's label text.
		/// </summary>
		public Color Color {
			get {
				return labelObject.Fill;
			}
			set {
				labelObject.Fill = value;
			}
		}

		/// <summary>
		/// The font that will be used for this button's text.
		/// </summary>
		public Font Typeface;
		#endregion

		#region Class Constructor
		public PushButton(string graphicFile, string label, Font typeface, Color color, float x, float y) {
      // Tell the log what we're doing.
      if(label != string.Empty)
        DebugLog.Whisper("Creating a new push button, " + label + ", in the GUI...");
      else
        DebugLog.Whisper("Creating a new push button (with no label) in the GUI...");
			      
      this.Handle = label;
      this.active = true;

			this.Graphic = new Sprite(Desktop.Stage, graphicFile, 0, x, y);
			Graphic.Source.SpriteSheet.OnLoaded += centerOrigin;
			this.GraphicHover = new Sprite(Desktop.Stage, graphicFile + "_h", 0, x, y);
			this.GraphicDown = new Sprite(Desktop.Stage, graphicFile + "_d", 0, x, y);
      this.position = new Coord(x, y);
			
			// Make sure every image has loaded before continuing.
			/*while(!Graphic.Source.IsLoaded && !Graphic.Source.FileWasBad) { Thread.Sleep(1); }
			while(!GraphicHover.Source.IsLoaded && !GraphicHover.Source.FileWasBad) { Thread.Sleep(1); }
			while(!GraphicDown.Source.IsLoaded && !GraphicDown.Source.FileWasBad) { Thread.Sleep(1); }*/			
			//queuedCenter = true;

      this.labelObject = new Label(label, typeface, color, 0, 0);
      this.labelObject.Alignment = Font.Align.Center;
      labelObject.Disabled = true;
      this.AddChild(labelObject);

      setArea();

      // Report success.
      if(label != string.Empty) 
        DebugLog.Whisper("Created a new push button, " + label + ", in the GUI.");
      else
        DebugLog.Whisper("Created a new push button (with no label) in the GUI.");
    }

    // TODO: Future button expansion, for custom skinned-sprites created using the OFE Integrated Sprite standard.
		/*public PushButton(Sprite graphic, Font typeface, string label, float x, float y, float z) {
			this.Graphic = graphic;
			this.Typeface = typeface;
			this.Label = label;      
			this.X = x;
			this.Y = y;

			Widget.all.Add(this);
		}*/
		#endregion

		public override void Dispose() {
      base.Dispose();

      if(Graphic != null) {
        Graphic.Dispose();
      }
      if(GraphicHover != null) {
        GraphicHover.Dispose();
      }
      if(GraphicDown != null) {
        GraphicDown.Dispose();
      }
    }

    protected override void onActivationChange(object sender, EventArgs e) {
      base.onActivationChange(sender, e);

      setActiveGraphic();
    }

    /// <summary>
    /// Sets this button's area rectangle based on the sprite.
    /// </summary>
    private void setArea() {
      if(Graphic != null) {
        // TODO: This may be altered once sprites can report their own area rectangles.
        Area = new Rectangle(0, 0, Graphic.Source.Width, Graphic.Source.Height);
        Area.Move(-Origin);
        this.Height = Area.Bottom - Area.Top;
        this.Width = Area.Right - Area.Left;
      }
    }

    /// <summary>
    /// Updates this button's properties, checks if it has been clicked, and so on.
    /// </summary>
    public override void Update() {
      base.Update();

			if(queuedCenter && Graphic.IsLoaded) {
				centerOrigin(this, EventArgs.Empty);
			}

      // Activate and set the correct graphic.
      setActiveGraphic();

			// Make sure our area is correct.
			setArea();
    }

    public override void SetGraphics() {
      base.SetGraphics();

      // Update the button's graphic and area property.
      if(Graphic != null) {
        Graphic.Origin = this.Origin;
        Graphic.Position = AbsolutePosition;
        Graphic.Z = this.z;
        Graphic.Opacity = this.Opacity;
      }

      setArea();

      if(labelObject != null) {
        //labelObject.Origin = new Coord(0, 0);
        labelObject.Position = new Coord(this.Width / 2 - Origin.X, this.Height / 2 - Origin.Y);
        labelObject.VerticalAlignment = Font.VerticalAlign.Center;
        labelObject.Opacity = this.Opacity;
      }
      if(GraphicHover != null) {
				GraphicHover.Origin = this.Origin;
        GraphicHover.Position = AbsolutePosition;
        GraphicHover.Z = this.z;
        Graphic.Opacity = this.Opacity;
      }
      if(GraphicDown != null) {
        GraphicDown.Origin = this.Origin;
        GraphicDown.Position = AbsolutePosition;
        GraphicDown.Z = this.z;
        GraphicDown.Opacity = this.Opacity;
      }

      setActiveGraphic();
    }

    /// <summary>
    /// Sets the button's graphic depending on its state.
    /// </summary>
    private void setActiveGraphic() {
      if(IsActive) {
				if (this == Desktop.Aim && !Disabled)
				{
          if(Mouse.Default.Held(Mouse.Button.Left) && GraphicDown != null && !GraphicDown.Source.FileWasBad) {
            GraphicDown.IsActive = true;						
            if(Graphic != null) {
              Graphic.IsActive = false;
            }
            if(GraphicHover != null) {
              GraphicHover.IsActive = false;
            }
          } else if(GraphicHover != null && !GraphicHover.Source.FileWasBad) {
            GraphicHover.IsActive = true;
            if(Graphic != null) {
              Graphic.IsActive = false;
            }
            if(GraphicDown != null) {
              GraphicDown.IsActive = false;
            }
          } else {
            Graphic.IsActive = true;
            if(GraphicHover != null) {
              GraphicHover.IsActive = false;
            }
            if(GraphicDown != null) {
              GraphicDown.IsActive = false;
            }
          }
        } else {
          if(Graphic != null) {
            Graphic.IsActive = true;
          }
          if(GraphicHover != null) {
            GraphicHover.IsActive = false;
          }
          if(GraphicDown != null) {
            GraphicDown.IsActive = false;
          }
        }
      } else {
        if(Graphic != null) {
          Graphic.IsActive = false;
        }
        if(GraphicHover != null) {
          GraphicHover.IsActive = false;
        }
        if(GraphicDown != null) {
          GraphicDown.IsActive = false;
        }
      }
		}

		#region Centering
		/// <summary>
		/// If the graphics need to be centered after loading.
		/// </summary>
		private bool queuedCenter;

		private void centerOrigin(object sender, EventArgs e) {
			if(Graphic.IsLoaded) {
				this.Origin = new Coord(Graphic.Source.Width / 2, Graphic.Source.Height / 2);
				queuedCenter = false;
				setArea();
			} else {
				queuedCenter = true;
			}
		}
		#endregion
	}
}
