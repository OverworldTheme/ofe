﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Input;

namespace VisionRiders.OFE.GUI {
	/// <summary>
	/// A box of text that can be used simply to display information,
	/// or present a string that can be edited.
	/// </summary>
	public class TextField : Widget {
		#region Class Constructor
		public TextField(string content, float width, float height, Font typeface, Color fill, float x, float y) {
			text = new Text(Desktop.Stage, content, typeface, Font.Align.Left, Font.VerticalAlign.Cap, Text.NoScroll, fill, 0, 0, 0, width);
			text.EnableTags = false;
			Caret = '|';
			CaretBlinkDuration = 0.5f;
			caretGraphic = new Text(Desktop.Stage, "|", typeface, Font.Align.Left, fill, 0);			
			Origin = new Coord(width / 2, height / 2);
			editing = false;

			this.height = height;
			X = x;
			Y = y;
		}
		#endregion

		#region Text
		/// <summary>
		/// The actual text we will be using.
		/// </summary>
		private Text text;

		/// <summary>
		/// The text content of this field.
		/// </summary>
		public string Content {
			get {
				return text.Content;
			}
			set {
				text.Set(value);
				new Desktop.Invocation(onGraphicsChange, this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// Gets the height of the text in this field at the moment.
		/// This is the height of just the text that is in the field,
		/// not the height of the widget itself.
		/// </summary>
		public float ContentHeight {
			get {
				return text.Height;
			}
		}

		/// <summary>
		/// The color to make the text in this box.
		/// </summary>
		public Color Fill {
			get {
				return text.Fill;
			}
			set {
				text.Fill = value;
				new Desktop.Invocation(onGraphicsChange, this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// The outline color to give the text in this field.
		/// Set to Color.None if you don't want an outline.
		/// </summary>
		public Color Outline {
			get {
				return text.Outline;
			}
			set {
				text.Outline = value;
				new Desktop.Invocation(onGraphicsChange, this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// The color to make the shadow behind text in this field.
		/// Set to Color.None if you don't want a shadow behind the text.
		/// </summary>
		public Color Shadow {
			get {
				return text.Shadow;
			}
			set {
				text.Shadow = value;
				new Desktop.Invocation(onGraphicsChange, this, EventArgs.Empty);
			}
		}

		public Font.Align Alignment {
			get {
				return text.Alignment;
			}
			set {
				text.Alignment = value;
				new Desktop.Invocation(onGraphicsChange, this, EventArgs.Empty);
			}
		}
		#endregion

		#region Editing
		/// <summary>
		/// Whether or not the user can edit the text within this field.
		/// </summary>
		public bool Editable = false;

		/// <summary>
		/// Gets whether or not the user is currently editing the field.
		/// </summary>
		public bool Editing {
			get {
				return editing;
			}
		}
		private bool editing;

		/// <summary>
		/// The maximum number of characters that can be entered in this field.
		/// If set to zero or less, unlimited characters will be allowed.
		/// </summary>
		public int MaxCharacters;

		/// <summary>
		/// The character used to display where the insertion point is.
		/// By default, a | vertical bar (also known as a pipe).
		/// </summary>
		public char Caret {
			get {
				return caret;
			}
			set {
				caret = value;
				if(caretGraphic != null) {
				}
			}
		}
		private char caret;

		/// <summary>
		/// The index of the insertion point.
		/// </summary>
		private int InsertionPoint;
		#endregion

		#region Update Cycle
		public override void Update() {
			base.Update();
			
			// If we lost focus, we can't be edited.
			if(!this.HasFocus) {
				editing = false;
			}

			// Get input.
			input();

			// Set graphics.
			SetGraphics();
		}
		#endregion

		#region Graphics
		/// <summary>
		/// The character showing where new text will be inserted.
		/// </summary>
		private Text caretGraphic;

		/// <summary>
		/// Internal variable used to time the blinking of the caret.
		/// </summary>
		private int blinkTimer = 0;

		/// <summary>
		/// How long, in seconds, for each caret blink to take.
		/// Set to zero if you don't want the caret to blink at all.
		/// </summary>
		public float CaretBlinkDuration { get; set; }

		public override float Width {
			get {
				return text.BoxWidth;
			}
			set {
				text.BoxWidth = value;
				new Desktop.Invocation(onGraphicsChange, this, EventArgs.Empty);
			}
		}

		public override float Height {
			get {
				return height;
			}
			set {
				height = value;
				new Desktop.Invocation(onGraphicsChange, this, EventArgs.Empty);
			}
		}
		private float height;

		/// <summary>
		/// The skinned box that acts as the border and background of this text field.
		/// </summary>
		private SkinBox box;

		/// <summary>
		/// Changes the skinned box graphic used for this text field's border and background.
		/// </summary>
		/// <param name="filename">The date file where the settings for the skinned box are located.</param>
		public void SetBox(string filename) {
			// Out with the old.
			if(box != null) {
				box.Dispose();
			}

			// In with the new.
			box = new SkinBox(Desktop.Stage, filename, 0, 0, 0, 0, 0);

			// Invoke relevant event(s).
			onGraphicsChange(this, EventArgs.Empty);
		}

		public override void SetGraphics() {
			base.SetGraphics();

			Area = new Rectangle(Width, Height);
			Area.Move(-Origin);

			text.Position = this.AbsolutePosition;
			text.Z = z;
			text.Origin = this.origin;
			caretGraphic.Z = z;

			if(box != null) {
				box.X = AbsolutePosition.X;
				box.Y = AbsolutePosition.Y;
				box.Z = z - float.Epsilon;
				box.Origin = Origin;
				box.Width = Width;
				box.Height = Height;
			}

			// Deal with the caret.
			if(CaretBlinkDuration > 0) {
				blinkTimer++;
				if(blinkTimer > CaretBlinkDuration * Core.SetLPS) {
					blinkTimer = 0;
				}
			} else {
				blinkTimer = 0;
				CaretBlinkDuration = 0;
			}

			if(editing && blinkTimer <= CaretBlinkDuration * Core.SetLPS / 2
				|| editing && blinkTimer == 0) {
				caretGraphic.IsActive = true;
				caretGraphic.X = text.X + text.GetCharacterPosition(InsertionPoint).X;
				if(Alignment == Font.Align.Center) {
					caretGraphic.X -= Width / 2;
				} else if(Alignment == Font.Align.Right) {
					caretGraphic.X -= Width;
				}

				int textLine = text.Lines - 1;
				if(textLine < 0) textLine = 0;
				caretGraphic.Y = text.Y + text.GetCharacterPosition(InsertionPoint).Y;
				caretGraphic.VerticalAlignment = Font.VerticalAlign.Cap;
			} else {				
				caretGraphic.IsActive = false;
			}
		}
		#endregion

		#region Events
		protected override void onClick(object sender, EventArgs e) {
			base.onClick(sender, e);

			// Only editable text fields can be edited, of course.
			if(Editable) {
				// When clicked, we can be edited! :D
				foreach(Widget widget in Desktop.Widgets) {
					// Only one text field can be edited at a time.
					// Set all the others so that they aren't being edited.
					if(widget is TextField) {
						(widget as TextField).editing = false;
					}
				}
				editing = true;
				InsertionPoint = Content.Length;
			}
		}
		#endregion

		#region Input
		/// <summary>
		/// Gets input from the keyboard.
		/// </summary>
		private void input() {
			if(editing) {
				// Allow the user to backspace characters.
				if(Keyboard.Pressed(Key.Backspace) && InsertionPoint > 0) {
					Content = Content.Remove(InsertionPoint - 1, 1);
					blinkTimer = 0;
					InsertionPoint--;
				}

				// Allow the user to delete characters in front of the insertion point.
				if(Keyboard.Pressed(Key.Delete) && InsertionPoint < Content.Length) {
					Content = Content.Remove(InsertionPoint, 1);
					blinkTimer = 0;
				}

				// Allow the user to move the insertion point.
				if(Keyboard.Pressed(Key.Left) && InsertionPoint > 0) {
					InsertionPoint--;
				}
				if(Keyboard.Pressed(Key.Right) && InsertionPoint < Content.Length) {
					InsertionPoint++;
				}

				// Allow the user to snap to the home and end positions.
				if(Keyboard.Pressed(Key.Home) && Content.Length > 0) {
					if(Keyboard.Held(Key.LeftShift) || Keyboard.Held(Key.RightShift)) {
						InsertionPoint = 0;
					} else {
						InsertionPoint = text.FirstIndexOfLine(text.GetLine(InsertionPoint));
					}
				}
				if(Keyboard.Pressed(Key.End) && Content.Length > 0) {
					if(Keyboard.Held(Key.LeftShift) || Keyboard.Held(Key.RightShift)) {
						InsertionPoint = Content.Length;
					} else {
						InsertionPoint = text.LastIndexOfLine(text.GetLine(InsertionPoint)) + 1;
					}
				}

				// Allow the user to back out of editing.
				if(Keyboard.Pressed(Key.Escape)) {
					editing = false;
				}

				// Allow the user to toggle between text fields.
				if(Keyboard.Pressed(Key.Tab)) {
					gotoNextField();
				}
			}
		}

#if OPENGL
		/// <summary>
		/// Reads input from the keyboard and adds it to the text field.
		/// (OpenTK version.)
		/// </summary>
		internal void KeyboardInput(object sender, OpenTK.KeyPressEventArgs e) {
			if(editing) {
				addChar(e.KeyChar);
			}
		}
#elif XNA
		/// <summary>
		/// Reads input from the keyboard and adds it to the text field.
		/// (XNA version.)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/*internal void KeyboardInput(object sender, KeyPressEventArgs e) {
			if(editing) {
				// TO-Do
			}
		}*/
#endif

		/// <summary>
		/// Enters the given string of text to the insertion point, as if the user had typed it.
		/// </summary>
		/// <param name="text">The text to add.</param>
		public void EnterText(string text) {

		}

		/// <summary>
		/// Adds a single character to the text field, at the insertion point.
		/// </summary>
		/// <param name="add">The character to add.</param>
		private void addChar(char add) {
			// Add the character, if it's valid.
			if(Content.Length < MaxCharacters || MaxCharacters <= 0) {
				if(!char.IsControl(add)) {
					// Add the character.
					Content = Content.Insert(InsertionPoint, add.ToString());

					// Make sure the caret is visible.
					blinkTimer = -Mather.Round(Core.SetLPS * CaretBlinkDuration);

					// Shift the insertion point.
					InsertionPoint++;
				}
			}
		}

		/// <summary>
		/// Activates the next text field; if there is one.
		/// </summary>
		private void gotoNextField() {
		}
		#endregion

		#region Death
		protected override void onDisposal(object sender, EventArgs e) {
			base.onDisposal(sender, e);

			text.Dispose();
			caretGraphic.Dispose();
			if(box != null) {
				box.Dispose();
			}
		}
		#endregion
	}
}
