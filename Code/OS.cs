﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE
{
	/// <summary>
	/// An enumeration of operating systems.
	/// </summary>
	public enum OS
	{
		/// <summary>
		/// Microsoft Windows.
		/// </summary>
		Windows,

		/// <summary>
		/// Linux.
		/// </summary>
		Linux,

		/// <summary>
		/// Apple OS X.
		/// </summary>
		OSX,

		/// <summary>
		/// Android.
		/// </summary>
		Android,

		/// <summary>
		/// Apple iOS.
		/// </summary>
		iOS,

		/// <summary>
		/// An unrecognized Unix system.
		/// </summary>
		Unix,

		/// <summary>
		/// The operating system is completely unrecognized,
		/// or could not be determined.
		/// </summary>
		Unknown
	}
}
