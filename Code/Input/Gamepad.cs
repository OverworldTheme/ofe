﻿using System;
using System.Collections.Generic;
using System.Linq;

#if SDL
using SDL2;
#endif

namespace VisionRiders.OFE.Input {
	#region Button Enumeration
  /// <summary>
  /// An enumeration of all the buttons on a modern XBox 360 style controller.
  /// </summary>
  public enum Button {
    /// <summary>
    /// The A button.
    /// </summary>
    A = 1,
    
    /// <summary>
    /// The B button.
    /// </summary>
    B = 2,
    
    /// <summary>
    /// The X button.
    /// </summary>
    X = 3,
    
    /// <summary>
    /// The Y button.
    /// </summary>
    Y = 4,

		/// <summary>
		/// The cross (X) button on a PS3 controller.
		/// </summary>
		Cross = 1,

		/// <summary>
		/// The circle button on a PS3 controller.
		/// </summary>
		Circle = 2,

		/// <summary>
		/// The square button on a PS3 controller.
		/// </summary>
		Square = 3,

		/// <summary>
		/// The triangle button on a PS3 controller.
		/// </summary>
		Triangle = 4,
    
    /// <summary>
    /// The left bumper.
    /// </summary>
    BumperLeft = 5,
    
    /// <summary>
    /// The right bumper.
    /// </summary>
    BumperRight = 6,

		/// <summary>
		/// The back button.
		/// </summary>
		Back = 7,

		/// <summary>
		/// The select button.
		/// </summary>
		Select = 7,

		/// <summary>
		/// The start button.
		/// </summary>
		Start = 8,

		/// <summary>
		/// The share button on a PlayStation 4 controller.
		/// </summary>
		Share = 7,

		/// <summary>
		/// The options button on a PlayStation 4 controller.
		/// </summary>
		Options = 8,

		/// <summary>
		/// The view button on an XBox One controller.
		/// </summary>
		View = 7,

		/// <summary>
		/// The menu button on an XBox One controller.
		/// </summary>
		Menu = 8,
    
    /// <summary>
    /// The left trigger.
    /// </summary>
    TriggerLeft = 9,
    
    /// <summary>
    /// The right trigger.
    /// </summary>
    TriggerRight = 10,

    /// <summary>
    /// Clicking the left stick in.
    /// </summary>
    StickLeft = 11,
    
    /// <summary>
    /// Clicking the right stick in.
    /// </summary>
    StickRight = 12,
    
    /// <summary>
    /// The up button on the D-pad.
    /// </summary>
    Up = 13,
    
    /// <summary>
    /// The down button on the D-pad.
    /// </summary>
    Down = 14,
    
    /// <summary>
    /// The left button on the D-pad.
    /// </summary>
    Left = 15,
    
    /// <summary>
    /// The right button on the D-pad.
    /// </summary>
    Right = 16,
    
    /// <summary>
    /// Tilting a stick up.
    /// </summary>
    StickPushUp = 17,
    
    /// <summary>
    /// Tilting a stick down.
    /// </summary>
    StickPushDown = 18,

    /// <summary>
    /// Tilting a stick to the left.
    /// </summary>
    StickPushLeft = 19,
    
    /// <summary>
    /// Tilting a stick to the right.
    /// </summary>
    StickPushRight = 20,
  }
	#endregion

  /// <summary>
  /// A gamepad controller that you can get player input from.
  /// </summary>
  public class Gamepad {
		#region Initialization
    /// <summary>
    /// Readies all the gamepads for use.
    /// </summary>
    internal static void Init() {
      // Tell the log what we're doing.
      DebugLog.Write("Initializing the gamepads...");

			// Figure out the total number of gamepads.
#if SDL
			Count = SDL.Joystick.NumJoysticks();
			if(Count == 0)
				DebugLog.Note("No gamepads are currently attached to this system..");
			else if (Count == 1)
				DebugLog.Note("There is currently one gamepad attached to this system.");
			else
				DebugLog.Note("There are currently " + Count + " gamepads attached to this system.");
#endif

			// Get all the gamepads together.
			// It'll be a blast!
			Any = new Gamepad();
      None = new Gamepad();

      One = new Gamepad(1);
      Two = new Gamepad(2);
      Three = new Gamepad(3);
      Four = new Gamepad(4);
      All = new List<Gamepad>() { One, Two, Three, Four };

      // Record success.
      DebugLog.Success("Finished initializing the gamepads.");
    }
		#endregion

		#region Class Constructor
    /// <summary>
    /// Creates a new gamepad instance.
    /// </summary>
    public Gamepad() : this(-1) { }

    /// <summary>
    /// Creates a new gamepad instance and assigns it the given index.
    /// </summary>
    /// <param name="index">The index value for the new gamepad.</param>
    public Gamepad(int index) {
			init(index);
    }

		/// <summary>
		/// Initializes (or reinitializes) this gamepad.
		/// </summary>
		/// <param name="index">The index value to assign to the gamepad.</param>
		private void init(int index)
		{
			// Tell the log what we're doing.
			if (index > 0)
				DebugLog.Note("Setting up gamepad #" + index + "...");
			else
				DebugLog.Note("Creating a new auxiliary gamepad object...");

			// Remember the index.
			Index = index;

			if (Index > 0)
			{
#if SDL
				// Get a link to the joystick.
				SDL_joystickID = SDL.Joystick.Open(index - 1);
#endif

				// Make sure we exist.
#if SDL
				if (!SDL.Joystick.GetAttached(SDL_joystickID))
#endif
				{
					DebugLog.Note("   Gamepad #" + index + " is not connected to the system.");
					Exists = false;
				}
				else
				{
					// Introduce ourselves.
#if SDL					
					string sdlName = SDL.Joystick.Name(SDL_joystickID);
					if (sdlName != null)
						DebugLog.Note("   Gamepad's name is \"" + sdlName + "\".");
					else
					{
						DebugLog.Failure("   Could not get the name of gamepad #" + index + "!");
						DebugLog.Failure("   Reported reason: " + SDL.Error.Get());
					}

					Guid sdlGuid = SDL.Joystick.GetGUID(SDL_joystickID);
					if (sdlGuid != Guid.Empty)
						DebugLog.Note("   Gamepad's GUID is " + sdlGuid.ToString() + ".");
					else
					{
						DebugLog.Failure("   Could not get the GUID of gamepad #" + index + "!");
						DebugLog.Failure("   Reported reason: " + SDL.Error.Get());
					}
#endif

					// Okay, we exist.
					Exists = true;
				}

				// Figure out the properties of this gamepad.
#if SDL
				NumberOfButtons = SDL.Joystick.NumButtons(SDL_joystickID);
				NumberOfSticks = SDL.Joystick.NumAxes(SDL_joystickID) / 2;
				NumberOfAxes = SDL.Joystick.NumAxes(SDL_joystickID);
				if (SDL.Joystick.NumHats(SDL_joystickID) > 0)
					HasHatSwitch = true;
				else
					HasHatSwitch = false;

				if (SDL.Joystick.IsHaptic(SDL_joystickID) == 1)
				{					
					IsRumbleAvailable = true;
					SDL_hapticID = SDL.Haptic.OpenFromJoystick(SDL_joystickID);
					SDL.Haptic.RumbleInit(SDL_hapticID);
					DebugLog.Note("   The gamepad has rumble support.");
				}
				else
				{					
					IsRumbleAvailable = false;
					DebugLog.Note("   The gamepad does not have rumble support.");
				}
#endif

				// Create our arrays.
				isDown = new bool[MaxButtons];
				wasDown = new bool[MaxButtons];
				axisMagnitude = new float[MaxAxes];
				previousAxisMagnitude = new float[MaxAxes];
				IsAxisInverted = new bool[MaxAxes];

				// Set our default options.
				IsHatswitchEnabled = true;
				IsRumbleEnabled = true;
				for (int i = 0; i < MaxAxes; i++)
					IsAxisInverted[i] = false;
			}
			else
			{
				// Auxiliary controllers don't actually exist.
				Exists = false;
			}

			// Record success.
			if (index > 0)
				DebugLog.Success("   Gamepad #" + index + " is ready to go!");
			else
				DebugLog.Success("   A new auxiliary gamepad object is ready to go!");
		}
		#endregion

		#region Constants
		/// <summary>
		/// The maximum number of gamepad stick axes supported by the engine.
		/// </summary>
		public const int MaxAxes = 16;

		/// <summary>
		/// The maximum number of gamepad buttons supported by the engine.
		/// </summary>
		public const int MaxButtons = 32;
		#endregion

		#region Gamepad Properties
		/// <summary>
		/// The number of gamepads recognized and connected to this system.
		/// </summary>
		public static int Count { get; private set; }

		/// <summary>
		/// Because gamepad sticks natrually "drift",
		/// this is the absolute value threshold a gamepad's stick must exceed to return a value other than 0.
		/// </summary>
		public static float DeadZone = .1f;

		/// <summary>
		/// Whether or not this gamepad actually exists.
		/// Auxiliary gamepads objects and gamepads that aren't connected to the system do not "exist".
		/// </summary>
		public bool Exists { get; private set; }

		/// <summary>
		/// The number of buttons on this gamepad.
		/// </summary>
		public int NumberOfButtons { get; private set; }

		/// <summary>
		/// Approximately how many control sticks this gamepad has.
		/// As other gamepad controls such as triggers are reported as axes,
		/// this number is usually a guess, and will not always be correct.
		/// </summary>
		public int NumberOfSticks { get; private set; }

		/// <summary>
		/// How many axes this gamepad has.
		/// </summary>
		public int NumberOfAxes { get; private set; }

		/// <summary>
		/// Whether or not this gamepad has a hat switch.
		/// </summary>
		/// <remarks>
		/// <para>Hat switches originated on flight sticks, mimicing the hat switch on the joysticks of actual aircraft.
		/// However many gamepads, including the XBox 360 controller, also incorporate them.</para>
		/// <para>
		/// A hat switch is basically a D-Pad, and it reports boolean 2d movement.
		/// That is, the hat stick can be pressed up, down, left, right, and any of the four diagonals in-between.
		/// When not being pressed, it is considered "centered".
		/// </para>
		/// </remarks>
		public bool HasHatSwitch { get; private set; }
		#endregion

		#region Gamepad Settings
		/// <summary>
		/// Whether a given stick axis is to be inverted.
		/// Can be done for preference, or because the stick is not being reported correctly.
		/// </summary>
		public bool[] IsAxisInverted;

		/// <summary>
		/// Whether input from the hat switch is enabled when checking directional input.
		/// </summary>
		public bool IsHatswitchEnabled { get; set; }
		#endregion

		#region Gamepad Enumeration
		/// <summary>
    /// A list of all the gamepads.
    /// </summary>
    public static List<Gamepad> All { get; private set; }

    /// <summary>
    /// Gets the first gamepad.
    /// </summary>
    public static Gamepad One { get; private set; }

    /// <summary>
    /// Gets the second gamepad.
    /// </summary>
    public static Gamepad Two { get; private set; }

    /// <summary>
    /// Gets the third gamepad.
    /// </summary>
    public static Gamepad Three { get; private set; }

    /// <summary>
    /// Gets the fourth gamepad.
    /// </summary>
    public static Gamepad Four { get; private set; }

    /// <summary>
    /// An object that represents any and all active gamepads.
    /// </summary>
    public static Gamepad Any { get; private set; }

    /// <summary>
    /// An object that specifies no gamepad.
    /// </summary>
    public static Gamepad None { get; private set; }

    /// <summary>
    /// The gamepad that most recently had a button pressed.
    /// (The thumb sticks do not count in this case.)
    /// </summary>
    public static Gamepad LastPressed { get; set; }
		#endregion

		#region Stick Enumeration
		/// <summary>
		/// An enumeration of the thumb sticks.
		/// </summary>
		public enum Stick {
      /// <summary>
      /// The left thumbstick.
      /// This is usually the one used to control the action on-screen.
      /// </summary>
      Left,

      /// <summary>
      /// The right thumbstick.
      /// This is often the one used to control the camera.
      /// </summary>
      Right
    }
		#endregion

		#region Axis Enumeration
#if !ItIsWWII
    /// <summary>
    /// An enumeration of stick axes.
    /// </summary>
    public enum Axis { X, Y };
#elif ItIsWWII
      /// <summary>
      /// An enumeration of the Axis powers.
      /// </summary>
      public enum Axis {
        /// <summary>
        /// The Tsardom of Bulgaria.
        /// </summary>
        Bulgaria,

        /// <summary>
        /// Nazi Germany; the Third Reich.
        /// A major Axis Power.
        /// </summary>
        Germany,

        /// <summary>
        /// The Kingdom of Hungary.
        /// </summary>
        Hungary,
        
        /// <summary>
        /// The Kingdom of Italy.
        /// A major Axis Power.
        /// </summary>
        Italy,

        /// <summary>
        /// The Empire of Japan.
        /// A major Axis Power.
        /// </summary>
        Japan,

        /// <summary>
        /// The Kingdom of Romania.
        /// </summary>
        Romania
      }
#endif
		#endregion

		#region Hat Switch Enumeration
		/// <summary>
		/// The directions a hat switch can be moved.
		/// </summary>
		public enum Direction {
			/// <summary>
			/// When the hat stick is not being moved, it is centered.
			/// </summary>
			Center,
			
			/// <summary>
			/// The hat stick is being pressed up.
			/// </summary>
			Up,

			/// <summary>
			/// The hat stick is being pressed down.
			/// </summary>
			Down,

			/// <summary>
			/// The hat stick is being pressed left.
			/// </summary>
			Left,

			/// <summary>
			/// The hat stick is being pressed right.
			/// </summary>
			Right,

			/// <summary>
			/// The hat stick is being pressed up and left.
			/// </summary>
			UpLeft,

			/// <summary>
			/// The hat stick is being pressed up and right.
			/// </summary>
			UpRight,

			/// <summary>
			/// The hat stick is being pressed down and left.
			/// </summary>
			DownLeft,

			/// <summary>
			/// The hat stick is being pressed down and right.
			/// </summary>
			DownRight,
		}
		#endregion

		#region Gamepad State
		/// <summary>
    /// Gets this controller's index.
    /// -1 indicates that it has none and that this controller is just a handle or shortcut for a larger controller macro
		/// (for example, the way Controller.Any is used to check a button press from any controllers).
    /// </summary>
    public int Index { get; private set; }

    /// <summary>
    /// Whether a given button is down at the moment.
    /// </summary>
    private bool[] isDown;

		/// <summary>
		/// Whether a given button was down last frame.
		/// </summary>
		private bool[] wasDown;

		/// <summary>
		/// The current direction that the hat switch is being held.
		/// </summary>
		/// <remarks>Controllers without hat switches,
		/// and those with the hat switch manually disabled,
		/// will always report Direction.Center.</remarks>
		public Direction HatSwitchDirection { get; private set; }

		/// <summary>
		/// Gets the direction of the hat switch last frame.
		/// </summary>
		private Direction hatSwitchDirectionLastFrame { get; set; }

		/// <summary>
		/// Gets whether or not the hat switch was pressed this frame.
		/// </summary>
		public bool WasHatSwitchPressedThisFrame { get; private set; }

		/// <summary>
		/// The direction that the hat stick was being held last frame.
		/// </summary>
		/// <remarks>Controllers without hat sticks will always report Direction.Center.</remarks>
		public Direction PreviousHatStickDirection { get; private set; }

		/// <summary>
		/// Sets all buttons and sticks to their default, neutral state.
		/// </summary>
		private void resetAllButtons() {
			for(int i = 0; i < isDown.Length; i++)
				isDown[i] = false;			
		}

		/// <summary>
		/// The current magnitudes of all axes.
		/// </summary>
		private float[] axisMagnitude;

		/// <summary>
		/// The magnitude of all axes last frame.
		/// </summary>
		private float[] previousAxisMagnitude;
		#endregion

		#region Rumble Power!
		/// <summary>
		/// Whether this gamepad is allowed to use the rumble feature.
		/// </summary>
		public bool IsRumbleEnabled { get; set; }

		/// <summary>
		/// Whether this gamepad has a rumble feature.
		/// </summary>
		public bool IsRumbleAvailable { get; private set; }

		/// <summary>
		/// Sets the gamepad to rumble for the given number of frames.
		/// </summary>
		/// <param name="intensity">The intensity of the rumble, from 0 to 1.</param>
		/// <param name="seconds">How many seconds the controller should rumble for.</param>
		public void Rumble(float intensity, float seconds) {
#if SDL
			if (IsRumbleAvailable)
			{
				SDL.Haptic.RumblePlay(SDL_hapticID, intensity, Convert.ToUInt32(seconds * 1000));
			}
#endif
		}

		/// <summary>
		/// Sets the gamepad to rumble for the given number of frames.
		/// </summary>
		/// <param name="lowMotor">How hard the low-frequency rumbler should function.</param>
		/// <param name="highMotor">How hard the high-frequency rumbler should function.</param>
		/// <param name="seconds">How many seconds this controller should rumble for.</param>
		public void Rumble(float lowMotor, float highMotor, float seconds) {
#if SDL
			if (IsRumbleAvailable) {
				SDL.Haptic.RumblePlay(SDL_hapticID, highMotor + lowMotor / 2, Convert.ToUInt32(seconds * 1000));
      }
#endif
		}

		/// <summary>
		/// Immediantly stops this gamepad's rumbling.
		/// </summary>
		public void StopRumble()
		{
#if SDL
			if(IsRumbleAvailable)
			{
				SDL.Haptic.StopAll(SDL_hapticID);
			}
#endif
		}
		#endregion

		#region Primary Button Check Class
		/// <summary>
		/// Checks if the given button is currently being held down by the user.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>True if the button is being held. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Held(Button button)
		{
			// Is the button a valid value?
			if ((int)button < 0 || (int)button >= MaxButtons)
				return false;

			// Check if we're supposed to look through all the controllers.
			if (this == Gamepad.Any)
			{
				// Return true if at least one controller is holding the button.
				for (int i = 0; i < Gamepad.All.Count; i++)
				{
					if (All[i].Held(button))
						return true;
				}

				// Looks like the button was not being held on any controller.
				return false;
			} else {
				// We'll just check ourselves then.
				if (isDown[(int)button - 1])
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Checks if any of the given buttons are being held down by the user.
		/// </summary>
		/// <param name="buttons">The button(s) to check.</param>
		/// <returns>True if any of the buttons is being held. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Held(params Button[] buttons) {
			// Just takes one.
			foreach (Button button in buttons) {
				if(Held(button))
					return true;
			}

			// No buttons were being held.
			return false;
    }

		/// <summary>
		/// Checks if any of the given buttons are being held down by the user.
		/// </summary>
		/// <param name="buttons">The button(s) to check.</param>
		/// <returns>True if any of the buttons is being held. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Held(List<Button> buttons) {
			// Just takes one.
			foreach (Button button in buttons)
			{
				if (Held(button))
					return true;
			}

			// No buttons were being held.
			return false;
		}

		/// <summary>
		/// Checks if the given button was pressed by the user.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>True if the button was just pressed this frame. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Pressed(Button button)
		{
			// Is the button a valid value?
			if ((int)button < 0 || (int)button >= MaxButtons)
				return false;

			// Check if we're supposed to look through all the controllers.
			if (this == Gamepad.Any)
			{
				// Return true if at least one controller pressed the button.
				for (int i = 0; i < Gamepad.All.Count; i++)
				{
					if (All[i].Pressed(button))
						return true;
				}

				// Looks like the button was not pressed on any controller.
				return false;
			}
			else
			{
				// We'll just check ourselves then.
				if (isDown[(int)button - 1] && !wasDown[(int)button - 1])
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Checks if any of the given buttons were pressed by the user.
		/// </summary>
		/// <param name="buttons">The button(s) to check.</param>
		/// <returns>True if any of the buttons were pressed down this frame. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Pressed(params Button[] buttons)
		{
			// Just takes one.
			foreach (Button button in buttons)
			{
				if (Pressed(button))
					return true;
			}

			// No buttons were being held.
			return false;
		}

		/// <summary>
		/// Checks if any of the given buttons were pressed by the user.
		/// </summary>
		/// <param name="buttons">The button(s) to check.</param>
		/// <returns>True if any of the buttons were pressed down this frame. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Pressed(List<Button> buttons)
		{
			// Just takes one.
			foreach (Button button in buttons)
			{
				if (Pressed(button))
					return true;
			}

			// No buttons were being held.
			return false;
		}

		/// <summary>
		/// Checks if the given button was released by the user.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>True if the button was lifted this frame. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Released(Button button)
		{
			// Is the button a valid value?
			if ((int)button < 0 || (int)button >= MaxButtons)
				return false;

			// Check if we're supposed to look through all the controllers.
			if (this == Gamepad.Any)
			{
				// Return true if at least one controller released the button.
				for (int i = 0; i < Gamepad.All.Count; i++)
				{
					if (All[i].Released(button))
						return true;
				}

				// Looks like the button was not released on any controller.
				return false;
			}
			else
			{
				// We'll just check ourselves then.
				if (!isDown[(int)button - 1] && wasDown[(int)button - 1])
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Checks if any of the given buttons were released by the user.
		/// </summary>
		/// <param name="buttons">The button(s) to check.</param>
		/// <returns>True if any of the buttons were lifted this frame. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Released(params Button[] buttons)
		{
			// Just takes one.
			foreach (Button button in buttons)
			{
				if (Released(button))
					return true;
			}

			// No buttons were released.
			return false;
		}

		/// <summary>
		/// Checks if any of the given buttons were released by the user.
		/// </summary>
		/// <param name="buttons">The button(s) to check.</param>
		/// <returns>True if any of the buttons were lifted this frame. False otherwise.
		/// Will also return false if parameter button is an invalid value.</returns>
		public bool Released(List<Button> buttons)
		{
			// Just takes one.
			foreach (Button button in buttons)
			{
				if (Released(button))
					return true;
			}

			// No buttons were released.
			return false;
		}
		#endregion

		#region Additional Button Checks
    /// <summary>
    /// Checks both the B button and the Back button as one unit,
		/// to see if they were pressed.
    /// </summary>
    /// <returns>True if either the B button or Back button were pressed down this frame.</returns>
    public bool Cancel() {
      if(Pressed(Button.B) || Pressed(Button.Back))
        return true;
      else
				return false;
    }

		/// <summary>
		/// Gets whether or not the primary stick or hat switch was pressed up this frame.
		/// </summary>
		public bool PressedUp { get { return checkUp(true, IsHatswitchEnabled); } }

		/// <summary>
		/// Gets whether or not the primary stick or hat switch is being held up.
		/// </summary>
		public bool HeldUp { get { return checkUp(false, IsHatswitchEnabled); } }

		/// <summary>
		/// Checks if the primary stick or hat switch is being held up.
		/// </summary>
		/// <param name="noRepeat">If true, this method will only return true if the change happened this frame.</param>
		/// <param name="checkHat">Whether the hat switch is to be checked.</param>
		private bool checkUp(bool noRepeat, bool checkHat) {
			// Check if we're supposed to look through all the gamepads.
			if(this == Gamepad.Any) {
				for(int i = 0; i < Gamepad.All.Count; i++) {
					if(Gamepad.All[i].Exists && Gamepad.All[i].checkUp(noRepeat, checkHat)) {
						Gamepad.LastPressed = Gamepad.All[i];
						// This controller pressed the button.
						return true;
					}
				}

				// The button was not pressed on any gamepad.
				return false;
			} else if(!this.Exists) {
				// This gamepad doesn't even exist.
				return false;
			}

			// Check the hat switch.
			if(checkHat && (!noRepeat || WasHatSwitchPressedThisFrame)) {
				switch(HatSwitchDirection) {
					case Direction.Up:
					case Direction.UpLeft:
					case Direction.UpRight:
						return true;
				}
			}

			// Check the left stick.
			if(NumberOfAxes >= 2 && axisMagnitude[1] < -.5f &&
				(!noRepeat || previousAxisMagnitude[1] > -.5f)) return true;

			// Doesn't look like either was pressed.
			return false;
		}

		/// <summary>
		/// Gets whether or not the primary stick or hat switch was pressed down this frame.
		/// </summary>
		public bool PressedDown { get { return checkDown(true, IsHatswitchEnabled); } }

		/// <summary>
		/// Gets whether or not the primary stick or hat switch is being held down.
		/// </summary>
		public bool HeldDown { get { return checkDown(false, IsHatswitchEnabled); } }

		/// <summary>
		/// Checks if the primary stick or hat switch is being held down.
		/// </summary>
		/// <param name="noRepeat">If true, this method will only return true if the change happened this frame.</param>
		/// <param name="checkHat">Whether the hat switch is to be checked.</param>
		private bool checkDown(bool noRepeat, bool checkHat) {
			// Check if we're supposed to look through all the gamepads.
			if(this == Gamepad.Any) {
				for(int i = 0; i < Gamepad.All.Count; i++) {
					if(Gamepad.All[i].Exists && Gamepad.All[i].checkDown(noRepeat, checkHat)) {
						Gamepad.LastPressed = Gamepad.All[i];
						// This controller pressed the button.
						return true;
					}
				}

				// The button was not pressed on any gamepad.
				return false;
			} else if(!this.Exists) {
				// This gamepad doesn't even exist.
				return false;
			}

			// Check the hat switch.
			if(checkHat && (!noRepeat || WasHatSwitchPressedThisFrame)) {
				switch(HatSwitchDirection) {
					case Direction.Down:
					case Direction.DownLeft:
					case Direction.DownRight:
						return true;
				}
			}

			// Check the left stick.
			if(NumberOfAxes >= 2 && axisMagnitude[1] > .5f &&
				(!noRepeat || previousAxisMagnitude[1] < .5f)) return true;

			// Doesn't look like either was pressed.
			return false;
		}

		/// <summary>
		/// Gets whether or not the primary stick or hat switch was pressed left this frame.
		/// </summary>
		public bool PressedLeft { get { return checkLeft(true, IsHatswitchEnabled); } }

		/// <summary>
		/// Gets whether or not the primary stick or hat switch is being held left.
		/// </summary>
		public bool HeldLeft { get { return checkLeft(false, IsHatswitchEnabled); } }
		
		/// <summary>
		/// Checks if the primary stick or hat switch is being held right.
		/// </summary>
		/// <param name="noRepeat">If true, this method will only return true if the change happened this frame.</param>
		/// <param name="checkHat">Whether the hat switch is to be checked.</param>
		private bool checkLeft(bool noRepeat, bool checkHat) {
			// Check if we're supposed to look through all the gamepads.
			if(this == Gamepad.Any) {
				for(int i = 0; i < Gamepad.All.Count; i++) {
					if(Gamepad.All[i].Exists && Gamepad.All[i].checkLeft(noRepeat, checkHat)) {
						Gamepad.LastPressed = Gamepad.All[i];
						// This controller pressed the button.
						return true;
					}
				}

				// The button was not pressed on any gamepad.
				return false;
			} else if(!this.Exists) {
				// This gamepad doesn't even exist.
				return false;
			}

			// Check the hat switch.
			if(checkHat && (!noRepeat || WasHatSwitchPressedThisFrame)) {
				switch(HatSwitchDirection) {
					case Direction.Left:
					case Direction.UpLeft:
					case Direction.DownLeft:
						return true;
				}
			}

			// Check the left stick.
			if(NumberOfAxes >= 2 && axisMagnitude[0] < -.5f &&
				(!noRepeat || previousAxisMagnitude[0] > -.5f)) return true;

			// Doesn't look like either was pressed.
			return false;
		}

		/// <summary>
		/// Gets whether or not the primary stick or hat switch was pressed right this frame.
		/// </summary>
		public bool PressedRight { get { return checkRight(true, IsHatswitchEnabled); } }

		/// <summary>
		/// Gets whether or not the primary stick or hat switch is being held right.
		/// </summary>
		public bool HeldRight { get { return checkRight(false, IsHatswitchEnabled); } }

		/// <summary>
		/// Checks if the primary stick or hat switch is being held right.
		/// </summary>
		/// <param name="noRepeat">If true, this method will only return true if the change happened this frame.</param>
		/// <param name="checkHat">Whether the hat switch is to be checked.</param>
		private bool checkRight(bool noRepeat, bool checkHat) {
				// Check if we're supposed to look through all the gamepads.
				if(this == Gamepad.Any) {
					for(int i = 0; i < Gamepad.All.Count; i++) {
						if(Gamepad.All[i].Exists && Gamepad.All[i].checkRight(noRepeat, checkHat)) {
							Gamepad.LastPressed = Gamepad.All[i];
							// This controller pressed the button.
							return true;
						}
					}

					// The button was not pressed on any gamepad.
					return false;
				} else if(!this.Exists) {
					// This gamepad doesn't even exist.
					return false;
				}

				// Check the hat switch.
				if(checkHat && (!noRepeat || WasHatSwitchPressedThisFrame)) {
					switch(HatSwitchDirection) {
						case Direction.Right:
						case Direction.UpRight:
						case Direction.DownRight:
							return true;
					}
				}

				// Check the left stick.
				if(NumberOfAxes >= 2 && axisMagnitude[0] > .5f &&
					(!noRepeat || previousAxisMagnitude[0] < .5f)) return true;

				// Doesn't look like either was pressed.
				return false;
		}
		#endregion

		#region Stick Checks
		/// <summary>
		/// Checks the state of the given axis.
		/// </summary>
		/// <param name="index">The index of the axis to check.</param>
		/// <returns>Will always be 0 if the given axis does not exist.</returns>
		public float AxisMagnitude(int index) {
			// Make sure it's a valid axis.
			if(index < 0 || index >= NumberOfAxes)
				return 0;

			// Return the current value.
			return axisMagnitude[index];
		}

    /// <summary>
    /// Returns the angle (in revolutions) that the player is holding the left stick at.
    /// </summary>
    /// <returns>A number from 0 to 1, or -1 if the stick is not being moved. 0 is up, and a higher number
    /// signifies clockwise rotation.</returns>
    public float LeftStickAngle() {
      float x = LeftStickX();
      float y = LeftStickY();

      if(x == 0 && y == 0) {
        return -1; //The user is not moving the stick at all.
      }

      return -Mather.Atan2(x, y) - .5f;
    }

    /// <summary>
    /// Returns a number representing how far the left stick is being held from its center.
    /// </summary>
    /// <returns>A number from 0 and 1. 0 is center, 1 is all the way to the side.</returns>
    public float LeftStickMagnitude() {
      float x = (float)Math.Abs(LeftStickX());
      float y = (float)Math.Abs(LeftStickY());
      float magnitude = (float)Math.Sqrt(x * x + y * y);
			
			if(Mather.Abs(magnitude) < DeadZone)
				return 0;
			else
      return magnitude;
    }

    /// <summary>
    /// Returns the status of the x-axis on the left stick.
    /// </summary>
    /// <returns>A number from -1.0 and 1.0</returns>
    public float LeftStickX() {
      return checkStick(Stick.Left, Axis.X);
    }

    /// <summary>
    /// Returns the status of the y-axis on the left stick.
    /// </summary>
    /// <returns>A number from -1.0 and 1.0</returns>
    public float LeftStickY() {
      return checkStick(Stick.Left, Axis.Y);
    }

    /// <summary>
    /// Returns the angle (in revolutions) that the player is holding the right stick at.
    /// </summary>
    /// <returns>A number from 0 to 1, or -1 if the stick is not being moved. 0 is up, and a higher number
    /// signifies clockwise rotation.</returns>
    public float RightStickAngle() {
      float x = RightStickX();
      float y = RightStickY();

      if(x == 0 && y == 0) {
        return -1; //The user is not moving the stick at all.
      }

      return -Mather.Atan2(x, y) - .5f;
    }

    /// <summary>
    /// Returns a number representing how far the right stick is being held from its center.
    /// </summary>
    /// <returns>A number from 0 and 1. 0 is center, 1 is all the way to the side.</returns>
    public float RightStickMagnitude() {
      float x = (float)Math.Abs(RightStickX());
      float y = (float)Math.Abs(RightStickY());
      float magnitude = (float)Math.Sqrt(x * x + y * y);
			if(Mather.Abs(magnitude) < DeadZone)
				return 0;
			else
				return magnitude;
    }

    /// <summary>
    /// Returns the status of the x-axis on the right stick.
    /// </summary>
    /// <returns>A number from -1.0 and 1.0</returns>
    public float RightStickX() {
      return checkStick(Stick.Right, Axis.X);
    }

    /// <summary>
    /// Returns the status of the y-axis on the right stick.
    /// </summary>
    /// <returns>A number from -1.0 and 1.0</returns>
    public float RightStickY() {
      return checkStick(Stick.Right, Axis.Y);
    }

    /// <summary>
    /// Gets the status of a given stick.
    /// </summary>
    /// <param name="StickToCheck">Which stick to check.</param>
    /// <param name="AxisToCheck">Which axis to check.</param>
    /// <returns>A float from -1.0 to 1.0 detailing the current state of the given stick axis.</returns>
    private float checkStick(Stick StickToCheck, Axis AxisToCheck) {
#if OPENGL

#elif XNA
      PlayerIndex xnaPlayer = XNAGetPlayer(this);
#endif
      float fValue = 0.0f;

      switch(StickToCheck) {
        case Stick.Left:
          switch(AxisToCheck) {
            case Axis.X:
#if OPENGL
							fValue = AxisMagnitude(0);
#elif XNA
              fValue = GamePad.GetState(xnaPlayer).ThumbSticks.Left.X;
#endif
              break;
            case Axis.Y:
#if OPENGL
							fValue = AxisMagnitude(1) * -1;
#elif XNA
              fValue = GamePad.GetState(xnaPlayer).ThumbSticks.Left.Y * -1;
#endif
              break;
          }
          break;
        case Stick.Right:
          switch(AxisToCheck) {
            case Axis.X:
#if XNA
              fValue = GamePad.GetState(xnaPlayer).ThumbSticks.Right.X;
#endif
              break;
            case Axis.Y:
#if XNA
              fValue = GamePad.GetState(xnaPlayer).ThumbSticks.Right.Y * -1;
#endif
              break;
          }
          break;
      }

      return fValue; // Done               
    }
		#endregion

		#region Update Cycle
		/// <summary>
		/// Updates the status of all gamepads.
		/// </summary>
		internal static void UpdateAll() {
#if OPENGL
			// TODO:
			// Check if gamepads have been added or removed.
			// If so, reinitialize.
#endif

			for(int i = 0; i < Gamepad.All.Count; i++) {
				Gamepad.All[i].update();
			}
		}

    /// <summary>
    /// Updates the status of this gamepad.
    /// </summary>
    private void update() {
			// Only check controllers that exist.
			if(!Exists)
				return;

			// Copy last frame's state.
			isDown.CopyTo(wasDown, 0);

			// Clear the status of this gamepad for the frame.
			resetAllButtons();

			// Check button states.			
#if SDL
			for(int i = 0; i < MaxButtons; i++) {
				if (i < NumberOfButtons && SDL.Joystick.GetButton(SDL_joystickID, i) == 1)
				{
					LastPressed = this;
					isDown[i] = true;
				}
				else
				{
					isDown[i] = false;
				}
			}
#endif

			// Check stick states.
#if SDL
			axisMagnitude.CopyTo(previousAxisMagnitude, 0);
			for(int i = 0; i < MaxAxes; i++) {
				if (i < NumberOfAxes)
				{
					axisMagnitude[i] = SDL.Joystick.GetAxis(SDL_joystickID, i) / short.MaxValue;
					if (IsAxisInverted[i])
						axisMagnitude[i] *= -1;
				}
				else
				{
					axisMagnitude[i] = 0;
				}
			}
#endif

			// Check hat switch
			hatSwitchDirectionLastFrame = HatSwitchDirection;
			if (IsHatswitchEnabled)
			{
#if SDL
				// Check the hat axes.
				bool up = false;
				bool down = false;
				bool left = false;
				bool right = false;

				if (HasHatSwitch)
				{
					if ((SDL.Joystick.GetHat(SDL_joystickID, 0) & SDL.Hat.Up) != 0) up = true;
					if ((SDL.Joystick.GetHat(SDL_joystickID, 0) & SDL.Hat.Down) != 0) down = true;
					if ((SDL.Joystick.GetHat(SDL_joystickID, 0) & SDL.Hat.Left) != 0) left = true;
					if ((SDL.Joystick.GetHat(SDL_joystickID, 0) & SDL.Hat.Right) != 0) right = true;
				}
				else
				{
					// Some platforms like to report the d-pad/hat as buttons.
					if (SDL.Joystick.GetButton(SDL_joystickID, 11) == 1) up = true;
					if (SDL.Joystick.GetButton(SDL_joystickID, 12) == 1) down = true;
					if (SDL.Joystick.GetButton(SDL_joystickID, 13) == 1) left = true;
					if (SDL.Joystick.GetButton(SDL_joystickID, 14) == 1) right = true;
				}

				// Apply states.
				if(up && left)
					HatSwitchDirection = Direction.UpLeft;
				else if (up && right)
					HatSwitchDirection = Direction.UpRight;
				else if (down && left)
					HatSwitchDirection = Direction.DownLeft;
				else if (down && right)
					HatSwitchDirection = Direction.DownRight;
				else if (up)
					HatSwitchDirection = Direction.Up;
				else if (down)
					HatSwitchDirection = Direction.Down;
				else if(left)
					HatSwitchDirection = Direction.Left;
				else if (right)
					HatSwitchDirection = Direction.Right;
				else
					HatSwitchDirection = Direction.Center;
#endif
			}
			else
			{
				HatSwitchDirection = Direction.Center;
			}

			if(HatSwitchDirection != hatSwitchDirectionLastFrame) {
				WasHatSwitchPressedThisFrame = true;
			} else {
				WasHatSwitchPressedThisFrame = false;
			}
    }
		#endregion

		#region API Interface
#if SDL
		/// <summary>
		/// A pointer to the SDL joystick structure used by this gamepad.
		/// </summary>
		private IntPtr SDL_joystickID;

		/// <summary>
		/// A pointer to the SDL haptic structure used to power this gamepad's rumble features (if any).
		/// </summary>
		private IntPtr SDL_hapticID;
#endif
#endregion
	}
}
