﻿using System;
using System.Collections.Generic;
using System.Linq;

#if OPENGL
using OpenTK;
using glIN = OpenTK.Input;
#elif XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using XNAin = Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
#endif

namespace VisionRiders.OFE.Input {
  /// <summary>
  /// This class is used to get input from the user's keyboard.
  /// </summary>
  public class Keyboard {
		#region Initialization
		/// <summary>
		/// Initializes the keyboard.
		/// </summary>
		internal static void Init()
		{
			// Tell the log our situation.
			DebugLog.Write("Initializing the keyboard...");

			// Initialize our arrays.
			isDown = new bool[numKeys];
			wasPressed = new bool[numKeys];
			wasReleased = new bool[numKeys];
			downBuffer = new bool[numKeys];
			pressBuffer = new bool[numKeys];
			releaseBuffer = new bool[numKeys];
		}
		#endregion

		#region States
		/// <summary>
		/// The size of the key state arrays.
		/// </summary>
		private const int numKeys = 256;

		/// <summary>
		/// Whether a key is being held down this frame.
		/// </summary>
		private static bool[] isDown;

		/// <summary>
		/// Whether a key was pressed this frame.
		/// </summary>
		private static bool[] wasPressed;

		/// <summary>
		/// Whether a key was released this frame.
		/// </summary>
		private static bool[] wasReleased;

		/// <summary>
		/// Buffer for key down state over the course of a frame.
		/// </summary>
		private static bool[] downBuffer;

		/// <summary>
		/// Buffer for key pressed state over the course of a frame.
		/// </summary>
		private static bool[] pressBuffer;

		/// <summary>
		/// Buffer for key released state over the course of a frame.
		/// </summary>
		private static bool[] releaseBuffer;
		#endregion

		#region Checks
		/// <summary>
		/// Checks if any of the given keys are being held down.
		/// </summary>
		/// <param name="keyToCheck">The key (or keys) on the keyboard to check.</param>
		/// <returns>True if any one of the keys are down.</returns>
		public static bool Held(params Key[] keyToCheck) {
			foreach(Key key in keyToCheck) {
				if(Keyboard.isDown[(int)key]) {
					return true;
				}
			}
			return false;
    }

		/// <summary>
		/// Checks if any of the given keys are being held down.
		/// </summary>
		/// <param name="keysToCheck">A list of the keys to check.</param>
		/// <returns>True if any one of the keys are down.</returns>
		public static bool Held(List<Key> keysToCheck) {
			foreach(Key key in keysToCheck) {
				if(Keyboard.isDown[(int)key]) {
					return true;
				}
			}
			return false;
		}

    /// <summary>
    /// Checks if any of the given keys were pressed.
    /// Only returns true the first frame that the key is held down.
    /// </summary>
		/// <param name="keyToCheck">The key (or keys) on the keyboard to check.</param>
		/// <returns>True if any one of the keys was first pressed this frame.</returns>
    public static bool Pressed(params Key[] keyToCheck) {
			foreach(Key key in keyToCheck) {
				if(wasPressed[(int)key]) {
					return true;
				}
			}
			return false;
    }

		/// <summary>
		/// Checks if any of the given keys were pressed.
		/// Only returns true the first frame that the key is held down.
		/// </summary>
		/// <param name="keysToCheck">The key (or keys) on the keyboard to check.</param>
		/// <returns>True if any one of the keys was first pressed this frame.</returns>
		public static bool Pressed(List<Key> keysToCheck) {
			foreach(Key key in keysToCheck) {
				if(Keyboard.wasPressed[(int)key]) {
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Checks if any of the given keys were released.
		/// Only returns true the first frame that the key is released after being held down the previous frame.
		/// </summary>
		/// <param name="keyToCheck">The key (or keys) on the keyboard to check.</param>
		/// <returns>True if any one of the keys was first released this frame.</returns>
		public static bool Released(params Key[] keyToCheck) {
			foreach(Key key in keyToCheck) {
				if(wasReleased[(int)key]) {
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Checks if any of the given keys were released.
		/// Only returns true the first frame that the key is released after being held down the previous frame.
		/// </summary>
		/// <param name="keyToCheck">The key (or keys) on the keyboard to check.</param>
		/// <returns>True if any one of the keys was first released this frame.</returns>
		public static bool Released(List<Key> keysToCheck) {
			foreach(Key key in keysToCheck) {
				if(wasReleased[(int)key]) {
					return true;
				}
			}
			return false;
		}
		#endregion

		#region Manual Input
		/// <summary>
		/// Manually input a key press.
		/// </summary>
		/// <remarks>
		/// Invalid values for input are ignored.
		/// </remarks>
		/// <param name="key">The key to press.</param>
		public static void Press(Key key)
		{
			// Ignore invalid input.
			if ((int)key < 0 || (int)key >= numKeys)
				return;

			// Record.
			pressBuffer[(int)key] = true;
			downBuffer[(int)key] = true;
		}

		/// <summary>
		/// Manually input a key release.
		/// </summary>
		/// <remarks>
		/// Invalid values for input are ignored.
		/// </remarks>
		/// <param name="key">The key to release.</param>
		public static void Release(Key key)
		{
			// Ignore invalid input.
			if ((int)key < 0 || (int)key >= numKeys)
				return;

			// Record.
			releaseBuffer[(int)key] = true;
			downBuffer[(int)key] = false;
		}
		#endregion

		#region Update Cycle
		/// <summary>
		/// Updates all keyboard variables.
		/// </summary>
		internal static void Update() {
			// Copy the buffer to the current frame.
			downBuffer.CopyTo(isDown, 0);
			pressBuffer.CopyTo(wasPressed, 0);
			releaseBuffer.CopyTo(wasReleased, 0);

			// Reset buffer states.		
			for (int i =0;i< numKeys;i++)
			{
				pressBuffer[i] = false;
				releaseBuffer[i] = false;
			}
		}
		#endregion
  }
}
