﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using VisionRiders.OFE;
using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;

#if OPENGL
using OpenTK.Input;
#endif

namespace VisionRiders.OFE.Input {
  /// <summary>
  /// A class used to get input from a mouse, or other similar device (like a trackball).
  /// </summary>
  public class Mouse {
		#region Initialization
		/// <summary>
		/// Initializes the mouse.
		/// </summary>
		internal static void Init() {
			// Tell the log what we're doing.
			DebugLog.Write("Initializing the mouse...");
			
			// Create the mouse object.
			all = new List<Mouse>();
#if OPENGL
			glMouse = new MouseState();//Core.glWindow.Mouse;
#endif
			Default = new Mouse(1);

			// Record success.
			DebugLog.Success("The mouse has been initialized!");
		}
		#endregion

		#region All Mice
		/// <summary>
		/// The default mouse.
		/// Usually, this will be the only mouse in use by the system.
		/// </summary>
		public static Mouse Default { get; private set; }

		/// <summary>
		/// All the mice in use by the system.
		/// </summary>
		private static List<Mouse> all;
		#endregion

		#region Class Constructor
		/// <summary>
		/// Creates a new mouse object.
		/// </summary>
		/// <param name="index">
		/// The index to assign to the mouse.
		/// The deault mouse is index 1.
		/// </param>
		public Mouse(int index) {
			Index = index;
			all.Add(this);

      NumberOfButtons = 10;// glMouse.NumberOfButtons;
			buttonIsDown = new bool[NumberOfButtons];
			buttonWasDown = new bool[NumberOfButtons];
		}
		#endregion

		#region Properties
		/// <summary>
		/// The index of this mouse.
		/// </summary>
		public int Index { get; private set; }

		/// <summary>
    /// Whether or not the mouse has been activated.
		/// If the mouse is inactive, it will remain invisible and not be updated.
    /// </summary>
		public bool IsActive { get; set; }
		#endregion

		#region Button Enumeration
		/// <summary>
		/// An enumeration of the most common mouse buttons.
		/// </summary>
		/// <remarks>
		/// On some mice, there may be other buttons than these.
		/// OFE supports these extra buttons,
		/// but it is unwise to assign actions to them that cannot be replicated by other means.
		/// Otherwise, you'll likely lock out the majority of your application's intended audience.
		/// </remarks>
		public enum Button {
			/// <summary>
			/// The left mouse button.
			/// This is the primary mouse button, usually used to select things or issue commands.
			/// On older mice (especially those made by Apple), this may be the only button.
			/// </summary>
			Left = 0,

			/// <summary>
			/// The right mouse button.
			/// In graphical user interfaces, this button is usually used to bring up context-sensative dropdown menues.
			/// </summary>
			Right = 2,

			/// <summary>
			/// The middle mouse button.
			/// Has been merged with the scrollwheel on most modern mice.
			/// </summary>
			Middle = 1
		}
		#endregion

    #region Button States
		/// <summary>
		/// The number of buttons supported by this mouse.
		/// </summary>
		public int NumberOfButtons { get; private set; }

		/// <summary>
		/// An array that stores the state of all mouse buttons this frame.
		/// </summary>
		private bool[] buttonIsDown;

		/// <summary>
		/// An array that stores the state of all mouse buttons from the last frame.
		/// </summary>
		private bool[] buttonWasDown;

		/// <summary>
		/// Checks if the given button was pressed this frame.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>False if the given button does not exist.</returns>
		public bool Pressed(int button) {
			if(button >= buttonIsDown.Length) return false;
			if(buttonIsDown[button] && !buttonWasDown[button]) return true; else return false;
		}

		/// <summary>
		/// Checks if the given button was pressed this frame.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>False if the given button does not exist.</returns>
		public bool Pressed(Button button) {
			return Pressed((int)button);
		}

		/// <summary>
		/// Checks if the given button is currently being held down.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>False if the given button does not exist.</returns>
		public bool Held(int button) {
			if(button >= buttonIsDown.Length) return false;
			if(buttonIsDown[button]) return true; else return false;
		}

		/// <summary>
		/// Checks if the given button is currently being held down.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>False if the given button does not exist.</returns>
		public bool Held(Button button) {
			return Held((int)button);
		}

		/// <summary>
		/// Checks if the given button was lifted this frame.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>False if the given button does not exist.</returns>
		public bool Lifted(int button) {
			if(button >= buttonIsDown.Length) return false;
			if(!buttonIsDown[button] && buttonWasDown[button]) return true; else return false;
		}

		/// <summary>
		/// Checks if the given button was lifted this frame.
		/// </summary>
		/// <param name="button">The button to check.</param>
		/// <returns>False if the given button does not exist.</returns>
		public bool Lifted(Button button) {
			return Lifted((int)button);
		}

    /// <summary>
    /// Checks if the user has left-clicked on the mouse.
    /// </summary>
    /// <returns>True if the button was lifted this frame.</returns>
    public bool LeftClick() {
			return Lifted(Button.Left);
    }

    /// <summary>
    /// Checks if the user has clicked the middle mouse button.
    /// </summary>
    /// <returns>True if the button was lifted this frame.</returns>
    public bool MiddleClick() {
			return Lifted(Button.Middle);
    }

    /// <summary>
    /// Checks if the user has right-clicked on the mouse.
    /// </summary>
    /// <returns>True if the button was lifted this frame.</returns>
    public bool RightClick() {
			return Lifted(Button.Right);
    }
    #endregion

    #region Mouse Coordinates
    /// <summary>
    /// Gets whether or not the mouse cursor is on-screen.
    /// If false it is outside the program window.
    /// </summary>
    public bool IsOnScreen {
      get {
#if OPENGL
				if (AbsolutePosition.X > 0 && AbsolutePosition.X < Display.Width
							&& AbsolutePosition.Y > 0 && AbsolutePosition.Y < Display.Height)
				{
          return true;
        } else {
          return false;
        }
#endif
      }
    }

		/// <summary>
		/// Gets the absolute position of the mouse cursor, in window coordinates.
		/// </summary>
		public CoordInt AbsolutePosition { get; internal set; }

    /// <summary>
    /// The position of the mouse cursor on the X axis, in relation to the main display.
    /// Use a stage's Mouse coordinates to get the coordinates in relation to that stage.
    /// </summary>
    public float X {
      get {
#if OPENGL
        return AbsolutePosition.X * Display.Scale.X;
#elif XNA
        return xnaMouse.X * Display.Scale.X;
#endif
      }
      set {
#if OPENGL
        System.Windows.Forms.Cursor.Position = new System.Drawing.Point(Mather.Round(value / Display.Scale.X),
          System.Windows.Forms.Cursor.Position.Y);
#elif XNA
        Microsoft.Xna.Framework.Input.Mouse.SetPosition(Mather.Round(value / Display.Scale.X), xnaMouse.Y);
#endif
      }
    }

    /// <summary>
    /// The position of the mouse cursor on the Y axis, in relation to the main display.
    /// Use a stage's Mouse coordinates to get the coordinates in relation to that stage.
    /// </summary>
    public float Y {
      get {
#if OPENGL
				return AbsolutePosition.Y * Display.Scale.Y;
#elif XNA
        return xnaMouse.Y * Display.Scale.Y;
#endif
      }
      set {
#if OPENGL
        System.Windows.Forms.Cursor.Position = new System.Drawing.Point(System.Windows.Forms.Cursor.Position.X,
          Mather.Round(value / Display.Scale.Y));
#elif XNA
        Microsoft.Xna.Framework.Input.Mouse.SetPosition(xnaMouse.X, Mather.Round(value / Display.Scale.Y));
#endif
      }
    }
    #endregion

    #region Scroll Wheel
    /// <summary>
    /// The amount that the mouse's scroll wheel has moved this frame.
    /// Positive numbers indicate up, negative ones indicate down.
    /// </summary>
		public int Scroll { get; private set; }

    /// <summary>
    /// Whether or not the scroll wheel was moved up at all this frame.
    /// </summary>
    public bool ScrolledUp {
      get {
        if(Scroll > 0) {
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// Whether or not the scroll wheel was moved down at all this frame.
    /// </summary>
    public bool ScrolledDown {
      get {
        if(Scroll < 0) {
          return true;
        } else {
          return false;
        }
      }
    }

    /// <summary>
    /// For some unfathomable reason likely known to people smarter than me who actually know what
    /// they're doing™, XNA reports the scroll wheel value as an accumulated total since the program
    /// began. To translate this to a more standard (and useful) relative value, we'll need to keep
    /// track of the value each frame. Since other APIs use this method of wheel reporting, this
    /// private value is not hidden behind a #if block for universal use.
    /// </summary>
    private int scrollWheelLastFrame = 0;
    #endregion
		
    #region Update Cycle
		/// <summary>
		/// Updates all the mice.
		/// </summary>
		internal static void UpdateAll() {
			foreach(Mouse mouse in all) {
				mouse.Update();
			}
		}

    /// <summary>
    /// Checks this mouse and updates its properties each frame.
    /// </summary>
    internal void Update() {
      if(IsActive) {
				// Poll the mouse.
#if OPENGL
				glMouse = OpenTK.Input.Mouse.GetState(1);
#endif

				// Center the cursor on the mouse's current location. We must remember to take into account
				// the stage's origin point.
				if(Cursor != null) {
					Cursor.X = Cursor.Stage.Mouse.X;
					Cursor.Y = Cursor.Stage.Mouse.Y;
				}

				// Copy last frame's button states.
				buttonWasDown = (bool[])buttonIsDown.Clone();

				// Get the state of all buttons this frame.
				for(int i = 0; i < buttonIsDown.Length; i++) {
#if OPENGL
					if(glMouse.IsButtonDown((MouseButton)i)) {
						buttonIsDown[i] = true;
					} else {
						buttonIsDown[i] = false;
					}
#endif
				}

        // Check the scroll wheel.
#if OPENGL
        if(IsOnScreen) {
          Scroll = glMouse.Wheel - scrollWheelLastFrame;
        }
        scrollWheelLastFrame = glMouse.Wheel;
#endif

        // If we're in fullscreen mode, make sure the mouse doesn't escape from the display,
        // for the sake of those with multiple monitors--like me. :)
        if(Display.IsFullScreen) {
          if(X < 0) {
            X = 0;
          }
          if(X >= Display.Width) {
            X = Display.Width - 1;
          }
          if(Y < 0) {
            Y = 0;
          }
          if(Y >= Display.Height) {
            Y = Display.Height - 1;
          }
        }

        // If the cursor is not otherwise being hidden, we need to check whether or not it should be visible.
        if(!Hiding && ShouldBeVisible) {
          // If the cursor is outside the game window, hide it.
          if(!Display.IsFullScreen) {
            if(IsOnScreen) {
              Cursor.IsActive = true;
            } else {
              Cursor.IsActive = false;
            }
          } else {
            Cursor.IsActive = true;
          }
        } else {
          Cursor.IsActive = false;
        }
      }
    }
    #endregion

    #region Cursor
    /// <summary>
    /// The sprite that will be displayed as the mouse pointer.
    /// </summary>
    public Sprite Cursor { get; set; }

    /// <summary>
    /// Gets whether or not the mouse cursor has been hidden.
    /// </summary>
		public bool Hiding { get; private set; }

		/// <summary>
		/// Gets whether or not the mouse cursor should be displayed by the system.
		/// Set to false when the cursor moves out of the window, for example.
		/// </summary>
		public bool ShouldBeVisible { get; internal set; }

    /// <summary>
    /// Activates the mouse and makes its pointer visible.
    /// </summary>
    /// <param name="cursor">The sprite to set as the pointer.</param>
    public void Show(Sprite cursor) {
      // Turn the mouse on.
      IsActive = true;

      // Make sure the old cursor, if there is one, is deleted.
      if(Cursor != null) {
        Cursor.Dispose();
      }

      // Set the requested sprite to be the cursor.
      Cursor = cursor;

      // Get the mouse's initial state so that it doesn't "jump" the first frame.
      Update();
    }

    /// <param name="cursorImageFileName">The image file where the cursor graphic can be found.</param>
    public void Show(string cursorImageFileName) {
      // Turn the mouse on.
      IsActive = true;
      Hiding = false;

      // If there's already a cursor sprite, load the requested image to it. Otherwise, create a
      // new sprite for the cursor.
      if(Cursor == null) {
        Cursor = new Sprite(Stage.System, cursorImageFileName, 0.1f);
      } else {
        Cursor.Load(cursorImageFileName);
        Cursor.IsActive = true;
      }

      // Get the mouse's initial state so that it doesn't "jump" the first frame.
      Update();
    }
				
		/// <param name="texture">The texture to set as the cursor graphic.</param>
		public void Show(Texture texture)
		{
			// Turn the mouse on.
			IsActive = true;
			Hiding = false;

			// Set the texture.
			if (Cursor == null)
			{
				Cursor = new Sprite(Stage.System, new SpriteSource(texture), 0.1f);
			}
			else
			{
				Cursor.SetSource(new SpriteSource(texture));
				Cursor.IsActive = true;
			}

			// Get the mouse's initial state so that it doesn't "jump" the first frame.
		}

    public void Show() {
      // Turn the mouse on.
      IsActive = true;
      Hiding = false;

      // If there's a cursor sprite, enable it too.
      if(Cursor != null) {
        Cursor.IsActive = true;
      }

      // Get the mouse's initial state so that it doesn't "jump" the first frame.
      Update();
    }

    /// <summary>
    /// Deactivates the mouse and hides the pointer.
    /// </summary>
    public void Disable() {
#if !XBOX
      // Turn the mouse off.
      IsActive = false;

      // Deactivate the cursor sprite, if it exists.
      if(Cursor != null) {
        Cursor.IsActive = false;
      }
#endif
    }

    /// <summary>
    /// Hides the mouse cursor, without turning off all the other mouse-related functions.
    /// </summary>
    public void Hide() {
      if(Cursor != null) {
        Cursor.IsActive = false;
        Hiding = true;
      }
    }
    #endregion

		#region Platform APIs
#if OPENGL
		/// <summary>
		/// An object used to check the state of the mouse in OpenTK.
		/// </summary>
    private static OpenTK.Input.MouseState glMouse;
#endif
		#endregion
	}
}
