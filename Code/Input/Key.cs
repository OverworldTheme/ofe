﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisionRiders.OFE.Input
{
	/// <summary>
	/// An enumeration of keycode values.
	/// </summary>
	/// <remarks>
	/// <para>These values are based on the official USB HID usage tables.
	/// The specifications can be found on the USB Implementers Forum site.</para>
	/// <para>Some of these may map strangely to non-English keyboards, so be careful.</para>
	/// </remarks>
	public enum Key
	{
		#region Control Characters
		/// <summary>
		/// A value representing no key.
		/// </summary>
		None = 0x00,
		#endregion

		#region Latin Letters
		/// <summary>
		/// The A key.
		/// </summary>
		A = 0x04,

		/// <summary>
		/// The B key.
		/// </summary>
		B = 0x05,

		/// <summary>
		/// The C key.
		/// </summary>
		C = 0x06,

		/// <summary>
		/// The D key.
		/// </summary>
		D = 0x07,

		/// <summary>
		/// The E key.
		/// </summary>
		E = 0x08,

		/// <summary>
		/// The F key.
		/// </summary>
		F = 0x09,

		/// <summary>
		/// The G key.
		/// </summary>
		G = 0x0A,

		/// <summary>
		/// The H key.
		/// </summary>
		H = 0x0B,

		/// <summary>
		/// The I key.
		/// </summary>
		I = 0x0C,

		/// <summary>
		/// The J key.
		/// </summary>
		J = 0x0D,

		/// <summary>
		/// The K key.
		/// </summary>
		K = 0x0E,

		/// <summary>
		/// The L key.
		/// </summary>
		L = 0x0F,

		/// <summary>
		/// The M key.
		/// </summary>
		M = 0x10,

		/// <summary>
		/// The N key.
		/// </summary>
		N = 0x11,

		/// <summary>
		/// The O key.
		/// </summary>
		O = 0x12,

		/// <summary>
		/// The P key.
		/// </summary>
		P = 0x13,

		/// <summary>
		/// The Q key.
		/// </summary>
		Q = 0x14,

		/// <summary>
		/// The R key.
		/// </summary>
		R = 0x15,

		/// <summary>
		/// The S key.
		/// </summary>
		S = 0x16,

		/// <summary>
		/// The T key.
		/// </summary>
		T = 0x17,

		/// <summary>
		/// The U key.
		/// </summary>
		U = 0x18,

		/// <summary>
		/// The V key.
		/// </summary>
		V = 0x19,

		/// <summary>
		/// The W key.
		/// </summary>
		W = 0x1A,

		/// <summary>
		/// The X key.
		/// </summary>
		X = 0x1B,

		/// <summary>
		/// The Y key.
		/// </summary>
		Y = 0x1C,

		/// <summary>
		/// The Z key.
		/// </summary>
		Z = 0x1D,
		#endregion

		#region Numbers
		/// <summary>
		/// The primary 1 key.
		/// Is also the exclamation point ! on English language keyboards.
		/// </summary>
		Num1 = 0x1E,

		/// <summary>
		/// The primay 2 key.
		/// Is also the at sign @ on most English languaged keyboards.
		/// (On some British keyboards, it is the double quotation mark symbol " instead.)
		/// </summary>
		Num2 = 0x1F,

		/// <summary>
		/// The primary 3 key.
		/// Is also the pound/number sign # on American keyboards,
		/// and the British pound sign £ on British keyboards.
		/// </summary>
		Num3 = 0x20,

		/// <summary>
		/// The primary 4 key.
		/// Is also the dollar sign $ on English language keyboards.
		/// </summary>
		Num4 = 0x21,

		/// <summary>
		/// The primary 5 key.
		/// Is also the percent sign % on English language keyboards.
		/// </summary>
		Num5 = 0x22,

		/// <summary>
		/// The primary 6 key.
		/// Is also the caret ^ on English language keyboards.
		/// </summary>
		Num6 = 0x23,

		/// <summary>
		/// The primary 7 key.
		/// Is also the ampersand & on English language keyboards.
		/// </summary>
		Num7 = 0x24,

		/// <summary>
		/// The primary 8 key.
		/// Is also the asterisk/star symbol * on English language keyboards.
		/// </summary>
		Num8 = 0x25,

		/// <summary>
		/// The primary 9 key.
		/// Is also the opening (left) parenthesis ( on English language keyboards.
		/// </summary>
		Num9 = 0x26,

		/// <summary>
		/// The primary 0 key.
		/// Is also the closing (right) parenthesis ) on English language keyboards.
		/// </summary>
		Num0 = 0x27,
		#endregion

		#region Line Entry
		/// <summary>
		/// The enter key.
		/// Same as the return key, just with a different name.
		/// </summary>
		/// <remarks>
		/// Both enumeration values have the same keycode.
		/// </remarks>
		Enter = 0x28,

		/// <summary>
		/// The return key.
		/// Same as the enter key, just with a different name.
		/// </summary>
		/// <remarks>
		/// Both enumeration values have the same keycode.
		/// </remarks>
		Return = 0x28,

		/// <summary>
		/// The escape key.
		/// Usually denoted simply as ESC.
		/// </summary>
		Escape = 0x29,

		/// <summary>
		/// The backspace key.
		/// Called the delete key in some regions,
		/// it should not be confused with the delete forward key.
		/// </summary>
		Backspace = 0x02A,

		/// <summary>
		/// The insert key.
		/// </summary>
		Insert = 0x49,

		/// <summary>
		/// The forward delete key.
		/// It should not be confused with the backspace key,
		/// which is called the delete key in some regions.
		/// </summary>
		Delete = 0x4C,

		/// <summary>
		/// The tab key.
		/// </summary>
		Tab = 0x2B,

		/// <summary>
		/// The iconic spacebar.
		/// </summary>
		Space = 0x2C,
		#endregion

		#region Symbols
		/// <summary>
		/// The primary minus - key.
		/// Is also the underscore _ on English language keyboards.
		/// </summary>
		Minus = 0x2D,

		/// <summary>
		/// The equals = key.
		/// Is also the plus symbol + on English language keyboards.
		/// </summary>
		Equals = 0x2E,

		/// <summary>
		/// The opening (left) bracket [ key.
		/// Is also the opening (left) curly bracket symbol { on English language keyboards.
		/// </summary>
		LeftBracket = 0x2F,

		/// <summary>
		/// The closing (right) bracket ] key.
		/// Is also the closing (right) curly bracket symbol } on English language keyboards.
		/// </summary>
		RightBracket = 0x30,

		/// <summary>
		/// The backslash \ key.
		/// Is also the vertical bar (a.k.a. vbar, glidus, vunderbar, pipe symbol, etc.) character | on English language keyboards.
		/// </summary>
		Backslash = 0x31,

		/// <summary>
		/// The key for the pound (number sign) symbol # on some non-US keyboards.
		/// Is also the tilde ~ on most keyboards that have it.
		/// </summary>
		Pound = 0x32,

		/// <summary>
		/// The semicolon ; key.
		/// Is also the regular colon : on English language keyboards.
		/// </summary>
		Semicolon = 0x33,

		/// <summary>
		/// The aprotrophe (also single quotation mark) ' key.
		/// It is also the double quotation mark " on most English language keyboards.
		/// On some British keyboards it is the at sign @ instead of a double quoation mark.
		/// </summary>
		Apostrophe = 0x34,

		/// <summary>
		/// The grave accent ` key.
		/// (Also called the backtick, or backquote.)
		/// Is also the tilde character ~ on most English language keyboards.
		/// On some British keyboards it is the negation (logical complement) symbol ¬ insteat of a tilde.
		/// </summary>
		Grave = 0x35,

		/// <summary>
		/// The comma , key.
		/// Is also the less than (opening/left pointy bracket) &lt; on English language keyboards.
		/// </summary>
		Comma = 0x26,

		/// <summary>
		/// The period . key.
		/// Is also the greater than (closing/right pointy bracket) &gt; on English language keyboards.
		/// </summary>
		Period = 0x27,

		/// <summary>
		/// The forward slash / key.
		/// Is also the question mark ? on English language keyboards.
		/// </summary>
		Slash = 0x28,
		#endregion

		#region Navigation
		/// <summary>
		/// The up arrow key.
		/// </summary>
		Up =  0x52,

		/// <summary>
		/// The down arrow key.
		/// </summary>
		Down = 0x51,

		/// <summary>
		/// The left arrow key.
		/// </summary>
		Left = 0x50,

		/// <summary>
		/// The right arrow key.
		/// </summary>
		Right = 0x4F,

		/// <summary>
		/// The home key.
		/// </summary>
		Home = 0x4A,

		/// <summary>
		/// The end key.
		/// </summary>
		End = 0x4D,

		/// <summary>
		/// The page up key.
		/// </summary>
		PageUp = 0x4B,

		/// <summary>
		/// The page down key.
		/// </summary>
		PageDown = 0x4E,
		#endregion

		#region Locks
		/// <summary>
		/// The caps lock key.
		/// </summary>
		CapsLock = 0x39,

		/// <summary>
		/// The scroll lock (ScrLk) key.
		/// </summary>
		ScrollLock = 0x47,

		/// <summary>
		/// The number lock (num lock) key on the keypad.
		/// </summary>
		NumLock = 0x53,
		#endregion

		#region Special Keys
		/// <summary>
		/// The print screen key.
		/// On most English language layouts,
		/// this is also the system request (SysRq) key.
		/// </summary>
		PrintScreen = 0x46,

		/// <summary>
		/// The pause key.
		/// On most English language layouts,
		/// this is also the break key.
		/// </summary>
		Pause = 0x48,

		/// <summary>
		/// The menu key, found on many modern keyboard layouts.
		/// Also called the application key.
		/// </summary>
		Menu = 0x64,
		#endregion

		#region Function Keys
		/// <summary>
		/// The F1 function key.
		/// </summary>
		F1 = 0x3A,

		/// <summary>
		/// The F2 function key.
		/// </summary>
		F2 = 0x3B,

		/// <summary>
		/// The F3 function key.
		/// </summary>
		F3 = 0x3C,

		/// <summary>
		/// The F4 function key.
		/// </summary>
		F4 = 0x3D,

		/// <summary>
		/// The F5 function key.
		/// </summary>
		F5 = 0x3E,

		/// <summary>
		/// The F6 function key.
		/// </summary>
		F6 = 0x3F,

		/// <summary>
		/// The F7 function key.
		/// </summary>
		F7 = 0x40,

		/// <summary>
		/// The F8 function key.
		/// </summary>
		F8 = 0x41,

		/// <summary>
		/// The F9 function key.
		/// </summary>
		F9 = 0x42,

		/// <summary>
		/// The F10 function key.
		/// </summary>
		F10 = 0x43,

		/// <summary>
		/// The F11 function key.
		/// </summary>
		F11 = 0x44,

		/// <summary>
		/// The F12 function key.
		/// </summary>
		F12 = 0x45,

		/// <summary>
		/// <para>The F13 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F13 = 0x68,

		/// <summary>
		/// <para>The F14 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F14 = 0x69,

		/// <summary>
		/// <para>The F15 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F15 = 0x6A,

		/// <summary>
		/// <para>The F16 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F16 = 0x6B,

		/// <summary>
		/// <para>The F17 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F17 = 0x6C,

		/// <summary>
		/// <para>The F18 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F18 = 0x6D,

		/// <summary>
		/// <para>The F19 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F19 = 0x6E,

		/// <summary>
		/// <para>The F20 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F20 = 0x6F,

		/// <summary>
		/// <para>The F21 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F21 = 0x70,

		/// <summary>
		/// <para>The F22 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F22 = 0x71,

		/// <summary>
		/// <para>The F23 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F23 = 0x73,

		/// <summary>
		/// <para>The F24 function key.</para>
		/// <para>Rarely used in modern keyboard layouts.</para>
		/// </summary>
		F24 = 0x73,
		#endregion

		#region Right/Left Keys and Modifier Keys
		/// <summary>
		/// The left shift key.
		/// </summary>
		LeftShift = 0xE1,

		/// <summary>
		/// The right shift key.
		/// </summary>
		RightShift = 0xE5,

		/// <summary>
		/// The left control (ctrl) key.
		/// </summary>
		LeftCtrl = 0xE0,

		/// <summary>
		/// The right control (ctrl) key.
		/// </summary>
		RightCtrl = 0xE4,

		/// <summary>
		/// The left alt key.
		/// </summary>
		LeftAlt = 0xE2,

		/// <summary>
		/// The right alt key.
		/// </summary>
		RightAlt = 0xE6,

		/// <summary>
		/// <para>The left GUI key. The exact name varies from system to system.</para>
		/// <para>On Windows systems, this is the left Windows start key.
		/// On Apple systems, this is the left command ⌘ key.</para>
		/// </summary>
		LeftGui = 0xE3,

		/// <summary>
		/// <para>The right GUI key. The exact name varies from system to system.</para>
		/// <para>On Windows systems, this is the right Windows start key.
		/// On Apple systems, this is the right command ⌘ key.</para>
		/// </summary>
		RightGui = 0xE7,
		#endregion

		#region Keypad		
		/// <summary>
		/// The 1 key on the keypad.
		/// Alternatively, functions as the end key.
		/// </summary>
		Pad1 = 0x59,

		/// <summary>
		/// The 2 key on the keypad.
		/// Alternatively, functions as the down arrow key.
		/// </summary>
		Pad2 = 0x5A,

		/// <summary>
		/// The 3 key on the keypad.
		/// Alternatively, functions as the page down key.
		/// </summary>
		Pad3 = 0x5B,

		/// <summary>
		/// The 4 key on the keypad.
		/// Alternatively, functions as the left arrow key.
		/// </summary>
		Pad4 = 0x5C,

		/// <summary>
		/// The 5 key on the keypad.
		/// </summary>
		Pad5 = 0x5D,

		/// <summary>
		/// The 6 key on the keypad.
		/// Alternatively, functions as the right arrow key.
		/// </summary>
		Pad6 = 0x5E,

		/// <summary>
		/// The 7 key on the keypad.
		/// Alternatively, functions as the home key.
		/// </summary>
		Pad7 = 0x5F,

		/// <summary>
		/// The 8 key on the keypad.
		/// Alternatively, functions as the up arrow key.
		/// </summary>
		Pad8 = 0x60,

		/// <summary>
		/// The 9 key on the keypad.
		/// Alternatively, functions as the page up key.
		/// </summary>
		Pad9 = 0x61,

		/// <summary>
		/// The 0 key on the keypad.
		/// Alternatively, functions as the insert key.
		/// </summary>
		Pad0 = 0x62,

		/// <summary>
		/// The decimal key on the keypad.
		/// Alternatively, functions as the delete key.
		/// </summary>
		PadDot = 0x63,

		/// <summary>
		/// The addition + key on the keypad.
		/// </summary>
		PadAdd = 0x57,

		/// <summary>
		/// The subtraction - key on the keypad.
		/// </summary>
		PadSubtract = 0x56,

		/// <summary>
		/// The multiply * key on the keypad.
		/// </summary>
		PadMultiply = 0x55,

		/// <summary>
		/// The divide / key on the keypad.
		/// </summary>
		PadDivide = 0x54,

		/// <summary>
		/// The equals = key on the keypad,
		/// found on some keypad layouts.
		/// </summary>
		PadEquals = 0x67,

		/// <summary>
		/// The enter key on the keypad.
		/// </summary>
		PadEnter = 0x58,
		#endregion
	}
}
