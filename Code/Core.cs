﻿///////////////////////////////////////
//                                   //
//  THE OPEN-ENDED FRAMEWORK ENGINE  //
//                                   //
///////////////////////////////////////
//
//  Copyright Vision Riders Entertainment, LLC
//  All rights reserved.
//  See license file for usage details.


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

using VisionRiders.OFE.Audio;
using VisionRiders.OFE.Data;
using VisionRiders.OFE.Graphics;
using VisionRiders.OFE.Graphics.OFE2D;
using VisionRiders.OFE.Graphics.OFE3D;
using VisionRiders.OFE.GUI;
using VisionRiders.OFE.Input;

#if OPENGL
using OpenTK;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
#endif

#if SDL
using SDL2;
#endif

namespace VisionRiders.OFE {
  /// <summary>
  /// The central class that boots the engine and oversees the execution of logic and graphics updates.
  /// </summary>
  public static class Core {
    #region Version
    /// <summary>
    /// The version number information for this build of OFE.
    /// </summary>
    /// <remarks>The number is given as: Major.Minor.Revision
    /// For example: 1.2.17</remarks>
    public static class Version {
      /// <summary>
      /// The major version number.
      /// </summary>
      public const int Major = 0;

      /// <summary>
      /// The minor version number.
      /// </summary>
      public const int Minor = 3;

      /// <summary>
      /// The version revision number.
      /// </summary>
      public const int Revision = 0;

      /// <summary>
      /// Whether or not this is a stable build of OFE.
      /// </summary>
      public const bool IsStable = false;

      /// <summary>
      /// A string containing the full version number of this build of OFE.
      /// </summary>
      public static string Number {
        get {
          return "v" + Major + "." + Minor + "." + Revision;
        }
      }
    }
		#endregion

		#region Initialization
		/// <summary>
		/// Initializes the OFE engine.
		/// </summary>
		/// <param name="appName">The name of the program.
		/// This will also be used as the name of the folder where the application's primary data will be saved.
		/// The exact location of the folder will depend on the current platform.</param>
		/// <param name="setup">The method to call when it's time for your program to start initializing.</param>
		/// <param name="height">The virtual resolution height you want the game to display at.
		/// The width will be scaled depending on the user's screen ratio, and the entire display will be scaled to their screen.</param>
		/// <param name="updatesPerSecond">The number of times per second to call your program's update method.
		/// This will set how quickly your game runs. (60 updates per second is recommended.)</param>
		public static void Init(string appName, Action setup, int height, int updatesPerSecond) {
      Init(appName, setup, height, updatesPerSecond, updatesPerSecond);
    }

		/// <summary>
		/// Initializes the OFE engine.
		/// </summary>
		/// <param name="appName">The name of the program.
		/// This will also be used as the name of the folder where the application's primary data will be saved.
		/// The exact location of the folder will depend on the current platform.</param>
		/// <param name="appSetup">The method to call when it's time for your program to start initializing.</param>
		/// <param name="height">The virtual resolution height you want the game to display at.
		/// The width will be scaled depending on the user's screen ratio, and the entire display will be scaled to their screen.</param>
		/// <param name="updatesPerSecond">The number of times per second to call your program's update method.
		/// This will set how quickly your game runs. (60 updates per second is recommended.)</param>
		/// <param name="framesPerSecond">The number of frames per second to target. This only affects the display
		/// rate. Your program will be called based on updatesPerSecond.</param>
		public static void Init(string appName, Action appSetup, int height, int updatesPerSecond, int framesPerSecond) {
			// First, let's remember our identity.
			AppName = appName;
			SetLPS = updatesPerSecond;
			SetFPS = framesPerSecond;

			// Figure out what we're running on.
			SysInfo.DetectPlatform();

			// Get our entry directory.
			Directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

			// Determine and construct the user data path based on the platform.
			switch (SysInfo.Platform)
			{
				case OS.Windows:
					// NOTE: The system does not define "My Games", so we have to access that manually.
					// This may cause issues on non-English installs,
					// so this bit of code should be updated ASAP as soon as a best practice is defined for this scenario.
					// (Supposing that ever happens... *Sigh.*)
					// Until then, we fudge it. :)
					UserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "My Games");
					break;
				case OS.OSX:
					// Has to be hard-coded because Mono is terrible.
					UserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Library/Application Support");
					//UserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
					break;
				case OS.Linux:
					UserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
					break;
				default:
					DebugLog.Failure("Operating system does not have a designated save path!");
					UserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
					break;
			}
			SaveData.UserFolderName = appName;

			// Initialize the debug log.
			DebugLog.Init();

			// Begin the debug log.
			DebugLog.Write(AppName.ToUpperInvariant());
			DebugLog.Write("DEBUG LOG FILE");
			DebugLog.Write("=================");
			DebugLog.Break();
			DebugLog.Write("Powered by OFE " + Version.Number + ".");
			DebugLog.Break();
			DebugLog.Write("The current date is " + DateTime.Now.ToString("d/MMM/yyyy") + ", and the time is " + DateTime.Now.ToString("H:mm:ss") + ".");

			// Initialize APIs.
#if SDL
			// Initialize the SDL2 subsystems we'll be using.
			DebugLog.Break();
			DebugLog.Write("Initializing SDL2...");
			try
			{
				int sdlError = SDL.Init(SDL.Subsystem.Joystick | SDL.Subsystem.Haptic | SDL.Subsystem.Video);
				if (sdlError < 0)
				{
					DebugLog.CriticalError("Could not initilialize SDL2!");
					throw new Exception("Could not initialize SDL2!\nReported reason: " + SDL.Error.Get());
				}
			}
			catch (DllNotFoundException e)
			{
				DebugLog.CriticalError("Could not find the SDL2 dynamic library file!");
				throw new DllNotFoundException("The SDL2 dynamic library file is missing. Check to make sure all the application's files are intact.", e);
			}

			// Success!
			DebugLog.Success("SDL2 was successfully initialized!");

			// Get SDL version information.
			SDL.Version.Number sdlVersion;
			SDL.Version.GetVersion(out sdlVersion);
			DebugLog.Note("SDL2 version is " + sdlVersion.Major + "." + sdlVersion.Minor + "." + sdlVersion.Patch + ".");
			DebugLog.Note("SDL2 revision identifier is " + SDL.Version.GetRevision() + ".");

			// Set the event filter.
			/*DebugLog.Note("Setting SDL event filter...");
			SDL.Event.SetFilter()
			DebugLog.Success("SDL event filter was initialized!");*/
#endif

			DebugLog.Break();

			// Get system information.
			SysInfo.Init();

			// Memorize the functions we need to call.
			Core.appSetup = appSetup;
			//Core.Logic = logic;

			// Record the requested display size.
			Display.SetViewport(height);

			// Run the initial setup.
			Core.setup();

			// Let's get started, shall we?
			mainLoop();

			// And... we're done.
			performShutdown();
		}

		/// <summary>
		/// Initializes the OFE engine.
		/// </summary>
		/// <param name="windowTitle">The title to give the game's window.</param>
		/// <param name="setup">The method to call when it's time for your program to start initializing.</param>
		/// <param name="logic">The method to call when it's time for your program to update.</param>
		/// <param name="updatesPerSecond">The number of times per second to call your program's update method.
		/// This will set how quickly your game runs. (60 updates per second is recommended.)</param>
		/// <param name="framesPerSecond">The number of frames per second to target. This only affects the display
		/// rate. Your program will be called based on updatesPerSecond.</param>
		/// <remarks>Calls to this method should be placed in try/catch blocks. In case of failure, this allows your
		/// program the chance to display an error message with troubleshooting information. This will be helpful
		/// both to the program's users, and yourself.</remarks>
		/// <example>
		/// using System;
		/// using System.Windows.Forms;
		/// 
		/// using VisionRiders.OFE;
		/// 
		/// public static void Main(string[] args) {
		///   // Boot the OFE engine.
		///   try {
		///     Core.Init("Program Name", MyClass.setup, MyClass.logic, 60, 60, 1280, 720);
		///   } catch(Exception e) {
		///     MessageBox.Show(e.ToString());
		///   }
		/// }
		/// </example>
		#endregion

		#region Application
		/// <summary>
		/// The name of the application that is using the OFE engine.
		/// </summary>
		public static string AppName { get; set; }
		#endregion

		#region System
		/// <summary>
		/// Gets the directory where your program was launched from.
		/// </summary>
		public static string Directory { get; private set; }

		/// <summary>
		/// Gets the absolute path to where global user data is to be stored on the current platform.
		/// </summary>
		public static string UserFolder { get; private set; }
		#endregion

		#region Status
		/// <summary>
		/// Whether the engine has finished its core initialization.
		/// </summary>
		/// <remarks>
		/// Is reset to false after shutdown is complete.
		/// </remarks>
		public static bool HasInitialized { get; private set; }

		/// <summary>
    /// Whether the program has system focus (id est, it is the active application).
    /// If the program does not have focus, it will not update,
		/// but the StandBy method will be called instead.
    /// </summary>
    /// <remarks>
    /// On some systems, this will be false when a system menu is up.
    /// </remarks>
    public static bool HasFocus { get; private set; }
    #endregion

    #region FPS (and LPS)
    /// <summary>
    /// The rate of frames per second that the engine is set to display.
    /// Note that this may not be the actual FPS that you're getting, depending on performance.
    /// </summary>
    public static int SetFPS { get; set; }

    /// <summary>
    /// "Logic per second".
		/// The number of times a second that the update cycle is set to be called.
    /// Note that this may not be the actual LPS that you're getting, depending on performance.
    /// </summary>
    public static int SetLPS { get; set; }

    /// <summary>
    /// The actual number of frames per second your program is getting.
    /// </summary>
    public static int FPS { get; internal set; }        

		/// <summary>
		/// The (approximate) total number of draw calls made last frame.
		/// </summary>
		public static int DrawCalls { get; internal set; }

		/// <summary>
		/// The (approximate) total number of vertices drawn last frame.
		/// </summary>
		public static int VertCount { get; internal set; }
    #endregion

    #region Setup and Shutdown
    /// <summary>
    /// The client program's setup routine.
    /// </summary>
    private static Action appSetup;

    /// <summary>
    /// Initializes the game engine.
    /// </summary>
    private static void setup() {
      // Tell the log what we're doing.
      DebugLog.Write("Calling all setup routines...");

			// We certainly aren't shutting down!
			IsShuttingDown = false;

			// Core initialization complete!
			HasInitialized = true;

			// Now, initialize all components.
			Grunt.Init();
					
			Display.Init();
			DrawCycle.Init();
      Stage.Init();
			ShaderProgram.Init();

			Gamepad.Init();
      Keyboard.Init();
      Mouse.Init();
			Music.Init();
      Shape.Init();
      Sound.Init();
      SpriteSource.Init();
      Texture.Init();
      Desktop.Init();

      // Update the log.
      DebugLog.Success("OFE setup complete!");
      DebugLog.Break();
      DebugLog.Write("Calling application setup routine...");

			// Run the main program's setup.
			appSetup();

      // Record success.
      DebugLog.Success("Application setup complete!");

			// Finally, we'll make the window visible for the first time.
			DebugLog.Write("Setting window to visible...");
			Display.Show();
    }

		/// <summary>
		/// Whether or not the engine is in the processing of shutting down.
		/// </summary>
    public static bool IsShuttingDown { get; private set; }

		/// <summary>
		/// The client program's shutdown routines, called before shutting down the program.
		/// This is a good place for saving settings and anything else that needs to be done regardless of how the program is being terminated.
		/// </summary>
		public static Action PrepareForShutdown { get; set; }

    /// <summary>
    /// Initiates the shutdown of the OFE engine and its components and ends your program.
    /// </summary>
    public static void Shutdown() {
      // Tell the log what we're doing.
			DebugLog.Break();
      DebugLog.Write("Shutdown initiated...");
			DebugLog.Write("Request came from:");
			StackTrace trace = new StackTrace(1);
			DebugLog.Write(trace.ToString());// Environment.StackTrace);
			DebugLog.Break();

			// Call the client routine.
			if(PrepareForShutdown != null) {
				PrepareForShutdown();
			}

			// Initiate shutdown!
			IsShuttingDown = true;
    }

		/// <summary>
		/// Performs the actual shutdown operation,
		/// after the main loop has finished out.
		/// </summary>
		private static void performShutdown()
		{
			// Hide the window.
			Display.Hide();

			// Stop the music.
			Music.StopAll();

			// Release ALL the textures!
			Texture.Shutdown();

			// Run shutdown routines.
			DrawCycle.Shutdown();
			Display.Shutdown();

			// Wait for Grunt.
			Grunt.Thread.Join();

			// Shutdown any API hooks.
#if SDL
			DebugLog.Note("Shutting down SDL2...");
			SDL.Quit();
#endif

			// Record success.
			HasInitialized = false;
			DebugLog.Write("OFE has finished shutting down.");

			// Close out the log.
			DebugLog.Release();
		}
		#endregion

		#region Callback Queue
		/// <summary>
		/// A queue of callbacks that must run in the main thread.
		/// </summary>
		/// <remarks>
		/// The update queue is guarded by itself.
		/// </remarks>
		private static Queue<Action> updateCallbacks = new Queue<Action>();

		/// <summary>
		/// Adds a callback to the queue that will run in the main thread.
		/// </summary>
		/// <param name="callback">The callback to run.</param>
		internal static void AddUpdateCallback(Action callback)
		{
			lock (updateCallbacks)
			{
				updateCallbacks.Enqueue(callback);
			}
		}

		/// <summary>
		/// Runs any update callbacks that must be exectured on the main thread..
		/// </summary>
		private static void RunUpdateCallbacks()
		{
			lock (updateCallbacks)
			{
				while (updateCallbacks.Count > 0)
				{
					Action callback = updateCallbacks.Dequeue();
					callback();
				}
			}
		}
		#endregion

		#region Main Loop
		/// <summary>
		/// The time of the last call to the logic cycle.
		/// </summary>
		private static DateTime lastUpdate;

		/// <summary>
		/// The time of the last call to the draw cycle.
		/// </summary>
		private static DateTime lastDraw;

		/// <summary>
		/// Whether the update cycle has run since the last draw cycle.
		/// If not, there's no reason to do the draw cycle yet.
		/// </summary>
		private static bool hasUpdatedSinceLastDraw;

		/// <summary>
		/// An internal counter used to tally frames per second.
		/// </summary>
		internal static int fpsCounter;

		/// <summary>
		/// An internal timer used to tally frames per second.
		/// </summary>
		internal static long fpsTimer;

		/// <summary>
		/// The number of frames to skip the draw cycle for,
		/// in an attempt to keep the logic cycle consistant.
		/// </summary>
		internal static int numFramesToSkip;

		/// <summary>
		/// The engine's main loop.
		/// Repeats endlessly until the end of time;
		/// or the engine is shut down,
		/// whichever comes first.
		/// </summary>
		private static void mainLoop()
		{
			DebugLog.Write("Entering the main loop...");
			DebugLog.Break();

			// We begin now.
			lastUpdate = DateTime.Now;
			lastDraw = DateTime.Now;

			while(!IsShuttingDown)
			{
				// Let's check our mail.
				checkPlatformEvents();

				// Time to update?
				if (numFramesToSkip > 0 || DateTime.Now >= lastUpdate + TimeSpan.FromSeconds(1f / SetLPS))
				{
					// If there's not already a frame skip in place, 
					if (DrawCycle.MaxFrameSkip > 0 && numFramesToSkip <= 0)
					{
						// Are we keeping up?
						float framesLost = ((DateTime.Now - lastUpdate).Milliseconds / 1000f) / (float)(1f / SetLPS);
						if (framesLost >= 2)
						{
							// Skip some frames by running the logic cycle every loop and ignoring the draw cycle.
							numFramesToSkip = Mather.Floor(framesLost) ;

#if DEBUG
							// We lost something precious today...
							DebugLog.Whisper("Skipping " + (numFramesToSkip - 1) + " frame(s).");
#endif

							// Don't skip more than the max allowed.
							if (numFramesToSkip > DrawCycle.MaxFrameSkip)
								numFramesToSkip = DrawCycle.MaxFrameSkip;
						}
					}

					// Advance by one time step.
					float elapsed = 1f / SetLPS;
					lastUpdate = lastUpdate + TimeSpan.FromSeconds(1f / SetLPS);

					// Update!
					Update(elapsed);

					// Reduce the frame skips remaining by one.
					if(numFramesToSkip > 0)
						numFramesToSkip--;

					// Okay, we've updated.
					// Draw cycle needs to run when it's time.
					hasUpdatedSinceLastDraw = true;
				}
				else
				{
					// Nap a moment to give other threads a chance to do things.
					if (!IsShuttingDown)
						Thread.Sleep(1);
				}

				// Time to draw?
				if (!IsShuttingDown && hasUpdatedSinceLastDraw)// && DateTime.Now >= lastDraw + TimeSpan.FromSeconds(1f / SetFPS))
				{
					// Are we allowed to update?
					if (numFramesToSkip <= 0) { 
						draw();
					}
				}				
			}

			DebugLog.Write("Exiting the main loop...");
			DebugLog.Break();
		}

		/// <summary>
		/// Logs FPS, performs the draw cycle, and copies the result to the screen.
		/// </summary>
		private static void draw()
		{
			// Update the FPS counter.
			Core.fpsCounter++;
			Core.fpsTimer += DateTime.Now.Ticks - Core.lastDraw.Ticks;
			if (Core.fpsTimer >= TimeSpan.TicksPerSecond)
			{
				Core.fpsTimer = TimeSpan.Zero.Ticks;
				Core.FPS = Core.fpsCounter;
				Core.fpsCounter = 0;
			}

			// Reset the last draw call time.
			Core.lastDraw = DateTime.Now;

			// Reset the update/draw status.
			hasUpdatedSinceLastDraw = false;

			// Draw.
			DrawCycle.Draw();
			Display.ToScreen();
		}
		#endregion

		#region Events
		/// <summary>
		/// Keep this in memory so that we're not generating extra garbage.
		/// </summary>
		private static SDL.Event.Generic sdlEvent;

		/// <summary>
		/// Check on and deal with events from the system,
		/// or whatever APIs are being used.
		/// </summary>
		private static void checkPlatformEvents()
		{
#if SDL
			// Is there anything waiting for us?			
			//SDL.Event.Generic sdlEvent;
			while (SDL.Event.Poll(out sdlEvent) > 0)
			{
				// What do we have here?
				switch (sdlEvent.Type)
				{
					case SDL.Event.Type.WindowEvent:
						SDL_checkWindowEvent(sdlEvent.Window);
						break;

					case SDL.Event.Type.KeyDown:
						// SDL and OFE use the same USB-derived values.
						Keyboard.Press((Key)sdlEvent.Key.Keysym.Scancode);
						break;
					case SDL.Event.Type.KeyUp:
						// SDL and OFE use the same USB-derived values.
						Keyboard.Release((Key)sdlEvent.Key.Keysym.Scancode);
						break;

					case SDL.Event.Type.Quit:
						Core.Shutdown();
						if (Display.OnClose != null)
							Display.OnClose(typeof(Core), EventArgs.Empty);
						break;
				}
			}
#endif
		}

#if SDL
		/// <summary>
		/// Check on an SDL window event.
		/// </summary>
		/// <param name="windowEvent">The window event structure containing the data recieved from SDL.</param>
		private static void SDL_checkWindowEvent(SDL.Window.Event windowEvent)
		{
			switch(windowEvent.EventID)
			{
				case SDL.Window.EventID.FocusGained:
					HasFocus = true;
					break;
				case SDL.Window.EventID.FocusLost:
					HasFocus = false;
					break;

				case SDL.Window.EventID.SizeChanged:
					// Make sure the new aspect ratio is recorded.
					//Display.SetViewportAspect(windowEvent.Data1 / windowEvent.Data2);
					Display.SetViewport(Display.Height);

					// Invoke the OnResize event.
					if (Display.OnResize != null)
						Display.OnResize.Invoke(typeof(Core), EventArgs.Empty);

					// Redraw the frame.
					draw();
					break;
			}
		}
#endif
#endregion

#region Update Cycle
		/// <summary>
		/// The client program's logic routine.
		/// </summary>
		public static Action<float> AppLogic;

		/// <summary>
		/// Runs all of OFE's various functions each frame.
		/// </summary>
		/// <param name="seconds">The number of seconds (or fraction of seconds) since the last frame.</param>
		internal static void Update(float seconds)
		{
			// Run anything a background thread scheduled for execution in the main thread.
			RunUpdateCallbacks();

			// Make sure we have focus.
			if (!IsInStandby)
			{
				// Always update input functions first!
				Gamepad.UpdateAll();
				Keyboard.Update();
				//Mouse.UpdateAll();

				// Now everything else.
				Fade.Update(seconds);
				Music.Update();
				Desktop.UpdateAll();

				// Run the main program's logic, and then whatever the current game state is.
				if (AppLogic != null)
				{
					AppLogic(seconds);
				}
				if (AppState.Current != null)
				{
					AppState.Current.Update(seconds);
				}

				// Fire any remaining widget events and other callbacks.
				Desktop.FireEvents();
				RunUpdateCallbacks();
				
				// And graphics and audio are always last, reflecting the changes made by everything else this frame.
				Desktop.UpdateGraphics();
				GraphicGroup.UpdateAll();
				Soundscape.UpdateAll();
			}
			else
			{
				if (AppStandby != null)
				{
					AppStandby(seconds);
				}
			}
		}
#endregion

#region Standby
		/// <summary>
		/// The client program's standy routine.
		/// This is called instead of the normal update delegate when the program goes into standby mode.
		/// </summary>
		/// <remarks>
		/// <para>This allows you to continue working in the background.
		/// In most cases, you shouldn't do this, but if you have a game or program that these user
		/// would want to leave running in the background to do something useful that the player
		/// can come back to later, using this delegate is the way to accomplish it.</para>
		/// <para>If you want, you can set Logic and Standby to the same method.</para></remarks>
		public static Action<float> AppStandby { get; set; }

		/// <summary>
		/// Gets whether the program is in standby mode.
		/// </summary>
		public static bool IsInStandby { 
			get {
				if(!HasFocus || isInManualStandby) return true; else return false;
			}
		}

		/// <summary>
		/// Whether or not the application has forced the engine into standby mode.
		/// </summary>
		private static bool isInManualStandby;

		/// <summary>
		/// Manually set the engine to go into standby mode.
		/// </summary>
		/// <remarks>
		/// Useful when using external APIs, such as Steamworks, that OFE does not internally interface with.
		/// When that API takes away focus, you can alert the engine this way.
		/// (Just remember to release it afterwards!)
		/// </remarks>
		public static void ForceStandby()
		{
			isInManualStandby = true;
		}

		/// <summary>
		/// Rescinds a forced standby.
		/// (But will not affect the engine's internally issued standby orders.)
		/// </summary>
		public static void ReleaseStandby()
		{
			isInManualStandby = false;
		}
#endregion
	}
}